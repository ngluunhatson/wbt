
$.widget( "custom.node", {
 
    options: {
        parentNode: undefined,
        childNodes: undefined,
        dataNode: undefined,
        cards: [],
        position: "",
        bod: undefined,
        dept: false,
        team_name: "",
        level: undefined,
        amount: undefined,
        year: undefined,
        headerClick: undefined,
        nodeClick: undefined,
    },

    _create: function() {
        var self = this;
        this.element.data('amount', this.options.amount);
        if(this.options.childNodes === undefined)
            this.options.childNodes = [];

        if(this.options.cards.length === 0
           || this.options.cards.length > 1) {
            
            var header = $('<div class="header"><div class="text position"></div><div class="team_name"></div></div>');
            header.find("div.position").text(this.options.position);
            //if(this.options.team_name)
                //header.find("div.team_name").text(this.options.team_name);
            // console.log(this.options);
            if(this.options.cards.length === 0) {
                    $(header).addClass('nocard');
                }
            if(this.options.bod === true)
                $(header).addClass("bod");
            else if(this.options.dept === true)
                $(header).addClass("dept");
            else if(this.options.level == 0)
                $(header).addClass("level0");
            else if(this.options.level == 1)
                $(header).addClass("level1");
            else if(this.options.level == 2)
                $(header).addClass("level2");
            else if(this.options.level == 3)
                $(header).addClass("level3");
            else if(this.options.level >= 4)
                $(header).addClass("level4");
            this.element.append(header);
            header.click(function() {
                var isVisible = $(self.nodeContent).toggle().is(":visible");
                self.options.dataNode.collapse = !isVisible;
                if(self.options.headerClick) {
                    // console.log("Click here!");
                    self.options.headerClick(self.options.dataNode, self.element);
                }
            });
            this.nodeHeader = header;
            
        }
        var nodeContent = $("<div class='content'></div>");
        nodeContent.append(this.options.cards);
        // console.log(this.options.childNodes);
        this.element.append(nodeContent);
        if (this.options.dept == false) {
            if (this.options.position != "COO") {
                this.element.append('<span class="edit_action" title="Update"></span')
                this.element.append('<span class="add_action" title="Add"></span>')
            }
        }
        
        var dotLeft = $('<div class="dotLeft"></div>');
        var dotRight = $('<div class="dotRight"></div>');
        var dotTop = $('<div class="dotTop"></div>');
        var dotBottom = $('<div class="dotBottom"></div>');
        
        this.dotLeft = dotLeft;
        this.dotRight = dotRight;
        this.dotTop = dotTop;
        this.dotBottom = dotBottom;

        this.element.append(dotLeft);
        this.element.append(dotRight);
        this.element.append(dotTop);
        this.element.append(dotBottom);
        
        nodeContent.click(function () {
            if(self.options.nodeClick)
                self.options.nodeClick(self.options.dataNode, self.element);
        });
        this.nodeContent = nodeContent;
        
        
        this.hasFooter = false;
        if(this.options.amount) {
            var footer = $('<div class="footerChart"></div>');
            var text = "(0/" + this.options.amount + ")";
            if(this.options.dataNode 
                && this.options.dataNode.employees 
                && this.options.dataNode.employees.length > 0) {
                    text = "(" + this.options.dataNode.employees.length + "/" + this.options.amount + ")";
            }
            if(this.options.year)
                text += " (" + this.options.year + ")";
            $(footer).text(text);
            this.element.append(footer);
            this.nodeFooter = footer;
            this.hasFooter = true;
        }
        
        if(this.options.dataNode && this.options.dataNode.collapse === true)
            $(self.nodeContent).hide();
        else if(this.options.dataNode && this.options.dataNode.collapse === false)
            $(self.nodeContent).show();
        
    },
    reDraw: function() {

    },
    // Create a public method.
    childNodes: function() {
        return this.options.childNodes;
    },
    clearChildNodes: function() {
        this.options.childNodes.length = 0;
    },
    parentNode: function() {
        return this.options.parentNode;
    },
    leftDot: function() {
        
        if(this.options.dataNode 
            && this.options.dataNode.employees 
            && this.options.dataNode.employees.length > 1) {

            var pos = getDot("left", this.element);
            var top = $(this.element).position().top + $(this.nodeHeader).height() / 2;
            if(this.hasFooter)
                top += $(this.nodeFooter).height() / 2 + 2;
            // console.log(pos);
            return {x: pos.x, y: top};
        }
        return getDot("left", this.element);
    },
    rightDot: function() {
        if(this.options.dataNode 
            && this.options.dataNode.employees 
            && this.options.dataNode.employees.length > 1) {

            var pos = getDot("right", this.element);
            var top = $(this.element).position().top + $(this.nodeHeader).height() / 2;
            if(this.hasFooter)
                top += $(this.nodeFooter).height() / 2 + 2;
            return {x: pos.x, y: top};
        }
        return getDot("right", this.element);
    },
    topDot: function() {
        return getDot("top", this.element);
    },
    bottomDot: function() {
        var dot = getDot("bottom", this.element);
        if(!this.hasFooter)
            return dot;
        
        return {x: dot.x, y: dot.y - 20};
    },
    showDot: function(dotType) {
        if(dotType === "left") {
            this.dotLeft.show();
        }
        if(dotType === "right") {
            this.dotRight.show();
        }
        if(dotType === "top") {
            this.dotTop.show();
        }
        if(dotType === "bottom") {
            this.dotBottom.show();
        }
    },
    cards: function() {
        return this.options.cards;
    },
    dataNode: function() {
        return this.options.dataNode;
    },
    isCollapse: function() {
        return !$(this.nodeContent).is(":visible");
    },
    initPos: function(pos) {
        if(pos === undefined) {
            if(this.initPosData === undefined)
                return $(this.element).position();
            else
                return this.initPosData;
        } else 
            this.initPosData = pos;
    }
});