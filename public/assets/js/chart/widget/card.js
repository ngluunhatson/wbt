
$.widget("custom.card", {
  
    options: {
        title: "No Name",
        avatar: "/uploads/common/icon.jpeg",
        department: "",
        code: "",
        position: undefined,
        amount : 0,
        data: {},
        showPos: true,
        level: undefined,
    },

    _create: function () {
        if (Object.keys(this.options.data).length === 0) {
            if (this.options.position !== undefined) {
                var position = $('<div class="position"></div>');
                position.text(this.options.position);
                this.element.append(`<div class="position">${this.options.position} <p>0/1</p></div>`);
            }
        } else {
            var avatar = $('<img class="avatar" src="/uploads/common/icon.jpeg"/>');
            if (this.options.avatar != null) {
                $(avatar).attr({
                    "src": this.options.avatar
                })
            }
                
            this.element.append(avatar);
    
            var hr = $('<hr/>');
            this.element.append(hr);
    
            var title = $('<div class="title"></div>');
            title.text(this.options.title);
            this.element.append(title);
    
            var code = $('<div class="code"></div>');
            code.text(this.options.code);
            this.element.append(code);
    
            // console.log(this.options.position);
            if (this.options.position !== undefined && this.options.showPos === true) {
                var html = `<div class="position ${!this.options.level ? 'level0' : `level${this.options.level}`} flex-column">`;
                html += '<div> ' + this.options.position + '</div>'
                // html += '<div> ' + this.options.department + '</div>'
                html += '</div>'

                this.element.append(html);
            }
    
            var dotLeft = $('<div class="dotLeft"></div>');
            var dotRight = $('<div class="dotRight"></div>');
            var dotTop = $('<div class="dotTop"></div>');
            var dotBottom = $('<div class="dotBottom"></div>');
            this.element.append(dotLeft);
            this.element.append(dotRight);
            this.element.append(dotTop);
            this.element.append(dotBottom);
        }
    },

    // Create a public method.
    title: function (value) {

        // No value passed, act as a getter.
        if (value === undefined) {
            return this.options.title;
        }

        // Value passed, act as a setter.
        this.options.title = value;
        var progress = this.options.value + "%";
        this.element.text(progress);
    }
});