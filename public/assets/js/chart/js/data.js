var rooms = [
    {
        "id": 7,
        "name": "BAN GIÁM ĐỐC",
        "bod": true,
        "positions": [
            {
                "id": 9999,
                "room_id": 7,
                "team_id": 7,
                "year": 2024,
                "name": "TỔNG GIÁM ĐỐC",
                "amount": 1,
                "employees": [
                    {
                        // "id": 1,
                        // "position_id": 9999,
                        // "room_id": 7,
                        // "team_id": 7,
                        // "name": "Nguyễn Thu Hằng",
                        // "avatar": "/uploads/common/icon.jpeg",
                        // "code": 100
                    },
                ]
            },
            {
                "id": 9998,
                "room_id": 7,
                "team_id": 7,
                "name": "TRỢ LÝ",
                "amount": 1,
                "employees": [
                    {
                        "id": 2,
                        "position_id": 9998,
                        "room_id": 7,
                        "team_id": 7,
                        "name": "Thái Thị Thu Hà",
                        "avatar": "/uploads/common/icon.jpeg",
                        "code": 101
                    }
                ]
            },
        ]
    },
    {
        "id": 1,
        "name": "QUẢN TRỊ MARKETING",
        "bod": false,
        
        "positions": [
            {
                "id": 1,
                "team_id": 1,
                "team_name": "MARKETING",
                "order": 1,
                "amount": 1,
                "name": "GIÁM ĐỐC",
                "year": null,
                "level": 0,
                "parent_position_id": null,
                "employees": [
                    {
                        "id": 3,
                        "position_id": 1,
                        "room_id": 1,
                        "team_id": 1,
                        "name": "Trần Văn Tèo",
                        "avatar": "/uploads/common/icon.jpeg",
                        "code": 102
                    }
                ],
                "children": [
                    {
                        "id": 2,
                        "team_id": 8,
                        "team_name": "MEDIA",
                        "order": 1,
                        "amount": 1,
                        "name": "TRƯỞNG PHÒNG",
                        "year": 2025,
                        "level": 1,
                        "parent_position_id": 1,
                        "employees": [
                            {
                                "id": 4,
                                "position_id": 1,
                                "room_id": 1,
                                "team_id": 1,
                                "name": "Trần Văn Tèo",
                                "avatar": "/uploads/common/icon.jpeg",
                                "code": 102
                            },
                        ],
                        "children": [
                            {
                                "id": 5,
                                "team_id": 8,
                                "team_name": "MEDIA",
                                "order": 1,
                                "amount": 1,
                                "name": "TỔ TRƯỞNG",
                                "year": 2022,
                                "level": 2,
                                "parent_position_id": 2,
                                "employees": [
                                    
                                ],
                                "children": [
                                    {
                                        "id": 10,
                                        "team_id": 8,
                                        "team_name": "QUAY PHIM",
                                        "order": 1,
                                        "amount": 1,
                                        "name": "CHUYÊN VIÊN / NHÂN VIÊN",
                                        "year": 2022,
                                        "level": 3,
                                        "parent_position_id": 5,
                                    },
                                    {
                                        "id": 11,
                                        "team_id": 8,
                                        "team_name": "THIẾT KẾ POSM",
                                        "order": 2,
                                        "amount": 1,
                                        "name": "CHUYÊN VIÊN",
                                        "year": 2022,
                                        "level": 3,
                                        "parent_position_id": 5,
                                    },
                                    {
                                        "id": 17,
                                        "team_id": 8,
                                        "team_name": "THIẾT KẾ VIDEO",
                                        "order": 3,
                                        "amount": 1,
                                        "name": "CHUYÊN VIÊN",
                                        "year": 2022,
                                        "level": 3,
                                        "parent_position_id": 5,
                                    },
                                    {
                                        "id": 18,
                                        "team_id": 8,
                                        "team_name": "SẢN XUẤT TRUYỀN THÔNG",
                                        "order": 4,
                                        "amount": 1,
                                        "name": "CHUYÊN VIÊN / NHÂN VIÊN",
                                        "year": 2022,
                                        "level": 3,
                                        "parent_position_id": 5,
                                    }
                                ]
                            },
                            {
                                "id": 6,
                                "team_id": 10,
                                "team_name": "TỔ CHỨC SỰ KIỆN",
                                "order": 2,
                                "amount": 1,
                                "name": "TỔ TRƯỞNG",
                                "year": 2022,
                                "level": 2,
                                "parent_position_id": 2,
                                "employees": [
                                    {
                                        "id": 5,
                                        "position_id": 1,
                                        "room_id": 1,
                                        "team_id": 1,
                                        "name": "Trần Văn Tèo",
                                        "avatar": "/uploads/common/icon.jpeg",
                                        "code": 102
                                    },
                                    
                                ],
                                "children": [
                                    {
                                        "id": 12,
                                        "team_id": 8,
                                        "team_name": "TỔ CHỨC SỰ KIỆN",
                                        "order": 1,
                                        "amount": 1,
                                        "name": "CHUYÊN VIÊN",
                                        "year": 2022,
                                        "level": 3,
                                        "parent_position_id": 5,
                                        "children": [
                                            {
                                                "id": 19,
                                                "team_id": 8,
                                                "team_name": "",
                                                "order": 1,
                                                "amount": 1,
                                                "name": "TẠP VỤ",
                                                "year": 2022,
                                                "level": 4,
                                                "parent_position_id": 5,
                                                "employees": [
                                                    {
                                                        "id": 5,
                                                        "position_id": 1,
                                                        "room_id": 1,
                                                        "team_id": 1,
                                                        "name": "Trần Văn Tèo",
                                                        "avatar": "/uploads/common/icon.jpeg",
                                                        "code": 102
                                                    },
                                                    {
                                                        "id": 52,
                                                        "position_id": 1,
                                                        "room_id": 1,
                                                        "team_id": 1,
                                                        "name": "Trần Văn Tèo",
                                                        "avatar": "/uploads/common/icon.jpeg",
                                                        "code": 102
                                                    }
                                                ],
                                                "children1": [
                                                    {
                                                        "id": 20,
                                                        "team_id": 8,
                                                        "team_name": "",
                                                        "order": 1,
                                                        "amount": 1,
                                                        "name": "BẢO VỆ",
                                                        "year": 2022,
                                                        "level": 4,
                                                        "parent_position_id": 5,
                                                        "children": [
                                                            
                                                        ],
                                                        "employees": [
                                                            {
                                                                "id": 4,
                                                                "position_id": 1,
                                                                "room_id": 1,
                                                                "team_id": 1,
                                                                "name": "Trần Văn Tèo",
                                                                "avatar": "/uploads/common/icon.jpeg",
                                                                "code": 102
                                                            },
                                                            {
                                                                "id": 6,
                                                                "position_id": 1,
                                                                "room_id": 1,
                                                                "team_id": 1,
                                                                "name": "Trần Văn Tèo",
                                                                "avatar": "/uploads/common/icon.jpeg",
                                                                "code": 102
                                                            }
                                                        ],
                                                    }
                                                ]
                                            },
                                            // {
                                            //     "id": 20,
                                            //     "team_id": 8,
                                            //     "team_name": "",
                                            //     "order": 2,
                                            //     "amount": 1,
                                            //     "name": "TẠP VỤ",
                                            //     "year": 2022,
                                            //     "level": 4,
                                            //     "parent_position_id": 5,
                                            // }
                                        ]
                                    },
                                    {
                                        "id": 18,
                                        "team_id": 8,
                                        "team_name": "",
                                        "order": 2,
                                        "amount": 2,
                                        "name": "TẠP VỤ",
                                        "year": 2022,
                                        "level": 4,
                                        "parent_position_id": 5,
                                        "employees": [
                                            {
                                                "id": 15,
                                                "position_id": 1,
                                                "room_id": 1,
                                                "team_id": 1,
                                                "name": "Trần Văn Tèo",
                                                "avatar": "/uploads/common/icon.jpeg",
                                                "code": 102
                                            },
                                            {
                                                "id": 151,
                                                "position_id": 1,
                                                "room_id": 1,
                                                "team_id": 1,
                                                "name": "Trần Văn Tèo",
                                                "avatar": "/uploads/common/icon.jpeg",
                                                "code": 102
                                            },
                                        ]
                                    },
                                    {
                                        "id": 19,
                                        "team_id": 8,
                                        "team_name": "",
                                        "order": 4,
                                        "amount": 1,
                                        "name": "BẢO VỆ",
                                        "year": 2022,
                                        "level": 4,
                                        "parent_position_id": 5,
                                    }
                                ]
                            },
                            {
                                "id": 62,
                                "team_id": 10,
                                "team_name": "TEST",
                                "order": 3,
                                "amount": 1,
                                "name": "TỔ TRƯỞNG",
                                "year": 2022,
                                "level": 2,
                                "parent_position_id": 2,
                                "employees": [
                                    
                                ],

                            },
                            {
                                "id": 61,
                                "team_id": 10,
                                "team_name": "TEST",
                                "order": 4,
                                "amount": 1,
                                "name": "TỔ TRƯỞNG",
                                "year": 2022,
                                "level": 2,
                                "parent_position_id": 2,
                                "employees": [
                                    
                                ],
                                "children": [
                                    {
                                        "id": 62,
                                        "team_id": 10,
                                        "team_name": "TEST",
                                        "order": 1,
                                        "amount": 1,
                                        "name": "GÁC CỔNG",
                                        "year": 2022,
                                        "level": 4,
                                        "parent_position_id": 2,
                                        "children": [
                                            {
                                                "id": 64,
                                                "team_id": 10,
                                                "team_name": "TEST",
                                                "order": 2,
                                                "amount": 1,
                                                "name": "GÁC CỔNG",
                                                "year": 2022,
                                                "level": 5,
                                                "parent_position_id": 2,
                                            }
                                        ]
                                    },
                                    {
                                        "id": 63,
                                        "team_id": 10,
                                        "team_name": "TEST",
                                        "order": 2,
                                        "amount": 1,
                                        "name": "GÁC CỔNG",
                                        "year": 2022,
                                        "level": 4,
                                        "parent_position_id": 2,
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "id": 3,
                        "team_id": 1,
                        "team_name": "MARKETING",
                        "order": 2,
                        "amount": 1,
                        "name": "TRƯỞNG PHÒNG",
                        "year": 2025,
                        "level": 1,
                        "parent_position_id": 1,
                        "employees": [
                            
                        ],
                        "children": [
                            {
                                "id": 7,
                                "team_id": 17,
                                "team_name": "DIGITAL MARKETING",
                                "order": 1,
                                "amount": 1,
                                "name": "TỔ TRƯỞNG",
                                "year": 2022,
                                "level": 2,
                                "parent_position_id": 3,
                                "employees": [
                                    
                                ],
                                "children": [
                                    {
                                        "id": 14,
                                        "team_id": 8,
                                        "team_name": "NỘI DUNG QC TRỰC TUYẾN",
                                        "order": 1,
                                        "amount": 1,
                                        "name": "CHUYÊN VIÊN / NHÂN VIÊN",
                                        "year": 2022,
                                        "level": 3,
                                        "parent_position_id": 7,
                                    },
                                    {
                                        "id": 13,
                                        "team_id": 8,
                                        "team_name": "DIGITAL MARKETING",
                                        "order": 2,
                                        "amount": 1,
                                        "name": "CHUYÊN VIÊN / NHÂN VIÊN",
                                        "year": 2022,
                                        "level": 3,
                                        "parent_position_id": 7,
                                    },
                                    {
                                        "id": 15,
                                        "team_id": 8,
                                        "team_name": "CSKH ONLINE",
                                        "order": 3,
                                        "amount": 1,
                                        "name": "CHUYÊN VIÊN / NHÂN VIÊN",
                                        "year": 2022,
                                        "level": 3,
                                        "parent_position_id": 7,
                                    },
                                    
                                ],
                            },
                            {
                                "id": 8,
                                "team_id": 18,
                                "team_name": "PR",
                                "order": 2,
                                "amount": 1,
                                "name": "TỔ TRƯỞNG",
                                "year": 2022,
                                "level": 2,
                                "parent_position_id": 3,
                                "employees": [
                                    
                                ],
                                "children": [
                                    {
                                        "id": 81,
                                        "team_id": 18,
                                        "team_name": "NỘI DUNG SEO",
                                        "order": 1,
                                        "amount": 1,
                                        "name": "CHUYÊN VIÊN",
                                        "year": 2022,
                                        "level": 3,
                                        "parent_position_id": 3,
                                        "employees": [
                                            
                                        ],
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "id": 4,
                        "team_id": 9,
                        "team_name": "CÔNG NGHỆ",
                        "order": 3,
                        "amount": 1,
                        "name": "TRƯỞNG PHÒNG",
                        "year": 2025,
                        "level": 1,
                        "parent_position_id": 1,
                        "employees": [
                            
                        ],
                        "children": [
                            {
                                "id": 8,
                                "team_id": 18,
                                "team_name": "PHẦN MÈM",
                                "order": 1,
                                "amount": 1,
                                "name": "TỔ TRƯỞNG",
                                "year": 2022,
                                "level": 2,
                                "parent_position_id": 3,
                                "employees": [
                                    
                                ],
                            },
                            {
                                "id": 9,
                                "team_id": 19,
                                "team_name": "KỸ THUẬT",
                                "order": 2,
                                "amount": 1,
                                "name": "TỔ TRƯỞNG",
                                "year": 2022,
                                "level": 2,
                                "parent_position_id": 3,
                                "employees": [
                                    
                                ],
                            }
                        ]
                    },
                    {
                        "id": 55,
                        "team_id": 9,
                        "team_name": "Test",
                        "order": 4,
                        "amount": 1,
                        "name": "TRƯỞNG PHÒNG",
                        "year": 2025,
                        "level": 1,
                        "parent_position_id": 1,
                        "employees": [
                            
                        ],
                        "children": [

                        ]
                    }
                ]
            }
        ]
    },
    {
        "id": 2,
        "name": "QUẢN TRỊ KINH DOANH",
        "bod": false
    },
    {
        "id": 3,
        "name": "QUẢN TRỊ CHĂM SÓC KHÁCH HÀNG",
        "bod": false
    },
    {
        "id": 4,
        "name": "QUẢN TRỊ TÀI CHÍNH - KẾ TOÁN",
        "bod": false
    },
    {
        "id": 5,
        "name": "QUẢN TRỊ MUA HÀNG",
        "bod": false
    },
    {
        "id": 6,
        "name": "QUẢN TRỊ HC - NS",
        "bod": false
    }
];
