// --------------------------------------------------------
// Sự kiện click vào header của node
// --------------------------------------------------------
function onHeaderClick(data, el) {
    // console.log("header click", data, el);
    // ---------------------------------------------------------
    // Chú ý:
    //   data: là dữ liệu json của node đang click
    //   el: là element trên giao diện của node đang click
    // Để lấy được node cha: $(el).node("parentNode")
    // Để lấy được danh sách node con: $(el).node("childNodes")
    // ---------------------------------------------------------
    if (data.employees.length > 1) {
        // ================================================================================
        // Đoạn này chỉ xử lý cho trường hợp các node từ level 3 trở đi
        // Vì các node level 3 trở đi được vẽ theo hình dạng nhánh nút 2 bên đối xứng nhau
        if (data.level >= 3) {
            var dataNode = $(el).node("parentNode").node("dataNode");
            if (dataNode) {
                correctSilblingLv3($(el).node("parentNode"), $(el));
            }
        }
        // ================================================================================

        // ////////////////////////////////////////////////////////////////////////////////
        // Đoạn này xử lý cho trường hợp node đang đóng mở có các node con
        var childNodes = $(el).node("childNodes");
        
        childNodes.forEach((item) => {
            clearChildren(item);
        });
        $(el).node("clearChildNodes");
        var id = $(el)[0].id
        console.log('id', id)
        
        var top = $(el).node("initPos")?.y;
        var left = $(el).node("initPos")?.x;
        if ($(el).node("isCollapse")) {
            drawChildPos(data.children, left, top + 150, el, data.id, $('.containerChart'));
            $(`#${id} > .level${data.level}`).css('border-radius', '8px')
        }
        else {
            $(el).css('border-radius', '8px 8px 0 0')
            drawChildPos(data.children, left, top + $(el).height() + 50, el, data.id, $('.containerChart'));
            // Cập nhật lại độ cao của các đối tượng SVG
            // Các đối tượng SVG là các đường line kết nối các node với nhau
            $("svg").attr("height", (farestY + 200) + "px");
            $(`#${id} > .level${data.level}`).css('border-radius', '8px 8px 0 0')
            data.employees.map(element => $(`#card${element.id}`).css('border-radius', 0));
            var contentEl = $(`#${id} > .content`)[0];
            var lastChildren = contentEl.children[contentEl.children.length -1].id;
            $(`#${lastChildren}`).css('border-radius', '0 0 8px 8px')
        }
        // ////////////////////////////////////////////////////////////////////////////////
    }
};

// ---------------------------------------------------------------------
// Hàm này sẽ vẽ lại các node level 3 bên dưới node vừa được bung/đóng 
// ---------------------------------------------------------------------
function correctSilblingLv3(parentNodeUI, currentNodeUI) {

    var childNodes = parentNodeUI.node("childNodes");
    if (childNodes && childNodes.length > 1) {
        var currentData = currentNodeUI.node("dataNode");

        // Sort các node con theo thứ tự của positin_order tăng dần
        childNodes.sort((a, b) => a.position_order - b.position_order);

        var parentNodeData = parentNodeUI.node("dataNode");
        // Vị trí động được tính toán để vẽ lại các node bên dưới
        var dynPos = undefined;
        // Cờ này dùng để chỉ định node vừa bung/đóng là node nằm bên lẻ hay không
        var isOdd = false;
        childNodes.forEach(el => {
            var childNodeData = el.node("dataNode");
            // Đây là trường hợp phát hiện ra node đang đóng/mở trong danh sách node con
            if (childNodeData.id === currentData.id) {

                if (childNodeData.position_order % 2 === 1)
                    isOdd = true;
                else
                    isOdd = false;

                // Vị trí nguyên thủy ban đầu của node này lúc mới bắt đầu vẽ chung toàn chart 
                // Chú ý: 
                //   . với panel có thanh cuộn (scroll) thì vị trí động sẽ thay đổi.
                //   . việc thiết lập vị trí absoution sẽ tính theo ví toàn bộ của panel chứ không phải vị trí động
                var oriPos = el.node("initPos");
                if ($(el).node("isCollapse"))
                    dynPos = { x: oriPos.x, y: oriPos.y + 150 };
                else
                    dynPos = { x: oriPos.x, y: oriPos.y + el.height() + 50 };
            }
            // Các node khác bên dưới node hiện tại
            // Chú ý: các node bên trên của node đang đóng/mở thì không phải đối tượng vẽ lại!
            else {
                if (dynPos) {
                    var connectId = "connect_" + parentNodeData.id + "_" + childNodeData.id;
                    if (childNodeData.position_order % 2 === 0 && !isOdd) {
                        setPosition(el, dynPos);
                        // Nhớ là sau khi thiết lập vị trí mới cho node, 
                        // -> cần phải lưu vị trí đó vào bộ dữ liệu bên trong node đó
                        el.node("initPos", dynPos);

                        connectNode(parentNodeUI, el, connectId, "bottomDot", "leftDot");
                        if ($(el).node("isCollapse"))
                            dynPos = { x: dynPos.x, y: dynPos.y + 152 };
                        else
                            dynPos = { x: dynPos.x, y: dynPos.y + el.height() + 50 };
                    }
                    else if (childNodeData.position_order % 2 === 1 && isOdd) {
                        setPosition(el, dynPos);
                        // Nhớ là sau khi thiết lập vị trí mới cho node, 
                        // -> cần phải lưu vị trí đó vào bộ dữ liệu bên trong node đó
                        el.node("initPos", dynPos);

                        connectNode(parentNodeUI, el, connectId, "bottomDot", "rightDot");
                        if ($(el).node("isCollapse"))
                            dynPos = { x: dynPos.x, y: dynPos.y + 152 };
                        else
                            dynPos = { x: dynPos.x, y: dynPos.y + el.height() + 50 };
                    }

                }
            }
            // Tính toán lại vị trí xa nhất sâu nhất để tăng độ rộng của các đối tương SVG 
            // Các đối tượng SVG là các đường line vẽ kết nối các node
            if (dynPos)
                if (farestY < dynPos.y)
                    farestY = dynPos.y;
        });
        $("svg").attr("height", (farestY + 200) + "px");
    }
};

// --------------------------------------------------------
// Sự kiện click vào 1 node có nhân sự
// --------------------------------------------------------
function onNodeClick(data, el) {
    // console.log("node click", data, el);
    // ---------------------------------------------------------
    // Chú ý:
    //   data: là dữ liệu json của node đang click
    //   el: là element trên giao diện của node đang click
    // Để lấy được node cha: $(el).node("parentNode")
    // Để lấy được danh sách node con: $(el).node("childNodes")
    // ---------------------------------------------------------
    // console.log("childNodes", $(el).node("childNodes"), "parentNode", $(el).node("parentNode"));
    console.log("data", data);
}

function actionEdit() {
    $(".edit_action").on("click", (event) => {
        console.log($(event.target).parent().data("room_id"));
        var room_id = $(event.target).parent().data("room_id");
        var team_id = $(event.target).parent().data("team_id");
        var position_id = $(event.target).parent().data("position_id");
        var rank = $(event.target).parent().data("rank");
        $("#myModalEdit").attr("style", "display: block;");
        if ($('#amount').length > 0 ) { 
            $('#amount').remove()
        }
        if ($('#idOrgan').length > 0 ) {
            $('#idOrgan').remove()
        }
        var url = `/chart/showFormEdit/${room_id}/${team_id}/${position_id}/${rank}`;
        var bodFlag = false;
        var personSelected = [];
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                $("#myModalEdit").attr("style", "display: block;");
                $(".contentBodyModalEdit").find(".appendEdit").remove();
                $(".contentBodyModalEdit").find("#idOrgan").remove();
                $("#staff").find("option").remove();
                var staffInChart = [];
                var idInChart = [];
                var i = 1;
                var amount = 0;
                var html = "";
                Object.keys(data.oragnizational).forEach((index) => {
                    staffInChart.push(data.oragnizational[index].basic_info_id);
                    idInChart.push(data.oragnizational[index].id);
                    amount = data.oragnizational[index].position.amount;
                    if (data.oragnizational[index].room.bod == true) {
                        bodFlag = true;
                    }
                    if (data.oragnizational[index].staff != null) {
                        personSelected.push('<option selected value="' + data.oragnizational[index].staff.id + '"> Mã nv : '
                            + data.oragnizational[index].staff.employee_code + ' || Tên : ' + data.oragnizational[index].staff.full_name + '</option>')
                    } else {
                        personSelected.push('')
                    }
                    $('#room_name').text(data.oragnizational[index].room.name)
                    $('#team_name').text(data.oragnizational[index].team.name)
                    $('#position_name').text(data.oragnizational[index].position.name)
                    $('#room_id').val(data.oragnizational[index].room.id)
                    $('#team_id').val(data.oragnizational[index].team.id)
                    $('#position_id').val(data.oragnizational[index].position.id)
                });
                if (amount > 0) {
                    label = "Họ tên";
                    for (i = 0; i < amount; i++) {
                        if (idInChart.length > 0) {
                            html +=
                                '<input id="idOrgan" hidden value="' +
                                idInChart[i] +
                                '" name="idOrgan_' +
                                (i + 1) +
                                '">';
                        }

                        html += '<div class="col-sm-12 d-flex justify-content-between row appendEdit">' +
                            '<p class="col-sm-4 form-label fw-bold" style="text-align:right;">' + label + '</p>' +
                            '<div class="text-left col-sm-8 form-label form-group"><select id="staff_' + (i + 1) + '" class="form-control" name="basic_info_id_' + (i + 1) + '">'
                        html += '<option selected value="">Chưa có</option>'
                        if (personSelected.length > 0) {
                            html += personSelected[i];
                        }
                        data.staff.forEach(function (value) {
                            html += '<option value="' + value.id + '"> Mã nv : ' + value.employee_code + ' || Tên : ' + value.full_name + '</option>'
                        })
                        // for(j = 0; j < data.staff.length; j++) {
                        //     if (staffInChart[j] == data.staff[j].id) {
                        //         var index = data.staff.indexOf(data.staff[j]);
                        //         html += '<option selected value="' + data.staff[j].id + '"> Mã nv : ' + data.staff[j].employee_code + ' || Tên : ' + data.staff[j].full_name + '</option>'
                        //     } else {
                        //         html += '<option value="' + data.staff[j].id + '"> Mã nv : ' + data.staff[j].employee_code + ' || Tên : ' + data.staff[j].full_name + '</option>'
                        //     }
                        //     if (amount > 1) {
                        //         data.staff.splice(index, 1);
                        //     }
                        // }

                        html += "</select></div></div>";

                        html +=
                            '<input id="amount" hidden value="' +
                            amount +
                            '" name="amount">';
                    }

                    $(".contentBodyModalEdit").append(html);
                }
            },
            error: function (error) {
                handleFails(error);
            },
        });
        $("#btnSaveEdit").off("click");
        $("#btnSaveEdit").on("click", () => {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $("#tokenForm").val(),
                },
            });
            $.ajax({
                type: "POST",
                url: "/chart/tree/storeUpdate",
                data: $("#formEditChart").serialize(),
                success: function (response) {
                    $("#myModalEdit").attr("style", "display: none;");
                    var container = $(".containerChart");
                    if (bodFlag) {
                        container.find(".bodContainer").empty();
                        container.find(".depts").empty();
                        container.find(".lineContainer").empty();
                        container.find("svg").empty();
                        container.find(".dotFly").empty();
                        $.ajax({
                            type: "GET",
                            url: "/chart/chartBasic",
                            success: function (data) {
                                rooms = data;
                            },
                        }).done(function () {
                            var posY = 185;
                            drawChart1(
                                rooms,
                                container.position().left + 500,
                                posY,
                                container
                            );
                            $(".dept").on("click", () => {
                                $(window.parent.$.find("#btnBack")).show();
                            });
                            actionEdit();
                        });
                    } else {
                        $(".chart_heading").hide();
                        var svg = container.find("svg").html();
                        container.empty();
                        container.append(
                            '<div class="lineContainer"> ' + svg + "</div>"
                        );
                        $.ajax({
                            type: "GET",
                            url: "/chart/getChartRoom/" + $("#room_id").val(),
                            success: function (data) {
                                dataRoom = data;
                            },
                        }).done(function () {
                            var posY = 185;
                            drawChart2(
                                dataRoom,
                                container.position().left + 500,
                                posY,
                                container
                            );
                            actionEdit();
                        });
                    }
                },
                error: function (error) {
                    handleFails(error, '#formEditChart');
                },
            });
        });
    });

    $(".add_action").on("click", (event) => {
        var room_id = $(event.target).parent().data("room_id");
        var team_id = $(event.target).parent().data("team_id");
        var position_id = $(event.target).parent().data("position_id");
        var rank = $(event.target).parent().data("rank");
        $("#myModalCreate").attr("style", "display: block;");
        if ($('#amount').length > 0 ) { 
            $('#amount').remove()
        }
        if ($('#idOrgan').length > 0 ) {
            $('#idOrgan').remove()
        }

        var url = `/chart/showFormAdd/${room_id}/${team_id}/${position_id}/${rank}`;
        var bodFlag = false;
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                $("#myModalCreate").attr("style", "display: block;");
                $(".contentBodyModalCreate").find(".appendEdit").remove();
                $(".contentBodyModalCreate").find("#idOrgan").remove();
                $("#staff").find("option").remove();
                var staffInChart = [];
                var idInChart = [];
                var i = 1;
                var amount = 0;
                var html = "";
                if (Object.keys(data.oragnizational).length != 3) {

                    Object.keys(data.oragnizational).forEach((index) => {
                        staffInChart.push(data.oragnizational[index].basic_info_id);
                        idInChart.push(data.oragnizational[index].id);
                        amount = data.oragnizational[index].position.amount;
                        if (data.oragnizational[index].room.bod == true) {
                            bodFlag = true;
                        }
                        $("#room_name_create").text(
                            data.oragnizational[index].room.name
                        );
                        $("#team_name_create").text(
                            data.oragnizational[index].team.name
                        );
                        $("#position_name_create").text(
                            data.oragnizational[index].position.name
                        );
                        $("#room_id_create").val(
                            data.oragnizational[index].room.id
                        );
                        $("#team_id_create").val(
                            data.oragnizational[index].team.id
                        );
                        $("#position_id_create").val(
                            data.oragnizational[index].position.id
                        );
                    });
                } else {

                    amount = data.oragnizational.position.amount;
                    if (data.oragnizational.room.bod == true) {
                        bodFlag = true;
                    }
                    $("#room_name_create").text(
                        data.oragnizational.room.name
                    );
                    $("#team_name_create").text(
                        data.oragnizational.team.name
                    );
                    $("#position_name_create").text(
                        data.oragnizational.position.name
                    );
                    $("#room_id_create").val(
                        data.oragnizational.room.id
                    );
                    $("#team_id_create").val(
                        data.oragnizational.team.id
                    );
                    $("#position_id_create").val(
                        data.oragnizational.position.id
                    );
                }

                if (amount > 0) {
                    label = "Họ tên";
                    for (i = 0; i < amount; i++) {
                        html +=
                            '<input id="idOrgan" hidden value="' +
                            idInChart[i] +
                            '" name="idOrgan_' +
                            (i + 1) +
                            '">';

                        html +=
                            '<div class="col-sm-12 d-flex justify-content-between row appendEdit">' +
                            '<p class="col-sm-4 form-label fw-bold" style="text-align:right;">' +
                            label +
                            "</p>" +
                            '<div class="text-left col-sm-8 form-label form-group"><select id="staff_' +
                            (i + 1) +
                            '" class="form-control" name="basic_info_id_' +
                            (i + 1) +
                            '">';
                        html += '<option selected value="">Chưa có</option>';
                        data.staff.forEach(function (value) {
                            if (staffInChart[i] == value.id) {
                                html +=
                                    '<option selected value="' +
                                    value.id +
                                    '"> Mã nv : ' +
                                    value.employee_code +
                                    " || Tên : " +
                                    value.full_name +
                                    "</option>";
                            } else {
                                html +=
                                    '<option value="' +
                                    value.id +
                                    '"> Mã nv : ' +
                                    value.employee_code +
                                    " || Tên : " +
                                    value.full_name +
                                    "</option>";
                            }
                        });

                        html += "</select></div></div>";
                    }
                    $('#amount').remove();
                    html +=
                        '<input id="amount" hidden value="' +
                        amount +
                        '" name="amount">';

                    $(".contentBodyModalCreate").append(html);
                }
            },
        });
        $("#btnSaveCreate").off("click");
        $("#btnSaveCreate").on("click", () => {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $("#tokenForm").val(),
                },
            });
            $.ajax({
                type: "POST",
                url: "/chart/tree/storeUpdate",
                data: $("#formCreateChart").serialize(),
                success: function (response) {
                    $("#myModalCreate").attr("style", "display: none;");
                    var container = $(".containerChart");
                    if (bodFlag) {
                        container.find(".bodContainer").empty();
                        container.find(".depts").empty();
                        container.find(".lineContainer").empty();
                        container.find("svg").empty();
                        container.find(".dotFly").empty();
                        $.ajax({
                            type: "GET",
                            url: "/chart/chartBasic",
                            success: function (data) {
                                rooms = data;
                            },
                        }).done(function () {
                            var posY = 185;
                            drawChart1(
                                rooms,
                                container.position().left + 500,
                                posY,
                                container
                            );
                            $(".dept").on("click", () => {
                                $(window.parent.$.find("#btnBack")).show();
                            });
                            actionEdit();
                        });
                    } else {
                        $(".chart_heading").hide();
                        var svg = container.find("svg").html();
                        container.empty();
                        container.append(
                            '<div class="lineContainer"> ' + svg + "</div>"
                        );
                        $.ajax({
                            type: "GET",
                            url: "/chart/getChartRoom/" + room_id,
                            success: function (data) {
                                dataRoom = data;
                            },
                        }).done(function () {
                            var posY = 185;
                            drawChart2(
                                dataRoom,
                                container.position().left + 500,
                                posY,
                                container
                            );
                            actionEdit();
                        });
                    }
                },
                error: function (error) {
                    handleFails(error, '#formCreateChart');
                },
            });
        });
    });
}

function handleFails(response, el) {
    $(
        "#selectPerson, #selectRoom, #selectTeam, #selectPosition, #assign"
    ).removeClass("is-invalid");
    if (typeof response.responseJSON.errors != "undefined") {
        var keys = Object.keys(response.responseJSON.errors);
        $(el).find(".has-error").find(".help-block").remove();
        $(el).find(".has-error").removeClass("has-error");

        for (var i = 0; i < keys.length; i++) {
            // Escape dot that comes with error in array fields
            var key = keys[i].replace(".", "\\.");
            var formarray = keys[i];

            // If the response has form array
            if (formarray.indexOf(".") > 0) {
                var array = formarray.split(".");
                response.responseJSON.errors[keys[i]] =
                    response.responseJSON.errors[keys[i]];
                key = array[0] + "[" + array[1] + "]";
            }

            var ele = $(el).find("[name='" + key + "']");

            ele.addClass("is-invalid");

            var grp = ele.closest(".form-group");
            $(grp).find(".help-block").remove();

            //check if wysihtml5 editor exist
            var wys = $(grp).find(".note-editor note-frame card").length;

            if (wys > 0) {
                var helpBlockContainer = $(grp);
            } else {
                var helpBlockContainer = $(grp).find("div:first");
            }
            if ($(ele).is(":radio")) {
                helpBlockContainer = $(grp);
            }

            if (helpBlockContainer.length == 0) {
                helpBlockContainer = $(grp);
            }

            helpBlockContainer.append(
                '<div class="ml-2 help-block text-danger">' +
                response.responseJSON.errors[keys[i]] +
                "</div>"
            );
            $(grp).addClass("has-error");
        }

        if (keys.length > 0) {
            var element = $("[name='" + keys[0] + "']");
            if (element.length > 0) {
                $("html, body").animate(
                    {
                        scrollTop: element.offset().top - 150,
                    },
                    200
                );
            }
        }
    }
}
