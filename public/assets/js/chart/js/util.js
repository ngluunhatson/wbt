function connectPoint(container, p1, p2, id) {
    var line = $("#svg" + id);

    if (line.length > 0) {
        line.find("line").attr({
            "stroke": "#D4DDEF",
            "stroke-width": "5",
            "x1": p1.x, "y1": p1.y,
            "x2": p2.x, "y2": p2.y
        });
        return;
    }

    var line = $("<svg><line/></svg>");
    line.attr({
        "width": "100%",
        "height": "100%",
        "class": "line",
        "id": "svg" + id
    });
    line.find("line").attr({
        "stroke": "#D4DDEF",
        "stroke-width": "5",
        "x1": p1.x, "y1": p1.y,
        "x2": p2.x, "y2": p2.y
    });
    $(container).append(line);

};
function spawnNode(id, room_id = null, team_id = null, position_id = null, amount = null, rank = null) {
    var node = $('<div class="node draggable droppable"></div>');
    node.attr("id", "node" + id);
    node.data("room_id", room_id);
    node.data("team_id", team_id);
    node.data("position_id", position_id);
    node.data("amount", amount);
    node.data("rank", rank);
    // $(container).append(node);
    return node;
};
function spawnCard(id, data) {
    var card = $(`<div class="ui-widget-content cardChart border-level${data.level} droppable"></div>`);
    card.attr("id", "card" + id);
    card.on('click', function () {
        $.ajax({
            type: 'GET',
            url: "/chart/getPerson/" + data.basic_info_id,
            success: function (data) {
                $('#myModal').attr('style', 'display: block;')
                html = `<div class="modal-contentChart">
                <span class="close">&times;</span>
                <div class="col-12">
                    <div class="card m-0">
                    <div class="card-header d-flex justify-content-center">
                        <img class="img-thumbnail rounded-circle avatar-xxl" alt="200x200" src="${data.avatar ?? "/assets/images/users/avatar-3.jpg"}">
                    </div>
                    <div class="card-body py-4">
                        <div class="row gy-2">
                        <div class="col-sm-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Phòng</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${data.organizational.room.name}</p>
                            </div>
                        </div>
                        <div class="col-sm-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Bộ phận</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${data.organizational.team.name}</p>
                            </div>
                        </div>`;
                if (data.rank == 2 || data.rank == 3) {
                    html += `<div class="col-sm-12 d-flex justify-content-between row">
                                        <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Chức danh</p>
                                        <div class="text-left col-sm-8">`;
                    if (data.organizational.position.name.split('CHUYÊN VIÊN / NHÂN VIÊN').length > 1) {
                        html += `<p class="form-label">${listRank[data.rank].toUpperCase()} ${data.organizational.position.name.split('CHUYÊN VIÊN / NHÂN VIÊN')[1]}</p>`
                    } else {
                        html += `<p class="form-label">${data.organizational.position.name}</p>`
                    }
                    html += `</div>
                                    </div>`
                } else {
                    html += `<div class="col-sm-12 d-flex justify-content-between row">
                                        <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Chức danh</p>
                                        <div class="text-left col-sm-8" style="text-transform: uppercase;">
                                        <p class="form-label">${data.organizational.position.name}</p>
                                        </div>
                                    </div>`
                }
                html += `
                        <div class="col-sm-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Cấp bậc</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${listRank[data.rank].toUpperCase()}</p>
                            </div>
                        </div>
                        <div class="col-sm-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Họ tên</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${data.full_name}</p>
                            </div>
                        </div>
                        <div class="col-sm-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Tên tiếng Anh</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${data.english_name}</p>
                            </div>
                        </div>
                        <div class="col-sm-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Mã nhân viên</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${data.employee_code}</p>
                            </div>
                        </div>
                        <div class="col-sm-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Ngày vào làm</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${data.date_of_entry_to_work}</p>
                            </div>
                        </div>
                        <div class="col-sm-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Giới tính</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${data.gender ? 'Nam' : 'Nữ'}</p>
                            </div>
                        </div>
                        <div class="col-sm-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Quốc tịch</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${listNation[data.nationality]}</p>
                            </div>
                        </div>
                        <div class="col-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Số điện thoại</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${data.phone}</p>
                            </div>
                        </div>
                        <div class="col-sm-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Email cá nhân</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${data.personal_email}</p>
                            </div>
                        </div>
                        <div class="col-sm-12 d-flex justify-content-between row">
                            <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Email công ty</p>
                            <div class="text-left col-sm-8">
                            <p class="form-label">${data.company_email}</p>
                            </div>
                        </div>`;
                if (data.disc != null) {
                    html += `<div class="col-sm-12 d-flex justify-content-between row">
                        <p class="col-sm-4 form-label fw-bold" style="text-align:right;">DISC</p>
                        <div class="text-left col-sm-8">
                        <a download="disc" href="${data.disc}" class="form-label">${data.disc.split('/')[5]}</a>
                        </div>
                    </div>`;
                }
                if (data.motivators != null) {
                    html += `<div class="col-sm-12 d-flex justify-content-between row">
                        <p class="col-sm-4 form-label fw-bold" style="text-align:right;">Motivator</p>
                        <div class="text-left col-sm-8">
                        <a download="motivator" href="${data.motivators}" class="form-label">${data.motivators.split('/')[5]}</a>
                        </div>
                    </div>`
                }

                html += `</div>
                            </div>
                            </div>
                        </div>
                    </div>`;
                $('#myModal').html(html)
            },
        });
    })

    // $(container).append(card);
    return card;
};
function spawnDot(container, pos, id) {
    var dotEl = $(container).find("#dot" + id);

    if (dotEl.length > 0) {
        var x = pos.x - 5;
        var y = pos.y - 5;
        $(dotEl[0]).css({
            "left": (x + "px"),
            "top": (y + "px")
        });
        return dotEl;
    }
    if (dotEl.length === 0)
        dotEl = $("<div></div>");
    dotEl.attr({
        "id": ("dot" + id)
    });
    dotEl.addClass("dotFly");
    var x = pos.x - 5;
    var y = pos.y - 6;
    dotEl.css({
        "left": (x + "px"),
        "top": (y + "px")
    });
    $(container).append(dotEl);
};
function setPosition(target, pos) {
    if (target === undefined)
        return;
    $(target).css({
        "left": pos.x,
        "top": pos.y
    });
};
function getDot(dotType, object) {
    var pos = $(object).position();
    var initPos = $(object).node("initPos");
    if (initPos && initPos.x && initPos.y)
        pos = { top: initPos.y, left: initPos.x };
    var w = $(object).width();
    var h = $(object).height();

    if (dotType === "top") {
        var x = pos.left + w / 2;
        var y = pos.top;
        return { "x": x, "y": y };
    }
    if (dotType === "bottom") {
        var x = pos.left + w / 2;
        var y = pos.top + h;
        return { "x": x, "y": y };
    }
    if (dotType === "left") {
        // console.log(pos);
        var x = pos.left;
        var y = pos.top + h / 2;
        return { "x": x, "y": y };
    }
    if (dotType === "right") {
        var x = pos.left + w;
        var y = pos.top + h / 2;
        return { "x": x, "y": y };
    }
};
function getMidDot(dot1, dot2) {
    var x, y;
    if (dot2.x > dot1.x)
        x = dot1.x + (dot2.x - dot1.x) / 2;
    else
        x = dot2.x + (dot1.x - dot2.x) / 2;

    if (dot2.y > dot1.y)
        y = dot1.y + (dot2.y - dot1.y) / 2;
    else
        y = dot2.y + (dot1.y - dot2.y) / 2;
    // console.log(dot2);
    // console.log(y);
    return { "x": x, "y": y };
};
function connectNode(node1, node2, id, dotType1, dotType2, direct = false) {
    var type1 = "bottomDot";
    var type2 = "topDot";


    if (dotType1 !== undefined)
        type1 = dotType1;
    if (dotType2 !== undefined)
        type2 = dotType2;

    var dot1 = node1.node(type1);
    var dot2 = node2.node(type2);
    // console.log(type2);
    // console.log(dot1, dot2);
    // console.log(dot1, dot2);
    // if(dotType1 === "bottomDot")
    //     node1.node("showDot", "bottom");
    // if(dotType1 === "topDot")
    //     node1.node("showDot", "top");
    // if(dotType1 === "leftDot")
    //     node1.node("showDot", "left");
    // if(dotType1 === "rightDot")
    //     node1.node("showDot", "right");

    // if(dotType2 === "bottomDot")
    //     node2.node("showDot", "bottom");
    // if(dotType2 === "topDot")
    //     node2.node("showDot", "top");
    // if(dotType2 === "leftDot")
    //     node2.node("showDot", "left");
    // if(dotType2 === "rightDot")
    //     node2.node("showDot", "right");

    // node2.node("showDot", "top");
    if (parseInt(dot1.x) === parseInt(dot2.x) || direct === true) {
        connectPoint($(".lineContainer"), dot1, dot2, id + "1");
    }
    else {
        // console.log(dot1, dot2);
        var midDot = getMidDot(dot1, dot2);
        // console.log("ok:" , midDot);
        var midDot1 = { x: dot1.x, y: midDot.y };
        var midDot2 = { x: dot2.x, y: midDot.y };
        // console.log(dot1, midDot1);
        // connectPoint($(".lineContainer"), dot1, midDot1);

        if (type2 !== "leftDot" && type2 !== "rightDot")
            spawnDot($(".containerChart"), midDot2, id + '2');
        else {
            midDot1.y = dot2.y;
            midDot2.y = dot2.y;
        }
        // console.log("midDot1=", midDot1);
        spawnDot($(".containerChart"), midDot1, id + '1');
        connectPoint($(".lineContainer"), dot1, midDot1, id + "1");
        connectPoint($(".lineContainer"), midDot1, midDot2, id + "2");
        if (type2 !== "leftDot" && type2 !== "rightDot")
            connectPoint($(".lineContainer"), midDot2, dot2, id + "3");
    }

};
function log(...data) {
    console.log(data);
}