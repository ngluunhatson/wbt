
// --------------------------------------------------------------------
// Vẽ sơ đồ BAN GIÁM ĐỐC và các phòng ban
// --------------------------------------------------------------------
var farestY = 0;
function drawChart1(nodeData, posX, posY, container) {
    if (nodeData === undefined || nodeData.length === undefined || nodeData.length === 0)
        return;
    $(container).find(".bodContainer").show();

    // ------------------------------------------------------------
    // Tổng giám đốc, Trợ lý
    var gmCards = [];
    var assistantCards = [];
    var nodeGm, nodeAssistant;
    var posGmData = {}, posAssistantData = {};
    nodeData.sort((a, b) => a.room_order - b.room_order);
    // console.log(nodeData);
    nodeData.forEach(deptItem => {
        if (deptItem.bod === true) {
            // console.log(deptItem);
            if (deptItem.teams && deptItem.teams.length > 0) {
                deptItem.teams.forEach(posItem => {
                    posItem.positions.forEach(item => {
                        // ---------------------------------------------------------
                        // Tổng giám đốc
                        if (item.id === 9999) {
                            if (item.employees && item.employees.length > 0) {
                                item.employees.forEach(empItem => {
                                    // console.log(empItem);
                                    gmCards.push(spawnCard(empItem.id, empItem).card({
                                        title: empItem.staff.full_name,
                                        code: empItem.staff.employee_code,
                                        position: item.name,
                                        avatar: empItem.staff.avatar,
                                        data: empItem,
                                        level: posItem.level,
                                    }));
                                });
                            }
                            posGmData.position = item.name;
                            posGmData.amount = item.amount;
                            posGmData.rank = item.rank;
                            posGmData.year = item.year;
                            posGmData.id = item.id;
                            posGmData.dataNode = item;
                            posGmData.room_id = 8;
                            posGmData.team_id = item.team_id
                        }

                        // ---------------------------------------------------------
                        // Trợ lý
                        if (item.id === 9998) {
                            if (item.employees && item.employees.length > 0) {
                                item.employees.forEach(empItem => {
                                    assistantCards.push(spawnCard(empItem.id, empItem).card({
                                        title: empItem.staff.full_name,
                                        code: empItem.staff.employee_code,
                                        position: item.name,
                                        avatar: empItem.staff.avatar,
                                        position: item.name,
                                        data: empItem,

                                    }));
                                });
                            }
                            posAssistantData.position = item.name;
                            posAssistantData.amount = item.amount;
                            posAssistantData.rank = item.rank;
                            posAssistantData.year = item.year;
                            posAssistantData.id = item.id;
                            posAssistantData.dataNode = item;
                            posAssistantData.room_id = 8;
                            posAssistantData.team_id = item.team_id
                        }
                    })
                });
            }
        }
    });
    // console.log(gmCards, posGmData);
    posGmData.cards = gmCards;
    posGmData.bod = true;
    // posGmData.nodeClick = onNodeClick;
    nodeGm = spawnNode(posGmData.id, posGmData.room_id, posGmData.team_id, posGmData.id, posGmData.amount, posGmData.rank).node(posGmData);
    $(container).find(".bodContainer").append(nodeGm);
    setPosition($(nodeGm), { x: posX, y: posY });
    nodeGm.node("initPos", { x: posX, y: posY });

    posAssistantData.cards = assistantCards;
    posAssistantData.bod = true;
    // posAssistantData.nodeClick = onNodeClick;
    posAssistantData.parentNode = nodeGm;
    nodeAssistant = spawnNode(posAssistantData.id, posAssistantData.room_id
        , posAssistantData.team_id, posAssistantData.id, posAssistantData.amount, posAssistantData.rank).node(posAssistantData);
    $(container).find(".bodContainer").append(nodeAssistant);
    setPosition($(nodeAssistant), { x: posX + 400, y: posY + 250 });
    nodeAssistant.node("initPos", { x: posX + 400, y: posY + 250 });

    nodeGm.node("childNodes").push(nodeAssistant);

    // ---------------------------------------------------------
    // COO
    var nodeCOO = spawnNode("coo").node({
        position: "COO",
        cards: [],
        bod: true,
        parentNode: nodeGm
    });
    $(container).find(".bodContainer").append(nodeCOO);

    setPosition($(nodeCOO), { x: posX, y: (posY + 400) });
    nodeCOO.node("initPos", { x: posX, y: (posY + 400) });
    nodeGm.node("childNodes").push(nodeCOO);

    // Update position of assistant node again
    var midDot = getMidDot(nodeGm.node("bottomDot"), nodeCOO.node("topDot"));
    // console.log(midDot);
    midDot.y -= nodeAssistant.height() / 2;
    setPosition($(nodeAssistant), { x: posX + 400, y: midDot.y });
    nodeAssistant.node("initPos", { x: posX + 400, y: midDot.y });
    connectNode(nodeGm, nodeAssistant, "connect_" + posGmData.id + "_" + posAssistantData.id,
        "bottomDot", "leftDot");
    connectNode(nodeGm, nodeCOO, "connect_" + posGmData.id + "_COO",
        "bottomDot", "topDot", true);

    // ---------------------------------------------------------
    // Vẽ các phòng ban
    drawDepts(nodeData, posX, posY, nodeCOO, container);
};

// --------------------------------------------------------------------
// Vẽ sơ đồ các phòng ban
// --------------------------------------------------------------------
function drawDepts(nodeData, posX, posY, nodeCOO, container) {
    if (nodeData === undefined || nodeData.length === undefined || nodeData.length === 0)
        return;

    $(container).find(".depts").show();
    var baseX = posX - 250;
    var count = 0;
    nodeData.forEach(deptItem => {

        if (deptItem.bod === false) {
            var deptData = {};
            deptData.position = "P. " + deptItem.name;
            deptData.id = deptItem.id;
            deptData.bod = false;
            deptData.dept = true;
            deptData.dataNode = deptItem;
            deptData.parentNode = nodeCOO;
            deptData.headerClick = function (data, el) {
                // console.log(data);
                $('.chart_heading').hide();
                container.find(".bodContainer").hide();
                container.find(".depts").hide();
                container.find(".lineContainer").empty();
                container.find("svg").hide();
                container.find(".dotFly").hide();
                var dataRoom;
                $.ajax({
                    type: 'GET',
                    url: '/chart/getChartRoom/' + data.id,
                    success: function (data) {
                        dataRoom = data
                    },
                }).done(function () {
                    if (dataRoom.length > 0) {
                        drawChart2(dataRoom, posX, posY, container);
                        actionEdit();
                    } else {
                        $('.chart_tree').append('<h1 class="chart_title">Chưa có dữ liệu</h1>')
                    }
                })
            };
            // var cards = [];
            var nodeDept = spawnNode(deptItem.id).node(deptData);
            $(container).find(".depts").append(nodeDept);
            setPosition($(nodeDept), { x: baseX + count * 250, y: posY + 600 });
            nodeDept.node("initPos", { x: baseX + count * 250, y: posY + 600 });
            nodeCOO.node("childNodes").push(nodeDept);

            connectNode(nodeCOO, nodeDept, "connect_coo_" + deptData.id, "bottomDot", "topDot");
            count++;
        }
    });
};

function drawChart2(nodeData, posX, posY, container) {
    // console.log(nodeData);
    if (nodeData === undefined || nodeData.length === 0)
        return;

    var deptTitle = $('<div class="deptTitle"><span>SƠ ĐỒ</span> TỔ CHỨC<div></div></div>');
    $(deptTitle).find("div").text(nodeData[0].team.room.name);
    container.append(deptTitle);

    var totalSpace1 = 0;
    if (nodeData[0].children && nodeData[0].children.length > 0)
        totalSpace1 = getTotalSpaceLevel1(nodeData[0].children);
    else
        totalSpace1 = container.width();
    var x = container.position().left + totalSpace1 / 2 - 100;
    setPosition(deptTitle, { x: x - 70, y: 0 });

    // -------------------------------------------------------------
    // Set zoom detail chart
    var containerW = pageContent.width();
    var zoomRatio = containerW * 100 / totalSpace1;
    zoomRatio -= 4;
    if(totalSpace1 <= containerW)
        zoomRatio = 100;
    // console.log('zoomRatio', zoomRatio);
    $('html').css('zoom', zoomRatio + "%");
    // -------------------------------------------------------------
    
    // Draw level 0
    drawChildPos(nodeData, x, posY, undefined, undefined, container);
    $("svg").attr("width", (container.position().left + totalSpace1 + 10) + "px");
    $("svg").attr("height", (farestY + 200) + "px");
    // console.log(farestY);
};
function getTotalSpaceLevel1(nodeData) {
    var totalSpace1 = 0;
    nodeData.forEach(posItem => {
        if (posItem.level === 1) {
            var space = calcSpaceLevel1(posItem.children);
            totalSpace1 += space;
        }
    });
    return totalSpace1;
};
function drawChildPos(nodeData, posX, posY, parentNodeUI, parentNodeId, container) {
    if (nodeData === undefined || nodeData.length === undefined || nodeData.length === 0)
        return;

    // var container = $(".container");
    var count = 0;
    // console.log(nodeData);
    var lastItemX = 0;
    var totalSpace1 = 0, totalSpace2 = 0;
    nodeData.forEach(posItem => {
        if (posItem.level === 1) {
            var space = calcSpaceLevel1(posItem.children);
            totalSpace1 += space;
        }
        if (posItem.level === 2) {
            var space = calcSpaceLevel2(posItem.children);
            totalSpace2 += space;
        }
    });

    // --------------------------------------------------------------
    // Sắp xếp tăng dần theo position_order
    nodeData.sort((a, b) => a.position_order - b.position_order);
    // --------------------------------------------------------------

    // console.log("totalSpace1=", totalSpace1);
    // console.log("totalSpace2=", totalSpace2);
    var preNodeDataOdd = undefined, preNodeUIOdd = undefined;
    var preNodeDataEven = undefined, preNodeUIEven = undefined;
    nodeData.forEach(posItem => {
        var posData = {};
        var posCards = [];
        posData.position = posItem.name;
        posData.team_name = posItem.team.name;
        posData.id = posItem.id;
        posData.bod = false;
        posData.dept = false;
        posData.level = posItem.level;
        // ------------------------------------------------------------
        // Tạo card hiển thị cho các nhân viên
        if (posItem.employees && posItem.employees.length > 0) {
            posItem.employees.forEach(empItem => {
                empItem.level = posItem.level;
                var tempData = {
                    position: posItem.name,
                    department: posItem.team.name,
                    data: empItem,
                    level: posData.level
                };
                if (empItem.staff) {
                    tempData.title = empItem.staff.full_name;
                    tempData.avatar = empItem.staff.avatar;
                    tempData.code = empItem.staff.employee_code;
                }
                if (posItem.employees.length > 1)
                    tempData.showPos = false;

                posCards.push(spawnCard(empItem.id, empItem).card(tempData));
            });
        }

        // ------------------------------------------------------------
        // Tạo node và thêm danh sách card vào node
        // Sau đó hiển thị node lên giao diện
        posData.cards = posCards;
        posData.amount = posItem.amount;
        posData.year = posItem.year;
        posData.dataNode = posItem;
        posData.nodeClick = onNodeClick;
        posData.headerClick = onHeaderClick;
        posData.parentNode = parentNodeUI;
        var nodePos = spawnNode(posData.id, nodeData[0].team.room.id, posItem.team_id, posItem.id, posItem.amount, posItem.rank).node(posData);
        container.append(nodePos);
        // ------------------------------------------------------------

        var x = posX + count * 250;
        var y = posY;

        // ------------------------------------------------------------
        // Vẽ level 1
        if (posItem.level === 1) {
            var space = calcSpaceLevel1(posItem.children);
            if (lastItemX === 0) {
                lastItemX = posX - totalSpace1 / 2;
            } else {
                // nothing :D
            }

            x = lastItemX + space / 2;
            lastItemX += space;
        }

        // ------------------------------------------------------------
        // Vẽ level 2
        if (posItem.level === 2) {
            var space = calcSpaceLevel2(posItem.children);
            if (lastItemX === 0) {
                lastItemX = posX - totalSpace2 / 2;
            } else {
                // nothing :D :D
            }

            x = lastItemX + space / 2;
            lastItemX += space;
        }

        // ------------------------------------------------------------
        // Vẽ level 3
        if (posItem.level >= 3) {
            if (nodeData.length === 1)
                x = posX;
            else {
                if (posItem.position_order % 2 == 1)
                    x = posX - 125;
                else
                    x = posX + 125;

                // ------------------------------------------------------------
                // Tính giá trị dựa theo thứ tự của node: position_order
                y += (Math.round(posItem.position_order / 2) - 1) * 150;
                
                // Nếu node trước đó cùng phía tồn tại, 
                // -> thì sẽ dựa vào vị trí node đó mà vẽ node hiện tại
                if (posItem.position_order % 2 == 1
                    && preNodeUIOdd) {
                        var oriPos = preNodeUIOdd.node("initPos");
                        y = oriPos.y + preNodeUIOdd.height() + 50;
                }
                else if (posItem.position_order % 2 == 0
                    && preNodeUIEven) {
                        var oriPos = preNodeUIEven.node("initPos");
                        y = oriPos.y + preNodeUIEven.height() + 50;
                }
            }
        }
        setPosition($(nodePos), { x: x, y: y });
        nodePos.node("initPos", { x: x, y: y });
        if (parentNodeUI) {
            parentNodeUI.node("childNodes").push(nodePos);
        }

        if (farestY < y)
            farestY = y;

        // -------------------------------------------------------------------------------------
        // Vẽ các đối tượng SVG (các đường line) kết nối node cha và node hiện tại lại với nhau
        if (parentNodeUI) {
            var connectId = "connect_" + parentNodeId + "_" + posData.id;

            // ------------------------------------------------------------
            // Với các node level 3 trở đi là cách vẽ đối xứng
            if (posData.level >= 3) {
                if (nodeData.length === 1)
                    connectNode(parentNodeUI, nodePos, connectId, "bottomDot", "topDot", true);
                else if (posItem.position_order % 2 == 0)
                    connectNode(parentNodeUI, nodePos, connectId, "bottomDot", "leftDot");
                else
                    connectNode(parentNodeUI, nodePos, connectId, "bottomDot", "rightDot");
            }
            // ------------------------------------------------------------
            // Các node level còn lại
            else
                connectNode(parentNodeUI, nodePos, connectId, "bottomDot", "topDot");
        }

        // ------------------------------------------------------------
        // Tiếp tục vẽ các node con của node hiện tại
        if (posItem.employees && posItem.employees.length > 0 && posItem.employees.length < 2)
            drawChildPos(posItem.children, x, y + (posItem.employees.length) * 250 + 150, nodePos, posData.id, container);
        else if (posItem.employees.length >= 2) {

            var childNodes = $(nodePos).node("childNodes");
            childNodes.forEach(item => {
                clearChildren(item);
            });
            $(nodePos).node("clearChildNodes");
            $(nodePos).find('.content').hide();
            if ($(nodePos).node("isCollapse")) {
                drawChildPos(posItem.children, $(nodePos).position().left, $(nodePos).position().top + 150, nodePos, posItem.id, $('.containerChart'));
            }
        }
        else
            drawChildPos(posItem.children, x, y + 150, nodePos, posData.id, container);

        count++;

        // ------------------------------------------------------------
        // Lưu vết các node dùng cho node kế tiếp
        if (posItem.position_order % 2 == 1) {
            preNodeDataOdd = posItem;
            preNodeUIOdd = nodePos;
        }
        else {
            preNodeDataEven = posItem;
            preNodeUIEven = nodePos;
        }

    });
};

// ------------------------------------------------------------
// Tính toán độ rộng của toàn chart
// ------------------------------------------------------------
function calcSpaceLevel1(nodeDataLevel2) {
    var space = 0;
    var existLv2 = false;
    nodeDataLevel2.forEach(itemLevel2 => {
        if(itemLevel2.level == 2) {
            existLv2 = true;
            if (itemLevel2.children && itemLevel2.children.length > 1)
                space += 500;
            else
                space += 250;
        }
    });
    if(!existLv2) {
        space = calcSpaceLevel2(nodeDataLevel2);
    }
    if (space === 0)
        space = 250;
    return space;
};

// ------------------------------------------------------------
// Tính toán độ rộng của khối node level 2
// ------------------------------------------------------------
function calcSpaceLevel2(nodeDataLevel3) {
    var space = 0;
    if (nodeDataLevel3 && nodeDataLevel3.length && nodeDataLevel3.length > 1)
        space += 500;
    else
        space += 250;
    if (space === 0)
        space = 250;
    return space;
    // return 250;
};

// ------------------------------------------------------------
// Xóa các node con của node chỉ định
// ------------------------------------------------------------
function clearChildren(nodeUI) {
    if (nodeUI === undefined)
        return;

    var childNodes = nodeUI.node("childNodes");
    if (childNodes && childNodes.length)
        childNodes.forEach(item => {
            clearChildren(item);
        });
    $(nodeUI).remove();
}