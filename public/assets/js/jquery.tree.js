(function ($) {
    "use strict";
    var choicesExamples = document.querySelectorAll("[data-choices]");
    choicesExamples.forEach(function (item) {
        var choiceData = {};
        var isChoicesVal = item.attributes;
        if (isChoicesVal["data-choices-groups"]) {
            choiceData.placeholderValue = "This is a placeholder set in the config";
        }
        if (isChoicesVal["data-choices-search-false"]) {
            choiceData.searchEnabled = false;
        }
        if (isChoicesVal["data-choices-search-true"]) {
            choiceData.searchEnabled = true;
        }
        if (isChoicesVal["data-choices-removeItem"]) {
            choiceData.removeItemButton = true;
        }
        if (isChoicesVal["data-choices-sorting-false"]) {
            choiceData.shouldSort = false;
        }
        if (isChoicesVal["data-choices-sorting-true"]) {
            choiceData.shouldSort = true;
        }
        if (isChoicesVal["data-choices-multiple-remove"]) {
            choiceData.removeItemButton = true;
        }
        if (isChoicesVal["data-choices-limit"]) {
            choiceData.maxItemCount = isChoicesVal["data-choices-limit"].value.toString();
        }
        if (isChoicesVal["data-choices-limit"]) {
            choiceData.maxItemCount = isChoicesVal["data-choices-limit"].value.toString();
        }
        if (isChoicesVal["data-choices-editItem-true"]) {
            choiceData.maxItemCount = true;
        }
        if (isChoicesVal["data-choices-editItem-false"]) {
            choiceData.maxItemCount = false;
        }
        if (isChoicesVal["data-choices-text-unique-true"]) {
            choiceData.duplicateItemsAllowed = false;
        }
        if (isChoicesVal["data-choices-text-disabled-true"]) {
            choiceData.addItems = false;
        }
        isChoicesVal["data-choices-text-disabled-true"] ? new Choices(item, choiceData).disable() : new Choices(item, choiceData);
    });
    $.fn.tree_structure = function (options) {
        var defaults = {
            'add_option': false,
            'edit_option': false,
            'delete_option': false,
            'confirm_before_delete': false,
            'fullwidth_option': false,
            'align_option': 'center',
            'draggable_option': false
        };
        return this.each(function () {
            if (options) {
                $.extend(defaults, options);
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Remove all event listeners on destroy plugin
            $.fn.tree_structure.destroy = function () {
                $(document).off('click', '.' + class_name + '[rel = ' + tree_id + '] span.add_action');
                $(document).off('click', '.' + class_name + '[rel = ' + tree_id + '] span.edit_action');
                $(document).off('click', '.' + class_name + '[rel = ' + tree_id + '] span.delete_action');
                $(document).off('click', '.' + class_name + '[rel = ' + tree_id + '] b.thide');
                $(document).off('mouseenter mouseleave', '.' + class_name + '[rel = ' + tree_id + '] li > div');
                $(document).off('click', '.' + class_name + '[rel = ' + tree_id + '] span.highlight');
                $(document).off('click', '.tree_view_popup .close');
                $(document).off('click', 'input.submit');
                $(document).off('click', 'img.close');
                $(document).off('click', 'input.edit');
            }

            // Options
            var add_option = defaults['add_option']; // Add a new branch to the tree by clicking on add icon. default value is false.
            var edit_option = defaults['edit_option']; // Update particular branch by clicking on edit icon. default value is false.
            var delete_option = defaults['delete_option']; // Delete particular branch with all child branches by clicking on delete icon. default value is false.
            var confirm_before_delete = defaults['confirm_before_delete']; // Confirm before delete any branch. default value is false.
            var fullwidth_option = defaults['fullwidth_option']; // If this is true then tree structure gets full width. if width is bigger than browser width then tree structure set with a horizontal scroll. default value is false.
            var align_option = defaults['align_option']; // Set the alignment of a tree structure with this option. options are left, right and center. default value is 'center'.
            var draggable_option = defaults['draggable_option']; // Perform a drag-and-drop operation with tree branches. Drag the tree branch which we want to move and then drop the dragged branch on the destination branch. The dragged branch will now bound to the destination branch. default value is false.

            // Common variables
            var vertical_line_text = '<span class="vertical"></span>';
            var horizontal_line_text = '<span class="horizontal"></span>';
            var add_action_text = add_option == true ? '<span class="add_action" title="Add"></span>' : '';
            var edit_action_text = edit_option == true ? '<span class="edit_action" title="Update"></span>' : '';
            var delete_action_text = delete_option == true ? '<span class="delete_action" title="Delete"></span>' : '';
            var highlight_text = '<span class="highlight" title="Highlight"></span>';
            var class_name = $(this).attr('class');
            var tree_id = $(this).attr('rel');
            var event_name = 'pageload';

            if (align_option != 'center') {
                $('.' + class_name + '[rel = ' + tree_id + '] li').css({ 'text-align': align_option });
            }

            // If fullwidth_option is true
            if (fullwidth_option) {
                var i = 0;
                var prev_width;
                var get_element;
                if ($('.' + class_name + '[rel = ' + tree_id + '] li li').length > 0) {
                    $('.' + class_name + '[rel = ' + tree_id + '] li li').each(function () {
                        var this_width = $(this).width();
                        if (i == 0 || this_width > prev_width) {
                            prev_width = $(this).width();
                            get_element = $(this);
                        }
                        i++;
                    });
                    var loop = get_element.closest('ul').children('li').eq(0).nextAll().length;
                    var fullwidth = parseInt(0);
                    for (var j = 0; j <= loop; j++) {
                        fullwidth += parseInt(get_element.closest('ul').children('li').eq(j).outerWidth(), 10);
                    }
                    $('.' + class_name + '[rel = ' + tree_id + ']').closest('div').width(fullwidth + 750);
                }
            }

            $('.' + class_name + '[rel = ' + tree_id + '] li.thide').each(function () {
                $(this).children('ul').hide();
            });

            // Update HTML structure to each branch by adding vertical and horizontal lines and action buttons
            function prepend_data(target) {
                target.prepend(vertical_line_text + horizontal_line_text).children('div').prepend(add_action_text + delete_action_text + edit_action_text);
                if (target.children('ul').length != 0) {
                    target.hasClass('thide') ? target.children('div').prepend('<b class="thide tshow"></b>') : target.children('div').prepend('<b class="thide"></b>');
                }
                target.children('div').prepend(highlight_text);
            }

            // Draw lines between each branch to show the relations
            function draw_line(target) {
                var tree_offset_left = $('.' + class_name + '[rel = ' + tree_id + ']').offset().left;
                tree_offset_left = parseInt(tree_offset_left, 10);
                var child_width = target.children('div').outerWidth(true) / 2;
                var child_left = target.children('div').offset().left;
                if (target.parents('li').offset() != null) {
                    var parent_child_height = target.parents('li').offset().top;
                }
                var vertical_height = (target.offset().top - parent_child_height) - target.parents('li').children('div').outerHeight(true) / 2;
                target.children('span.vertical').css({ 'height': vertical_height, 'margin-top': -vertical_height, 'margin-left': child_width, 'left': child_left - tree_offset_left });
                if (target.parents('li').offset() == null) {
                    var width = 0;
                } else {
                    var parents_width = target.parents('li').children('div').offset().left + (target.parents('li').children('div').width() / 2);
                    var current_width = child_left + (target.children('div').width() / 2);
                    var width = parents_width - current_width;
                }
                var horizontal_left_margin = width < 0 ? -Math.abs(width) + child_width : child_width;
                target.children('span.horizontal').css({ 'width': Math.abs(width), 'margin-top': -vertical_height, 'margin-left': horizontal_left_margin, 'left': child_left - tree_offset_left });
            }

            // Update tree structure
            function call_structure() {
                $('.' + class_name + '[rel = ' + tree_id + '] li').each(function () {
                    if (event_name == 'pageload') {
                        prepend_data($(this));
                    }
                    draw_line($(this));
                });
            }

            call_structure();

            event_name = 'others';

            $(window).resize(function () {
                call_structure();
            });

            // Extend and shrink all child branches by click on show/hide button
            $(document).on("click", '.' + class_name + '[rel = ' + tree_id + '] b.thide', function () {
                $(this).toggleClass('tshow');
                $(this).closest('li').toggleClass('thide').children('ul').toggle();
                call_structure();
            });

            // Change the branch color by its relation with parent and child branch on mouse movement
            $(document).on("mouseenter mouseleave", '.' + class_name + '[rel = ' + tree_id + '] li > div', function (event) {
                if (event.type == 'mouseenter' || event.type == 'mouseover') {
                    $('.' + class_name + '[rel = ' + tree_id + '] li > div.current').removeClass('current');
                    $('.' + class_name + '[rel = ' + tree_id + '] li > div.children').removeClass('children');
                    $('.' + class_name + '[rel = ' + tree_id + '] li > div.parent').removeClass('parent');
                    $(this).addClass('current');
                    $(this).closest('li').children('ul').children('li').children('div').addClass('children');
                    $(this).closest('li').closest('ul').closest('li').children('div').addClass('parent');
                    $(this).children('span.highlight, span.add_action, span.delete_action, span.edit_action').show();
                } else {
                    $(this).children('span.highlight, span.add_action, span.delete_action, span.edit_action').hide();
                }
            });

            // Display particular branch only with their parent and child branches
            $(document).on("click", '.' + class_name + '[rel = ' + tree_id + '] span.highlight', function () {
                if ($(this).closest("ul").attr("class") != "tree") {
                    $('.' + class_name + '[rel = ' + tree_id + '] li.highlight').removeClass('highlight');
                    $('.' + class_name + '[rel = ' + tree_id + '] li > div.parent').removeClass('parent');
                    $('.' + class_name + '[rel = ' + tree_id + '] li > div.children').removeClass('children');
                    $(this).closest('li').addClass('highlight');
                    $('.highlight li > div').addClass('children');
                    var _this = $(this).closest('li').closest('ul').closest('li');
                    find_parent(_this);

                    if (fullwidth_option) {
                        $('.' + class_name + '[rel = ' + tree_id + ']').parent('div').parent('div').scrollLeft(0);
                    }

                    $('.' + class_name + '[rel = ' + tree_id + '] li > div').not(".parent, .current, .children").closest('li').addClass('tnone');
                    $('.' + class_name + '[rel = ' + tree_id + '] li div b.thide.tshow').closest('div').closest('li').children('ul').addClass('tshow');
                    $('.' + class_name + '[rel = ' + tree_id + '] li div b.thide').addClass('tnone');
                    if ($('.back_btn').length == 0) {
                        $('.' + class_name + '[rel = ' + tree_id + ']').prepend('<img src="/assets/chartImg/back.png" class="back_btn" />');
                    }
                    call_structure();

                    $('.back_btn').click(function () {
                        $('.' + class_name + '[rel = ' + tree_id + '] ul.tshow').removeClass('tshow');
                        $('.' + class_name + '[rel = ' + tree_id + '] li.tnone').removeClass('tnone');
                        $('.' + class_name + '[rel = ' + tree_id + '] li div b.thide').removeClass('tnone');
                        $(this).remove();
                        call_structure();
                    });
                }
            });

            function find_parent(_this) {
                if (_this.length > 0) {
                    _this.children('div').addClass('parent');
                    _this = _this.closest('li').closest('ul').closest('li');
                    return find_parent(_this);
                }
            }

            // Display branch details in popup by click on view button
            if ($('.' + class_name + '[rel = ' + tree_id + '] .view_btn').length > 0) {
                $(document).on("click", '.' + class_name + '[rel = ' + tree_id + '] .view_btn', function () {
                    var view_ele_id = $(this).closest("div").data('id');;
                    var data = {
                        "_token": csrf,
                        action: 'viewdetail',
                        view_ele_id: view_ele_id,
                        tree_id: tree_id
                    };

                    $.ajax({
                        type: 'POST',
                        url: '/chart/tree/action',
                        data: data,
                        success: function (data) {
                            $("body").append(data);
                        }
                    });
                });

                $(document).on("click", '.tree_view_popup .close', function () {
                    $(this).closest(".tree_view_popup").remove();
                });
            }

            // Functionality to add new branch by click on add icon
            if (add_option) {
                $(document).on("click", '.' + class_name + '[rel = ' + tree_id + '] span.add_action', function () {
                    var _this = $(this);
                    _this.closest("div").find("span.add_action, span.edit_action, span.delete_action, span.highlight").hide();
                    if ($('form.add_data').length > 0) {
                        $('form.add_data').remove();
                    }
                    // if ($('form.edit_data').length > 0) {
                    //     $('form.edit_data').remove();
                    // }

                    var data = {
                        "_token": csrf,
                        action: 'addform',
                        tree_id: tree_id
                    };


                    $.ajax({
                        type: 'POST',
                        url: '/chart/tree/action',
                        data: data,
                        success: function (data) {
                            var addquery = data;
                            _this.parent('div').addClass('z-index').append("<section class='form_box'>" + addquery + "</section>");
                            $('#selectYear').datepicker({
                                format: 'yyyy',
                                startView: "years",
                                minViewMode: "years",
                            });

                            $('#selectStaff').on('change', function () {
                                var id = $('#selectStaff').find(":selected").val();
                                $.ajax({
                                    type: 'GET',
                                    url: '/chart/tree/getAvatar/' + id,
                                    success: function (data) {
                                        $('#avatarInsert').attr('src', data.avatar)
                                    }
                                });
                            })
                        }
                    });

                    $(document).on("click", "input.submit", function (event) {
                        var _addthis = $(this);
                        var ajax_add_id;
                        event.preventDefault();
                        var parent_id = _addthis.closest('div').data('id');
                        var data = new FormData(_addthis.closest('form')[0]);
                        var params = "action=add&parent_id=" + parent_id + "&tree_id=" + tree_id + "&room_id=" + $('#roomSelect').val();
                        _addthis.closest("li").before("<img src='/assets/chartImg/load.gif' class='load' />");
                        $.ajax({
                            type: 'POST',
                            url: '/chart/tree/add?' + params,
                            data: data,
                            enctype: 'multipart/form-data',
                            processData: false,
                            contentType: false,
                            cache: false,
                            success: function (data) {
                                $("img.load").remove();

                                if (data.status === false) {
                                    alert(obj.msg_text);
                                } else {
                                    var html = `<li>
                                                <span class='vertical'></span>
                                                <span class='horizontal'></span>
                                                <div data-id='${data.id}'>
                                                    <span class='highlight' title='Highlight'></span>
                                                    <span class='add_action' title='Add'></span>
                                                    <span class='delete_action' title='Delete'></span>
                                                    <span class='edit_action' title='Update'></span>
                                                    <div class='img'>
                                                        <img src='${(data.staff != null) > 0 ? data.staff.avatar : '/assets/chartImg/user.jpg'}'>
                                                    </div>
                                                    <h6 class='name'>${(data.staff != null) ? data.staff.full_name : 'Chưa có'}</h6> <!-- Tên -->
                                                    <span class='id'>${(data.staff != null) > 0 ? data.staff.employee_code : 'Chưa có'}</span> <!-- Mã nv -->
                                                    <h6 class='position'>${(data.position != null) > 0 ? data.position.name : 'Chưa có'}</h6> <!-- Chức danh -->
                                                    <span class='room'>${(data.team != null) > 0 ? data.team.name : 'Chưa có'}</span> <!-- Phòng -->
                                                    
                                                </div>
                                            </li>`;

                                    $(document).off("click", "input.submit");
                                    var elemntUL = $(`#parent[data-parentid="${parent_id}"]`);
                                    if (elemntUL.length > 0) {
                                        elemntUL.append(html);
                                    } else {
                                        _addthis.closest('div').after('<ul id="parent" data-parentid="' + parent_id + '">' + html + "</ul>")
                                    }
                                    _addthis.closest('form.add_data').find(".close").trigger("click");
                                    if (!edit_option) {
                                        element_target.find("#" + data.id).find(".edit_action").remove();
                                    }
                                    if (!delete_option) {
                                        element_target.find("#" + data.id).find(".delete_action").remove();
                                    }
                                    call_structure();
                                    if (draggable_option) {
                                        draggable_event();
                                    }
                                }
                            }
                        });
                    });

                    $(document).on("click", "#insertClose", function () {
                        $(document).off("click", "input.submit");
                        $(this).closest('.tree').find('.z-index').removeClass('z-index');
                        $(this).parent().parent().remove();
                    });
                });
            }

            // Functionality to update branch by click on edit icon
            if (edit_option) {
                $(document).on("click", '.' + class_name + '[rel = ' + tree_id + '] span.edit_action', function () {
                    var _this = $(this);

                    var edit_ele_id = _this.closest("div").data('id');;
                    var data = {
                        "_token": csrf,
                        action: 'editform',
                        edit_ele_id: edit_ele_id,
                        tree_id: tree_id
                    };

                    $.ajax({
                        type: 'GET',
                        url: '/chart/showFormEdit/' + edit_ele_id,
                        data: data,
                        success: function (data) {
                            var editquery = data;
                            _this.closest('.tree').find('.z-index').removeClass('z-index');
                            _this.closest('div').addClass('z-index');
                            $('#editMode').show();
                            $('#editId').val(data.oragnizational.id)
                            if (data.oragnizational.staff) {
                                $('#avatarEdit').attr('src', '/' + data.oragnizational.staff.avatar)
                                $('#title')
                                    .find('option')
                                    .remove()
                            }
                            $('#title').append($('<option>', {
                                value : '',
                                text: 'Vui lòng chọn nhân viên',
                                selected: true,
                            }));
                            data.staff.forEach(function (value) {
                                $('#title').append($('<option>', {
                                    value: value.id,
                                    text: 'Mã nv : ' + value.employee_code + ' || Tên : ' + value.full_name
                                }));
                            })
                            $("#title").find('option').removeAttr("selected");
                            $("#teamEdit").find('option').removeAttr("selected");
                            $("#positionEdit").find('option').removeAttr("selected");

                            $(`#title option[value="${(data.oragnizational.basic_info_id != null ? data.oragnizational.basic_info_id : '')}"]`).attr('selected', true)
                            $("#title").val(data.oragnizational.basic_info_id != null ? data.oragnizational.basic_info_id : '')
                            $(`#teamEdit option[value="${(data.oragnizational.team_id != null ? data.oragnizational.team_id : '')}"]`).attr('selected', true)
                            $("#teamEdit").val(data.oragnizational.team_id != null ? data.oragnizational.team_id : '')
                            $(`#positionEdit option[value="${(data.oragnizational.position_id != null ? data.oragnizational.position_id : '')}"]`).attr('selected', true)
                            $("#positionEdit").val(data.oragnizational.position_id != null ? data.oragnizational.position_id : '')
                            // if (data.hide) {
                            //     $('#hideEdit').prop('checked', true);
                            // } else {
                            //     $('#hideEdit').prop('checked', false);
                            // }

                            var roomID = Number.parseInt($('#roomSelect').find(":selected").val());
                            var teamID = data.oragnizational.team_id;
                            var positionID = data.oragnizational.position_id;
                            var staffID = data.oragnizational.basic_info_id ;

                            if (roomID != '') {
                                loadTeam(roomID, teamID)
                            }

                            if (teamID != '') {
                                loadPosition(teamID, positionID)
                            }

                            // if (roomID != '' && teamID != '' && positionID != '') {
                            //     loadStaff(roomID, teamID, positionID, staffID)
                            // }
                            
                            $('#datepicker').val(data.oragnizational.year)
                        }
                    });
                });
            }

            $('#editSubmit').on("click", function (event) {
                event.preventDefault();
                var _editthis = $(this);
                var params = $('#formEdit').serializeArray()
                var data = new FormData(_editthis.closest('form')[0]);
                _editthis.closest("li").before("<img src='/assets/chartImg/load.gif' class='load' />");
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '/chart/tree/edit/' + params[1]['value'],
                    data: data,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (response) {
                        $("img.load").remove();
                        $('#editMode').hide();
                        if (response.status === false) {
                            alert(obj.msg_text);
                        } else {
                            if (response.data.staff) {
                                $("div").find(`[data-id='${params[1]['value']}']`).find("#avatarView").attr("src", '/' + response.data.staff.avatar);
                            }
                            $("div").find(`[data-id='${params[1]['value']}']`).find('.name').text(response.data.staff != null ? response.data.staff.full_name : 'Chưa Có');
                            $("div").find(`[data-id='${params[1]['value']}']`).find('.employeeCode').text(response.data.staff != null ? response.data.staff.employee_code : 'Chưa Có');
                            $("div").find(`[data-id='${params[1]['value']}']`).find('.position').text(response.data.position != null ? response.data.position.name : 'Chưa Có');
                            $("div").find(`[data-id='${params[1]['value']}']`).find('.room').text(response.data.team != null ? response.data.team.name : 'Chưa Có');

                            call_structure();
                            if (draggable_option) {
                                draggable_event();
                            }
                        }
                    }
                });
            });

            $('#editClose').on("click", function () {
                $('#editMode').hide();
            });

            // Functionality to delete branch by click on delete icon
            if (delete_option) {
                $(document).on("click", '.' + class_name + '[rel = ' + tree_id + '] span.delete_action', function () {
                    var _deletethis = $(this);
                    if (_deletethis.closest('div').data('id') == 1) {
                        alert("You cant delete root person");
                    } else {
                        var target_element = _deletethis.closest('li').closest('ul').closest('li');
                        var confirm_message = 1;
                        if (confirm_before_delete) {
                            var confirm_text = _deletethis.closest('li').children('ul').length === 0 ? "Do you want to deleat this?" : "Do you want to deleat this with\nall child elements?";
                            confirm_message = confirm(confirm_text);
                        }
                        if (confirm_message) {
                            _deletethis.closest('li').addClass('ajax_delete_all');
                            var ajax_delete_id = Array();
                            ajax_delete_id.push(_deletethis.closest('div').data('id'));
                            $('.ajax_delete_all li').each(function () {
                                ajax_delete_id.push($(this).children('div').data('id'));
                            });
                            _deletethis.closest('li').removeClass('ajax_delete_all');
                            var data = {
                                "_token": csrf,
                                action: 'delete',
                                id: ajax_delete_id,
                                tree_id: tree_id
                            };
                            _deletethis.closest("li").before("<img src='/assets/chartImg/load.gif' class='load' />");

                            $.ajax({
                                type: 'POST',
                                url: '/chart/tree/action',
                                data: data,
                                success: function (data) {
                                    if (data) {
                                        $("img.load").remove();
                                        _deletethis.closest('li').fadeOut().remove();
                                        call_structure();
                                        if (target_element.children('ul').children('li').length == 0) {
                                            target_element.children('ul').remove();
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }

            // Drag and drop functionality
            function draggable_event() {
                droppable_event();
                $('.' + class_name + '[rel = ' + tree_id + '] li > div').draggable({
                    cursor: 'move',
                    distance: 40,
                    zIndex: 5,
                    revert: true,
                    revertDuration: 100,
                    snap: '.tree li div',
                    snapMode: 'inner',
                    start: function (event, ui) {
                        $('li.li_children').removeClass('li_children');
                        $(this).closest('li').addClass('li_children');
                    },
                    stop: function (event, ul) {
                        droppable_event();
                    }
                });
            }

            function droppable_event() {
                $('.' + class_name + '[rel = ' + tree_id + '] li > div').droppable({
                    accept: '.tree li div',
                    drop: function (event, ui) {
                        $('div.check_div').removeClass('check_div');
                        $('.li_children div').addClass('check_div');
                        var _this = $(this);
                        if (_this.hasClass('check_div')) {
                            alert('Cant Move on Child Element.');
                        } else {
                            var data = {
                                "_token": csrf,
                                action: 'drag',
                                id: $(ui.draggable[0]).data('id'),
                                parent_id: _this.data('id'),
                                tree_id: tree_id
                            };

                            $.ajax({
                                type: 'POST',
                                url: '/chart/tree/action',
                                data: data,
                                success: function (data) { }
                            });

                            if (_this.next('ul').length == 0) {
                                _this.after('<ul><li>' + $(ui.draggable[0]).attr({ 'style': '' }).closest('li').html() + '</li></ul>');
                            } else {
                                $(this).next('ul').append('<li>' + $(ui.draggable[0]).attr({ 'style': '' }).closest('li').html() + '</li>');
                            }

                            if ($(ui.draggable[0]).closest('ul').children('li').length == 1) {
                                $(ui.draggable[0]).closest('ul').remove();
                            } else {
                                $(ui.draggable[0]).closest('li').remove();
                            }
                            call_structure();
                            draggable_event();
                        }
                    }
                });
            }

            if (draggable_option) {
                draggable_event();
            }
        });
    };
})(jQuery);