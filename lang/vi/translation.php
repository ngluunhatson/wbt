<?php
return [
    "dayoff" => [
        'insert' => [
            'page_title' => 'Ngày nghỉ',
            'all_company' => 'Toàn bộ công ty',
            'room' => 'Phòng',
            'team' => 'Bộ phận',
            'position' => 'Chức danh',
            'staff' => 'Mã nhân viên | Họ và tên',
            'time_day_off' => 'Thời gian nghỉ',
            'start_date' => 'Ngày bắt đầu',
            'start_time' => 'Giờ bắt đầu',
            'end_date' => 'Ngày kết thúc',
            'end_time' => 'Giờ kết thúc',
            'total_date' => 'Tổng số ngày',
            'unit_date' => 'Ngày',
            'title' => 'Tiêu  đề',
            'note' => 'Ghi chú',
            'board_of_executive' => 'Ban giám đốc',
            'hr_addmin' => 'P. QT HC-NS',
        ],
    ],
];
?>
