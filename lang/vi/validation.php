<?php

return
    [
        'custom' => [
            'title.required' => 'Vui lòng nhập :attribute',
            'title.max' => 'Vui lòng nhập :attribute không quá :max ký tự',
            'note.required' => 'Vui lòng nhập :attribute',
            'note.max' => 'Vui lòng nhập :attribute không quá :max ký tự',
            'room_id.required' => 'Vui lòng chọn :attribute',
        ],

        'attributes' => [
            'title' => 'Tiêu đề',
            'note' => 'Ghi chú',
            'room_id' => 'Phòng ban'
        ],
    ];
