<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AssignPerson extends Notification
{
    use Queueable;

    private $data;
   
    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function via($notifiable)
    {
         return ['database'];
    }

    public function toDatabase($notifiable)
    {
        return [
           'data' => $this->data
        ];
    }
}
