<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\BasicInfo;
use App\Models\Timekeeping;
use Carbon\Carbon;

use Illuminate\Support\Facades\DB;


class MonthlyCreateTimeKeep extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timekeep:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create timekeeping every month and create payroll at the same time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $staffs = BasicInfo::with(['larborBbt'])->where('delete_flag',0)->get();
            $nextDay = Carbon::now()->addDay();
            $month = $nextDay->month;
            $year =  $nextDay->year;
            
            if ($month != 2) {
                foreach ($staffs as $staff) {
                    $isStaffOff = $staff->larborBbt->from_date_qdtv  && strtotime($staff->larborBbt->from_date_qdtv) <= strtotime($nextDay);
                    // dump("CALL generateTimeKeep($monthNextDay, $yearNextDay, $person->basic_info_id)");
                    if(!$isStaffOff) 
                        DB::select("CALL generateTimeKeep($month, $year, $staff->id)");
                }
            } else {
                $create_input = [
                    'month'         => $month,
                    'year'          => $year,
                    'with_salary'   => 0,
                    'paid_holidays' => 0,
                    'no_salary'     => 0,
                    'other'         => 0,
                    'total_salary'  => 0,
                ];


                $isLeapYear = false;
                if ($year % 4 == 0)
                    if ($year % 100 == 0) {
                        if ($year % 400 == 0)
                            $isLeapYear = true;
                    } else
                        $isLeapYear = true;
                
                $tempDayOfWeek = $nextDay->dayOfWeek;
                for ($i = 1; $i <= ($isLeapYear ? 29 : 28); $i++) {
                    if (in_array($tempDayOfWeek, [1,2,3,4,5]))
                        $create_input['day_'.$i] = 2;
                    else if ($tempDayOfWeek == 6)
                        $create_input['day_'.$i] = 1;
                    else if ($tempDayOfWeek == 7) {
                        $create_input['day_'.$i] = 0;
                        $tempDayOfWeek = 0;
                    }
                    $tempDayOfWeek += 1;
                }

                if (!$isLeapYear) 
                    $create_input['day_29'] = 0;
                $create_input['day_30'] = 0;
                $create_input['day_31'] = 0;
                foreach ($staffs as $staff) {
                    $isStaffOff = $staff->larborBbt->from_date_qdtv  && strtotime($staff->larborBbt->from_date_qdtv) <= strtotime($nextDay);
                    if(!$isStaffOff) {
                        $create_input['basic_info_id'] = $staff -> id;
                        Timekeeping::firstOrCreate($create_input);
                    }
                       
                }        
            }
    }
}
