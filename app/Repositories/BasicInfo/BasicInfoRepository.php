<?php

namespace App\Repositories\BasicInfo;
use App\Models\BasicInfo;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

class BasicInfoRepository extends BaseRepository 
{
    public function __construct(BasicInfo $model)     
    {         
        $this->model = $model;
    }

    /**
     * Save a BasicInfo
     *
     * @author lam
     * @since 
     * @param  
     * @return 
     */
    public function saveBasicInfo(array $input)
    {

        try {
             $result = $this->create([
                'company'                   => $input['company'],
                'rank'                      => $input['rank'],
                'full_name'                 => $input['full_name'],
                'english_name'              => $input['name_eng'],
                'employee_code'             => $input['code_staff'],
                'date_of_entry_to_work'     => $input['date_work'],
                'gender'                    => $input['genders'],
                'nationality'               => $input['nation'],
                'phone'                     => $input['phone'],
                'personal_email'            => $input['email'],
                'company_email'             => $input['email_company'],
                'dob'                       => $input['birth_date'],
                'disc'                      => "",
                'motivators'                => "",
                'cv'                        => "",
                'created_by'                => auth()->id(),
                'updated_by'                => auth()->id(),
            ]);

            return ($result);

            
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }

    public function updateBasicInfo( $id, array $input)
    {
        
        try {
            // dd($input);
            $result = $this->update($id, $input);
            return ($result);

            
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }

    public function deleteBasicInfo( $id)
    {
        try {
             $result = $this->update($id, [
                'delete_flag' => 1
             ]);             

            return ($result);

            
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }

    
}
