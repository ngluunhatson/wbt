<?php

namespace App\Repositories\Plan10y;

use App\Models\Plan10y;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Plan10yRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param Plan10y $model
     */
    public function __construct(Plan10y $model)
    {
        parent::__construct($model);
    }

}
