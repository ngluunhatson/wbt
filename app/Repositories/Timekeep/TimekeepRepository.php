<?php

namespace App\Repositories\Timekeep;

use App\Models\Timekeeping;
use App\Repositories\BaseRepository;

class TimekeepRepository extends BaseRepository
{

    public function __construct(Timekeeping $model)     
    {         
        parent::__construct($model);
    }

}