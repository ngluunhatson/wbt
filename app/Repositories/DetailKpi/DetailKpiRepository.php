<?php

namespace App\Repositories\DetailKpi;

use App\Models\DetailsKpi;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Support\Facades\Log;

class DetailKpiRepository extends BaseRepository
{
    /**
     * DetailKpiRepository constructor.
     *
     * @param DetailsKpi $model
     */
    public function __construct(DetailsKpi $model)
    {
        parent::__construct($model);
    }

    public function getDataByKpiID( $id)
    {
        // dd($input);
        try {
             $result = $this->model->where('kpis_id',$id)->get();

            return ($result);

            
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }

    public function deleteByID( $id)
    {
        // dd($input);
        try {
            $ids = $this->model->where('kpis_id',$id)->pluck('id');
            // $id = $id[0]->id;
            foreach($ids as $id){
                $this->update($id, [
                    'delete_flag' => 1
                 ]);
            }

            return ($result = true);

            
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }
}
