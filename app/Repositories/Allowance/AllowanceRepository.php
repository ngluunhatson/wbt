<?php

namespace App\Repositories\Allowance;

use App\Models\Allowance;
use App\Repositories\BaseRepository;

class AllowanceRepository extends BaseRepository
{

    /**      
     * AllowanceRepository constructor.      
     *      
     * @param Allowance $model      
     */ 
    public function __construct(Allowance $model)     
    {         
        parent::__construct($model);
    }

}