<?php

namespace App\Repositories\Insurance;

use App\Models\Insurance;
use App\Models\Salary;
use App\Repositories\BaseRepository;

class InsuranceRepository extends BaseRepository
{

    /**      
     * InsuranceRepository constructor.      
     *      
     * @param Insurance $model      
     */ 
    public function __construct(Insurance $model)     
    {         
        parent::__construct($model);
    }

}