<?php

namespace App\Repositories\RecruitConfirm;

use App\Models\RecruitConfirm;
use App\Repositories\BaseRepository;

class RecruitConfirmRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param RecruitConfirm $model
     */
    public function __construct(RecruitConfirm $model)
    {
        parent::__construct($model);
    }
}
