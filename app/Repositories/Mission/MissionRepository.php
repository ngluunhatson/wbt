<?php

namespace App\Repositories\Mission;

use App\Models\Mission;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class MissionRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param Mission $model
     */
    public function __construct(Mission $model)
    {
        parent::__construct($model);
    }
}
