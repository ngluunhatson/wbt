<?php

namespace App\Repositories\CriteriaConfirm;

use App\Models\CriteriaConfirm;
use App\Repositories\BaseRepository;

class CriteriaConfirmRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param CriteriaConfirm $model
     */
    public function __construct(CriteriaConfirm $model)
    {
        parent::__construct($model);
    }
}
