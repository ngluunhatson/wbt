<?php
namespace App\Repositories;

interface BaseRepositoryInterface
{
    public function findAll();

    public function getById($id);

    public function findFirst(array $conditions);

    public function findByCondition(array $conditions);

    public function create(array $data);

    public function update($id,array $data);
    
    public function destroy($id);
}