<?php

namespace App\Repositories\Team;

use App\Models\Team;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class TeamRepository extends BaseRepository
{

    public function __construct(Team $model)     
    {         
        parent::__construct($model);
    }

}