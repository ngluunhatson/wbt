<?php

namespace App\Repositories\LaborContract;

use App\Models\LaborContract;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

class LaborContractRepository extends BaseRepository
{

    public function __construct(LaborContract $model)
    {
        $this->model = $model;
    }

    /**
     * Save a PersonalInfo
     *
     * @author lam
     * @since 
     * @param  
     * @return 
     */
    public function saveLaborContract(array $input)
    {
        try {
            for ($i = 1; $i < 4; $i++) {
                $this->create([
                    'basic_info_id'                     => $input['basic_info_id'],
                    'number_of_labor_contract'          => isset($input['number_hdld_' . $i]) ? $input['number_hdld_' . $i] : NULL,
                    'type_of_contract'                  => isset($input['contract_' . $i]) ? $input['contract_' . $i] : NULL,
                    'form_date_labor'                   => isset($input['date_form_' . $i]) ? $input['date_form_' . $i] : NULL,
                    'to_date'                           => isset($input['to_form_' . $i]) ? $input['to_form_' . $i] : NULL,
                    'labor_contract_salary'             => isset($input['wage_hdld_' . $i]) ? $input['wage_hdld_' . $i] : NULL,
                    'contract_addendum_number'          => isset($input['number_plhd_' . $i]) ? $input['number_plhd_' . $i] : NULL,
                    'form_date_addendum'                => isset($input['date_form_plhd_' . $i]) ? $input['date_form_plhd_' . $i] : NULL,
                    'contract_addendum_salary'          => isset($input['wage_plhd_' . $i]) ? $input['wage_plhd_' . $i] : NULL,
                    'salary_kpis'                       => isset($input['wage_kpi_' . $i]) ? $input['wage_kpi_' . $i] : NULL,
                    'other_policy'                      => isset($input['diff_' . $i]) ? $input['diff_' . $i] : NULL,
                    'type'                              => 1,
                    'created_by'                        => auth()->id(),
                ]);
            }

            $this->create([
                'basic_info_id'                     => $input['basic_info_id'],
                'no_bbtlhd'                         => isset($input['number_bbtlhd']) ? $input['number_bbtlhd'] : NULL,
                'from_date_bbtlhd'                  => isset($input['date_form_bbtlhd']) ? $input['date_form_bbtlhd'] : NULL,
                'no_qdtv'                           => isset($input['number_qdtv']) ? $input['number_qdtv'] : NULL,
                'from_date_qdtv'                    => isset($input['date_form_qdtv']) ? $input['date_form_qdtv'] : NULL,
                'type'                              => 2,
                'created_by'                        => auth()->id(),
            ]);


            return true;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }


    public function updateLaborContract(array $input)
    {
        try {
            // $id = $this->model->where('basic_info_id',$basic_info_id)->get('id');
            // $id = $id[0]->id;
            // dd($input['date_form_bbtlhd']);
            // $result = $this->model->find($id)->update( $input);

            // $a = $this->model->find($id);
            // dd($a);

            for ($i = 1; $i < 4; $i++) {
                if (array_key_exists('labor_id_' . $i, $input)) {
                    $this->update($input['labor_id_' . $i], [
                        'number_of_labor_contract'          => isset($input['number_hdld_' . $i]) ? $input['number_hdld_' . $i] : NULL,
                        'type_of_contract'                  => isset($input['contract_' . $i]) ? $input['contract_' . $i] : NULL,
                        'form_date_labor'                   => isset($input['date_form_' . $i]) ? $input['date_form_' . $i] : NULL,
                        'to_date'                           => isset($input['to_form_' . $i]) ? $input['to_form_' . $i] : NULL,
                        'labor_contract_salary'             => isset($input['wage_hdld_' . $i]) ? $input['wage_hdld_' . $i] : NULL,
                        'contract_addendum_number'          => isset($input['number_plhd_' . $i]) ? $input['number_plhd_' . $i] : NULL,
                        'form_date_addendum'                => isset($input['date_form_plhd_' . $i]) ? $input['date_form_plhd_' . $i] : NULL,
                        'contract_addendum_salary'          => isset($input['wage_plhd_' . $i]) ? $input['wage_plhd_' . $i] : NULL,
                        'salary_kpis'                       => isset($input['wage_kpi_' . $i]) ? $input['wage_kpi_' . $i] : NULL,
                        'other_policy'                      => isset($input['diff_' . $i]) ? $input['diff_' . $i] : NULL,
                        'updated_by'                        => auth()->id(),
                    ]);
                }
            }

            // for($i=1 ; $i<2 ; $i++){
            if ($input['bbt_id_id']) {
                $this->update($input['bbt_id_id'], [
                    'no_bbtlhd'                         => isset($input['number_bbtlhd']) ? $input['number_bbtlhd'] : NULL,
                    'from_date_bbtlhd'                  => isset($input['date_form_bbtlhd']) ? $input['date_form_bbtlhd'] : NULL,
                    'no_qdtv'                           => isset($input['number_qdtv']) ? $input['number_qdtv'] : NULL,
                    'from_date_qdtv'                    => isset($input['date_form_qdtv']) ? $input['date_form_qdtv'] : NULL,
                    'updated_by'                        => auth()->id(),
                ]);
            }
            // }


            return ($result = true);
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }

    public function deleteLaborContract($basic_info_id)
    {
        try {
            $ids = $this->model->where('basic_info_id', $basic_info_id)->pluck('id');
            // $id = $id[0]->id;
            foreach ($ids as $id) {
                $this->update($id, [
                    'delete_flag' => 1
                ]);
            }

            return ($result = true);
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }
}
