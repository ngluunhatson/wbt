<?php

namespace App\Repositories\Internship;

use App\Models\InternshipDetail;
use App\Repositories\BaseRepository;


class InternshipDetailRepository extends BaseRepository
{
    public function __construct(InternshipDetail $model)     
    {         
        parent::__construct($model);
    }
}