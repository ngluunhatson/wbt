<?php

namespace App\Repositories\Internship;

use App\Models\Internship;
use App\Repositories\BaseRepository;


class InternshipRepository extends BaseRepository
{
    public function __construct(Internship $model)     
    {         
        parent::__construct($model);
    }
}