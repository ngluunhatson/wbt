<?php

namespace App\Repositories\Internship;

use App\Models\InternshipConfirm;
use App\Repositories\BaseRepository;


class InternshipConfirmRepository extends BaseRepository
{
    public function __construct(InternshipConfirm $model)     
    {         
        parent::__construct($model);
    }
}