<?php

namespace App\Repositories\Blog;

use App\Models\Blog;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class BlogRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param Blog $model
     */
    public function __construct(Blog $model)
    {
        parent::__construct($model);
    }

    public function getBlogsByCategory($category, $search)
    {
        // GET BLOG WHEN:
        // CASE 1: All companies can see it
        // CASE 2: Only the room and all members of the room can see
        // CASE 3: Only team and team members can see
        // CASE 4: Only position and position members can see
        // CASE 5: Only one person can see it
        $view_blog = new $this->model;
        if (isset($search['searchPost'])) {
            $view_blog = $this->model->with(['category'])
                ->where('title', 'like', '%' . $search['searchPost'] . '%');
        } else if (isset($search['month'])) {
            // dd(\Carbon\Carbon::parse( $search['start']))->valueOf();
            // \Carbon\Carbon::parse('1-'.$search['start'])->gt($test) {

            // }
            $view_blog = $this->model->with(['category'])
                ->where('category_id', $category)
                ->where([
                    ['created_at', '>=', \Carbon\Carbon::createFromFormat('d/m/Y', '1/'.$search['month'])->format('Y-m-d')],
                    ['created_at', '<', \Carbon\Carbon::createFromFormat('d/m/Y', '1/'.$search['month'])->addMonth()->format('Y-m-d')],
                ]);
        } else {
            $view_blog = $this->model->with(['category'])
                ->where([
                    ['category_id', $category],
                    ['all_company', 1],
                ]);
        }
        // if($search != '') {
        //     $view_blog = $this->model->with(['category'])
        //     ->where('title', 'like', '%' . $search['searchPost'] . '%');
        // } else {
        //     $view_blog = $this->model->with(['category'])
        //     ->where([
        //         ['category_id', $category],
        //         ['all_company', 1],
        //     ]);
        // }
        $view_blog =
            $view_blog->orWhere([
                ['category_id', $category],
                ['all_company', 0],
                ['team_id', 0],
                ['room_id', auth()->user()->organizational->room_id ?? -1],
            ])
            ->orWhere([
                ['category_id', $category],
                ['all_company', 0],
                ['position_id', 0],
                ['room_id', auth()->user()->organizational->room_id ?? -1],
                ['team_id', auth()->user()->organizational->team_id ?? -1],
            ])
            ->orWhere([
                ['category_id', $category],
                ['all_company', 0],
                ['basic_info_id', 0],
                ['room_id', auth()->user()->organizational->room_id ?? -1],
                ['team_id', auth()->user()->organizational->team_id ?? -1],
                ['position_id', auth()->user()->organizational->position_id ?? -1],
            ])
            ->orWhere([
                ['category_id', $category],
                ['all_company', 0],
                ['basic_info_id', auth()->user()->organizational->basic_info_id ?? -1],
            ]);


        $blogs = $view_blog;
        // OLD DATA
        // $blogs = $this->model->with(['category'])->where('category_id', $category)->orderBy('created_at', 'desc')->paginate(9);
        return $blogs;
    }
}
