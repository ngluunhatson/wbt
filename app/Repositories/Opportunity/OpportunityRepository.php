<?php

namespace App\Repositories\Opportunity;

use App\Models\Opportunity;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class OpportunityRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param Opportunity $model
     */
    public function __construct(Opportunity $model)
    {
        parent::__construct($model);
    }
}
