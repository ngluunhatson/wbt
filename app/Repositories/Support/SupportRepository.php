<?php

namespace App\Repositories\Support;

use App\Models\Support;
use App\Repositories\BaseRepository;

class SupportRepository extends BaseRepository
{

    /**      
     * SupportRepository constructor.      
     *      
     * @param Support $model      
     */     
    public function __construct(Support $model)     
    {         
        parent::__construct($model);
    }

}