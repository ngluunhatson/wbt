<?php

namespace App\Repositories;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class BaseRepository implements BaseRepositoryInterface
{
    /**      
     * @var Model      
     */
    protected $model;

    /**      
     * BaseRepository constructor.      
     *      
     * @param Model $model      
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function loadRelation(array $relationships)
    {
        $result = [];

        try {
            $query = $this->model->with($relationships)->get();
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function findAll()
    {
        $result = [];

        try {
            $query = $this->model->get();
            $result = !empty($query) ? $query->toArray() : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function getById($id)
    {
        $result = [];
        try {
            $query = $this->model->findorFail($id);
            $result = !empty($query) ? $query->toArray() : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function findFirst(array $conditions)
    {
        $result = [];

        try {
            $query = $this->model->where($conditions)->first();
            $result = !empty($query) ? $query->toArray() : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function findByCondition(array $conditions)
    {
        $result = [];

        try {
            $query = $this->model->where($conditions)->get();
            $result = !empty($query) ? $query->toArray() : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function create(array $data)
    {
        $result = [];

        try {
            $result = $this->model->firstOrCreate($data);
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function forceCreate(array $data)
    {
        $result = [];

        try {
            $result = $this->model->create($data);
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function update($id, array $data)
    {
        $result = [];
        try {
            $result = $this->model->where('id', $id)->update($data);
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }



        return $result;
    }
    /*
        Delete 1 record  
    */
    public function destroy($id)
    {
        $result = [];

        try {
            $result = $this->model->where('id', $id)->delete();
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    /*
        Update delete_flag to 1 (status delete) 
    */
    public function delete($id)
    {
        $result = [];

        try {
            $result = $this->model->where('id', $id)->update([
                'delete_flag' => 1,
            ]);
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }
}
