<?php

namespace App\Repositories\Tax;

use App\Models\Tax;
use App\Repositories\BaseRepository;

class TaxRepository extends BaseRepository
{

    /**      
     * TaxRepository constructor.      
     *      
     * @param Tax $model      
     */     
    public function __construct(Tax $model)     
    {         
        parent::__construct($model);
    }

}