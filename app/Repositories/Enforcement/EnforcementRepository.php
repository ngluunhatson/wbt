<?php

namespace App\Repositories\Enforcement;

use App\Models\Enforcement;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class EnforcementRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param Enforcement $model
     */
    public function __construct(Enforcement $model)
    {
        parent::__construct($model);
    }
}
