<?php

namespace App\Repositories\Room;

use App\Models\Room;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class RoomRepository extends BaseRepository
{
    public function __construct(Room $model)     
    {         
        parent::__construct($model);
    }
}