<?php

namespace App\Repositories\Dayoff;

use App\Models\DetailsDayOff;
use App\Repositories\BaseRepository;


class DetailDayOffRepository extends BaseRepository
{
    public function __construct(DetailsDayOff $model)     
    {         
        parent::__construct($model);
    }
}