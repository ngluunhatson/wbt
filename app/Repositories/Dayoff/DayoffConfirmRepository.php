<?php

namespace App\Repositories\Dayoff;

use App\Models\DayOffConfirm;
use App\Repositories\BaseRepository;


class DayoffConfirmRepository extends BaseRepository
{
    public function __construct(DayOffConfirm $model)     
    {         
        parent::__construct($model);
    }
}