<?php

namespace App\Repositories\Dayoff;

use App\Models\DayOff;
use App\Repositories\BaseRepository;


class DayoffRepository extends BaseRepository
{
    public function __construct(DayOff $model)     
    {         
        parent::__construct($model);
    }
}