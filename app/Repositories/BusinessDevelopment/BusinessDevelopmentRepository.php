<?php

namespace App\Repositories\BusinessDevelopment;

use App\Models\BusinessDevelopment;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class BusinessDevelopmentRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param BusinessDevelopment $model
     */
    public function __construct(BusinessDevelopment $model)
    {
        parent::__construct($model);
    }
}
