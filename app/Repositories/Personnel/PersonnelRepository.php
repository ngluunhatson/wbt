<?php

namespace App\Repositories\Personnel;

use App\Models\Personnel;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class PersonnelRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param Personnel $model
     */
    public function __construct(Personnel $model)
    {
        parent::__construct($model);
    }
}
