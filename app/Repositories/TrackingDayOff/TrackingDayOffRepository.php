<?php

namespace App\Repositories\TrackingDayOff;

use App\Models\TrackingDayOff;
use App\Repositories\BaseRepository;


class TrackingDayOffRepository extends BaseRepository
{
    public function __construct(TrackingDayOff $model)     
    {         
        parent::__construct($model);
    }
}