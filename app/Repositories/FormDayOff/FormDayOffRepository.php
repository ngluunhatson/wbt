<?php

namespace App\Repositories\FormDayOff;

use App\Models\FormDayOff;
use App\Repositories\BaseRepository;


class FormDayOffRepository extends BaseRepository
{
    public function __construct(FormDayOff $model)     
    {         
        parent::__construct($model);
    }
}