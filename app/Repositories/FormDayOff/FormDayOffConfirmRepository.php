<?php

namespace App\Repositories\FormDayOff;

use App\Models\FormDayOffConfirm;
use App\Repositories\BaseRepository;


class FormDayOffConfirmRepository extends BaseRepository
{
    public function __construct(FormDayOffConfirm $model)     
    {         
        parent::__construct($model);
    }
}