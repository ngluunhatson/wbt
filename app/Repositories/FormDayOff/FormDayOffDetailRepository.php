<?php

namespace App\Repositories\FormDayOff;

use App\Models\FormDayOffDetail;
use App\Repositories\BaseRepository;


class FormDayOffDetailRepository extends BaseRepository
{
    public function __construct(FormDayOffDetail $model)     
    {         
        parent::__construct($model);
    }
}