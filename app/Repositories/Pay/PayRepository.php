<?php

namespace App\Repositories\Pay;

use App\Models\Pay;
use App\Repositories\BaseRepository;

class PayRepository extends BaseRepository
{

    /**      
     * PayRepository constructor.      
     *      
     * @param Pay $model      
     */ 
    public function __construct(Pay $model)     
    {         
        parent::__construct($model);
    }

}