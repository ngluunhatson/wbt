<?php

namespace App\Repositories\Position;

use App\Models\Position;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class PositionRepository extends BaseRepository
{
    public function __construct(Position $model)     
    {         
        parent::__construct($model);
    }
}