<?php

namespace App\Repositories\Criteria;

use App\Models\Criterion;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CriteriaRepository extends BaseRepository
{
    /**
     * CriteriaRepository constructor.
     *
     * @param Criterion $model
     */
    public function __construct(Criterion $model)
    {
        parent::__construct($model);
    }
}
