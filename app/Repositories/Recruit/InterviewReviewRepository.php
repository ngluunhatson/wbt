<?php

namespace App\Repositories\Recruit;

use App\Models\InterviewReview;
use App\Repositories\BaseRepository;

class InterviewReviewRepository extends BaseRepository
{
    /**
     * InterviewReviewRepository constructor.
     *
     * @param InterviewReview $model
     */
    public function __construct(InterviewReview $model)
    {
        parent::__construct($model);
    }
}
