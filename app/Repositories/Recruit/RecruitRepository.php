<?php

namespace App\Repositories\Recruit;

use App\Models\Recruitment;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class RecruitRepository extends BaseRepository
{
    /**
     * RecruitRepository constructor.
     *
     * @param Recruitment $model
     */
    public function __construct(Recruitment $model)
    {
        parent::__construct($model);
    }
}
