<?php

namespace App\Repositories\Recruit;

use App\Models\InterviewScore;
use App\Repositories\BaseRepository;


class InterviewScoreRepository extends BaseRepository
{
    /**
     * InterviewScoreRepository constructor.
     *
     * @param InterviewScore $model
     */
    public function __construct(InterviewScore $model)
    {
        parent::__construct($model);
    }
}
