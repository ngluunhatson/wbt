<?php

namespace App\Repositories\Plan;

use App\Models\PlanWeek;
use App\Repositories\BaseRepository;

class PlanWeekRepository extends BaseRepository
{
    /**
     * PlanWeekRepository constructor.
     *
     * @param PlanWeek $model
     */
    public function __construct(PlanWeek $model)
    {
        parent::__construct($model);
    }

}
