<?php

namespace App\Repositories\Plan;

use App\Models\PlanQuarter;
use App\Repositories\BaseRepository;

class PlanQuarterRepository extends BaseRepository
{
    /**
     * PlanQuarterRepository constructor.
     *
     * @param PlanQuarter $model
     */
    public function __construct(PlanQuarter $model)
    {
        parent::__construct($model);
    }
}
