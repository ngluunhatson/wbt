<?php

namespace App\Repositories\Plan;

use App\Models\PlanDayTimeslot;
use App\Repositories\BaseRepository;

class PlanDayTimeslotRepository extends BaseRepository
{
    /**
     * PlanDayTimeslottRepository constructor.
     *
     * @param PlanDayTimeslot $model
     */
    public function __construct(PlanDayTimeslot $model)
    {
        parent::__construct($model);
    }

}
