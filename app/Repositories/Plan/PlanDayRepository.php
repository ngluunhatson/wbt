<?php

namespace App\Repositories\Plan;

use App\Models\PlanDay;
use App\Repositories\BaseRepository;

class PlanDayRepository extends BaseRepository
{
    /**
     * PlanDayRepository constructor.
     *
     * @param PlanDay $model
     */
    public function __construct(PlanDay $model)
    {
        parent::__construct($model);
    }

}
