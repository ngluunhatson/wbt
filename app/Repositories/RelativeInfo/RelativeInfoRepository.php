<?php

namespace App\Repositories\RelativeInfo;
use App\Models\RelativeInfo;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

class RelativeInfoRepository extends BaseRepository 
{
    
    public function __construct(RelativeInfo $model)     
    {         
        $this->model = $model;
    }

    /**
     * Save a PersonalInfo
     *
     * @author lam
     * @since 
     * @param  
     * @return 
     */
    public function saveRelativeInfo(array $input)
    {
        try {
            for($i=1; $i<3; $i++){
                // if($input['prioritized_'.$i]){
                    $this->model::create([
                        'basic_info_id'                 => $input['basic_info_id'] ,
                        'full_name_priority'            => isset($input['prioritized_'.$i]) ? $input['prioritized_'.$i] : NULL,
                        'relationship'                  => isset($input['relationship_prioritized_'.$i]) ? $input['relationship_prioritized_'.$i] : NULL,
                        'phone_number'                  => isset($input['phone_prioritized_'.$i]) ? $input['phone_prioritized_'.$i] : NULL,
                        'type'                          => 1,
                        'created_by'                    => auth()->id(),
                    ]);
                // }
            }
            for($i=1; $i<4; $i++){
                // if($input['child_'.$i]){
                    $this->model::create([
                        'basic_info_id'                 => $input['basic_info_id'],
                        'full_name_child'               => isset($input['child_'.$i]) ? $input['child_'.$i] : NULL,
                        'gender'                        => isset($input['gender_child_'.$i]) ? $input['gender_child_'.$i] : NULL,
                        'dob'                           => isset($input['birth_date_child_'.$i]) ? $input['birth_date_child_'.$i] : NULL,
                        'type'                          => 2,
                        'created_by'                    => auth()->id(),
                    ]);
                // }
            } 
            return ($result = true);
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }


    public function updateRelativeInfo(array $input)
    {
        try {
            // $id = $this->model->where('basic_info_id',$basic_info_id)->get('id');
            // $id = $id[0]->id;
            // dd($input['providers']);
            // $result = $this->model->find($id)->update( $input);

            // $a = $this->model->find($id);
            // dd($a);

            

            for($i=1 ; $i<3 ; $i++){
                if($input['priority_id_'.$i]){
                    $this->update($input['priority_id_'.$i], [
                        'full_name_priority'            => isset($input['prioritized_'.$i]) ? $input['prioritized_'.$i] : NULL,
                        'relationship'                  => isset($input['relationship_'.$i]) ? $input['relationship_'.$i] : NULL,
                        'phone_number'                  => isset($input['phone_prioritized_'.$i]) ? $input['phone_prioritized_'.$i] : NULL,
                        'created_by'                    => auth()->id(),
                        'updated_by'                    => auth()->id(),
                     ]);
                }
            }

            for($i=1 ; $i<4 ; $i++){
                if($input['childPriority_id_'.$i]){
                    $this->update($input['childPriority_id_'.$i], [
                        'full_name_child'               => isset($input['child_'.$i]) ? $input['child_'.$i] : NULL,
                        'gender'                  => isset($input['gender_child_'.$i]) ? $input['gender_child_'.$i] : NULL,
                        'dob'                           => isset($input['birth_date_child_'.$i]) ? $input['birth_date_child_'.$i] : NULL,
                        'created_by'                    => auth()->id(),
                        'updated_by'                    => auth()->id(),
                     ]);
                }
            }

            
            return ($result = true);

            
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }

    public function deleteRelativeInfo( $basic_info_id)
    {
        try {
            $ids = $this->model->where('basic_info_id',$basic_info_id)->pluck('id');
            // $id = $id[0]->id;
            foreach($ids as $id){
                $this->update($id, [
                    'delete_flag' => 1
                 ]);
            }            

            return ($result = true);

            
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }
}