<?php

namespace App\Repositories\Confirm;

use App\Models\Confirm;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class ConfirmRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param Confirm $model
     */
    public function __construct(Confirm $model)
    {
        parent::__construct($model);
    }
}
