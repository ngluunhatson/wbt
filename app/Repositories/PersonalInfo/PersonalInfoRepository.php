<?php

namespace App\Repositories\PersonalInfo;
use App\Models\PersonalInfo;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

class PersonalInfoRepository extends BaseRepository 
{
    
    public function __construct(PersonalInfo $model)     
    {         
        $this->model = $model;
    }

    /**
     * Save a PersonalInfo
     *
     * @author lam
     * @since 
     * @param  
     * @return 
     */
    public function savePersonalInfo(array $input)
    {
        try {
             $result = $this->create([
                'basic_info_id'                 => $input['basic_info_id'],
                'ethnic'                        => $input['ethnic'],
                'cccd'                          => $input['passport'],
                'date_range'                    => $input['issue_date'],
                'issued_by'                     => $input['providers'],
                'permanent_address'             => $input['permanent_address'],
                'temporary_residence_address'   => $input['temporary_address'],
                'social_insurance_code'         => $input['insurance_code'],
                'health_insurance_code'         => $input['medical_code'],
                'healthcare_place'              => $input['address_kcb'],
                'personal_income_tax'           => $input['tax'],
                'dependent_person'              => $input['dependent_person'],
                'details_dependent_person'      => $input['npt'],
                'academic_level'                => $input['level'],
                'school'                        => $input['school'],
                'specialized'                   => $input['specialized'],
                'number_bank_account'           => $input['tknh'],
                'bank_name'                     => $input['bank'],
                'license_plates'                => $input['license_plates'],
                'range_of_vehicle'              => $input['carriage_category'],
                'created_by'                    => auth()->id(),
            ]);
            return ($result);
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }

    public function updatePersonalInfo( $basic_info_id, array $input)
    {
        // dd($input);
        try {
            $id = $this->model->where('basic_info_id',$basic_info_id)->get('id');
            $id = $id[0]->id;
            // dd($input['providers']);
            // $result = $this->model->find($id)->update( $input);

            // $a = $this->model->find($id);
            // dd($a);

            $result = $this->update($id, $input);
            return ($result);

            
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }

    public function deletePersonalInfo( $basic_info_id)
    {
        try {
            $ids = $this->model->where('basic_info_id',$basic_info_id)->pluck('id');
            // $id = $id[0]->id;
            foreach($ids as $id){
                $this->update($id, [
                    'delete_flag' => 1
                 ]);
            }
                          

            return ($result = true);

            
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }
    }
}