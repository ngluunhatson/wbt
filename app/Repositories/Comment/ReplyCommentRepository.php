<?php

namespace App\Repositories\Comment;
use App\Models\ReplyComment;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class ReplyCommentRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param ReplyComment $model
     */
    public function __construct(ReplyComment $model)
    {
        parent::__construct($model);
    }

}
