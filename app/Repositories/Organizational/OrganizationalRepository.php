<?php

namespace App\Repositories\Organizational;

use App\Models\Organizational;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class OrganizationalRepository extends BaseRepository
{
    /**
     * OrganizationalRepository constructor.
     *
     * @param Organizational $model
     */
    public function __construct(Organizational $model)
    {
        parent::__construct($model);
    }

    /**
     * Get data of Organizational table by room_id 
     * author : ntqui
     * since : 20221005
     *
     * @param  String $id
     * @return array
     */
    public function getDataById(string $id): array
    {
        $result = [];

        try {
            $query = $this->loadRelation(['staff'])->find($id);
            $result = !empty($query) ? $query->toArray() : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    /**
     * Get data of Organizational table by room_id 
     * author : ntqui
     * since : 20221005
     *
     * @param  String $id
     * @return Collection
     */
    public function getDataByRomID(string $room_id): Collection
    {
        $result = [];

        try {
            $query = $this->model->where('room_id', $room_id)->get();
            $result = !empty($query) ? $query->toTree() : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function createNodeData(array $input)
    {
        $result = [];

        try {
            $this->model->fixTree();
            $input['created_by'] = auth()->id();
            $query = $this->create($input);
            $result = !empty($query) ? $this->model->with(['room', 'team', 'position', 'staff'])->find($query->id) : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function updateNodeData($id, array $input)
    {
        $result = [];

        try {
            $this->model->fixTree();
            $input['updated_by'] = auth()->id();
            $query = $this->update($id, $input);
            $result = !empty($query) ? $this->model->with(['room', 'team', 'position', 'staff'])->find($id) : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function getRooms()
    {
        $result = [];

        try {
            $query = $this->model->all()->unique('room_id');
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function getRoomByBasicInfo($basic_info_id)
    {
        $result = [];
        // dd($room_id);
        try {
            $query = $this->loadRelation(['room'])->where('basic_info_id', $basic_info_id)->unique('room_id');
            $result = !empty($query) ? $query : [];
            // dd($result);
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function getTeamsByRoom($room_id)
    {
        $result = [];
        // dd($room_id);
        try {
            $query = $this->loadRelation(['team'])->where('room_id', $room_id)->unique('team_id');
            $result = !empty($query) ? $query : [];
            // dd($result);
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function getPositionsByTeam($team_id)
    {
        $result = [];

        try {
            $query = $this->loadRelation(['position'])->where('position.flag_filter', 1)->where('team_id', $team_id)->unique('position_id');
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function getStaffByConditions($room_id, $team_id, $position_id)
    {
        $result = [];

        try {
            if (!is_array($position_id)) {
                $query = $this->loadRelation(['staff', 'room', 'team', 'position'])
                    ->where('room_id', $room_id)
                    ->where('team_id', $team_id)
                    ->where('position_id', $position_id);
            } else {
                $query = $this->loadRelation(['staff', 'room', 'team', 'position'])
                    ->where('room_id', $room_id)
                    ->where('team_id', $team_id)
                    ->whereIn('position_id', $position_id);
            }
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function getMangerInRoom($room_id)
    {
        $result = [];

        try {
            $query = $this->model
                ->with(['staff', 'position' => function ($query) {
                    $query->whereIn('level', [0, 1]);
                }])
                ->where('room_id', $room_id)
                ->get()
                ->where('position', '<>', null)
                ->where('basic_info_id', '<>', null);
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }

    public function getListChief($position_id)
    {
        $result = [];

        try {
            $query = $this->loadRelation(['staff'])
                ->where('position_id', $position_id)
                ->unique('basic_info_id');
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            throw $e;
        }

        return $result;
    }
}
