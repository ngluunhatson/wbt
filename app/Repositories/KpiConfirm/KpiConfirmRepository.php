<?php

namespace App\Repositories\KpiConfirm;

use App\Models\KpiConfirm;
use App\Repositories\BaseRepository;

class KpiConfirmRepository extends BaseRepository
{
    /**
     * ConfirmRepository constructor.
     *
     * @param KpiConfirm $model
     */
    public function __construct(KpiConfirm $model)
    {
        parent::__construct($model);
    }
}
