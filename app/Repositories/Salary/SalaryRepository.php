<?php

namespace App\Repositories\Salary;

use App\Models\Salary;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Support\Facades\Log;

class SalaryRepository extends BaseRepository
{

    /**      
     * SalaryRepository constructor.      
     *      
     * @param Salary $model      
     */ 
    public function __construct(Salary $model)     
    {         
        parent::__construct($model);
    }

    public function getDataById($id)
    {
        $result = [];
        try {
            $data = $this->loadRelation([
                    'staff', 'timekeep', 'organizational.team', 'organizational.position', 'income',
                    'support', 'allowance', 'insurance', 'tax', 'pay'
                ])
                ->where('id', $id)
                ->where('delete_flag', 0)
                ->first();
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getIdByTimekeep($timekeep_id)
    {
        $result = [];
        try {
            $data = $this->loadRelation([
                    'income:id',
                    'timekeep',
                    'support:id', 'allowance:id', 'insurance:id', 'tax:id', 'pay:id'
                ])
                ->where('timekeeping_id', $timekeep_id)
                ->where('delete_flag', 0)
                ->first();
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}