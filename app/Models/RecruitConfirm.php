<?php
namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RecruitConfirm extends Model
{
    protected $guarded = ['id'];
    public function confirm(){
        return $this->belongsTo(Confirm::class, 'confirm_id');
    }

    public function confirmReceive() {
        return $this->belongsTo(Confirm::class, 'confirm_id')->where('confirm.type_confirm', 1);
    }
}
