<?php
namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CriteriaConfirm extends Model
{
    protected $guarded = ['id'];
    public function confirm(){
        return $this->belongsTo(Confirm::class, 'confirm_id');
    }

    public function criteria(){
        return $this->belongsTo(Criterion::class, 'criteria_id');
    }

    public function confirmReceive() {
        return $this->belongsTo(Confirm::class, 'confirm_id')->where('confirm.type_confirm', 1);
    }
}
