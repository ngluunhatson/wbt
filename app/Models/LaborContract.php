<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LaborContract
 * 
 * @property int $id
 * @property int $basic_info_id
 * @property int|null $labor_contract_times
 * @property string|null $number_of_labor_contract
 * @property string|null $type_of_contract
 * @property Carbon|null $form_date_labor
 * @property Carbon|null $to_date
 * @property string|null $labor_contract_salary
 * @property string|null $contract_addendum_number
 * @property string|null $form_date_addendum
 * @property string|null $contract_addendum_salary
 * @property string|null $salary_kpis
 * @property string|null $other_policy
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class LaborContract extends Model
{
	protected $table = 'labor_contract';

	protected $casts = [
		'basic_info_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	// protected $dates = [
	// 	'form_date_labor',
	// 	'to_date'
	// ];

	protected $fillable = [
		'basic_info_id',
		'number_of_labor_contract',
		'type_of_contract',
		'form_date_labor',
		'to_date',
		'labor_contract_salary',
		'contract_addendum_number',
		'form_date_addendum',
		'contract_addendum_salary',
		'salary_kpis',
		'other_policy',
		'no_bbtlhd',
		'from_date_bbtlhd',
		'no_qdtv',
		'from_date_qdtv',
		'type',
	];
}
