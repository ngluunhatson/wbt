<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FormDayOffDetail
 * 
 * @property int $id
 * @property int $day_off_id
 * @property Carbon|null $start_date
 * @property Carbon|null $start_time
 * @property Carbon|null $end_date
 * @property Carbon|null $end_time
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class FormDayOffDetail extends Model
{
	protected $table = 'form_day_off_detail';

	protected $casts = [
		'total_day_off_in_timeframe'    => 'float',
		'created_by' 			        => 'int',
		'updated_by' 				    => 'int',
		'delete_flag' 				    => 'bool'
	];

	protected $dates = [
		'start_date',
		'end_date',
	];

	protected $fillable = [
        'form_day_off_id',
		'start_date',
		'start_time',
		'end_date',
		'end_time',
		'reason',
		'total_day_off_in_timeframe',
        'type',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	
}
