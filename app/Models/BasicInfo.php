<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BasicInfo
 * 
 * @property int $id
 * @property string $company
 * @property string $full_name
 * @property string $english_name
 * @property string $employee_code
 * @property Carbon $date_of_entry_to_work
 * @property bool $gender
 * @property string $nationality
 * @property string $phone
 * @property string $personal_email
 * @property string $company_email
 * @property Carbon $dob
 * @property string $disc
 * @property string cv
 * @property string $motivators
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class BasicInfo extends Model
{
	protected $table = 'basic_info';

	protected $casts = [
		'gender' => 'bool',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool',
		'rank' => 'int',
	];

	// protected $dates = [
	// 	'date_of_entry_to_work',
	// 	'dob'
	// ];

	protected $fillable = [
		'avatar',
		'company',
		'room_id',
		'rank',
		'team_id',
		'position_id',
		'full_name',
		'english_name',
		'employee_code',
		'date_of_entry_to_work',
		'gender',
		'nationality',
		'phone',
		'personal_email',
		'company_email',
		'dob',
		'cv',
		'disc',
		'motivators',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function organizational()
	{
		return $this->belongsTo(Organizational::class, 'id', 'basic_info_id');
	}

	public function organizationalSpecial()
	{
		return $this->hasMany(Organizational::class, 'basic_info_id', 'id');
	}

	public function personInfo()
	{
		return $this->hasOne(PersonalInfo::class, 'basic_info_id', 'id');
	}

	public function relativePriority()
	{
		return $this->hasMany(RelativeInfo::class, 'basic_info_id', 'id')->where('type', 1);
	}
	
	public function relativeChild()
	{
		return $this->hasMany(RelativeInfo::class, 'basic_info_id', 'id')->where('type', 2);
	}

	public function laborContract()
	{
		return $this->hasMany(LaborContract::class, 'basic_info_id', 'id')->where('type', 1);
	}

	public function larborBbt()
	{
		return $this->hasOne(LaborContract::class, 'basic_info_id', 'id')->where('type', 2);
	}

	public function room()
	{
		return $this->hasOne(Room::class, 'id',  'room_id');
	}

	public function team()
	{
		return $this->hasOne(Team::class, 'id', 'team_id');
	}

	public function position()
	{
		return $this->hasOne(Position::class, 'id', 'position_id');
	}

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'created_by');
	}

	public function confirms()
	{
		return $this->hasMany(CriteriaConfirm::class, 'criteria_id');
	}

	public function timekeeps()
	{
		return $this->hasMany(Timekeeping::class, 'basic_info_id');
	}

	public function planColors()
	{
		return $this->hasMany(PlanColor::class, 'basic_info_id');
	}

	// public function user()
	// {
	// 	return $this->hasOne(User::class, 'id', 'created_by');
	// }
}
