<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PersonalInfo
 * 
 * @property int $id
 * @property int $basic_info_id
 * @property string|null $ethnic
 * @property string|null $cccd
 * @property Carbon|null $date_range
 * @property string|null $issued_by
 * @property string|null $permanent_address
 * @property string|null $temporary_residence_address
 * @property string|null $social_insurance_code
 * @property string|null $health_insurance_code
 * @property string|null $healthcare_place
 * @property string|null $personal_income_tax
 * @property string|null $dependent_person
 * @property string|null $details_dependent_person
 * @property string|null $academic_level
 * @property string|null $school
 * @property string|null $specialized
 * @property string|null $number_bank_account
 * @property string|null $bank_name
 * @property string|null $license_plates
 * @property string|null $range_of_vehicle
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class PersonalInfo extends Model
{
	protected $table = 'personal_info';

	protected $casts = [
		'basic_info_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	// protected $dates = [
	// 	'date_range'
	// ];

	protected $fillable = [
		'basic_info_id',
		'ethnic',
		'cccd',
		'date_range',
		'issued_by',
		'permanent_address',
		'temporary_residence_address',
		'social_insurance_code',
		'health_insurance_code',
		'healthcare_place',
		'personal_income_tax',
		'dependent_person',
		'details_dependent_person',
		'academic_level',
		'school',
		'specialized',
		'number_bank_account',
		'bank_name',
		'license_plates',
		'range_of_vehicle',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
