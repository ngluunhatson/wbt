<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanColor extends Model
{
	protected $table = 'plan_colors';

	protected $casts = [
        'basic_info_id' => 'int',
		'created_by'    => 'int',
		'updated_by'    => 'int',
		'delete_flag'   => 'bool'
	];

	protected $fillable = [
        'hex_code',
        'category_name',
        'basic_info_id',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}