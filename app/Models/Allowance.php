<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Allowance
 * 
 * @property int $id
 * @property float|null $main_salary_with_tax
 * @property float|null $money_for_meals_with_tax
 * @property float|null $uniform_with_tax
 * @property float|null $job_performance_with_tax
 * @property float|null $money_for_meals_without_tax
 * @property float|null $uniform_without_tax
 * @property float|null $overtime_salary
 * @property float|null $additional_adjustments
 * @property float|null $adjustment_of_deductions
 * @property float|null $total_income
 * @property float|null $total_taxable_income
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Allowance extends Model
{
	protected $table = 'allowance';

	protected $casts = [
		'main_salary_with_tax' => 'float',
		'money_for_meals_with_tax' => 'float',
		'uniform_with_tax' => 'float',
		'job_performance_with_tax' => 'float',
		'money_for_meals_without_tax' => 'float',
		'uniform_without_tax' => 'float',
		'overtime_salary' => 'float',
		'additional_adjustments' => 'float',
		'adjustment_of_deductions' => 'float',
		'total_income' => 'float',
		'total_taxable_income' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'main_salary_with_tax',
		'money_for_meals_with_tax',
		'uniform_with_tax',
		'job_performance_with_tax',
		'money_for_meals_without_tax',
		'uniform_without_tax',
		'overtime_salary',
		'additional_adjustments',
		'adjustment_of_deductions',
		'total_income',
		'total_taxable_income',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
