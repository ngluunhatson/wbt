<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Salary
 * 
 * @property int $id
 * @property int $room_id
 * @property int $team_id
 * @property int $position_id
 * @property int $basic_info_id
 * @property int $timekeeping_id
 * @property int $income_id
 * @property int $support_id
 * @property int $allowance_id
 * @property int $insurance_id
 * @property int $tax_id
 * @property int $pay_id
 * @property float|null $total_gross
 * @property float|null $work_day
 * @property float|null $other_support
 * @property string|null $note
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Salary extends Model
{
	protected $table = 'salary';

	protected $casts = [
		'salary_id' => 'int',
		'salary_list_id' => 'int',
		'room_id' => 'int',
		'team_id' => 'int',
		'position_id' => 'int',
		'basic_info_id' => 'int',
		'timekeeping_id' => 'int',
		'income_id' => 'int',
		'support_id' => 'int',
		'allowance_id' => 'int',
		'insurance_id' => 'int',
		'tax_id' => 'int',
		'pay_id' => 'int',
		'total_gross' => 'float',
		'work_day' => 'float',
		'other_support' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'salary_id',
		'salary_list_id',
		'room_id',
		'team_id',
		'position_id',
		'basic_info_id',
		'timekeeping_id',
		'income_id',
		'support_id',
		'allowance_id',
		'insurance_id',
		'tax_id',
		'pay_id',
		'official_contract_date',
		'total_gross',
		'work_day',
		'other_support',
		'note',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function organizational()
	{
		return $this->hasOne(Organizational::class, 'basic_info_id', 'basic_info_id');
	}

	public function timekeep()
	{
		return $this->hasOne(Timekeeping::class, 'id', 'timekeeping_id');
	}

	public function income()
	{
		return $this->hasOne(Income::class, 'id', 'income_id');
	}

	public function support()
	{
		return $this->hasOne(Support::class, 'id', 'support_id');
	}

	public function allowance()
	{
		return $this->hasOne(Allowance::class, 'id', 'allowance_id');
	}

	public function insurance()
	{
		return $this->hasOne(Insurance::class, 'id', 'insurance_id');
	}

	public function tax()
	{
		return $this->hasOne(Tax::class, 'id', 'tax_id');
	}

	public function pay()
	{
		return $this->hasOne(Pay::class, 'id', 'pay_id');
	}

	public function staff()
	{
		return $this->hasOne(BasicInfo::class, 'id', 'basic_info_id');
	}

	public function salaryList()
	{
		return $this->belongsTo(SalaryList::class, 'salary_list_id');

	}
}
