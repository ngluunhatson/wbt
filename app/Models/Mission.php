<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Mission
 * 
 * @property int $id
 * @property string|null $purpose
 * @property string|null $core_values
 * @property string|null $community_contribution
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Mission extends Model
{
	protected $table = 'mission';

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'purpose',
		'core_values',
		'community_contribution',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
