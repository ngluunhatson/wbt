<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
/**
 * Class Position
 * 
 * @property int $id
 * @property int $team_id
 * @property string $name
 * @property int|null $amount
 * @property Carbon|null $year
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Position extends Model
{
	protected $table = 'position';
	use NodeTrait;

	protected $casts = [
		'team_id' => 'int',
		'amount' => 'int',
		'parent_id'	=> 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'rank' => 'int',
		'delete_flag' => 'bool',
		'level' => 'int'
	];

	protected $fillable = [
		'team_id',
		'name',
		'amount',
		'year',
		'parent_id',
		'created_by',
		'updated_by',
		'delete_flag',
		'level',
		'rank',
		'flag_chart',
		'flag_filter',
	];

	public function team()
	{
		return $this->belongsTo(Team::class, 'team_id', 'id');
	}

	public function employees()
	{
		return $this->hasMany(Organizational::class, 'position_id', 'id')->whereNotNull('basic_info_id');
	}

	public function childrenPositions()
	{
		return $this->hasMany(Position::class, 'parent_id', 'id')->where('delete_flag', 0);
	}
}
