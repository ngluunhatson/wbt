<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * Class Internship
 * 
 * @property int $id
 * @property int $room_id
 * @property int $team_id
 * @property int $position_id
 * @property int $basic_info_id
 * @property int $confirm_id
 * @property int $status
 * @property int $confirm_id
 * @property Carbon|null $start_date
 * @property Carbon|null $end_date

 * @property string|null $intern_comment
 * @property int|null    $direct_boss_basic_info_id
 * @property string|null $direct_boss_comment
 * @property bool|null   $direct_boss_decision_1 
 * @property float|null  $direct_boss_decision_2
 * @property Carbon|null $direct_boss_decision_3 
 * @property string|null $direct_boss_decision_4
 * @property Carbon|null $direct_boss_review_date
 * @property int|null    $room_manager_basic_info_id
 * @property string|null $room_manager_comment
 * @property Carbon|null $room_manager_review_date
 * @property int|null    $hr_basic_info_id
 * @property string|null $hr_comment
 * @property float|null  $hr_decision_1
 * @property float|null  $hr_decision_2
 * @property float|null  $hr_decision_3
 * @property Carbon|null $hr_decision_4
 * @property string|null $hr_decision_5
 * @property Carbon|null $hr_review_date
 * @property int|null    $ceo_basic_info_id
 * @property Carbon|null $ceo_review_date
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Internship extends Model
{

    protected $table = 'internship';

    protected $casts = [
        'room_id'       => 'int',
		'team_id'       => 'int',
		'position_id'   => 'int',
		'basic_info_id' => 'int',

        'direct_boss_basic_info_id'  => 'int',
        'room_manager_basic_info_id' => 'int',
        'hr_basic_info_id'           => 'int',
        'ceo_basic_info_id'          => 'int',

        'confirm_id'           => 'array',
        'internship_detail_id' => 'array',

        'direct_boss_decision_2' => 'float',
        'hr_decision_3'          => 'float',

        'created_by'    => 'int',
		'updated_by'    => 'int',
		'delete_flag'   => 'bool',

    ];  

    protected $dates = [
        'start_date',
        'end_date',

        'direct_boss_decision_3',
        
        'hr_decision_4',

        'intern_confirm_date',

        'hr_review_date',
        'room_manager_review_date',
        'ceo_review_date',
        'direct_boss_review_date',

	];

    protected $fillable = [
		'room_id',
		'team_id',
		'position_id',
		'basic_info_id',
		'confirm_id',
		'status',
        
        'internship_detail_id',

        'start_date',
        'end_date',

        'intern_comment',
        'intern_confirm_date',

        'direct_boss_basic_info_id',
        'direct_boss_comment',
        'direct_boss_decision_1', //binary
        'direct_boss_decision_2', //direct_boss_decision_month_amount
        'direct_boss_decision_3', //direct_boss_decision_start_date
        'direct_boss_decision_4', //direct_boss_decision_other
        'direct_boss_review_date',

        'room_manager_basic_info_id',
        'room_manager_comment',
        'room_manager_review_date',

        'hr_basic_info_id',
        'hr_comment',
        'hr_decision_1', //hr_decision_salary_amount
        'hr_decision_2', //hr_decision_other_allowance
        'hr_decision_3', //hr_decision_month_amount
        'hr_decision_4', //hr_decision_start_date
        'hr_decision_5', //hr_decision_other
        'hr_review_date',

        'ceo_basic_info_id',
        'ceo_review_date',

        'created_by',
		'updated_by',
		'delete_flag',


	];

    public function user()
	{
		return $this->hasOne(User::class, 'id', 'created_by');
	}

    public function intern() 
	{
		return $this -> hasOne(BasicInfo::class, 'id', 'basic_info_id');
	}

    public function confirms()
	{
		return $this->hasMany(InternshipConfirm::class, 'internship_id');
	}

    public function directBoss() 
	{
		return $this -> hasOne(BasicInfo::class, 'id', 'direct_boss_basic_info_id');
	}

    public function roomManager() 
	{
		return $this -> hasOne(BasicInfo::class, 'id', 'room_manager_basic_info_id');
	}

    public function hr() 
	{
		return $this -> hasOne(BasicInfo::class, 'id', 'hr_basic_info_id');
	}

    public function ceo() 
	{
		return $this -> hasOne(BasicInfo::class, 'id', 'ceo_basic_info_id');
	}

    public function internshipDetails(){
        return $this -> hasMany(InternshipDetail::class, 'internship_id');
    }

}