<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InternshipDetail
 * 
 * @property int $id
 * @property int|null    $internship_id
 * @property int|null    $part_number
 * @property string|null $section_1
 * @property string|null $section_2
 * @property string|null $section_3
 * @property string|null $section_4
 * @property string|null $section_5
 * @property string|null $section_6
 * @property string|null $section_7
 * @property string|null $section_8
 * @property string|null $section_9
 * @property string|null $section_10
 * @property string|null $section_11
 * @property string|null $section_12
 * @property string|null $section_13
 * @property string|null $section_14
 * @property string|null $section_15
 * @property string|null $section_16
 * @property string|null $section_17
 * @property string|null $section_18
 * @property string|null $section_19
 * @property string|null $section_20
 * @property string|null $section_21
 * @property string|null $section_22
 * @property string|null $section_23
 * @property string|null $section_24
 * @property string|null $section_25
 * @property string|null $section_26
 * @property string|null $section_27 
 * @property string|null $section_28
 * @property string|null $section_29
 * @property Carbon|null $created_at
 * @property int         $created_by
 * @property Carbon|null $updated_at
 * @property int         $updated_by
 * @property bool        $delete_flag
 *
 * @package App\Models
 */
class InternshipDetail extends Model
{

    protected $table = 'internship_detail';

    protected $casts = [
        'internship_id' => 'int',
        'part_number'   => 'int',

        'section_title_array'   => 'array',
        'section_1'     => 'array',
        'section_2'     => 'array',
        'section_3'     => 'array',
        'section_4'     => 'array',
        'section_5'     => 'array',
        'section_6'     => 'array',
        'section_7'     => 'array',
        'section_8'     => 'array',
        'section_9'     => 'array',
        'section_10'    => 'array',
        'section_11'    => 'array',
        'section_12'    => 'array',
        'section_13'    => 'array',
        'section_14'    => 'array',
        'section_15'    => 'array',
        'section_16'    => 'array',
        'section_17'    => 'array',
        'section_18'    => 'array',
        'section_19'    => 'array',
        'section_20'    => 'array',
        'section_21'    => 'array',
        'section_22'    => 'array',
        'section_23'    => 'array',
        'section_24'    => 'array',
        'section_25'    => 'array',
        'section_26'    => 'array',
        'section_27'    => 'array',
        'section_28'    => 'array',
        'section_29'    => 'array',

        'created_by'    => 'int',
        'updated_by'    => 'int',
        'delete_flag'   => 'bool',
        

    ];


    protected $fillable = [
        'internship_id',
        'part_number',
        'part_title',
        'section_title_array',
        
        'section_1',
        'section_2',
        'section_3',
        'section_4',
        'section_5',
        'section_6',
        'section_7',
        'section_8',
        'section_9',
        'section_10',
        'section_11',
        'section_12',
        'section_13',
        'section_14',
        'section_15',
        'section_16',
        'section_17',
        'section_18',
        'section_19',
        'section_20',
        'section_21',
        'section_22',
        'section_23',
        'section_24',
        'section_25',
        'section_26',
        'section_27',
        'section_28',
        'section_29',
        'created_by',
        'updated_by',
        'delete_flag',

    ];

}
