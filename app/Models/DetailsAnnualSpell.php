<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DetailsAnnualSpell
 * 
 * @property int $id
 * @property int $annual_spells_id
 * @property float|null $january
 * @property float|null $february
 * @property float|null $march
 * @property float|null $april
 * @property float|null $may
 * @property float|null $june
 * @property float|null $july
 * @property float|null $august
 * @property float|null $september
 * @property float|null $october
 * @property float|null $november
 * @property float|null $december
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class DetailsAnnualSpell extends Model
{
	protected $table = 'details_annual_spells';

	protected $casts = [
		'annual_spells_id' => 'int',
		'january' => 'float',
		'february' => 'float',
		'march' => 'float',
		'april' => 'float',
		'may' => 'float',
		'june' => 'float',
		'july' => 'float',
		'august' => 'float',
		'september' => 'float',
		'october' => 'float',
		'november' => 'float',
		'december' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'annual_spells_id',
		'january',
		'february',
		'march',
		'april',
		'may',
		'june',
		'july',
		'august',
		'september',
		'october',
		'november',
		'december',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
