<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PlanQuarter
 * 
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class PlanQuarter extends Model
{
    protected $table = 'plan_quarter';

    protected $casts = [
        'room_id'       => 'int',
        'team_id'       => 'int',
        'position_id'   => 'int',
        'basic_info_id' => 'int',
        'plan_week_ids' => 'array',
        'year'          => 'int',
        'quarter'       => 'int',
        'created_by'    => 'int',
        'updated_by'    => 'int',
        'delete_flag'   => 'bool'
    ];

    protected $dates = [
        'start_date',
        'end_date'
    ];

    protected $fillable = [
        'room_id',
        'team_id',
        'position_id',
        'basic_info_id',
        'plan_week_ids',
        'quarter_goal',
        'year',
        'quarter',
        'start_date',
        'end_date',
        'created_by',
        'updated_by',
        'delete_flag',
    ];

    public function planWeeks()
    {
        return $this->hasMany(PlanWeek::class, 'plan_quarter_id')->where('delete_flag', 0);;
    }

    public function staff()
    {
        return $this->hasOne(BasicInfo::class, 'id', 'basic_info_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
