<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Pay
 * 
 * @property int $id
 * @property float|null $company_account_1
 * @property float|null $personal_account_1
 * @property float|null $total
 * @property float|null $company_account_2
 * @property float|null $personal_account_2
 * @property float|null $company_account_3
 * @property float|null $personal_account_3
 * @property float|null $company_account_4
 * @property float|null $personal_account_4
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Pay extends Model
{
	protected $table = 'pay';

	protected $casts = [
		'company_account_1' => 'float',
		'personal_account_1' => 'float',
		'total' => 'float',
		'company_account_2' => 'float',
		'personal_account_2' => 'float',
		'company_account_3' => 'float',
		'personal_account_3' => 'float',
		'company_account_4' => 'float',
		'personal_account_4' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'company_account_1',
		'personal_account_1',
		'total',
		'company_account_2',
		'personal_account_2',
		'company_account_3',
		'personal_account_3',
		'company_account_4',
		'personal_account_4',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
