<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PlanWeek
 * 
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class PlanWeek extends Model
{
    protected $table = 'plan_week';

    protected $casts = [
        'plan_quarter_id'       => 'int',
        'room_id'               => 'int',
        'team_id'               => 'int',
        'position_id'           => 'int',
        'basic_info_id'         => 'int',
        'plan_day_ids'          => 'array',
        'five_weekly_goals'     => 'array',
        'five_weekly_aspects'   => 'array',
        'year'                  => 'int',
        'quarter'               => 'int',
        'week_in_quarter'       => 'int',
        'start_time'            => 'float',
        'end_time'              => 'float',
        'created_by'            => 'int',
        'updated_by'            => 'int',
        'delete_flag'           => 'bool'
    ];

    protected $dates = [
        'start_date',
        'end_date'
    ];

    protected $fillable = [
        'plan_quarter_id',
        'room_id',
        'team_id',
        'position_id',
        'basic_info_id',
        'plan_day_ids',
        'five_weekly_goals',
        'five_weekly_aspects',
        'year',
        'quarter',
        'week_in_quarter',
        'start_date',
        'end_date',
        'created_by',
        'updated_by',
        'delete_flag',
    ];

    public function planDays()
    {
        return $this->hasMany(PlanDay::class, 'plan_week_id')->where('delete_flag', 0);;
    }

    public function staff()
    {
        return $this->hasOne(BasicInfo::class, 'id', 'basic_info_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
