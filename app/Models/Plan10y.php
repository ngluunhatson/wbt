<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Plan10y
 * 
 * @property int $id
 * @property int $opportunity_id
 * @property int $business_development_id
 * @property int $personnel_id
 * @property int $enforcement_id
 * @property int $mission_id
 * @property string|null $full_name
 * @property string|null $company
 * @property string|null $position
 * @property Carbon|null $from_year
 * @property Carbon|null $to_year
 * @property string|null $ten_year_goal
 * @property string|null $three_year_goal
 * @property string|null $priority_in_three_years
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Plan10y extends Model
{
	protected $table = 'plan_10y';

	protected $casts = [
		'opportunity_id' => 'int',
		'business_development_id' => 'int',
		'personnel_id' => 'int',
		'enforcement_id' => 'int',
		'mission_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $dates = [
		'from_year',
		'to_year'
	];

	protected $fillable = [
		'opportunity_id',
		'business_development_id',
		'personnel_id',
		'enforcement_id',
		'mission_id',
		'full_name',
		'company',
		'position',
		'from_year',
		'to_year',
		'ten_year_goal',
		'three_year_goal',
		'priority_in_three_years',
	];

	public function opportunity()
    {
        return $this->hasOne(Opportunity::class, 'id', 'opportunity_id');
    }

	public function businessDevelopment()
    {
        return $this->hasOne(BusinessDevelopment::class,'id' ,'business_development_id');
    }
	public function personnel()
    {
        return $this->hasOne(Personnel::class, 'id', 'personnel_id');
    }
	public function enforcement()
    {
        return $this->hasOne(Enforcement::class, 'id', 'enforcement_id');
    }

	public function mission()
    {
        return $this->hasOne(Mission::class, 'id', 'mission_id');
    }
}
