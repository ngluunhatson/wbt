<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tax
 * 
 * @property int $id
 * @property float|null $taxable_income
 * @property float|null $progressive
 * @property float|null $tax_10
 * @property float|null $tax_20
 * @property float|null $total
 * @property float|null $other_recoveries
 * @property float|null $other_refunds
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Tax extends Model
{
	protected $table = 'tax';

	protected $casts = [
		'taxable_income' => 'float',
		'progressive' => 'float',
		'tax_10' => 'float',
		'tax_20' => 'float',
		'total' => 'float',
		'other_recoveries' => 'float',
		'other_refunds' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'taxable_income',
		'progressive',
		'tax_10',
		'tax_20',
		'total',
		'other_recoveries',
		'other_refunds',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
