<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Kpi
 * 
 * @property int $id
 * @property int $room_id
 * @property int $team_id
 * @property int $position_id
 * @property int $basic_info_id
 * @property int $confirm_id
 * @property string|null $quarter
 * @property Carbon|null $year
 * @property Carbon|null $delivery_date
 * @property Carbon|null $review_date
 * @property int|null $status
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Kpi extends Model
{
	protected $table = 'kpis';

	protected $casts = [
		'room_id' => 'int',
		'team_id' => 'int',
		'position_id' => 'int',
		'basic_info_id' => 'int',
		'confirm_id' => 'array',
		'status' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $dates = [
		'delivery_date',
		'review_date'
	];

	protected $fillable = [
		'room_id',
		'team_id',
		'position_id',
		'basic_info_id',
		'confirm_id',
		'quarter',
		'year',
		'delivery_date',
		'review_date',
		'status',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function room()
	{
		return $this->hasOne(Room::class, 'id',  'room_id');
	}

	public function team()
	{
		return $this->hasOne(Team::class, 'id', 'team_id');
	}

	public function position()
	{
		return $this->hasOne(Position::class, 'id', 'position_id');
	}

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'created_by');
	}

	public function confirms()
	{
		return $this->hasMany(KpiConfirm::class, 'kpi_id');
	}
}
