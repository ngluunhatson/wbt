<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Confirm
 * 
 * @property int $id
 * @property int $basic_info_id
 * @property string|null $assignment_type
 * @property string|null $status
 * @property string|null $note
 * @property Carbon|null $sign_day
 * @property string|null $proposal_type
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Confirm extends Model
{
	protected $table = 'confirm';
	protected $guarded = ['id'];

	protected $casts = [
		'basic_info_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $dates = [
		'sign_day'
	];

	protected $fillable = [
		'basic_info_id',
		'assignment_type',
		'type_confirm',
		'status',
		'note',
		'sign_day',
		'proposal_type',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function staff()
	{
		return $this->hasOne(BasicInfo::class, 'id', 'basic_info_id');
	}
}
