<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InterviewScore
 * 
 * @property int $id
 * @property int $confirm_id
 * @property int $interview_review_id
 * @property Carbon|null $interview_date
 * @property bool|null $expertise
 * @property bool|null $work_experience
 * @property bool|null $problem_solving
 * @property bool|null $plan
 * @property bool|null $orientation
 * @property bool|null $commitment_value
 * @property bool|null $communicate
 * @property bool|null $team_work
 * @property bool|null $english_level
 * @property bool|null $it_skills
 * @property bool|null $psychological_control
 * @property bool|null $attitude
 * @property string|null $note
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class InterviewScore extends Model
{
	protected $table = 'interview_score';

	protected $casts = [
		'confirm_id' => 'array',
		'interview_review_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $dates = [
		'interview_date'
	];

	protected $fillable = [
		'confirm_id',
		'interview_review_id',
		'interview_date',
		'expertise',
		'work_experience',
		'problem_solving',
		'plan',
		'orientation',
		'commitment_value',
		'communicate',
		'team_work',
		'english_level',
		'it_skills',
		'psychological_control',
		'attitude',
		'note',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function confirm()
	{
		return $this->hasOne(Confirm::class, 'id', 'confirm_id');
	}

	public function getInterviewDateAttribute($value) {
		if (!empty($value)) {
			return date('d/m/Y', strtotime($value));
		}
	}
}
