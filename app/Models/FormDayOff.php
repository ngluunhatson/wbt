<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DayOff
 * 
 * @property int $id
 * @property int $basic_info_id
 * @property int $confirm_id
 * @property int|null $status
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 * @package App\Models
 */
class FormDayOff extends Model
{
	protected $table = 'form_day_off';

	protected $casts = [

        'status' => 'int',
		'basic_info_id' => 'int',
		'confirm_id' => 'array',
		'detail_id' => 'array',
		'total_day_off_asked_for' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $dates = [
		'review_date',
        'start_date',
		'end_date',
	];

	protected $fillable = [
		'basic_info_id',
		'confirm_id',
		'detail_id',
		'status',
		'total_day_off_asked_for',
        'review_date',
		'created_by',
		'updated_by',
		'delete_flag',
	];


	public function basicInfo() 
	{
		return $this -> hasOne(BasicInfo::class, 'id', 'basic_info_id');
	}

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'created_by');
	}

	public function confirms()
	{
		return $this->hasMany(FormDayOffConfirm::class, 'form_day_off_id');
	}

	public function detailForms()
	{
		return $this->hasMany(FormDayOffDetail::class, 'form_day_off_id');
	}

}
