<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AnnualSpell
 * 
 * @property int $id
 * @property int $room_id
 * @property int $team_id
 * @property int $position_id
 * @property int $basic_info_id
 * @property float|null $spell_left_last_year
 * @property float|null $spell_used
 * @property string|null $note
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class AnnualSpell extends Model
{
	protected $table = 'annual_spells';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'room_id' => 'int',
		'team_id' => 'int',
		'position_id' => 'int',
		'basic_info_id' => 'int',
		'spell_left_last_year' => 'float',
		'spell_used' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'room_id',
		'team_id',
		'position_id',
		'basic_info_id',
		'spell_left_last_year',
		'spell_used',
		'note',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
