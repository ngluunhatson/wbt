<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Probationary
 * 
 * @property int $id
 * @property int $confirm_id
 * @property float|null $total_points_nv
 * @property float|null $total_points_gv
 * @property string|null $result
 * @property string|null $status
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Probationary extends Model
{
	protected $table = 'probationary';

	protected $casts = [
		'confirm_id' => 'array',
		'total_points_nv' => 'float',
		'total_points_gv' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'confirm_id',
		'total_points_nv',
		'total_points_gv',
		'result',
		'status',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
