<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Enforcement
 * 
 * @property int $id
 * @property string|null $my_kpis
 * @property string|null $team_kpis
 * @property string|null $personal_work_schedule
 * @property string|null $team_work_schedule
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Enforcement extends Model
{
	protected $table = 'enforcement';

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'my_kpis',
		'team_kpis',
		'personal_work_schedule',
		'team_work_schedule',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
