<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Room
 * 
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Room extends Model
{
	protected $table = 'room';

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool',
		'bod' => 'bool'
	];

	protected $fillable = [
		'name',
		'bod',
		'room_order',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function teams()
	{
		return $this->hasMany(Team::class, 'room_id', 'id')->where('delete_flag', 0);
	}

}
