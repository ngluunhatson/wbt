<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RelativeInfo
 * 
 * @property int $id
 * @property int $basic_info_id
 * @property string|null $full_name_priority
 * @property string|null $relationship
 * @property string|null $phone_number
 * @property string|null $full_name_child
 * @property bool|null $gender
 * @property Carbon|null $dob
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class RelativeInfo extends Model
{
	protected $table = 'relative_info';

	protected $casts = [
		'basic_info_id' => 'int',
		'gender' => 'bool',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	// protected $dates = [
	// 	'dob'
	// ];

	protected $fillable = [
		'basic_info_id',
		'full_name_priority',
		'relationship',
		'phone_number',
		'full_name_child',
		'gender',
		'dob',
		'type',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
