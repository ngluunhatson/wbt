<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Support
 * 
 * @property int $id
 * @property float|null $main_salary
 * @property float|null $food_support
 * @property float|null $support_uniforms
 * @property float|null $job_performance
 * @property float|null $total
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Support extends Model
{
	protected $table = 'support';

	protected $casts = [
		'main_salary' => 'float',
		'food_support' => 'float',
		'support_uniforms' => 'float',
		'job_performance' => 'float',
		'total' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'main_salary',
		'food_support',
		'support_uniforms',
		'job_performance',
		'total',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
