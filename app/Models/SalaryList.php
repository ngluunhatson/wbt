<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryList extends Model
{
	protected $table = 'salary_list';

	protected $casts = [
		'salary_ids'    => 'array',
        'confirm_ids'   => 'array',
        'status'        => 'int',
		'created_by'    => 'int',
		'updated_by'    => 'int',
		'delete_flag'   => 'bool'
	];

	protected $dates = [
		'review_date',
	];

	protected $fillable = [
        'salary_ids',
        'confirm_ids',
        'status',
        'year',
        'month',
		'review_date',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function salaries()
	{
		return $this->hasMany(Salary::class, 'salary_list_id', 'id')->where('delete_flag', 0);
	}

	public function salaryConfirms()
	{
		return $this->hasMany(SalaryListConfirm::class, 'salary_list_id');

	}

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'created_by');

	}

	public function salarySetting()
	{
		return $this->hasOne(SalarySetting::class, 'salary_list_id');
	}
}