<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InterviewReview
 * 
 * @property int $id
 * @property int $room_id
 * @property int $team_id
 * @property int $position_id
 * @property int $confirm_id
 * @property string|null $candidates_name
 * @property int|null $specialize
 * @property string|null $disc
 * @property float|null $salary
 * @property string|null $status
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class InterviewReview extends Model
{
	protected $table = 'interview_review';

	protected $casts = [
		'room_id' => 'int',
		'team_id' => 'int',
		'position_id' => 'int',
		'confirm_id' => 'array',
		'specialize' => 'int',
		'salary' => 'float',
		'disc' => 'array',
		'status' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'room_id',
		'team_id',
		'position_id',
		'confirm_id',
		'candidates_name',
		'specialize',
		'disc',
		'salary',
		'status',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function room()
	{
		return $this->hasOne(Room::class, 'id',  'room_id');
	}

	public function team()
	{
		return $this->hasOne(Team::class, 'id', 'team_id');
	}

	public function position()
	{
		return $this->hasOne(Position::class, 'id', 'position_id');
	}

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'created_by');
	}

	public function interviewScores()
	{
		return $this->hasMany(InterviewScore::class, 'interview_review_id');
	}
}
