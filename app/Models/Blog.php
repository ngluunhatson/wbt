<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    // use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
                'maxLength'          => null,
                'maxLengthKeepWords' => true,
                'method'             => null,
                'separator'          => '-',
                'unique'             => true,
                'uniqueSuffix'       => null,
                'includeTrashed'     => false,
                'reserved'           => null,
                'onUpdate'           => false
            ]
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // 'language_id',
        'category_id',
        // 'category_name',
        'title',
        'content',
        'blog_image',
        'author',
        'created_by',
        'slug',
        'view',
        'status',
        'tag',
        'created_at',
        'updated_at',
        'updated_by',
        'delete_flag',
        'room_id',
        'team_id',
        'position_id',
        'basic_info_id',
        'all_company',
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id','id');
    }

    public function comments() {
        return $this->hasMany('App\Models\Comment', 'blog_id', 'id');
    }


}
