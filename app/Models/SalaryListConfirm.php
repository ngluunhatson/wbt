<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SalaryList;

class SalaryListConfirm extends Model
{
    protected $table = 'salary_list_confirms';
    protected $guarded = ['id'];
    public function confirm(){
        return $this->belongsTo(Confirm::class, 'confirm_id');
    }

    public function salaryList(){
        return $this->belongsTo(SalaryList::class, 'salary_list_id');
    }

}