<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PlanDay
 * 
 * 
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class PlanDay extends Model
{
    protected $table = 'plan_day';

    protected $casts = [
        'plan_week_id'          => 'int',
        'room_id'               => 'int',
        'team_id'               => 'int',
        'position_id'           => 'int',
        'basic_info_id'         => 'int',
        'plan_day_timeslot_ids' => 'array',
        'three_new_habits'      => 'array',
        'year'                  => 'int',
        'quarter'               => 'int',
        'week_in_quarter'       => 'int',
        'day_in_week'           => 'int',
        'start_time'            => 'float',
        'end_time'              => 'float',
        'created_by'            => 'int',
        'updated_by'            => 'int',
        'delete_flag'           => 'bool'
    ];

    protected $dates = [
        'date'
    ];

    protected $fillable = [
        'plan_week_id',
        'room_id',
        'team_id',
        'position_id',
        'basic_info_id',
        'plan_day_timeslot_ids',
        'three_new_habits',
        'year',
        'quarter',
        'week_in_quarter',
        'day_in_week',
        'date',
        'lesson_main',
        'lesson_failure',
        'lesson_success',
        'lesson_thankful',
        'created_by',
        'updated_by',
        'delete_flag',
    ];

    public function planDayTimeslots()
    {
        return $this->hasMany(PlanDayTimeslot::class, 'plan_day_id')->where('delete_flag', 0);
    }

    public function staff()
    {
        return $this->hasOne(BasicInfo::class, 'id', 'basic_info_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
