<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PlanDayTimeslot
 * 

 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class PlanDayTimeslot extends Model
{
    protected $table = 'plan_day_timeslot';

    protected $casts = [
        'plan_day_id'   => 'int',
        'room_id'       => 'int',
        'team_id'       => 'int',
        'position_id'   => 'int',
        'basic_info_id' => 'int',
        'start_time'    => 'float',
        'end_time'      => 'float',
        'created_by'    => 'int',
        'updated_by'    => 'int',
        'isDone'        => 'bool',
        'delete_flag'   => 'bool'
    ];

    protected $dates = [
        'date'
    ];

    protected $fillable = [
        'plan_day_id',
        'room_id',
        'team_id',
        'position_id',
        'basic_info_id',
        'date',
        'start_time',
        'end_time',
        'title',
        'location',
        'description',
        'isDone',
        'plan_color_id',
        'created_by',
        'updated_by',
        'delete_flag',
    ];

    public function staff()
    {
        return $this->hasOne(BasicInfo::class, 'id', 'basic_info_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function planColor() {
        return $this->hasOne(PlanColor::class, 'id', 'plan_color_id')->where('delete_flag', 0);
    }
}
