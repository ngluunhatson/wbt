<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Insurance
 * 
 * @property int $id
 * @property float|null $reduce_yourself
 * @property float|null $number_of_dependents
 * @property float|null $dependent_person
 * @property float|null $social_insurance_nv
 * @property float|null $health_insurance_nv
 * @property float|null $unemployment_insurance_nv
 * @property float|null $total_nv
 * @property float|null $social_insurance_cty
 * @property float|null $health_insurance_cty
 * @property float|null $unemployment_insurance_cty
 * @property float|null $occupational_accident_insurance
 * @property float|null $total_cty
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Insurance extends Model
{
	protected $table = 'insurance';

	protected $casts = [
		'reduce_yourself' => 'float',
		'number_of_dependents' => 'float',
		'dependent_person' => 'float',
		'social_insurance_nv' => 'float',
		'health_insurance_nv' => 'float',
		'unemployment_insurance_nv' => 'float',
		'total_nv' => 'float',
		'social_insurance_cty' => 'float',
		'health_insurance_cty' => 'float',
		'unemployment_insurance_cty' => 'float',
		'occupational_accident_insurance' => 'float',
		'total_cty' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'reduce_yourself',
		'number_of_dependents',
		'dependent_person',
		'social_insurance_nv',
		'health_insurance_nv',
		'unemployment_insurance_nv',
		'total_nv',
		'social_insurance_cty',
		'health_insurance_cty',
		'unemployment_insurance_cty',
		'occupational_accident_insurance',
		'total_cty',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
