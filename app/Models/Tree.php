<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Tree extends Model
{
	use NodeTrait;
	protected $table = 'tree';

    protected $fillable = [
		'title',
		'description',
		'img',
		'parent_id',
        'hide'
	];
}
