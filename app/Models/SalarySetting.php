<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SalarySetting extends Model
{
	protected $table = 'salary_settings';

    protected $casts = [
		'salary_list_id' => 'int',
		'input_column_27' => 'float',
		'input_column_29' => 'float',
		'input_column_30' => 'float',
		'input_column_31' => 'float',
		'input_column_32' => 'float',
		'input_column_34' => 'float',
		'input_column_35' => 'float',
		'input_column_36' => 'float',
		'input_column_37' => 'float',
		'salary_base' => 'float',
		'salary_min' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

    protected $fillable = [
		'salary_list_id',
		'input_column_27',
		'input_column_29',
		'input_column_30',
		'input_column_31',
		'input_column_32',
		'input_column_34',
		'input_column_35',
		'input_column_36',
		'input_column_37',
		'salary_base',
		'salary_min',
		'created_by',
		'updated_by',
		'delete_flag'
	];

    public function salaryList(){
        return $this->belongsTo(SalaryList::class, 'salary_list_id');
    }
}
