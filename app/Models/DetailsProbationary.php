<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DetailsProbationary
 * 
 * @property int $id
 * @property int $probationary_id
 * @property string|null $label
 * @property string|null $execution_time
 * @property string|null $learned
 * @property string|null $applied
 * @property string|null $nv_assessment
 * @property string|null $gv_designer
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class DetailsProbationary extends Model
{
	protected $table = 'details_probationary';

	protected $casts = [
		'probationary_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'probationary_id',
		'label',
		'execution_time',
		'learned',
		'applied',
		'nv_assessment',
		'gv_designer',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
