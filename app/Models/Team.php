<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Team
 * 
 * @property int $id
 * @property int $room_id
 * @property string $name
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Team extends Model
{
	protected $table = 'team';

	protected $casts = [
		'room_id' => 'int',
		'is_main'	  => 'bool',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool',
	];

	protected $fillable = [
		'room_id',
		'name',
		'is_main',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function positions()
	{
		return $this->hasMany(Position::class, 'team_id', 'id')->where('delete_flag', 0);
	}

	public function room()
	{
		return $this->belongsTo(Room::class, 'room_id', 'id')->where('delete_flag', 0);
	}
}
