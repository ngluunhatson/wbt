<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InternshipConfirm extends Model
{
    protected $table = 'internship_confirms';
    protected $guarded = ['id'];
    public function confirm(){
        return $this->belongsTo(Confirm::class, 'confirm_id');
    }

    public function confirmReceive() {
        return $this->belongsTo(Confirm::class, 'confirm_id')->where('confirm.type_confirm', 1);
    }
}
