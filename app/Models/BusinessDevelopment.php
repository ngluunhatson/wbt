<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BusinessDevelopment
 * 
 * @property int $id
 * @property string|null $core_customers
 * @property string|null $strategy_in_one_sentence
 * @property string|null $one_year_goal
 * @property string|null $priorities_for_one_year
 * @property string|null $quarterly_goals
 * @property string|null $priorities_for_the_quarter
 * @property string|null $brand_promise
 * @property string|null $customer_standards
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class BusinessDevelopment extends Model
{
	protected $table = 'business_development';

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'core_customers',
		'strategy_in_one_sentence',
		'one_year_goal',
		'priorities_for_one_year',
		'quarterly_goals',
		'priorities_for_the_quarter',
		'brand_promise',
		'customer_standards',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
