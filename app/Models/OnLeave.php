<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OnLeave
 * 
 * @property int $id
 * @property int $room_id
 * @property int $team_id
 * @property int $position_id
 * @property int $basic_info_id
 * @property int $annual_spells_id
 * @property int $confirm_id
 * @property string|null $spell_type
 * @property string|null $reason
 * @property string|null $status
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class OnLeave extends Model
{
	protected $table = 'on_leave';

	protected $casts = [
		'room_id' => 'int',
		'team_id' => 'int',
		'position_id' => 'int',
		'basic_info_id' => 'int',
		'annual_spells_id' => 'int',
		'confirm_id' => 'array',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'room_id',
		'team_id',
		'position_id',
		'basic_info_id',
		'annual_spells_id',
		'confirm_id',
		'spell_type',
		'reason',
		'status',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
