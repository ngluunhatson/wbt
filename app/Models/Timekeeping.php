<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Timekeeping
 * 
 * @property int $id
 * @property int $room_id
 * @property int $team_id
 * @property int $position_id
 * @property int $basic_info_id
 * @property Carbon|null $month
 * @property Carbon|null $year
 * @property int|null $day_1
 * @property int|null $day_2
 * @property int|null $day_3
 * @property int|null $day_4
 * @property int|null $day_5
 * @property int|null $day_6
 * @property int|null $day_7
 * @property int|null $day_8
 * @property int|null $day_9
 * @property int|null $day_10
 * @property int|null $day_11
 * @property int|null $day_12
 * @property int|null $day_13
 * @property int|null $day_14
 * @property int|null $day_15
 * @property int|null $day_16
 * @property int|null $day_17
 * @property int|null $day_18
 * @property int|null $day_19
 * @property int|null $day_20
 * @property int|null $day_21
 * @property int|null $day_22
 * @property int|null $day_23
 * @property int|null $day_24
 * @property int|null $day_25
 * @property int|null $day_26
 * @property int|null $day_27
 * @property int|null $day_28
 * @property int|null $day_29
 * @property int|null $day_30
 * @property int|null $day_31
 * @property float|null $with_salary
 * @property float|null $paid_holidays
 * @property float|null $no_salary
 * @property float|null $other
 * @property float|null $total_salary
 * @property string|null $note
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Timekeeping extends Model
{
	protected $table = 'timekeeping';

	protected $casts = [
		'room_id' => 'int',
		'team_id' => 'int',
		'position_id' => 'int',
		'basic_info_id' => 'int',
		'day_1' => 'int',
		'day_2' => 'int',
		'day_3' => 'int',
		'day_4' => 'int',
		'day_5' => 'int',
		'day_6' => 'int',
		'day_7' => 'int',
		'day_8' => 'int',
		'day_9' => 'int',
		'day_10' => 'int',
		'day_11' => 'int',
		'day_12' => 'int',
		'day_13' => 'int',
		'day_14' => 'int',
		'day_15' => 'int',
		'day_16' => 'int',
		'day_17' => 'int',
		'day_18' => 'int',
		'day_19' => 'int',
		'day_20' => 'int',
		'day_21' => 'int',
		'day_22' => 'int',
		'day_23' => 'int',
		'day_24' => 'int',
		'day_25' => 'int',
		'day_26' => 'int',
		'day_27' => 'int',
		'day_28' => 'int',
		'day_29' => 'int',
		'day_30' => 'int',
		'day_31' => 'int',
		'with_salary' => 'float',
		'paid_holidays' => 'float',
		'no_salary' => 'float',
		'other' => 'float',
		'total_salary' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'room_id',
		'team_id',
		'position_id',
		'basic_info_id',
		'month',
		'year',
		'day_1',
		'day_2',
		'day_3',
		'day_4',
		'day_5',
		'day_6',
		'day_7',
		'day_8',
		'day_9',
		'day_10',
		'day_11',
		'day_12',
		'day_13',
		'day_14',
		'day_15',
		'day_16',
		'day_17',
		'day_18',
		'day_19',
		'day_20',
		'day_21',
		'day_22',
		'day_23',
		'day_24',
		'day_25',
		'day_26',
		'day_27',
		'day_28',
		'day_29',
		'day_30',
		'day_31',
		'with_salary',
		'paid_holidays',
		'no_salary',
		'other',
		'total_salary',
		'note',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function organizational()
	{
		return $this->hasOne(Organizational::class, 'basic_info_id', 'basic_info_id');
	}

	public function organizationalSpecial()
	{
		return $this->hasMany(Organizational::class, 'basic_info_id', 'basic_info_id')->where(function($query) {
			return $query->with('position')->whereHas('position', function($query) {
				return $query->where('rank', '<>',9);
			});
		});
	}

	public function staff()
	{
		return $this->belongsTo(BasicInfo::class, 'basic_info_id');
	}

	public function salary() {
		return $this->hasOne(Salary::class,'timekeeping_id');
	}
}
