<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DetailsKpi
 * 
 * @property int $id
 * @property int $kpis_id
 * @property string|null $categories
 * @property string|null $content_kpis
 * @property float|null $proportion
 * @property string|null $reality
 * @property float|null $result
 * @property string|null $note
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class DetailsKpi extends Model
{
	protected $table = 'details_kpis';

	protected $casts = [
		'kpis_id' => 'int',
		'proportion' => 'float',
		'result' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'kpis_id',
		'categories',
		'content_kpis',
		'proportion',
		'reality',
		'result',
		'note',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
