<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DetailsDayOff
 * 
 * @property int $id
 * @property int $day_off_id
 * @property Carbon|null $start_date
 * @property Carbon|null $start_time
 * @property Carbon|null $end_date
 * @property Carbon|null $end_time
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class DetailsDayOff extends Model
{
	protected $table = 'details_day_off';

	protected $casts = [
		'total_day_off' 			  => 'float',
		'number_of_tracking_day_offs' => 'float',
		'created_by' 			      => 'int',
		'updated_by' 				  => 'int',
		'delete_flag' 				  => 'bool'
	];

	protected $dates = [
		'start_date',
		'end_date',
	];

	protected $fillable = [
		'start_date',
		'start_time',
		'end_date',
		'end_time',
		'total_day_off',
		'number_of_tracking_day_offs',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
