<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DetailsOnLeave
 * 
 * @property int $id
 * @property int $on_leave_id
 * @property Carbon|null $start_date
 * @property Carbon|null $start_time
 * @property Carbon|null $end_date
 * @property Carbon|null $end_time
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class DetailsOnLeave extends Model
{
	protected $table = 'details_on_leave';

	protected $casts = [
		'on_leave_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $dates = [
		'start_date',
		'start_time',
		'end_date',
		'end_time'
	];

	protected $fillable = [
		'on_leave_id',
		'start_date',
		'start_time',
		'end_date',
		'end_time',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
