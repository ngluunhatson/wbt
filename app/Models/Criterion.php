<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Criterion
 * 
 * @property int $id
 * @property int $room_id
 * @property int $team_id
 * @property int $position_id
 * @property int $confirm_id
 * @property string|null $location
 * @property string|null $target
 * @property string|null $recruitment_requirements
 * @property string|null $capability_requirements
 * @property string|null $function
 * @property string|null $mission
 * @property string|null $power
 * @property string|null $wage
 * @property string|null $kpis
 * @property string|null $bonus
 * @property string|null $disc
 * @property string|null $report
 * @property int|null $status
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Criterion extends Model
{
	protected $table = 'criteria';
	protected $guarded = ['id'];

	protected $casts = [
		'room_id' => 'int',
		'team_id' => 'int',
		'position_id' => 'int',
		'confirm_id' => 'int',
		'status' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'room_id',
		'team_id',
		'position_id',
		'confirm_id',
		'location',
		'target',
		'recruitment_requirements',
		'capability_requirements',
		'function',
		'mission',
		'power',
		'wage',
		'kpis',
		'bonus',
		'disc',
		'report',
		'status',
		'review_date',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function room()
	{
		return $this->hasOne(Room::class, 'id',  'room_id');
	}

	public function team()
	{
		return $this->hasOne(Team::class, 'id', 'team_id');
	}

	public function position()
	{
		return $this->hasOne(Position::class, 'id', 'position_id');
	}

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'created_by');
	}

	public function confirms()
	{
		return $this->hasMany(CriteriaConfirm::class, 'criteria_id');
	}
}
