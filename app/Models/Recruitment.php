<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Recruitment
 * 
 * @property int $id
 * @property int $room_id
 * @property int $team_id
 * @property int $position_id
 * @property int $confirm_id
 * @property int|null $number_vacancies
 * @property string|null $recruitment_purpose
 * @property string|null $reason
 * @property Carbon|null $recruitment_time
 * @property string|null $type_work
 * @property string|null $manpower_demarcation
 * @property string|null $age
 * @property string|null $gender
 * @property float|null $experience_year
 * @property string|null $academic_level
 * @property string|null $academic_level_other
 * @property string|null $qualification
 * @property string|null $job_duties
 * @property string|null $disc
 * @property string|null $english_level
 * @property string|null $computer_skill
 * @property string|null $device_requirements
 * @property string|null $other_requirements
 * @property string|null $status
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
*  @property Carbon|null $review_date
 *
 * @package App\Models
 */
class Recruitment extends Model
{
	protected $table = 'recruitment';

	protected $casts = [
		'room_id' => 'int',
		'team_id' => 'int',
		'position_id' => 'int',
		'confirm_id' => 'array',
		'number_vacancies' => 'int',
		'experience_year' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $dates = [
		'review_date'
	];

	protected $fillable = [
		'room_id',
		'team_id',
		'position_id',
		'confirm_id',
		'number_vacancies',
		'recruitment_purpose',
		'reason',
		'recruitment_time',
		'type_work',
		'manpower_demarcation',
		'age',
		'gender',
		'experience_year',
		'academic_level',
		'academic_level_other',
		'qualification',
		'job_duties',
		'disc',
		'english_level',
		'computer_skill',
		'computer_skill_other',
		'device_requirements',
		'other_requirements',
		'status',
		'review_date',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function room()
	{
		return $this->hasOne(Room::class, 'id',  'room_id');
	}

	public function team()
	{
		return $this->hasOne(Team::class, 'id', 'team_id');
	}

	public function position()
	{
		return $this->hasOne(Position::class, 'id', 'position_id');
	}

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'created_by');
	}

	public function confirms()
	{
		return $this->hasMany(RecruitConfirm::class, 'recruit_id');
	}
}
