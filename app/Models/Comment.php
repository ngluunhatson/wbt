<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\ReplyComment;

/**
 * Class Allowance
 * 
 * @property int $id
 * @property int $blog_id
 * @property int $user_id
 * @property string $content
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property tinyint $delete_flag
 *
 * @package App\Models
 */

class Comment extends Model
{
    use HasFactory;


    protected $table = 'comment';

    protected $casts = [
		'id' => 'int',
		'blog_id' => 'int',
		'user_id' => 'int',
		'content' => 'string',
	];


    protected $fillable = [
        'blog_id',
        'user_id',
        'content',
        'created_at',
        'updated_at',
        'delete_flag',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function replyComment()
    {
        return $this->hasMany(ReplyComment::class, 'comment_id', 'id');
    }

}
