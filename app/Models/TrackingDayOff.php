<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TrackingDayOff
 * 
 * @property int $id
 * @property int $basic_info_id
 * @property float|null year_pto_count
 * @property int|null year_pto_array
 * @property int|null prev_year_tracking_day_off_id
 * @property string|null $note
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $detele_flag
 *
 * @package App\Models
 */
class TrackingDayOff extends Model
{
    protected $table = 'tracking_day_off';

    protected $casts = [
        'basic_info_id'         => 'int',
        'year'                  => 'int',
        'year_pto_array'        => 'array',
        'year_pto_count'        => 'float',
        'prev_year_tracking_day_off_id' => 'int',
        'created_by'            => 'int',
        'updated_by'            => 'int',
        'delete_flag'           => 'bool',
    ];


    protected $fillable = [
        'basic_info_id',
        'year_pto_array',
        'year_pto_count',
        'year',
        'note',
        'prev_year_tracking_day_off_id',
        'created_by',
        'updated_by',
        'delete_flag'
    ];


    public function staff()
    {
        return $this->hasOne(BasicInfo::class, 'id', 'basic_info_id');
    }

    public function prevYearTrackingDayOff() 
    {
        return $this->hasOne(TrackingDayOff::class, 'id', 'prev_year_tracking_day_off_id');
    }
}
