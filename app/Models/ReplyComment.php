<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\models\User;

/**
 * Class Allowance
 * 
 * @property int $id
 * @property int $comment_id
 * * @property int $user_id
 * @property string $content
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property tinyint $delete_flag
 *
 * @package App\Models
 */

class ReplyComment extends Model
{
    use HasFactory;


    protected $table = 'reply_comment';

    protected $casts = [
		'id' => 'int',
		'comment_id' => 'int',
        'user_id' => 'int',
		'content' => 'string',
	];


    protected $fillable = [
        'user_id',
        'comment_id',
        'content',
        'created_at',
        'updated_at',
        'delete_flag',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
