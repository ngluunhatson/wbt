<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DayOff
 * 
 * @property int $id
 * @property int $room_id
 * @property int $team_id
 * @property int $position_id
 * @property int $basic_info_id
 * @property int $confirm_id
 * @property bool|null $all_company
 * @property string|null $title
 * @property string|null $note
 * @property string|null $status
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class DayOff extends Model
{
	protected $table = 'day_off';

	protected $casts = [
		'room_id' => 'int',
		'team_id' => 'int',
		'position_id' => 'int',
		'basic_info_id' => 'int',
		'detail_id'	=> 'int',
		'confirm_id' => 'array',
		'all_company' => 'bool',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $dates = [
		'review_date'
	];

	protected $fillable = [
		'room_id',
		'team_id',
		'position_id',
		'basic_info_id',
		'confirm_id',
		'detail_id',
		'all_company',
		'title',
		'note',
		'status',
		'created_by',
		'updated_by',
		'delete_flag',
		'review_date',
	];

	public function room()
	{
		return $this->hasOne(Room::class, 'id',  'room_id');
	}

	public function team()
	{
		return $this->hasOne(Team::class, 'id',  'team_id');
	}

	public function position()
	{
		return $this->hasOne(Position::class, 'id',  'position_id');
	}

	public function basicInfo() 
	{
		return $this -> hasOne(BasicInfo::class, 'id', 'basic_info_id');
	}

	public function detailDayOff()
	{
		return $this->hasOne(DetailsDayOff::class, 'id',  'detail_id');
	}

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'created_by');
	}

	public function confirms()
	{
		return $this->hasMany(DayOffConfirm::class, 'day_off_id');
	}

}
