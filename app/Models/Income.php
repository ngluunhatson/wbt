<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Income
 * 
 * @property int $id
 * @property float|null $other_support
 * @property float|null $salary_kpis
 * @property float|null $additional_adjustments
 * @property float|null $adjustment_of_deductions
 * @property float|null $total
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Income extends Model
{
	protected $table = 'income';

	protected $casts = [
		'other_support' => 'float',
		'salary_kpis' => 'float',
		'additional_adjustments' => 'float',
		'adjustment_of_deductions' => 'float',
		'total' => 'float',
		'overtime_salary' => 'float',
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'other_support',
		'salary_kpis',
		'additional_adjustments',
		'adjustment_of_deductions',
		'total',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
