<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

/**
 * Class Organizational
 * 
 * @property int $id
 * @property int $room_id
 * @property int $team_id
 * @property int $position_id
 * @property int $basic_info_id
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Organizational extends Model
{
	protected $table = 'organizational';

	protected $casts = [
		'room_id' => 'int',
		'team_id' => 'int',
		'position_id' => 'int',
		'basic_info_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'year' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'room_id',
		'team_id',
		'position_id',
		'basic_info_id',
		'parent_id',
		'year',
		'created_by',
		'updated_by',
		'delete_flag'
	];

	public function room()
	{
		return $this->hasOne(Room::class, 'id',  'room_id');
	}

	public function team()
	{
		return $this->hasOne(Team::class, 'id', 'team_id');
	}

	public function position()
	{
		return $this->hasOne(Position::class, 'id', 'position_id');
	}

	public function positionSpecial()
	{
		return $this->hasOne(Position::class, 'id', 'position_id')->whereIn('position.rank', [2, 3])->oldest();
	}

	public function staff()
	{
		return $this->hasOne(BasicInfo::class, 'id', 'basic_info_id');
	}

	public function positionForTimekeep()
	{
		return $this->hasOne(Position::class, 'id', 'position_id')->where('position.rank', '<>', 9);
	}

	public function laborContract()
    {
        return $this->hasMany('App\Models\LaborContract','basic_info_id','basic_info_id');
    }
}
