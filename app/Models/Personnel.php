<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Personnel
 * 
 * @property int $id
 * @property string|null $personal_development_plan
 * @property string|null $language_development_plan
 * @property Carbon|null $created_at
 * @property int $created_by
 * @property Carbon|null $updated_at
 * @property int $updated_by
 * @property bool $delete_flag
 *
 * @package App\Models
 */
class Personnel extends Model
{
	protected $table = 'personnel';

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int',
		'delete_flag' => 'bool'
	];

	protected $fillable = [
		'personal_development_plan',
		'language_development_plan',
		'created_by',
		'updated_by',
		'delete_flag'
	];
}
