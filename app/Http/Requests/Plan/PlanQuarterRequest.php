<?php

namespace App\Http\Requests\Plan;

use Illuminate\Foundation\Http\FormRequest;

class PlanQuarterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'basic_info_id' => 'required|gt:0',
            'quarter'       => 'required|gt:0',
            'year'          => 'required',
            'quarter_goal'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'basic_info_id.required'    => 'Vui lòng chọn Nhân Viên',
            'basic_info_id.gt'          => 'Vui lòng chọn Nhân Viên',
            'quarter_goal.required'     => 'Vui lòng nhập Mục Tiêu Quý',
            'quarter.gt'                => 'Vui lòng chọn Quý',
            'year.required'             => 'Vui lòng chọn Năm',
        ];
    }
}
