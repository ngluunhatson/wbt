<?php

namespace App\Http\Requests\Plan;

use Illuminate\Foundation\Http\FormRequest;

class PlanDayTimeslotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'task_title'               => 'required',
            'task_date'          => 'required',
            'task_start_time'          => 'required',
            'task_end_time'            => 'required',
        ];
    }

    public function messages()
    {
        return [
            'task_title.required'       => 'Vui lòng nhập Tên Công Việc',
            'task_date.required'  => 'Vui lòng chọn Ngày Bắt Đầu',
            'task_start_time.required'  => 'Vui lòng chọn Giờ Bắt Đầu',
            'task_end_time.required'    => 'Vui lòng chọn Giờ Kết Thúc',

        ];
    }
}
