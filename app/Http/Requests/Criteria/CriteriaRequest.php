<?php

namespace App\Http\Requests\Criteria;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class CriteriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('not_empty_html', function ($attribute, $value) {
            return !empty(trim(strip_tags($value)));
        });
        
        $rules = [
            'room_id' => 'required',
            'location' => 'required|not_empty_html',
            'target' => 'required|not_empty_html',
            'recruitment_requirements' => 'required|not_empty_html',
            'capability_requirements' => 'required|not_empty_html',
            'function' => 'required|not_empty_html',
            'mission' => 'required|not_empty_html',
            'power' => 'required|not_empty_html',
            'wage' => 'required|not_empty_html',
            'kpis' => 'required|not_empty_html',
            'bonus' => 'required|not_empty_html',
            'disc' => 'required|not_empty_html',
            'report' => 'required|not_empty_html',
        ];

        if (!empty($this->input('room_id'))) {
            $rules['team_id'] = 'required';
        }

        if (!empty($this->input('team_id'))) {
            $rules['position_id'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'room_id.required' => 'Vui lòng chọn :attribute',
            'team_id.required' => 'Vui lòng chọn :attribute',
            'position_id.required' => 'Vui lòng chọn :attribute',

            'location.required' => 'Vui lòng nhập nội dung :attribute',
            'target.required'=> 'Vui lòng nhập nội dung :attribute',
            'recruitment_requirements.required'=> 'Vui lòng nhập nội dung :attribute',
            'capability_requirements.required'=> 'Vui lòng nhập nội dung :attribute',
            'function.required'=> 'Vui lòng nhập nội dung :attribute',
            'mission.required'=> 'Vui lòng nhập nội dung :attribute',
            'power.required'=> 'Vui lòng nhập nội dung :attribute',
            'wage.required'=> 'Vui lòng nhập nội dung :attribute',
            'kpis.required'=> 'Vui lòng nhập nội dung :attribute',
            'bonus.required'=> 'Vui lòng nhập nội dung :attribute',
            'disc.required'=> 'Vui lòng nhập nội dung :attribute',
            'report.required'=> 'Vui lòng nhập nội dung :attribute',

            'location.not_empty_html' => 'Vui lòng nhập nội dung :attribute',
            'target.not_empty_html'=> 'Vui lòng nhập nội dung :attribute',
            'recruitment_requirements.not_empty_html'=> 'Vui lòng nhập nội dung :attribute',
            'capability_requirements.not_empty_html'=> 'Vui lòng nhập nội dung :attribute',
            'function.not_empty_html'=> 'Vui lòng nhập nội dung :attribute',
            'mission.not_empty_html'=> 'Vui lòng nhập nội dung :attribute',
            'power.not_empty_html'=> 'Vui lòng nhập nội dung :attribute',
            'wage.not_empty_html'=> 'Vui lòng nhập nội dung :attribute',
            'kpis.not_empty_html'=> 'Vui lòng nhập nội dung :attribute',
            'bonus.not_empty_html'=> 'Vui lòng nhập nội dung :attribute',
            'disc.not_empty_html'=> 'Vui lòng nhập nội dung :attribute',
            'report.not_empty_html'=> 'Vui lòng nhập nội dung :attribute',
        ];
    }

    public function attributes()
    {
        return [
            'room_id' => 'phòng ban',
            'location' => 'vị trí',
            'target' => 'mục tiêu',
            'recruitment_requirements' => 'tuyển dụng',
            'capability_requirements' => 'năng lực',
            'function' => 'chức năng',
            'mission' => 'nhiệm vụ',
            'power' => 'quyền hạn',
            'wage' => 'lương',
            'kpis' => 'KPIs',
            'bonus' => 'thưởng',
            'disc' => 'DISC',
            'report' => 'báo cáo',
        ];
    }
}
