<?php

namespace App\Http\Requests\Room;

use Illuminate\Foundation\Http\FormRequest;

class RoomCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                 => 'required|unique:room,name,' . $this->id,
        ];
    }

    public function messages()
    {
        return [
            'name.required'             => 'Tên Phòng không để trống',
            'name.unique'               => 'Phòng này đã tồn tại',
        ];
    }
}
