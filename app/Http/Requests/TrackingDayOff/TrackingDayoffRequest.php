<?php

namespace App\Http\Requests\TrackingDayOff;

use Illuminate\Foundation\Http\FormRequest;

class TrackingDayoffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        for ($i = 1; $i < 13; $i++) {
            if ($i < 4) {
                $rules['prev_mth' . $i] = 'required|integer|between:0,24';
                $rules['cur_mth' . $i]  = 'required|integer|between:0,24';
            } else {
                $rules['prev_mth' . $i] = 'required|integer|between:0,12';
                $rules['cur_mth' . $i]  = 'required|integer|between:0,12';
            }
        }

        return $rules;
    }
}
