<?php

namespace App\Http\Requests\Recruitment;

use Illuminate\Foundation\Http\FormRequest;

class RecruitmentProposalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "room_id" => 'required',
            "number_vacancies" => 'required',
            "recruitment_purpose" => 'required',
            "reason" => 'required',
            "recruitment_time" => 'required',
            "type_work" => 'required',
            "manpower_demarcation" => 'required',
            "age" => 'required',
            "gender" => 'required',
            "experience_year" => 'required',
            "qualification" => 'required',
            "job_duties" => 'required',
            "disc" => 'required',
            "english_level" => 'required',
            "computer_skill" => 'required',
            "device_requirements" => 'required',
            "other_requirements" => 'required',
            "academic_level" => 'required',
        ];

        if ($this->input('academic_level') == 3) {
            $rules['academic_level'] = 'nullable';
            $rules['academic_level_other'] = 'required';
        }

        if ($this->input('computer_skill') == 2) {
            $rules['computer_skill_other'] = 'required';
        }

        return $rules;
    }
}
