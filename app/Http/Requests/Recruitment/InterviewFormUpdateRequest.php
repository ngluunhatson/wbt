<?php

namespace App\Http\Requests\Recruitment;

use Illuminate\Foundation\Http\FormRequest;

class InterviewFormUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'candidates_name' => 'required|max:255',
        ];
    }

    public function messages()
    {
        return [
            'candidates_name.required' => 'Vui lòng nhập :attribute',
            'candidates_name.max' => 'Vui lòng nhập :attribute không quá :max',
        ];
    }

    public function attributes()
    {
        return [
            'candidates_name' => 'họ tên ứng viên',
        ];
    }
}
