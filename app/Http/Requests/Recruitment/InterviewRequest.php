<?php

namespace App\Http\Requests\Recruitment;

use Illuminate\Foundation\Http\FormRequest;

class InterviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'interview_date'               => 'required' . $this->id,
            'expertise'                 => 'required' . $this->id,
            'work_experience'           => 'required' . $this->id,
            'problem_solving'           => 'required' . $this->id,
            'plan'                      => 'required' . $this->id,
            'orientation'               => 'required' . $this->id,
            'commitment_value'          => 'required' . $this->id,
            'communicate'               => 'required' . $this->id,
            'team_work'                 => 'required' . $this->id,
            'english_level'             => 'required' . $this->id,
            'it_skills'                 => 'required' . $this->id,
            'psychological_control'     => 'required' . $this->id,
            'attitude'                  => 'required' . $this->id,
            'status'                    => 'required' . $this->id,
        ];

        if ($this->input('status') == 3) {
            $rules['department_resource_notes'] = 'required'. $this->id;
        }

        if (auth()->user()->hasRole('ceo-manager')) {
            $rules = [
                'interview_date'            => 'nullable',
                'expertise'                 => 'nullable',
                'work_experience'           => 'nullable',
                'problem_solving'           => 'nullable',
                'plan'                      => 'nullable',
                'orientation'               => 'nullable',
                'commitment_value'          => 'nullable',
                'communicate'               => 'nullable',
                'team_work'                 => 'nullable',
                'english_level'             => 'nullable',
                'it_skills'                 => 'nullable',
                'psychological_control'     => 'nullable',
                'attitude'                  => 'nullable',
                'status'                    => 'nullable',
            ];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'interview_date.required'          => 'Vui lòng không để trống :attribute',
            'expertise.required'            => 'Vui lòng không để trống :attribute',
            'work_experience.required'      => 'Vui lòng không để trống :attribute',
            'problem_solving.required'      => 'Vui lòng không để trống :attribute',
            'plan.required.required'        => 'Vui lòng không để trống :attribute',
            'orientation.required'          => 'Vui lòng không để trống :attribute',
            'commitment_value.required'     => 'Vui lòng không để trống :attribute',
            'communicate.required'          => 'Vui lòng không để trống :attribute',
            'team_work.required'            => 'Vui lòng không để trống :attribute',
            'english_level.required'        => 'Vui lòng không để trống :attribute',
            'it_skills.required'            => 'Vui lòng không để trống :attribute',
            'psychological_control.required'=> 'Vui lòng không để trống :attribute',
            'attitude.required'             => 'Vui lòng không để trống :attribute',
            'status.required'               => 'Vui lòng chọn đánh giá',
            'department_resource_notes.required' => 'Vui lòng nhập :attribute',
        ];
    }

    public function attributes()
    {
        return [
            'interview_date' => 'Ngày phỏng vấn',
            'department_resource_notes' => 'Lý do'
        ];
    }
}
