<?php

namespace App\Http\Requests\Recruitment;

use Illuminate\Foundation\Http\FormRequest;

class InterviewFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'room_id' => 'required' . $this->id,
            'team_id' => 'required' . $this->id,
            'position_id' => 'required' . $this->id,
            'candidates_name' => 'required|max:255',
            'selectConfirm.*' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'room_id.required' => 'Vui lòng chọn :attribute',
            'team_id.required' => 'Vui lòng chọn :attribute',
            'position_id.required' => 'Vui lòng chọn :attribute',
            'candidates_name.required' => 'Vui lòng nhập :attribute',
            'candidates_name.max' => 'Vui lòng nhập :attribute không quá :max',
            'selectConfirm.*.required' => 'Vui lòng chọn :attribute',
        ];
    }

    public function attributes()
    {
        return [
            'room_id' => 'phòng ban',
            'team_id' => 'bộ phận',
            'position_id' => 'chức danh',
            'candidates_name' => 'họ tên ứng viên',
            'selectConfirm.0' => 'P.QT HC-NS',
            'selectConfirm.1' => 'Trưởng phòng',
            'selectConfirm.2' => 'Ban Giám Đốc',
        ];
    }
}
