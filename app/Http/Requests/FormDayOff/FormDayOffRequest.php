<?php

namespace App\Http\Requests\FormDayOff;

use Illuminate\Foundation\Http\FormRequest;


class FormDayOffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {

        $input_all = $this->except('_token');

        $rules = [];

        if ($input_all['request_type'] == 'create_admin') {
            $rules['direct_boss_basic_info_id'] = 'required';
            $rules['hr_basic_info_id'] = 'required';
        }

        if ($input_all['request_type'] == 'create_admin' || $input_all['request_type'] == 'update_admin')
            $rules['basic_info_id'] = 'required';

        for ($i = 1; $i <= $input_all['index_time_frame']; $i++) {
            if (array_key_exists('start_date' . $i, $input_all) && !$input_all['start_date' . $i]) {
                $rules['start_date' . $i] = 'required';
                
            }

            if (array_key_exists('end_date' . $i, $input_all) && !$input_all['end_date' . $i]) {
                $rules['end_date' . $i] = 'required';
              
            }

            if (array_key_exists('start_time' . $i, $input_all) && !$input_all['start_time' . $i]) {
                $rules['start_time' . $i]= 'required';
              
            }

            if (array_key_exists('end_time' . $i, $input_all) && !$input_all['end_time' . $i]) {
                $rules['end_time' . $i] = 'required';
              
            }
            if (array_key_exists('type' . $i, $input_all) && $input_all['type' . $i] == 0) {
                $rules['type' . $i] = 'required';
                
            }

            if (array_key_exists('reason' . $i, $input_all) && $input_all['reason' . $i] == 0) {
                $rules['reason' . $i] = 'required';
            }
        }


        return $rules;
    }

    public function messages()
    {


        $messages = [
            'reason.required'                             => 'Vui lòng nhập :attribute',
            'direct_boss_basic_info_id.required'          => 'Vui lòng nhập :attribute',
            'hr_basic_info_id.required'                   => 'Vui lòng nhập :attribute',
            'basic_info_id.required'                      => 'Vui lòng nhập :attribute',
        ];

        $input_all = $this->except('_token');

        for ($i = 1; $i <= $input_all['index_time_frame']; $i++) {
            if (array_key_exists('start_date' . $i, $input_all) && !$input_all['start_date' . $i]) {
                $messages['start_date' . $i.'required'] = 'Vui lòng nhập Ngày Bắt Đầu '. $i;
                
            }

            if (array_key_exists('end_date' . $i, $input_all) && !$input_all['end_date' . $i]) {
                $messages['end_date' . $i.'required'] = 'Vui lòng nhập Ngày Kết Thúc '.$i;
              
            }

            if (array_key_exists('start_time' . $i, $input_all) && !$input_all['start_time' . $i]) {
                $messages['start_time' . $i.'required']= 'Vui lòng nhập Giờ Bắt Đầu '.$i;
              
            }

            if (array_key_exists('end_time' . $i, $input_all) && !$input_all['end_time' . $i]) {
                $messages['end_time' . $i.'required'] = 'Vui lòng nhập Giờ Kết Thúc '.$i;
              
            }
            if (array_key_exists('type' . $i, $input_all) && $input_all['type' . $i] == 0) {
                $messages['type' . $i.'required'] = 'Vui lòng nhập Loại Nghỉ Phép '.$i;
                
            }

            if (array_key_exists('reason' . $i, $input_all) && $input_all['reason' . $i] == 0) {
                $messages['reason' . $i] = 'Vui lòng nhập Lý Do '.$i;
            }
        }


        return $messages;
    }

    public function attributes()
    {

        $attributes =  [
            'reason'                    => 'Lý do',
            'direct_boss_basic_info_id' => 'Người Quản Lý Trực Tiếp',
            'hr_basic_info_id'          => 'Quản Lý Nhân Sự',
            'basic_info_id'             => 'Nhân Viên',

        ];


        return $attributes;
    }
}
