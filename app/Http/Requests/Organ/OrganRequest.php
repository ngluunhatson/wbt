<?php

namespace App\Http\Requests\Organ;

use App\Models\BasicInfo;
use Illuminate\Foundation\Http\FormRequest;

class OrganRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [];

        for($i = 1; $i <= $this->input('amount'); $i++) {
            if ($this->input('basic_info_id_' . $i) != null && $i > 1) {
                $rule['basic_info_id_' . $i] = 'different:basic_info_id_'.( $i - 1 );
            }
        }
        return $rule;
    }

    public function messages()
    {
        $messge = [];
        for($i = 1; $i <= $this->input('amount'); $i++) {
            $messge['basic_info_id_'. $i . '.different'] = 'Không được chọn trùng';
        }
        return $messge;
    }
}
