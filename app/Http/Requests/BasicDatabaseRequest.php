<?php

namespace App\Http\Requests;

use App\Models\BasicInfo;
use Illuminate\Foundation\Http\FormRequest;

class BasicDatabaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // BasicInfo
            'company'           => 'required|max:100',
            'full_name'         => 'required|max:100',
            'name_eng'          => 'required|max:100',
            'code_staff'        => 'required|max:10',
            'date_work'         => 'required',
            'phone'             => 'required|max:11',
            'email'             => 'required|email|unique:basic_info,personal_email,' . $this->id,
            'email_company'     => 'required|email|unique:basic_info,company_email,' . $this->id,
            'birth_date'        => 'required|before:01/01/2005|after:01/01/1950',
            'rank'              => 'required',

            // PersonalInfo
            'passport'                  => 'max:20',
            // 'issue_date'                => 'after:birth_date',
            'providers'                 => 'max:200',
            'permanent_address'         => 'max:200',
            'temporary_address'         => 'max:200',
            'insurance_code'            => 'max:20',
            'medical_code'              => 'max:20',
            'address_kcb'               => 'max:200',
            'tax'                       => 'max:20',
            'dependent_person'          => 'max:2',
            // 'npt'                       => 'required',
            'school'                    => 'max:100',
            'specialized'               => 'max:50',
            'tknh'                      => 'max:20',
            'bank'                      => 'max:50',
            'license_plates'            => 'max:20',
            'carriage_category'         => 'max:20',

            // RelativeInfo
            'prioritized_1'                 => 'max:100',
            'phone_prioritized_1'           => 'max:11',
            'prioritized_2'                 => 'max:100',
            'phone_prioritized_2'           => 'max:11',
            'child_1'                       => 'max:100',
            // 'birth_date_child_1'            => 'before:tomorow',
            'child_2'                       => 'max:100',
            // 'birth_date_child_2'            => 'before:tomorow',
            'child_3'                       => 'max:100',
            // 'birth_date_child_3'            => 'before:tomorow'

        ];
    }

    public function messages()
    {
        return [
            // BasicInfo
            'company.required'          => 'Không để trống tên công ty',
            'company.max'               => 'Tên công ty không quá 100 ký tự',
            'full_name.required'        => 'Không để trống họ và tên',
            'name_eng.required'         => 'Không để trống tên Tiếng Anh',
            'code_staff.required'       => 'Không để trống mã',
            'date_work.required'        => 'Không để trống Ngày làm việc',
            'phone.required'            => 'Không để trống số điện  thoại',
            'email.required'            => 'Không để trống email',
            'email.email'               => 'Không đúng định dạng Email',
            'email_company.required'    => 'Không để trống email công ty',
            'email_company.email'       => 'viết đúng định dạng mail',
            'birth_date.required'       => 'Không để trống ngày sinh',
            'birth_date.before'         => 'Bạn chưa đủ tuổi lao động',
            'birth_date.after'          => 'Bạn  đã quá tuổi lao động',

            'file_disc.max'             => 'File quá nặng',

            'file_motivator.max'        => 'File quá nặng',

            'file_cv.max'               => 'File quá nặng',

            // PersonalInfo
            // 'passport.required'                 => 'Không để trống CCCD',
            'passport.max'                      => 'CCD không quá 20 ký tự',
            // 'issue_date.required'               => 'Không để trống ngày cấp CCCD',
            // 'issue_date.before'                 => 'Ngày cấp CCCD phải trước hôm nay',
            'issue_date.after'                  => 'Ngày cấp CCCD phải sau ngày sinh',
            // 'providers.required'                => 'Không để trống nơi cấp CCCD',
            'providers.max'                     => 'Địa chỉ nơi cấp CCCD quá dài',
            // 'permanent_address.required'        => 'Không để trống địa chỉ thường trú',
            'permanent_address.max'             => 'Địa chỉ thường trú quá dài',
            // 'temporary_address.required'        => 'Không để trống địa chỉ tạm trú',
            'temporary_address.max'             => 'Địa chỉ tạm trú quá dải',
            // 'insurance_code.required'           => 'Không để trống mã bảo hiểm xã hội',
            'insurance_code.max'                => 'Mã bảo hiểm xã hội quá dài',
            // 'medical_code.required'             => 'Không để trống mã bảo hiểm y tế',
            'medical_code.max'                  => 'Mã bảo hiểm y tế quá dài',
            // 'address_kcb.required'              => 'Không để trống thông tin bệnh viện',
            'address_kcb.max'                   => 'Thông tin bệnh viện quá dài',
            // 'tax.required'                      => 'Không để trống thuế TNCN',
            'tax.max'                           => 'Mã thuế TNCN quá dài',
            // 'dependent_person.required'         => 'Không để trống số người phụ thuộc',
            'dependent_person.max'              => 'Số người phụ thuộc quá nhiều',
            // 'dependent_person.not_regex'        => 'Số người phụ thuộc phải lớn hơn 0',
            // 'npt.required'                      => 'Không để trống thông tin người phụ thuộc',
            // 'school.required'                   => 'Không để trống tên trường',
            'school.max'                        => 'Tên trường quá dài',
            // 'specialized.required'              => 'Không để trống chuên ngành',
            'specialized.max'                   => 'Chuyên ngành quá dài',
            // 'tknh.required'                     => 'Không để trống số TKNH',
            'tknh.max'                          => 'Số TKNH quá dài',
            // 'bank.required'                     => 'Không để trống tên ngân hàng',
            'bank.max'                          => 'Tên ngân hàng quá dài',
            // 'license_plates.required'           => 'Không để trống biển số xe',
            'license_plates.max'                => 'Biển số xe quá dài',
            // 'carriage_category.required'        => 'Không để trống loại xe',
            'carriage_category.max'             => 'Loại xe quá dài',


            // RelativeInfo
            'prioritized_1.max'               => 'Tên quá dài',
            'phone_prioritized_1.max'         => 'Số điện thoại quá dài',
            'prioritized_2.max'               => 'Tên quá dài',
            'phone_prioritized_2.max'         => 'Số điện thoại quá dài',
            'child_1.max'                     => 'Tên quá dài',
            // 'birth_date_child_1.before'       => 'Phải nhập trước ngày hôm nay',
            'child_2.max'                     => 'Tên quá dài',
            // 'birth_date_child_2.before'       => 'Phải nhập trước ngày hôm nay',
            'child_3.max'                     => 'Tên quá dài',
            // 'birth_date_child_3.before'       => 'Phải nhập trước ngày hôm nay',

            'rank.required'                   => 'Vui lòng chọn cấp bậc',
        ];
    }
}
