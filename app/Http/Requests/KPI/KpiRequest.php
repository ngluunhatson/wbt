<?php

namespace App\Http\Requests\KPI;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class KpiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'quarter' => 'required' . $this->id,
            'year' => 'required' . $this->id,
            'room_id' => 'required' . $this->id,
            'team_id' => 'required' . $this->id,
            'position_id' => 'required' . $this->id,
            'selectStaff' => 'required' . $this->id,
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'room_id.required' => 'Vui lòng chọn :attribute',
            'year.required' => 'Vui lòng chọn :attribute',
            'team_id.required' => 'Vui lòng chọn :attribute',
            'position_id.required' => 'Vui lòng chọn :attribute',
            'quarter.required' => 'Vui lòng nhập nội dung :attribute',
            'selectStaff.required' => 'Vui lòng chọn :attribute',
        ];
    }

    public function attributes()
    {
        return [
            'room_id' => 'phòng ban',
            'year' => 'Năm',
            'team_id' => 'bộ phận',
            'position_id' => 'chức danh',
            'quarter' => 'quý',
            'selectStaff' => 'Người nhận việc'
        ];
    }
}
