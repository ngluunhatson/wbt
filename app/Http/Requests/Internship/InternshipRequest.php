<?php

namespace App\Http\Requests\Internship;

use Illuminate\Foundation\Http\FormRequest;


class InternshipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {

        $input_all = $this->except('_token');
        $rules = [];

        $type_user = intval($input_all['type_user']);

        if ($type_user == 2) {
            if (array_key_exists('direct_boss_decision_check', $input_all))
                $rules['direct_boss_decision_' . $input_all['direct_boss_decision_check']] = 'required';

            $rules['direct_boss_confirm'] = 'required';

            if (array_key_exists('direct_boss_confirm', $input_all) && intval($input_all['direct_boss_confirm']) == 2) {
                $rules['direct_boss_decline_reason'] = 'required';
            }
        }

        if ($type_user == 3) {
            $rules['room_manager_confirm'] = 'required';
            if (array_key_exists('room_manager_confirm', $input_all) && intval($input_all['room_manager_confirm']) == 2) {
                $rules['room_manager_decline_reason'] = 'required';
            }
        }

        if ($type_user == 4) {
            $rules['hr_confirm'] = 'required';

            for ($i = 1; $i <= 2; $i++)
                if (array_key_exists('hr_decision_'. $i.'_check', $input_all)) {
                    $rules['hr_decision_'. $i] = 'required';
                }

            if (array_key_exists('hr_decision_hdld_check', $input_all)) {
                $rules['hr_decision_'. $input_all['hr_decision_hdld_check']] = 'required';
            }
            if (array_key_exists('hr_confirm', $input_all) && intval($input_all['hr_confirm']) == 2) {
                $rules['hr_decline_reason'] = 'required';
            }
        }

        if ($type_user == 5) {
            $rules['ceo_confirm'] = 'required';
            if (array_key_exists('ceo_confirm', $input_all) && intval($input_all['ceo_confirm']) == 2) {
                $rules['ceo_decline_reason'] = 'required';
            }
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [
            'basic_info_id.required'        => 'Vui lòng nhập :attribute',
        ];

        return $messages;
    }

    public function attributes()
    {

        $attributes =  [

            'basic_info_id'             => 'Nhân Viên Thử Việc',

        ];


        return $attributes;
    }
}
