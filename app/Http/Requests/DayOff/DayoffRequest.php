<?php

namespace App\Http\Requests\DayOff;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class DayoffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:300',
            'note' => 'required|max:300',
            'room_id' => 'required',
            'start_date' => 'required',
            'start_time' => 'required',
            'end_date' => 'required',
            'end_time' => 'required',
            'total_day_off' => 'required',
            'day_off_id' => 'nullable',
        ];

        if (array_key_exists('all_company', $this->input())) {
            $rules['room_id'] = 'nullable';
        }

        return $rules;
    }

    // public function messages()
    // {
    //     return [
    //         'title.required' => 'Vui lòng nhập :attribute',
    //         'title.max' => 'Vui lòng nhập :attribute không quá :max ký tự',
    //         'note.required' => 'Vui lòng nhập :attribute',
    //         'note.max' => 'Vui lòng nhập :attribute không quá :max ký tự',
    //         'room_id.required' => 'Vui lòng chọn :attribute',
    //     ];
    // }

    // public function attributes()
    // {
    //     return [
    //         'title' => 'Tiêu đề',
    //         'note' => 'Ghi chú',
    //         'room_id' => 'Phòng ban'
    //     ];
    // }
}
