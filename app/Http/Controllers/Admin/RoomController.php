<?php

namespace App\Http\Controllers\Admin;

use App\Models\Room;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Room\RoomCreateRequest;
use App\Http\Requests\Room\RoomUpdateRequest;

class RoomController extends Controller
{
    public function index()
    {
        $rooms = Room::where('delete_flag', 0)->get();
        return view('frontend.room.index', compact('rooms'));
    }

    public function create(RoomCreateRequest $request)
    {

        $input = $request->all();
        $room = Room::firstOrCreate([
            'name' => $input['name'],
            'created_by' => auth()->id(),
            'updated_by' => auth()->id(),
            'bod'        => array_key_exists('bod_flag', $input),
        ]);
        $room->room_order = $room->id;
        $room->save();
        return redirect()->route('room')
            ->with('success', 'Thêm thành công');
    }

    public function edit($id)
    {
        // dd($id);
        $name = Room::find($id);

        return view('frontend.room.update', compact('name'));
    }

    public function update(RoomUpdateRequest $request, $id)
    {
        // dd($id);
        $input = $request->all();
        // dd($input);
        Room::find($id)->update($input);

        return redirect()->route('room')
            ->with('success', 'Sửa thành công');
    }

    public function delete($id)
    {
        // dd($id);
        $room = Room::with(['teams'])->find($id);

        if (count($room->teams) > 0)
            return redirect()->route('room')->with('error', 'Vui lòng xóa các bộ phận của phòng '. $room->name . ' trước!');


        $room->delete();

        return redirect()->route('room')
            ->with('success', 'Xóa thành công');
    }
}
