<?php

namespace App\Http\Controllers\Admin\FormDayoff;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormDayOff\FormDayOffRequest;
use App\Services\Dayoff\DayOffService;
use App\Services\FormDayOff\FormDayOffService;
use App\Services\TrackingDayOff\TrackingDayOffService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FormDayoffController extends Controller
{

    private $formDayOffService;
    public function __construct(FormDayOffService $formDayOffService, DayOffService $dayOffService, TrackingDayOffService $trackingDayOffService)
    {
        $this->formDayOffService     = $formDayOffService;
        $this->dayOffService         = $dayOffService;
        $this->trackingDayOffService = $trackingDayOffService;
    }

    public function index()
    {
        $dataInit = [];

        $this->trackingDayOffService->populateTrackingDayOffsNew();
        $this->trackingDayOffService->updateAllTrackingDayOffsNew();

        $dataInit['myFormDayOffs']         = [];
        $dataInit['allFormDayOffs']        = $this->formDayOffService->getFormDayOffAssignedToUser(auth()->id());
        $dataInit['myFormDayOffsDict']     = $this->formDayOffService->getFormDayOffsAvailableToUserDict(auth()->id());
        $dataInit['allFormDayOffsDict']    = $this->formDayOffService->getFormDayOffAssignedToUserDict(auth()->id());

        if (auth()->user()->hasAnyPermission(['form_day_off-index']))
            $dataInit['myFormDayOffs']     = $this->formDayOffService->getFormDayOffsAvailableToUser(auth()->id());


        if (auth()->user()->hasAnyPermission(['form_day_off-index_admin'])) {
            $dataInit['allFormDayOffs']        = $this->formDayOffService->getAllFormDayOffs();
            $dataInit['allFormDayOffsDict']    = $this->formDayOffService->getAllFormDayOffsDict();
        }


        return view('frontend.formDayoff.index', compact('dataInit'));
    }

    public function create()
    {
        $dataInit = [];
        $this->trackingDayOffService->populateTrackingDayOffsNew();
        $this->trackingDayOffService->updateAllTrackingDayOffsNew();

        $dataInit['tracking_day_off_of_user']          = $this->trackingDayOffService->getTrackingDayOffById(auth()->id(), intval(date('Y', strtotime(now()))), 'user_id');
        $dataInit['user_day_off_array']                = $this->dayOffService->getAllDayOffsAvailableToUser(auth()->id());
        $dataInit['confirmed_form_day_offs']           = $this->formDayOffService->getConfirmedFormDayOffsOfUser(auth()->id());
        $dataInit['basic_info_of_replacement_array']   = $this->formDayOffService->getBasicInfoOfReplacementArray(auth()->id());
        $dataInit['basic_info_of_direct_boss_array']   = $this->formDayOffService->getBasicInfoOfDirectBossArray(auth()->id());
        $dataInit['hr']                                = $this->formDayOffService->getHR();

        return view('frontend.formDayoff.create', compact('dataInit'));
    }

    public function create_admin()
    {
        $dataInit = [];
        $this->trackingDayOffService->populateTrackingDayOffsNew();
        $this->trackingDayOffService->updateAllTrackingDayOffsNew();
        $dataInit['rooms']              =  $this->formDayOffService->getAll_X('rooms');
        $dataInit['hr']                 =  $this->formDayOffService->getHR();

        return view('frontend.formDayoff.create_admin', compact('dataInit'));
    }


    public function view($id)
    {
        $dataInit = [];
        $this->trackingDayOffService->populateTrackingDayOffsNew();
        $this->trackingDayOffService->updateAllTrackingDayOffsNew();

        $getFormResult = $this->formDayOffService->getFormDayOffById($id);

        $dataInit['current_form_day_off']              = $getFormResult[0];
        $dataInit['current_form_day_off_user_id']      = $getFormResult[1];
        $dataInit['tracking_day_off_of_form']          = $this->trackingDayOffService->getTrackingDayOffById($dataInit['current_form_day_off']->basic_info_id, intval(date('Y', strtotime(now()))));

        return view('frontend.formDayoff.view', compact('dataInit'));
    }

    public function edit_admin($id)
    {
        $dataInit = [];
        $this->trackingDayOffService->populateTrackingDayOffsNew();
        $this->trackingDayOffService->updateAllTrackingDayOffsNew();

        $dataInit['current_form_day_off']  = $this->formDayOffService->getFormDayOffById($id)[0];
        $dataInit['basic_info_of_replacement_array']   = $this->formDayOffService->getBasicInfoOfReplacementArray($dataInit['current_form_day_off']->basic_info_id, 'basic');
        $dataInit['basic_info_of_direct_boss_array']   = $this->formDayOffService->getBasicInfoOfDirectBossArray($dataInit['current_form_day_off']->basic_info_id, 'basic');
        $dataInit['rooms']                 =  $this->formDayOffService->getAll_X('rooms');
        $dataInit['hr']                    =  $this->formDayOffService->getHR();

        return view('frontend.formDayoff.edit_admin', compact('dataInit'));
    }

    public function edit($id)
    {
        $dataInit = [];
        $this->trackingDayOffService->populateTrackingDayOffsNew();
        $this->trackingDayOffService->updateAllTrackingDayOffsNew();
        $dataInit['current_form_day_off']              = $this->formDayOffService->getFormDayOffById($id)[0];
        $dataInit['current_tracking_day_off']          = $this->trackingDayOffService->getTrackingDayOffById($dataInit['current_form_day_off']->basic_info_id, intval(date('Y', strtotime(now()))));
        $dataInit['confirmed_form_day_offs']           = $this->formDayOffService->getConfirmedFormDayOffsOfUser(auth()->id());
        $dataInit['user_day_off_array']                = $this->dayOffService->getAllDayOffsAvailableToUser(auth()->id());
        $dataInit['basic_info_of_replacement_array']   = $this->formDayOffService->getBasicInfoOfReplacementArray(auth()->id());
        $dataInit['basic_info_of_direct_boss_array']   = $this->formDayOffService->getBasicInfoOfDirectBossArray(auth()->id());
        $dataInit['hr']                                = $this->formDayOffService->getHR();

        return view('frontend.formDayoff.edit', compact('dataInit'));
    }


    public function storeDataInsert(FormDayOffRequest $request)
    {
        $result = $this->formDayOffService->saveDataNew($request);
        if ($result['status']) {
            return redirect()->route('FormDayOffIndex')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('FormDayOffIndex')
                ->with('error', $result['message']);
        }
    }

    public function storeDataUpdate(FormDayOffRequest $request)
    {
        $result = $this->formDayOffService->updateDataNew($request);
        if ($result['status']) {
            return redirect()->route('FormDayOffIndex')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('FormDayOffIndex')
                ->with('error', $result['message']);
        }
    }

    public function updateStatus(Request $request)
    {
        $result = $this->formDayOffService->updateStatus($request);
        if ($result['status']) {
            return redirect()->route('FormDayOffIndex')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('FormDayOffIndex')
                ->with('error', $result['message']);
        }
    }

    public function delete($id)
    {
        $result = $this->formDayOffService->delete($id);

        if ($result['status']) {
            return redirect()->route('FormDayOffIndex')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('FormDayOffIndex')
                ->with('error', $result['message']);
        }
    }

    public function getBasicInfoOfReplacementArrayByBasicInfoId($id)
    {
        return response()->json($this->formDayOffService->getBasicInfoOfReplacementArray($id, "basic"));
    }


    public function getBasicInfoOfDirectBossArrayByBasicInfoId($id)
    {
        return response()->json($this->formDayOffService->getBasicInfoOfDirectBossArray($id, "basic"));
    }

    public function getBasicInfoOfHighestBossArrayByBasicInfoId($id)
    {
        return response()->json($this->formDayOffService->getBasicInfoOfDirectBossArray($id, "basic", false, false));
    }

    public function getConfirmedFormDayOffsOfBasicInfoId($id)
    {
        return response()->json($this->formDayOffService->getConfirmedFormDayOffsOfUser($id, "basic"));
    }

    public function checkForm(Request $request)
    {
        $input_all = $request -> all();
        $rules = [];

        if ($input_all['request_type'] == 'create_admin' || $input_all['request_type'] == 'update_admin')
            $rules['basic_info_id'] = 'required';


        for ($i = 1; $i <= $input_all['index_time_frame']; $i++) {
            if (array_key_exists('start_date' . $i, $input_all) && !$input_all['start_date' . $i]) {
                $rules['start_date' . $i] = 'required';
            }

            if (array_key_exists('end_date' . $i, $input_all) && !$input_all['end_date' . $i]) {
                $rules['end_date' . $i] = 'required';
            }

            if (array_key_exists('start_time' . $i, $input_all) && !$input_all['start_time' . $i]) {
                $rules['start_time' . $i] = 'required';
            }

            if (array_key_exists('end_time' . $i, $input_all) && !$input_all['end_time' . $i]) {
                $rules['end_time' . $i] = 'required';
            }
            if (array_key_exists('type' . $i, $input_all) && $input_all['type' . $i] == 0) {
                $rules['type' . $i] = 'required';
            }

            if (array_key_exists('reason' . $i, $input_all) && $input_all['reason' . $i] == 0) {
                $rules['reason' . $i] = 'required';
            }
        }
        $validator = Validator::make($request->all(), $rules);
   
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => "Thiếu Thông Tin Cần Nhập"
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Success'
            ], 200);
        }
    }
}
