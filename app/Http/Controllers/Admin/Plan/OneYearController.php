<?php

namespace App\Http\Controllers\Admin\Plan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OneYearController extends Controller
{
    public function index()
    {
        return view('frontend.plan.oneYear.index');
    }
    public function edit()
    {
        return view('frontend.plan.oneYear.edit');
    }
}
