<?php

namespace App\Http\Controllers\Admin\Plan;

use App\Http\Controllers\Controller;
use App\Services\Plan\PlanDay\PlanDayService;
use Illuminate\Http\Request;

class PlanDayController extends Controller
{
    private $planDayService;
    public function __construct(PlanDayService $planDayService)
    {
        $this->planDayService = $planDayService;
    }

    public function index()
    {

        $dataInit = [];
        $dataInit['rooms'] =  $this->planDayService->getAll_X('rooms');
        if (auth()->user()->staff) {
            $dataInit['planDay'] = $this->planDayService->findCurPlanDayOfStaff(auth()->user()->basic_info_id);
        }
        return view('frontend.plan.plan_day.index', compact('dataInit'));
    }
    public function create()
    {
        return view('frontend.plan.plan_day.create');
    }

    public function edit($id)
    {
        $dataInit = [];
        $dataInit['modeEdit'] = true;
        $dataInit['planDay'] = $this->planDayService->getPlanDayById($id);
        return view('frontend.plan.plan_day.edit', compact('dataInit'));
    }
    public function view($id)
    {
        $dataInit = [];
        $dataInit['modeEdit'] = false;
        $dataInit['planDay'] = $this->planDayService->getPlanDayById($id);

        return view('frontend.plan.plan_day.view', compact('dataInit'));
    }

    public function storeInsert(Request $request)
    {
        $result = $this->planDayService->saveData($request);
        if ($result['status']) {
            return redirect()->route('plan_day.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('plan_day.index')
                ->with('error', $result['message']);
        }
    }

    public function storeUpdate(Request $request)
    {
        $result = $this->planDayService->updateData($request);
        if ($result['status']) {
            return redirect()->route('plan_day.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('plan_day.index')
                ->with('error', $result['message']);
        }
    }

    public function delete($id)
    {
        $result = $this->planDayService->delete($id);

        if ($result['status']) {
            return redirect()->route('plan_day.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('plan_day.index')
                ->with('error', $result['message']);
        }
    }


    public function filterPlanDay(Request $request)
    {
        return response()->json($this->planDayService->filterPlanDay($request));
    }

    public function checkIfExistPlanWeek(Request $request)
    {
        $result = [
            'success' => false,
            'message' => "Chưa Có Kế Hoạch Quý cho Ngày Này, Tạo Kế Hoạch Quý Trước!"
        ];

        if ($this->planDayService->checkIfExistPlanWeek($request) != null) {
            $result = [
                'success' => true,
                'message' => "Ngày Này Tạo Kế Hoạch Được. Ấn Xác Nhận Để Tạo"
            ];
        }

        return response()->json($result);
    }
}
