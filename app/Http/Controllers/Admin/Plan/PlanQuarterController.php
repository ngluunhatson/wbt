<?php

namespace App\Http\Controllers\Admin\Plan;

use App\Http\Controllers\Controller;
use App\Http\Requests\Plan\PlanQuarterRequest;
use App\Services\Plan\PlanQuarter\PlanQuarterService;
use Illuminate\Http\Request;

class PlanQuarterController extends Controller
{
    private $planQuarterService;
    public function __construct(PlanQuarterService $planQuarterService)
    {
        $this->planQuarterService = $planQuarterService;
    }

    public function index()
    {
        $dataInit = [];
        $dataInit['rooms'] =  $this->planQuarterService->getAll_X('rooms');
        if (auth()->user()->hasAnyPermission(['plan_quarter-index_admin'])) {
           
            return view('frontend.plan.plan_quarter.index', compact('dataInit'));
        } else {
            $dataInit['indexView']   = true;
            $dataInit['planQuarter'] = null;
            return view('frontend.plan.plan_quarter.view', compact('dataInit'));
        }
    }

    public function create()
    {
        $dataInit          = [];
        $dataInit['rooms'] =  $this->planQuarterService->getAll_X('rooms');
        return view('frontend.plan.plan_quarter.create', compact('dataInit'));
    }

    public function edit($id)
    {
        return view('frontend.plan.plan_quarter.edit');
    }

    public function view($id)
    {
        $dataInit = [];
        $dataInit['indexView']   = false;
        $dataInit['planQuarter'] = $this->planQuarterService->getPlanQuarterById($id);
        $dataInit['rooms'] =  $this->planQuarterService->getAll_X('rooms');

        return view('frontend.plan.plan_quarter.view', compact('dataInit'));
    }

    public function storeInsert(PlanQuarterRequest $request)
    {
        $result = $this->planQuarterService->saveData($request);
        if ($result['status']) {
            return redirect()->route('plan_quarter.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('plan_quarter.index')
                ->with('error', $result['message']);
        }
    }

    public function storeUpdate(PlanQuarterRequest $request)
    {
        if ($request['plan_quarter_id'])
            $result = $this->planQuarterService->updateData($request);
        else
            $result = $this->planQuarterService->saveData($request);
        if ($result['status']) {
            return redirect()->route('plan_quarter.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('plan_quarter.index')
                ->with('error', $result['message']);
        }
    }

    public function checkIfExistPlanQuarter(Request $request)
    {
        $result  = $this->planQuarterService->checkIfExistPlanQuarter($request);

        if (count($result) > 0) {
            return response()->json([
                'success' => false,
                'message' => "Đã Tồn Tại Kế Hoạch cho Quý " .
                    $result['quarter'] . " của Năm " .
                    $result['year'] . " cho Nhân Viên " . $result['staff_full_name'],
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Success'
            ], 200);
        }
    }

    public function getPlanQuarter(Request $request)
    {
        return response()->json($this->planQuarterService->getPlanQuarter($request));
    }

    public function filterPlanQuarter(Request $request)
    {
        return response()->json($this->planQuarterService->filterPlanQuarter($request));
    }

    public function delete($id)
    {
        $result = $this->planQuarterService->delete($id);

        if ($result['status']) {
            return redirect()->route('plan_quarter.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('plan_quarter.index')
                ->with('error', $result['message']);
        }
    }
}
