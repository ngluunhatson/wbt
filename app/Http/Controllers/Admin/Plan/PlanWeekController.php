<?php

namespace App\Http\Controllers\Admin\Plan;

use App\Http\Controllers\Controller;
use App\Http\Requests\Plan\PlanDayTimeslotRequest;
use App\Services\Plan\PlanWeek\PlanWeekService;
use Illuminate\Http\Request;

class PlanWeekController extends Controller
{
    private $planWeekService;
    public function __construct(PlanWeekService $planWeekService)
    {
        $this->planWeekService = $planWeekService;
    }

    public function index()
    {
        $dataInit = [];
        $dataInit['rooms'] =  $this->planWeekService->getAll_X('rooms');
        if (auth()->user()->staff) {
            $dataInit['planWeek'] = $this->planWeekService->findCurWeekPlanOfStaff(auth()->user()->basic_info_id);
        }

        return view('frontend.plan.plan_week.index', compact('dataInit'));
    }
    public function create()
    {
        return view('frontend.plan.plan_week.create');
    }

    public function edit($id)
    {
        $dataInit = [];
        $dataInit['modeEdit'] = true;
        $dataInit['planWeek'] = $this->planWeekService->getPlanWeekById($id);
        return view('frontend.plan.plan_week.edit', compact('dataInit'));
    }

    public function view($id)
    {
        $dataInit = [];
        $dataInit['modeEdit'] = false;
        $dataInit['planWeek'] = $this->planWeekService->getPlanWeekById($id);
        return view('frontend.plan.plan_week.view', compact('dataInit'));
    }

    public function delete($id)
    {
        $result = $this->planWeekService->delete($id);

        if ($result['status']) {
            return redirect()->route('plan_week.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('plan_week.index')
                ->with('error', $result['message']);
        }
    }

    public function storeInsert(Request $request)
    {
        $result = $this->planQuarterService->saveData($request);
        if ($result['status']) {
            return redirect()->route('plan_quarter.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('plan_quarter.index')
                ->with('error', $result['message']);
        }
    }

    public function storeUpdate(Request $request)
    {
        $result = $this->planQuarterService->updateData($request);
        if ($result['status']) {
            return redirect()->route('plan_quarter.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('plan_quarter.index')
                ->with('error', $result['message']);
        }
    }

    public function filterPlanWeek(Request $request)
    {
        return response()->json($this->planWeekService->filterPlanWeek($request));
    }

    public function addPlanDayTimeslot(Request $request)
    {
        $result_check_plan_day_timeslot = $this->planWeekService->checkPlanDayTimeslot($request);

        if ($result_check_plan_day_timeslot['status']) {

            if(count($request -> only('plan_day_timeslot_id')) > 0)
                $result = $this->planWeekService->updatePlanDayTimeslot($request);
            else
                $result = $this->planWeekService->addPlanDayTimeslot($request);
            
            if ($result['status'])
                return response()->json([
                    'success' => true,
                    'message' => $result['message']
                ], 200);
            else
                return response()->json([
                    'success' => false,
                    'message' => $result['message']
                ], 200);
        
        } else {
            return response()->json([
                'success' =>  false,
                'message' => $result_check_plan_day_timeslot['message']
            ], 200);
        }
    }

    public function getPlanDayTimeslotDataFromDates(Request $request) {
        $result = [];
        $result = $this -> planWeekService -> getPlanDayTimeslotDataFromDates($request);
        return response()->json($result);
    }

    public function saveWeekAspects(Request $request) {
       $result = $this -> planWeekService -> saveWeekAspects($request);

        $return_response = [
            'success' => $result['status'],
            'message' => $result['message'],
            'data'    => $result['data'],
        ];

       return response()->json($return_response);
    
    }

    public function deletePlanDayTimeslot(Request $request) {


        $result = $this -> planWeekService -> deletePlanDayTimeslot($request['plan_day_timeslot_id']);
        $return_response = [
            'success' => $result['status'],
            'message' => $result['message'],
        ];

        return response() -> json($return_response);
    }

    public function getPlanDayIdFromDate(Request $request) {
        $result = $this -> planWeekService -> getPlanDayIdFromDate($request);

        $return_response = [
            'success' => $result['status'],
            'message' => $result['message'],
            'plan_day_id'    => $result['data'],
        ];

       return response()->json($return_response);
    }

    public function getPlanWeekDataFromDates(Request $request) {
        $result = null;
        $result = $this -> planWeekService -> getPlanWeekDataFromDates($request);
        return response()->json($result);
    }

    public function getPlanColors(Request $request) {
        return response()->json($this -> planWeekService -> getPlanColors($request));
    }

    public function upsertPlanColor(Request $request) {
        $result = $this -> planWeekService -> upsertPlanColor($request);
        $return_response = [
            'success' => $result['status'],
            'message' => $result['message'],
        ];

        return response()->json($return_response);
    }

    public function deletePlanColor(Request $request) {

        $result = $this -> planWeekService -> deletePlanColor($request['plan_color_id']);
        $return_response = [
            'success' => $result['status'],
            'message' => $result['message'],
        ];
        return response()->json($return_response);
    }
}
