<?php

namespace App\Http\Controllers\Admin;

use App\Models\Team;
use App\Models\Room;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function index()
    {
        $rooms = Room::all();
        // $teams = Team::all();
        $teams = Team::with('room')->get();
    
        return view('frontend.team.index', compact('teams','rooms'));
    }

    public function create(Request $request)
    {

        $input = $request->all();

        if (array_key_exists('is_main_flag', $input)) {
            $team = Team::where('room_id', $input['room_id']) -> where('is_main', 1) -> where('delete_flag', 0) -> first();
            if ($team)
                return redirect()->route('team')->with('error', 'Đã có bộ phận quản trị chinh của phòng này. Bộ phận đó là '. $team -> name);

        }

        Team::firstOrCreate([
            'name' => $input['name'],
            'room_id' => $input['room_id'],
            'is_main'  => array_key_exists('is_main_flag', $input),
            'created_by' => auth()->id(),
            'updated_by' => auth()->id(),
        ]);
        return redirect()->route('team')->with('success', 'Thêm thành công');
    }

    public function edit($id)
    {
        // dd($id);
        $teams = Team::find($id);
        
        return view('frontend.team.update', compact('teams'));
    }

    public function update(Request $request, $id)
    {
        // dd($id);
        $input = $request->all();
        // dd($input);
         Team::find($id)->update($input);
        
        return redirect()->route('team')
        ->with('success', 'Sửa thành công');
    }

    public function delete( $id)
    {
        // dd($id);
         $team = Team::with(['positions'])->find($id);

         if (count($team->positions) > 0)
            return redirect()->route('team')->with('error', 'Xoá thất bại, vui lòng xóa các chức danh của bộ phận '. $team->name . ' trước!');

        $team -> delete();
        
        return redirect()->route('team') ->with('success', 'Xóa thành công');
    }
}
