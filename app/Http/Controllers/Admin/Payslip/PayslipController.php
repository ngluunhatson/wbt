<?php

namespace App\Http\Controllers\Admin\Payslip;

use App\Http\Controllers\Controller;
use App\Services\Timekeep\TimekeepService;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PayslipController extends Controller
{
    private $timekeepService;
    public function __construct(TimekeepService $timekeepService)
    {
        $this->timekeepService = $timekeepService;
    }

    public function index()
    {
        $dataInit = [];

        $dataInit['startDate'] = Carbon::now()->startOfMonth();
        $dataInit['endDate'] = Carbon::now()->endOfMonth();

        $rangeDate = CarbonPeriod::create($dataInit['startDate'],  $dataInit['endDate']);
        $workDefault = 0;
        foreach ($rangeDate as $key => $date) {
            if ($date->format('D') != 'Sun') {
                if ($date->format('D') == 'Sat') {
                    $workDefault += 0.5;
                } else {
                    $workDefault += 1;
                }
                $dataInit['header_day'][$key] = [
                    'date' => $date->format('d'),
                    'days' => $date->format('D')
                ];
            }
        }
        $dataInit['workNumber'] = $workDefault;
        $dataInit['daysInMonth'] = Carbon::now()->daysInMonth;
        $dataInit['rooms'] = $this->timekeepService->getRooms();
        $dataInit['mySalary'] = $this->timekeepService
            ->getMySalary($dataInit['startDate']->month, $dataInit['startDate']->year);

        return view('frontend.payslip.index', compact('dataInit'));
    }

    public function loadData(Request $request)
    {
        $startDate = Carbon::createFromFormat('d/m/Y', '01/' . $request['monthYear'])->startOfMonth();
        $endDate = Carbon::createFromFormat('d/m/Y', '01/' . $request['monthYear'])->endOfMonth();

        $rangeDate = CarbonPeriod::create($startDate,  $endDate);
        $workDefault = 0;
        foreach ($rangeDate as $key => $date) {
            if ($date->format('D') != 'Sun') {
                if ($date->format('D') == 'Sat') {
                    $workDefault += 0.5;
                } else {
                    $workDefault += 1;
                }
            }
        }

        $dataInit['dataList'] = $this->timekeepService
            ->adminGetPayslips($request['room_id'], $startDate->month, $startDate->year);

        $result['html'] = view('frontend.payslip.detail', compact('dataInit'))->render();
        $result['startDate'] = $startDate->format('d/m/Y');
        $result['endDate'] = $endDate->format('d/m/Y');
        $result['workNumber'] = $workDefault;
        return $result;
    }

    public function loadMyData(Request $request)
    {
        $startDate = Carbon::createFromFormat('d/m/Y', '01/' . $request['monthYearMyPayslip'])->startOfMonth();
        $endDate = Carbon::createFromFormat('d/m/Y', '01/' . $request['monthYearMyPayslip'])->endOfMonth();

        $rangeDate = CarbonPeriod::create($startDate,  $endDate);
        $workDefault = 0;
        foreach ($rangeDate as $key => $date) {
            if ($date->format('D') != 'Sun') {
                if ($date->format('D') == 'Sat') {
                    $workDefault += 0.5;
                } else {
                    $workDefault += 1;
                }
            }
        }
        $dataInit['workNumber'] = $workDefault;
        $dataInit['mySalary'] = $this->timekeepService
            ->getMySalary($startDate->month, $startDate->year);
        $result['html'] = view('frontend.payslip.personal', compact('dataInit'))->render();
        $result['startDate'] = $startDate->format('d/m/Y');
        $result['endDate'] = $endDate->format('d/m/Y');
        $result['workNumber'] = $workDefault;
        return $result;
    }
}
