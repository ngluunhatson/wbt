<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Position;
use App\Models\Team;
use Illuminate\Support\Facades\DB;

class PositionController extends Controller
{
    public function index()
    {
        $positions = Position::where('delete_flag', 0)->get();
        $teams = Team::where('delete_flag', 0)->get();
        return view('frontend.position.index', compact('teams', 'positions'));
    }

    public function create(Request $request)
    {

        $input = $request->all();
        $position_level = 0;

        if ($input['rank'] == 1)
            $position_level = 4;
        else if (in_array($input['rank'], [2,3,9]))
            $position_level = 3;
        else if ($input['rank'] == 4)
            $position_level = 2;
        else if ($input['rank'] == 5)
            $position_level = 1;


        $parent_id_input = null;

        if (array_key_exists('parent_position_id', $input)) {
            if ($input['parent_position_id']) {
                $parent_id_input = $input['parent_position_id'];
            } else
                return response()->json([
                    'status' => false,
                    'message' => 'Vui lòng thêm vị trí cấp trên cho cấp bậc ' . config('constants.rank')[$input['rank']]
                ]);
        }

        $position = Position::firstOrCreate([
            'name' => $input['name'],
            'team_id' => $input['team_id'],
            'year' => $input['year'],
            'amount' => $input['amount'],
            'rank' => $input['rank'],
            'level' => $position_level,
            'position_order' => 0,
            'flag_chart' => $input['flag_chart'] ?? 0,
            'flag_filter' => $input['flag_filter'] ?? 0,
            'created_by' => auth()->id(),
        ]);

        if ($input['child_position_id']) {
            $childPositionId = $input['child_position_id'];
            DB::select("CALL setParentIdOfPosition($childPositionId, null )");
            DB::select("CALL setParentIdOfPosition($childPositionId, $position->id )");
        }

        DB::select("CALL setParentIdOfPosition($position->id, $parent_id_input )");

        $position_order_input = 0;
        if (array_key_exists('parent_position_id', $input)  && $input['parent_position_id']) {

            $position_order = Position::where('parent_id', $parent_id_input)->where('delete_flag', 0)->where('flag_chart', 1)->max('position_order');

            $position_order_input = $position_order + 1;
        }

        $position->position_order = $position_order_input;
        $position->save();

        return response()->json([
            'status'  => true,
            'message' => 'Thêm thành công!'
        ]);
    }

    public function edit($id)
    {
        $positions = Position::find($id);
        $teams = Team::where('delete_flag', 0)->get();
        return view('frontend.position.update', compact('positions', 'teams'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        if (!array_key_exists('flag_chart', $input)) {
            $input['flag_chart'] = 0;
        }

        if (!array_key_exists('flag_filter', $input)) {
            $input['flag_filter'] = 0;
        }

        Position::find($id)->update($input);
        return redirect()->route('position')->with('success', 'Sửa thành công');
    }

    public function delete($id)
    {
        // dd($id);
        $position = Position::with(['childrenPositions', 'employees'])->find($id);

        if (count($position->childrenPositions) > 0)
            return redirect()->route('position')->with('error', 'Xóa thất bại. Vui lòng xóa các chức danh cấp dưới trước!');

        if (count($position->employees) > 0)
            return redirect()->route('position')->with('error', 'Xóa thất bại. Vẫn có nhân viên với chức danh này');

        $position->update([
            'delete_flag' => 1,
            'updated_by'  => auth()->id(),
        ]);
        return redirect()->route('position')->with('success', 'Xóa thành công');
    }

    public function getUpperPositions(Request $request)
    {
        $findRequestInput = $request->all();

        $team = Team::with(['room'])->where('id', $findRequestInput['team_id'])->first();


        if ($findRequestInput['rank'] == 7 && !($team->room->bod)) {
            return response()->json([
                'status'    => false,
                'data'      => [],
                'message'   => 'Cấp bậc Tổng Giám Đốc chỉ dành cho Bộ Phận Ban Giám Đốc!',
            ]);
        }

        if ($findRequestInput['rank'] == 6 && !($team->is_main)) {
            $tempTeam = Team::where('room_id', $team->room_id)->where('is_main', 1)->where('delete_flag', 0)->first();
            return response()->json([
                'status'    => false,
                'data'      => [],
                'message'   => 'Cấp bậc Giám Đốc chỉ dành cho Bộ Phận ' . $tempTeam->name . ' của Phòng ' . $team->room->name,
            ]);
        }

        if (in_array($findRequestInput['rank'], [1, 2, 3, 4, 9]) && ($team->is_main)) {
            $tempTeam = Team::where('room_id', $team->room_id)->where('is_main', 1)->where('delete_flag', 0)->first();
            return response()->json([
                'status'    => false,
                'data'      => [],
                'message'   => 'Bộ Phận ' . $tempTeam->name . ' của Phòng ' . $team->room->name . ' chỉ dành cho cấp bậc Giám Đốc/Trưởng Phòng'
            ]);
        }


        if (in_array($findRequestInput['rank'], [5, 6, 7])) {
            $position = Position::where('team_id', $findRequestInput['team_id'])->where('rank', $findRequestInput['rank'])->where('delete_flag', 0)->first();
            if ($position)
                return response()->json([
                    'status' => false,
                    'data'   => [],
                    'message' => 'Đã có vị trí (' . $position->name . ') cho cấp bậc ' . config('constants.rank')[$findRequestInput['rank']] . ' của bộ phận ' . $team->name,
                ]);
        }


        $rankQuery = $findRequestInput['rank'] != 9 ? $findRequestInput['rank'] : 3;
        $tempPositions1Query = Position::with(['childrenPositions'])->where('team_id', $findRequestInput['team_id'])->where('flag_chart', 1)->where('delete_flag', 0);


        if (in_array($rankQuery, [4, 5]))
            $tempPositions1Query = $tempPositions1Query->where('rank', '>', $rankQuery)->where('rank', '<', 9);
        else
            $tempPositions1Query = $tempPositions1Query->where('rank', '>', $rankQuery);



        $tempPositions1 = $tempPositions1Query->get();

        $upperPositions = $tempPositions1;

        return response()->json([
            'status'  => true,
            'data'    => $upperPositions,
            'message' => '',
        ]);
    }

    public function getLowerPositions(Request $request)
    {
        $findRequestInput = $request->all();
        $rankQuery = $findRequestInput['rank'] != 9 ? $findRequestInput['rank'] : 3;

        $lowerPositions = Position::where('parent_id', $findRequestInput['parent_position_id'])->where('rank', '<', $rankQuery)->where('flag_chart', 1)->get();

        return response()->json([
            'status'  => true,
            'data'    => $lowerPositions,
            'message' => '',
        ]);
    }
}
