<?php

namespace App\Http\Controllers\Admin\Recruitment;

use App\Http\Controllers\Controller;
use App\Http\Requests\Recruitment\RecruitmentProposalRequest;
use App\Services\Recruit\RecruitService;
use Illuminate\Http\Request;

class RecruitmentProposalController extends Controller
{

    private $recruitService;
    public function __construct(RecruitService $recruitService)
    {
        $this->recruitService = $recruitService;
    }

    public function index()
    {
        $dataInit = [];

        if (auth()->user()->hasAnyRole(['manager', 'chief', 'leader', 'specialist', 'staff'])) {
            $room_id = auth()->user()->staff->organizational->room_id;
            $dataInit['listData'] = $this->recruitService->getDataListByRoom($room_id);
        } else {
            $dataInit['listData'] = $this->recruitService->getDataList();
        }

        return view('frontend.recruitment-proposal.index', compact('dataInit'));
    }

    public function create()
    {
        $dataInit = [];

        if (auth()->user()->hasAnyPermission(['recruit_proposal-create-full-room'])) {
            $dataInit['room'] = $this->recruitService->getRooms();
        } else {
            $room_id = auth()->user()->staff->organizational->room_id;
            $dataInit['room'] = $this->recruitService->getRoomsByUser($room_id);
        }

        $dataInit['hr'] = $this->recruitService->getHR();
        $dataInit['manager'] = $this->recruitService->getCEO();
        return view('frontend.recruitment-proposal.create', compact('dataInit'));
    }

    public function storeInsert(RecruitmentProposalRequest $request)
    {
        $result = $this->recruitService->saveData($request);
        $result['link'] = route('recruitment-proposal.index');

        return $result;
    }

    public function storeUpdate(RecruitmentProposalRequest $request)
    {
        $result = $this->recruitService->updateData($request);
        $result['link'] = route('recruitment-proposal.index');

        return $result;
    }

    public function edit($id)
    {
        $dataInit = [];

        $dataInit['recruit'] = $this->recruitService->getDataByID($id)->toArray();
        $dataInit['room'] = $this->recruitService->getRooms();
        $dataInit['hr'] = $this->recruitService->getHR();
        $dataInit['manager'] = $this->recruitService->getCEO();

        return view('frontend.recruitment-proposal.edit', compact('dataInit'));
    }

    public function view($id)
    {
        $dataInit = [];

        $dataInit['recruit'] = $this->recruitService->getDataByID($id);
        if (!empty($dataInit['recruit'])) {
            $dataInit['room'] = $this->recruitService->getRooms();
            $dataInit['hr'] = $this->recruitService->getHR();
            $dataInit['manager'] = $this->recruitService->getCEO();

            return view('frontend.recruitment-proposal.view', compact('dataInit'));
        } else {
            return redirect(404);
        }
    }

    public function updateStatus(Request $request, $id)
    {
        $result = $this->recruitService->updateStatus($request, $id);

        if ($result['status']) {
            return redirect()->route('recruitment-proposal.index')->with('success', $result['message']);
        } else {
            return redirect()->route('recruitment-proposal.index')->with('error', $result['message']);
        }
    }

    public function delete($id)
    {
        $result = $this->recruitService->delete($id);

        if ($result['status']) {
            return redirect()->route('recruitment-proposal.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('recruitment-proposal.index')
                ->with('error', $result['message']);
        }
    }
}
