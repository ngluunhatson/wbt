<?php

namespace App\Http\Controllers\Admin\Description;

use App\Http\Controllers\Controller;
// use App\Http\Requests\Criteria\CriteriaRequest;
use App\Models\Organizational;
use App\Models\Position;
use App\Models\Team;
use App\Services\Description\DescriptionService;
use App\Services\Organizational\OrganizationalService;
use Illuminate\Http\Request;


class DescriptionController extends Controller
{
    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    private $descriptionService;
    public function __construct(DescriptionService $descriptionService)
    {
        $this->descriptionService = $descriptionService;
    }

    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function description()
    {
        if (auth()->user()->hasAnyRole(['manager', 'chief', 'leader', 'specialist', 'staff'])) {
            $room_id = auth()->user()->staff->organizational->room_id;
            $dataInit['listData'] = $this->descriptionService->getDataListByRoom($room_id);
            $dataInit['myWork'] = $this->descriptionService->getDataMyWork(auth()->user()->basic_info_id);
        } else {
            $dataInit['listData'] = $this->descriptionService->getDataList();
        }
        return view('frontend.description.index', compact('dataInit'));
    }

    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function view($id)
    {
        // return "test";
        $dataInit = [];
        $dataInit['descrip'] = $this->descriptionService->getDataByID($id)->toArray();
        // dd($dataInit['descrip']);
       
        $dataInit['room'] = $this->descriptionService->getRooms();
        $dataInit['hr'] = $this->descriptionService->getStaffsByTeam(6, 6, [1,3]);
        $dataInit['manager'] = $this->descriptionService->getCEO();
        return view('frontend.description.view', compact('dataInit'));
    }
}
