<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserRequest;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Support\Arr;
use Exception;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }
    public function index(Request $request)
    {

        $users = User::orderBy('id', 'DESC')->get();

        return view('frontend.users.index', compact('users'));
    }


    public function create()
    {
        if (auth()->user()->hasRole('Super-Admin')) {
            $roles = Role::pluck('name', 'name')->all();
        } else {
            $roles = Role::pluck('name', 'name')->except(['name', 'Super-Admin']);
        }

        return view('frontend.users.create', compact('roles'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'confirm-password' => 'required|same:password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        $notification = array(
            'success' => 'User created successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('users.index')
            ->with($notification);
    }

    public function show($id)
    {
        return redirect()->route('users.index');
    }

    public function edit($id)
    {
        $user = User::find($id);
        if ($user->hasRole('Super-Admin')) {
            $notification = array(
                'error' => "You have no permission for edit this user",
                'alert-type' => 'error'
            );
            return redirect()->route('users.index')
                ->with($notification);
        }
        if (auth()->user()->hasRole('Super-Admin')) {
            $roles = Role::pluck('name', 'name')->all();
        } else {
            $roles = Role::pluck('name', 'name')->except(['name', 'Super-Admin']);
        }
        $userRole = $user->roles->pluck('name', 'name')->all();

        return view('frontend.users.edit', compact('user', 'roles', 'userRole'));
    }

    public function update(UserRequest $request, $id)
    {
        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id', $id)->delete();

        $user->assignRole($request->input('roles'));

        $notification = array(
            'success' => 'User updated successfully',
            'alert-type' => 'success'
        );
        return redirect()->route('users.index')
            ->with($notification);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        if (auth()->id() == $id) {
            $notification = array(
                'error' => "You cannot delete yourself",
                'alert-type' => 'error'
            );
            return redirect()->route('users.index')
                ->with($notification);
        }
        if ($user->hasRole('Super-Admin')) {
            $notification = array(
                'error' => "You have no permission for delete this user",
                'alert-type' => 'error'
            );
            return redirect()->route('users.index')
                ->with($notification);
        }
        $user->delete();
        $notification = array(
            'error' => "User deleted successfully",
            'alert-type' => 'success'
        );
        return redirect()->route('users.index')
            ->with($notification);
    }

    public function userProfile()
    {
        // $user = auth()->user();
        return view('frontend.users.profile');
    }

    public function changePassword(Request $request)
    {   
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
            'link' => '',
        ];

        $request->validate([
            'old_password' => ['required', 'string'],
            'new_password' => ['required', 'string', 'min:6'],
            'confirm-password' => ['required|same:new_password']
        ]);
        $data = $request->except('_token');
        $userId = auth()->id();
        if (!(Hash::check($request->get('old_password'), auth()->user()->password))) {
            $result['message'] = "Password hiện tại không đúng";
            return $result;
            // return response()->json([
            //     'isSuccess' => false,
            //     'Message' => "Password hiện tại không đúng"
            // ], 200); // Status code
        } else {
            try {
                $user = User::find($userId);
                $user->password = Hash::make($data['new_password']);
                $user->update();
                $result['status'] = true;
                $result['message'] = 'Lưu thành công !!';
                $result['link'] = route('root');
            } catch (Exception $e) {
                DB::rollBack();
                Log::channel('daily')->error($e->getMessage());
            }
            
            return $result;
        }
    }
}