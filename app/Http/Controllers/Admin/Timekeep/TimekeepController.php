<?php

namespace App\Http\Controllers\Admin\Timekeep;

use App\Http\Controllers\Controller;
use App\Services\Timekeep\TimekeepService;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class TimekeepController extends Controller
{
    private $timekeepService;
    public function __construct(TimekeepService $timekeepService)
    {
        $this->timekeepService = $timekeepService;
    }

    public function index(Request $request)
    {
        $dataInit = [];

        $dataInit['startDate'] = Carbon::now()->startOfMonth();
        $dataInit['endDate'] = Carbon::now()->endOfMonth();

        $rangeDate = CarbonPeriod::create($dataInit['startDate'],  $dataInit['endDate']);
        $workDefault = 0;
        foreach ($rangeDate as $key => $date) {

            if ($date->format('D') == 'Sat') {
                $workDefault += 0.5;
            } else if ($date->format('D') != 'Sun') {
                $workDefault += 1;
            }
            $dataInit['header_day'][$key] = [
                'date' => $date->format('d'),
                'days' => $date->format('D')
            ];
        }

        $dataInit['workNumber'] = $workDefault;
        $dataInit['daysInMonth'] = Carbon::now()->daysInMonth;
        $dataInit['rooms'] = $this->timekeepService->getRooms();
        return view('frontend.timekeep.index', compact('dataInit'));
    }

    public function edit(Request $request, $id, $day)
    {
        // dump(Carbon::createFromFormat('m/Y', $request['monthYear']));
        $result = $this->timekeepService->updateStatusDay($request->all(), $id, $day);

        return $result;
    }

    public function loadData(Request $request)
    {
        $result = [];
        $dataInit = [];
        $startDate = Carbon::createFromFormat('d/m/Y', '01/' . $request['monthYear'])->startOfMonth();
        $endDate = Carbon::createFromFormat('d/m/Y', '01/' . $request['monthYear'])->endOfMonth();

        $rangeDate = CarbonPeriod::create($startDate,  $endDate);
        $workDefault = 0;
        foreach ($rangeDate as $key => $date) {

            if ($date->format('D') == 'Sat') {
                $workDefault += 0.5;
            } else   if ($date->format('D') != 'Sun') {
                $workDefault += 1;
            }
            $dataInit['header_day'][$key] = [
                'date' => $date->format('d'),
                'days' => $date->format('D')
            ];
        }

        $dataInit['daysInMonth'] = count($dataInit['header_day']);
        $dataInit['dataList'] = $this->timekeepService
            ->getDataListByRoomAndCalculateTimeKeep($request['room_id'], $startDate->month, $startDate->year);

        $result['html'] = view('frontend.timekeep.detail', compact('dataInit'))->render();
        $result['startDate'] = $startDate->format('d/m/Y');
        $result['endDate'] = $endDate->format('d/m/Y');
        $result['workNumber'] = $workDefault;
        $result['daysInMonth'] = count($dataInit['header_day']);

        return $result;
    }

    public function changeOtherFields(Request $request) {

        $result = $this -> timekeepService -> updateTimeKeepOtherFields($request -> all());

        return $result;
    }
}
