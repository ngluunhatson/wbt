<?php

namespace App\Http\Controllers\Admin\Recruit;

use App\Http\Controllers\Controller;
use App\Http\Requests\Recruitment\InterviewFormRequest;
use App\Services\Recruit\RecruitService;
use Illuminate\Http\Request;

class FormController extends Controller
{
    private $recruitService;
    public function __construct(RecruitService $recruitService)
    {
        $this->recruitService = $recruitService;
    }

    public function index()
    {
        $dataInit = [];
        $dataInit['listData'] = $this->recruitService->getDataForm();
        // if (auth()->user()->hasAnyRole(['manager', 'chief', 'leader', 'specialist', 'staff'])) {
        //     $room_id = auth()->user()->staff->organizational->room_id;
        //     $dataInit['listData'] = $this->recruitService->getDataListByRoom($room_id);
        // } else {
        //     $dataInit['listData'] = $this->recruitService->getDataForm();
        // }

        return view('frontend.recruit.form.index', compact('dataInit'));
    }

    public function create()
    {
        $dataInit = [];
        $dataInit['room'] = $this->recruitService->getRooms();
        $dataInit['hr'] = $this->recruitService->getHR();
        $dataInit['chief'] = $this->recruitService->getStaffsByPositionID(3);
        $dataInit['manager'] = $this->recruitService->getCEO();
        // if (auth()->user()->hasAnyPermission(['recruit_form-create-full-room'])) {
        //     $dataInit['room'] = $this->recruitService->getRooms();
        // } else {
        //     $room_id = auth()->user()->staff->organizational->room_id;
        //     $dataInit['room'] = $this->recruitService->getRoomsByUser($room_id);
        // }
        return view('frontend.recruit.form.create', compact('dataInit'));
    }

    public function storeInsert(InterviewFormRequest $request)
    {
        $result = $this->recruitService->saveFormInterview($request);
        $result['link'] = route('form.index');

        return $result;
    }

    public function storeUpdate(InterviewFormRequest $request)
    {
        $result = $this->recruitService->updateData($request);
        $result['link'] = route('form.index');

        return $result;
    }

    public function view()
    {
        return view('frontend.recruit.form.view');
    }

    public function edit()
    {
        return view('frontend.recruit.form.edit');
    }
}
