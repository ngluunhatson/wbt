<?php

namespace App\Http\Controllers\Admin\Recruit;

use App\Http\Controllers\Controller;
use App\Http\Requests\Recruitment\InterviewFormRequest;
use App\Http\Requests\Recruitment\InterviewFormUpdateRequest;
use App\Http\Requests\Recruitment\InterviewRequest;
use App\Services\Recruit\RecruitService;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    private $recruitService;
    public function __construct(RecruitService $recruitService)
    {
        $this->recruitService = $recruitService;
    }

    public function index()
    {
        $dataInit = [];
        $dataInit['listData'] = $this->recruitService->getDataForm();

        return view('frontend.recruit.result.index', compact('dataInit'));
    }

    public function create()
    {
        $dataInit = [];
        $dataInit['room'] = $this->recruitService->getRooms();
        $dataInit['hr'] = $this->recruitService->getHR();
        $dataInit['manager'] = $this->recruitService->getCEO();

        return view('frontend.recruit.result.create', compact('dataInit'));
    }

    public function storeInsert(InterviewFormRequest $request)
    {
        $result = $this->recruitService->saveFormInterview($request);
        $result['link'] = route('result.index');

        return $result;
    }

    public function storeUpdate(InterviewFormUpdateRequest $request)
    {
        $result = $this->recruitService->updateInterview($request);
        $result['link'] = route('result.index');

        return $result;
    }

    public function view($id)
    {
        $dataInit = [];

        $dataInit['interview'] = $this->recruitService->getDataFormByID($id);
        $dataInit['room'] = $this->recruitService->getRooms();
        $dataInit['hr'] = $this->recruitService->getHR();
        $dataInit['manager'] = $this->recruitService->getCEO();

        return view('frontend.recruit.result.view', compact('dataInit'));
    }

    public function edit($id)
    {
        $dataInit = [];

        $dataInit['interview'] = $this->recruitService->getDataFormByID($id);
        $dataInit['room'] = $this->recruitService->getRooms();
        $dataInit['hr'] = $this->recruitService->getHR();
        $dataInit['manager'] = $this->recruitService->getCEO();

        return view('frontend.recruit.result.edit', compact('dataInit'));
    }

    public function interview($id)
    {
        $dataInit = [];

        $dataInit['interview'] = $this->recruitService->getDataFormByID($id);
        $dataInit['room'] = $this->recruitService->getRooms();
        $dataInit['hr'] = $this->recruitService->getHR();
        $dataInit['manager'] = $this->recruitService->getCEO();

        return view('frontend.recruit.result.interview', compact('dataInit'));
    }

    public function interviewCandidate(InterviewRequest $request)
    {
        $result = $this->recruitService->interviewCandidate($request);
        $result['link'] = route('result.index');

        return $result;
    }

    public function delete($id)
    {
        $result = $this->recruitService->deleteInterview($id);

        if ($result['status']) {
            return redirect()->route('result.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('result.index')
                ->with('error', $result['message']);
        }
    }
}
