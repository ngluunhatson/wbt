<?php

namespace App\Http\Controllers\Admin\Dayoff;

use App\Http\Controllers\Controller;
use App\Http\Requests\DayOff\DayoffRequest;
use App\Models\Organizational;
use App\Models\User;
use App\Services\Dayoff\DayOffService;
use App\Services\TrackingDayOff\TrackingDayOffService;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;

class DayoffController extends Controller
{
    private $dayOffService, $trackingDayOffService;
    public function __construct(dayOffService $dayOffService, TrackingDayOffService $trackingDayOffService)
    {
        $this->dayOffService = $dayOffService;
        $this->trackingDayOffService = $trackingDayOffService;
    }

    public function index()
    {   
      
        $this->trackingDayOffService->populateTrackingDayOffsNew();
        $this->trackingDayOffService->updateAllTrackingDayOffsNew();
        $dataInit['listData'] = $this->dayOffService->getAllDayOffs();
        $dataInit['myDayOffs'] = $this->dayOffService->getAllDayOffsAvailableToUser(auth() -> id());
        return view('frontend.dayoff.index', compact('dataInit'));
    }


    public function view($id)
    {
        $this -> trackingDayOffService -> populateTrackingDayOffsNew();
        $this -> trackingDayOffService -> updateAllTrackingDayOffsNew();
        $dataInit = [];
        $dataInit['dayOff'] = $this->dayOffService->getDataByID($id);
        if (!empty($dataInit['dayOff'])) {
            $dataInit['basicInfo'] = $this->dayOffService->getBasicInfoById($dataInit['dayOff']['basic_info_id']);
            return view('frontend.dayoff.view', compact('dataInit'));
        } else {
            return redirect(404);
        }
    }

    public function delete($id)
    {
        $result = $this->dayOffService->delete($id);

        if ($result['status']) {
            return redirect()->route('dayoffIndex')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('dayoffIndex')
                ->with('error', $result['message']);
        }
    }

    public function edit($id)
    {
        $this -> trackingDayOffService -> populateTrackingDayOffsNew();
        $this -> trackingDayOffService -> updateAllTrackingDayOffsNew();
        $dataInit = [];
        $dataInit['dayOff'] = $this->dayOffService->getDataByID($id);
        $dataInit['rooms'] = $this->dayOffService->getRooms();
        $dataInit['hr'] = $this->dayOffService->getHR();
        $dataInit['manager'] = $this->dayOffService->getCEO();
        return view('frontend.dayoff.edit', compact('dataInit'));
    }

    public function create()
    {
        $this -> trackingDayOffService -> populateTrackingDayOffsNew();
        $this -> trackingDayOffService -> updateAllTrackingDayOffsNew();
        $dataInit = [];
        $dataInit['rooms'] = $this->dayOffService->getRooms();
        $dataInit['hr'] = $this->dayOffService->getHR();
        $dataInit['manager'] = $this->dayOffService->getCEO();

        return view('frontend.dayoff.create', compact('dataInit'));
    }

    public function storeDataInsert(DayoffRequest $request)
    {

        $result = $this->dayOffService->saveData($request);
        if ($result['status']) {
            return redirect()->route('dayoffIndex')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('dayoffIndex')
                ->with('error', $result['message']);
        }
    }

    public function storeDataUpdate(DayoffRequest $request)
    {
        $result = $this->dayOffService->updateData($request);
        if ($result['status']) {
            return redirect()->route('dayoffIndex')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('dayoffIndex')
                ->with('error', $result['message']);
        }
    }

    public function updateStatus(Request $request)
    {
        $result = $this->dayOffService->updateStatus($request);
        if ($result['status']) {
            return redirect()->route('dayoffIndex')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('dayoffIndex')
                ->with('error', $result['message']);
        }
    }

    public function getTeamByRommID($room_id)
    {
        return response()->json($this->dayOffService->getTeamsByRoomID($room_id));
    }

    public function getPositionByTeamID($team_id)
    {
        return response()->json($this->dayOffService->getPositionByTeamID($team_id));
    }

    // Remember fix in service (MVC model to respository)
    public function getStaffByConditons($room_id, $team_id, $position_id)
    {

        return response()->json($this->dayOffService->getStaffByConditions($room_id, $team_id, $position_id));
    }

    public function getByBasicInfoId($id) 
    {
        return response()->json($this->dayOffService->getAllDayOffsAvailableToUser($id, "basic_info_id"));
    }

    public function getDateCalendar(Request $request)
    {      
        $this->trackingDayOffService->populateTrackingDayOffsNew();
        $this->trackingDayOffService->updateAllTrackingDayOffsNew();
        $start_request = (int) $request->query()['start'];
        $end_request = (int) $request->query()['end'];
        $date = [];
        $dataInit['myDayOffs'] = $this->dayOffService->getAllDayOffsAvailableToUser(auth() -> id());
        
        foreach($dataInit['myDayOffs'] as $dayOff) {
            $start = $dayOff->detaildayoff->start_date;
            $end = $dayOff->detaildayoff->end_date;
            if(($start_request <= strtotime($start) || $start_request <= strtotime($end)) && ($end_request >= strtotime($start) && $end_request >= strtotime($end))) {
                $start_time = (int)$dayOff->detaildayoff->start_time < 10 ? '0'.$dayOff->detaildayoff->start_time.':00.000000Z' : $dayOff->detaildayoff->start_time.':00.000000Z';
                $end_time = (int)$dayOff->detaildayoff->end_time < 10 ? '0'.$dayOff->detaildayoff->end_time.':00.000000Z' : $dayOff->detaildayoff->end_time.':00.000000Z'; 
                array_push($date, [
                    'title' => $dayOff->title,
                    'start' => date_format($start,"Y-m-d").'T'.$start_time,
                    'end' => date_format($end,"Y-m-d").'T'.$end_time,
                    'note' => $dayOff->note,
                    'start_time' => $dayOff->detaildayoff->start_time,
                    'end_time' => $dayOff->detaildayoff->end_time,
                    'color' => '#FBE2DB',

            ]);
            }
        }
        return $date;
    }
}
