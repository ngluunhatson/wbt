<?php

namespace App\Http\Controllers\Admin\Salary;

use App\Http\Controllers\Controller;
use App\Services\Salary\SalaryService;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class SalaryController extends Controller
{
    private $salaryService;
    public function __construct(SalaryService $salaryService)
    {
        $this->salaryService = $salaryService;
    }

    public function index()
    {
        $dataInit = [];

        $dataInit['startDate'] = Carbon::now()->startOfMonth();
        $dataInit['endDate'] = Carbon::now()->endOfMonth();

        $rangeDate = CarbonPeriod::create($dataInit['startDate'],  $dataInit['endDate']);
        $workDefault = 0;
        foreach ($rangeDate as $key => $date) {
            if ($date->format('D') != 'Sun') {
                if ($date->format('D') == 'Sat') {
                    $workDefault += 0.5;
                } else {
                    $workDefault += 1;
                }
                $dataInit['header_day'][$key] = [
                    'date' => $date->format('d'),
                    'days' => $date->format('D')
                ];
            }
        }
        $dataInit['workNumber'] = $workDefault;
        $dataInit['daysInMonth'] = Carbon::now()->daysInMonth;
        $dataInit['rooms'] = $this->salaryService->getRooms();

        // dd($dataInit['dataList']);
        return view('frontend.salary.index', compact('dataInit'));
    }

    public function loadData(Request $request)
    {
        $result = [];
        $dataInit = [];
        $startDate = Carbon::createFromFormat('d/m/Y', '01/' . $request['monthYear'])->startOfMonth();
        $endDate = Carbon::createFromFormat('d/m/Y', '01/' . $request['monthYear'])->endOfMonth();

        $rangeDate = CarbonPeriod::create($startDate,  $endDate);
        $workDefault = 0;
        foreach ($rangeDate as $key => $date) {
            if ($date->format('D') != 'Sun') {
                if ($date->format('D') == 'Sat') {
                    $workDefault += 0.5;
                } else {
                    $workDefault += 1;
                }
                $dataInit['header_day'][$key] = [
                    'date' => $date->format('d'),
                    'days' => $date->format('D')
                ];
            }
        }
        $dataInit['daysInMonth'] = count($dataInit['header_day']);
        $dataInit['workNumber'] = $workDefault;


        $queryResult = $this->salaryService
            ->getDataListByRoom($request['room_id'], $startDate->month, $startDate->year, $request['basic_info_id']);
        $dataInit['dataList'] = $queryResult['data'];

        $result['html'] = view('frontend.salary.detail', compact('dataInit'))->render();
        $result['startDate'] = $startDate->format('d/m/Y');
        $result['endDate'] = $endDate->format('d/m/Y');
        $result['workNumber'] = $workDefault;
        $result['daysInMonth'] = count($dataInit['header_day']);
        $result['canUpdate']   = $queryResult['canUpdate'];
        $result['salaryStatus'] = $queryResult['salaryStatus'];

        $result['salarySetting']  = $queryResult['salarySetting'];
        $result['salaryListId']  = $queryResult['salaryListId'];

        return $result;
    }

    public function edit(Request $request)
    {
        $result = $this->salaryService->updateDataSalary($request->all());

        return $result;
    }

    public function getDetail($id)
    {
        return response()->json($this->salaryService->getDataById($id));
    }

    public function create()
    {
        $dataInit = [];
        $dataInit['hr_room_leaders'] = $this->salaryService->getRoomLeaderOrManager(6);
        $dataInit['finance_room_leaders'] = $this->salaryService->getRoomLeaderOrManager(4);
        $dataInit['ceo'] = $this->salaryService->getCEO();
        return view('frontend.salary.create', compact('dataInit'));
    }

    public function createSalaryList(Request $request)
    {

        $result = $this->salaryService->createSalaryList($request);
        $response = [
            'success' => $result['status'],
            'message' => $result['message'],
            'link'    => route('salary.index'),
        ];

        return response()->json($response);
    }

    public function confirmSalaryList(Request $request)
    {

        $result = $this->salaryService->confirmSalaryList($request);
        $response = [
            'success' => $result['status'],
        ];
        return response()->json($response);
    }

    public function saveSalarySetting(Request $request)
    {
        $response = [
            'success' => false,
            'message' => 'Lưu không thành công do thiếu thông tin'
        ];
        foreach ($request->all() as $key => $request_value) {
            if ($key != 'salary_setting_id' && !$request_value)
                return response()->json($response);
        }

        if (!$request['salary_setting_id'])
            $this->salaryService->createSalarySetting($request);
        else
            $this->salaryService->updateSalarySetting($request);

        $response = [
            'success' => true,
            'message' => 'Lưu cài đặt thành công!'
        ];
        return response()->json($response);
    }
}
