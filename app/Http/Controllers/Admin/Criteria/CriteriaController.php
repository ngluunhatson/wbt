<?php

namespace App\Http\Controllers\Admin\Criteria;

use App\Http\Controllers\Controller;
use App\Http\Requests\Criteria\CriteriaRequest;
use App\Services\Criteria\CriteriaService;
use Illuminate\Http\Request;

class CriteriaController extends Controller
{
    private $criteriaService;
    public function __construct(CriteriaService $criteriaService)
    {
        $this->criteriaService = $criteriaService;
    }

    public function index()
    {
        $dataInit = [];

        if (auth()->user()->hasAnyRole(['manager', 'chief', 'leader', 'specialist', 'staff'])) {
            $room_id = auth()->user()->staff->organizational->room_id;
            $dataInit['listData'] = $this->criteriaService->getDataListByRoom($room_id);
            $dataInit['myWork'] = $this->criteriaService->getDataMyWork(auth()->user()->basic_info_id);
        } else {
            $dataInit['listData'] = $this->criteriaService->getDataList();
        }

        return view('frontend.criteria.index', compact('dataInit'));
    }

    public function create()
    {
        $dataInit = [];

        if (auth()->user()->hasAnyPermission(['12_criteria-create-full-room'])) {
            $dataInit['room'] = $this->criteriaService->getRooms();
        } else {
            $room_id = auth()->user()->staff->organizational->room_id;
            $dataInit['room'] = $this->criteriaService->getRoomsByUser($room_id);
        }
        $dataInit['hr'] = $this->criteriaService->getHR();
        $dataInit['manager'] = $this->criteriaService->getCEO();
        return view('frontend.criteria.create', compact('dataInit'));
    }

    public function storeInsert(CriteriaRequest $request)
    {
        $result = $this->criteriaService->saveData($request);
        $result['link'] = route('criteria.index');

        return $result;
    }

    public function storeUpdate(CriteriaRequest $request)
    {
        $result = $this->criteriaService->updateData($request);
        $result['link'] = route('criteria.index');

        return $result;
    }

    public function edit($id)
    {
        $dataInit = [];

        $dataInit['criteria'] = $this->criteriaService->getDataByID($id);
        $dataInit['room'] = $this->criteriaService->getRooms();
        $dataInit['hr'] = $this->criteriaService->getHR();
        $dataInit['manager'] = $this->criteriaService->getCEO();
        return view('frontend.criteria.edit', compact('dataInit'));
    }

    public function view($id)
    {
        $dataInit = [];

        $dataInit['criteria'] = $this->criteriaService->getDataByID($id);
        if (!empty($dataInit['criteria'])) {
            $dataInit['room'] = $this->criteriaService->getRooms();
            $dataInit['hr'] = $this->criteriaService->getHR();
            $dataInit['manager'] = $this->criteriaService->getCEO();

            return view('frontend.criteria.view', compact('dataInit'));
        } else {
            return redirect(404);
        }
    }

    public function updateStatus(Request $request, $id)
    {
        $result = $this->criteriaService->updateStatus($request, $id);

        if ($result['status']) {
            return redirect()->route('criteria.index')->with('success', $result['message']);
        } else {
            return redirect()->route('criteria.index')->with('error', $result['message']);
        }
    }

    public function delete($id)
    {
        $result = $this->criteriaService->delete($id);

        if ($result['status']) {
            return redirect()->route('criteria.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('criteria.index')
                ->with('error', $result['message']);
        }
    }
}
