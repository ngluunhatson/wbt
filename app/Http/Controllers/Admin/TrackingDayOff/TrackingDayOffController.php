<?php

namespace App\Http\Controllers\Admin\TrackingDayOff;

use App\Http\Controllers\Controller;
use App\Http\Requests\TrackingDayOff\TrackingDayoffRequest;
use App\Services\TrackingDayOff\TrackingDayOffService;
use Illuminate\Http\Request;

class TrackingDayOffController extends Controller
{
    private $trackingDayOffService;
    public function __construct(TrackingDayOffService $trackingDayOffService)
    {
        $this -> trackingDayOffService = $trackingDayOffService;
    }
    public function index()
    {
        $dataInit = [];
        $this -> trackingDayOffService -> populateTrackingDayOffsNew();
        $this -> trackingDayOffService -> updateAllTrackingDayOffsNew();
        $dataInit['allTrackingDayOffsDict'] = $this -> trackingDayOffService -> getAllTrackingDayOffsDict();
        $dataInit['myTrackingDayOffsDict']   = $this -> trackingDayOffService -> getTrackingDayOffsDict( auth() -> id() );


        return view('frontend.trackingDayoff.index', compact('dataInit'));
    }
   

    public function getTrackingDayOffByBasicInfoId($id, $year) {

        return response()->json($this->trackingDayOffService->getTrackingDayOffById($id, $year, 'basic_info_id'));

    } 

   
}
