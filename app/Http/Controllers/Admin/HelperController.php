<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BasicInfo;
use App\Models\Organizational;
use App\Services\Criteria\CriteriaService;
use App\Services\Organizational\OrganizationalService;
use Illuminate\Http\Request;

class HelperController extends Controller
{
    private $organizationalService;
    public function __construct(OrganizationalService $organizationalService, CriteriaService $criteriaService)
    {
        $this->organizationalService = $organizationalService;
        $this->criteriaService = $criteriaService;
    }

    public function getRoomByBasicInfoID($basic_info_id)
    {
        return response()->json($this->organizationalService->getRoomByBasicInfoID($basic_info_id));
    }

    public function getTeamByRommID($room_id)
    {
        return response()->json($this->organizationalService->getTeamsByRoomID($room_id));
    }

    public function getPositionByTeamID($team_id)
    {
        return response()->json($this->organizationalService->getPositionByTeamID($team_id));
    }

    public function getStaffByConditons($room_id, $team_id, $position_id)
    {
        return response()->json($this->organizationalService->getStaffByConditions($room_id, $team_id, $position_id));
    }

    public function getCriteriaByConditions($room_id, $team_id, $position_id)
    {
        $conditons = [
            'room_id' => $room_id,
            'team_id' => $team_id,
            'position_id' => $position_id,
        ];

        return response()->json($this->criteriaService->getDataByConditons($conditons));
    }

    public function markAsReadNoti($id)
    {
        auth()->user()->unreadNotifications->where('id', $id)->markAsRead();
    }

    public function getMangerInRoom($room_id)
    {
        return response()->json($this->organizationalService->getMangerInRoom($room_id));
    }

    public function getStaffNotYetInChart($id) {
        $user = $this->organizationalService->getDataByID($id);
        $staffInChart = Organizational::where('id', '<>', $id)->whereNotNull('basic_info_id')->where('room_id', session('room_id'))->get()->pluck('basic_info_id');
        $staffs = BasicInfo::whereNotIn('id', $staffInChart)->where('delete_flag', 0)->get();
        return response()->json([
            'oragnizational' => $user,
            'staff' => $staffs,
        ]);
    }
}
