<?php

namespace App\Http\Controllers\Admin\Internship;

use App\Http\Controllers\Controller;
use App\Http\Requests\Internship\InternshipRequest;
use App\Services\Internship\InternshipService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InternshipController extends Controller
{

    private $internshipService;
    public function __construct(InternshipService $internshipService)
    {
        $this->internshipService = $internshipService;
    }

    public function index()
    {
        $dataInit = [];

        $dataInit['allInternships'] = $this -> internshipService -> getAllInternships();
        return view('frontend.internship.index', compact('dataInit'));
    }
    public function edit($id)
    {
        $dataInit = [];

        $dataInit['currentInternship'] = $this -> internshipService -> getInternshipById($id);
        return view('frontend.internship.edit', compact('dataInit'));
    }

    public function create()
    {
        $dataInit = [];
        $dataInit['rooms']    =  $this->internshipService->getAll_X('rooms');
        $dataInit['hr']       =  $this->internshipService->getHR();
        $dataInit['ceo']      =  $this->internshipService->getCEO();
        return view('frontend.internship.create', compact('dataInit'));
    }

    public function storeInsert(Request $request)
    {
        $result = $this->internshipService->saveData($request);
        if ($result['status']) {
            return redirect()->route('internship.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('internship.index')
                ->with('error', $result['message']);
        }
    }

    public function storeUpdate(InternshipRequest $request)
    {
        $result = $this->internshipService->updateData($request);
        if ($result['status']) {
            return redirect()->route('internship.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('internship.index')
                ->with('error', $result['message']);
        }
    }

    public function checkCreateInternship(Request $request)
    {
        $rules = [
            'basic_info_id'              => ['required', 'gt:0'], //Người Nhận Việc
            'confirm_1_basic_info_id'    => ['required', 'gt:0'], //Người Giao Việc
            'confirm_2_basic_info_id'    => ['required', 'gt:0'], //Quản Lý Phòng
            'confirm_3_basic_info_id'    => ['required', 'gt:0'], //Giám Đốc Nhân Sự
            'confirm_4_basic_info_id'    => ['required', 'gt:0'], // BGĐ
            'start_date'                 => ['required'],
            'end_date'                   => ['required'],
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => "Thiếu Thông Tin Cần Nhập"
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'message' => 'Success'
            ], 200);
        }
    }

    public function view($id)
    {
        $dataInit = [];

        $dataInit['currentInternship'] = $this -> internshipService -> getInternshipById($id);
        return view('frontend.internship.view', compact('dataInit'));
    }

    public function delete($id)
    {
        $result = $this->internshipService->delete($id);

        if ($result['status']) {
            return redirect()->route('internship.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('internship.index')
                ->with('error', $result['message']);
        }
    }

}
