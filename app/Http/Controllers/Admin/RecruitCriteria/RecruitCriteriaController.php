<?php

namespace App\Http\Controllers\Admin\RecruitCriteria;

use App\Http\Controllers\Controller;
use App\Services\Recruit\RecruitService;
use App\Services\Criteria\CriteriaService;
class RecruitCriteriaController extends Controller
{
    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    private $recruitService;
    private $criteriaService;
    public function __construct(RecruitService $recruitService, CriteriaService $criteriaService)
    {
        $this->recruitService = $recruitService;
        $this->criteriaService = $criteriaService;
    }

    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->user()->hasAnyRole(['manager', 'chief', 'leader', 'specialist', 'staff'])) {
            $room_id = auth()->user()->staff->organizational->room_id;
            $dataInit['listData'] = $this->recruitService->getDataListByRoomForRecruitCriteria($room_id);
        } else {
            $dataInit['listData'] = $this->recruitService->getDataListForRecruitCriteria();
        }
    
        return view('frontend.recruit-criteria.index', compact('dataInit'));
    }

    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function view($room_id, $team_id, $position_id)
    {
        $dataInit = [];
     
        $dataInit['recruit'] = $this->recruitService->getDataByRoomTeamPositionId($room_id, $team_id, $position_id);
        $dataInit['criteria'] = $this->criteriaService->getDataByRoomTeamPositionId($room_id, $team_id, $position_id);
        if (!empty($dataInit['recruit'])) {
            
            $dataInit['room'] = $this->recruitService->getRooms();
            $dataInit['hr'] = $this->recruitService->getStaffsByTeam(6, 19, [92, 93]);
            $dataInit['manager'] = $this->recruitService->getCEO();
            return view('frontend.recruit-criteria.view', compact('dataInit'));
        } else {
            return redirect(404);
        }
    }
    
    

    
    
}
