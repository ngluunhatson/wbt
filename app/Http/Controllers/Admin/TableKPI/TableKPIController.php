<?php

namespace App\Http\Controllers\Admin\TableKPI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Criteria\CriteriaService;
use App\Services\BasicInfo\BasicInfoService;
use App\Services\TableKpi\TableKpiService;
use App\Services\DetailKpi\DetailKpiService;
use App\Http\Requests\KPI\KpiRequest;
use PhpOffice\PhpWord\TemplateProcessor;
use Yajra\DataTables\Facades\DataTables;

class TableKPIController extends Controller
{

    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    private $criteriaService;
    private $basicInfoService;
    private $tableKpiService;
    private $detailKpiService;
    public function __construct(CriteriaService $criteriaService, BasicInfoService $basicInfoService, TableKpiService $tableKpiService, DetailKpiService $detailKpiService)
    {
        $this->criteriaService = $criteriaService;
        $this->basicInfoService = $basicInfoService;
        $this->tableKpiService = $tableKpiService;
        $this->detailKpiService = $detailKpiService;
    }
    public function tablekpi()
    {
        $dataInit = [];

        if (auth()->user()->hasAnyRole(['manager', 'chief', 'leader', 'specialist', 'staff'])) {
            $room_id = auth()->user()->staff->organizational->room_id;
            $dataInit['listData'] = $this->tableKpiService->getDataListByRoom($room_id);
            $dataInit['myWork'] = $this->tableKpiService->getDataMyWork(auth()->user()->basic_info_id);
        } else {
            $dataInit['listData'] = $this->tableKpiService->getDataList();
        }

        return view('frontend.kpi.index', compact('dataInit'));
    }

    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $dataInit = [];
        $idPerson = $id;
        $dataInit['editByRecive'] = $this->tableKpiService->checkUserCanBeReceived($id, auth()->user()->basic_info_id);
        $dataInit['kpi'] = $this->tableKpiService->getDataByID($id);
        $bsID = $dataInit['kpi']['basic_info_id'];
        $person = $this->basicInfoService->getById($bsID);
        // dd($dataInit['kpi'] );
        $dataInit['detailKpi'] = $this->detailKpiService->getDataByKpiID($id)->toArray();
        // dd($dataInit['detailKpi'][0]);
        if (auth()->user()->hasAnyPermission(['table_kpi-create-full-room'])) {
            $dataInit['room'] = $this->tableKpiService->getRooms();
        } else {
            $room_id = auth()->user()->staff->organizational->room_id;
            $dataInit['room'] = $this->tableKpiService->getRoomsByUser($room_id);
        }
        $dataInit['hr'] = $this->tableKpiService->getHR();
        $dataInit['manager'] = $this->tableKpiService->getCEO();
        return view('frontend.kpi.edit', compact('dataInit', 'idPerson', 'person'));
    }

    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $dataInit = [];

        if (auth()->user()->hasAnyPermission(['table_kpi-create-full-room'])) {
            $dataInit['room'] = $this->tableKpiService->getRooms();
        } else {
            $room_id = auth()->user()->staff->organizational->room_id;
            $dataInit['room'] = $this->tableKpiService->getRoomsByUser($room_id);
        }

        $dataInit['hr'] = $this->tableKpiService->getHR();
        $dataInit['manager'] = $this->tableKpiService->getCEO();
        return view('frontend.kpi.create', compact('dataInit'));
    }


    public function storeInsert(KpiRequest $request)
    {
        // dd($request['selectConfirm']);
        $result = $this->tableKpiService->saveData($request);
        $result['link'] = route('tablekpi');
        return $result;
    }

    public function storeUpdate(KpiRequest $request)
    {

        $data = $request->all();
        $id = $data['kpis_id'];
        $dataInit['detailKpi'] = $this->detailKpiService->getDataByKpiID($id)->toArray();
        $idDetails = [];
        foreach ($dataInit['detailKpi'] as $detail) {
            array_push($idDetails, $detail['id']);
        }
        $result = $this->tableKpiService->updateData($request, $idDetails);
        $result['link'] = route('tablekpi');

        return $result;
    }

    public function delete($id)
    {

        $result = $this->tableKpiService->deleteByID($id);

        if ($result['status']) {
            return redirect()->route('tablekpi')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('tablekpi')
                ->with('error', $result['message']);
        }
    }

    public function view($id)
    {

        $dataInit = [];

        $dataInit['kpi'] = $this->tableKpiService->getDataByID($id);
        $dataInit['detailKpi'] = $this->detailKpiService->getDataByKpiID($id)->toArray();
        $dataInit['room'] = $this->tableKpiService->getRooms();
        $dataInit['hr'] = $this->tableKpiService->getHR();
        $dataInit['manager'] = $this->tableKpiService->getCEO();


        return view('frontend.kpi.view', compact('dataInit'));
    }

    public function updateStatus(Request $request, $id)
    {
        $result = $this->tableKpiService->updateStatus($request, $id);

        if ($result['status']) {
            return redirect()->route('tablekpi')->with('success', $result['message']);
        } else {
            return redirect()->route('tablekpi')->with('error', $result['message']);
        }
    }

    public function loadData(Request $request)
    {

        if (auth()->user()->hasAnyRole(['manager', 'chief', 'leader', 'specialist', 'staff'])) {
            $room_id = auth()->user()->staff->organizational->room_id;
            $data = $this->tableKpiService->getDataListByRoom($room_id);
        } else {
            $data = $this->tableKpiService->getDataList();
        }
        if (!empty($data)) {

            $param = $request->all();

            if (!empty($param['quarter'])) {
                $data = $data->where('quarter', $param['quarter']);
            }

            if (!empty($param['year'])) {
                $data = $data->where('year', $param['year']);
            }

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('quarter', function ($row) {
                    return $row->quarter;
                })
                ->addColumn('year', function ($row) {
                    return $row->year;
                })
                ->addColumn('staff_kpi', function ($row) {
                    if (!empty($row->confirms)) {
                        foreach ($row->confirms as $data) {
                            if (!empty($data->confirmReceive)) {
                                return !empty($data->confirmReceive->staff) ? $data->confirmReceive->staff->full_name : 'Chưa có';
                            }
                        }
                    }
                })
                ->addColumn('staff_assign_kpi', function ($row) {
                    return $row->user->staff->full_name ?? $row->user->name;
                })
                ->editColumn('room', function ($row) {
                    return $row->room->name;
                })
                ->editColumn('team', function ($row) {
                    return $row->team->name;
                })
                ->editColumn('position', function ($row) {
                    return $row->position->name;
                })
                ->editColumn('status', function ($row) {
                    return config('constants.status_confirm.kpi')[$row['status']];
                })
                ->editColumn('action', function ($row) {
                    $html = '<td>
                            <div class="dropdown d-inline-block">
                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-fill align-middle"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-end">';
                    if (auth()->user()->hasAnyPermission(['table_kpi-read'])) {
                        $html .= '<li><a href="/tablekpi/view/' . $row->id . '" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i>
                        View</a></li>';
                    }

                    if (auth()->user()->hasAnyPermission(['table_kpi-edit'])) {
                        if (auth()->id() == $row->created_by) {
                            if (in_array($row->status, [1, 9, 10])) {
                                $html .= '<li><a href="/tablekpi/edit/' . $row->id . '" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i>
                                                Edit</a>
                                        </li>';
                            }
                        } else {
                            $canEdit = $this->tableKpiService->checkUserCanBeReceived($row->id, auth()->user()->basic_info_id);
                            if ($canEdit && in_array($row->status, [4, 11, 12, 13])) {
                                $html .= '<li><a href="/tablekpi/edit/' . $row->id . '" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i>
                                                Edit</a>
                                        </li>';
                            }
                        }
                    }

                    if (auth()->user()->hasAnyPermission(['table_kpi-delete'])) {
                        if (auth()->id() == $row->created_by && in_array($row->status, [1, 9, 10])) {
                            $token = csrf_token();
                            $url = route("tablekpi.delete", $row->id);
                            $html .= '<form method="post"  class="formDelete" action="' . $url . '" style="display:inline">
                                        <input hidden name ="_method" value="delete">
                                        <input hidden name="_token" value="' . $token . '" >
                                        <a href="#" type="submit" class="dropdown-item remove-item-btn">
                                            <i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Delete
                                        </a>
                                    </form>';
                        }
                    }
                    return $html;
                })
                ->make(true);
        }
    }

    public function loadDataMyWork(Request $request)
    {

        $data = $this->tableKpiService->getDataMyWork(auth()->user()->basic_info_id, $request['quarter'], $request['year']);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('quarter', function ($row) {
                return $row->kpi->quarter;
            })
            ->addColumn('year', function ($row) {
                return $row->kpi->year;
            })
            ->addColumn('staff_kpi', function ($row) {
                if (!empty($row->confirmReceive)) {
                    return $row->confirmReceive->staff->full_name;
                }
            })
            ->addColumn('staff_assign_kpi', function ($row) {
                return $row->kpi->user->staff->full_name;
            })
            ->editColumn('room', function ($row) {
                return $row->kpi->room->name;
            })
            ->editColumn('team', function ($row) {
                return $row->kpi->team->name;
            })
            ->editColumn('position', function ($row) {
                return $row->kpi->position->name;
            })
            ->editColumn('status', function ($row) {
                return config('constants.status_confirm.kpi')[$row->kpi->status];
            })
            ->editColumn('action', function ($row) {
                $html = '<td>
                            <div class="dropdown d-inline-block">
                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-fill align-middle"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-end">';
                if (auth()->user()->hasAnyPermission(['table_kpi-read'])) {
                    $html .= '<li><a href="/tablekpi/view/' . $row->kpi->id . '" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i>
                        View</a></li>';
                }

                if (auth()->user()->hasAnyPermission(['table_kpi-edit'])) {
                    if (auth()->id() == $row->created_by) {
                        if (in_array($row->kpi->status, [1, 9, 10])) {
                            $html .= '<li><a href="/tablekpi/edit/' . $row->kpi->id . '" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i>
                                                Edit</a>
                                        </li>';
                        }
                    } else {
                        $canEdit = $this->tableKpiService->checkUserCanBeReceived($row->kpi->id, auth()->user()->basic_info_id);
                        if ($canEdit && in_array($row->kpi->status, [4, 11, 12, 13])) {
                            $html .= '<li><a href="/tablekpi/edit/' . $row->kpi->id . '" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i>
                                                Edit</a>
                                        </li>';
                        }
                    }
                }

                if (auth()->user()->hasAnyPermission(['table_kpi-delete'])) {
                    if (auth()->id() == $row->created_by && in_array($row->kpi->status, [1, 9, 10])) {
                        $token = csrf_token();
                        $url = route("tablekpi.delete", $row->kpi->id);
                        $html .= '<form method="post" class="formDelete" action="' . $url . '" style="display:inline">
                                        <input hidden name ="_method" value="delete">
                                        <input hidden name="_token" value="' . $token . '" >
                                        <a href="#" type="submit" class="dropdown-item remove-item-btn">
                                            <i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Delete
                                        </a>
                                    </form>';
                    }
                }
                return $html;
            })
            ->make(true);
    }

    public function exportFile($id)
    {
        $dataInit['kpi'] = $this->tableKpiService->getDataByID($id);
        $dataInit['detailKpi'] = $this->detailKpiService->getDataByKpiID($id)->toArray();

        $templateProcessor = new TemplateProcessor(storage_path() . '/template/kpi.docx');
        $totalPro = 0;
        $totalReal = 0;
        $templateProcessor->setValues([
            'room_name' => $dataInit['kpi']['room']['name'],
            'team_name' => $dataInit['kpi']['team']['name'],
            'position_name' => $dataInit['kpi']['position']['name'],
            'quarter' => $dataInit['kpi']['quarter'],
            'year' => $dataInit['kpi']['year'],
        ]);
        $text = new \PhpOffice\PhpWord\PhpWord();
        $section = $text->addSection();
        $someHTMLcode = '<ul><li>abvasdasd</li><li>ascascas</li></ul>';
        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $someHTMLcode, false, false);
        dd($section->getElements()[0]->getElements()[0]->getText());

        for ($i = 1; $i <= count($dataInit['detailKpi']); $i++) {
            $templateProcessor->setComplexValue('content_kpis_'.$i, $text);
            $templateProcessor->setValues([
                'proportion_' . $i => $dataInit['detailKpi'][$i - 1]['proportion'] . '%',
                'reality_' . $i => $dataInit['detailKpi'][$i - 1]['reality'],
                'result_' . $i => $dataInit['detailKpi'][$i - 1]['result'] . '%',
                'note_' . $i => $dataInit['detailKpi'][$i - 1]['note'],
            ]);

            $totalPro += $dataInit['detailKpi'][$i - 1]['proportion'];
            $totalReal += $dataInit['detailKpi'][$i - 1]['result'];
        }

        $templateProcessor->setValues([
            'total_proport' => $totalPro . '%',
            'total_real' => $totalReal . '%',
        ]);

        if (!empty($dataInit['kpi']['delivery_date'])) {
            if (!empty($dataInit['kpi']['review_date'])) {
                $date = \Carbon\Carbon::parse($dataInit['kpi']['review_date']);
                $converDate =  'Ngày ' . $date->day . ' Tháng ' . $date->month . ' Năm ' . $date->year;
                $templateProcessor->setValue('date_review', $converDate);
            } else {
                $date = \Carbon\Carbon::parse($dataInit['kpi']['delivery_date']);
                $converDate =  'Ngày ' . $date->day . ' Tháng ' . $date->month . ' Năm ' . $date->year;
                $templateProcessor->setValue('date_review', $converDate);
            }
        } else {
            $templateProcessor->setValue('date_review', '');
        }

        foreach ($dataInit['kpi']['confirms'] as $person) {
            switch ($person['confirm']['type_confirm']) {
                case 1:
                    $templateProcessor->setValue('person_receive', $person['confirm']['staff']['full_name']);
                    switch ($person['confirm']['status']) {
                        case 0:
                            $templateProcessor->setValue('status_receive', 'Chưa xác nhận');
                            break;
                        case 1:
                            $templateProcessor->setValue('status_receive', 'Đã xác nhận');
                            break;
                        case 2:
                            $templateProcessor->setValue('status_receive', 'Từ chối');
                            break;
                    }
                    break;
                case 99:
                    $templateProcessor->setValue('person_assign', $person['confirm']['staff']['full_name']);
                    switch ($person['confirm']['status']) {
                        case 0:
                            $templateProcessor->setValue('status_person', 'Chưa xác nhận');
                            break;
                        case 1:
                            $templateProcessor->setValue('status_person', 'Đã xác nhận');
                            break;
                        case 2:
                            $templateProcessor->setValue('status_person', 'Từ chối');
                            break;
                    }
                    break;
                case 2:
                    $templateProcessor->setValue('hr_assign', $person['confirm']['staff']['full_name']);
                    switch ($person['confirm']['status']) {
                        case 0:
                            $templateProcessor->setValue('status_hr', 'Chưa xác nhận');
                            break;
                        case 1:
                            $templateProcessor->setValue('status_hr', 'Đã xác nhận');
                            break;
                        case 2:
                            $templateProcessor->setValue('status_hr', 'Từ chối');
                            break;
                    }
                    break;
                case 3:
                    $templateProcessor->setValue('ceo_assign', $person['confirm']['staff']['full_name']);
                    switch ($person['confirm']['status']) {
                        case 0:
                            $templateProcessor->setValue('status_ceo', 'Chưa xác nhận');
                            break;
                        case 1:
                            $templateProcessor->setValue('status_ceo', 'Đã xác nhận');
                            break;
                        case 2:
                            $templateProcessor->setValue('status_ceo', 'Từ chối');
                            break;
                    }
                    break;
            }
        }
        $fileName = rawurlencode('kpi_' . time() . '.docx');
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');
        header("Content-Disposition: attachment; filename=\"{$fileName}\"");
        header('Cache-Control: max-age=0');
        $templateProcessor->saveAs('php://output');
        exit;
    }
}
