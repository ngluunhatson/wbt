<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Home\HomeService;

use App\Http\Requests\BasicDatabaseRequest;
use App\Models\BasicInfo;
use App\Models\Organizational;
use App\Models\SalaryList;
use App\Models\Timekeeping;
use App\Services\Salary\SalaryService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PersonalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $homeService, $salaryService;
    public function __construct(HomeService $homeService, SalaryService $salaryService)
    {
        $this->middleware('auth');
        $this->homeService = $homeService;
        $this->salaryService = $salaryService;
    }


    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function personal()
    {
        if (!auth()->user()->hasAnyPermission(['personal-view-all'])) {

            // return redirect()->route('personal.view', ['id' => auth()->user()->id]);
            if (!isset(auth()->user()->basic_info_id))
                return redirect()->back();
            return redirect()->route('personal.view', ['id' => auth()->user()->basic_info_id]);
        }

        if (auth()->user()->hasAnyRole(['manager', 'chief', 'leader', 'specialist', 'staff'])) {
            $room_id = auth()->user()->staff->organizational->room_id;
            $idInRoom = Organizational::where('room_id', $room_id)->pluck('basic_info_id');
            $persons = BasicInfo::with('organizationalSpecial.positionSpecial')
                ->whereIn('id', $idInRoom)->where('delete_flag', '=', '0')->get();
        } else {
            $persons = BasicInfo::with('organizationalSpecial.positionSpecial')->withCount(['organizationalSpecial'])
                ->where('delete_flag', '=', '0')->get();
            // dd($persons);
        }
        return view('frontend.personal.index', compact('persons'));
    }

    public function delete($id)
    {
        $result = $this->homeService->deleteBasicInfo($id);
        $basic_info_id = $id;
        $result = $this->homeService->deletePersonalInfo($basic_info_id);
        $result = $this->homeService->deleteRelativeInfo($basic_info_id);
        $result = $this->homeService->deleteLaborContract($basic_info_id);

        if ($result['status']) {
            return redirect('personal')->with('success', $result['message']);
        } else {
            return redirect('personal')->with('error', $result['message']);
        }
    }

    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function editPersonal($id)
    {
        $basic = BasicInfo::find($id);
        $person = $basic->personInfo ?? [];
        $relative = [];
        $relative['priority'] = $basic->relativePriority;
        $relative['child'] = $basic->relativeChild;

        $labors['hdld'] = $basic->laborContract;
        $labors['bbt'] = $basic->larborBbt;

        return view('frontend.personal.editPersonal', compact('basic', 'person', 'relative', 'labors'));
    }

    public function view($id)
    {
        $basic = BasicInfo::find($id);
        $person = $basic->personInfo ?? [];
        $relative = [];
        $relative['priority'] = $basic->relativePriority;
        $relative['child'] = $basic->relativeChild;

        $labors['hdld'] = $basic->laborContract;
        $labors['bbt'] = $basic->larborBbt;

        return view('frontend.personal.view', compact('basic', 'person', 'relative', 'labors'));
    }

    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function add()
    {
        $dataInit = [];
        $dataInit['rooms'] = $this->homeService->getRooms();

        return view('frontend.personal.create', compact('dataInit'));
    }

    /**
     * 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(BasicDatabaseRequest $request)
    {
        $result = $this->homeService->saveBasicInfo($request);

        if ($result['status']) {
            $dateEntry = Carbon::parse($request['date_work']);
            $monthDateEntry     = $dateEntry->month;
            $yearDateEntry      = $dateEntry->year;
            $basic_info_id = $result['basic_info_id'];

            $salaryList = SalaryList::where('month', $monthDateEntry)->where('year', $yearDateEntry)->first();
            $timekeepId = null;
            if ($monthDateEntry != 2) {
                $value = DB::select("CALL generateTimeKeep($monthDateEntry, $yearDateEntry, $basic_info_id )");
                $timekeepId = $value[0]->timekeeping_id;
            } else {
                $firstDay = Carbon::create($yearDateEntry, $monthDateEntry, 1);
                $create_input = [
                    'month'         => $monthDateEntry,
                    'year'          => $yearDateEntry,
                    'with_salary'   => 0,
                    'paid_holidays' => 0,
                    'no_salary'     => 0,
                    'other'         => 0,
                    'total_salary'  => 0,
                ];

                $isLeapYear = false;
                if ($yearDateEntry % 4 == 0)
                    if ($yearDateEntry % 100 == 0) {
                        if ($yearDateEntry % 400 == 0)
                            $isLeapYear = true;
                    } else
                        $isLeapYear = true;
                
                $tempDayOfWeek = $firstDay->dayOfWeek;
                for ($i = 1; $i <= ($isLeapYear ? 29 : 28); $i++) {
                    if (in_array($tempDayOfWeek, [1,2,3,4,5]))
                        $create_input['day_'.$i] = 2;
                    else if ($tempDayOfWeek == 6)
                        $create_input['day_'.$i] = 1;
                    else if ($tempDayOfWeek == 7) {
                        $create_input['day_'.$i] = 0;
                        $tempDayOfWeek = 0;
                    }
                    $tempDayOfWeek += 1;
                }

                if (!$isLeapYear) 
                    $create_input['day_29'] = 0;
                $create_input['day_30'] = 0;
                $create_input['day_31'] = 0;

                $create_input['basic_info_id'] = $basic_info_id;
                $timekeepId = (Timekeeping::firstOrCreate($create_input))->id;
            }
            
            if ($salaryList && $salaryList->status != 5) {
                $salaryIds = $salaryList->salary_ids;
                if (!empty($value)) {
                    $salary = $this->salaryService->createSalary($salaryList->id, $timekeepId, $basic_info_id, null);
                    $salaryIds[] = $salary->id;
                    $salaryList->salary_ids = $salaryIds;
                    $salaryList->save();
                }
            }
            return redirect()->route('personal')->with('success', $result['message']);
        } else {
            return redirect()->back()->withInput()->with('error', $result['message']);
        }
    }


    public function update(BasicDatabaseRequest $request, $id)
    {

        $input = $request->all();


        $update_basic_info_input = [
            'company'                   => $input['company'],
            'rank'                      => $input['rank'],
            'full_name'                 => $input['full_name'],
            'english_name'              => $input['name_eng'],
            'employee_code'             => $input['code_staff'],
            'date_of_entry_to_work'     => $input['date_work'],
            'gender'                    => $input['genders'],
            'nationality'               => $input['nation'],
            'phone'                     => $input['phone'],
            'personal_email'            => $input['email'],
            'company_email'             => $input['email_company'],
            'dob'                       => $input['birth_date'],
            'disc'                      => "",
            'motivators'                => "",
            'cv'                        => "",
            'created_by'                => auth()->id(),
            'updated_by'                => auth()->id(),
        ];


        $update_personal_info_input = [
            'ethnic'                        => $input['ethnic'],
            'cccd'                          => $input['passport'],
            'date_range'                    => $input['issue_date'],
            'issued_by'                     => $input['providers'],
            'permanent_address'             => $input['permanent_address'],
            'temporary_residence_address'   => $input['temporary_address'],
            'social_insurance_code'         => $input['insurance_code'],
            'health_insurance_code'         => $input['medical_code'],
            'healthcare_place'              => $input['address_kcb'],
            'personal_income_tax'           => $input['tax'],
            'dependent_person'              => $input['dependent_person'],
            'details_dependent_person'      => $input['npt'],
            'academic_level'                => $input['level'],
            'school'                        => $input['school'],
            'specialized'                   => $input['specialized'],
            'number_bank_account'           => $input['tknh'],
            'bank_name'                     => $input['bank'],
            'license_plates'                => $input['license_plates'],
            'range_of_vehicle'              => $input['carriage_category'],
            'updated_by'                    => auth()->id(),

        ];

        $update_relative_info_input = [
            "priority_id_1"     => $input["priority_id_1"],
            "prioritized_1" => $input["prioritized_1"],
            "relationship_1" => $input["relationship_1"],
            "phone_prioritized_1" => $input["phone_prioritized_1"],
            "priority_id_2" => $input["priority_id_2"],
            "prioritized_2" => $input["prioritized_2"],
            "relationship_2" => $input["relationship_2"],
            "childPriority_id_1" => $input["childPriority_id_1"],
            "phone_prioritized_2" => $input["phone_prioritized_2"],
            "child_1" => $input["child_1"],
            "gender_child_1" => $input["gender_child_1"],
            "birth_date_child_1" => $input["birth_date_child_1"],
            "childPriority_id_2" => $input["childPriority_id_2"],
            "child_2" => $input["child_2"],
            "gender_child_2" => $input["gender_child_2"],
            "birth_date_child_2" => $input["birth_date_child_2"],
            "childPriority_id_3" => $input["childPriority_id_3"],
            "child_3" => $input["child_3"],
            "gender_child_3" => $input["gender_child_3"],
            "birth_date_child_3" => $input["birth_date_child_3"],
            'updated_by'                    => auth()->id(),
        ];

        $update_labor_contract_input = [
            "labor_id_1" =>  $input["labor_id_1"],
            "number_hdld_1" =>  $input["number_hdld_1"],
            "contract_1" =>  $input["contract_1"],
            "date_form_1" =>  $input["date_form_1"],
            "to_form_1" =>  $input["to_form_1"],
            "wage_hdld_1" =>  $input["wage_hdld_1"],
            "number_plhd_1" => $input["number_plhd_1"],
            "date_form_plhd_1" => $input["date_form_plhd_1"],
            "wage_plhd_1" => $input["wage_plhd_1"],
            "wage_kpi_1" => $input["wage_kpi_1"],
            "diff_1" => $input["diff_1"],
            "labor_id_2" =>  $input["labor_id_2"],
            "number_hdld_2" =>  $input["number_hdld_2"],
            "contract_2" => $input["contract_2"],
            "date_form_2" => $input["date_form_2"],
            "to_form_2" => $input["to_form_2"],
            "wage_hdld_2" => $input["wage_hdld_2"],
            "number_plhd_2" => $input["number_plhd_2"],
            "date_form_plhd_2" => $input["date_form_plhd_2"],
            "wage_plhd_2" => $input["wage_plhd_2"],
            "wage_kpi_2" => $input["wage_kpi_2"],
            "diff_2" =>  $input["diff_2"],
            "labor_id_3" => $input["labor_id_3"],
            "number_hdld_3" =>  $input["number_hdld_3"],
            "contract_3" => $input["contract_3"],
            "date_form_3" => $input["date_form_3"],
            "to_form_3" => $input["to_form_3"],
            "wage_hdld_3" => $input["wage_hdld_3"],
            "number_plhd_3" => $input["number_plhd_3"],
            "date_form_plhd_3" => $input["date_form_plhd_3"],
            "wage_plhd_3" => $input["wage_plhd_3"],
            "wage_kpi_3" => $input["wage_kpi_3"],
            "diff_3" => $input["diff_3"],
            "bbt_id_id" => $input["bbt_id_id"],
            "number_bbtlhd" => $input["number_bbtlhd"],
            "date_form_bbtlhd" => $input["date_form_bbtlhd"],
            "number_qdtv" => $input["number_qdtv"],
            "date_form_qdtv" => $input["date_form_qdtv"],
            'updated_by'                    => auth()->id(),
        ];


        $result = $this->homeService->updateBasicInfo($update_basic_info_input, $id, $request);


        $result = $this->homeService->updatePersonalInfo($update_personal_info_input, $id);
        $result = $this->homeService->updateRelativeInfo($update_relative_info_input);
        $result = $this->homeService->updateLaborContract($update_labor_contract_input);


        if ($result['status']) {
            if (auth()->user()->basic_info_id == $id) {
                $this->homeService->notifyToAdmin($id);
            }
            return redirect('personal')->with('success', $result['message']);
        } else {
            return redirect('personal')->with('error', $result['message']);
        }
    }
}
