<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Http\Controllers\Controller;
use App\Services\Blog\BlogService;
// use Illuminate\Support\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class BlogController extends Controller
{
    private $blogService;
    public function __construct(BlogService $blogService)
    {
        $this->blogService = $blogService;
    }

    public function index()
    {
        $dataInit = [];
        // if(auth()->user()->hasAnyRole(['manager', 'chief', 'leader', 'specialist', 'staff'])) {
        $dataInit['blogs'] = $this->blogService->getBlogs();
        // }
        return view('frontend.blog.admin.index', compact('dataInit'));
    }

    public function create()
    {
        $dataInit = [];
        // if(auth()->user()->hasAnyRole(['manager', 'chief', 'leader', 'specialist', 'staff'])) {
        $dataInit['categories'] = $this->blogService->getCategories();
        $dataInit['rooms'] = $this->blogService->getRooms();
        // }
        return view('frontend.blog.admin.create', compact('dataInit'));
    }

    public function storeInsert(Request $request)
    {
        // return $request;
        // Form validation
        $result = $this->blogService->saveData($request);
        $result['link'] = route('blog.index');
        return $result;

        // Find category
        // $category = Category::find($input['category_id']);

        // Record to database
        // Blog::create([
        //     'language_id' => getLanguage()->id,
        //     'category_name' => $category->category_name,
        //     'category_id' => $input['category_id'],
        //     'title' => $input['title'],
        //     'desc' => Purifier::clean($input['desc']),
        //     'blog_image' => $input['blog_image'],
        //     'author' => $input['author'],
        //     'tag' => $input['tag'],
        //     'status' => $input['status']
        // ]);

        // return redirect()->route('blog.index')
        //     ->with('success', 'content.created_successfully');
    }

    public function edit($id)
    {
        $dataInit = [];
        // if(auth()->user()->hasAnyRole(['manager', 'chief', 'leader', 'specialist', 'staff'])) {
        $dataInit['blog'] = $this->blogService->getBlogById($id);
        $dataInit['categories'] = $this->blogService->getCategories();
        $dataInit['rooms'] = $this->blogService->getRooms();
        $dataInit['teams'] = $this->blogService->getTeamsByRoomID($dataInit['blog']->room_id);

        $dataInit['positions'] = $this->blogService->getPositionByTeamID($dataInit['blog']->team_id);
        $dataInit['staffs'] = $this->blogService->getStaffByConditions($dataInit['blog']->room_id, $dataInit['blog']->team_id, $dataInit['blog']->position_id);
        // dd($dataInit['staffs']);
        // die();

        // dd($dataInit['rooms']);
        // }
        return view('frontend.blog.admin.edit', compact('dataInit'));
    }

    public function storeUpdate(Request $request)
    {
        // Form validation
        $result = $this->blogService->updateData($request);
        $result['link'] = route('blog.index');
        return $result;
    }

    public function delete($id)
    {
        $result = $this->blogService->delete($id);

        if ($result['status']) {
            return redirect()->route('blog.index')
                ->with('success', $result['message']);
        } else {
            return redirect()->route('blog.index')
                ->with('error', $result['message']);
        }
    }


    public function viewPost($category)
    {
        $data = [];
        if ($category === 'rules') {
            // return  auth()->user()->organizational->room_id;
            $data['blogs'] = $this->blogService->getBlogsByCategory('noiqui', $category);
            // dd($data['blogs']);
            // return $data['blogs'];
        } else {
            $data['blogs'] = $this->blogService->getBlogsByCategory('truyenthong', $category);
        }
        return view('frontend.blog.post.index', compact('data'));
    }

    public function viewPostDetail($category, $id)
    {
        $checkAuthor = $this->blogService->checkRuleAccessDetailBlog($id);

        if ($checkAuthor == false) {
            abort(403);
        }

        $data = [];

        if ($category === 'rules') {
            $data['blog'] = $this->blogService->getDetailBlog('noiqui', $id, $category);
        } else {
            $data['blog'] = $this->blogService->getDetailBlog('truyenthong', $id, $category);
        }

        //get comment by blog id
        $data['comment'] = $this->blogService->getCommentByBlogId($id);
        // dump($data['comment']);
        // die();

        return view('frontend.blog.post.detail', compact('data'));
    }

    private function getConditionByCate(string $rule): string
    {
        $rules = [
            'rules' => 'noiqui',
            'media' => 'truyenthong',
        ];

        return Arr::get($rules, 'rule', 'noiqui');
    }

    public function viewPostSearch(Request $request, $category)
    {
        // return $request->searchPost;
        // return $request->searchPost;
        $blogs = $this->blogService->getPostWithCondition($this->getConditionByCate($category), $category, $request);
        // $data['link'] = route('blog.viewPost', $category);


        // return response()->json($blogs);
        return view('frontend.blog.post.list', compact('blogs'));
    }

    public function viewPostFilter(Request $request, $category)
    {
        // return $request;
        // dd(\Carbon\Carbon::parse('1/'.$request->end)->addMonth()->addMonth()->format('Y-m-d')->valueOf());
        // return $request;
        // return \Carbon\Carbon::createFromFormat('d/m/Y', '01/'.$request->month)->format('Y-m-d');
        $blogs = [];
        if ($category === 'rules') {
            $blogs = $this->blogService->getPostWithCondition('noiqui', $category, $request);
            // $data['link'] = route('blog.viewPost', $category);
        }
        if ($category === 'media') {
            $blogs = $this->blogService->getPostWithCondition('truyenthong', $category, $request);
            // $data['link'] = route('blog.viewPost', $category);
        }
        // return $blogs;
        // return response()->json($blogs);
        return view('frontend.blog.post.list', compact('blogs'));
    }

    public function getTeamByRommID($room_id)
    {
        return response()->json($this->blogService->getTeamsByRoomID($room_id));
    }

    public function getPositionByTeamID($team_id)
    {
        return response()->json($this->blogService->getPositionByTeamID($team_id));
    }

    // Remember fix in service (MVC model to respository)
    public function getStaffByConditons($room_id, $team_id, $position_id)
    {

        return response()->json($this->blogService->getStaffByConditions($room_id, $team_id, $position_id));
    }


    public function view()
    {
        return view('frontend.plan.weekly.view');
    }
}
