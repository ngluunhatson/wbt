<?php

namespace App\Http\Controllers;

use App\Http\Requests\Organ\OrganRequest;
use App\Models\BasicInfo;
use App\Models\Organizational;
use App\Models\Position;
use App\Models\Room;
use App\Models\Team;
use App\Models\Tree;
use App\Services\Organizational\OrganizationalService;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TreeController extends Controller
{
    private $organizationalService;
    public function __construct(OrganizationalService $organizationalService)
    {
        $this->organizationalService = $organizationalService;
    }

    public function index(Request $request)
    {
        $tree = [];
        $dataOfSelect = $this->organizationalService->getConditions($request['room_id']);
        if ($request['room_id']) {
            session()->put('room_id', $request['room_id']);
            $tree = $this->organizationalService->getDataByRoomID($request['room_id']);
        } else {
            session()->forget('room_id');
        }

        return view('frontend.chart.tree.index', compact('tree', 'dataOfSelect'));
    }

    public function add(Request $request)
    {
        $result = [];

        $input = $request->except('_token', 'action', 'tree_id', 'myfile');

        $input['_lft'] = 0;
        $input['_rgt'] = 0;

        $result = $this->organizationalService->createData($input);
        $result['status'] = true;

        if (array_key_exists('showhideval', $input)) {
            $input['hide'] = $input['showhideval'] == 'on' ? 1 : 0;
        }

        return response()->json($result);
    }

    public function showFormEdit($room_id, $team_id, $position_id, $rank)
    {
        $user = $this->organizationalService->getStaffByConditions($room_id, $team_id, $position_id)->sortBy('basic_info_id');
        $staffInChart = Organizational::whereNotNull('basic_info_id')->get()->pluck('basic_info_id');    
        if ($rank != 9) {
            $staffs = BasicInfo::whereNotIn('id', $staffInChart)->where('rank', $rank)->where('delete_flag', 0)->get();
        } else {
            $staffs = BasicInfo::whereNotIn('id', $staffInChart)->whereIn('rank', [2, 3])->where('delete_flag', 0)->get();
        }
        return response()->json([
            'oragnizational' => $user,
            'staff' => $staffs,
        ]);
    }

    public function showFormAdd($room_id, $team_id, $position_id, $rank)
    {
        $user = $this->organizationalService->getStaffByConditions($room_id, $team_id, $position_id);
        if (empty($user->toArray())) {
            $room = Room::where('id', $room_id)->first();
            $team = Team::where('id', $team_id)->first();
            $position = Position::where('id', $position_id)->first();
            $organ = [
                'room' => $room,
                'team' => $team,
                'position' => $position,
            ];
        } else {
            $organ = $user;
        }


        $staffInChart = Organizational::whereNotNull('basic_info_id')->get()->pluck('basic_info_id');
        if ($rank != 9) {
            $staffs = BasicInfo::whereNotIn('id', $staffInChart)->where('rank', $rank)->where('delete_flag', 0)->get();
        } else {
            $staffs = BasicInfo::whereNotIn('id', $staffInChart)->whereIn('rank', [2, 3])->where('delete_flag', 0)->get();
        }
        

        return response()->json([
            'oragnizational' => $organ,
            'staff' => $staffs,
        ]);
    }

    public function edit(Request $request, $id)
    {
        $result = [];
        try {
            $input = $request->except('_token', 'action', 'tree_id');

            if (array_key_exists('showhideval', $input)) {
                $input['hide'] = $input['showhideval'] == 'on' ? 1 : 0;
            }

            $result['data'] = $this->organizationalService->updateData($input, $id);
            $result['status'] = true;
            $result['message'] = 'Thanh cong';
        } catch (Exception $e) {
            $result['status'] = false;
            $result['message'] = 'Khong thanh cong';
        }

        return response()->json($result);
    }

    public function action(Request $request)
    {

        $action = $_REQUEST['action'];
        $target_dir = 'uploads/img/chart-avatar/';

        // Delete a branch to the tree and returning a true or false
        if ($action == 'delete') {
            $ids = $_REQUEST['id'];
            $result = false;
            try {
                Organizational::fixTree();
                Organizational::whereIn('id', $ids)->delete();
                $result = true;
            } catch (Exception $e) {
                Log::channel('daily')->error($e->getMessage());
            }

            return $result;
            exit;
        }

        // Update a branch's parent id on the drag and drop event
        if ($action == 'drag') {;
            $parent_id = $request['parent_id'];
            $id = $request['id'];
            Organizational::fixTree();
            Organizational::find($id)->update([
                'parent_id' => $parent_id
            ]);
        }

        // Return add form HTML code on clicking of add icon
        if ($action == 'addform') {
            $csrf = $request['_token'];
            $room = Room::where('id', session('room_id'))->first();
            $teams = Team::where('room_id', session('room_id'))->get();
            $option = '<select name="team_id" class="form-select form-select-sm  mb-3" aria-label=".form-select-sm example">';
            $option .= "<option value=''> Vui lòng chọn bộ phận </option>";

            foreach ($teams as $team) {
                $option .= "<option value='$team->id'> $team->name </option> <br>";
            }
            $option .= '</select>';

            $positions = Position::all();
            $option1 = '<select name="position_id" class="form-select form-select-sm  mb-3" aria-label=".form-select-sm example">';
            $option1 .= "<option value=''> Vui lòng chọn chức danh </option>";

            foreach ($positions as $position) {
                $option1 .= "<option value='$position->id'> $position->name </option> <br>";
            }
            $option1 .= '</select>';

            $staffInChart = Organizational::whereNotNull('basic_info_id')->where('room_id', session('room_id'))->get()->pluck('basic_info_id');

            $staffs = BasicInfo::whereNotIn('id', $staffInChart)->where('delete_flag', 0)->get();
            $optionStaff = '<select id="selectStaff" name="basic_info_id" data-choices class="form-select form-select-sm  mb-3" aria-label=".form-select-sm example">';
            $optionStaff .= "<option value=''> Vui lòng chọn nhân viên </option>";

            foreach ($staffs as $staff) {
                $optionStaff .= "<option value='$staff->id'> Mã nv : $staff->employee_code || Tên : $staff->full_name </option> <br>";
            }
            $optionStaff .= '</select>';
            // dd($option);
            echo <<<EOL
            <form class="add_data" method="post" action="" enctype="multipart/form-data">
                <input hidden value="$csrf" name="_token"/>
                <img class="close" id="insertClose" src="/assets/chartImg/close.png" />
                <div class="img"><img id="avatarInsert" src="/assets/chartImg/user.jpg" /></div>
                <label class="title">Nhân viên</label>

                $optionStaff

                <div>
                    <label for="readonlyInput" class="form-label">Phòng</label>
                    <input type="text" class="form-control" id="readonlyInput" value="$room->name" readonly>
                </div>
                <div>
                    <label for="readonlyInput" class="form-label">Bộ phận</label>
                    $option
                </div>
                <div>
                    <label for="readonlyInput" class="form-label">Chức danh</label>
                    $option1
                </div>

                <div class="form-group">
                    <label for="datepicker" class="form-label">Thời gian</label>
                    <input type="text" id="selectYear" name="year" class="date form-control">
                </div>

                <input type="submit" class="submit mt-2" name="submit" value="Submit">
            </form>
        EOL;
            exit;
        }

        // Return edit form HTML code on clicking of edit icon
        if ($action == 'editform') {
            $csrf = $request['_token'];
            $edit_ele_id = $_REQUEST['edit_ele_id'];
            $sql = "SELECT * FROM tree WHERE id = $edit_ele_id";
            $query = mysqli_query($con, $sql);
            $csrf = csrf_token();
            while ($row = mysqli_fetch_array($query)) {
                $id = $row['id'];
                $title = $row['title'];
                $description = $row['description'];
                $img = $row['img'] ? $row['img'] : "user.jpg";
                $parent_id = $row['parent_id'];
                $hide = $row['hide'];
                $checked = $hide == 1 ? "checked" : "";
                echo <<<EOL
        <form class="edit_data" method="post" action="" enctype="multipart/form-data">
        <input hidden value="{$csrf}" name="_token"/>
            <img class="close" id="insertClose" src="/assets/chartImg/close.png" />
            <div class="img">
                <img  src="/assets/chartImg/{$img}" />
            </div>
            <input  class="form-control" type="file" id="myfile" name="myfile">
            <label class="title">Nhân viên</label>
            <select class="form-select form-select-sm  mb-3" aria-label=".form-select-sm example">
                    <option selected>LOPMASE</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
                <div>
                    <label for="datepicker" class="form-label">Thời gian</label>
                    <input type="date" id="datepicker" class="form-control">
                </div>

                <div class="form-check form-switch form-switch-secondary mt-2">
                    <input class="form-check-input" type="checkbox" {$checked} value="{$hide}" role="switch" name="showhideval" id="hide">
                    <label class="form-check-label" for="hide">Hide Child Branch</label>
                </div>
            <input type="submit" class="edit" name="submit" value="Update">
        </form>
    EOL;
            }
            exit;
        }

        // Return branch's details HTML code on clicking of view button
        if ($action == 'viewdetail') {
            $view_ele_id = $_REQUEST['view_ele_id'];
            $sql = "SELECT * FROM tree WHERE id = $view_ele_id";
            $query = mysqli_query($con, $sql);
            while ($row = mysqli_fetch_array($query)) {
                $id = $row['id'];
                $title = $row['title'];
                $description = $row['description'];
                $img = $row['img'] ? $row['img'] : "user.jpg";
                $parent_id = $row['parent_id'];
                $hide = $row['hide'];
                $checked = $hide == 1 ? "checked" : "";
                echo <<<EOL
                <div class="tree_view_popup"><div>
                    <img class="close" src="/assets/chartImg/close.png">
                    <span class="img">
                        <img src="/assets/chartImg/{$img}"></span>
                        <span class="description">
                            <b>Code ID : </b> Mã nhân viên
                        </span>
                        <span class="title">
                            <b>Name : </b> {$title}
                        </span>
                        <span class="description">
                            <b>Phòng : </b> Công nghệ thuê ngoài
                        </span>
                        <span class="description">
                            <b>Bộ phận : </b> Bộ phận s...
                        </span>
                        <span class="description">
                            <b>Chức danh : </b> CEO
                        </span>
                    </div>
                </div>
            EOL;
            }
            exit;
        }
        mysqli_close($con);
    }

    public function clearSession($key)
    {
        session()->forget($key);
        return response()->json([
            'status' => true
        ]);
    }

    public function getAvatar($id)
    {
        $result = BasicInfo::find($id);

        return response()->json($result);
    }

    public function getChartBasic()
    {
        $data = Room::with('teams.positions.employees.staff')->orderBy('bod', 'desc')->get();
        return response()->json($data);
    }

    public function getChartRoom($id)
    {
        $data = Position::with(['team' => function ($query){
            $query->select('id', 'name', 'room_id');
        }, 'employees' => function($query) use ($id) {
            $query->where('room_id', $id);
        }, 'employees.staff', 'team.room'])->whereHas('team', function ($query) use ($id) {
            return $query->where('room_id', $id);
        })->where('flag_chart', 1)->get()->toTree();
        return response()->json($data);
    }

    public function getPerson($id)
    {
        $data = BasicInfo::with(['organizational.room', 'organizational.team', 'organizational.position'])->find($id);
        return response()->json($data);
    }

    public function storeUpdate(OrganRequest $request)
    {
        $result = [];
        try {
            $input = $request->except('_token', 'amount');
            for($i = 1 ; $i <= $request['amount']; $i++) {
                $position = Position::where('id', $input['position_id'])->first();
                if ($position->rank == 9) {
                    $name = explode('CHUYÊN VIÊN / NHÂN VIÊN', $position->name)[1];
                    $staff = BasicInfo::where('id', $input['basic_info_id_' . $i])->first();
                    if (!empty($staff) ){
                        $position = Position::where('name', config('constants.rank')[$staff->rank] . $name)->first();
                        if (!empty($position)) {
                            $chartExits = Organizational::where('room_id', $input['room_id'])
                            ->where('team_id', $input['team_id'])->where('position_id', $position->id)
                            ->first();
                            if (empty($chartExits)) {
                                Organizational::create([
                                    'created_by' => auth()->id(),
                                    'room_id' => $input['room_id'],
                                    'team_id' => $input['team_id'],
                                    'position_id' => $position->id,
                                    'basic_info_id' => $input['basic_info_id_' . $i], 
                                ]);
                            } else {
                                if ($input['basic_info_id_' . $i] != null) {
                                    if ($chartExits->basic_info_id != $input['basic_info_id_' . $i]) {
                                        Organizational::create([
                                            'created_by' => auth()->id(),
                                            'room_id' => $input['room_id'],
                                            'team_id' => $input['team_id'],
                                            'position_id' => $position->id,
                                            'basic_info_id' => $input['basic_info_id_' . $i], 
                                        ]);
                                    }
                                } else {
                                    Organizational::find($chartExits->id)->update([
                                        'basic_info_id' => $input['basic_info_id_' . $i], 
                                    ]);
                                }
                            }
                        }
                    } else {
                        if ($input['idOrgan_' . $i] != 'undefined' ) {
                            $staffOld = BasicInfo::where('id', Organizational::where('id', $input['idOrgan_' . $i])->first()->basic_info_id)->first();
                            if (!empty($staffOld)) {
                                $position = Position::where('name', config('constants.rank')[$staffOld->rank] . $name)->first();
                                if ($input['basic_info_id_' . $i] != null) {
                                    Organizational::where('room_id', $input['room_id'])
                                    ->where('team_id', $input['team_id'])->where('position_id', $position->id)->first()->update([
                                        'basic_info_id' => $input['basic_info_id_' . $i], 
                                    ]);
                                } else {
                                    Organizational::where('position_id', $position->id)->where('basic_info_id', $staffOld->id)->delete();
                                }
                            }
                        }
                    }
                }

                if ($input['idOrgan_' . $i] == 'undefined' ) {
                    Organizational::create([
                        'created_by' => auth()->id(),
                        'room_id' => $input['room_id'],
                        'team_id' => $input['team_id'],
                        'position_id' => $input['position_id'],
                        'basic_info_id' => $input['basic_info_id_' . $i], 
                    ]);
                } else {
                    Organizational::find($input['idOrgan_' . $i])->update([
                        'basic_info_id' => $input['basic_info_id_' . $i], 
                    ]);
                }
            }
            $result['status'] = true;
            $result['message'] = 'Thanh cong';
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['status'] = false;
            $result['message'] = 'Khong thanh cong';
        }

        return response()->json($result);
    }
}
