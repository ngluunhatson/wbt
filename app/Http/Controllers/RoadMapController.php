<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Services\Organizational\OrganizationalService;

class RoadMapController extends Controller
{
    private $organizationalService;
    public function __construct(OrganizationalService $organizationalService)
    {
        $this->organizationalService = $organizationalService;
    }

    public function index()
    {
        // return 'test';
        $dataInit = [];
        // return 'test';
        $dataInit['directorate'] = $this->organizationalService->getRoomDirectorate();
        $dataInit['plan10y'] = $this->organizationalService->getPlan10y();

        // $dataInit['plan_10y'] = $this
        
        // dd($dataInit['directorate']);
        return view('frontend.roadmap.index', compact('dataInit'));
    }

    public function update(Request $request) {
        // dd($request);
        // return $request;
        // return 'day la trang post';
        $result = $this->organizationalService->update10y($request);
        // return $result;
        return redirect()->back();
    }

    // public 
}
