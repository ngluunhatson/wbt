<?php

namespace App\Http\Controllers\User\Comment;

use App\Http\Controllers\Controller;
use App\Services\Comment\CommentService;

use Illuminate\Http\Request;

class CommentController extends Controller
{
    private $commentService;

    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    public function storeCommentPost(Request $request)
    {
        $result = $this->commentService->saveData($request);
        return $result;
    }

    public function storeReplyComment($id,Request $request)
    {
        $result = $this->commentService->saveDataReplyComment($request, $id);


        if($result['status'])
        return redirect()->back()->withInput()->with('success', $result['message']);

        return redirect()->back()->withInput()->with('error', $result['message']);
    }

    public function deleteCommentPost($id)
    {
        $result = $this->commentService->delete($id);
        if ($result['status']) {
            return redirect()->back()
                ->withInput()
                ->with('success', $result['message']);
        } else {
            return redirect()->back()
                ->withInput()
                ->with('error', $result['message']);
        }
    }

    public function deleteReplyComment($id) {
        $result = $this->commentService->deleteReplyComment($id);

        if ($result['status']) {
            return redirect()->back()
                ->withInput()
                ->with('success', $result['message']);
        } else {
            return redirect()->back()
                ->withInput()
                ->with('error', $result['message']);
        }
    }
}
