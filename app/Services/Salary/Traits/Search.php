<?php

namespace App\Services\Salary\Traits;

use App\Models\BasicInfo;
use App\Models\Confirm;
use App\Models\SalaryList;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{
    public function getRooms()
    {
        $result = [];
        try {
            $rooms = $this->roomRepository->findAll();
            $result = !empty($rooms) ? $rooms : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataListByRoom($roomID, $month, $year, $basic_info_id)
    {
        $result = [
            'data'          => [],
            'canUpdate'     => false,
            'salaryStatus'  => null,
            'salarySetting' => null,
            'salaryListId'  => null,
        ];
        try {
            $salaryList = SalaryList::with(['salaries', 'salarySetting',  'salaryConfirms.confirm'])->where('month', $month)->where('year', $year)->first();
            $data = [];

            if ($salaryList) {
                if ($basic_info_id) {
                    $count = 0;
                    foreach ($salaryList->salaryConfirms as $salaryConfirm) {
                        if ($salaryConfirm->confirm->basic_info_id == $basic_info_id) {
                            break;
                        }
                        $count++;
                    }
                    if ($count == (count($salaryList->salaryConfirms))) 
                        return $result;
                    
                }

                $data =  $salaryList->salaries;

                if ($roomID != -1)
                    $data = $data->where('staff.organizational.room_id', $roomID);


                $result['canUpdate'] = auth()->user()->hasAnyPermission(['salary-update']) && $salaryList && (!in_array($salaryList->status, [2, 3, 4, 5]));
                $result['salaryStatus']  =  $salaryList->status;
                $result['salarySetting'] = $salaryList->salarySetting;
                $result['salaryListId']  = $salaryList->id;
            }

            $result['data'] = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataById($id)
    {
        $result = null;
        try {
            $result = $this->salaryRepository->getDataById($id);

            if ($result->salaryList->status != 5) {
                $result = null;
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getRoomLeaderOrManager($room_id)
    {
        $result = [];
        try {

            $cur_collection  = BasicInfo::with(['organizational'])->get()->where('organizational.room_id', $room_id)->where('delete_flag', 0);;

            $potential_basic_infos = $cur_collection->whereIn('rank', [4, 5]);

            if (count($potential_basic_infos) == 0) {
                $potential_basic_infos = $cur_collection->where('rank', 6)->where('delete_flag', 0);

                if (count($potential_basic_infos) == 0) {
                    $potential_basic_infos = BasicInfo::where('rank', 7)->where('delete_flag', 0)->get();
                }
            }

            $result = $potential_basic_infos->all();
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
