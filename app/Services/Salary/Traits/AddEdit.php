<?php

namespace App\Services\Salary\Traits;

use App\Models\BasicInfo;
use App\Models\Confirm;
use App\Models\Organizational;
use App\Models\Salary;
use App\Models\SalaryList;
use App\Models\SalaryListConfirm;
use App\Models\SalarySetting;
use App\Models\Timekeeping;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait AddEdit
{
    public function updateDataSalary($input)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $this->typeOfColumnUpdate($input);
            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function typeOfColumnUpdate($input)
    {
        $newValue = $input['newValue'];
        $salary_id = $input['salary_id'];
        $income_id = array_key_exists('income_id', $input) ? $input['income_id'] : null;
        $support_id = array_key_exists('income_id', $input) ? $input['support_id'] : null;
        $allowance_id = array_key_exists('income_id', $input) ? $input['allowance_id'] : null;
        $insurance_id = array_key_exists('income_id', $input) ? $input['insurance_id'] : null;
        $tax_id = array_key_exists('income_id', $input) ? $input['tax_id'] : null;
        $pay_id = array_key_exists('income_id', $input) ? $input['pay_id'] : null;
        $work_number = $input['workNumber'];
        $work_salary = $input['workSalary'];


        switch ($input['type']) {
            case "salary":
                switch ($input['column']) {
                    case 'official_contract_date':
                        Salary::where('id', $salary_id)->update([
                            'official_contract_date' => $newValue,
                            'updated_by'             => auth()->id()
                        ]);
                        break;

                    case 'type_contact':
                        DB::select("CALL calculateSalary_Typecontact($newValue,$salary_id, $insurance_id, $allowance_id, $tax_id, $pay_id)");
                        break;
                    case 'other_support':
                        DB::select("CALL calculateSalary_OtherSupport($newValue, $salary_id, $support_id, $income_id, $pay_id, $work_number, $work_salary)");
                        break;
                    case 'note':
                        $this->salaryRepository->update($salary_id, [
                            'note' => $newValue,
                        ]);
                        break;
                }
                break;
            case "income":
                switch ($input['column']) {
                    case 'salary_kpis':
                        DB::select("CALL updateIncome_Salarykpi($newValue, $income_id, $pay_id)");
                        break;

                    case 'overtime_salary':
                        DB::select("CALL updateIncome_Overtime($newValue, $income_id, $pay_id)");
                        break;

                    case 'additional_adjustments':
                        DB::select("CALL updateIncome_Adjust($newValue, $income_id, $pay_id)");
                        break;

                    case 'adjustment_of_deductions':
                        DB::select("CALL updateIncome_Deductions($newValue, $income_id, $pay_id)");
                        break;
                }
                break;
            case "support":
                switch ($input['column']) {
                    case 'main_salary':
                        DB::select("CALL updateSupport_salary($newValue, $work_number, $work_salary, 
                            $support_id, $salary_id, $income_id, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                        break;

                    case 'food_support':
                        DB::select("CALL updateSupport_supportEat($newValue, $work_number, $work_salary, 
                            $support_id, $salary_id, $income_id, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                        break;

                    case 'support_uniforms':
                        DB::select("CALL updateSupport_supportUniform($newValue, $work_number, $work_salary, 
                            $salary_id, $support_id, $income_id, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                        break;

                    case 'job_performance':
                        DB::select("CALL updateSupport_jobPerformance($newValue, $work_number, $work_salary, 
                            $salary_id, $support_id, $income_id, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                        break;
                }
                break;
            case "allowance":
                switch ($input['column']) {
                    case 'overtime_salary':
                        DB::select("CALL updateAllowance_overtimeSalary($newValue, $income_id, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                        break;

                    case 'additional_adjustments':
                        DB::select("CALL updateAllowance_additionalAdjustments($newValue, $income_id, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                        break;

                    case 'adjustment_of_deductions':
                        DB::select("CALL updateAllowance_adjustmentDeductions($newValue, $income_id, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                        break;
                }
                break;
            case "insurance":
                switch ($input['column']) {
                    case 'number_of_dependents':
                        DB::select("CALL updateInsurance_SLNPT($newValue, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                        break;
                }
                break;
            case "tax":
                switch ($input['column']) {
                    case 'tax_10':
                        DB::select("CALL updateTax_10percent($newValue, $tax_id, $allowance_id, $insurance_id, $pay_id)");
                        break;
                    case 'tax_20':
                        DB::select("CALL updateTax_20percent($newValue, $tax_id, $allowance_id, $insurance_id, $pay_id)");
                        break;

                    case 'other_recoveries':
                        DB::select("CALL updateTax_otherRecoveries($newValue, $tax_id, $allowance_id, $insurance_id, $pay_id)");
                        break;

                    case 'other_refunds':
                        DB::select("CALL updateTax_otherRefunds($newValue, $tax_id, $allowance_id, $insurance_id, $pay_id)");
                        break;
                }
                break;
            case "pay":
                switch ($input['column']) {
                    case 'company_account_2':
                        DB::select("CALL updatePay_Company2($newValue, $pay_id)");
                        break;
                    case 'personal_account_2':
                        DB::select("CALL updatePay_Personal2($newValue, $pay_id)");
                        break;

                    case 'company_account_3':
                        DB::select("CALL updatePay_Company3($newValue, $pay_id)");
                        break;

                    case 'personal_account_3':
                        DB::select("CALL updatePay_Personal3($newValue, $pay_id)");
                        break;
                }
                break;
        }
    }

    public function createSalary($salary_list_id, $timekeeping_id, $basic_info_id, $previousStaffSalary)
    {
        $result = null;
        DB::beginTransaction();
        try {
            $salary = new Salary();
            if ($previousStaffSalary) {
                $income = $previousStaffSalary->income->replicate();
                $income->created_at = Carbon::now();
                $income->created_by = auth()->id();
                $income->save();

                $support = $previousStaffSalary->support->replicate();
                $support->created_at = Carbon::now();
                $support->created_by = auth()->id();
                $support->save();

                $allowance = $previousStaffSalary->allowance->replicate();
                $allowance->created_at = Carbon::now();
                $allowance->created_by = auth()->id();
                $allowance->save();

                $insurance = $previousStaffSalary->insurance->replicate();
                $insurance->created_at = Carbon::now();
                $insurance->created_by = auth()->id();
                $insurance->save();

                $tax = $previousStaffSalary->tax->replicate();
                $tax->created_at = Carbon::now();
                $tax->created_by = auth()->id();
                $tax->save();

                $pay = $previousStaffSalary->pay->replicate();
                $pay->created_at = Carbon::now();
                $pay->created_by = auth()->id();
                $pay->save();

                $salary = $previousStaffSalary->replicate();
            } else {
                $income = $this->incomeRepository->forceCreate([
                    'created_by' => auth()->id() ?? 1,
                ]);
                $support = $this->supportRepository->forceCreate([
                    'created_by' => auth()->id() ?? 1,
                ]);
                $allowance = $this->allowanceRepository->forceCreate([
                    'created_by' => auth()->id() ?? 1,
                ]);
                $insurance = $this->insuranceRepository->forceCreate([
                    'created_by' => auth()->id() ?? 1,
                ]);
                $tax = $this->taxRepository->forceCreate([
                    'created_by' => auth()->id() ?? 1,
                ]);
                $pay = $this->payRepository->forceCreate([
                    'created_by' => auth()->id() ?? 1,
                ]);
                $salary->basic_info_id = $basic_info_id;
            }

            $salary->salary_list_id = $salary_list_id;
            $salary->timekeeping_id = $timekeeping_id;
            $salary->income_id = $income->id;
            $salary->support_id = $support->id;
            $salary->allowance_id = $allowance->id;
            $salary->insurance_id = $insurance->id;
            $salary->tax_id = $tax->id;
            $salary->pay_id = $pay->id;
            $salary->created_by = auth()->id();
            $salary->created_at = Carbon::now();
            $salary->save();

            $result = $salary;

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function createSalaryList($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];
        DB::beginTransaction();
        try {

            $create_input = $request->all();
            $monthYearStr = $create_input['monthYear'];
            $month_int    = intval(substr($monthYearStr, 0, 3));
            $year_int     = intval(substr($monthYearStr, 3));


            $monthNameStr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][$month_int - 1];

            $dateCarbon = Carbon::parse('last day of ' . $monthNameStr . ' ' . $year_int);
            $previousDateCarbon = Carbon::parse('last day of ' . $monthNameStr . ' ' . $year_int)->subMonths(1)->subDays(10);
        

            $salary_list_exist = SalaryList::where('month', $month_int)->where('year', $year_int)->where('delete_flag', 0)->first();
            $timekeeps = Timekeeping::where('month', $month_int)->where('year', $year_int)->where('delete_flag', 0)->get();

            if (count($timekeeps) == 0) {
                $result['message'] = 'Chưa Có Bảng Chấm Công Nào cho Tháng Này!';
                return $result;
            }

            if ($salary_list_exist) {
                $result['message'] = 'Đã Có Bảng Lương cho Tháng Này!';
                return $result;
            }


            $salaryList = SalaryList::create([
                'salary_ids'    => [],
                'confirm_ids'   => [],
                'status'        => 1,
                'year'          => $year_int,
                'month'         => $month_int,
                'created_by'    => auth()->id(),
            ]);

            $previousSalaryList = SalaryList::with(['salaries', 'salarySetting'])->where('month', $previousDateCarbon->month)->where('year', $previousDateCarbon->year)->where('delete_flag', 0)->first();
            $staffs = BasicInfo::with(['larborBbt', 'timekeeps'])->where('delete_flag', 0)->get();

            if ($previousSalaryList) {
                $salarySetting = $previousSalaryList->salarySetting->replicate();
                $salarySetting->salary_list_id =  $salaryList->id;
                $salarySetting->created_at = Carbon::now();
                $salarySetting->created_by = auth()->id();
                $salarySetting->save();
            } else {
                SalarySetting::create([
                    'salary_list_id'  => $salaryList->id,
                    'input_column_27' => 11000000,
                    'input_column_29' => 4400000,
                    'input_column_30' => 8,
                    'input_column_31' => 1.5,
                    'input_column_32' => 1,
                    'input_column_34' => 17,
                    'input_column_35' => 3,
                    'input_column_36' => 1,
                    'input_column_37' => 0.5,
                    'salary_base'     => 1490000,
                    'salary_min'      => 4680000,
                ]);
            }

            $salaryIds = [];

            foreach ($staffs as $staff) {
                $isStaffOff = $staff->larborBbt->from_date_qdtv  && strtotime($staff->larborBbt->from_date_qdtv) <= strtotime($dateCarbon);
                if (!$isStaffOff) {
                    $staffTimeKeep = $staff->timekeeps->where('month', $month_int)->where('year', $year_int)->first();

                    if ($staffTimeKeep) {
                        $previousStaffSalary = null;
                        if ($previousSalaryList)
                            $previousStaffSalary = $previousSalaryList->salaries->where('basic_info_id', $staff->id)->first();
                        $staffSalary = $this->createSalary($salaryList->id, $staffTimeKeep->id, $staff->id, $previousStaffSalary);

                        $salaryIds[] = $staffSalary->id;
                    }
                }
            }

            $confirmIds = [];
            foreach ($create_input['type_confirms'] as $key => $typeConfirm) {
                $confirmX = Confirm::create([
                    'basic_info_id'     => $create_input['select_confirms_basic_info_ids'][$key],
                    'type_confirm'      => $typeConfirm,
                    'assignment_type'   => config('constants.assignment_type')[11],
                    'status'            => 0,
                    'created_by'        => auth()->id(),
                ]);

                SalaryListConfirm::create([
                    'salary_list_id'    => $salaryList->id,
                    'confirm_id'        => $confirmX->id,
                ]);


                $confirmIds[] = $confirmX->id;
            }

            $salaryList->salary_ids = $salaryIds;
            $salaryList->confirm_ids = $confirmIds;

            $salaryList->save();

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function confirmSalaryList($request)
    {
        $result = [
            'status' => false,
        ];

        DB::beginTransaction();
        try {

            $confirmInput = $request->all();
            $salaryList = SalaryList::where('id', $confirmInput['salary_list_id'])->first();

            $newData = [
                'status'     => $salaryList->status,
                'updated_by' => auth()->id(),
            ];


            $notiMessage =  'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
            $link = route('salary.index');
            $notiTargets = [];
            $notiTargetIdsSent = [];

            if ($confirmInput['next_confirm_basic_info_id']) {
                $notiTargets = [User::where('basic_info_id', $confirmInput['next_confirm_basic_info_id'])->first()];
            }

            if (!in_array($salaryList->status, [2, 3, 4, 5]))
                $newData['status'] = 2;
            else {
                $isConfirmed = $confirmInput['confirm_flag'] == 1;

                if (!$isConfirmed) {
                    $notiTargets = [User::where('id', $salaryList->user->id)->first()];
                    $notiMessage = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                }

                switch ($salaryList->status) {
                    case (2):
                        $newData['status'] = $isConfirmed ? 3 : 6;
                        break;
                    case (3):
                        $newData['status'] = $isConfirmed ? 4 : 7;
                        break;
                    case (4):
                        $newData['status'] = $isConfirmed ? 5 : 8;

                        if ($isConfirmed) {
                            $link = route('payslip.index');
                            $notiMessage = "BGĐ đã xác nhận lương cho tháng này";
                            $newData['review_date'] =  Carbon::now()->format('Y-m-d');
                            $notiTargets = User::where('delete_flag', 0)->get()->unique('basic_info_id');
                        } else {
                            $notiTargets[] = $salaryList->user;
                            foreach ($salaryList->confirms as $salaryListConfirm) {
                                $notiTargets[] = User::where('basic_info_id', $salaryListConfirm->confirm->staff->id)->first();
                            }
                        }
                }
            }

            foreach ($notiTargets as $notiTarget) {
                if (!in_array($notiTarget->id, $notiTargetIdsSent)) {
                    $this->sendNotification($notiTarget, $salaryList->id, $link, $notiMessage);
                    $notiTargetIdsSent[] = $notiTarget->id;
                }
            }


            SalaryList::where('id', $confirmInput['salary_list_id'])->update($newData);

            if (array_key_exists('confirm_id_to_update_status', $confirmInput))
                $this->confirmRepository->update($confirmInput['confirm_id_to_update_status'], [
                    'status'     => $confirmInput['confirm_flag'],
                    'note'       => $confirmInput['reason'],
                    'updated_by' => auth()->id(),
                ]);

            $result['status'] = true;

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function createSalarySetting($request)
    {
        $result = null;
        DB::beginTransaction();
        try {
            $createSalarySettingInput = $request->except('_token', 'salary_setting_id');

            $salaryList = SalaryList::where('id', $createSalarySettingInput['salary_list_id'])->first();

            $monthNameStr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][$salaryList->month - 1];

            $dateCarbon = Carbon::parse('last day of ' . $monthNameStr . ' ' . $salaryList->year);
            $nextDateCarbon = $dateCarbon->addDays(1);
            $nextSalaryList = SalaryList::with(['salaries'])->where('month', $nextDateCarbon->month)->where('year', $nextDateCarbon->year)->where('delete_flag', 0)->first();



            $salarySetting = SalarySetting::firstOrCreate($createSalarySettingInput);
            if ($nextSalaryList) {
                $salarySettingNext = $salarySetting->replicate();
                $salarySettingNext->salary_list_id =  $nextSalaryList->id;
                $salarySettingNext->created_at = Carbon::now();
                $salarySettingNext->created_by = auth()->id();
                $salarySettingNext->save();
            }

            $salarySetting->created_by = auth()->id();

            $salarySetting->save();
            $result = $salarySetting;
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
    public function updateSalarySetting($request)
    {
        $result = null;
        DB::beginTransaction();
        try {
            $salarySettingId = $request->only('salary_setting_id');

            $salarySetting = SalarySetting::where('id', $salarySettingId['salary_setting_id'])->first();


            $updateSalarySettingInput = $request->except('_token', 'salary_setting_id');
            $updateSalarySettingInput['updated_by'] =  auth()->id();

            $salarySetting->update($updateSalarySettingInput);
            $salarySetting->save();

            $salaryList = SalaryList::where('id', $updateSalarySettingInput['salary_list_id'])->first();

            foreach ($salaryList->salaries as $salary) {
                $salary_type_contact = $salary->type_contact;
                $salary_id = $salary->id;
                $insurance_id = $salary->insurance_id;
                $allowance_id  = $salary->allowance_id;
                $tax_id = $salary->tax_id;
                $pay_id = $salary->pay_id;
                $support_main_salary = $salary->support->main_salary;
                $number_of_dependents = $salary->insurance->number_of_dependents;
                DB::select("CALL calculateSalary_Typecontact($salary_type_contact,$salary_id, $insurance_id, $allowance_id, $tax_id, $pay_id)");
                DB::select("CALL updateInsurance_SLNPT($number_of_dependents, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                DB::select("CALL updateInsuranceData($support_main_salary, $insurance_id)");
            }


            $result = $salarySetting;

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
