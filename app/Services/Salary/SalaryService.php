<?php

namespace App\Services\Salary;

use App\Repositories\Allowance\AllowanceRepository;
use App\Repositories\Confirm\ConfirmRepository;
use App\Repositories\Income\IncomeRepository;
use App\Repositories\Insurance\InsuranceRepository;
use App\Repositories\Organizational\OrganizationalRepository;
use App\Repositories\Pay\PayRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\Salary\SalaryRepository;
use App\Repositories\Support\SupportRepository;
use App\Repositories\Tax\TaxRepository;
use App\Repositories\Timekeep\TimekeepRepository;
use App\Services\BaseService;
use App\Services\Salary\Traits\AddEdit;
use App\Services\Salary\Traits\Delete;
use App\Services\Salary\Traits\Search;

class SalaryService extends BaseService
{
    use Search, AddEdit, Delete;
    public $organizationalRepository, $roomRepository, $timekeepRepository,
        $salaryRepository, $incomeRepository, $supportRepository, $allowanceRepository, 
        $insuranceRepository, $taxRepository, $payRepository, $confirmRepository;

    public function __construct(
        OrganizationalRepository $organizationalRepository,
        RoomRepository $roomRepository,
        TimekeepRepository $timekeepRepository,
        SalaryRepository $salaryRepository,
        IncomeRepository $incomeRepository,
        SupportRepository $supportRepository,
        AllowanceRepository $allowanceRepository,
        InsuranceRepository $insuranceRepository,
        TaxRepository $taxRepository,
        PayRepository $payRepository,
        ConfirmRepository $confirmRepository

    ) {
        $this->organizationalRepository = $organizationalRepository;
        $this->roomRepository = $roomRepository;
        $this->timekeepRepository = $timekeepRepository;
        $this->salaryRepository = $salaryRepository;
        $this->incomeRepository = $incomeRepository;
        $this->supportRepository = $supportRepository;
        $this->allowanceRepository = $allowanceRepository;
        $this->insuranceRepository = $insuranceRepository;
        $this->taxRepository = $taxRepository;
        $this->payRepository = $payRepository;
        $this->confirmRepository = $confirmRepository;
    }
}
