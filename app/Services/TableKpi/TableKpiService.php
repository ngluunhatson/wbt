<?php

namespace App\Services\TableKpi;

use App\Repositories\Confirm\ConfirmRepository;
use App\Repositories\Criteria\CriteriaRepository;
use App\Repositories\CriteriaConfirm\CriteriaConfirmRepository;
use App\Repositories\Organizational\OrganizationalRepository;
use App\Repositories\TableKpi\TableKpiRepository;
use App\Repositories\KpiConfirm\KpiConfirmRepository;
use App\Repositories\DetailKpi\DetailKpiRepository;
use App\Repositories\Room\RoomRepository;
use App\Services\BaseService;
use App\Services\TableKpi\Traits\AddEdit;
use App\Services\TableKpi\Traits\Delete;
use App\Services\TableKpi\Traits\Search;

class TableKpiService extends BaseService
{
    use Search , AddEdit, Delete;
    public $organizationalRepository;
    public $tableKpiRepository;
    public $roomRepository;

    public function __construct(
        OrganizationalRepository $organizationalRepository,
        TableKpiRepository $tableKpiRepository,
        ConfirmRepository $confirmRepository,
        CriteriaConfirmRepository $criteriaConfirmRepository,
        CriteriaRepository $criteriaRepository,
        KpiConfirmRepository $kpiConfirmRepository,
        DetailKpiRepository $detailKpiRepository,
        RoomRepository $roomRepository,
    ) {
        $this->organizationalRepository = $organizationalRepository;
        $this->tableKpiRepository = $tableKpiRepository;
        $this->confirmRepository = $confirmRepository;
        $this->criteriaConfirmRepository = $criteriaConfirmRepository;
        $this->criteriaRepository = $criteriaRepository;
        $this->kpiConfirmRepository = $kpiConfirmRepository;
        $this->detailKpiRepository = $detailKpiRepository;
        $this->roomRepository = $roomRepository;
    }
}