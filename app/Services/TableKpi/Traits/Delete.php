<?php

namespace App\Services\TableKpi\Traits;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Support\Facades\DB;

trait Delete
{
    public function deleteByID($id)
    {
        $result = [
            'status' => false,
            'message' => 'Xoá không thành công !!',
        ];
        try {
            $this->tableKpiRepository->update($id, [
                'delete_flag' => 1
            ]);
            $this->detailKpiRepository->deleteByID($id );
            $result['status'] = true;
            $result['message'] = 'Xoá thành công !!';
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }
}
