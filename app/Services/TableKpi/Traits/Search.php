<?php

namespace App\Services\TableKpi\Traits;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{
    public function getDataList()
    {
        $result = [];
        try {
            $data = $this->tableKpiRepository->loadRelation(['room', 'team', 'position','user', 'confirms.confirmReceive'])->where('delete_flag', '=', '0');
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataByID($id)
    {
        $result = [];
        try {
            $data = $this->tableKpiRepository->loadRelation(['room', 'team', 'position','user', 'confirms', 'confirms.confirm', 'confirms.confirm.staff'])->find($id);
            $result = !empty($data) ? $data->toArray() : [];
            if ($result['status'] == 11) {
                foreach($result['confirms'] as $value) {
                    if($value['confirm']['status'] == 2) {
                        $result['reason'] = $value['confirm']['note'];
                    }
                }
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getRoomsByUser($id)
    {
        $result = [];
        try {
            $rooms = $this->roomRepository->findByCondition([
               'id' => $id
            ]);
            $result = !empty($rooms) ? $rooms : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getRooms()
    {
        $result = [];
        try {
            $rooms = $this->roomRepository->findAll();
            $result = !empty($rooms) ? $rooms : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getHR()
    {
        $result = [];
        try {
            $positions = User::with('roles')->whereHas('roles', function($query) {
                $query->where('name', 'hr-manager')->orWhere('name', 'hr-chief');
            })->get();
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getStaffsByTeam($room_id, $team_id, $position_id)
    {
        $result = [];
        try {
            $positions = $this->organizationalRepository->getStaffByConditions($room_id, $team_id, $position_id);
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataMyWork($user_id, $quarter = null, $year = null)
    {
        $result = [];
        try {
            if (!empty($user_id)) {
                $tableKpi = $this->kpiConfirmRepository->loadRelation([
                    'confirmReceive', 'confirmReceive.staff', 'kpi', 'kpi.room', 'kpi.team',
                    'kpi.position', 'kpi.user', 'kpi.confirms', 'kpi.confirms.confirm',  'kpi.confirms.confirm.staff'
                ])
                    ->where('kpi.status', '>=', 4)
                    ->where('confirmReceive.basic_info_id', $user_id);
                if (!empty($quarter)) {
                    $tableKpi = $tableKpi->where('kpi.quarter', $quarter);
                }
    
                if (!empty($year)) {
                    $tableKpi = $tableKpi->where('kpi.year', $year);
                }
                
                $result = !empty($tableKpi) ? $tableKpi : [];
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataListByRoom($roomID)
    {
        $result = [];
        try {
            $data = $this->tableKpiRepository->loadRelation(['room', 'team', 'position', 'user', 'confirms.confirmReceive'])->where('room_id', $roomID)->where('delete_flag', 0);;
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function checkUserCanBeReceived($id, $user_id)
    {
        $result = false;
        try {
            $data = $this->kpiConfirmRepository->loadRelation(['confirmReceive'])->where('kpi_id', $id)->where('delete_flag', 0)->first();
            if ($user_id == $data->confirmReceive->basic_info_id) {
                $result = true;
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
