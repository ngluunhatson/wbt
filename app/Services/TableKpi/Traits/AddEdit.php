<?php

namespace App\Services\TableKpi\Traits;

use Arr;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Request;
use Illuminate\Support\Facades\Validator;
use App\Events\NotificationEvent;
use App\Models\User;
use App\Notifications\AssignPerson;
use Carbon\Carbon;

trait AddEdit
{
    public function saveData($request)
    {
        // dd($request->all());
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $dataTableKpi = $request->except(
                '_token',
                'selectConfirm',
                'type_confirm_1',
                'type_confirm_2',
                'type_confirm_3',
                'content_1',
                'content_2',
                'content_3',
                'content_4',
                'proportion_1',
                'proportion_2',
                'proportion_3',
                'proportion_4',
                'reality_1',
                'reality_2',
                'reality_3',
                'reality_4',
                'result_1',
                'result_2',
                'result_3',
                'result_4',
                'note_1',
                'note_2',
                'note_3',
                'note_4',
                'medium',
                'categories_1',
                'categories_2',
                'categories_3',
                'categories_4',
                'pqt',
                'bgd',
                'take',
                'assign',
                'selectStaff'
            );
            $dataDetailKpi = $request->except('_token', 'selectStaff');
            $dataDetailKpi['created_by'] = auth()->id();
            $dataConfirm = $request['selectConfirm'];
            if ($request['selectStaff']) {
                array_unshift($dataConfirm, $request['selectStaff']);
            }
            $dataTableKpi['created_by'] = auth()->id();
            $dataTableKpi['status'] = 1;
            $dataTableKpi['basic_info_id'] = auth()->id();
            $tableKpi = $this->tableKpiRepository->create($dataTableKpi);

            foreach ($dataConfirm as $key => $confimer) {
                $confim = $this->confirmRepository->create([
                    'basic_info_id' => $confimer ?? null,
                    'type_confirm' => $request['type_confirm_' . ($key + 1)],
                    'assignment_type' => config('constants.assignment_type')[2],
                    'status' => 0,
                    'created_by' => auth()->id()
                ]);
                $this->kpiConfirmRepository->create([
                    'kpi_id' => $tableKpi->id,
                    'confirm_id' => $confim->id
                ]);
                if ($confimer != null && $request['type_confirm_' . ($key + 1)] == 2 && array_key_exists('assign', $request->all()) && $request['assign']) {
                    $user = User::where('basic_info_id', $confimer)->first();
                    $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                    $link = route('tablekpi.view', $tableKpi->id);
                    $this->sendNotification($user, $tableKpi->id, $link, $message);
                    $tableKpi->status = 2;
                }

                $confirmID[] = $confim->id;
            }
            $i = 1;
            for ($i; $i < 5; $i++) {
                $this->detailKpiRepository->create([
                    'kpis_id' => $tableKpi->id,
                    'categories' => $dataDetailKpi['categories_' . $i],
                    'content_kpis' => $dataDetailKpi['content_' . $i],
                    'proportion' => $dataDetailKpi['proportion_' . $i],
                    'reality' => $dataDetailKpi['reality_' . $i],
                    'result' => !empty($dataDetailKpi['result_' . $i]) ? $dataDetailKpi['result_' . $i] : 0,
                    'note' => $dataDetailKpi['note_' . $i],
                ]);
            }
            $tableKpi->confirm_id = $confirmID;
            $tableKpi->save();

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function updateData($request, $idDetails)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhật không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $dataTableKpi = $request->except(
                '_token',
                'confirm_id_1',
                'confirm_id_2',
                'confirm_id_3',
                'selectConfirm',
                'type_confirm_1',
                'type_confirm_2',
                'type_confirm_3',
                'content_1',
                'content_2',
                'content_3',
                'content_4',
                'proportion_1',
                'proportion_2',
                'proportion_3',
                'proportion_4',
                'reality_1',
                'reality_2',
                'reality_3',
                'reality_4',
                'result_1',
                'result_2',
                'result_3',
                'result_4',
                'note_1',
                'note_2',
                'note_3',
                'note_4',
                'medium',
                'categories_1',
                'categories_2',
                'categories_3',
                'categories_4',
                'pqt',
                'bgd',
                'take',
                'assign',
                'kpis_id',
                'job_assignor',
                'selectStaff'
            );
            $dataDetailKpi = $request->except('_token', 'selectStaff');
            $dataDetailKpi['updated_by'] = auth()->id();
            $dataTableKpi['updated_by'] = auth()->id();
            $dataConfirm = $request['selectConfirm'];
            if ($request['selectStaff']) {
                array_unshift($dataConfirm, $request['selectStaff']);
            }
            if (array_key_exists('year', $dataDetailKpi)) {
                $dataTableKpi['year'] = date('Y', strtotime($dataTableKpi['year']));
            }
            $kpiOld = $this->tableKpiRepository->getByID($request['kpis_id']);
            if (in_array($kpiOld['status'], [9, 10]) && auth()->id() == $kpiOld['created_by']) {
                $dataTableKpi['status'] = 2;
            }

            if (
                in_array($kpiOld['status'], [11, 12, 13]) &&
                auth()->id() != $kpiOld['created_by']
                || ($kpiOld['status'] == 4 && auth()->id() != $kpiOld['created_by'])
            ) {
                $dataTableKpi['status'] = 5;
            }

            for ($i = 1; $i < 4; $i++) {
                $this->confirmRepository->update($request['confirm_id_' . $i], [
                    'basic_info_id' => $dataConfirm[$i - 1],
                    'status' => 0,
                    'note' => null,
                    'assignment_type' => config('constants.assignment_type')[2],
                    'updated_by' => auth()->id()
                ]);
                
                if ($dataConfirm[$i - 1] != null && $request['type_confirm_' . $i] == 2 
                && ($kpiOld['status'] == 5 || $kpiOld['status'] == 6 || 
                (array_key_exists('assign', $request->all()) && $request['assign']))) {
                    $user = User::where('basic_info_id', $dataConfirm[$i - 1])->first();
                    $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                    $link = route('tablekpi.view', $request['kpis_id']);
                    $this->sendNotification($user, $request['kpis_id'], $link, $message);
                    $dataTableKpi['status'] = 2;
                }
            }
            $tableKpi = $this->tableKpiRepository->update($request['kpis_id'], $dataTableKpi);

            if (auth()->id() != $kpiOld['created_by']) {
                $user = User::where('id', $kpiOld['created_by'])->first();
                $confim = $this->confirmRepository->findFirst([
                    'basic_info_id' => $user->basic_info_id,
                    'type_confirm' => 99,
                    'assignment_type' => config('constants.assignment_type')[2],
                ]);
                if (empty($confim)) {
                    $confim = $this->confirmRepository->create([
                        'basic_info_id' => $user->basic_info_id ?? $user->id,
                        'type_confirm' => 99,
                        'assignment_type' => config('constants.assignment_type')[2],
                        'status' => 0,
                        'created_by' => auth()->id()
                    ]);
                } else {
                    $this->confirmRepository->update($confim['id'], [
                        'status' => 0,
                        'updated_by' => auth()->id(),
                    ]);
                }

                $this->kpiConfirmRepository->create([
                    'kpi_id' => $request['kpis_id'],
                    'confirm_id' => is_array($confim) ? $confim['id'] : $confim->id,
                ]);

                $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                $link = route('tablekpi.view', $request['kpis_id']);
                $this->sendNotification($user, $request['kpis_id'], $link, $message);
            }


            $dataDetailKpiOld = $this->detailKpiRepository->getDataByKpiID($request['kpis_id']);
            if (auth()->id() == $kpiOld['created_by'])
            {
                for ($i = 1; $i <= 4; $i++) {
                    $this->detailKpiRepository->update($dataDetailKpiOld[$i - 1]['id'], [
                        'content_kpis' => array_key_exists('content_' . $i, $dataDetailKpi) ? $dataDetailKpi['content_' . $i] : $dataDetailKpiOld[$i - 1]['content'],
                        'proportion' => array_key_exists('proportion_' . $i, $dataDetailKpi) ? $dataDetailKpi['proportion_' . $i] : $dataDetailKpiOld[$i - 1]['proportion'],
                        'reality' => $dataDetailKpi['reality_' . $i],
                        'result' => !empty($dataDetailKpi['result_' . $i]) ? $dataDetailKpi['result_' . $i] : 0,
                        'note' => $dataDetailKpi['note_' . $i],
                        'updated_by' => auth()->id(),
                    ]);
                }
            } else {
                for ($i = 1; $i <= 4; $i++) {
                    $this->detailKpiRepository->update($dataDetailKpiOld[$i - 1]['id'], [
                        'reality' => $dataDetailKpi['reality_' . $i],
                        'result' => !empty($dataDetailKpi['result_' . $i]) ? $dataDetailKpi['result_' . $i] : 0,
                        'note' => $dataDetailKpi['note_' . $i],
                        'updated_by' => auth()->id(),
                    ]);
                }
            }

            $result['status'] = true;
            $result['message'] = 'Cập nhật thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateStatus($request, $id)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhật không thành công !!',
        ];
        DB::beginTransaction();
        try {
            if ($request['statusConfirm'] == 1) {
                switch ($request['type']) {
                    case 2:
                        $this->tableKpiRepository->update($id, [
                            'status' => $request['typeconfirm'] == 1 ? 3 : 7,
                            'updated_by' => auth()->id()
                        ]);
                        if (!empty($request['confimerIDNext'])) {
                            $user = User::where('basic_info_id', $request['confimerIDNext'])->first();
                            $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                            $link = route('tablekpi.view', $id);
                            $this->sendNotification($user, $id, $link, $message);
                        }
                        break;
                    case 3:
                        $alreadyNotified = -1000;
                        $this->tableKpiRepository->update($id, [
                            'status' => $request['typeconfirm'] == 1 ? 4 : 8,
                            'updated_by' => auth()->id()
                        ]);
                        if ($request['typeconfirm'] == 1) {
                            $this->tableKpiRepository->update($id, [
                                'delivery_date' => Carbon::now()->format('Y-m-d'),
                            ]);
                        } else {
                            $this->tableKpiRepository->update($id, [
                                'review_date' => Carbon::now()->format('Y-m-d'),
                            ]);
                        }
                        $data = $this->tableKpiRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        $message = 'Thông báo !! Yêu cầu đã được duyệt bởi ' . auth()->user()->name;
                    
                        if ($data->user) {
                            $link = route('tablekpi.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                            $alreadyNotified = $data->user->basic_info_id;
                        }
                        foreach ($data->confirms as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirmID'] && $value->confirm->basic_info_id != $alreadyNotified) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                if (!empty($user)) {
                                    $link = route('tablekpi.view', $id);
                                    $this->sendNotification($user, $id, $link, $message);
                                }
                            }
                        }
                        break;
                    case 99:
                        $this->tableKpiRepository->update($id, [
                            'status' => 6,
                            'updated_by' => auth()->id()
                        ]);
                        if (!empty($request['confimerIDNext'])) {
                            $user = User::where('basic_info_id', $request['confimerIDNext'])->first();
                            $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                            $link = route('tablekpi.view', $id);
                            $this->sendNotification($user, $id, $link, $message);
                        }
                        break;
                }
            } else {
                switch ($request['type']) {
                    case 2:
                        $this->tableKpiRepository->update($id, [
                            'status' => $request['typeconfirm'] == 1 ? 9 : 12,
                            'updated_by' => auth()->id()
                        ]);
                        if ($request['typeconfirm'] == 1) {
                            $data = $this->tableKpiRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                            if ($data->user) {
                                $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                                $link = route('tablekpi.view', $id);
                                $this->sendNotification($data->user, $id, $link, $message);
                            }
                        } else {
                            $data = $this->tableKpiRepository->loadRelation(['user', 'confirms.confirmReceive'])
                                ->where('id', $id)->first();
                            if ($data->user) {
                                $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                                $link = route('tablekpi.view', $id);
                                $this->sendNotification($data->user, $id, $link, $message);
                            }

                            if ($data->confirms) {
                                foreach ($data->confirms as $value) {
                                    if ($value->confirmReceive) {
                                        $user = User::where('basic_info_id', $value->confirmReceive->basic_info_id)->first();
                                        $link = route('tablekpi.view', $id);
                                        $this->sendNotification($user, $id, $link, $message);
                                    }
                                }
                            }
                        }
                        break;
                    case 3:
                        $this->tableKpiRepository->update($id, [
                            'status' => $request['typeconfirm'] == 1 ? 10 : 13,
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->tableKpiRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                        dd($data);
                        if ($data->user) {
                            $link = route('tablekpi.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        foreach ($data->confirms as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirmID']) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                $link = route('tablekpi.view', $id);
                                $this->sendNotification($user, $id, $link, $message);
                            }
                        }
                        break;
                    case 99:
                        $this->tableKpiRepository->update($id, [
                            'status' => 11,
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->kpiConfirmRepository->loadRelation(['confirmReceive'])->where('kpi_id', $id)->first();
                        if ($data->confirmReceive) {
                            $user = User::where('basic_info_id', $data->confirmReceive->basic_info_id)->first();
                            $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                            $link = route('tablekpi.view', $id);
                            $this->sendNotification($user, $id, $link, $message);
                        }
                        break;
                }
            }

            $this->confirmRepository->update($request['confirmID'], [
                'status' => $request['statusConfirm'],
                'note' => $request['reason'],
                'updated_by' => auth()->id()
            ]);

            $result['status'] = true;
            $result['message'] = 'Cập nhật thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
