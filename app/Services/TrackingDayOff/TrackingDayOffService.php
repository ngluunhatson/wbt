<?php

namespace App\Services\TrackingDayOff;

use App\Repositories\Organizational\OrganizationalRepository;
use App\Repositories\Position\PositionRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\Team\TeamRepository;
use App\Repositories\Timekeep\TimekeepRepository;
use App\Repositories\TrackingDayOff\TrackingDayOffRepository;
use App\Services\BaseService;
use App\Services\TrackingDayOff\Traits\AddEdit;
use App\Services\TrackingDayOff\Traits\Search;

class TrackingDayOffService extends BaseService
{
    use Search, AddEdit;
    public $trackingDayOffRepository, $roomRepository, $teamRepository, $positionRepository, $organizationalRepository, $timekeepRepository;

    public function __construct(
        TrackingDayOffRepository $trackingDayOffRepository,
        RoomRepository $roomRepository,
        TeamRepository $teamRepository,
        PositionRepository $positionRepository,
        OrganizationalRepository $organizationalRepository,
        TimekeepRepository $timekeepRepository

    ) {
        $this -> trackingDayOffRepository = $trackingDayOffRepository;
        $this -> roomRepository           = $roomRepository;
        $this -> teamRepository           = $teamRepository;
        $this -> positionRepository       = $positionRepository;
        $this -> organizationalRepository = $organizationalRepository;
        $this -> timekeepRepository       = $timekeepRepository;

    }
}
