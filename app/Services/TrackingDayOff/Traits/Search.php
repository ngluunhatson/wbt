<?php

namespace App\Services\TrackingDayOff\Traits;

use App\Models\TrackingDayOff;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{
    public function getTrackingDayOffByUserId($user_id)
    {
        try {

            $basic_info_id = (User::where(['id'  => $user_id]))->first()->basic_info_id;


            $trackingDayOffs = [];
            if ($basic_info_id)
                $trackingDayOffs = TrackingDayOff::where('basic_info_id', $basic_info_id)->get();

            return $trackingDayOffs;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
    }


    public function getTrackingDayOffById($id, $year, $mode = 'basic_info_id')
    {
        try {


            $basic_info_id = $id;

            if ($mode == 'user_id')
                $basic_info_id = (User::where(['id'  => $id]))->first()->basic_info_id;

            $trackingDayOff = $this->trackingDayOffRepository->loadRelation(['staff', 'prevYearTrackingDayOff'])->where('basic_info_id', $basic_info_id)->where('year', $year)->first();

            return $trackingDayOff;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
    }


    public function getAllTrackingDayOffsDict()
    {
        $result = [];
        try {
            $dictionaryYearToTrackingDayOffs = [];
            $trackingDayOffs =  $this->trackingDayOffRepository->loadRelation(['staff', 'prevYearTrackingDayOff'])->where('delete_flag', 0);

            foreach ($trackingDayOffs as $trackingDayOff) {

                $dictionaryYearToTrackingDayOffs[$trackingDayOff->year][] = $trackingDayOff;
            }

            $result = !empty($dictionaryYearToTrackingDayOffs) ? $dictionaryYearToTrackingDayOffs : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getTrackingDayOffsDict($id, $mode = 'user')
    {
        $result = [];
        try {
            $dictionaryYearToTrackingDayOff = [];

            $basic_info_id = $id;

            if ($mode == 'user')
                $basic_info_id = User::where('id', $id)->first()->basic_info_id;


            $trackingDayOffs =  $this->trackingDayOffRepository->loadRelation(['staff', 'prevYearTrackingDayOff'])->where('delete_flag', 0)->where('basic_info_id', $basic_info_id);

            foreach ($trackingDayOffs as $trackingDayOff) {

                $dictionaryYearToTrackingDayOff[$trackingDayOff->year] = $trackingDayOff;
            }

            $result = !empty($dictionaryYearToTrackingDayOff) ? $dictionaryYearToTrackingDayOff : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }



        return $result;
    }

    public function getDataByID($id)
    {
        $result = [];
        try {

            $trackingDayOff = $this->trackingDayOffRepository->loadRelation(['staff'])->find($id);
            $result = !empty($trackingDayOff) ? $trackingDayOff : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
