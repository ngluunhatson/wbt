<?php

namespace App\Services\TrackingDayOff\Traits;

use App\Models\Organizational;
use App\Models\Timekeeping;
use App\Models\TrackingDayOff;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait AddEdit
{
    private function calculatePTO($basic_info_entry_date)
    {

        $date_entry  = strtotime($basic_info_entry_date);
        $year_entry  = intval(date('Y', $date_entry));
        $month_entry = intval(date('m', $date_entry));
        $day_entry   = intval(date('d', $date_entry));

        $date_now = strtotime(Carbon::now());
        $year_now  = intval(date('Y', $date_now));
        $month_now = intval(date('m', $date_now));
        $day_now = intval(date('m',   $date_now));

        $total_cur_year_pto_input = 0;
        $total_prev_year_pto_input = 12;
        $total_prev_prev_year_pto_input = 0;


        if ($year_now > $year_entry) {
            $total_cur_year_pto_input = $month_now;
            if ($year_now - $year_entry > 1) {
                $total_prev_prev_year_pto_input = 12;
            }
        } else {
            $total_cur_year_pto_input += ($month_now) - ($month_entry);
            $total_prev_year_pto_input = 0;
            if ($day_entry == 1) $total_cur_year_pto_input += 1;
        }

        $isNewYear = ($month_now == 1) && ($day_now >= 1);

        return [$total_cur_year_pto_input, $total_prev_year_pto_input, $isNewYear, $total_prev_prev_year_pto_input];
    }

    public function populateTrackingDayOffs()
    {

        DB::beginTransaction();
        try {
            $organizationals = Organizational::whereNotNull('basic_info_id')->where('delete_flag', 0)->get();


            $trackingDayOffArrayFormAll =  $this->trackingDayOffRepository->findAll();

            $exist_basic_info_id = [];

            foreach ($trackingDayOffArrayFormAll as $trackingdayoff) {
                $exist_basic_info_id[] = $trackingdayoff['basic_info_id'];
            }


            foreach ($organizationals as $organizational) {
                $basic_info = $organizational->staff;
                if (!in_array($basic_info->id, $exist_basic_info_id)) {

                    $PTO_result = $this->calculatePTO($basic_info->date_of_entry_to_work);
                    $total_cur_year_pto_input       = $PTO_result[0];
                    $total_prev_year_pto_input      = $PTO_result[1];
                    $total_prev_prev_year_pto_input = $PTO_result[3];

                    $tracking_day_off_input = [
                        'basic_info_id'                     => $basic_info->id,
                        'cur_year_pto'                      => array_fill(0, 12, 0),
                        'total_cur_year_pto'                => $total_cur_year_pto_input,
                        'prev_year_pto'                     => array_fill(0, 12, 0),
                        'total_prev_year_pto'               => $total_prev_year_pto_input,
                        'to_be_used_in_first_3_months_cur'  => $total_prev_year_pto_input,
                        'to_be_used_in_first_3_months_prev' => $total_prev_prev_year_pto_input,
                        'note'                              => null,
                        'created_by'                        => auth()->id(),
                        'delete_flag'                       => 0,

                    ];
                    $this->trackingDayOffRepository->create($tracking_day_off_input);
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }
    }

    public function updateAllTrackingDayOffs()
    {

        DB::beginTransaction();

        try {
            $date_now  = strtotime(Carbon::now());
            $year_now  = intval(date('Y', $date_now));
            $month_now = intval(date('m', $date_now));
            $day_now = intval(date('d', $date_now));


            $AllTrackingDayOffsObjectForm =  $this->trackingDayOffRepository->loadRelation(['staff', 'prevYearTrackingDayOff'])->where('delete_flag', 0);

            foreach ($AllTrackingDayOffsObjectForm as $trackingDayOff) {

                $basic_info = $trackingDayOff->staff;
                $AllTimeKeepsArrayForm = $this->timekeepRepository->findAll();

                $total_cur_year_pto_input       = $trackingDayOff->total_cur_year_pto;
                $total_prev_year_pto_input      = $trackingDayOff->total_prev_year_pto;
                $total_prev_prev_year_pto_input = $trackingDayOff->total_prev_prev_year_pto;


                $cur_year_array_pto_input  = $trackingDayOff->cur_year_pto;
                $prev_year_array_pto_input = $trackingDayOff->prev_year_pto;


                foreach ($AllTimeKeepsArrayForm as $timekeepArrayForm) {
                    if ($timekeepArrayForm['basic_info_id'] == $basic_info->id) {
                        $totalPTOInMonth = 0;
                        $timekeepMonth = intval($timekeepArrayForm['month']);
                        $timekeepYear  = intval($timekeepArrayForm['year']);

                        $isLeapYear = false;
                        if ($timekeepYear % 4 == 0) {
                            if ($timekeepYear % 100) {
                                if ($timekeepYear % 400 == 0)
                                    $isLeapYear = true;
                            } else
                                $isLeapYear = true;
                        }
                        $limit = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][$timekeepMonth - 1];

                        if ($limit == 28 && $isLeapYear) $limit++;
                        for ($i = 1; $i < ($limit + 1); $i++) {

                            if (in_array($timekeepArrayForm['day_' . $i], [3, 9, 10, 12])) {
                                $totalPTOInMonth += 0.5;
                            }
                            if ($timekeepArrayForm['day_' . $i] == 5) {
                                $totalPTOInMonth += 1;
                            }
                        }

                        if ($year_now - $timekeepYear == 1) {

                            if ($timekeepMonth < 4) {

                                if ($total_prev_prev_year_pto_input - $totalPTOInMonth >= 0) {
                                    $total_prev_prev_year_pto_input -= $totalPTOInMonth;
                                } else {
                                    $total_prev_prev_year_pto_input = 0;
                                    $total_prev_year_pto_input += ($total_prev_prev_year_pto_input - $totalPTOInMonth);
                                }
                            } else {
                                if ($totalPTOInMonth > $total_prev_year_pto_input)
                                    $totalPTOInMonth = $total_prev_year_pto_input;
                                $total_prev_year_pto_input -= $totalPTOInMonth;
                            }

                            $prev_year_array_pto_input[$timekeepMonth - 1] += $totalPTOInMonth;
                        }

                        if ($year_now == $timekeepYear) {

                            if ($timekeepMonth < 4) {
                                if ($total_prev_year_pto_input - $totalPTOInMonth >= 0) {
                                    $total_prev_year_pto_input -= $totalPTOInMonth;
                                } else {
                                    $total_prev_year_pto_input = 0;
                                    $total_cur_year_pto_input += ($total_prev_year_pto_input - $totalPTOInMonth);
                                }
                            } else {
                                if ($totalPTOInMonth > $total_cur_year_pto_input)
                                    $totalPTOInMonth = $total_cur_year_pto_input;
                                $total_cur_year_pto_input -= $totalPTOInMonth;
                            }

                            $cur_year_array_pto_input[$timekeepMonth - 1] += $totalPTOInMonth;
                        }
                    }
                }




                $isNewYear = ($month_now == 1 && $day_now >= 1);

                $tracking_day_off_update_input = [
                    'total_cur_year_pto'                => $total_cur_year_pto_input,
                    'total_prev_year_pto'               => $total_prev_year_pto_input,
                    'cur_year_pto'                      => $cur_year_array_pto_input,
                    'prev_year_pto'                     => $prev_year_array_pto_input,
                    'to_be_used_in_first_3_months_cur'  => $total_prev_year_pto_input,
                    'to_be_used_in_first_3_months_prev' => $total_prev_prev_year_pto_input,
                    'updated_by'                        => auth()->id(),

                ];

                if ($isNewYear) {
                    $tracking_day_off_input['prev_year_pto'] = $trackingDayOff->cur_year_pto;
                    $tracking_day_off_input['cur_year_pto']  = array_fill(0, 12, 0);
                }

                $this->trackingDayOffRepository->update($trackingDayOff->id, $tracking_day_off_update_input);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }
    }

    public function updateTrackingDayOff($request)
    {

        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $update_input = $request->except('_token');


            $prev_year_pto_update = [];
            $cur_year_pto_update = [];

            $trackingDayOffX = TrackingDayOff::where('id', $update_input['tracking_day_off_id'])->first();

            $total_cur_year_pto_update = $trackingDayOffX->total_cur_year_pto;
            $total_prev_year_pto_update = $trackingDayOffX->total_prev_year_pto;

            for ($i = 1; $i < 13; $i++) {
                $prev_month = intval($update_input['prev_mth' . $i]);
                $cur_month = intval($update_input['cur_mth' . $i]);
                $prev_year_pto_update[] = $prev_month;
                $cur_year_pto_update[] = $cur_month;
                $total_prev_year_pto_update -= $prev_month;
                $total_cur_year_pto_update  -= $cur_month;
            }

            $total_cur_year_pto_update  = ($total_cur_year_pto_update < 0)  ? 0 : $total_cur_year_pto_update;
            $total_prev_year_pto_update = ($total_prev_year_pto_update < 0) ? 0 : $total_prev_year_pto_update;

            $tracking_day_off_update_input = [
                'cur_year_pto'        => $cur_year_pto_update,
                'total_cur_year_pto'  => $total_cur_year_pto_update,
                'prev_year_pto'       => $prev_year_pto_update,
                'total_prev_year_pto' => $total_prev_year_pto_update,
                'note'                => $update_input['note'],
                'updated_by'          => auth()->id(),
            ];


            $this->trackingDayOffRepository->update($trackingDayOffX->id, $tracking_day_off_update_input);

            $result['status'] = true;
            $result['message'] = 'Cập nhật thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }


    public function populateTrackingDayOffsNew()
    {
        DB::beginTransaction();
        try {
            $organizationals = Organizational::whereNotNull('basic_info_id')->where('delete_flag', 0)->get();

            $trackingDayOffObjectFormAll =  TrackingDayOff::whereNotNull('basic_info_id')->select('basic_info_id')->distinct()->get();

            $exist_basic_info_id = [];

            foreach ($trackingDayOffObjectFormAll as $trackingdayoff) {
                $exist_basic_info_id[] = $trackingdayoff->basic_info_id;
            }


            $date_now = strtotime(Carbon::now());
            $year_now  = intval(date('Y', $date_now));
            $month_now = intval(date('m', $date_now));



            foreach ($organizationals as $organizational) {
                $basic_info = $organizational->staff;

                $isBasicInfoOff = false;
                if ($basic_info->larborBbt->from_date_qdtv  && strtotime($basic_info->larborBbt->from_date_qdtv) <= strtotime(now()))
                    $isBasicInfoOff= true;


                if (!in_array($basic_info->id, $exist_basic_info_id) && !$isBasicInfoOff) {

                    $date_entry  = strtotime($basic_info->date_of_entry_to_work);
                    $year_entry  = intval(date('Y', $date_entry));
                    $month_entry = intval(date('m', $date_entry));

                    $fatherTrackingDayOff = null;

                    for ($cur_year = $year_now; $cur_year >= $year_entry; $cur_year--) {
                        $tracking_day_off_create_input = [
                            'basic_info_id'                     => $basic_info->id,
                            'year'                              => $cur_year,
                            'year_pto_array'                    => array_fill(0, 12, 0),
                            'year_pto_count'                    => ($cur_year ==  $year_now) ? ($month_now) : ($cur_year == $year_entry ? (12 - $month_entry) : 12),
                            'created_by'                        => auth()->id(),
                            'delete_flag'                       => 0,

                        ];
                        if ($fatherTrackingDayOff) {
                            $childTrackingDayOff =  $this->trackingDayOffRepository->create($tracking_day_off_create_input);
                            $fatherTrackingDayOff->prev_year_tracking_day_off_id = $childTrackingDayOff->id;
                            $fatherTrackingDayOff->save();
                            $fatherTrackingDayOff = $childTrackingDayOff;
                        } else
                            $fatherTrackingDayOff =  $this->trackingDayOffRepository->create($tracking_day_off_create_input);
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }
    }

    public function updateAllTrackingDayOffsNew()
    {

        DB::beginTransaction();

        try {

            $allTimeKeepArrayForm = Timekeeping::where('delete_flag', 0)->get()->toArray();
            $date_now = strtotime(Carbon::now());
            $year_now  = intval(date('Y', $date_now));
            $month_now = intval(date('m', $date_now));


            foreach ($allTimeKeepArrayForm as $timeKeepArrayForm_X) {

                $trackingDayOffObjectForm_Cur = TrackingDayOff::with(['staff', 'prevYearTrackingDayOff'])->where('basic_info_id', $timeKeepArrayForm_X['basic_info_id'])->where('year', intval($timeKeepArrayForm_X['year'])) -> first();
                if ($trackingDayOffObjectForm_Cur) {
                    $trackingDayOffObjectForm_Prev = $trackingDayOffObjectForm_Cur->prevYearTrackingDayOff;

                    $date_entry  = strtotime($trackingDayOffObjectForm_Cur->staff->date_of_entry_to_work);
                    $year_entry  = intval(date('Y', $date_entry));
                    $month_entry = intval(date('m', $date_entry));


                    // $trackingDayOff_Cur_UpdatedAtTimeMs = $trackingDayOffObjectForm_Cur->updated_at->valueOf();
                    // $timeKeepArrayForm_X_UpdatedAtTimeMs = (Carbon::parse($timeKeepArrayForm_X['updated_at'])->valueOf());

                    $timekeepMonth = intval($timeKeepArrayForm_X['month']);
                    $curYear       = $trackingDayOffObjectForm_Cur->year;


                    $isLeapYear = false;
                    if ($curYear % 4 == 0) {
                        if ($curYear % 100) {
                            if ($curYear % 400 == 0)
                                $isLeapYear = true;
                        } else
                            $isLeapYear = true;
                    }

                    $totalPTOInMonth = 0;
                    $limit = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][$timekeepMonth - 1];

                    if ($limit == 28 && $isLeapYear) $limit++;
                   
                    for ($i = 1; $i <= $limit; $i++) {

                        // $this -> timekeepRepository -> update($timeKeepArrayForm_X['id'], [
                        //     'day_' . $i => $timeKeepArrayForm_X['day_' . $i],
                        // ]);

                        if (in_array($timeKeepArrayForm_X['day_' . $i], [3, 9, 10, 11, 13])) {
                            $totalPTOInMonth += 0.5;
                        }
                        if ($timeKeepArrayForm_X['day_' . $i] == 5) {
                            $totalPTOInMonth += 1;
                        }
                    }

                    $year_pto_count_update_input_cur = ($curYear ==  $year_now) ? ($month_now) : ($curYear == $year_entry ? (12 - $month_entry) : 12);
                    $year_pto_array_update_input_cur = array_fill(0, 12, 0);

                    if ($timekeepMonth < 4 && $trackingDayOffObjectForm_Prev) {
                        // $trackingDayOff_Prev_UpdatedAtTimeMs = $trackingDayOffObjectForm_Prev->updated_at->valueOf();

                        $year_pto_count_update_input_prev =  ($trackingDayOffObjectForm_Prev -> year == $year_entry) ? (12 - $month_entry) : 12;

                        $difference_prev = $year_pto_count_update_input_prev - $totalPTOInMonth;

                        if ($difference_prev >= 0) {
                            $year_pto_count_update_input_prev -= $totalPTOInMonth;
                        } else {
                            $year_pto_count_update_input_prev = 0;
                            $year_pto_count_update_input_cur += $difference_prev;
                        }

                        if ($year_pto_count_update_input_cur >= 0) {
                            $tracking_day_off_update_input_prev = [
                                'year_pto_count'    => $year_pto_count_update_input_prev,
                                'updated_by'        => auth()->id()

                            ];
                            $this->trackingDayOffRepository->update($trackingDayOffObjectForm_Prev->id, $tracking_day_off_update_input_prev);
                        }
                    } else
                        $year_pto_count_update_input_cur -= $totalPTOInMonth;


                    if ($year_pto_count_update_input_cur >= 0) {
                        $year_pto_array_update_input_cur[$timekeepMonth - 1] = $totalPTOInMonth;

                        $tracking_day_off_update_input_cur = [
                            'year_pto_count'    => $year_pto_count_update_input_cur,
                            'year_pto_array'    => $year_pto_array_update_input_cur,
                            'updated_by'        => auth()->id()

                        ];

                        $this->trackingDayOffRepository->update($trackingDayOffObjectForm_Cur->id, $tracking_day_off_update_input_cur);
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }
    }
}
