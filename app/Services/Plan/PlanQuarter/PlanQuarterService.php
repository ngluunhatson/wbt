<?php

namespace App\Services\Plan\PlanQuarter;

use App\Repositories\Plan\PlanDayRepository;
use App\Repositories\Plan\PlanQuarterRepository;
use App\Repositories\Plan\PlanWeekRepository;
use App\Services\BaseService;
use App\Services\Plan\PlanQuarter\Traits\AddEdit;
use App\Services\Plan\PlanQuarter\Traits\Delete;
use App\Services\Plan\PlanQuarter\Traits\Search;

class PlanQuarterService extends BaseService
{
    use Search, AddEdit, Delete;
    public $planQuarterRepository, $planWeekRepository, $planDayRepository;

    public function __construct(
        PlanQuarterRepository $planQuarterRepository,
        PlanWeekRepository $planWeekRepository,
        PlanDayRepository $planDayRepository,
    ) {
        $this->planQuarterRepository = $planQuarterRepository;
        $this->planWeekRepository    = $planWeekRepository;
        $this->planDayRepository     = $planDayRepository;
    }
}
