<?php

namespace App\Services\Plan\PlanQuarter\Traits;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
trait Delete
{
    public function delete($id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa không thành công !!',
        ]; 

        DB::beginTransaction();
        try {
            $queries = [];

            $plan_quarter = $this -> getPlanQuarterById($id);

            if ($plan_quarter) {
                foreach ($plan_quarter->planWeeks as $plan_week) {
                    $queries[] = $this -> planWeekRepository -> delete($plan_week -> id);
                }
            }
            $queries[] = $this->planQuarterRepository->delete($id);
            if (!in_array(null, $queries));
            {
                $result['status'] = true;
                $result['message'] = 'Xóa thành công !!';
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}