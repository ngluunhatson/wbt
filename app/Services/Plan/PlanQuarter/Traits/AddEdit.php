<?php

namespace App\Services\Plan\PlanQuarter\Traits;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait AddEdit
{
    public function saveData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $request_input = $request->except('_token');

            $created_plan_week_ids = [];

            $plan_quarter_create_input = [
                'room_id'       => $request_input['room_id'],
                'team_id'       => $request_input['team_id'],
                'position_id'   => $request_input['position_id'],
                'basic_info_id' => $request_input['basic_info_id'],
                'plan_week_ids' => [],
                'quarter_goal'  => $request_input['quarter_goal'],
                'year'          => $request_input['year'],
                'quarter'       => $request_input['quarter'],
                'start_date'    => $request_input['week1-from_date'],
                'end_date'      => $request_input['week13-to_date'],
                'created_by'    => auth()->id(),
            ];

            $plan_quarter = $this->planQuarterRepository->create($plan_quarter_create_input);

            for ($w = 1; $w <= 13; $w++) {

                $from_date_string = $request_input['week' . $w . '-from_date'];
                $to_date_string   = $request_input['week' . $w . '-to_date'];

                $plan_week_create_input = [
                    'plan_quarter_id'     => $plan_quarter->id,
                    'room_id'             => $request_input['room_id'],
                    'team_id'             => $request_input['team_id'],
                    'position_id'         => $request_input['position_id'],
                    'basic_info_id'       => $request_input['basic_info_id'],
                    'plan_day_ids'        => [],
                    'five_weekly_goals'   => [],
                    'five_weekly_aspects' => [],
                    'year'                => $request_input['year'],
                    'quarter'             => $request_input['quarter'],
                    'week_in_quarter'     => $w,
                    'start_date'          => $from_date_string,
                    'end_date'            => $to_date_string,
                    'created_by'          => auth()->id(),

                ];
                for ($g = 1; $g <= 5; $g++) {
                    $weekly_goal = $request_input['week' . $w . '-goal' . $g];
                    if ($weekly_goal)
                        $plan_week_create_input['five_weekly_goals'][] = $weekly_goal;
                }


                $plan_week = $this->planWeekRepository->create($plan_week_create_input);
                $created_plan_week_ids[] = $plan_week->id;
            }
            $plan_quarter->plan_week_ids  = $created_plan_week_ids;
            $plan_quarter->save();

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }


    public function updateData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $request_input = $request->except('_token');

            $plan_quarter = $this->getPlanQuarterById($request_input['plan_quarter_id']);

            $plan_quarter_update_input = [
                'quarter_goal'   => $request_input['quarter_goal'],
                'updated_by'     => auth()->id(),
            ];

            foreach ($plan_quarter->planWeeks as $plan_week) {
                $plan_week_update_input = [
                    'updated_by'  => auth()->id(),
                ];
                for ($g = 1; $g <= 5; $g++) {
                    $week_goal_str = $request_input['week' . $plan_week->week_in_quarter . '-goal' . $g];
                    if ($week_goal_str)
                        $plan_week_update_input['five_weekly_goals'][] = $week_goal_str;
                }

                $this->planWeekRepository->update($plan_week->id, $plan_week_update_input);
            }

            $this->planQuarterRepository->update($plan_quarter->id, $plan_quarter_update_input);

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
