<?php

namespace App\Services\Plan\PlanQuarter\Traits;

use App\Models\PlanQuarter;
use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{

    public function checkIfExistPlanQuarter($request)
    {

        $request_input = $request->only('quarter', 'year', 'basic_info_id');
        $result = [];
        try {
            $plan_quarter = $this->planQuarterRepository->loadRelation(['staff', 'planWeeks'])
                ->where('quarter', $request_input['quarter'])
                ->where('year', $request_input['year'])
                ->where('basic_info_id', $request_input['basic_info_id'])
                ->where('delete_flag', 0)->first();
            if ($plan_quarter) {
                $result                    = $request_input;
                $result['staff_full_name'] = $plan_quarter->staff->full_name;
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getPlanQuarterById($id)
    {
        $result = null;
        try {
            $plan_quarter = $this->planQuarterRepository->loadRelation(['staff', 'planWeeks', 'user'])
                ->where('id', $id)
                ->where('delete_flag', 0)
                ->first();

            if ($plan_quarter)
                $result = $plan_quarter;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getPlanQuarter($request)
    {
        $result = null;

        try {
            $plan_quarter = $this->planQuarterRepository->loadRelation(['staff', 'planWeeks', 'user'])
                ->where('basic_info_id', $request['basic_info_id'])
                ->where('quarter', $request['quarter'])
                ->where('year', $request['year'])
                ->where('delete_flag', 0)
                ->first();

            if ($plan_quarter)
                $result = $plan_quarter;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function filterPlanQuarter($request)
    {
        $result = [];
        try {
            $cur_query = PlanQuarter::with([
                'staff', 'planWeeks', 'user',
                'staff.organizational.room',  'staff.organizational.position',
                'staff.organizational.team'
            ])->where('delete_flag', 0);

            foreach ($request->all() as $key => $value) {
                if (intval($value) != -1) {
                    $cur_query->where($key, $value);
                }
            }

            $return_result = $cur_query->get();

            if (!empty($return_result))
                $result = $return_result;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
