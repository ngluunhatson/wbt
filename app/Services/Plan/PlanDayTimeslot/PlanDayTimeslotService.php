<?php

namespace App\Services\Plan\PlanDayTimeslot;

use App\Repositories\Plan\PlanDayTimeslotRepository;
use App\Services\BaseService;
use App\Services\Plan\PlanDayTimeslot\Traits\AddEdit;
use App\Services\Plan\PlanDayTimeslot\Traits\Delete;
use App\Services\Plan\PlanDayTimeslot\Traits\Search;

class PlanDayTimeslotService extends BaseService
{
    use Search, AddEdit, Delete;
    public $planDayTimeslotRepository;

    public function __construct(
        PlanDayTimeslotRepository $planDayTimeslotRepository,
    ) {
        $this -> planDayTimeslotRepository = $planDayTimeslotRepository;
    }
}
