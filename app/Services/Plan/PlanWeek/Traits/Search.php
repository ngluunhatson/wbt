<?php

namespace App\Services\Plan\PlanWeek\Traits;

use App\Models\PlanColor;
use App\Models\PlanDay;
use App\Models\PlanDayTimeslot;
use App\Models\PlanWeek;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait Search
{
    public function getPlanWeekById($id)
    {
        $result = null;
        try {
            $plan_week = $this->planWeekRepository->loadRelation(['staff', 'planDays', 'user'])
                ->where('id', $id)
                ->where('delete_flag', 0)
                ->first();

            if ($plan_week)
                $result = $plan_week;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }



    public function filterPlanWeek($request)
    {
        $result = [];
        try {
            $cur_query = PlanWeek::with([
                'staff', 'planDays', 'user',
                'staff.organizational.room',  'staff.organizational.position',
                'staff.organizational.team'
            ])->where('delete_flag', 0);

            foreach ($request->all() as $key => $value) {
                if (intval($value) != -1) {
                    $cur_query->where($key, $value);
                }
            }

            $return_result = $cur_query->get();

            if (!empty($return_result))
                $result = $return_result;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function findCurWeekPlanOfStaff($basic_info_id)
    {
        $result = null;
        try {

            $date_str = (Carbon::now())->toDateString();
            $plan_week = PlanWeek::with(['planDays', 'staff', 'user'])
                ->whereDate('start_date', '<=', $date_str)
                ->whereDate('end_date', '>=', $date_str)
                ->where('basic_info_id', $basic_info_id)
                ->where('delete_flag', 0)
                ->first();
            if ($plan_week)
                $result = $plan_week;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function checkPlanDayTimeslot($request)
    {
        $result = [
            'status'  => false,
            'message' => "",
        ];

        try {
            $missing_fail_message = "Thiếu Thông Tin! Cần Nhập ";

            $search_input = $request->all();

            $missing_fail_message = $this->getMissingFailMessage($search_input, $missing_fail_message);


            if ($search_input['task_title'] && $search_input['task_date'] && $search_input['task_start_time'] && $search_input['task_end_time']) {
                $result['message'] = "Đã Tồn Tại Công Việc Khác trong Khoảng Thời Gian Này";

                $start_time_float = intval(substr($search_input['task_start_time'], 0, 2)) + intval(substr($search_input['task_start_time'], 3)) / 60;
                $end_time_float   = intval(substr($search_input['task_end_time'], 0, 2))   + intval(substr($search_input['task_end_time'], 3)) / 60;

                $plan_day_timeslot = PlanDayTimeslot::with(['staff', 'user'])
                    ->whereDate('date', $search_input['task_date'])
                    ->where(function ($query_main) use ($start_time_float, $end_time_float) {
                        $query_main
                            ->where(function ($query_sub1) use ($end_time_float) {
                                $query_sub1->where('start_time', '<',  $end_time_float)
                                            ->where('end_time', '>',    $end_time_float);
                            })
                            ->orWhere(function ($query_sub2) use ($start_time_float, $end_time_float) {
                                $query_sub2->where('start_time', '>=', $start_time_float)
                                            ->where('end_time', '<=', $end_time_float);
                            })
                            ->orWhere(function ($query_sub3) use ($start_time_float) {
                                $query_sub3->where('start_time', '<', $start_time_float)
                                    ->where('end_time', '>', $start_time_float);
                            });
                    })
                    ->where('delete_flag', 0)
                    ->first();

                if (!$plan_day_timeslot || (array_key_exists('plan_day_timeslot_id', $search_input) && $plan_day_timeslot->id == $search_input['plan_day_timeslot_id'])) {
                    $result['status']  = true;
                    $result['message'] = "Công Việc này Chưa Tồn Tại";
                }
            } else
                $result['message'] = substr($missing_fail_message, 0, -2);
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    private function getMissingFailMessage($search_input, $missing_fail_message)
    {
        if (!$search_input['task_title']) {
            $missing_fail_message = $missing_fail_message . 'Tên Công Việc, ';
        }

        if (!$search_input['task_date']) {
            $missing_fail_message = $missing_fail_message . 'Ngày của Công Việc, ';
        }

        if (!$search_input['task_start_time']) {
            $missing_fail_message = $missing_fail_message . 'Giờ Bắt Đầu, ';
        }

        if (!$search_input['task_end_time']) {
            $missing_fail_message = $missing_fail_message . 'Giờ Kết Thúc, ';
        }

        return $missing_fail_message;
    }

    public function getPlanDayTimeslotDataFromDates($request)
    {
        $result = [];

        try {
            $search_input = $request->all();

            $start_date = date('Y-m-d', strtotime('+1 day' . $search_input['start']));
            $end_date = $search_input['end'];

            $plan_week = PlanWeek::with(['planDays', 'staff', 'user', 'planDays.planDayTimeslots.planColor'])
                ->whereDate('start_date', $start_date)
                ->whereDate('end_date', $end_date)
                ->where('basic_info_id', $search_input['basic_info_id'])
                ->where('delete_flag', 0)
                ->first();

            if ($plan_week) {
                foreach ($plan_week->planDays as $plan_day) {
                    foreach ($plan_day->planDayTimeslots as $plan_day_timeslot) {
                        $result[] = $plan_day_timeslot;
                    }
                }
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function getOrCreatePlanDay($date, $basic_info_id)
    {
        $result = null;

        DB::beginTransaction();
        try {
            $plan_day  = PlanDay::with(['staff', 'user', 'planDayTimeslots'])
                ->whereDate('date', $date)
                ->where('basic_info_id', $basic_info_id)
                ->where('delete_flag', 0)
                ->first();

            if (!$plan_day) {
                $plan_week = PlanWeek::with(['planDays', 'staff', 'user'])
                    ->whereDate('start_date', '<=', $date)
                    ->whereDate('end_date', '>=', $date)
                    ->where('basic_info_id', $basic_info_id)
                    ->where('delete_flag', 0)
                    ->first();


                if ($plan_week) {
                    $plan_day = $this->planDayRepository->create([
                        'plan_week_id'          => $plan_week->id,
                        'room_id'               => $plan_week->room_id,
                        'team_id'               => $plan_week->team_id,
                        'position_id'           => $plan_week->position_id,
                        'basic_info_id'         => $plan_week->basic_info_id,
                        'plan_day_timeslot_ids' => [],
                        'three_new_habits'      => [],
                        'year'                  => $plan_week->year,
                        'quarter'               => $plan_week->quarter,
                        'week_in_quarter'       => $plan_week->week_in_quarter,
                        'day_in_week'           => date('w', strtotime($date)),
                        'date'                  => $date,
                        'created_by'            => auth()->id(),
                    ]);


                    $plan_day_ids = $plan_week->plan_day_ids;
                    $plan_day_ids[] = $plan_day->id;

                    $this->planWeekRepository->update($plan_week->id, [
                        'plan_day_ids'  => $plan_day_ids,
                        'updated_by'    =>  auth()->id(),
                    ]);
                }
            }
            $result = $plan_day;
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getPlanDayIdFromDate($request)
    {
        $result = [
            'status'  => false,
            'message' => "Chưa Có Kế Hoạch Ngày, Vui Lòng Thêm Công Việc Trước!",
            'data'    => null,
        ];


        try {
            $search_input = $request->all();

            $plan_day = PlanDay::where('basic_info_id', $search_input['basic_info_id'])
                ->whereDate('date', $search_input['date'])
                ->first();

            if ($plan_day)
                $result = [
                    'status'  => true,
                    'message' => "Có Kế Hoạch Ngày",
                    'data'    => $plan_day->id,
                ];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function getPlanWeekDataFromDates($request)
    {
        $result = null;

        try {
            $search_input = $request->all();
            $start_date =  $search_input['start'];
            $end_date = date('Y-m-d', strtotime('-1 day' . $search_input['end']));

            $plan_week = PlanWeek::with(['planDays', 'staff', 'user'])
                ->whereDate('start_date', $start_date)
                ->whereDate('end_date', $end_date)
                ->where('basic_info_id', $search_input['basic_info_id'])
                ->where('delete_flag', 0)
                ->first();

            if ($plan_week)
                $result = $plan_week;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function getPlanColors($request) {
        $result = [];
        try {
            $basic_info_id = $request['basic_info_id'];
            $plan_colors = PlanColor::where('basic_info_id', $basic_info_id)->where('delete_flag', 0)->get();
            if (!empty($plan_colors))   
                $result = $plan_colors;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;

    }
}
