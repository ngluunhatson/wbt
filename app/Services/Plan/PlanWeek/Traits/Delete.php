<?php

namespace App\Services\Plan\PlanWeek\Traits;

use App\Models\PlanColor;
use App\Models\PlanDayTimeslot;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait Delete
{
    public function delete($id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $query = $this->planWeekRepository->delete($id);
            if ($query) {
                $result['status'] = true;
                $result['message'] = 'Xóa thành công !!';
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function deletePlanDayTimeslot($plan_day_timeslot_id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa không thành công !!',
        ];

        DB::beginTransaction();
        try {

            $plan_day_timeslot = $this->planDayTimeslotRepository->loadRelation(['staff', 'user'])->where('id', $plan_day_timeslot_id)->first();

            $query = $this->planDayTimeslotRepository->delete($plan_day_timeslot_id);

            if ($query) {

                $plan_day = $this->planDayRepository->loadRelation(['planDayTimeslots', 'staff', 'user'])->where('id', $plan_day_timeslot->plan_day_id)->first();

                $plan_day_timeslot_ids_update =  array_diff($plan_day->plan_day_timeslot_ids, [$plan_day_timeslot->id]);


                $this->planDayRepository->update($plan_day->id, [
                    'plan_day_timeslot_ids' => $plan_day_timeslot_ids_update,
                    'updated_by'            => auth()->id(),
                ]);

                $result['status'] = true;
                $result['message'] = 'Xóa thành công !!';
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function deletePlanColor($plan_color_id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa không thành công !!',
        ];
        DB::beginTransaction();
        try {
            PlanColor::where('id', $plan_color_id) -> update([
                'updated_by'  => auth() -> id(),
                'delete_flag' => 1
            ]);
            $result['status'] = true;
            $result['message'] = 'Xóa thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;

    }
}
