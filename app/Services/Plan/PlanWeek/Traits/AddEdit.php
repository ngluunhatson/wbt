<?php

namespace App\Services\Plan\PlanWeek\Traits;

use App\Models\PlanColor;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait AddEdit
{
    public function saveData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function addPlanDayTimeslot($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $create_input = $request->all();

            $plan_day = $this->getOrCreatePlanDay($create_input['task_date'], $create_input['basic_info_id']);
            if ($plan_day) {
                $start_time_float = intval(substr($create_input['task_start_time'], 0, 2)) + intval(substr($create_input['task_start_time'], 3)) / 60;
                $end_time_float   = intval(substr($create_input['task_end_time'], 0, 2))   + intval(substr($create_input['task_end_time'], 3)) / 60;

                $plan_day_timeslot = $this->planDayTimeslotRepository->create([
                    'plan_day_id'   => $plan_day->id,
                    'room_id'       => $plan_day->room_id,
                    'team_id'       => $plan_day->team_id,
                    'position_id'   => $plan_day->position_id,
                    'basic_info_id' => $plan_day->basic_info_id,
                    'date'          => $plan_day->date,
                    'start_time'    => $start_time_float,
                    'end_time'      => $end_time_float,
                    'title'         => $create_input['task_title'],
                    'location'      => $create_input['task_location'],
                    'description'   => $create_input['task_description'],
                    'plan_color_id' => (!array_key_exists('task_color_id', $create_input) || $create_input['task_color_id'] == -1)  ? null : $create_input['task_color_id'],
                    'created_by'    => auth()->id(),

                ]);

                $plan_day_timeslot_ids               = $plan_day->plan_day_timeslot_ids;
                $plan_day_timeslot_ids[]             = $plan_day_timeslot->id;

                $this->planDayRepository->update($plan_day->id, [
                    'plan_day_timeslot_ids'  => $plan_day_timeslot_ids,
                    'updated_by'             => auth()->id(),
                ]);
                $result['status'] = true;
                $result['message'] = 'Lưu thành công !!';
            } else
                $result['message'] = 'Chưa Có Kế Hoạch Quý! Tạo kế hoạch Quý trước';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updatePlanDayTimeslot($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $update_input = $request->all();

            $plan_day_timeslot = $this->planDayTimeslotRepository->loadRelation(['staff', 'user'])->where('id', $update_input['plan_day_timeslot_id'])->first();
            $plan_day_old = $this->getOrCreatePlanDay($plan_day_timeslot->date, $plan_day_timeslot->basic_info_id);

            $plan_day_old_timeslot_ids = $plan_day_old->plan_day_timeslot_ids;

            if (strtotime($plan_day_timeslot->date) != strtotime($update_input['task_date'])) {
                $plan_day_new = $this->getOrCreatePlanDay($update_input['task_date'], $update_input['basic_info_id']);
                if ($plan_day_new) {

                    $plan_day_old_timeslot_ids_update = array_diff($plan_day_old_timeslot_ids, [$plan_day_timeslot->id]);

                    $plan_day_new_timeslots_ids = $plan_day_new->plan_day_timeslot_ids;
                    $plan_day_new_timeslots_ids[] = $plan_day_timeslot->id;


                    $this->planDayRepository->update($plan_day_old->id, [
                        'plan_day_timeslot_ids' => $plan_day_old_timeslot_ids_update,
                        'updated_by'            => auth()->id(),
                    ]);

                    $this->planDayRepository->update($plan_day_new->id, [
                        'plan_day_timeslot_ids' => $plan_day_new_timeslots_ids,
                        'updated_by'            => auth()->id(),
                    ]);


                   
                } else {
                    $result['message'] = 'Chưa Có Kế Hoạch Quý! Tạo kế hoạch Quý trước';
                    return $result;
                }
            }
            $start_time_float = intval(substr($update_input['task_start_time'], 0, 2)) + intval(substr($update_input['task_start_time'], 3)) / 60;
            $end_time_float   = intval(substr($update_input['task_end_time'], 0, 2))   + intval(substr($update_input['task_end_time'], 3)) / 60;

            $this->planDayTimeslotRepository->update($plan_day_timeslot->id, [
                'title'         => $update_input['task_title'],
                'date'          => $update_input['task_date'],
                'start_time'    => $start_time_float,
                'end_time'      => $end_time_float,
                'location'      => $update_input['task_location'],
                'description'   => $update_input['task_description'],
                'plan_color_id' => (!array_key_exists('task_color_id', $update_input) || $update_input['task_color_id'] == -1) ? null : $update_input['task_color_id'],
                'updated_by'    => auth()->id(),
            ]);

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function saveWeekAspects($request)
    {
        $result = [
            'status'  => false,
            'message' => 'Lưu Khía Cạnh không thành công!',
            'data'    => [],
        ];

        DB::beginTransaction();
        try {
            $update_input = $request->except('_token');

            $five_weekly_aspects_update = [];

            for ($i = 1; $i <= 5; $i++)
                if (array_key_exists('week_aspect' . $i, $update_input))
                    $five_weekly_aspects_update[] = $update_input['week_aspect' . $i];
                else
                    $five_weekly_aspects_update[] = '';
            $this->planWeekRepository->update(
                $update_input['plan_week_id_week_aspect_form'],
                ['five_weekly_aspects' => $five_weekly_aspects_update]
            );


            $result['status']  = true;
            $result['message'] = 'Lưu Khía Cạnh thành công!';
            $result['data']    = $five_weekly_aspects_update;

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function upsertPlanColor($request)
    {
        $result = [
            'status'  => false,
            'message' => 'Lưu Màu không thành công!',
        ];
        DB::beginTransaction();
        try {   
            $request_input = $request -> all();

            if($request_input['mode_edit'] == 0) {
                $plan_color = PlanColor::where('basic_info_id', $request_input['basic_info_id']) -> where('hex_code', $request_input['hex_code']) -> where('delete_flag', 0) -> first();

                if ($plan_color) {
                    $result['message'] = 'Đã tồn tại màu này!';
                    return $result;
                }

                PlanColor::firstOrCreate([
                    'basic_info_id' => $request_input['basic_info_id'],
                    'hex_code'      => $request_input['hex_code'],
                    'category_name' => $request_input['category_name'],
                    'created_by'    => auth() -> id(),
                ]);
            } else  {
                PlanColor::where('id', $request_input['plan_color_id']) -> update([
                    'hex_code'      => $request_input['hex_code'],
                    'category_name' => $request_input['category_name'],
                    'updated_by'    => auth() -> id(),
                ]);
            }
            $result = [
                'status'  => true,
                'message' => 'Lưu Màu thành công!',
            ];
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;

    }
}
