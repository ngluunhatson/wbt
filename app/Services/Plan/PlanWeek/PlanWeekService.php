<?php

namespace App\Services\Plan\PlanWeek;

use App\Repositories\Plan\PlanDayRepository;
use App\Repositories\Plan\PlanDayTimeslotRepository;
use App\Repositories\Plan\PlanWeekRepository;
use App\Services\BaseService;
use App\Services\Plan\PlanWeek\Traits\AddEdit;
use App\Services\Plan\PlanWeek\Traits\Delete;
use App\Services\Plan\PlanWeek\Traits\Search;

class PlanWeekService extends BaseService
{
    use Search, AddEdit, Delete;
    public $planWeekRepository, $planDayTimeslotRepository, $planDayRepository;

    public function __construct(
        PlanWeekRepository $planWeekRepository, PlanDayTimeslotRepository $planDayTimeslotRepository, 
        PlanDayRepository $planDayRepository
    ) {
        $this -> planWeekRepository        = $planWeekRepository;
        $this -> planDayTimeslotRepository = $planDayTimeslotRepository;
        $this -> planDayRepository         = $planDayRepository;
    }
}
