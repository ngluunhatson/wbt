<?php

namespace App\Services\Plan\PlanDay;

use App\Repositories\Plan\PlanDayRepository;
use App\Repositories\Plan\PlanDayTimeslotRepository;
use App\Repositories\Plan\PlanWeekRepository;
use App\Services\BaseService;
use App\Services\Plan\PlanDay\Traits\AddEdit;
use App\Services\Plan\PlanDay\Traits\Delete;
use App\Services\Plan\PlanDay\Traits\Search;

class PlanDayService extends BaseService
{
    use Search, AddEdit, Delete;
    public $planDayRepository;

    public function __construct(
        PlanDayRepository $planDayRepository, PlanWeekRepository $planWeekRepository, PlanDayTimeslotRepository $planDayTimeslotRepository
    ) {
        $this -> planDayRepository         = $planDayRepository;
        $this -> planWeekRepository        = $planWeekRepository;
        $this -> planDayTimeslotRepository = $planDayTimeslotRepository;
    }
}
