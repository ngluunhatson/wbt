<?php

namespace App\Services\Plan\PlanDay\Traits;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait Delete
{
    public function delete($id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $query = $this->planDayRepository->delete($id);
            if ($query) {
                $result['status'] = true;
                $result['message'] = 'Xóa thành công !!';
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
