<?php

namespace App\Services\Plan\PlanDay\Traits;

use App\Models\PlanDay;
use App\Models\PlanWeek;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{

    public function getPlanDayById($id) 
    {
        $result = null;
        try {
            $plan_day = $this -> planDayRepository -> loadRelation(['staff', 'user', 'planDayTimeslots'])
                ->where('id', $id) 
                -> first();
            
            if ($plan_day)
                $result = $plan_day;

        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function findCurPlanDayOfStaff($basic_info_id)
    {
        $result = null;
        try {
            $now_date_str = now() -> format('Y-m-d');
            $plan_day = PlanDay::with(['staff', 'user', 'planDayTimeslots'])
                -> whereDate('date', $now_date_str) -> where('basic_info_id', $basic_info_id) 
                -> first();
            
            if ($plan_day)
                $result = $plan_day;

        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function filterPlanDay($request)
    {
        $result = [];
        try {
            $cur_query = PlanDay::with([
                'staff', 'planDayTimeslots', 'user',
                'staff.organizational.room',  'staff.organizational.position',
                'staff.organizational.team'
            ])->where('delete_flag', 0);

            foreach ($request->all() as $key => $value) {
                if ($key != 'start_date' &&  $key != 'end_date') {
                    if (intval($value) != -1) {
                        $cur_query->where($key, $value);
                    }
                } else {
                    $cur_query->where(function ($query_sub) use ($request) {
                        $query_sub->whereDate('date', '>=', $request['start_date']);
                        if ($request['end_date'])
                            $query_sub->whereDate('date', '<=', $request['end_date']);
                    });
                }
            }

            $return_result = $cur_query->get();

            if (!empty($return_result))
                $result = $return_result;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function checkIfExistPlanWeek($request) {
        $search_input = $request -> all();

        $date_str = $search_input['task_date'];
        $basic_info_id = $search_input['basic_info_id'];

        $plan_week = PlanWeek::with(['planDays', 'staff', 'user'])
            ->whereDate('start_date', '<=', $date_str)
            ->whereDate('end_date', '>=', $date_str)
            ->where('basic_info_id', $basic_info_id)
            ->where('delete_flag', 0)
            ->first();
        

        return $plan_week;
    }
}
