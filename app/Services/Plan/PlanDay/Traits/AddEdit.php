<?php

namespace App\Services\Plan\PlanDay\Traits;

use App\Models\PlanDay;
use App\Models\PlanWeek;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait AddEdit
{
    public function saveData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $update_input = $request->all();

            $plan_day = $this->getPlanDayById($update_input['plan_day_id']);
            $basic_info_id = $update_input['basic_info_id'];

            $new_date = $update_input['task_date'];
            $old_date = $plan_day->date;

            $plan_week_old =  PlanWeek::with(['planDays', 'staff', 'user'])
                ->whereDate('start_date', '<=', $old_date)
                ->whereDate('end_date', '>=', $old_date)
                ->where('basic_info_id', $basic_info_id)
                ->where('delete_flag', 0)
                ->first();

            $plan_week_new = PlanWeek::with(['planDays', 'staff', 'user'])
                ->whereDate('start_date', '<=', $new_date)
                ->whereDate('end_date', '>=', $new_date)
                ->where('basic_info_id', $basic_info_id)
                ->where('delete_flag', 0)
                ->first();

            if ($plan_week_old->id != $plan_week_new->id) {

                $plan_week_new_plan_day_ids = $plan_week_new->plan_day_ids;
                $plan_week_old_plan_day_ids = $plan_week_old->plan_day_ids;

                $plan_week_old_plan_day_ids_update = array_diff($plan_week_old_plan_day_ids, [$plan_day->id]);
                $plan_week_new_plan_day_ids[] = $plan_day->id;

                $this->planWeekRepository->update($plan_week_old->id, [
                    'plan_day_ids'      => $plan_week_old_plan_day_ids_update,
                    'updated_by'        => auth()->id()
                ]);

                $this->planWeekRepository->update($plan_week_new->id, [
                    'plan_day_ids'      => $plan_week_new_plan_day_ids,
                    'updated_by'        => auth()->id()
                ]);
            }

            for ($i = 1; $i <= count($plan_day->planDayTimeslots); $i++) {
                $this -> planDayTimeslotRepository -> update($update_input['taskId'.$i], [
                    'isDone'        => array_key_exists('taskDone'.$i, $update_input),
                    'updated_by'    => auth() -> id()
                ]); 
            }

            $this->planDayRepository->update($plan_day->id, [
                'plan_week_id'         => $plan_week_new->id,
                'three_new_habits'     => [$update_input['new_habit1'] ?? '', $update_input['new_habit2']  ?? '', $update_input['new_habit3']  ?? '' ],
                'lesson_main'          => $update_input['lesson_main'],
                'lesson_failure'       => $update_input['lesson_failure'],
                'lesson_success'       => $update_input['lesson_success'],
                'lesson_thankful'      => $update_input['lesson_thankful'],
                'updated_by'           => auth() -> id(),
            ]);

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
