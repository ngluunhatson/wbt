<?php

namespace App\Services\Dayoff;

use App\Repositories\BasicInfo\BasicInfoRepository;
use App\Repositories\Confirm\ConfirmRepository;
use App\Repositories\Dayoff\DayoffConfirmRepository;
use App\Repositories\Dayoff\DayoffRepository;
use App\Repositories\Dayoff\DetailDayoffRepository;
use App\Repositories\Organizational\OrganizationalRepository;
use App\Repositories\Position\PositionRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\Team\TeamRepository;
use App\Repositories\Timekeep\TimekeepRepository;
use App\Repositories\TrackingDayOff\TrackingDayOffRepository;
use App\Services\BaseService;
use App\Services\Dayoff\Traits\AddEdit;
use App\Services\Dayoff\Traits\Delete;
use App\Services\Dayoff\Traits\Search;

class DayOffService extends BaseService
{
    use Search, AddEdit, Delete;
    public $roomRepository, $teamRepository, $positionRepository, $organizationalRepository, $dayoffRepository, $detailDayOffRepository, $confirmRepository, $dayoffConfirmRepository, $basicInfoRepository, $trackingDayOffRepository, $timekeepRepository;

    public function __construct(
        RoomRepository $roomRepository,
        TeamRepository $teamRepository,
        PositionRepository $positionRepository,
        DayoffRepository $dayoffRepository,
        OrganizationalRepository $organizationalRepository,
        DetailDayOffRepository $detailDayOffRepository,
        ConfirmRepository $confirmRepository,
        DayoffConfirmRepository $dayoffConfirmRepository,
        BasicInfoRepository $basicInfoRepository,
        TrackingDayOffRepository $trackingDayOffRepository,
        TimekeepRepository $timekeepRepository,
    ) {
        $this->roomRepository = $roomRepository;
        $this->teamRepository = $teamRepository;
        $this->positionRepository = $positionRepository;
        $this->dayoffRepository = $dayoffRepository;
        $this->detailDayOffRepository = $detailDayOffRepository;
        $this->organizationalRepository = $organizationalRepository;
        $this->confirmRepository = $confirmRepository;
        $this->dayoffConfirmRepository = $dayoffConfirmRepository;
        $this->basicInfoRepository = $basicInfoRepository;
        $this->trackingDayOffRepository = $trackingDayOffRepository;
        $this->timekeepRepository = $timekeepRepository;
    }
}
