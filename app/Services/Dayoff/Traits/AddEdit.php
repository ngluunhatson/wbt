<?php

namespace App\Services\Dayoff\Traits;

use App\Models\Confirm;
use App\Models\DayOff;
use App\Models\DetailsDayOff;
use App\Models\Organizational;
use App\Models\Timekeeping;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait AddEdit
{

    public function updateTimeKeepHelper($timeKeepDayType, $dayOffType,  $isHalfDayOff, $numberOfTrackingDayOffs, $numberOfCompanyDayOffs)
    {
        $returnDayType = $timeKeepDayType;
        $sameCompanyTrackingDay = $numberOfTrackingDayOffs == 0.5 && $numberOfCompanyDayOffs > 0;

        switch ($timeKeepDayType) {
            case 1:
                switch ($dayOffType) {
                    case 1:
                        $returnDayType = 10;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

            case 2:
                switch ($dayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ? ($sameCompanyTrackingDay ? 3 : 11) : 5;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

                // case 3:
                //     switch ($dayOffType) {
                //         case 1:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 5:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //     }

                //     break;

                // case 4:
                //     switch ($dayOffType) {
                //         case 1:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 5:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //     }

                //     break;

            case 5:
                switch ($dayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ?  ($sameCompanyTrackingDay ? 3 : 11) : $returnDayType;
                        break;

                    case 5:
                        $returnDayType = ($isHalfDayOff) ? 3 : 4;
                        break;
                }

                break;

            case 6:
                switch ($dayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ? 13 : 5;
                        break;
                    case 5:
                        $returnDayType = ($isHalfDayOff) ? 12 : 4;
                        break;
                }

                break;

            case 7:
                switch ($dayOffType) {
                    case 1:
                        $returnDayType = 10;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

                // case 8:
                //     switch ($dayOffType) {
                //         case 1:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 5:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //     }

                //     break;

                // case 9:
                //     switch ($dayOffType) {
                //         case 1:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 5:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //     }

                //     break;

            case 10:
                switch ($dayOffType) {
                    case 1:
                        $returnDayType = $returnDayType;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

            case 11:
                switch ($dayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ? ($sameCompanyTrackingDay ? 3 : $returnDayType) : 5;
                        break;

                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

            case 12:
                switch ($dayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ?  ($sameCompanyTrackingDay ? 3 : 11) : 5;
                        break;

                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

            case 13:
                switch ($dayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ?  ($sameCompanyTrackingDay ? 3 : 11) : 6;
                        break;

                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

            case 14:
                switch ($dayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ? ($sameCompanyTrackingDay ? 3 : 11) : 5;
                        break;

                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;
        }
        return $returnDayType;
    }


    public function updateTimeKeep($dayoff_X, $timekeepTargets)
    {
        DB::beginTransaction();
        try {
            $dayOffStartDateStrToTime = strtotime($dayoff_X->detailDayOff->start_date);
            $dayOffEndDateStrToTime   = strtotime($dayoff_X->detailDayOff->end_date);

            $dayOffStartYearInt  = intval(date('Y', $dayOffStartDateStrToTime));
            $dayOffStartMonthInt = intval(date('m', $dayOffStartDateStrToTime));
            $dayoffStartDayInt   = intval(date('d', $dayOffStartDateStrToTime));

            $dayOffEndYearInt  = intval(date('Y', $dayOffEndDateStrToTime));
            $dayOffEndMonthInt = intval(date('m', $dayOffEndDateStrToTime));
            $dayoffEndDayInt   = intval(date('d', $dayOffEndDateStrToTime));

            $isOffHalfDayStart = $dayoff_X->detailDayOff->start_time == '13:00';
            $isOffHalfDayEnd   = $dayoff_X->detailDayOff->end_time   == '12:00';

            $orginialNumberOfTrackingDayOffs = $dayoff_X->detailDayOff->number_of_tracking_day_offs;
            $originalNumberOfCompanyDayOffs  = $dayoff_X->detailDayOff->total_day_off - $orginialNumberOfTrackingDayOffs;



            $monthLoopCount = 0;

            for ($y = $dayOffStartYearInt; $y <= $dayOffEndYearInt; $y++) {

                $isLeapYear = false;
                if ($y % 4 == 0)
                    if ($y % 100 == 0) {
                        if ($y % 400 == 0)
                            $isLeapYear = true;
                    } else
                        $isLeapYear = true;


                $monthLimit = $y != $dayOffEndYearInt ? 12 : $dayOffEndMonthInt;

                $m = ($y != $dayOffEndYearInt) ? 1 : $dayOffStartMonthInt;

                while ($m <= $monthLimit) {

                    foreach ($timekeepTargets as $timekeepTarget) {

                        $cur_timekeep_object_form = Timekeeping::where('basic_info_id', $timekeepTarget->basic_info_id)->where('year', $y)->where('month', $m)->first();

                        if ($cur_timekeep_object_form) {
                            $numberOfTrackingDayOffs = $orginialNumberOfTrackingDayOffs;
                            $numberOfCompanyDayOffs  = $originalNumberOfCompanyDayOffs;

                            $cur_timekeep_array_form = $cur_timekeep_object_form->toArray();
                            $cur_timekeep_update_input = [];


                            $limitDayInMonth = [31, ($isLeapYear) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][$m - 1];

                            $dayLimit = $limitDayInMonth;
                            $d = 1;

                            if (($m == $monthLimit) && ($y == $dayOffEndYearInt)) {
                                $dayLimit = $dayoffEndDayInt;
                                if ($monthLoopCount == 0) {
                                    $d = $dayoffStartDayInt;
                                }
                            }

                            while ($d <= $dayLimit) {

                                $isStartEndSameDate = ($d == $dayoffEndDayInt) && ($m == $dayOffEndMonthInt) && ($y ==  $dayOffEndYearInt);

                                $result_timekeep_day_type = $cur_timekeep_array_form['day_' . $d];

                                $isHalfDayOff  = ($isStartEndSameDate && ($isOffHalfDayStart || $isOffHalfDayEnd)) || ($numberOfTrackingDayOffs == 0.5);


                                if ($numberOfTrackingDayOffs > 0) {

                                    $result_timekeep_day_type = $this->updateTimeKeepHelper($result_timekeep_day_type, 1, $isHalfDayOff, $numberOfTrackingDayOffs, $numberOfCompanyDayOffs);
                                    $numberOfTrackingDayOffs -= ($isHalfDayOff) ? 0.5 : 1;
                                } else {
                                    $result_timekeep_day_type = $this->updateTimeKeepHelper($result_timekeep_day_type, 5, $isHalfDayOff, $numberOfTrackingDayOffs, $numberOfCompanyDayOffs);
                                    $numberOfCompanyDayOffs -= ($isHalfDayOff) ? 0.5 : 1;
                                }


                                if ($result_timekeep_day_type != $cur_timekeep_array_form['day_' . $d])
                                    $cur_timekeep_update_input['day_' . $d] = $result_timekeep_day_type;

                                $d += 1;
                            }

                            if (!empty($cur_timekeep_update_input)) {
                                $cur_timekeep_update_input['updated_by'] = auth()->id();
                                $this->timekeepRepository->update($cur_timekeep_array_form['id'], $cur_timekeep_update_input);
                            }
                        }
                    }

                    $m += 1;
                    $monthLoopCount += 1;
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }
    }

    public function saveData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $input_all = $request->except('_token', 'selectConfirm', 'type_confirm_1', 'type_confirm_2', 'type_confirm_3');
            $dataConfirm = $request['selectConfirm'];

            $creatorConfirmFlag = array_key_exists("assign", $input_all);

            if ($input_all["number_of_tracking_day_offs"] == null)
                $input_all["number_of_tracking_day_offs"] = 0;


            $detaildayoff                              =     new DetailsDayOff;
            $detaildayoff->start_date                  =     $input_all["start_date"];
            $detaildayoff->start_time                  =     $input_all["start_time"];
            $detaildayoff->end_date                    =     $input_all["end_date"];
            $detaildayoff->end_time                    =     $input_all["end_time"];
            $detaildayoff->total_day_off               =     $input_all["total_day_off"];
            $detaildayoff->number_of_tracking_day_offs =     floatval($input_all["number_of_tracking_day_offs"]);
            $detaildayoff->created_by                  =     auth()->id();
            $detaildayoff->save();



            $input_day_off = [
                "room_id"       =>     $input_all["room_id"]                ??  null,
                "team_id"       =>     $input_all["team_id"]                ??  null,
                "position_id"   =>     $input_all["position_id"]            ??  null,
                "basic_info_id" =>     $input_all["basic_info_id"]          ??  null,
                "title"         =>     $input_all["title"]                  ??  null,
                "note"          =>     $input_all["note"]                   ??  null,
                "confirm_id"    =>     [],
                "detail_id"     =>     $detaildayoff->id,
                "created_by"    =>     auth()->id(),
                "status"        =>     1,
            ];

            $input_day_off["all_company"] = (array_key_exists("all_company", $input_all)) ? 1 : 0;

            $createdDayOff = $this->dayoffRepository->create($input_day_off);


            if ($dataConfirm) {
                foreach ($dataConfirm as $key => $confirmerID) {


                    $confirmX = new Confirm;
                    $confirmX->basic_info_id = $confirmerID ?? null;
                    $confirmX->type_confirm = $request['type_confirm_' . ($key + 2)];
                    $confirmX->assignment_type =  config('constants.assignment_type')[7];
                    $confirmX->status = 0;
                    $confirmX->created_by = auth()->id();

                    $confirmX->save();

                    $this->dayoffConfirmRepository->create([
                        'day_off_id'        =>  $createdDayOff->id,
                        'confirm_id'        =>  $confirmX->id
                    ]);
                    $confirm_id[] = $confirmX->id;


                    if ($creatorConfirmFlag) {
                        if ($request['type_confirm_' . ($key + 2)] == 2) {
                            $createdDayOff->status = 2;
                            $user = User::where('basic_info_id', $confirmerID)->first();
                            $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                            $link = route('dayoff.view', $createdDayOff->id);
                            $this->sendNotification($user, $createdDayOff->id, $link, $message);
                        }
                    }
                }

                $createdDayOff->confirm_id = $confirm_id;

                $createdDayOff->save();
            }



            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';


            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $new_input = $request->except('_token');

            $creatorConfirmFlag = array_key_exists("assign", $new_input);
            $oldDataDayOff = $this->getDataByID($new_input['day_off_id']);
            $new_confirm_data = $new_input['selectConfirm'];
            $confirm_ids = $new_input['confirm_id'];

            $new_day_off_data = [
                "room_id"       =>     $new_input["room_id"]                ??  null,
                "team_id"       =>     $new_input["team_id"]                ??  null,
                "position_id"   =>     $new_input["position_id"]            ??  null,
                "basic_info_id" =>     $new_input["basic_info_id"]          ??  null,
                "title"         =>     $new_input["title"]                  ??  null,
                "note"          =>     $new_input["note"]                   ??  null,
                "updated_by"    =>     auth()->id(),
                "status"        => ($creatorConfirmFlag ? 2 : 1),
            ];

            if ($new_input["number_of_tracking_day_offs"] == null)
                $new_input["number_of_tracking_day_offs"] = 0;

            $new_detail_day_off_data = [
                'start_date'                  =>     $new_input["start_date"],
                'start_time'                  =>     $new_input["start_time"],
                'end_date'                    =>     $new_input["end_date"],
                'end_time'                    =>     $new_input["end_time"],
                'total_day_off'               =>     $new_input["total_day_off"],
                'number_of_tracking_day_offs' =>     floatval($new_input["number_of_tracking_day_offs"]),
                "updated_by"                  =>     auth()->id(),
            ];


            $new_day_off_data["all_company"] = 0;
            if (array_key_exists("all_company", $new_input)) {
                $new_day_off_data["all_company"] = 1;
                $new_day_off_data["room_id"] = null;
                $new_day_off_data["team_id"] = null;
                $new_day_off_data["position_id"] = null;
                $new_day_off_data["basic_info_id"] = null;
            }


            $this->detailDayOffRepository->update($oldDataDayOff->detaildayoff->id, $new_detail_day_off_data);

            if ($new_confirm_data) {
                for ($i = 0; $i < 2; $i++) {
                    $this->confirmRepository->update(
                        $confirm_ids[$i],
                        [
                            'basic_info_id' => $new_confirm_data[$i],
                            'updated_by'    => auth()->id()
                        ]
                    );
                }
            }

            $this->dayoffRepository->update($new_input['day_off_id'], $new_day_off_data);

            if ($creatorConfirmFlag) {
                $user = User::where('basic_info_id', $new_confirm_data[0])->first();
                $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                $link = route('dayoff.view', $new_input['day_off_id']);
                $this->sendNotification($user, $new_input['day_off_id'], $link, $message);
            }

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateStatus($request)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhật không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $update_status_input = $request->except('_token');
            $new_data = [];
            $confirm_flag = ($update_status_input['confirm_flag'] == 1);
            $link = route('dayoff.view', $update_status_input['day_off_id']);

            $noti_targets = [User::where('id', $update_status_input['day_off_creator_user_id'])->first()];
            $noti_targets_user_ids = [$update_status_input['day_off_creator_user_id']];

            $noti_message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;


            if ($update_status_input['next_confirm_staff_basic_id'] == null) {
                $new_data['status'] = 6;
                $noti_targets[] = User::where('basic_info_id', $update_status_input['hr_staff_basic_id'])->first();
                $noti_targets_user_ids[] = $update_status_input['hr_staff_basic_id'];

                if ($confirm_flag) {
                    $new_data['status'] = 4;
                    $new_data['review_date'] = Carbon::now()->format('Y-m-d');
                    $noti_message = 'Thông báo !! Yêu cầu đã được duyệt bởi ' . auth()->user()->name;
                    $potential_noti_targets = [];

                    $dayoff_X = DayOff::where('id', $update_status_input['day_off_id'])->first();
                    if ($dayoff_X->all_company == 1)
                        $potential_noti_targets = User::where('delete_flag', 0)->get();
                    else {
                        $organizationals = [];
                        if ($dayoff_X->team_id == 0)
                            $organizationals = Organizational::where('room_id', $dayoff_X->room_id)->whereNotNull('basic_info_id')->get();

                        else {
                            if ($dayoff_X->position_id == 0)
                                $organizationals = Organizational::where('team_id', $dayoff_X->team_id)->whereNotNull('basic_info_id')->get();
                            else {
                                if ($dayoff_X->basic_info_id == 0)
                                    $organizationals = Organizational::where('position_id', $dayoff_X->position_id)->whereNotNull('basic_info_id')->get();
                                else
                                    $organizationals =  Organizational::where('basic_info_id', $dayoff_X->basic_info_id)->get();
                            }
                        }

                        $potential_noti_target_basic_info_ids = [];
                        foreach ($organizationals as $organizational)
                            $potential_noti_target_basic_info_ids[] = $organizational->basic_info_id;


                        $potential_users = (User::whereIn('basic_info_id', $potential_noti_target_basic_info_ids))->get();
                        foreach ($potential_users as $potential_user) {
                            $potential_noti_targets[] = $potential_user;
                        }
                    }


                    foreach ($potential_noti_targets as $potential_target) {
                        if (!in_array($potential_target->id, $noti_targets_user_ids)) {
                            $noti_targets[] = $potential_target;
                            $noti_targets_user_ids[] = $potential_target->id;
                        }
                    }

                    $timekeepTargets = ($dayoff_X->all_company == 1) ? $potential_noti_targets : $potential_users;

                    $this->updateTimeKeep($dayoff_X, $timekeepTargets);
                }
            } else {
                $new_data['status'] = 5;

                if ($confirm_flag) {
                    $noti_message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                    $noti_targets = [User::where('basic_info_id', $update_status_input['next_confirm_staff_basic_id'])->first()];
                    $new_data['status'] = 3;
                }
            }

            foreach ($noti_targets as $noti_target)
                $this->sendNotification($noti_target, $update_status_input['day_off_id'], $link, $noti_message);




            $new_data['updated_by'] = auth()->id();

            $this->dayoffRepository->update($update_status_input['day_off_id'], $new_data);

            $this->confirmRepository->update($update_status_input['confirm_id'], [
                'status'     => $update_status_input['confirm_flag'],
                'note'       => $update_status_input['reason'],
                'updated_by' => auth()->id(),
            ]);

            $result['status'] = true;
            $result['message'] = 'Cập nhật thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }


        return $result;
    }
}
