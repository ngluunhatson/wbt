<?php

namespace App\Services\Dayoff\Traits;

use App\Models\Organizational;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{
    public function getRooms()
    {
        $result = [];
        try {
            $rooms = $this->roomRepository->findAll();
            $result = !empty($rooms) ? $rooms : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getTeamsByRoomID($room_id)
    {
        $result = [];
        try {
            $teams = $this->teamRepository->findByCondition([
                'room_id' => $room_id,
            ]);
            $result = !empty($teams) ? $teams : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getPositionByTeamID($team_id)
    {
        $result = [];
        try {
            $positions = $this->positionRepository->findByCondition([
                'team_id' => $team_id,
            ]);
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getStaffByConditions($room_id, $team_id, $position_id)
    {
        $result = [];
        try {
            $organizationals = Organizational::where([
                'room_id' => $room_id,
                'team_id' => $team_id,
                'position_id' => $position_id,
                'delete_flag' => 0,
            ])->get();
            $staffs = [];
            foreach ($organizationals as $organizational)

                $staffs[] = $organizational->staff;

            $result = !empty($staffs) ? $staffs : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return ($result);
    }

    public function getStaffsByTeam($room_id, $team_id, $position_id)
    {
        $result = [];
        try {
            $positions = $this->organizationalRepository->getStaffByConditions($room_id, $team_id, $position_id);
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataByID($id)
    {
        $result = [];
        try {

            $dayoff = $this->dayoffRepository->loadRelation(['detailDayOff', 'room', 'user'])->find($id);
            $result = !empty($dayoff) ? $dayoff : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getBasicInfoById($basic_info_id)
    {
        $result = [];
        try {
            $basic_info = $this->basicInfoRepository->getById($basic_info_id);
            $result = !empty($basic_info) ? $basic_info : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function getAllDayOffs()
    {
        $result = [];
        try {

            $dayoffs = $this->dayoffRepository->loadRelation(['detailDayOff', 'room', 'user'])->where('delete_flag', 0);
            $result = !empty($dayoffs) ? $dayoffs : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function getAllDayOffsAvailableToUser($id, $mode = 'user_id')
    {
        $result = [];
        try {


            $dayoffs = [];
            $dayoffIds = [];

            $user_basic_info_id = $id;
            if ($mode == 'user_id')
                $user_basic_info_id  = User::where('id', $id)->first()->basic_info_id;

           

            $user_organizational = Organizational::where('basic_info_id', $user_basic_info_id)->first();

            $user_room_id        = $user_organizational->room->id;
            $user_team_id        = $user_organizational->team->id;
            $user_position_id    = $user_organizational->position->id;


            $allDayOffCollectionsQueried = [];

            $allDayOffCollectionsQueried[] = $this->dayoffRepository->loadRelation(['detailDayOff', 'room', 'user'])->where('delete_flag', 0)->where('all_company', 1)->where('status', 4);


            $allDayOffCollectionsQueried[] = $this->dayoffRepository->loadRelation(['detailDayOff', 'room', 'user'])->where('delete_flag', 0)->where('room_id', $user_room_id)->where('team_id', 0)->where('status', 4);

            $allDayOffCollectionsQueried[] = $this->dayoffRepository->loadRelation(['detailDayOff', 'room', 'user'])->where('delete_flag', 0)
                ->where('room_id', $user_room_id)->where('team_id', $user_team_id)->where('position_id', 0)->where('status', 4);

            $allDayOffCollectionsQueried[] = $this->dayoffRepository->loadRelation(['detailDayOff', 'room', 'user'])->where('delete_flag', 0)
                ->where('room_id', $user_room_id)->where('team_id', $user_team_id)->where('position_id', $user_position_id)->where('basic_info_id', 0)->where('status', 4);

            $allDayOffCollectionsQueried[] = $this->dayoffRepository->loadRelation(['detailDayOff', 'room', 'user'])->where('delete_flag', 0)
                ->where('basic_info_id', $user_basic_info_id)->where('status', 4);


            if ($user_organizational->position->flag_chart == 0) {
                $cvnv_position_id = ($user_organizational->position->rank == 2) ? ($user_position_id - 2) : ($user_position_id - 1);
                $allDayOffCollectionsQueried[] = $this->dayoffRepository->loadRelation(['detailDayOff', 'room', 'user'])->where('delete_flag', 0)
                    ->where('room_id', $user_room_id)->where('team_id', $user_team_id)->where('position_id', $cvnv_position_id)->where('basic_info_id', 0)->where('status', 4);
            }


            $allDayOffsQueried = collect();

            foreach ($allDayOffCollectionsQueried as $dayOffCollection) {
                foreach ($dayOffCollection as $x) {
                    $allDayOffsQueried->add($x);
                }
            }

            foreach ($allDayOffsQueried as $dayOffQueried) {
                if (!in_array($dayOffQueried->id, $dayoffIds)) {
                    $dayoffs[] = $dayOffQueried;
                    $dayoffIds[] = $dayOffQueried->id;
                }
            }

            $result = !empty($dayoffs) ? $dayoffs : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function getHR()
    {
        $result = [];
        try {
            $positions = User::with('roles')->whereHas('roles', function ($query) {
                $query->where('name', 'hr-manager')->orWhere('name', 'hr-chief');
            })->get();
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
