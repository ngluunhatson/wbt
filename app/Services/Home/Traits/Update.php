<?php

namespace App\Services\Home\Traits;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Exception;
use Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

trait Update
{
    public function updateBasicInfo($input, $id, $request)
    {
        $result = ['status' => false];
        
        try {

            if (!$request->hasFile('file_disc')) {
                $input['disc'] = $request['disc_old'];
            }

            if (!$request->hasFile('file_motivator')) {
                $input['motivators'] = $request['motivator_old'];
            }

            if (!$request->hasFile('file_cv')) {
                $input['cv'] = $request['cv_old'];
            }


            if ($request->hasFile('file_cv') || $request->hasFile('file_disc') || $request->hasFile('file_motivator') || $request->hasFile('image_path')) {
                $lstFileSave = $this->saveFile($request, $id, 'update');
                $input = array_merge($input, $lstFileSave);
            }

            $query = $this->basicInfoRepository->updateBasicInfo($id, $input);

            if ($query) {
                $result['status'] = true;
                $result['message'] = "Sửa thành công";
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Sửa không thành công";
        }

        return $result;
    }

    public function updatePersonalInfo($input, $basic_info_id)
    {
        $result = ['status' => false];

        // dd($input);

        try {

            $query = $this->personalInfoRepository->updatePersonalInfo($basic_info_id, $input);

            if ($query) {
                $result['status'] = true;
                $result['message'] = "Sửa thành công";
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Sửa không thành công";
        }

        return $result;
    }

    public function updateRelativeInfo($input)
    {
        $result = ['status' => false];
   

        try {

            $query = $this->relaviteInfoRepository->updateRelativeInfo($input);

            if ($query) {
                $result['status'] = true;
                $result['message'] = "Sửa thành công";
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Sửa không thành công";
        }

        return $result;
    }

    public function updateLaborContract($input)
    {
        $result = ['status' => false];

        try {

            $query = $this->laborContractRepository->updateLaborContract($input);

            if ($query) {
                $result['status'] = true;
                $result['message'] = "Sửa thành công";
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Sửa không thành công";
        }

        return $result;
    }

    public function updateInfo($request, $id) {
        $input = $request -> all();
        // dd($input);
    }

    public function notifyToAdmin($id) {
        $allAdmin = (User::with('roles')->whereHas('roles', function($query) {
            $query->where('name', 'super-admin');
        })->get());

        foreach($allAdmin as $user) {
            $message = 'Thông báo !!' . auth()->user()->name . ' đã thay đổi thông tin';
            $link = route('personal.view', $id);
            $this->sendNotification($user, $id, $link, $message);
        }
    }
}