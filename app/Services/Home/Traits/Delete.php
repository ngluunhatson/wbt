<?php

namespace App\Services\Home\Traits;

use Illuminate\Support\Facades\Validator;
use Exception;
use Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

trait Delete
{
    public function deleteBasicInfo($id)
    {
        // dd($id);
        try {
            $query = $this->basicInfoRepository->deleteBasicInfo($id);
            if ($query) {
                $result['status'] = true;
                $result['message'] = "Xoá thành công ";
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Xoá không thành công ";
        }

        return $result;
    }

    public function deletePersonalInfo($basic_info_id)
    {
        // dd($id);
        try {
            $query = $this->personalInfoRepository->deletePersonalInfo($basic_info_id);
            if ($query) {
                $result['status'] = true;
                $result['message'] = "Xoá thành công ";
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Xoá không thành công ";
        }

        return $result;
    }

    public function deleteRelativeInfo($basic_info_id)
    {
        // dd($id);
        try {
            $query = $this->relaviteInfoRepository->deleteRelativeInfo($basic_info_id);
            if ($query) {
                $result['status'] = true;
                $result['message'] = "Xoá thành công ";
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Xoá không thành công ";
        }

        return $result;
    }

    public function deleteLaborContract($basic_info_id)
    {
        // dd($id);
        try {
            $query = $this->laborContractRepository->deleteLaborContract($basic_info_id);
            if ($query) {
                $result['status'] = true;
                $result['message'] = "Xoá thành công ";
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Xoá không thành công ";
        }

        return $result;
    }
    
}

