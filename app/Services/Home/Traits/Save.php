<?php

namespace App\Services\Home\Traits;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Exception;
use Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

trait Save
{
    public function savePersonalInfo($request, $lastBasicInfo)
    {
        $result = ['status' => false];
        $input = $request->all();
        $input['basic_info_id'] = $lastBasicInfo;
        try {
            $query = $this->personalInfoRepository->savePersonalInfo($input);
            if (!empty($query)) {
                $result['status'] = true;
                $result['message'] = "Tạo dữ liệu thành công";
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Tạo dữ liệu không thành công";
        }
        return $result;
    }

    public function saveRelativeInfo($request, $lastBasicInfo)
    {
        $result = ['status' => false];
        $input = $request->all();
        $input['basic_info_id'] = $lastBasicInfo;
        try {
            $query = $this->relaviteInfoRepository->saveRelativeInfo($input);
            if (!empty($query)) {
                $result['status'] = true;
                $result['message'] = "Tạo dữ liệu thành công";
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Tạo dữ liệu không thành công";
        }
        return $result;
    }

    public function saveLaborContract($request, $lastBasicInfo)
    {
        $result = ['status' => false];
        $input = $request->all();
        $input['basic_info_id'] = $lastBasicInfo;
        try {
            $query = $this->laborContractRepository->saveLaborContract($input);
            if (!empty($query)) {
                $result['status'] = true;
                $result['message'] = "Tạo dữ liệu thành công";
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Tạo dữ liệu không thành công";
        }
        return $result;
    }

    public function saveBasicInfo($request)
    {
        $result = ['status' => false];
        $input = $request->all();

        try {
            $query = $this->basicInfoRepository->saveBasicInfo($input);
            $lstFileSave = $this->saveFile($request, $query->id, 'insert');
            $query->update($lstFileSave);
            if (!empty($query)) {

                $this->savePersonalInfo($request, $query->id);
                $this->saveRelativeInfo($request, $query->id);
                $this->saveLaborContract($request, $query->id);
                
                $user = User::create([
                    'name' => $query->full_name,
                    'email' => $query->company_email,
                    'password' => Hash::make($query->phone),
                    'basic_info_id' => $query->id,
                ]);

                $result['status'] = true;
                $result['message'] = "Tạo dữ liệu thành công";
                $result['basic_info_id'] = $query->id;
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
            $result['message'] = "Tạo dữ liệu không thành công";
        }

        return $result;
    }

    public function saveFile($request, $id, $type)
    {
        $result = [];

        if ($request->hasFile('image_path')) {

            // Get image file
            $image_path = $request->file('image_path');

            // Folder path
            $folder = 'uploads/img/staff/' . $id . '/';

            // Make image name
            $category_image_name = time() . '-' . $image_path->getClientOriginalName();

            // Original size upload file
            $image_path->move($folder, $category_image_name);
            
            // Set input
            $result['avatar'] = $folder . $category_image_name;
        } else {
            // Set input
            if ($type == 'insert') {
                $result['avatar'] = '/uploads/common/user.jpg';
            }
        }

        if ($request->hasFile('file_disc')) {

            // Get image file
            $image_path = $request->file('file_disc');

            // Folder path
            $folder = 'uploads/img/staff/' . $id . '/disc/';

            // Make image name
            $category_image_name = time() . '-' . $image_path->getClientOriginalName();

            // Original size upload file
            $image_path->move($folder, $category_image_name);

            // Set input
            $result['disc'] = $folder . $category_image_name;
        }

        if ($request->hasFile('file_motivator')) {

            // Get image file
            $image_path = $request->file('file_motivator');

            // Folder path
            $folder = 'uploads/img/staff/' . $id . '/motivator/';

            // Make image name
            $category_image_name = time() . '-' . $image_path->getClientOriginalName();

            // Original size upload file
            $image_path->move($folder, $category_image_name);

            // Set input
            $result['motivators'] = $folder . $category_image_name;
        }


        if ( $request->hasFile('file_cv') ) {

            // Get image file
            $image_path = $request->file('file_cv');

            // Folder path
            $folder = 'uploads/img/staff/' . $id . '/cv/';

            // Make image name
            $category_image_name = time() . '-' . $image_path->getClientOriginalName();

            // Original size upload file
            $image_path->move($folder, $category_image_name);

            // Set input
            $result['cv'] = $folder . $category_image_name;
        }


        return $result;
    }
}
