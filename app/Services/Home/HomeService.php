<?php
namespace App\Services\Home;

use App\Repositories\BasicInfo\BasicInfoRepository;
use App\Repositories\PersonalInfo\PersonalInfoRepository;
use App\Repositories\RelativeInfo\RelativeInfoRepository;
use App\Repositories\LaborContract\LaborContractRepository;
use App\Repositories\Room\RoomRepository;
use App\Services\BaseService;
use App\Services\Home\Traits\Search;
use App\Services\Home\Traits\Save;
use App\Services\Home\Traits\Update;
use App\Services\Home\Traits\Delete;
use Exception;
use Illuminate\Support\Facades\Log;

class HomeService extends BaseService
{
    use  Save, Update, Delete;
    public $basicInfoRepository;
    public $personalInfoRepository;
    public $relaviteInfoRepository;
    public $laborContractRepository, $roomRepository;


    public function __construct(BasicInfoRepository $basicInfoRepository, PersonalInfoRepository $personalInfoRepository,
        RelativeInfoRepository $relaviteInfoRepository,LaborContractRepository  $laborContractRepository, RoomRepository $roomRepository)
    {
        $this->basicInfoRepository = $basicInfoRepository;
        $this->personalInfoRepository = $personalInfoRepository;
        $this->relaviteInfoRepository = $relaviteInfoRepository;
        $this->laborContractRepository = $laborContractRepository;
        $this->roomRepository = $roomRepository;
    }

    public function getRooms()
    {
        $result = [];
        try {
            $rooms = $this->roomRepository->findAll();
            $result = !empty($rooms) ? $rooms : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }      

    public function saveFile($request, $id, $type)
    {
        $result = [];
        if ($request->hasFile('image_path')) {
            // Get image file
            $image_path = $request->file('image_path');
            // Folder path
            $folder = 'uploads/img/staff/' . $id . '/';
            // Make image name
            $category_image_name = time() . '-' . $image_path->getClientOriginalName();
            // Original size upload file
            $image_path->move($folder, $category_image_name);
            
            // Set input
            $result['avatar'] = $folder . $category_image_name;
        } else {
            // Set input
            if ($type == 'insert') {
                $result['avatar'] = 'uploads/common/user.jpg';
            }
        }
        if ($request->hasFile('file_disc')) {
            // Get image file
            $image_path = $request->file('file_disc');
            // Folder path
            $folder = 'uploads/img/staff/' . $id . '/disc/';
            // Make image name
            $category_image_name = time() . '-' . $image_path->getClientOriginalName();
            // Original size upload file
            $image_path->move($folder, $category_image_name);
            // Set input
            $result['disc'] = $folder . $category_image_name;
        }
        if ($request->hasFile('file_motivator')) {
            // Get image file
            $image_path = $request->file('file_motivator');
            // Folder path
            $folder = 'uploads/img/staff/' . $id . '/motivator/';
            // Make image name
            $category_image_name = time() . '-' . $image_path->getClientOriginalName();
            // Original size upload file
            $image_path->move($folder, $category_image_name);
            // Set input
            $result['motivators'] = $folder . $category_image_name;
        }

        if ( $request->hasFile('file_cv') ) {

            // Get image file
            $image_path = $request->file('file_cv');

            // Folder path
            $folder = 'uploads/img/staff/' . $id . '/cv/';

            // Make image name
            $category_image_name = time() . '-' . $image_path->getClientOriginalName();

            // Original size upload file
            $image_path->move($folder, $category_image_name);

            // Set input
            $result['cv'] = $folder . $category_image_name;
        }

        return $result;
    }
}