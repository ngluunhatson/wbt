<?php

namespace App\Services\Description\Traits;

use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{
    public function getDataList()
    {
        $result = [];
        try {
            $data = $this->criteriaRepository->loadRelation(['room', 'team', 'position','user'])->where('delete_flag', 0);
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getRooms()
    {
        $result = [];
        try {
            $rooms = $this->organizationalRepository->getRooms();
            $result = !empty($rooms) ? $rooms : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getTeamsByRoomID($room_id)
    {
        $result = [];
        try {
            $teams = $this->organizationalRepository->getTeamsByRoom($room_id);
            $result = !empty($teams) ? $teams : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getStaffsByTeam($room_id, $team_id, $position_id)
    {
        $result = [];
        try {
            $positions = $this->organizationalRepository->getStaffByConditions($room_id, $team_id, $position_id);
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataByID($id)
    {
        $result = [];
        try {
            $criteria = $this->criteriaRepository->loadRelation(['room', 'team', 'position','user', 'confirms', 'confirms.confirm', 'confirms.confirm.staff'])->where('delete_flag', 0)
            ->where('status', 4)->find($id);
            $result = !empty($criteria) ? $criteria : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataListByRoom($roomID)
    {
        $result = [];
        try {
            $data = $this->criteriaRepository->loadRelation(['room', 'team', 'position', 'user', 'confirms.confirmReceive'])->where('room_id', $roomID)->where('delete_flag', 0);
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataMyWork($user_id)
    {
        $result = [];
        try {
            $criteria = $this->criteriaConfirmRepository->loadRelation([
                'confirmReceive', 'criteria', 'criteria.room', 'criteria.team',
                'criteria.position', 'criteria.user', 'criteria.confirms', 'criteria.confirms.confirm',  'criteria.confirms.confirm.staff'
            ])
                ->where('criteria.status', 4)
                ->where('confirmReceive.basic_info_id', $user_id)->first();
            $result = !empty($criteria) ? $criteria->toArray() : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

}