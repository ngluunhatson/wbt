<?php

namespace App\Services\Description;

use App\Repositories\Confirm\ConfirmRepository;
use App\Repositories\Criteria\CriteriaRepository;
use App\Repositories\CriteriaConfirm\CriteriaConfirmRepository;
use App\Repositories\Description\DescriptionRepository;
use App\Repositories\Dayoff\DayoffRepository;
use App\Repositories\Organizational\OrganizationalRepository;
use App\Repositories\Position\PositionRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\Team\TeamRepository;
use App\Services\BaseService;
// use App\Services\Description\Traits\AddEdit;
// use App\Services\Description\Traits\Delete;
use App\Services\Description\Traits\Search;

class DescriptionService extends BaseService
{
    use Search;
    public $organizationalRepository, $criteriaRepository;

    public function __construct(
        OrganizationalRepository $organizationalRepository,
        CriteriaRepository $criteriaRepository,
        ConfirmRepository $confirmRepository,
        CriteriaConfirmRepository $criteriaConfirmRepository,
    ) {
        $this->organizationalRepository = $organizationalRepository;
        $this->criteriaRepository = $criteriaRepository;
        $this->confirmRepository = $confirmRepository;
        $this->criteriaConfirmRepository = $criteriaConfirmRepository;
    }
}