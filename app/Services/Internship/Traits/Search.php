<?php

namespace App\Services\Internship\Traits;

use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{

    public function getAllInternships()
    { {
            $result = [];

            try {
                $internships = $this->internshipRepository
                    ->loadRelation(['user', 'intern', 'confirms', 'directBoss', 'roomManager', 'hr', 'ceo', 'internshipDetails'])
                    ->where('delete_flag', 0);

                if (!empty($internships))
                    $result = $internships;
            } catch (Exception $e) {
                Log::channel('daily')->error($e->getMessage());
            }

            return $result;
        }
    }

    public function getInternshipById($id)
    {
        $result = null;

        try {
            $internship = $this->internshipRepository
                ->loadRelation(['user', 'intern', 'confirms', 'directBoss', 'roomManager', 'hr', 'ceo', 'internshipDetails'])
                ->where('id', $id)->first();
            if ($internship)
                $result = $internship;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
