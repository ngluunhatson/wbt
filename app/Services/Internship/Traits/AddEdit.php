<?php

namespace App\Services\Internship\Traits;

use App\Models\Confirm;
use App\Models\Room;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait AddEdit
{
    public function saveData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {

            $input_all = $request->except('_token');
            $creatorConfirmFlag = array_key_exists("assign", $input_all);
            $confirm_id = [];
            $internship_detail_id = [];

            $internship_create_input = [
                "room_id"                    => $input_all['room_id'],
                "team_id"                    => $input_all['team_id'],
                "position_id"                => $input_all['position_id'],
                "basic_info_id"              => $input_all['basic_info_id'],
                "direct_boss_basic_info_id"  => $input_all['confirm_1_basic_info_id'],
                "room_manager_basic_info_id" => $input_all['confirm_2_basic_info_id'],
                "hr_basic_info_id"           => $input_all['confirm_3_basic_info_id'],
                "ceo_basic_info_id"          => $input_all['confirm_4_basic_info_id'],
                "start_date"                 => $input_all['start_date'],
                "end_date"                   => $input_all['end_date'],
                'internship_detail_id'       => [],
                "confirm_id"                 => [],
                'status'                     => 1,
                "created_by"                 => auth()->id(),
            ];

            $internship = $this->internshipRepository->create($internship_create_input);


            for ($part = 1; $part <= 2; $part++) {
                $internship_detail_input = [
                    'internship_id'         => $internship->id,
                    'part_number'           => $part,
                    'section_title_array'   => [],
                    'part_title'            => ($part == 1 ? "9 BƯỚC XÂY DỰNG DOANH NGHIỆP (9 STEPs)" : "SẢN PHẨM ACTIONCOACH CBD"),
                ];

                for ($i = 1; $i <= 29; $i++) {
                    $internship_detail_input["section_" . $i] = [];
                }

                $internshipDetail       = $this->internshipDetailRepository->create($internship_detail_input);
                $internship_detail_id[] = $internshipDetail->id;
            }

            for ($i = 1; $i <= 4; $i++) {
                if ($i != 2 || ($input_all['confirm_1_basic_info_id'] != $input_all['confirm_2_basic_info_id'])) {
                    $confirmX = new Confirm;
                    $confirmX->basic_info_id = $input_all['confirm_' . $i . '_basic_info_id'];
                    $confirmX->type_confirm = $input_all['type_confirm_' . $i];
                    $confirmX->assignment_type =  config('constants.assignment_type')[10];
                    $confirmX->status = 0;
                    $confirmX->created_by = auth()->id();

                    $confirmX->save();
                    $this->internshipConfirmRepository->create([
                        'internship_id'     =>  $internship->id,
                        'confirm_id'        =>  $confirmX->id
                    ]);
                    $confirm_id[] = $confirmX->id;
                }
            }

            if ($creatorConfirmFlag) {
                $internship->status = 2;
                $intern_user      = User::where('basic_info_id', $input_all['basic_info_id'])->first();
                $direct_boss_user = User::where('basic_info_id', $input_all['confirm_1_basic_info_id'])->first();
                $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                $link = route('internship.edit', $internship->id);
                $this->sendNotification($intern_user, $internship->id, $link, $message);
                $this->sendNotification($direct_boss_user, $internship->id, $link, $message);
            }

            $internship->confirm_id = $confirm_id;
            $internship->internship_detail_id = $internship_detail_id;

            $internship->save();


            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateData($request)
    {

        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $update_input = $request->except('_token');

            $internshipX = $this->getInternshipById($update_input['internship_id']);
            $internship_update_input = [];

            $test_admin_intern       = false;
            $test_admin_direct_boss  = false;
            $test_admin_room_manager = false;
            $test_admin_hr           = false;
            $test_admin_ceo          = false;

            $isUserIntern            = auth()->user()->basic_info_id == $internshipX->basic_info_id              || (auth()->user()->hasAnyRole(['super-admin']) && $test_admin_intern);
            $isUserDirectBoss        = auth()->user()->basic_info_id == $internshipX->direct_boss_basic_info_id  || (auth()->user()->hasAnyRole(['super-admin']) && $test_admin_direct_boss);
            $isUserRoomManager       = auth()->user()->basic_info_id == $internshipX->room_manager_basic_info_id || (auth()->user()->hasAnyRole(['super-admin']) && $test_admin_room_manager);
            $isUserHr                = auth()->user()->basic_info_id == $internshipX->hr_basic_info_id           || (auth()->user()->hasAnyRole(['super-admin']) && $test_admin_hr);
            $isUserCeo               = auth()->user()->basic_info_id == $internshipX->ceo_basic_info_id          || (auth()->user()->hasAnyRole(['super-admin']) && $test_admin_ceo);
            $sameDBandRM             = $internshipX->direct_boss_basic_info_id == $internshipX->room_manager_basic_info_id;


            $internAccount          = User::where('basic_info_id', $internshipX->basic_info_id)->first();
            $directBossAccount      = User::where('basic_info_id', $internshipX->direct_boss_basic_info_id)->first();
            $roomManagerAccount     = User::where('basic_info_id', $internshipX->room_manager_basic_info_id)->first();
            $hrAccount              = User::where('basic_info_id', $internshipX->hr_basic_info_id)->first();
            $ceoAccount             = User::where('basic_info_id', $internshipX->ceo_basic_info_id)->first();

            $noti_targets = [];
            $noti_message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
            $link         = route('internship.edit', $internshipX->id);

            switch ($internshipX->status) {
                case (2):
                case (3 && $isUserIntern):
                case (8):
                case (9):
                case (10):
                case (11):
                case (14):
                    if ($isUserIntern) {
                        $internConfirm = array_key_exists('intern_confirm', $update_input);

                        foreach ($internshipX->internshipDetails as $internshipX_Detail) {
                            $internshipX_Detail_ArrayForm = $internshipX_Detail->toArray();
                            $detail_update_input = [];

                            $buffer_1 = $internshipX_Detail_ArrayForm['part_number']  == 1 ? 0 : 29;
                            $buffer_2 = $internshipX_Detail_ArrayForm['part_number']  == 1 ? 0 : 12;
                            for ($sec_num = 1 + $buffer_1; $sec_num <= 29 + $buffer_2; $sec_num++) {
                                if (!in_array($sec_num, [4, 7, 11, 24, 26, 28, 31])) {
                                    $detail_section_update_input = [];
                                    $s_end = ($sec_num == 27 && $internshipX_Detail_ArrayForm['part_number']  == 1) ? 5 : 4;
                                    for ($s = 0; $s <= $s_end; $s++) {
                                        if (array_key_exists('section_' . $sec_num . '_' . $s, $update_input)) {
                                            $sec_temp_info = $update_input['section_' . $sec_num . '_' . $s];
                                            $sec_info = $sec_temp_info ? $sec_temp_info : "";
                                            if ($s == 3)
                                                $sec_info = intval($sec_info);
                                            $detail_section_update_input[]  = $sec_info;
                                        } else {
                                            $detail_section_update_input[] = array_key_exists($s, $internshipX_Detail_ArrayForm['section_' . $sec_num - $buffer_1])
                                                ? $internshipX_Detail_ArrayForm['section_' . $sec_num - $buffer_1][$s] : 0;
                                        }
                                    }
                                    $detail_update_input['section_' . $sec_num - $buffer_1] = $detail_section_update_input;
                                }
                            }

                            $detail_update_input['updated_by'] = auth()->id();
                            $this->internshipDetailRepository->update($internshipX_Detail_ArrayForm['id'], $detail_update_input);
                        }

                        $intern_score     = intval($update_input['intern_score']);
                        $intern_max_score = intval($update_input['intern_max_score']);

                        $internship_update_input = [
                            'status'                    => 2,
                            'intern_comment'            => $update_input['intern_comment'],
                            'intern_score'              => $intern_score,
                            'intern_confirm_date'       => null,
                            'updated_by'                => auth()->id(),

                        ];

                        if ($internConfirm) {
                            $internship_update_input = [
                                'status'                    => 3,
                                'intern_comment'            => $update_input['intern_comment'],
                                "intern_confirm_date"       => $update_input['intern_confirm_date'],
                                'intern_score'              => $intern_score,
                                'direct_boss_comment'       => null,
                                'direct_boss_decision_1'    => null,
                                'direct_boss_decision_2'    => null,
                                'direct_boss_decision_3'    => null,
                                'direct_boss_decision_4'    => null,
                                'direct_boss_review_date'   => null,
                                'room_manager_comment'      => null,
                                'room_manager_review_date'  => null,
                                'hr_comment'                => null,
                                'hr_decision_1'             => null,
                                'hr_decision_2'             => null,
                                'hr_decision_3'             => null,
                                'hr_decision_4'             => null,
                                'hr_decision_5'             => null,
                                'hr_review_date'            => null,
                                'ceo_review_date'           => null,
                                'updated_by'                => auth()->id(),

                            ];
                            if ($intern_max_score > 0)
                                $internship_update_input['intern_max_score'] = $intern_max_score;


                            foreach ($internshipX->confirms as $internshipConfirm) {
                                $confirm_update_input = [
                                    'status'     => 0,
                                    'note'       => null,
                                    'updated_by' => auth()->id(),
                                ];

                                $this->confirmRepository->update($internshipConfirm->confirm_id, $confirm_update_input);
                            }

                            $noti_targets[] = User::where('basic_info_id', $internshipX->direct_boss_basic_info_id)->first();
                        }
                    }

                    break;
                case (3):
                    if ($isUserDirectBoss) {

                        foreach ($internshipX->internshipDetails as $internshipX_Detail) {

                            $internshipX_Detail_ArrayForm = $internshipX_Detail->toArray();
                            $detail_update_input = [];

                            $buffer_1 = $internshipX_Detail->part_number  == 1 ? 0 : 29;
                            $buffer_2 = $internshipX_Detail->part_number  == 1 ? 0 : 12;

                            for ($sec_num = 1 + $buffer_1; $sec_num <= 29 + $buffer_2; $sec_num++) {
                                if (!in_array($sec_num, [4, 7, 11, 24, 26, 28, 31]) && array_key_exists('section_' . $sec_num . '_4', $update_input)) {
                                    $detail_section_update_input    = $internshipX_Detail_ArrayForm['section_' . $sec_num - $buffer_1];
                                    $detail_section_update_input[4] = intval($update_input['section_' . $sec_num . '_4']);
                                    $detail_update_input['section_' . $sec_num - $buffer_1] = $detail_section_update_input;
                                }
                            }

                            $detail_update_input['updated_by'] = auth()->id();
                            $this->internshipDetailRepository->update($internshipX_Detail->id, $detail_update_input);
                        }

                        $directBossConfirm          = intval($update_input['direct_boss_confirm']);
                        $internship_update_input = [
                            'direct_boss_score'     => intval($update_input['direct_boss_score']),
                            'updated_by'            => auth()->id(),
                            'direct_boss_comment'   => $update_input['direct_boss_comment'],
                        ];
                        $direct_boss_max_score = intval($update_input['direct_boss_max_score']);
                        if ($direct_boss_max_score > 0)
                            $internship_update_input['direct_boss_max_score'] = $direct_boss_max_score;

                        if (array_key_exists('direct_boss_decision_check', $update_input)) {
                            $direct_boss_decision_check = intval($update_input['direct_boss_decision_check']);
                            switch ($direct_boss_decision_check) {
                                case (1):
                                    $internship_update_input['direct_boss_decision_1'] = intval($update_input['direct_boss_decision_1']);
                                    break;
                                case (2):
                                    $internship_update_input['direct_boss_decision_2'] = floatval($update_input['direct_boss_decision_2']);
                                    break;
                                case (3):
                                    $internship_update_input['direct_boss_decision_3'] = $update_input['direct_boss_decision_3'];
                                    break;
                            }

                            for ($i = 1; $i <= 3; $i++) {
                                if ($i != $direct_boss_decision_check)
                                    $internship_update_input['direct_boss_decision_' . $i] = ($i == 1) ? 0 : null;
                            }
                        }


                        if (array_key_exists('direct_boss_decision_4_check', $update_input)) {
                            $internship_update_input['direct_boss_decision_4'] = $update_input['direct_boss_decision_4'];
                        }



                        if ($directBossConfirm == 2) {
                            $internship_update_input['status'] = 8;
                            $internship_update_input['direct_boss_review_date']  = null;
                            $noti_targets[] = $internAccount;
                            $noti_message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                        } else {
                            $internship_update_input['status'] = ($direct_boss_decision_check == 1) ? 12
                                : ($sameDBandRM ? 5 : 4);
                            $internship_update_input['direct_boss_review_date']  = $update_input['direct_boss_review_date'];
                            $noti_targets[]  = ($sameDBandRM) ? $hrAccount : $roomManagerAccount;
                        }

                        $this->confirmRepository->update($update_input['direct_boss_confirm_id'], [
                            'status'        => $directBossConfirm,
                            'note'          => $directBossConfirm == 2 ? $update_input['direct_boss_decline_reason'] : null,
                            'updated_by'    => auth()->id(),
                        ]);
                    }
                    break;

                case (4):
                    if ($isUserRoomManager) {
                        $roomManagerConfirm       = intval($update_input['room_manager_confirm']);

                        $internship_update_input  = [
                            'room_manager_comment'  => $update_input['room_manager_comment'],
                            'updated_by'            => auth()->id(),
                        ];

                        if ($roomManagerConfirm == 2) {
                            $internship_update_input['status'] = 9;
                            $internship_update_input['room_manager_review_date'] = null;
                            $noti_message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                            $noti_targets = [$internAccount, $directBossAccount];
                        } else {
                            $internship_update_input['status'] = 5;
                            $internship_update_input['room_manager_review_date'] = $update_input['room_manager_review_date'];
                            $noti_targets[] = $hrAccount;
                        }

                        $this->confirmRepository->update($update_input['room_manager_confirm_id'], [
                            'status'        => $roomManagerConfirm,
                            'note'          => $roomManagerConfirm == 2 ? $update_input['room_manager_decline_reason'] : null,
                            'updated_by'    => auth()->id(),
                        ]);
                    }
                    break;

                case (5):
                    if ($isUserHr) {
                        $hrConfirm              = intval($update_input['hr_confirm']);


                        $internship_update_input  = [
                            'hr_comment'        => $update_input['hr_comment'],
                            'updated_by'        => auth()->id(),
                        ];

                        foreach ([1, 2, 5] as $i)
                            if (array_key_exists('hr_decision_' . $i . '_check', $update_input)) {
                                if ($i != 5)
                                    $internship_update_input['hr_decision_' . $i] = floatval($update_input['hr_decision_' . $i]);
                                else
                                    $internship_update_input['hr_decision_' . $i] = $update_input['hr_decision_' . $i];
                            }

                        if (array_key_exists('hr_decision_hdld_check', $update_input)) {
                            $hr_decision_hdld_check = intval($update_input['hr_decision_hdld_check']);
                            switch ($hr_decision_hdld_check) {
                                case (3):
                                    $internship_update_input['hr_decision_3'] = floatval($update_input['hr_decision_3']);
                                    break;
                                case (4):
                                    $internship_update_input['hr_decision_4'] = $update_input['hr_decision_4'];
                                    break;
                            }
                        }

                        if ($hrConfirm == 2) {
                            $internship_update_input['status'] = 10;
                            $internship_update_input['hr_review_date'] = null;
                            $noti_message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                            $noti_targets = [$internAccount, $directBossAccount];
                            if (!$sameDBandRM)
                                $noti_targets[] = $roomManagerAccount;
                        } else {
                            $internship_update_input['status'] = 6;
                            $internship_update_input['hr_review_date'] = $update_input['hr_review_date'];
                            $noti_targets[] = $ceoAccount;
                        }

                        $this->confirmRepository->update($update_input['hr_confirm_id'], [
                            'status'        => $hrConfirm,
                            'note'          => $hrConfirm == 2 ? $update_input['hr_decline_reason'] : null,
                            'updated_by'    => auth()->id(),
                        ]);
                    }
                    break;
                case (6):
                    if ($isUserCeo) {
                        $ceoConfirm  = intval($update_input['ceo_confirm']);
                        if ($ceoConfirm == 2) {
                            $internship_update_input['status'] = 11;
                            $internship_update_input['hr_review_date'] = null;
                            $noti_message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                        } else {
                            $internship_update_input['status'] = 7;
                            $internship_update_input['ceo_review_date'] = $update_input['ceo_review_date'];
                        }

                        $this->confirmRepository->update($update_input['ceo_confirm_id'], [
                            'status'        => $ceoConfirm,
                            'note'          => $ceoConfirm == 2 ? $update_input['ceo_decline_reason'] : null,
                            'updated_by'    => auth()->id(),
                        ]);

                        $noti_targets = [$internAccount, $directBossAccount, $hrAccount];
                        if (!$sameDBandRM)
                            $noti_targets[] = $roomManagerAccount;
                        if ($internshipX->basic_info_id != auth()->user()->basic_info_id)
                            $noti_targets[] = auth()->user();
                    }
                    break;
                case (12):
                    if ($isUserHr) {
                        $hrConfirm    = intval($update_input['hr_confirm']);
                        $internship_update_input  = [
                            'hr_comment'        => $update_input['hr_comment'],
                            'updated_by'        => auth()->id(),
                        ];

                        if (array_key_exists('hr_decision_5_check', $update_input)) {
                            $internship_update_input['hr_decision_5'] = $update_input['hr_decision_5'];
                        }

                        if ($hrConfirm == 2) {
                            $internship_update_input['status'] = 14;
                            $internship_update_input['hr_review_date'] = null;
                            $noti_message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                        } else {
                            $internship_update_input['status'] = 13;
                            $internship_update_input['hr_review_date'] = $update_input['hr_review_date'];
                        }

                        $noti_targets = [$internAccount, $directBossAccount];
                        if (!$sameDBandRM)
                            $noti_targets[] = $roomManagerAccount;

                        $this->confirmRepository->update($update_input['hr_confirm_id'], [
                            'status'        => $hrConfirm,
                            'note'          => $hrConfirm == 2 ? $update_input['hr_decline_reason'] : null,
                            'updated_by'    => auth()->id(),
                        ]);
                    }
                    break;
            }

            foreach ($noti_targets as $noti_target)
                $this->sendNotification($noti_target, $internshipX->id, $link, $noti_message);

            $this->internshipRepository->update($internshipX->id, $internship_update_input);

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
