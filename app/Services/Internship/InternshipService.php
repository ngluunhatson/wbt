<?php

namespace App\Services\Internship;

use App\Repositories\Confirm\ConfirmRepository;
use App\Repositories\Internship\InternshipConfirmRepository;
use App\Repositories\Internship\InternshipDetailRepository;
use App\Repositories\Internship\InternshipRepository;
use App\Services\BaseService;
use App\Services\Internship\Traits\AddEdit;
use App\Services\Internship\Traits\Delete;
use App\Services\Internship\Traits\Search;

class InternshipService extends BaseService
{
    use Search, AddEdit, Delete;
    public $internshipRepository, $internshipConfirmRepository, $internshipDetailRepository, $confirmRepository;

    public function __construct(
        InternshipRepository $internshipRepository, 
        InternshipConfirmRepository $internshipConfirmRepository,
        InternshipDetailRepository $internshipDetailRepository,
        ConfirmRepository $confirmRepository
    ) {
        $this -> internshipRepository        = $internshipRepository;
        $this -> internshipConfirmRepository = $internshipConfirmRepository;
        $this -> internshipDetailRepository  = $internshipDetailRepository;
        $this -> confirmRepository           = $confirmRepository;
    }
}
