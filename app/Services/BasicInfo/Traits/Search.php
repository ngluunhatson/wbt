<?php

namespace App\Services\BasicInfo\Traits;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{

    public function getByID($id)
    {
        $result = [];
        try {
            $basicInfo = $this->basicInfoRepository->getByID($id);
            $result = !empty($basicInfo) ? $basicInfo : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataByID($id)
    {
        $result = [];
        try {
            $basicInfo = $this->basicInfoRepository->loadRelation(['room', 'team', 'position','user', 'confirms', 'confirms.confirm'])->find($id);
            $result = !empty($basicInfo) ? $basicInfo : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getTeamsByRoomID($room_id)
    {
        $result = [];
        try {
            $teams = $this->organizationalRepository->getTeamsByRoom($room_id);
            $result = !empty($teams) ? $teams : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getStaffsByTeam($room_id, $team_id, $position_id)
    {
        $result = [];
        try {
            $positions = $this->organizationalRepository->getStaffByConditions($room_id, $team_id, $position_id);
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
