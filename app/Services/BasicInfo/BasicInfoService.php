<?php

namespace App\Services\BasicInfo;

use App\Repositories\Confirm\ConfirmRepository;
use App\Repositories\BasicInfo\BasicInfoRepository;
use App\Repositories\Criteria\CriteriaRepository;
use App\Repositories\CriteriaConfirm\CriteriaConfirmRepository;
use App\Repositories\Dayoff\DayoffRepository;
use App\Repositories\Organizational\OrganizationalRepository;
use App\Repositories\Position\PositionRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\Team\TeamRepository;
use App\Services\BaseService;
use App\Services\BasicInfo\Traits\AddEdit;
use App\Services\BasicInfo\Traits\Delete;
use App\Services\BasicInfo\Traits\Search;

class BasicInfoService extends BaseService
{
    use Search;
    public $organizationalRepository;

    public function __construct(
        OrganizationalRepository $organizationalRepository,
        BasicInfoRepository $basicInfoRepository,
        ConfirmRepository $confirmRepository,
        CriteriaConfirmRepository $criteriaConfirmRepository,
        CriteriaRepository $criteriaRepository,
    ) {
        $this->organizationalRepository = $organizationalRepository;
        $this->basicInfoRepository = $basicInfoRepository;
        $this->confirmRepository = $confirmRepository;
        $this->criteriaConfirmRepository = $criteriaConfirmRepository;
        $this->criteriaRepository = $criteriaRepository;
    }
}