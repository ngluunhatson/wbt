<?php

namespace App\Services\Criteria\Traits;

use App\Events\NotificationEvent;
use App\Models\User;
use App\Notifications\AssignPerson;
use Arr;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Request;
use Illuminate\Support\Facades\Validator;

trait AddEdit
{
    public function saveData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $dataCrieria = $request->except('_token', 'selectConfirm', 'assign', 'type_confirm_1', 'type_confirm_2', 'type_confirm_3');
            $dataConfirm = $request['selectConfirm'];

            $dataCrieria['created_by'] = auth()->id();
            $dataCrieria['status'] = 1;
            $criteria = $this->criteriaRepository->create($dataCrieria);
            foreach ($dataConfirm as $key => $confimer) {
                $confim = $this->confirmRepository->create([
                    'basic_info_id' => $confimer ?? null,
                    'type_confirm' => $request['type_confirm_' . ($key + 1)],
                    'assignment_type' => config('constants.assignment_type')[1],
                    'status' => 0,
                    'created_by' => auth()->id()
                ]);

                $this->criteriaConfirmRepository->create([
                    'criteria_id' => $criteria->id,
                    'confirm_id' => $confim->id
                ]);
                if (array_key_exists('assign', $request->all()) && $request['assign']) {
                    if ($confimer != null && $request['type_confirm_' . ($key + 1)] == 2) {
                        $user = User::where('basic_info_id', $confimer)->first();
                        $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                        $link = route('criteria.view', $criteria->id);
                        $this->sendNotification($user, $criteria->id, $link, $message);
                    }
                    $criteria->status = 2;
                }
                $confirmID[] = $confim->id;
            }

            $criteria->confirm_id = $confirmID;
            $criteria->save();
            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhật không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $dataCrieria = $request->except(
                '_token',
                'assign',
                'selectConfirm',
                'criteria_id',
                'confirm_id_1',
                'confirm_id_2',
                'confirm_id_3',
                'type_confirm_1',
                'type_confirm_2',
                'type_confirm_3',
                'reason'
            );
            $dataConfirm = $request['selectConfirm'];
            $dataCrieria['updated_by'] = auth()->id();
            $criteriaOld = $this->criteriaRepository->getByID($request['criteria_id']);
            if ($criteriaOld['status'] == 5 || $criteriaOld['status'] == 6 || (array_key_exists('assign', $request->all()) && $request['assign'])) {
                $dataCrieria['status'] = 2;
            }
            $criteria = $this->criteriaRepository->update($request['criteria_id'], $dataCrieria);

            for ($i = 1; $i < 4; $i++) {
                $this->confirmRepository->update($request['confirm_id_' . $i], [
                    'basic_info_id' => $dataConfirm[$i - 1],
                    'status' => 0,
                    'note' => null,
                    'assignment_type' => config('constants.assignment_type')[1],
                    'updated_by' => auth()->id()
                ]);

                if ($dataConfirm[$i - 1] != null && $request['type_confirm_' . $i] == 2 && ($criteriaOld['status'] == 5 || $criteriaOld['status'] == 6 || (array_key_exists('assign', $request->all()) && $request['assign']))) {
                    $user = User::where('basic_info_id', $dataConfirm[$i - 1])->first();
                    $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                    $link = route('criteria.view', $request['criteria_id']);
                    $this->sendNotification($user, $request['criteria_id'], $link, $message);
                }
            }

            $result['status'] = true;
            $result['message'] = 'Cập nhật thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateStatus($request, $id)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhật không thành công !!',
        ];
        DB::beginTransaction();
        try {
            if ($request['statusConfirm'] == 1) {
                switch ($request['type']) {
                    case 2:
                        $this->criteriaRepository->update($id, [
                            'status' => 3,
                            'updated_by' => auth()->id()
                        ]);
                        if (!empty($request['confimerIDNext'])) {
                            $user = User::where('basic_info_id', $request['confimerIDNext'])->first();
                            $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                            $link = route('criteria.view', $id);
                            $this->sendNotification($user, $id, $link, $message);
                        }
                        break;
                    case 3:
                        $this->criteriaRepository->update($id, [
                            'status' => 4,
                            'review_date' => Carbon::now()->format('Y-m-d'),
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->criteriaRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        $message = 'Thông báo !! Yêu cầu đã được duyệt bởi ' . auth()->user()->name;
                        if ($data->user) {
                            $link = route('criteria.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        foreach ($data->confirms as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirmID']) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                $link = route('criteria.view', $id);
                                $this->sendNotification($user, $id, $link, $message);
                            }
                        }
                        break;
                }
            } else {
                switch ($request['type']) {
                    case 2:
                        $this->criteriaRepository->update($id, [
                            'status' => 5,
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->criteriaRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        if ($data->user) {
                            $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                            $link = route('criteria.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        break;
                    case 3:
                        $this->criteriaRepository->update($id, [
                            'status' => 6,
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->criteriaRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                        if ($data->user) {
                            $link = route('criteria.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        foreach ($data->confirms as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirmID']) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                $link = route('criteria.view', $id);
                                $this->sendNotification($user, $id, $link, $message);
                            }
                        }
                        break;
                }
            }

            $this->confirmRepository->update($request['confirmID'], [
                'status' => $request['statusConfirm'],
                'note' => $request['reason'],
                'updated_by' => auth()->id()
            ]);

            $result['status'] = true;
            $result['message'] = 'Cập nhật thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
