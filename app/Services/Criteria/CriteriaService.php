<?php

namespace App\Services\Criteria;

use App\Repositories\Confirm\ConfirmRepository;
use App\Repositories\Criteria\CriteriaRepository;
use App\Repositories\CriteriaConfirm\CriteriaConfirmRepository;
use App\Repositories\Organizational\OrganizationalRepository;
use App\Repositories\Room\RoomRepository;
use App\Services\BaseService;
use App\Services\Criteria\Traits\AddEdit;
use App\Services\Criteria\Traits\Delete;
use App\Services\Criteria\Traits\Search;

class CriteriaService extends BaseService
{
    use Search, AddEdit, Delete;
    public $organizationalRepository;

    public function __construct(
        OrganizationalRepository $organizationalRepository,
        CriteriaRepository $criteriaRepository,
        ConfirmRepository $confirmRepository,
        RoomRepository $roomRepository,
        CriteriaConfirmRepository $criteriaConfirmRepository,
    ) {
        $this->organizationalRepository = $organizationalRepository;
        $this->criteriaRepository = $criteriaRepository;
        $this->confirmRepository = $confirmRepository;
        $this->roomRepository = $roomRepository;
        $this->criteriaConfirmRepository = $criteriaConfirmRepository;
    }
}
