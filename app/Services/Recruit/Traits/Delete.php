<?php

namespace App\Services\Recruit\Traits;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait Delete
{
    public function delete($id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa không thành công !!',
        ]; 

        DB::beginTransaction();
        try {
            $query = $this->recruitRepository->delete($id);
            if ($query)
            {
                $result['status'] = true;
                $result['message'] = 'Xóa thành công !!';
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function deleteInterview($id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa không thành công !!',
        ]; 

        DB::beginTransaction();
        try {
            $query = $this->interviewReviewRepository->delete($id);
            if ($query)
            {
                $result['status'] = true;
                $result['message'] = 'Xóa thành công !!';
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
