<?php

namespace App\Services\Recruit\Traits;

use App\Models\User;
use Arr;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Request;
use Illuminate\Support\Facades\Validator;

trait AddEdit
{
    public function saveData($request)
    {

        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $dataRecruit = $request->except('_token', 'selectConfirm', 'assign', 'type_confirm_1', 'type_confirm_2', 'type_confirm_3');
            $dataConfirm = $request['selectConfirm'];

            $dataRecruit['created_by'] = auth()->id();
            $dataRecruit['status'] = 1;


            $recruit = $this->recruitRepository->create($dataRecruit);
            $noRoomManagerFlag = false;
            if ($dataConfirm) {
                foreach ($dataConfirm as $key => $confimer) {

                    $confim = $this->confirmRepository->create([
                        'basic_info_id' => $confimer ?? null,
                        'type_confirm' => $request['type_confirm_' . ($key + 1)],
                        'assignment_type' => config('constants.assignment_type')[3],
                        'status' => 0,
                        'created_by' => auth()->id()
                    ]);

                    $this->recruitConfirmRepository->create([
                        'recruit_id' => $recruit->id,
                        'confirm_id' => $confim->id
                    ]);
                    $confirmID[] = $confim->id;

                    if ($request['type_confirm_' . ($key + 1)] == 4 && $confimer == null) {
                        $recruit->status = 9;
                        $noRoomManagerFlag = true;
                    }
                    if (array_key_exists('assign', $request->all()) && $request['assign']) {
                        if (!$noRoomManagerFlag) {
                            $recruit->status = 2;
                        }
                        if ($request['type_confirm_' . ($key + 1)] == 2 || (!$noRoomManagerFlag)) {
                            $user = User::where('basic_info_id', $confimer)->first();
                            $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                            $link = route('recruitment-proposal.view', $recruit->id);
                            $this->sendNotification($user, $recruit->id, $link, $message);
                        }
                    }
                }
                $recruit->confirm_id = $confirmID;
                $recruit->save();
            }

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhật không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $dataRecruit = $request->except('_token', 'selectConfirm', 'assign', 'recruit_id', 'confirm_id_1', 'confirm_id_2', 'confirm_id_3');
            $dataConfirm = $request['selectConfirm'];

            $dataRecruit['updated_by'] = auth()->id();
            $recruitOld = $this->recruitRepository->getByID($request['recruit_id']);
            // status cancel in file constants in recruitment-proposal 
            if (in_array($recruitOld['status'], [6, 7, 8]) || (array_key_exists('assign', $request->all()) && $request['assign']) || ($recruitOld['status'] == 9 && $dataConfirm[0] != null)) {
                $dataRecruit['status'] = 2;
                if ($request['type_confirm_1'] == 4 && $dataConfirm[0]  == null) {
                    $dataRecruit['status'] = 9;
                }
            }
            $recruit = $this->recruitRepository->update($request['recruit_id'], $dataRecruit);


            for ($i = 1; $i < 3; $i++) {
                $this->confirmRepository->update($request['confirm_id_' . $i], [
                    'basic_info_id' => $dataConfirm[$i - 1] ?? null,
                    'status' => 0,
                    'assignment_type' => config('constants.assignment_type')[3],
                    'updated_by' => auth()->id()
                ]);

                if (
                    (($dataConfirm[$i - 1] != null && $request['type_confirm_' . $i] == 4) || $request['type_confirm_' . $i] == 2) &&
                    (in_array($recruitOld['status'], [6, 7, 8]) || (array_key_exists('assign', $request->all()) && $request['assign']))
                ) {
                    $user = User::where('basic_info_id', $dataConfirm[$i - 1])->first();
                    $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                    $link = route('recruitment-proposal.view', $request['recruit_id']);
                    $this->sendNotification($user, $request['recruit_id'], $link, $message);
                }
            }

            $result['status'] = true;
            $result['message'] = 'Cập nhật thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateStatus($request, $id)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhật không thành công !!',
        ];
        DB::beginTransaction();
        try {
            if ($request['statusConfirm'] == 1) {
                switch ($request['type']) {
                        // HR confirm
                    case 2:
                        $this->recruitRepository->update($id, [
                            'status' => 4,
                            'updated_by' => auth()->id()
                        ]);
                        if (!empty($request['confimerIDNext'])) {
                            $user = User::where('basic_info_id', $request['confimerIDNext'])->first();
                            $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                            $link = route('recruitment-proposal.view', $id);
                            $this->sendNotification($user, $id, $link, $message);
                        }
                        break;
                        // CEO Manger confirm
                    case 3:
                        $this->recruitRepository->update($id, [
                            'status' => 5,
                            'review_date' => Carbon::now()->format('Y-m-d'),
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->recruitRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        $message = 'Thông báo !! Yêu cầu đã được duyệt bởi ' . auth()->user()->name;
                        if ($data->user) {
                            $link = route('recruitment-proposal.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        foreach ($data->confirms as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirmID']) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                $link = route('recruitment-proposal.view', $id);
                                $this->sendNotification($user, $id, $link, $message);
                            }
                        }
                        break;
                        // Manager in room confirm
                    case 4:
                        $this->recruitRepository->update($id, [
                            'status' => 3,
                            'updated_by' => auth()->id()
                        ]);
                        if (!empty($request['confimerIDNext'])) {
                            $user = User::where('basic_info_id', $request['confimerIDNext'])->first();
                            $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                            $link = route('recruitment-proposal.view', $id);
                            $this->sendNotification($user, $id, $link, $message);
                        }
                        break;
                }
            } else {
                switch ($request['type']) {
                    case 2:
                        $this->recruitRepository->update($id, [
                            'status' => 7,
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->recruitRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        if ($data->user) {
                            $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                            $link = route('recruitment-proposal.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        $data = $this->recruitRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                        if ($data->user) {
                            $link = route('recruitment-proposal.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        foreach ($data->confirms as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirmID'] && $value->confirm->type_confirm != 3) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                $link = route('recruitment-proposal.view', $id);
                                $this->sendNotification($user, $id, $link, $message);
                            }
                        }
                        break;
                    case 3:
                        $this->recruitRepository->update($id, [
                            'status' => 8,
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->recruitRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                        if ($data->user) {
                            $link = route('recruitment-proposal.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        foreach ($data->confirms as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirmID']) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                $link = route('recruitment-proposal.view', $id);
                                $this->sendNotification($user, $id, $link, $message);
                            }
                        }
                        break;
                        // Manager in room confirm
                    case 4:
                        $this->recruitRepository->update($id, [
                            'status' => 6,
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->recruitRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        if ($data->user) {
                            $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                            $link = route('recruitment-proposal.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        break;
                }
            }

            $this->confirmRepository->update($request['confirmID'], [
                'status' => $request['statusConfirm'],
                'note' => $request['reason'],
                'updated_by' => auth()->id()
            ]);

            $result['status'] = true;
            $result['message'] = 'Cập nhật thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function saveFormInterview($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $dataFormInsert = $request->except('_token', 'selectConfirm', 'type_confirm_1', 'type_confirm_2', 'type_confirm_3');
            $dataAssign = $request['selectConfirm'];
            $dataFormInsert['created_by'] = auth()->id();
            $dataFormInsert['status'] = 1;
            $interviewForm = $this->interviewReviewRepository->create($dataFormInsert);

            foreach ($dataAssign as $key => $confimer) {
                $confim = $this->confirmRepository->create([
                    'basic_info_id' => $confimer ?? null,
                    'type_confirm' => $request['type_confirm_' . ($key + 1)],
                    'assignment_type' => config('constants.assignment_type')[5],
                    'status' => 0,
                    'created_by' => auth()->id()
                ]);

                $interviewDetail = $this->interviewScoreRepository->create([
                    'confirm_id' => $confim->id,
                    'interview_review_id' => $interviewForm->id,
                    'created_by' => auth()->id()
                ]);
                if ($request['type_confirm_' . ($key + 1)] == 2) {
                    $user = User::where('basic_info_id', $confimer)->first();
                    $message = 'Thông báo !! Có một lời đề nghị đánh giá phỏng vấn từ ' . auth()->user()->name;
                    $link = route('result.interview', $interviewForm->id);
                    $this->sendNotification($user, $interviewForm->id, $link, $message);
                }
            }
            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateInterview($request) {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $dataFormUpdate = $request->except('_token', 'selectConfirm', 'interview_id', 'type_confirm_1', 'type_confirm_2', 'type_confirm_3');
            $dataFormUpdate['updated_by'] = auth()->id();
            $interviewForm = $this->interviewReviewRepository->update($request['interview_id'], $dataFormUpdate);
            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function interviewCandidate($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $dataDetail = $request->except(
                '_token',
                'detail_interview_id',
                'confirm_id',
                'status',
                'disc',
                'salary',
                'interview_id',
                'type_confirm',
                'specialize',
                'confimerIDNext',
                'department_resource_notes',
            );
            if ($request['status'] == 1) {
                switch ($request['type_confirm']) {
                    case 2:
                        $this->interviewReviewRepository->update($request['interview_id'], [
                            'salary' => $request['salary'],
                            'disc' => $request['disc'],
                            'status' => 2,
                            'updated_by' => auth()->id(),
                        ]);
                        if (!empty($request['confimerIDNext'])) {
                            $user = User::where('basic_info_id', $request['confimerIDNext'])->first();
                            $message = 'Thông báo !! Có một lời đề nghị đánh giá phỏng vấn từ ' . auth()->user()->name;
                            $link = route('result.interview', $request['interview_id']);
                            $this->sendNotification($user, $request['interview_id'], $link, $message);
                        }
                        break;
                    case 6:
                        $this->interviewReviewRepository->update($request['interview_id'], [
                            'specialize' => $request['specialize'],
                            'status' => 3,
                            'updated_by' => auth()->id(),
                        ]);
                        if (!empty($request['confimerIDNext'])) {
                            $user = User::where('basic_info_id', $request['confimerIDNext'])->first();
                            $message = 'Thông báo !! Có một lời đề nghị đánh giá phỏng vấn từ ' . auth()->user()->name;
                            $link = route('result.interview', $request['interview_id']);
                            $this->sendNotification($user, $request['interview_id'], $link, $message);
                        }
                        break;
                    case 3:
                        $this->interviewReviewRepository->update($request['interview_id'], [
                            'status' => 4,
                            'updated_by' => auth()->id(),
                        ]);
                        $data = $this->interviewReviewRepository->loadRelation(['user', 'interviewScores.confirm.staff'])
                        ->where('id', $request['interview_id'])
                        ->where('interviewScores.confirm.confirm_id', '<>', $request['confirm_id'])
                        ->first();
                        $message = 'Thông báo !! Đánh giá phỏng đã được duyệt bởi ' . auth()->user()->name;
                        if ($data->user) {
                            $link = route('result.view', $request['interview_id']);
                            $this->sendNotification($data->user, $request['interview_id'], $link, $message);
                        }
                        foreach ($data->interviewScores as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirm_id']) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                $link = route('result.view', $request['interview_id']);
                                $this->sendNotification($user, $request['interview_id'], $link, $message);
                            }
                        }
                        break;
                }
            } else {
                switch ($request['type_confirm']) {
                    case 2:
                        $this->interviewReviewRepository->update($request['interview_id'], [
                            'salary' => $request['salary'],
                            'disc' => $request['disc'],
                            'status' => 5,
                            'updated_by' => auth()->id(),
                        ]);
                        $data = $this->interviewReviewRepository->loadRelation(['user'])
                        ->where('id', $request['interview_id'])
                        ->first();
                        if ($data->user) {
                            $message = 'Thông báo !! Ứng viên bị từ chối bởi ' . auth()->user()->name;
                            $link = route('result.view', $request['interview_id']);
                            $this->sendNotification($data->user, $request['interview_id'], $link, $message);
                        }
                        break;
                    case 6:
                        $this->interviewReviewRepository->update($request['interview_id'], [
                            'specialize' => $request['specialize'],
                            'status' => 6,
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->interviewReviewRepository->loadRelation(['user', 'interviewScores.confirm.staff'])
                        ->where('id', $request['interview_id'])
                        ->first();
                        if ($data->user) {
                            $message = 'Thông báo !! Ứng viên bị từ chối ' . auth()->user()->name;
                            $link = route('result.view', $request['interview_id']);
                            $this->sendNotification($data->user, $request['interview_id'], $link, $message);
                        }
                        foreach ($data->interviewScores as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirm_id'] && $value->confirm->type_confirm != 3) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                $link = route('result.view', $request['interview_id']);
                                $this->sendNotification($user, $request['interview_id'], $link, $message);
                            }
                        }
                        break;
                    case 3:
                        $this->interviewReviewRepository->update($request['interview_id'], [
                            'status' => 7,
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->interviewReviewRepository->loadRelation(['user', 'interviewScores.confirm.staff'])
                        ->where('id', $request['interview_id'])
                        ->first();
                        $message = 'Thông báo !! Ứng viên bị từ chối ' . auth()->user()->name;
                        if ($data->user) {
                            $link = route('result.view', $request['interview_id']);
                            $this->sendNotification($data->user, $request['interview_id'], $link, $message);
                        }
                        foreach ($data->interviewScores as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirm_id']) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                $link = route('result.view', $request['interview_id']);
                                $this->sendNotification($user, $request['interview_id'], $link, $message);
                            }
                        }
                        break;
                }
            }

            $dataDetail['interview_date'] = Carbon::createFromFormat('d/m/Y', $request->interview_date)->format('Y-m-d');
            $this->interviewScoreRepository->update($request['detail_interview_id'], $dataDetail);
            $this->confirmRepository->update($request['confirm_id'], [
                'status' => $request['status'],
                'note' => $request['department_resource_notes'] ?? null,
                'updated_by' => auth()->id()
            ]);
            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }
}
