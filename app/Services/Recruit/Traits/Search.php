<?php

namespace App\Services\Recruit\Traits;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{

    public function getDataList()
    {
        $result = [];
        try {
            $data = $this->recruitRepository->loadRelation(['room', 'team', 'position', 'user', 'confirms.confirmReceive'])->where('delete_flag', 0);
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataListForRecruitCriteria()
    {
        $result = [];
        try {
            $data = $this->recruitRepository->loadRelation(['room', 'team', 'position', 'user', 'confirms.confirmReceive'])->where('delete_flag', 0)->where('status', 5);
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }


    public function getDataListByRoom($roomID)
    {
        $result = [];
        try {
            $data = $this->recruitRepository->loadRelation(['room', 'team', 'position', 'user', 'confirms.confirmReceive'])->where('room_id', $roomID)->where('delete_flag', 0);
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataListByRoomForRecruitCriteria($roomID)
    {
        $result = [];
        try {
            $data = $this->recruitRepository->loadRelation(['room', 'team', 'position', 'user', 'confirms.confirmReceive'])->where('room_id', $roomID)->where('delete_flag', 0)->where('status', 5);
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function getRooms()
    {
        $result = [];
        try {
            $rooms = $this->roomRepository->findAll();
            $result = !empty($rooms) ? $rooms : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getRoomsByUser($id)
    {
        $result = [];
        try {
            $rooms = $this->roomRepository->findByCondition([
               'id' => $id
            ]);
            $result = !empty($rooms) ? $rooms : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getTeamsByRoomID($room_id)
    {
        $result = [];
        try {
            $teams = $this->organizationalRepository->getTeamsByRoom($room_id);
            $result = !empty($teams) ? $teams : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getHR()
    {
        $result = [];
        try {
            $positions = User::with('roles')->whereHas('roles', function($query) {
                $query->where('name', 'hr-manager')->orWhere('name', 'hr-chief');
            })->get();
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getStaffsByTeam($room_id, $team_id, $position_id)
    {
        $result = [];
        try {
            $positions = $this->organizationalRepository->getStaffByConditions($room_id, $team_id, $position_id);
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getStaffsByPositionID($position_id)
    {
        $result = [];
        try {
            $positions = $this->organizationalRepository->getListChief($position_id);
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataByID($id)
    {
        $result = [];
        try {
            $criteria = $this->recruitRepository->loadRelation(['room', 'team', 'position', 'user', 'confirms', 'confirms.confirm'])->find($id);
            $result = !empty($criteria) ? $criteria : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataForm()
    {
        $result = [];
        try {
            $interview = $this->interviewReviewRepository
            ->loadRelation(['room', 'team', 'position', 'interviewScores.confirm.staff', 'user.staff'])
            ->where('delete_flag', 0);
            $result = !empty($interview) ? $interview->toArray() : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
    
    public function getDataFormByID($id)
    {
        $result = [];
        try {
            $interview = $this->interviewReviewRepository->loadRelation(['room', 'team', 'position', 'interviewScores.confirm.staff'])->find($id);
            $result = !empty($interview) ? $interview->toArray() : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function getDataByRoomTeamPositionId($room_id, $team_id, $position_id) {
        $result = [];
        try {
            $criteria = $this->recruitRepository->loadRelation(['room', 'team', 'position', 'user', 'confirms', 'confirms.confirm', 'confirms.confirm.staff'])
                ->where('room_id', $room_id)->where('team_id', $team_id)->where('position_id', $position_id)->first();
            $result = !empty($criteria) ? $criteria->toArray() : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }
}
