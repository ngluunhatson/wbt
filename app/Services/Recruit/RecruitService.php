<?php

namespace App\Services\Recruit;

use App\Repositories\Confirm\ConfirmRepository;
use App\Repositories\Organizational\OrganizationalRepository;
use App\Repositories\Recruit\InterviewReviewRepository;
use App\Repositories\Recruit\InterviewScoreRepository;
use App\Repositories\Recruit\RecruitRepository;
use App\Repositories\RecruitConfirm\RecruitConfirmRepository;
use App\Repositories\Room\RoomRepository;
use App\Services\BaseService;
use App\Services\Recruit\Traits\AddEdit;
use App\Services\Recruit\Traits\Delete;
use App\Services\Recruit\Traits\Search;

class RecruitService extends BaseService
{
    use Search, AddEdit, Delete;
    public $organizationalRepository;

    public function __construct(
        OrganizationalRepository $organizationalRepository,
        RecruitRepository $recruitRepository,
        ConfirmRepository $confirmRepository,
        RoomRepository $roomRepository,
        RecruitConfirmRepository $recruitConfirmRepository,
        InterviewReviewRepository $interviewReviewRepository,
        InterviewScoreRepository $interviewScoreRepository,
    ) {
        $this->organizationalRepository = $organizationalRepository;
        $this->recruitRepository = $recruitRepository;
        $this->confirmRepository = $confirmRepository;
        $this->roomRepository = $roomRepository;
        $this->recruitConfirmRepository = $recruitConfirmRepository;
        $this->interviewReviewRepository = $interviewReviewRepository;
        $this->interviewScoreRepository = $interviewScoreRepository;
    }
}
