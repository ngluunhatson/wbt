<?php

namespace App\Services\Organizational\Traits;

use Arr;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Request;
use Illuminate\Support\Facades\Validator;

trait AddEdit
{
    public function createData(array $input)
    {
        $result = [];
        
        try {
            $query = $this->organizationalRepository->createNodeData($input);
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateData(array $input, $id)
    {
        $result = [];
        
        try {
            $query = $this->organizationalRepository->updateNodeData($id, $input);
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function update10y($request) {
        $result = [
            'status' => false,
            'message' => 'Cập nhật không thành công !!',
        ];
        // $request->validate([
        //     'category_id'   =>  'integer|required',
        //     'title'   =>  'required',
        //     'content'   =>  'required',
        //     'status'   =>  'integer|in:0,1',
        //     'blog_image'   =>  'mimes:svg,png,jpeg,jpg|max:2048'
        // ]);
        

        DB::beginTransaction();
        try {
            $dataUpdate = $request->except(
                '_token',
            );

            $dataUpdate['updated_by'] = auth()->id();

            $input_update_10y = [
                'opportunity_id' => 1,
                'business_development_id' => 1,
                'personnel_id' => 1,
                'enforcement_id' => 1,
                'mission_id' => 1,
                'full_name' => $dataUpdate["name"],
                'company' => $dataUpdate["company"],
                'position' => $dataUpdate["position"],
                'from_year' => $dataUpdate['start'] == 'hiện tại' ? \Carbon\Carbon::now('Asia/Ho_Chi_Minh') : \Carbon\Carbon::createFromFormat('d/m/Y',$dataUpdate["start"])->format('Y-m-d'),
                'to_year' => $dataUpdate['end'] == 'hiện tại' ? \Carbon\Carbon::now('Asia/Ho_Chi_Minh') : \Carbon\Carbon::createFromFormat('d/m/Y',$dataUpdate["end"])->format('Y-m-d'),
                'ten_year_goal' => $dataUpdate['ten_year_goal'],
                'three_year_goal' => $dataUpdate['three_year_goal'],
                'priority_in_three_years' => $dataUpdate['priority_in_three_years'],
            ];

            $input_update_opportunity = [
                'opportunities' => $dataUpdate["opportunities"],
                'strategy_in_one_sentence' => $dataUpdate["strategy_in_one_sentence"],
                'one_year_goal' => $dataUpdate["one_year_goal"],
                'priorities_for_one_year' => $dataUpdate["priorities_for_one_yea"],
                'quarterly_goals' => $dataUpdate["quarterly_goals"],
                'priorities_for_the_quarter' => $dataUpdate["priorities_for_the_quarter"],
            ];

            $input_update_business = [
                'core_customers' => $dataUpdate["core_customers"],
                'brand_promise' => $dataUpdate["brand_promise"],
                'customer_standards' => $dataUpdate["customer_standards"],
            ];

            $input_update_personnel = [
                'personal_development_plan' => $dataUpdate["personal_development_plan"],
                'language_development_plan' => $dataUpdate["team_development_plan"],
            ];

            $input_update_enforcement = [
                'my_kpis' => $dataUpdate["my_kpis"],
                'team_kpis' => $dataUpdate["team_kpis"],
                'personal_work_schedule' => $dataUpdate["personal_work_schedul"],
                'team_work_schedule' => $dataUpdate["team_work_schedule"],
            ];
            
            $input_update_mission = [
                'purpose' => $dataUpdate["purpose"],
                'core_values' => $dataUpdate["core_values"],
                'community_contribution' => $dataUpdate["community_contribution"],
            ];
            //  return count($this->plan10yRepository->findByCondition([['id',1]]));
            //CREATE IF NOT EXIST
            if(count($this->plan10yRepository->findByCondition([['id',1]])) == 0) {
                $plan = $this->plan10yRepository->create($input_update_10y);
                $plan->save();
            } else {
                $this->plan10yRepository->update(1, $input_update_10y);
            }


            if(count($this->opportunityRepository->findByCondition([['id',1]])) == 0){
                $oppor = $this->opportunityRepository->create($input_update_opportunity);
                $oppor->save();
            } else {
                $this->opportunityRepository->update(1, $input_update_opportunity);
            }

            if(count($this->businessDevelopmentRepository->findByCondition([['id',1]])) == 0){
                $business = $this->businessDevelopmentRepository->create($input_update_business);
                $business->save();
            } else {
                $this->businessDevelopmentRepository->update(1, $input_update_business);
            }

            if(count($this->personnelRepository->findByCondition([['id',1]])) == 0) {
                $personnel = $this->personnelRepository->create($input_update_personnel);
                $personnel->save();
            } else {
                $this->personnelRepository->update(1, $input_update_personnel);
            }

            if(count($this->enforcementRepository->findByCondition([['id',1]])) == 0) {
                $enforcement = $this->enforcementRepository->create($input_update_enforcement);
                $enforcement->save();
            } else {
                $this->enforcementRepository->update(1, $input_update_enforcement);
            }

            if(count($this->missionRepository->findByCondition([['id',1]])) == 0) {
                $mission = $this->missionRepository->create($input_update_mission);
                $mission->save();
            } else {
                $this->missionRepository->update(1, $input_update_mission);
            }


            $result['status'] = true;
            $result['message'] = 'Cập nhật thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return $e->getMessage();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
