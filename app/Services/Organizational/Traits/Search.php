<?php

namespace App\Services\Organizational\Traits;

use Arr;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use Request;
use Illuminate\Support\Facades\Validator;

trait Search
{
    public function getConditions($room_id)
    {
        $result = [];
        try {
            $teams = $this->teamRepository->findByCondition([
                ['room_id', $room_id],
            ]);
            $positions = $this->positionRepository->findAll();
            $rooms = $this->roomRepository->findAll();

            $result = [
                'teams' => $teams,
                'positions' => $positions,
                'rooms' => $rooms,
            ];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    /**
     * Get data of Organizational by id 
     * author : ntqui
     * since : 20221005
     *
     * @param  String $id
     * @return array
     */
    public function getDataByID(string $id): array
    {
        $result = [];
        try {
            $query = $this->organizationalRepository->getDataById($id);
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    /**
     * Get data of Organizational by room_id 
     * author : ntqui
     * since : 20221005
     *
     * @param  String $id
     * @return Collection
     */
    public function getDataByRoomID(string $room_id): Collection
    {
        $result = [];
        try {
            $query = $this->organizationalRepository->getDataByRomID($room_id);
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getRoomDirectorate() {
        $result = [];
        try {
            $query = $this->organizationalRepository->loadRelation(['staff', 'position', 'laborContract'])->where('room_id',8);
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            return $e->getMessage();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getPlan10y() {
        $result = [];
        try {
            $query = $this->plan10yRepository->loadRelation(['opportunity', 'businessDevelopment', 'personnel', 'enforcement', 'mission'])->where('id',1);
            $result = !empty($query) ? $query : [];
        } catch (Exception $e) {
            return $e->getMessage();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getRooms()
    {
        $result = [];
        try {
            $rooms = $this->organizationalRepository->getRooms();
            $result = !empty($rooms) ? $rooms : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getRoomByBasicInfoID($basic_info_id)
    {
        $result = [];
        try {
            $teams = $this->organizationalRepository->getRoomByBasicInfo($basic_info_id);
            $result = !empty($teams) ? $teams : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getTeamsByRoomID($room_id)
    {
        $result = [];
        try {
            $teams = $this->teamRepository->findByCondition(['room_id' => $room_id]);
            $result = !empty($teams) ? $teams : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getPositionByTeamID($team_id)
    {
        $result = [];
        try {
            $positions = $this->positionRepository->findByCondition([
                'team_id' => $team_id,
                ['flag_filter', 1]
            ]);
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getStaffByConditions($room_id, $team_id, $position_id)
    {
        $result = [];
        try {
            $staffs = $this->organizationalRepository->getStaffByConditions($room_id, $team_id, $position_id);
            $result = !empty($staffs) ? $staffs : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getMangerInRoom($room_id)
    {
        $result = [];
        try {
            $staffs = $this->organizationalRepository->getMangerInRoom($room_id);
            $result = !empty($staffs) ? $staffs : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
