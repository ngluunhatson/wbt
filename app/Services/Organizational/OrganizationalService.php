<?php

namespace App\Services\Organizational;

use App\Repositories\Organizational\OrganizationalRepository;
use App\Repositories\Position\PositionRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\Team\TeamRepository;
use App\Repositories\Plan10y\Plan10yRepository;
use App\Repositories\Opportunity\OpportunityRepository;
use App\Repositories\BusinessDevelopment\BusinessDevelopmentRepository;
use App\Repositories\Personnel\PersonnelRepository;
use App\Repositories\Enforcement\EnforcementRepository;
use App\Repositories\Mission\MissionRepository;
use App\Services\BaseService;
use App\Services\Organizational\Traits\AddEdit;
use App\Services\Organizational\Traits\Delete;
use App\Services\Organizational\Traits\Search;

class OrganizationalService extends BaseService
{
    use Search, AddEdit, Delete;
    public $roomRepository, $teamRepository, $positionRepository, $organizationalRepository, $plan10yRepository, $opportunityRepository, $businessDevelopmentRepository;

    public $personnelRepository, $enforcementRepository, $missionRepository;

    public function __construct(
        RoomRepository $roomRepository,
        TeamRepository $teamRepository,
        PositionRepository $positionRepository,
        OrganizationalRepository $organizationalRepository,
        Plan10yRepository $plan10yRepository,
        OpportunityRepository $opportunityRepository,
        BusinessDevelopmentRepository $businessDevelopmentRepository,
        PersonnelRepository $personnelRepository,
        EnforcementRepository $enforcementRepository,
        MissionRepository $missionRepository,
    ) {
        $this->roomRepository = $roomRepository;
        $this->teamRepository = $teamRepository;
        $this->positionRepository = $positionRepository;
        $this->organizationalRepository = $organizationalRepository;
        $this->plan10yRepository = $plan10yRepository;
        $this->opportunityRepository = $opportunityRepository;
        $this->businessDevelopmentRepository = $businessDevelopmentRepository;
        $this->personnelRepository = $personnelRepository;
        $this->enforcementRepository = $enforcementRepository;
        $this->missionRepository = $missionRepository;
    }
}
