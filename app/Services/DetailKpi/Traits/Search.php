<?php

namespace App\Services\DetailKpi\Traits;

use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{
    public function getDataByID($id)
    {
        $result = [];
        try {
            $data = $this->detailKpiRepository->getById($id);
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataByKpiID($id)
    {
        $result = [];
        try {
            $data = $this->detailKpiRepository->getDataByKpiID($id);
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}