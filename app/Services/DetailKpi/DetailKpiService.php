<?php

namespace App\Services\DetailKpi;

use App\Repositories\Confirm\ConfirmRepository;
use App\Repositories\Criteria\CriteriaRepository;
use App\Repositories\CriteriaConfirm\CriteriaConfirmRepository;
use App\Repositories\Organizational\OrganizationalRepository;
use App\Repositories\TableKpi\TableKpiRepository;
use App\Repositories\KpiConfirm\KpiConfirmRepository;
use App\Repositories\DetailKpi\DetailKpiRepository;
use App\Services\BaseService;
use App\Services\TableKpi\Traits\AddEdit;
use App\Services\TableKpi\Traits\Delete;
use App\Services\DetailKpi\Traits\Search;

class DetailKpiService extends BaseService
{
    use Search;
    public $organizationalRepository;
    public $tableKpiRepository;

    public function __construct(
        OrganizationalRepository $organizationalRepository,
        TableKpiRepository $tableKpiRepository,
        ConfirmRepository $confirmRepository,
        CriteriaConfirmRepository $criteriaConfirmRepository,
        CriteriaRepository $criteriaRepository,
        KpiConfirmRepository $kpiConfirmRepository,
        DetailKpiRepository $detailKpiRepository,
    ) {
        $this->organizationalRepository = $organizationalRepository;
        $this->tableKpiRepository = $tableKpiRepository;
        $this->confirmRepository = $confirmRepository;
        $this->criteriaConfirmRepository = $criteriaConfirmRepository;
        $this->criteriaRepository = $criteriaRepository;
        $this->kpiConfirmRepository = $kpiConfirmRepository;
        $this->detailKpiRepository = $detailKpiRepository;
    }
}