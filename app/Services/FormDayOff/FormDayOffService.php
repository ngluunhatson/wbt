<?php

namespace App\Services\FormDayOff;

use App\Repositories\Confirm\ConfirmRepository;
use App\Repositories\FormDayOff\FormDayOffConfirmRepository;
use App\Repositories\FormDayOff\FormDayOffDetailRepository;
use App\Repositories\FormDayOff\FormDayOffFormDayOffDetailRepository;
use App\Repositories\FormDayOff\FormDayOffRepository;
use App\Repositories\Timekeep\TimekeepRepository;
use App\Repositories\TrackingDayOff\TrackingDayOffRepository;
use App\Services\BaseService;
use App\Services\FormDayOff\Traits\AddEdit;
use App\Services\FormDayOff\Traits\Delete;
use App\Services\FormDayOff\Traits\Search;

class FormDayOffService extends BaseService
{
    use Search, AddEdit, Delete;
    public $formDayOffRepository, $formDayOffConfirmRepository, $confirmRepository, $trackingDayOffRepository, $formDayOffDetailRepository, $timekeepRepository;

    public function __construct(
        FormDayOffRepository $formDayOffRepository,
        FormDayOffConfirmRepository $formDayOffConfirmRepository,
        ConfirmRepository $confirmRepository,
        TrackingDayOffRepository $trackingDayOffRepository,
        FormDayOffDetailRepository $formDayOffDetailRepository,
        TimekeepRepository $timekeepRepository,
    
    ) {
        $this -> formDayOffRepository        = $formDayOffRepository;
        $this -> formDayOffConfirmRepository = $formDayOffConfirmRepository;
        $this -> confirmRepository           = $confirmRepository;
        $this -> trackingDayOffRepository    = $trackingDayOffRepository;
        $this -> formDayOffDetailRepository  = $formDayOffDetailRepository;
        $this -> timekeepRepository          = $timekeepRepository;

    }
}
