<?php

namespace App\Services\FormDayOff\Traits;


use App\Models\FormDayOff;
use App\Models\Organizational;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{

    public function getConfirmedFormDayOffsOfUser($id, $mode = 'user')
    {
        $result = [];

        try {

            $basic_info_id = $id;
            if ($mode == 'user')
                $basic_info_id = User::where('id', $id)->first()->basic_info_id;


            $formDayOffs = FormDayOff::with(['detailForms']) -> where('basic_info_id', $basic_info_id)->whereIn('status', [2,3,4,5,6]) -> get();

            if (!empty($formDayOffs))
                $result = $formDayOffs;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getFormDayOffById($id)
    {
        $result = [];

        try {
            $formDayOff = FormDayOff::where('id', $id)->first();

            if (!empty($formDayOff)) {
                $form_user_id = User::where('basic_info_id', $formDayOff->basic_info_id)->first()->id;
                $result = [$formDayOff, $form_user_id];
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getAllFormDayOffs()
    {
        $result = [];

        try {
            $formDayOffs = $this->formDayOffRepository->loadRelation(['basicInfo', 'user', 'confirms', 'detailForms'])->where('delete_flag', 0);

            if (!empty($formDayOffs))
                $result = $formDayOffs;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getAllFormDayOffsDict()
    {
        $result = [];

        try {
            $formDayOffs = $this->formDayOffRepository->loadRelation(['basicInfo', 'user', 'confirms'])->where('delete_flag', 0);

            $formDayOffDict = [];

            foreach ($formDayOffs as $formDayOff) {
                $formFirstStartDate     = $formDayOff->detailForms[0]->start_date;
                $formFirstStartMonthInt = intval(date('m', strtotime($formFirstStartDate)));
                $formFirstStartYearInt  = intval(date('Y', strtotime($formFirstStartDate)));
                $formDayOffDict[$formFirstStartYearInt][$formFirstStartMonthInt][] = $formDayOff;
            }


            if (!empty($formDayOffDict))
                $result = $formDayOffDict;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getFormDayOffsAvailableToUser($user_id)
    {
        $result = [];

        try {
            $user_basic_info_id  =  User::where('id', $user_id)->first()->basic_info_id;
            $formDayOffs = $this->formDayOffRepository->loadRelation(['basicInfo', 'user', 'confirms', 'detailForms'])->where('basic_info_id', $user_basic_info_id)->where('delete_flag', 0);

            if (!empty($formDayOffs))
                $result = $formDayOffs;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getFormDayOffsAvailableToUserDict($user_id)
    {
        $result = [];

        try {
            $user_basic_info_id  =  User::where('id', $user_id)->first()->basic_info_id;
            $formDayOffs = $this->formDayOffRepository->loadRelation(['basicInfo', 'user', 'confirms'])->where('basic_info_id', $user_basic_info_id)->where('delete_flag', 0);

            $formDayOffDict = [];

            foreach ($formDayOffs as $formDayOff) {
                $formFirstStartDate     = $formDayOff->detailForms[0]->start_date;
                $formFirstStartMonthInt = intval(date('m', strtotime($formFirstStartDate)));
                $formFirstStartYearInt  = intval(date('Y', strtotime($formFirstStartDate)));
                $formDayOffDict[$formFirstStartYearInt][$formFirstStartMonthInt][] = $formDayOff;
            }


            if (!empty($formDayOffDict))
                $result = $formDayOffDict;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }


    public function getFormDayOffAssignedToUser($user_id)
    {
        $result = [];

        try {
            $user_basic_info_id  =  User::where('id', $user_id)->first()->basic_info_id;
            $formDayOffs = $this->getAllFormDayOffs();

            $assignedFormDayOffs = [];

            foreach ($formDayOffs as $formDayOff) {
                if ($formDayOff->user->id == $user_id) {
                    $assignedFormDayOffs[] = $formDayOff;
                    break;
                }
                foreach ($formDayOff->confirms as $formDayOffConfirm) {
                    if ($formDayOffConfirm->confirm->basic_info_id == $user_basic_info_id) {
                        $assignedFormDayOffs[] = $formDayOff;
                        break;
                    }
                }
            }


            if (!empty($assignedFormDayOffs))
                $result = $assignedFormDayOffs;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getFormDayOffAssignedToUserDict($user_id)
    {
        $result = [];

        try {
            $user_basic_info_id  =  User::where('id', $user_id)->first()->basic_info_id;
            $formDayOffs = $this->getAllFormDayOffs();

            $assignedFormDayOffs = [];
            $assignedFormDayOffDict = [];

            foreach ($formDayOffs as $formDayOff) {
                foreach ($formDayOff->confirms as $formDayOffConfirm) {
                    if ($formDayOffConfirm->confirm->basic_info_id == $user_basic_info_id) {
                        $assignedFormDayOffs[] = $formDayOff;
                        break;
                    }
                }
            }

            foreach ($assignedFormDayOffs as $assignedFormDayOff) {
                $formFirstStartDate     = $assignedFormDayOff->detailForms[0]->start_date;
                $formFirstStartMonthInt = intval(date('m', strtotime($formFirstStartDate)));
                $formFirstStartYearInt  = intval(date('Y', strtotime($formFirstStartDate)));
                $assignedFormDayOffDict[$formFirstStartYearInt][$formFirstStartMonthInt][] = $assignedFormDayOff;
            }


            if (!empty($assignedFormDayOffDict))
                $result = $assignedFormDayOffDict;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getBasicInfoOfReplacementArray($id, $mode = "user")
    {

        $result = [];

        try {

            $staffs = [];
            $user_basic_info_id = $id;
            if ($mode == 'user')
                $user_basic_info_id = User::where('id', $id)->first()->basic_info_id;

            $organizational_team_id = Organizational::where('basic_info_id', $user_basic_info_id)->first()->team_id;

            $organizationals = Organizational::where('team_id', $organizational_team_id)->whereNotNull('basic_info_id')->select('basic_info_id')->distinct()->get();


            foreach ($organizationals as $organizational) {
                if ($organizational->staff->id !=  $user_basic_info_id)
                    $staffs[] = $organizational->staff;
            }

            // dd($staffs);

            if (!empty($staffs))
                $result = $staffs;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }


  

}
