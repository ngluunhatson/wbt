<?php

namespace App\Services\FormDayOff\Traits;

use App\Models\Confirm;
use App\Models\FormDayOff;
use App\Models\FormDayOffDetail;
use App\Models\Timekeeping;
use App\Models\TrackingDayOff;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait AddEdit
{

    public function checkOtherConfirmedForms($formDayOff_X)
    {

        $otherConfirmedformDayOffs = FormDayOff::with(['detailForms'])
            ->where('id', '!=', $formDayOff_X->id)
            ->where('basic_info_id', $formDayOff_X->basic_info_id)
            ->where('status', 6)
            ->get();

        $new_status  = $formDayOff_X->status;
        $isCancelled = false;
        $isChanged   = false;

        foreach ($otherConfirmedformDayOffs as $otherConfirmedformDayOff)
            foreach ($otherConfirmedformDayOff->detailForms as $otherFormDetailForm) {
                foreach ($formDayOff_X->detailForms as $detailForm_X) {

                    $detailForm_X_StartDate_StrToTime        = strtotime($detailForm_X->start_date);
                    $detailForm_X_EndDate_StrToTime          = strtotime($detailForm_X->end_date);
                    $otherFormDetailForm_StartDate_StrToTime = strtotime($otherFormDetailForm->start_date);
                    $otherFormDetailForm_EndDate_StrToTime   = strtotime($otherFormDetailForm->end_date);


                    if ($detailForm_X_EndDate_StrToTime  >= $otherFormDetailForm_StartDate_StrToTime) {
                        $isChanged = true;
                        break;
                    }

                    if ($detailForm_X_StartDate_StrToTime <= $otherFormDetailForm_EndDate_StrToTime) {
                        $isChanged = true;
                        break;
                    }

                    if ($detailForm_X_StartDate_StrToTime  > $otherFormDetailForm_StartDate_StrToTime && $detailForm_X_EndDate_StrToTime < $otherFormDetailForm_EndDate_StrToTime) {
                        $isChanged = true;
                        break;
                    }

                    if ($detailForm_X_StartDate_StrToTime <= $otherFormDetailForm_StartDate_StrToTime && $detailForm_X_EndDate_StrToTime >= $otherFormDetailForm_EndDate_StrToTime) {
                        $isCancelled = true;
                        break;
                    }
                }
                if ($isChanged && !$isCancelled)
                    $new_status = 11;

                if ($isCancelled && !$isChanged)
                    $new_status = 10;

                if ($new_status != $formDayOff_X->status)
                    $this->formDayOffRepository->update($otherConfirmedformDayOff->id, [
                        'status'        => $new_status,
                        'updated_by'    => auth()->id()

                    ]);
            }
    }

    public function updateTimeKeepHelper($timeKeepDayType, $formDayOffType,  $isOffHalfDayStart, $isOffHalfDayEnd, $isStartEndSameDate)
    {
        $returnDayType = $timeKeepDayType;
        $isHalfDayOff  = $isStartEndSameDate && ($isOffHalfDayStart || $isOffHalfDayEnd);

        switch ($timeKeepDayType) {
            case 1:
                switch ($formDayOffType) {
                    case 1:
                        $returnDayType = 10;
                        break;
                    case 2:
                        $returnDayType = 7;
                        break;
                    case 3:
                        $returnDayType = 7;
                        break;
                    case 4:
                        $returnDayType = 14;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

            case 2:
                switch ($formDayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ? 11 : 5;
                        break;
                    case 2:
                        $returnDayType = ($isHalfDayOff) ? 12 : 6;
                        break;
                    case 3:
                        $returnDayType = ($isHalfDayOff) ? 12 : 6;
                        break;
                    case 4:
                        $returnDayType = 14;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

                // case 3:
                //     switch ($formDayOffType) {
                //         case 1:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 2:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 3:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 4:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 5:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //     }

                //     break;

                // case 4:
                //     switch ($formDayOffType) {
                //         case 1:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 2:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 3:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 4:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 5:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //     }

                //     break;

            case 5:
                switch ($formDayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ? 11 : $returnDayType;
                        break;
                    case 2:
                        $returnDayType = ($isHalfDayOff) ? 13 : 6;
                        break;
                    case 3:
                        $returnDayType = ($isHalfDayOff) ? 13 : 6;
                        break;
                    case 4:
                        $returnDayType = 14;
                        break;
                    case 5:
                        $returnDayType = ($isHalfDayOff) ? 3 : 4;
                        break;
                }

                break;

            case 6:
                switch ($formDayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ? 13 : 5;
                        break;
                    case 2:
                        $returnDayType = ($isHalfDayOff) ? 12 : $returnDayType;
                        break;
                    case 3:
                        $returnDayType = ($isHalfDayOff) ? 12 : $returnDayType;
                        break;
                    case 4:
                        $returnDayType = 14;
                        break;
                    case 5:
                        $returnDayType = ($isHalfDayOff) ? 12 : 4;
                        break;
                }

                break;

            case 7:
                switch ($formDayOffType) {
                    case 1:
                        $returnDayType = 10;
                        break;
                    case 2:
                        $returnDayType = $returnDayType;
                        break;
                    case 3:
                        $returnDayType = $returnDayType;
                        break;
                    case 4:
                        $returnDayType = 14;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

                // case 8:
                //     switch ($formDayOffType) {
                //         case 1:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 2:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 3:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 4:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 5:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //     }

                //     break;

                // case 9:
                //     switch ($formDayOffType) {
                //         case 1:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 2:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 3:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 4:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //         case 5:
                //             $returnDayType = ($isHalfDayOff) ? $returnDayType : $returnDayType;
                //             break;
                //     }

                //     break;

            case 10:
                switch ($formDayOffType) {
                    case 1:
                        $returnDayType = $returnDayType;
                        break;
                    case 2:
                        $returnDayType = 7;
                        break;
                    case 3:
                        $returnDayType = 7;
                        break;
                    case 4:
                        $returnDayType = 14;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

            case 11:
                switch ($formDayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ? $returnDayType : 5;
                        break;
                    case 2:
                        $returnDayType = ($isHalfDayOff) ? 12 : 6;
                        break;
                    case 3:
                        $returnDayType = ($isHalfDayOff) ? 12 : 6;
                        break;
                    case 4:
                        $returnDayType = 14;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

            case 12:
                switch ($formDayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ? 11 : 5;
                        break;
                    case 2:
                        $returnDayType = ($isHalfDayOff) ? $returnDayType : 6;
                        break;
                    case 3:
                        $returnDayType = ($isHalfDayOff) ? $returnDayType : 6;
                        break;
                    case 4:
                        $returnDayType = 14;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

            case 13:
                switch ($formDayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ? 11 : 6;
                        break;
                    case 2:
                        $returnDayType = ($isHalfDayOff) ?  12 : 6;
                        break;
                    case 3:
                        $returnDayType = ($isHalfDayOff) ? 12 : 6;
                        break;
                    case 4:
                        $returnDayType = 14;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;

            case 14:
                switch ($formDayOffType) {
                    case 1:
                        $returnDayType = ($isHalfDayOff) ? 11 : 5;
                        break;
                    case 2:
                        $returnDayType = ($isHalfDayOff) ? 12 : 6;
                        break;
                    case 3:
                        $returnDayType = ($isHalfDayOff) ? 12 : 6;
                        break;
                    case 4:
                        $returnDayType = $returnDayType;
                        break;
                    case 5:
                        $returnDayType = 4;
                        break;
                }

                break;
        }
        return $returnDayType;
    }

    public function updateTimeKeep($formDayOff_X)
    {
        try {
            foreach ($formDayOff_X->detailForms as $detailForm) {

                $detailFormStartDay = intval(date('d', strtotime($detailForm->start_date)));
                $detailFormStartMonth = intval(date('m', strtotime($detailForm->start_date)));
                $detailFormStartYear = intval(date('Y', strtotime($detailForm->start_date)));


                $detailFormEndDay = intval(date('d', strtotime($detailForm->end_date)));
                $detailFormEndMonth = intval(date('m', strtotime($detailForm->end_date)));
                $detailFormEndYear = intval(date('Y', strtotime($detailForm->end_date)));

                $isOffHalfDayStart = $detailForm->start_time == '13:00';
                $isOffHalfDayEnd   = $detailForm->end_time == '12:00';

                $monthLoopCount = 0;


                for ($y = $detailFormStartYear; $y <= $detailFormEndYear; $y++) {
                    $isLeapYear = false;
                    if ($y % 4 == 0)
                        if ($y % 100 == 0) {
                            if ($y % 400 == 0)
                                $isLeapYear = true;
                        } else
                            $isLeapYear = true;


                    $monthLimit = $y != $detailFormEndYear ? 12 : $detailFormEndMonth;

                    $m = ($y != $detailFormEndYear) ? 1 : $detailFormStartMonth;

                    while ($m <= $monthLimit) {

                        $cur_timekeep_object_form = Timekeeping::where('basic_info_id', $formDayOff_X->basic_info_id)->where('year', $y)->where('month', $m)->first();

                        if ($cur_timekeep_object_form) {

                            $cur_timekeep_array_form = $cur_timekeep_object_form->toArray();
                            $cur_timekeep_update_input = [];

                            $limitDayInMonth = [31, ($isLeapYear) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][$m - 1];

                            $dayLimit = $limitDayInMonth;
                            $d = 1;

                            if (($m == $monthLimit) && ($y == $detailFormEndYear)) {
                                $dayLimit = $detailFormEndDay;
                                if ($monthLoopCount == 0)
                                    $d = $detailFormStartDay;
                            }



                            while ($d <= $dayLimit) {
                                $isStartEndSameDate = ($d == $detailFormEndDay) && ($m == $detailFormEndMonth) && ($y ==  $detailFormEndYear);

                                $result_timekeep_day_type = $this->updateTimeKeepHelper($cur_timekeep_array_form['day_' . $d], $detailForm->type, $isOffHalfDayStart, $isOffHalfDayEnd, $isStartEndSameDate);

                                if ($result_timekeep_day_type != $cur_timekeep_array_form['day_' . $d])
                                    $cur_timekeep_update_input = [
                                        'day_' . $d       =>  $result_timekeep_day_type,
                                    ];

                                $d += 1;
                            }

                            if (!empty($cur_timekeep_update_input)) {
                                $cur_timekeep_update_input['updated_by'] = auth()->id();
                                $this->timekeepRepository->update($cur_timekeep_array_form['id'], $cur_timekeep_update_input);
                            }
                        }

                        $m += 1;
                        $monthLoopCount += 1;
                    }
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }
    }


    public function saveDataNew($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {

            $input_all = $request->except('_token');

            $creatorConfirmFlag = array_key_exists("assign", $input_all);

            $confirm_id = [];
            $detail_id = [];

            // for ($i = 1; $i <= $input_all['index_time_frame']; $i++) {
            //     if (array_key_exists('start_date' . $i, $input_all) && !$input_all['start_date' . $i]) {
            //         $result['message'] = 'Lỗi: Không Có Ngày Bắt Đầu ' . $i;
            //         return $result;
            //     }

            //     if (array_key_exists('end_date' . $i, $input_all) && !$input_all['end_date' . $i]) {
            //         $result['message'] = 'Lỗi Không Có Ngày Kết Thúc ' . $i;
            //         return $result;
            //     }

            //     if (array_key_exists('start_time' . $i, $input_all) && !$input_all['start_time' . $i]) {
            //         $result['message'] = 'Lỗi Không Có Giờ Bắt Đầu ' . $i;
            //         return $result;
            //     }

            //     if (array_key_exists('end_time' . $i, $input_all) && !$input_all['end_time' . $i]) {
            //         $result['message'] = 'Lỗi Không Có Giờ Kết Thúc ' . $i;
            //         return $result;
            //     }
            //     if (array_key_exists('type' . $i, $input_all) && $input_all['type' . $i] == 0) {
            //         $result['message'] = 'Lỗi Không Có Loại Nghỉ Phép ' . $i;
            //         return $result;
            //     }

            //     if (array_key_exists('reason' . $i, $input_all) && $input_all['reason' . $i] == 0) {
            //         $result['reason'] = 'Lỗi Không Có Lý Do ' . $i;
            //         return $result;
            //     }
            // }

            $form_day_off_create_input = [
                "basic_info_id"           => $input_all['basic_info_id_of_form'],
                "detail_id"               => [],
                "confirm_id"              => [],
                'status'                  => 1,
                "created_by"              => auth()->id(),
                "delete_flag"             => 0

            ];
            $formDayOff = $this->formDayOffRepository->create($form_day_off_create_input);

            $total_day_off_asked_for = 0;

            for ($i = 1; $i <= intval($input_all['index_time_frame']); $i++) {
                if (array_key_exists('start_date' . $i, $input_all)) {
                    $total_day_off_asked_for += floatval($input_all['total_day_off_in_timeframe' . $i]);
                    $form_day_off_detail_create_input = [
                        "form_day_off_id"            => $formDayOff->id,
                        "start_date"                 => $input_all['start_date' . $i],
                        "start_time"                 => $input_all['start_time' . $i],
                        "end_date"                   => $input_all['end_date' . $i],
                        "end_time"                   => $input_all['end_time' . $i],
                        "reason"                     => $input_all['reason' . $i],
                        "total_day_off_in_timeframe" => floatval($input_all['total_day_off_in_timeframe' . $i]),
                        "type"                       => $input_all['type' . $i],
                        "created_by"                 => auth()->id(),
                        "delete_flag"                => 0
                    ];

                    $formDayOffDetail = $this->formDayOffDetailRepository->create($form_day_off_detail_create_input);

                    $detail_id[] = $formDayOffDetail->id;
                }
            }


            $replacementExistFlag = $input_all['replacement_basic_info_id'] != 0;

            $confirm_basic_info_ids = [$input_all['replacement_basic_info_id'], $input_all['direct_boss_basic_info_id'], $input_all['hr_basic_info_id']];
            $key_increment = 1;

            if (!$replacementExistFlag) {
                $confirm_basic_info_ids = [$input_all['direct_boss_basic_info_id'], $input_all['hr_basic_info_id']];
                $key_increment = 2;
            }

            foreach ($confirm_basic_info_ids as $key => $confirm_basic_info_id) {

                $confirmX = new Confirm;
                $confirmX->basic_info_id = $confirm_basic_info_id;
                $confirmX->type_confirm = $request['type_confirm_' . ($key + $key_increment)];
                $confirmX->assignment_type =  config('constants.assignment_type')[9];
                $confirmX->status = 0;
                $confirmX->created_by = auth()->id();

                $confirmX->save();

                $this->formDayOffConfirmRepository->create([
                    'form_day_off_id'   =>  $formDayOff->id,
                    'confirm_id'        =>  $confirmX->id
                ]);
                $confirm_id[] = $confirmX->id;
            }

            if ($creatorConfirmFlag) {
                $formDayOff->status = 2;
                if (!$replacementExistFlag)
                    $formDayOff->status = 3;
                $user_noti_target = User::where('basic_info_id', $confirm_basic_info_ids[0])->first();
                $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                $link = route('FormDayOffView', $formDayOff->id);
                $this->sendNotification($user_noti_target, $formDayOff->id, $link, $message);
            }


            $formDayOff->confirm_id = $confirm_id;
            $formDayOff->detail_id  = $detail_id;
            $formDayOff->total_day_off_asked_for = $total_day_off_asked_for;

            $formDayOff->save();

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateDataNew($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $update_input_all = $request->except('_token');

            for ($i = 1; $i <= $update_input_all['index_time_frame']; $i++) {
                if (array_key_exists('start_date' . $i, $update_input_all) && !$update_input_all['start_date' . $i]) {
                    $result['message'] = 'Lỗi: Không Có Ngày Bắt Đầu ' . $i;
                    return $result;
                }

                if (array_key_exists('end_date' . $i, $update_input_all) && !$update_input_all['end_date' . $i]) {
                    $result['message'] = 'Lỗi Không Có Ngày Kết Thúc ' . $i;
                    return $result;
                }

                if (array_key_exists('start_time' . $i, $update_input_all) && !$update_input_all['start_time' . $i]) {
                    $result['message'] = 'Lỗi Không Có Giờ Bắt Đầu ' . $i;
                    return $result;
                }

                if (array_key_exists('end_time' . $i, $update_input_all) && !$update_input_all['end_time' . $i]) {
                    $result['message'] = 'Lỗi Không Có Giờ Kết Thúc ' . $i;
                    return $result;
                }
                if (array_key_exists('type' . $i, $update_input_all) && $update_input_all['type' . $i] == 0) {
                    $result['message'] = 'Lỗi Không Có Loại Nghỉ Phép ' . $i;
                    return $result;
                }
                if (array_key_exists('reason' . $i, $update_input_all) && $update_input_all['reason' . $i] == 0) {
                    $result['reason'] = 'Lỗi Không Có Lý Do ' . $i;
                    return $result;
                }
            }



            $form_day_off_update_input = [
                "basic_info_id"           => $update_input_all['basic_info_id_of_form'],
                "detail_id"               => [],
                'status'                  => 1,
                "updated_by"              => auth()->id(),
            ];

            $form_day_off_id = $update_input_all['form_day_off_id'];

            FormDayOffDetail::where('form_day_off_id', $form_day_off_id)->delete();

            $total_day_off_asked_for = 0;

            for ($i = 1; $i <= intval($update_input_all['index_time_frame']); $i++) {
                if (array_key_exists('start_date' . $i, $update_input_all)) {
                    $total_day_off_asked_for += floatval($update_input_all['total_day_off_in_timeframe' . $i]);
                    $form_day_off_detail_create_input = [
                        "form_day_off_id"            => $form_day_off_id,
                        "start_date"                 => $update_input_all['start_date' . $i],
                        "start_time"                 => $update_input_all['start_time' . $i],
                        "end_date"                   => $update_input_all['end_date' . $i],
                        "end_time"                   => $update_input_all['end_time' . $i],
                        "reason"                     => $update_input_all['reason' . $i],
                        "total_day_off_in_timeframe" => floatval($update_input_all['total_day_off_in_timeframe' . $i]),
                        "type"                       => $update_input_all['type' . $i],
                        "created_by"                 => auth()->id(),
                        "delete_flag"                => 0
                    ];

                    $formDayOffDetail = $this->formDayOffDetailRepository->create($form_day_off_detail_create_input);

                    $form_day_off_update_input['detail_id'][] = $formDayOffDetail->id;
                }
            }

            $form_day_off_update_input['total_day_off_asked_for'] = $total_day_off_asked_for;

            $replacementExistFlag = $update_input_all['confirm_basic_info_ids'][0] != 0;
            $creatorConfirmFlag = array_key_exists("assign", $update_input_all);

            $confirm_basic_info_ids = $update_input_all['confirm_basic_info_ids'];
            if (!$replacementExistFlag)
                $confirm_basic_info_ids = array_slice($update_input_all['confirm_basic_info_ids'], 1);

            $confirm_ids = $update_input_all['confirm_ids'];


            $new_confirm_ids = [];
            if (count($confirm_basic_info_ids) > count($confirm_ids)) {
                $confirmX = new Confirm;
                $confirmX->basic_info_id = $confirm_basic_info_ids[0];
                $confirmX->type_confirm =  7;
                $confirmX->assignment_type =  config('constants.assignment_type')[9];
                $confirmX->status = 0;
                $confirmX->created_by = auth()->id();
                $confirmX->save();

                $this->formDayOffConfirmRepository->create([
                    'form_day_off_id'   =>  $form_day_off_id,
                    'confirm_id'        =>  $confirmX->id
                ]);
                $new_confirm_ids[] = $confirmX->id;

                $confirm_basic_info_ids = array_slice($confirm_basic_info_ids, 1);
            }

            if (count($confirm_ids) > count($confirm_basic_info_ids)) {
                Confirm::destroy($confirm_ids[0]);
                $confirm_ids = array_slice($confirm_ids, 1);
            }

            foreach ($confirm_ids as $key => $confirm_id) {
                $old_confirm = Confirm::where('id', $confirm_id)->first();

                $confirm_update_input = [];

                if ($old_confirm->basic_info_id != $confirm_basic_info_ids[$key])
                    $confirm_update_input =  [
                        'basic_info_id' => $confirm_basic_info_ids[$key],
                        'status'        => 0,
                        'note'          => null,
                        'updated_by'    => auth()->id()
                    ];

                if ($creatorConfirmFlag) {
                    $confirm_update_input['note']        = null;
                    $confirm_update_input['status']      = 0;
                    $confirm_update_input['updated_by']  = auth()->id();
                }

                $this->confirmRepository->update($confirm_id, $confirm_update_input);
                $new_confirm_ids[] = intval($confirm_id);
            }

            $form_day_off_update_input['confirm_id'] = $new_confirm_ids;

            if ($creatorConfirmFlag) {
                $form_day_off_update_input['status'] = 2;
                if (!$replacementExistFlag)
                    $form_day_off_update_input['status'] = 3;
                $user_noti_target = User::where('basic_info_id', $confirm_basic_info_ids[0])->first();
                $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                $link = route('FormDayOffView', $form_day_off_id);
                $this->sendNotification($user_noti_target, $form_day_off_id, $link, $message);
            }

            $this->formDayOffRepository->update($form_day_off_id, $form_day_off_update_input);

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function saveData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {

            $input_all = $request->except('_token');

            $creatorConfirmFlag = array_key_exists("assign", $input_all);

            $confirm_id = [];

            for ($i = 1; $i <= $input_all['index_time_frame']; $i++) {
                if (array_key_exists('start_date' . $i, $input_all) && !$input_all['start_date' . $i]) {
                    $result['message'] = 'Lỗi: Không Có Ngày Bắt Đầu ' . $i;
                    return $result;
                }

                if (array_key_exists('end_date' . $i, $input_all) && !$input_all['end_date' . $i]) {
                    $result['message'] = 'Lỗi Không Có Ngày Kết Thúc ' . $i;
                    return $result;
                }

                if (array_key_exists('start_time' . $i, $input_all) && !$input_all['start_time' . $i]) {
                    $result['message'] = 'Lỗi Không Có Giờ Bắt Đầu ' . $i;
                    return $result;
                }

                if (array_key_exists('end_time' . $i, $input_all) && !$input_all['end_time' . $i]) {
                    $result['message'] = 'Lỗi Không Có Giờ Kết Thúc ' . $i;
                    return $result;
                }
                if (array_key_exists('type' . $i, $input_all) && $input_all['type' . $i] == 0) {
                    $result['message'] = 'Lỗi Không Có Loại Nghỉ Phép ' . $i;
                    return $result;
                }
            }




            $replacementExistFlag = $input_all['replacement_basic_info_id'] != 0;

            $confirm_basic_info_ids = [$input_all['replacement_basic_info_id'], $input_all['direct_boss_basic_info_id'], $input_all['hr_basic_info_id']];
            $key_increment = 1;

            if (!$replacementExistFlag) {
                $confirm_basic_info_ids = [$input_all['direct_boss_basic_info_id'], $input_all['hr_basic_info_id']];
                $key_increment = 2;
            }


            $form_day_off_create_input = [
                "basic_info_id"           => $input_all['basic_info_id_of_form'],
                "confirm_id"              => [],
                "start_date"              => $input_all['start_date'],
                "start_time"              => $input_all['start_time'],
                "end_date"                => $input_all['end_date'],
                "end_time"                => $input_all['end_time'],
                "total_day_off_asked_for" => floatval($input_all['total_day_off_asked_for']),
                "type"                    => $input_all['type'],
                "reason"                  => $input_all['reason'],
                'status'                  => 1,
                "created_by"              => auth()->id(),
                "delete_flag"             => 0

            ];

            $formDayOff = $this->formDayOffRepository->create($form_day_off_create_input);

            foreach ($confirm_basic_info_ids as $key => $confirm_basic_info_id) {

                $confirmX = new Confirm;
                $confirmX->basic_info_id = $confirm_basic_info_id;
                $confirmX->type_confirm = $request['type_confirm_' . ($key + $key_increment)];
                $confirmX->assignment_type =  config('constants.assignment_type')[9];
                $confirmX->status = 0;
                $confirmX->created_by = auth()->id();

                $confirmX->save();

                $this->formDayOffConfirmRepository->create([
                    'form_day_off_id'   =>  $formDayOff->id,
                    'confirm_id'        =>  $confirmX->id
                ]);
                $confirm_id[] = $confirmX->id;
            }

            if ($creatorConfirmFlag) {
                $formDayOff->status = 2;
                if (!$replacementExistFlag)
                    $formDayOff->status = 3;
                $user_noti_target = User::where('basic_info_id', $confirm_basic_info_ids[0])->first();
                $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                $link = route('FormDayOffView', $formDayOff->id);
                $this->sendNotification($user_noti_target, $formDayOff->id, $link, $message);
            }



            $formDayOff->confirm_id = $confirm_id;
            $formDayOff->save();

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';


            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $update_input_all = $request->except('_token');

            $form_day_off_id = $update_input_all['form_day_off_id'];
            $replacementExistFlag = $update_input_all['confirm_basic_info_ids'][0] != 0;
            $creatorConfirmFlag = array_key_exists("assign", $update_input_all);

            $confirm_basic_info_ids = $update_input_all['confirm_basic_info_ids'];
            if (!$replacementExistFlag)
                $confirm_basic_info_ids = array_slice($update_input_all['confirm_basic_info_ids'], 1);

            $confirm_ids = $update_input_all['confirm_ids'];

            $form_day_off_update_input = [
                "basic_info_id"           => $update_input_all['basic_info_id_of_form'],
                "start_date"              => $update_input_all['start_date'],
                "start_time"              => $update_input_all['start_time'],
                "end_date"                => $update_input_all['end_date'],
                "end_time"                => $update_input_all['end_time'],
                "total_day_off_asked_for" => floatval($update_input_all['total_day_off_asked_for']),
                "type"                    => $update_input_all['type'],
                "reason"                  => $update_input_all['reason'],
                'status'                  => 1,
                "updated_by"              => auth()->id(),
            ];

            $new_confirm_ids = [];
            if (count($confirm_basic_info_ids) > count($confirm_ids)) {
                $confirmX = new Confirm;
                $confirmX->basic_info_id = $confirm_basic_info_ids[0];
                $confirmX->type_confirm =  7;
                $confirmX->assignment_type =  config('constants.assignment_type')[9];
                $confirmX->status = 0;
                $confirmX->created_by = auth()->id();
                $confirmX->save();

                $this->formDayOffConfirmRepository->create([
                    'form_day_off_id'   =>  $form_day_off_id,
                    'confirm_id'        =>  $confirmX->id
                ]);
                $new_confirm_ids[] = $confirmX->id;

                $confirm_basic_info_ids = array_slice($confirm_basic_info_ids, 1);
            }

            if (count($confirm_ids) > count($confirm_basic_info_ids)) {
                Confirm::destroy($confirm_ids[0]);
                $confirm_ids = array_slice($confirm_ids, 1);
            }

            foreach ($confirm_ids as $key => $confirm_id) {
                $old_confirm = Confirm::where('id', $confirm_id)->first();

                $confirm_update_input = [];

                if ($old_confirm->basic_info_id != $confirm_basic_info_ids[$key])
                    $confirm_update_input =  [
                        'basic_info_id' => $confirm_basic_info_ids[$key],
                        'status'        => 0,
                        'note'          => null,
                        'updated_by'    => auth()->id()
                    ];

                if ($creatorConfirmFlag) {
                    $confirm_update_input['note']        = null;
                    $confirm_update_input['status']      = 0;
                    $confirm_update_input['updated_by']  = auth()->id();
                }

                $this->confirmRepository->update($confirm_id, $confirm_update_input);
                $new_confirm_ids[] = intval($confirm_id);
            }

            $form_day_off_update_input['confirm_id'] = $new_confirm_ids;

            if ($creatorConfirmFlag) {
                $form_day_off_update_input['status'] = 2;
                if (!$replacementExistFlag)
                    $form_day_off_update_input['status'] = 3;
                $user_noti_target = User::where('basic_info_id', $confirm_basic_info_ids[0])->first();
                $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                $link = route('FormDayOffView', $form_day_off_id);
                $this->sendNotification($user_noti_target, $form_day_off_id, $link, $message);
            }
            $this->formDayOffRepository->update($form_day_off_id, $form_day_off_update_input);

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateStatus($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $update_status_input = $request->except('_token');
            $new_data = [];
            $confirm_flag = ($update_status_input['confirm_flag'] == 1);
            $link = route('FormDayOffView', $update_status_input['form_day_off_id']);

            $formDayOff_X = FormDayOff::where('id', $update_status_input['form_day_off_id'])->first();

            $noti_targets = [User::where('id', $formDayOff_X->user->id)->first()];
            $noti_message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
            $noti_targets_sent_user_ids = [auth()->id()];

            if ($update_status_input['next_confirm_basic_info_id'] && $confirm_flag) {
                $user_to_notify = User::where('basic_info_id', $update_status_input['next_confirm_basic_info_id'])->first();
                $noti_message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                $noti_targets = [$user_to_notify];
            }

            switch ($formDayOff_X->status) {
                case (2):

                    $new_data['status'] = $confirm_flag ? 4 : 7;
                    break;
                case (3):
                case (4):
                    $new_data['status'] = $confirm_flag ? 5 : 8;
                    break;
                case (5):
                    $new_data['status'] = $confirm_flag ? 6 : 9;

                    $noti_targets[] = User::where('basic_info_id', $formDayOff_X->basic_info_id)->first();

                    foreach ($formDayOff_X->confirms as $formDayOffConfirm)
                        $noti_targets[] = User::where('basic_info_id', $formDayOffConfirm->confirm->staff->id)->first();

                    if ($confirm_flag) {
                        $new_data['review_date'] = Carbon::now()->format('Y-m-d');
                        $noti_message = 'Thông báo !! Yêu cầu đã được duyệt bởi ' . auth()->user()->name;
                        $this->updateTimeKeep($formDayOff_X);
                        $this->checkOtherConfirmedForms($formDayOff_X);
                    }
                    break;
            }


            foreach ($noti_targets as $noti_target)
                if (!in_array($noti_target->id, $noti_targets_sent_user_ids)) {
                    $this->sendNotification($noti_target, $formDayOff_X->id, $link, $noti_message);
                    $noti_targets_sent_user_ids[] = $noti_target->id;
                }

            $new_data['updated_by'] = auth()->id();

            $this->formDayOffRepository->update($formDayOff_X->id, $new_data);
            $this->confirmRepository->update($update_status_input['confirm_id_to_update_status'], [
                'status'     => $update_status_input['confirm_flag'],
                'note'       => $update_status_input['reason'],
                'updated_by' => auth()->id(),
            ]);

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
