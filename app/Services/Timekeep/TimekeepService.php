<?php

namespace App\Services\Timekeep;

use App\Models\Timekeeping;
use App\Repositories\Organizational\OrganizationalRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\Salary\SalaryRepository;
use App\Repositories\Timekeep\TimekeepRepository;
use App\Services\BaseService;
use App\Services\Timekeep\Traits\AddEdit;
use App\Services\Timekeep\Traits\Delete;
use App\Services\Timekeep\Traits\Search;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TimekeepService extends BaseService
{
    use Search, AddEdit, Delete;
    public $organizationalRepository;

    public function __construct(
        OrganizationalRepository $organizationalRepository,
        RoomRepository $roomRepository,
        TimekeepRepository $timekeepRepository,
        SalaryRepository $salaryRepository
    ) {
        $this->organizationalRepository = $organizationalRepository;
        $this->roomRepository = $roomRepository;
        $this->timekeepRepository = $timekeepRepository;
        $this->salaryRepository = $salaryRepository;
    }

    public function calculateTimekeep($id)
    {
        DB::beginTransaction();
        try {
            $timeKeepObjectForm = Timekeeping::where('id', $id)->first();

            $paid_work_day = 0;
            $pto_day       = 0;
            $non_paid_day  = 0;
            $other_day     = $timeKeepObjectForm -> other;

            if ($timeKeepObjectForm) {

                $month = $timeKeepObjectForm->month;
                $year = $timeKeepObjectForm->year;

                $timeKeepArrayForm = $timeKeepObjectForm->toArray();

                $startDate = Carbon::createFromFormat('m-Y', $month . "-" . $year)->startOfMonth();
                $endDate = Carbon::createFromFormat('m-Y', $month . "-" . $year)->endOfMonth();

                $rangeDate = CarbonPeriod::create($startDate,  $endDate);

                foreach ($rangeDate as $key => $date) {
                    $timeKeepDayType = $timeKeepArrayForm['day_' . $key + 1];

                    switch ($timeKeepDayType) {
                        case 1:
                            $paid_work_day += 0.5;
                            break;
                        case 2:
                            $paid_work_day += 1.0;
                            break;
                        case 3:
                            $pto_day += 0.5;
                            $paid_work_day += 0.5;
                            break;
                        case 4:
                            $paid_work_day += 1.0;
                            break;
                        case 5:
                            $pto_day += 1.0;
                            break;
                        case 6:
                            $non_paid_day += 1.0;
                            break;
                        case 7:
                            $non_paid_day += 0.5;
                            break;
                        case 8:
                            $paid_work_day += 1.0;
                            break;
                        case 9:
                            break;
                        case 10:
                            $pto_day += 0.5;
                            break;
                        case 11:
                            $pto_day += 0.5;
                            $paid_work_day += 0.5;
                            break;
                        case 12:
                            $non_paid_day += 0.5;
                            $paid_work_day += 0.5;
                            break;
                        case 13:
                            $pto_day += 0.5;
                            $non_paid_day += 0.5;
                            break;

                        case 14:
                            break;
                    }
                }

                $this->timekeepRepository->update($id, [
                    'with_salary'   => $paid_work_day,
                    'paid_holidays' => $pto_day,
                    'no_salary'     => $non_paid_day,
                    'other'         => $other_day,
                    'total_salary'  => $paid_work_day + $pto_day + $other_day,
                    'updated_by'    => auth()->id(),
                ]);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }
    }
}
