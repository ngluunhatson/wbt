<?php

namespace App\Services\Timekeep\Traits;

use App\Models\SalaryList;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait Search
{
    public function getRooms()
    {
        $result = [];
        try {
            $rooms = $this->roomRepository->findAll();
            $result = !empty($rooms) ? $rooms : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataListByRoomAndCalculateTimeKeep($roomID, $month, $year)
    {
        $result = [];

        DB::beginTransaction();
        try {
            $data = [];

    
            if ($roomID != null) {
                $data = $this->timekeepRepository->loadRelation(['organizational', 'staff'])
                    ->where('month', $month)
                    ->where('year', $year)
                    ->where('delete_flag', 0);
                if ($roomID > 0)
                    $data = $data->where('room_id', $roomID);
            }

            foreach ($data as $timeKeep) {
                $timeKeep_id = $timeKeep->id;
                $this->calculateTimekeep($timeKeep_id);
            }

            if ($roomID != null) {
                $data = $this->timekeepRepository->loadRelation(['organizational', 'staff'])
                    ->where('month', $month)
                    ->where('year', $year)
                    ->where('delete_flag', 0);
                if ($roomID > 0)
                    $data = $data->where('room_id', $roomID);
            }

            DB::commit();
            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDataListByRoom($roomID, $month, $year)
    {
        $result = [];
        try {

            $data = $this->timekeepRepository->loadRelation(['organizational', 'staff', 'salary'])
                ->where('month', $month)
                ->where('year', $year)
                ->where('delete_flag', 0);

            if ($roomID != -1)
                $data = $data->where('organizational.room_id', $roomID);

            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getMySalary($month, $year)
    {
        $result = [];
        try {

            $salaryListConfirmed = SalaryList::with(['salaries'])->where('month', $month)->where('year', $year)->where('status', 5)->first();

            if ($salaryListConfirmed) {
                $result = $salaryListConfirmed->salaries->where('basic_info_id', auth()->user()->basic_info_id)->first();
            }
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function adminGetPayslips($roomID, $month, $year)
    {
        $result = [];
        try {

            $salaryList = SalaryList::with(['salaries', 'salaries.staff', 'salaries.staff.organizational', 'salaries.timekeep'])
                ->where('month', $month)->where('year', $year)->where('status', 5) ->first();
            
            $data = $salaryList -> salaries;
           
            if ($roomID != -1)
                $data = $data->where('organizational.room_id', $roomID);

            $result = !empty($data) ? $data : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
