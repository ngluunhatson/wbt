<?php

namespace App\Services\Timekeep\Traits;


use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait AddEdit
{
   

    public function updateStatusDay($input, $id, $dayId)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $this->timekeepRepository->update($id, [
                'day_' . $dayId => $input['newValue'] + 1
            ]);
            // $dayCreate = Carbon::createFromFormat('m/Y', $input['monthYear']);

            $this->calculateTimekeep($id);           
            $listIdSalary = $this->salaryRepository->getIdByTimekeep($id);
            $work_salary = $listIdSalary['timekeep']['total_salary'];
            $work_number = $listIdSalary['timekeep']['total_salary'] + $listIdSalary['timekeep']['no_salary'];
            $newValue = -99;
            if (!empty($listIdSalary)) {
                $salary_id = $listIdSalary['id'];
                $income_id = $listIdSalary['income']['id'];
                $support_id = $listIdSalary['support']['id'];
                $allowance_id = $listIdSalary['allowance']['id'];
                $insurance_id = $listIdSalary['insurance']['id'];
                $tax_id = $listIdSalary['tax']['id'];
                $pay_id = $listIdSalary['pay']['id'];
                DB::select("CALL calculateSalary_OtherSupport($newValue, $salary_id, $support_id, $income_id, $pay_id, $work_number, $work_salary)");
                DB::select("CALL updateSupport_salary($newValue, $work_number, $work_salary, 
                            $support_id, $salary_id, $income_id, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                DB::select("CALL updateSupport_supportEat($newValue, $work_number, $work_salary, 
                            $support_id, $salary_id, $income_id, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                DB::select("CALL updateSupport_supportUniform($newValue, $work_number, $work_salary, 
                            $salary_id, $support_id, $income_id, $allowance_id, $insurance_id, $tax_id, $pay_id)");
                DB::select("CALL updateSupport_jobPerformance($newValue, $work_number, $work_salary, 
                            $salary_id, $support_id, $income_id, $allowance_id, $insurance_id, $tax_id, $pay_id)");
            }
            // DB::select("call calculateTimeKeep($dayCreate->month, $dayCreate->year, $id)");
            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateTimeKeepOtherFields($input) {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];
        DB::beginTransaction();
        try {

            $this->timekeepRepository->update($input['timeKeepId'], [
                $input["fieldName"] => $input["newValue"]
            ]);

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';


            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
