<?php

namespace App\Services\Comment;
use App\Repositories\Comment\CommentRepository;
use App\Repositories\Comment\ReplyCommentRepository;
use App\Services\BaseService;
use App\Services\Comment\Traits\AddEdit;
use App\Services\Comment\Traits\Delete;
use App\Services\Comment\Traits\Search;

class CommentService extends BaseService
{

    use Search, AddEdit, Delete;

    public $commentRepo;
    public $replyCommentRepo;

    public function __construct(
        CommentRepository $commentRepository,
        ReplyCommentRepository $replyCommentRepository,
    ) {
       $this->commentRepo = $commentRepository;
       $this->replyCommentRepo = $replyCommentRepository;
    }
}
