<?php

namespace App\Services\Comment\Traits;

use App\Events\NotificationEvent;
use App\Models\User;
use App\Notifications\AssignPerson;
use Arr;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Request;
use Illuminate\Support\Facades\Validator;

trait AddEdit
{
    public function saveData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Comment không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $dataComment = $request->except('_token');
            $request->validate([
                'content'   =>  'required',
            ]);
      
            // $dataComment['blog_id'] = $blogId;
            $dataComment['user_id'] = auth()->id();
            $comment = $this->commentRepo->create($dataComment);

            $comment->save();
            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function saveDataReplyComment($request, $commentId)
    {
        $result = [
            'status' => false,
            'message' => 'Reply Comment không thành công !!',
        ];

        DB::beginTransaction();
        try {
            $dataComment = $request->except('_token', 'files');
            $request->validate([
                'content'   =>  'required',
            ]);
      
            $dataComment['comment_id'] = $commentId;
            $dataComment['user_id'] = auth()->id();
            $comment = $this->replyCommentRepo->create($dataComment);

            $comment->save();
            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            return $e->getMessage();
            DB::rollBack();

            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

}
