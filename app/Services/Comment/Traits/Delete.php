<?php

namespace App\Services\Comment\Traits;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


trait Delete
{
    public function delete($id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa không thành công !!',
        ]; 

        DB::beginTransaction();
        try {
            $query = $this->commentRepo->delete($id);
            if ($query)
            {
                $result['status'] = true;
                $result['message'] = 'Xóa thành công !!';
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function deleteReplyComment($id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa không thành công !!',
        ]; 

        DB::beginTransaction();
        try {
            $query = $this->replyCommentRepo->delete($id);
            if ($query)
            {
                $result['status'] = true;
                $result['message'] = 'Xóa thành công !!';
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}