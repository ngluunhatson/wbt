<?php

namespace App\Services\Comment\Traits;

use Exception;
use Illuminate\Support\Facades\Log;

trait Search
{


    public function getComment()
    {
        $result = [];
        try {
            $comments = $this->commentRepo->loadRelation(['user', 'replyComment']);
            return $comments;
            die();
            $result = !empty($comments) ? $comments  : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
