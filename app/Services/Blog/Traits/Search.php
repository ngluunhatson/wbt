<?php

namespace App\Services\Blog\Traits;

use App\Models\Blog;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Models\Organizational;

trait Search
{

    // public function getDataList()
    // {
    //     $result = [];
    //     try {
    //         $data = $this->criteriaRepository->loadRelation(['room', 'team', 'position', 'user', 'confirms.confirmReceive'])->where('delete_flag', 0);
    //         $result = !empty($data) ? $data : [];
    //     } catch (Exception $e) {
    //         Log::channel('daily')->error($e->getMessage());
    //     }

    //     return $result;
    // }

    public function getCategories()
    {
        $result = [];
        try {
            $categories = $this->categoryRepo->findAll();
            $result = !empty($categories) ? $categories : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }


    public function getBlogs()
    {
        $result = [];
        try {
            $blogs = $this->blogRepo->loadRelation(['category'])->where('delete_flag', 0);
            $result = !empty($blogs) ? $blogs : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getBlogById($id)
    {
        $result = [];
        try {
            $blog = $this->blogRepo->loadRelation(['category'])->find($id);
            $result = !empty($blog) ? $blog : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }


    public function getBlogsByCategory($slug, $category)
    {
        $result = [];
        try {
            $categoryId = $this->categoryRepo->findFirst(['category_slug' => '/' . $slug])['id'];
            $blogs = $this->blogRepo->getBlogsByCategory($categoryId,'')->orderBy('created_at', 'desc')->paginate(9);
            foreach ($blogs as $blog) {
                $blog->link = route('blog.viewPostDetail', [
                    'category' => $category,
                    'id' => $blog->id
                ]);
            }
            $result = !empty($blogs) ? $blogs : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getCommentByBlogId($id)
    {
        $result = [];
        try {
            $comment = $this->commentRepo->loadRelation(['user', 'replyComment'])->where('blog_id', $id)->where('delete_flag', 0);
            $result = !empty($comment) ? $comment : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getDetailBlog($slug, $id, $category)
    {
        $result = [];
        try {
            $categoryId = $this->categoryRepo->findFirst(['category_slug' => '/' . $slug])['id'];
            $blog = $this->blogRepo->loadRelation(['category'])->find($id);
            $recommendBlogs = $this->blogRepo->loadRelation(['category'])->where('category_id', $categoryId)->where('id', '!=', $id);
            foreach ($recommendBlogs as $recommend) {
                $recommend->link = route('blog.viewPostDetail', [
                    'category' => $category,
                    'id' => $recommend->id
                ]);
            }

            $result = !empty($blog) ? $blog : [];
            $result['recommend'] = $recommendBlogs;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function getPostWithCondition($slug, $category, $request)
    {
        $result = [];
        try {
            $search = $request->except('_token');
            // dd($search);

            $categoryId = $this->categoryRepo->findFirst(['category_slug' => '/' . $slug])['id'];
            // dd($categoryId);
            $blogs = $this->blogRepo->getBlogsByCategory($categoryId, $search)->orderBy('created_at', 'desc')->paginate(9);
            // $blogs = $blogs->where('id','=',26)->get();
            return $blogs;
            // foreach ($blogs as $blog) {
            //     if (str_contains($blog->title, $search['searchPost'])) {
            //         $blog->link = route('blog.viewPostDetail', [
            //             'category' => $category,
            //             'id' => $blog->id
            //         ]);
            //         array_push($result, $blog);
            //     }
            // }

            // $result = !empty($blogs) ? $blogs : [];
            // $result = !empty($blogs) ? $blogs : [];


        } catch (Exception $e) {
            return $e->getMessage();
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function getPostWithDayCondition($slug, $category, $request)
    {
        $result = [];
        try {
            $search = $request->except('_token');
            // dd($search);
            
            $categoryId = $this->categoryRepo->findFirst(['category_slug' => '/' . $slug])['id'];
            // dd($categoryId);
            $blogs = $this->blogRepo->getBlogsByCategory($categoryId, $search, $filter)->orderBy('created_at', 'desc')->paginate(9);
            // $blogs = $blogs->where('id','=',26)->get();
            return $blogs;
            // foreach ($blogs as $blog) {
            //     if (str_contains($blog->title, $search['searchPost'])) {
            //         $blog->link = route('blog.viewPostDetail', [
            //             'category' => $category,
            //             'id' => $blog->id
            //         ]);
            //         array_push($result, $blog);
            //     }
            // }

            // $result = !empty($blogs) ? $blogs : [];
            // $result = !empty($blogs) ? $blogs : [];


        } catch (Exception $e) {
            return $e->getMessage();
            Log::channel('daily')->error($e->getMessage());
        }
        return $result;
    }

    public function getRooms()
    {
        $result = [];
        try {
            $rooms = $this->roomRepo->findAll();
            $result = !empty($rooms) ? $rooms : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getTeams()
    {
        $result = [];
        try {
            $teams = $this->teamRepo->findAll();
            $result = !empty($teams) ? $teams : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getTeamsByRoomID($room_id)
    {
        $result = [];
        try {
            $teams = $this->teamRepo->findByCondition([
                'room_id' => $room_id,
            ]);
            $result = !empty($teams) ? $teams : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function checkRuleAccessDetailBlog($blog_id)
    {
        // Check Rule access BLOG WHEN:
        // CASE 1: All companies can see it
        // CASE 2: Only the room and all members of the room can see
        // CASE 3: Only team and team members can see
        // CASE 4: Only position and position members can see
        // CASE 5: Only one person can see it

        try {
            $blog = $this->blogRepo->getById($blog_id);
            if ($blog['all_company'] == 1)
                return true;

            if (
                $blog['all_company'] == 0 && $blog['room_id'] == auth()->user()->organizational->room_id
                && $blog['team_id'] == 0
            )
                return true;

            if (
                $blog['all_company'] == 0 && $blog['team_id'] == auth()->user()->organizational->team_id
                && $blog['room_id'] == auth()->user()->organizational->room_id && $blog['position_id'] == 0
            )
                return true;

            if (
                $blog['all_company'] == 0 && $blog['team_id'] == auth()->user()->organizational->team_id
                && $blog['room_id'] == auth()->user()->organizational->room_id && $blog['position_id'] == auth()->user()->organizational->position_id
                && $blog['basic_info_id'] == 0
            )
                return true;

            if( $blog['all_company'] == 0 && $blog['basic_info_id'] == auth()->user()->organizational->basic_info_id)
            return true;    
            
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }
        return false;
    }

    public function getPositionByTeamID($team_id)
    {
        $result = [];
        try {
            $positions = $this->positionRepo->findByCondition([
                'team_id' => $team_id,
            ]);
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getStaffByConditions($room_id, $team_id, $position_id)
    {
        $result = [];
        try {
            $organizationals = Organizational::where([
                'room_id' => $room_id,
                'team_id' => $team_id,
                'position_id' => $position_id,
                'delete_flag' => 0,
            ])->get();
            $staffs = [];
            foreach ($organizationals as $organizational)

                $staffs[] = $organizational->staff;

            $result = !empty($staffs) ? $staffs : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return ($result);
    }
}
