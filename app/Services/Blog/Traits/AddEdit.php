<?php

namespace App\Services\Blog\Traits;

use App\Events\NotificationEvent;
use App\Models\User;
use App\Notifications\AssignPerson;
use Arr;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Request;
use Illuminate\Support\Facades\Validator;

trait AddEdit
{
    public function saveData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Lưu không thành công !!',
        ];
        DB::beginTransaction();
        try {
            $dataBlog = $request->except('_token');
            $request->validate([
                'category_id'   =>  'integer|required',
                'title'   =>  'required',
                'content'   =>  'required',
                'status'   =>  'integer|in:0,1',
                'tag'   =>  'required',
                'author'   =>  'required',
                'blog_image'   =>  'mimes:svg,png,jpeg,jpg|max:2048'
            ]);

            $input_create_blog = [
                'title' =>  $dataBlog["title"],
                'category_id' => $dataBlog["category_id"],
                'content' => $dataBlog["content"],
                'tag' => $dataBlog["tag"],
                'author' => $dataBlog["author"],
                'status' => $dataBlog["status"],
                'room_id' => $dataBlog["room_id"]                ??  null,
                'team_id' => $dataBlog["team_id"]                ??  null,
                'position_id' => $dataBlog["position_id"]        ??  null,
                'basic_info_id' =>  $dataBlog["basic_info_id"]   ??  null,
                'all_company' =>  isset($dataBlog["all_company"])   ?  1 : 0,
                'created_by' => auth()->id(),
            ];

            $blog = $this->blogRepo->create($input_create_blog);
            if ($request->hasFile('blog_image')) {

                // Get image file
                $blog_image_file = $request->file('blog_image');

                // Folder path
                $folder = 'uploads/img/blogs/' . $blog->id . '/';

                // Make image name
                $blog_image_name = time() . '-' . $blog_image_file->getClientOriginalName();

                // Original size upload file
                $blog_image_file->move($folder, $blog_image_name);

                // Set input
                $blog->blog_image = $folder . $blog_image_name;
            }
            $blog->save();


            $message = 'Một Bài Blog Mới Đã Đuợc Thêm!!!';

            $link = route('blog.viewPostDetail', [$blog->category_id, $blog->id]);

            $users = User::all();

            $id = $blog->id;

            foreach ($users as $user) {
                if ($blog->all_company == 1) {
                    $this->sendNotification($user, $id, $link, $message);
                } else if ($blog->all_company != 1 && $blog->team_id == 0 && $blog->room_id == $user->organizational->room_id) {
                    $this->sendNotification($user, $id, $link, $message);
                } else if($blog->all_company != 1 && $blog->team_id == $user->organizational->team_id && $blog->room_id == $user->organizational->room_id && $blog->position_id == 0) {
                    $this->sendNotification($user, $id, $link, $message);
                } else if($blog->all_company != 1 && $blog->team_id == $user->organizational->team_id && $blog->room_id == $user->organizational->room_id && $blog->position_id == $user->organizational->position_id && $blog->basic_info_id == 0)
                {
                    $this->sendNotification($user, $id, $link, $message);
                } else if($blog->all_company != 1 && $blog->basic_info_id == $user->organizational->basic_info_id) {
                    $this->sendNotification($user, $id, $link, $message);
                }

            }

            // $this->sendNotification($user, $id, $link, $message);

            $result['status'] = true;
            $result['message'] = 'Lưu thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateData($request)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhật không thành công !!',
        ];
        $request->validate([
            'category_id'   =>  'integer|required',
            'title'   =>  'required',
            'content'   =>  'required',
            'status'   =>  'integer|in:0,1',
            'blog_image'   =>  'mimes:svg,png,jpeg,jpg|max:2048'
        ]);

        DB::beginTransaction();
        try {
            $dataUpdate = $request->except(
                '_token',
                'id'
            );
            $dataUpdate['updated_by'] = auth()->id();
            // $criteriaOld = $this->criteriaRepository->getByID($request['criteria_id']);
            // if ($criteriaOld['status'] == 5 || $criteriaOld['status'] == 6 || (array_key_exists('assign', $request->all()) && $request['assign'])) {
            //     $dataCrieria['status'] = 2;
            // }
            // $criteria = $this->criteriaRepository->update($request['criteria_id'], $dataUpdate);
            // $id = int() ;

            $input_update_blog = [
                'title' =>  $dataUpdate["title"],
                'category_id' => $dataUpdate["category_id"],
                'content' => $dataUpdate["content"],
                'tag' => $dataUpdate["tag"],
                'author' => $dataUpdate["author"],
                'status' => $dataUpdate["status"],
                'room_id' => $dataUpdate["room_id"]                ??  null,
                'team_id' => $dataUpdate["team_id"]                ??  null,
                'position_id' => $dataUpdate["position_id"]        ??  null,
                'basic_info_id' =>  $dataUpdate["basic_info_id"]   ??  null,
                'all_company' =>  isset($dataUpdate["all_company"])   ?  1 : 0,
                'updated_by' => $dataUpdate['updated_by'],
            ];

            $blogOld = $this->blogRepo->getById($request['id']);
            $blog = $this->blogRepo->update($request['id'], $input_update_blog);
            if ($request->hasFile('blog_image')) {

                // Get image file
                $blog_image_file = $request->file('blog_image');

                // Folder path
                $folder = 'uploads/img/blogs/' . $request['id'] . '/';

                // Make image name
                $blog_image_name =  time() . '-' . $blog_image_file->getClientOriginalName();

                // Delete Image
                \File::delete(public_path($blogOld['blog_image']));

                // Original size upload file
                $blog_image_file->move($folder, $blog_image_name);

                // Set input
                $this->blogRepo->update($request['id'], [
                    'blog_image' => $folder . $blog_image_name,
                ]);
            }


            $result['status'] = true;
            $result['message'] = 'Cập nhật thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return $e->getMessage();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function updateStatus($request, $id)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhật không thành công !!',
        ];
        DB::beginTransaction();
        try {
            if ($request['statusConfirm'] == 1) {
                switch ($request['type']) {
                    case 2:
                        $this->criteriaRepository->update($id, [
                            'status' => 3,
                            'updated_by' => auth()->id()
                        ]);
                        if (!empty($request['confimerIDNext'])) {
                            $user = User::where('basic_info_id', $request['confimerIDNext'])->first();
                            $message = 'Thông báo !! Yêu cầu phê duyệt từ ' . auth()->user()->name;
                            $link = route('criteria.view', $id);
                            $this->sendNotification($user, $id, $link, $message);
                        }
                        break;
                    case 3:
                        $this->criteriaRepository->update($id, [
                            'status' => 4,
                            'review_date' => Carbon::now()->format('Y-m-d'),
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->criteriaRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        $message = 'Thông báo !! Yêu cầu đã được duyệt bởi ' . auth()->user()->name;
                        if ($data->user) {
                            $link = route('criteria.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        foreach ($data->confirms as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirmID']) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                $link = route('criteria.view', $id);
                                $this->sendNotification($user, $id, $link, $message);
                            }
                        }
                        break;
                }
            } else {
                switch ($request['type']) {
                    case 2:
                        $this->criteriaRepository->update($id, [
                            'status' => 5,
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->criteriaRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        if ($data->user) {
                            $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                            $link = route('criteria.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        break;
                    case 3:
                        $this->criteriaRepository->update($id, [
                            'status' => 6,
                            'updated_by' => auth()->id()
                        ]);
                        $data = $this->criteriaRepository->loadRelation(['user', 'confirms.confirm'])->where('id', $id)->where('confirms.confirm_id', '<>', $request['confirmID'])->first();
                        $message = 'Thông báo !! Yêu cầu đã bị từ chối bởi ' . auth()->user()->name;
                        if ($data->user) {
                            $link = route('criteria.view', $id);
                            $this->sendNotification($data->user, $id, $link, $message);
                        }
                        foreach ($data->confirms as $value) {
                            if ($value->confirm->basic_info_id != null && $value->confirm->id != $request['confirmID']) {
                                $user = User::where('basic_info_id', $value->confirm->basic_info_id)->first();
                                $link = route('criteria.view', $id);
                                $this->sendNotification($user, $id, $link, $message);
                            }
                        }
                        break;
                }
            }

            $this->confirmRepository->update($request['confirmID'], [
                'status' => $request['statusConfirm'],
                'note' => $request['reason'],
                'updated_by' => auth()->id()
            ]);

            $result['status'] = true;
            $result['message'] = 'Cập nhật thành công !!';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
