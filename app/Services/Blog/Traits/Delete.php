<?php

namespace App\Services\Blog\Traits;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


trait Delete
{
    public function delete($id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa không thành công !!',
        ]; 

        DB::beginTransaction();
        try {
            $blog = $this->blogRepo->getById($id);

            // Folder path
            $folder = 'uploads/img/blogs/' . $id;
            \File::deleteDirectory(public_path($folder));
            // Delete Image
            // \File::delete(public_path($blog['blog_image']));
            $query = $this->blogRepo->update($id, [
                'delete_flag' => 1
            ]);
            if ($query)
            {
                $result['status'] = true;
                $result['message'] = 'Xóa thành công !!';
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }
}
