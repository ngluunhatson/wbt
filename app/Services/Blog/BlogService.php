<?php

namespace App\Services\Blog;

use App\Models\Position;
use App\Repositories\Blog\BlogRepository;
use App\Repositories\Blog\CategoryRepository;
use App\Repositories\Comment\CommentRepository;
use App\Repositories\Room\RoomRepository;
use App\Repositories\Team\TeamRepository;
use App\Repositories\Position\PositionRepository;
use App\Services\BaseService;
use App\Services\Blog\Traits\AddEdit;
use App\Services\Blog\Traits\Delete;
use App\Services\Blog\Traits\Search;

class BlogService extends BaseService
{
    use Search, AddEdit, Delete;
    public $blogRepo;
    public $categoryRepo;
    public $commentRepo;
    public $roomRepo;
    public $teamRepo;
    public $positionRepo;

    public function __construct(
        BlogRepository $blogRepository,
        CategoryRepository $caterogyRepository,
        CommentRepository $commentRepository,
        RoomRepository $roomRepository,
        TeamRepository $teamRepository,
        PositionRepository $positionRepository
    ) {
       $this->blogRepo = $blogRepository;
       $this->categoryRepo = $caterogyRepository;
       $this->commentRepo = $commentRepository;
       $this->roomRepo = $roomRepository;
       $this->teamRepo =  $teamRepository;
       $this->positionRepo =  $positionRepository;
    }
}
