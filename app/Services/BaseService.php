<?php

namespace App\Services;

use App\Events\NotificationEvent;
use App\Models\BasicInfo;
use App\Models\Organizational;
use App\Models\Position;
use App\Models\Room;
use App\Models\Team;
use App\Models\User;
use App\Notifications\AssignPerson;
use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class BaseService
{

    public function __construct(BaseRepository $baseReRepository)
    {
    }

    /**
     * Change type varriable to 1. Camelcase 2. Snake Case
     *
     * @author ntqui
     * @since 20220714
     * @param Array $arrayData
     * @param String $typeConvert
     * @return Array
     */
    public function modifyKeyData(array $arrayData, string $typeConvert)
    {
        $newArray = [];
        foreach ($arrayData as $key => $value) {
            foreach ($value as $keyValue => $data) {
                switch ($typeConvert) {
                    case 'camel':
                        $newKey = Str::camel($keyValue);
                        break;
                    case 'snake':
                        $newKey = Str::snake($keyValue);
                        break;
                }
                $newArray[$key][$newKey] = $arrayData[$key][$keyValue];
                if ($newKey == 'carCategory' && !empty($arrayData[$key][$keyValue])) {
                    $newArray[$key]['brand'] = $arrayData[$key][$keyValue]['brand'];
                    unset($newArray[$key][$newKey]);
                }

                if (array_key_exists('car_description', $value) && $newKey == 'carDescription' && !empty($arrayData[$key][$keyValue])) {
                    $newArray[$key]['description'] = $arrayData[$key][$keyValue]['description'];
                    unset($newArray[$key][$newKey]);
                }

                if ($newKey == 'imagePath' && !empty($arrayData[$key][$keyValue])) {
                    unset($newArray[$key][$newKey]);
                    $newArray[$key]['imagePath'] = url($arrayData[$key][$keyValue]);
                }
            }
        }
        return $newArray;
    }

    /**
     * Create response when success
     *
     * @author ntqui
     * @since 20220714
     * @param Array $data
     * @param Int $statusCode
     * @param Array $message
     * @return Array
     */
    public function makeResponeSuccess(array $data = [], int $statusCode = 200, array $message = [])
    {
        $result = [];

        $result['response'] = [
            'success' => true,
            'data' => $data ?? [],
            'message' => $message ?? config('message.en.success'),
        ];

        $result['statusCode'] = $statusCode;

        return $result;
    }

    /**
     * Create response when fail
     *
     * @author ntqui
     * @since 20220714
     * @param Int $statusCode
     * @param String $message
     * @return Array
     */
    public function makeResponeError(int $statusCode = 500, string $message)
    {
        $result = [];

        $result['response'] = [
            'success' => false,
            'message' => $message,
        ];

        $result['statusCode'] = $statusCode;

        return $result;
    }

    public function sendNotification(User $user, $id, $link, $message)
    {
        $user->notify(new AssignPerson([
            'id' => $id,
            'creator' => auth()->id(),
            'message' => $message,
            'link' => $link,
        ]));
        event(new NotificationEvent([
            'id' => $id,
            'creator' => auth()->id(),
            'message' => $message,
            'link' => $link,
        ], $user->id));
    }

    public function getHR()
    {
        $result = [];
        try {
            $positions = User::with('roles')->whereHas('roles', function ($query) {
                $query->where('name', 'hr-manager')->orWhere('name', 'hr-chief');
            })->get();
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getCEO()
    {
        $result = [];
        try {
            $positions = User::with('roles')->whereHas('roles', function ($query) {
                $query->where('name', 'ceo-manager');
            })->get();
            $result = !empty($positions) ? $positions : [];
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getAll_X($x)
    {

        $result = [];

        try {
            $result_queried = [];
            if ($x == 'rooms')
                $result_queried = Room::where('delete_flag', 0)->get();
            if ($x == 'teams')
                $result_queried = Team::where('delete_flag', 0)->get();
            if ($x == 'positions')
                $result_queried = Position::where('delete_flag', 0)->get();

            if (!empty($result_queried))
                $result = $result_queried;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    public function getBasicInfoOfDirectBossArray($id, $mode = "user", $mode2 = true, $mode3 = true)
    {

        $result = [];
        try {

            $user_basic_info = BasicInfo::where('id', $id)->first();
            if ($mode == 'user')
                $user_basic_info = User::where('id', $id)->first()->staff;

            $user_basic_info_organizational = Organizational::where('basic_info_id', $user_basic_info->id)->first();

            $organizational_room_id = $user_basic_info_organizational->room_id;
            $organizational_team_id = $user_basic_info_organizational->team_id;
            $organizational_position_id = $user_basic_info_organizational->position_id;


            $boss_basic_infos = [];

            if ($user_basic_info->rank == 7 && $organizational_team_id == 24 && $organizational_room_id == 8 && $organizational_position_id == 9999) {
                $potential_basic_info =  Organizational::where('room_id', 8)->where('team_id', 24)->where('position_id', 9998)->first()->staff;
                if ($potential_basic_info)
                    $boss_basic_infos[] = $potential_basic_info;
            } else {
                $boss_basic_infos = $this->bossHelper($organizational_room_id, $organizational_team_id, $user_basic_info->rank, $mode2, $mode3);
            }

            if (!empty($boss_basic_infos))
                $result = $boss_basic_infos;
        } catch (Exception $e) {
            Log::channel('daily')->error($e->getMessage());
        }

        return $result;
    }

    private function bossHelper($organizational_room_id, $organizational_team_id, $basic_info_rank, $mode2, $mode3)
    {
        if ($mode2)
            for ($i = $basic_info_rank + 1; $i < 8; $i++) {

                $boss_potential = $this->getBossPotential($i, $organizational_room_id, $organizational_team_id, $mode3);

                if (!empty($boss_potential)) {
                    return $boss_potential;
                }
            }
        else {
            $boss_potential = [];
            for ($i = 8; $i >= $basic_info_rank + 1; $i--) {

                $boss_potential = $this->getBossPotential($i, $organizational_room_id, $organizational_team_id, $mode3);

                if (!empty($boss_potential)) {
                    return $boss_potential;
                }
            }

            if (empty($boss_potential)) {
                $boss_potential = $this->getBossPotential(7, $organizational_room_id, $organizational_team_id, true);
                return $boss_potential;
            }
        }
    }

    private function getBossPotential($boss_rank, $organizational_room_id, $organizational_team_id, $mode3)
    {
        $potential_basic_infos = BasicInfo::where('rank', $boss_rank)->where('delete_flag', 0)->get();
        $boss_potential = [];

        foreach ($potential_basic_infos as $b_info) {
            $b_info_organizational =  Organizational::where('basic_info_id', $b_info->id)->first();
            $potential_team_id = $b_info_organizational->team_id;
            $potential_room_id = $b_info_organizational->room_id;
            if (($potential_team_id == $organizational_team_id && (in_array($boss_rank, [4, 5])))
                || ($potential_room_id == $organizational_room_id && $boss_rank == 6)
                || ($boss_rank == 7 && $mode3)
            )
                $boss_potential[] = $b_info;
        }

        return $boss_potential;
    }
}
