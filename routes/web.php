<?php

use App\Http\Controllers\Admin\Criteria\CriteriaController;
use App\Http\Controllers\Admin\Dayoff\DayoffController;
use App\Http\Controllers\Admin\FormDayoff\FormDayoffController;
use App\Http\Controllers\Admin\Payslip\PayslipController;
use App\Http\Controllers\Admin\Salary\SalaryController;
use App\Http\Controllers\Admin\HelperController;
use App\Http\Controllers\Admin\Recruit\ResultController;
use App\Http\Controllers\Admin\Recruitment\RecruitmentProposalController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\PersonalController;
use App\Http\Controllers\Admin\Plan\OneYearController;
use App\Http\Controllers\Admin\Recruit\FormController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\TableKPI\TableKPIController;
use App\Http\Controllers\Admin\Timekeep\TimekeepController;
use App\Http\Controllers\Admin\TrackingDayOff\TrackingDayOffController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\Blog\BlogController;
use App\Http\Controllers\Admin\Internship\InternshipController;
use App\Http\Controllers\Admin\Plan\PlanDayController;
use App\Http\Controllers\Admin\Plan\PlanQuarterController;
use App\Http\Controllers\Admin\Plan\PlanWeekController;
use App\Http\Controllers\User\Comment\CommentController;
use App\Http\Controllers\RoadMapController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::group(['middleware' => ['auth']], function () {
    Route::prefix('users')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('users.index');
        Route::get('/create', [UserController::class, 'create'])->name('users.create');
        Route::post('/store', [UserController::class, 'store'])->name('users.store');

        Route::post('/update/{id}', [UserController::class, 'update'])->name('users.update');
        Route::get('/destroy/{id}', [UserController::class, 'destroy'])->name('users.destroy');

        Route::get('/edit/{id}', [UserController::class, 'edit'])->name('users.edit');
        // change password
        Route::get('/profile', [UserController::class, 'userProfile'])->name('userProfile');
        Route::post('/changePassword', [UserController::class, 'changePassword'])->name('user.changePassword');
    });
    Route::prefix('roles')->group(function () {
        Route::get('/', [RoleController::class, 'index'])->name('roles.index');
        Route::get('/create', [RoleController::class, 'create'])->name('roles.create');
        Route::post('/store', [RoleController::class, 'store'])->name('roles.store');

        Route::post('/update/{id}', [RoleController::class, 'update'])->name('roles.update');
        Route::get('/destroy/{id}', [RoleController::class, 'destroy'])->name('roles.destroy');

        Route::get('/edit/{id}', [RoleController::class, 'edit'])->name('roles.edit');
    });

    Route::prefix('dayoff')->group(function () {
        Route::get('/', [DayoffController::class, 'index'])->name('dayoffIndex');
        Route::post('/store', [DayOffController::class, 'storeDataInsert'])->name('dayoff.store')->middleware('permission:dayoff-create');
        Route::post('/storeUpdate', [DayOffController::class, 'storeDataUpdate'])->name('dayoff.storeUpdate')->middleware('permission:dayoff-update');
        Route::get('/view/{id}', [DayOffController::class, 'view'])->name('dayoff.view');
        Route::get('/getTeams/{room_id}', [DayoffController::class, 'getTeamByRommID'])->name('dayoff.getTeamByRommID');
        Route::get('/getPositions/{team_id}', [DayoffController::class, 'getPositionByTeamID'])->name('dayoff.getPositionByRommID');
        Route::get('/getStaff/{room_id}/{team_id}/{positon_id}', [DayoffController::class, 'getStaffByConditons'])->name('dayoff.getStaffByConditons');
        Route::get('/edit/{id}', [DayoffController::class, 'edit'])->name('dayoffEdit')->middleware('permission:dayoff-update');
        Route::get('/create', [DayoffController::class, 'create'])->name('dayoffCreate')->middleware('permission:dayoff-create');
        Route::delete('/delete/{id}', [DayoffController::class, 'delete'])->name('dayoffDelete')->middleware('permission:dayoff-delete');
        Route::post('/view/updateStatus', [DayoffController::class, 'updateStatus'])->name('dayoff.updateStatus');
        Route::get('/getByBasicInfoId/{id}', [DayoffController::class, 'getByBasicInfoId'])->name('dayoff.getByBasicInfoId');
        Route::get('/calendar', [DayoffController::class, 'getDateCalendar'])->name('dayoffCalendar');
    });

    // Route::prefix('basic_info')->group(function () {
    //     Route::post('/{}', [DayOffController::class, 'storeDataInsert']);
    // });

    Route::group(['prefix' => 'helper'], function () {
        Route::get('/getRoom/{basic_info_id}', [HelperController::class, 'getRoomByBasicInfoID'])->name('helper.getRoomByBasicInfoID');
        Route::get('/getTeams/{room_id}', [HelperController::class, 'getTeamByRommID'])->name('helper.getTeamByRommID');
        Route::get('/getPositions/{team_id}', [HelperController::class, 'getPositionByTeamID'])->name('helper.getPositionByTeamID');
        Route::get('/getStaff/{room_id}/{team_id}/{positon_id}', [HelperController::class, 'getStaffByConditons'])->name('helper.getStaffByConditons');
    });
    Route::prefix('permissions')->group(function () {
        Route::get('/', [PermissionController::class, 'index'])->name('permissions.index');
        Route::post('/store', [PermissionController::class, 'store'])->name('permissions.store');
        Route::PUT('/update/{id}', [PermissionController::class, 'update'])->name('permissions.update');
        Route::get('/destroy/{id}', [PermissionController::class, 'destroy'])->name('permissions.destroy');

        Route::get('/show/{id}', [PermissionController::class, 'show'])->name('permissions.show');
    });

    //Language Translation
    Route::get('index/{locale}', [App\Http\Controllers\HomeController::class, 'lang']);

    Route::get('/', [App\Http\Controllers\HomeController::class, 'root'])->name('root');

    Route::get('roadmap', [RoadMapController::class, 'index'])->name('roadmap.index')->middleware('permission:roadmap-view');
    Route::post('roadmap', [RoadMapController::class, 'update'])->name('roadmap.update');

    //Update User Details
    Route::post('/update-profile/{id}', [App\Http\Controllers\HomeController::class, 'updateProfile'])->name('updateProfile');
    Route::post('/update-password/{id}', [App\Http\Controllers\HomeController::class, 'updatePassword'])->name('updatePassword');



    Route::group(['prefix' => 'helper'], function () {
        Route::get('/markAsReadNoti/{id}', [HelperController::class, 'markAsReadNoti'])->name('helper.markAsReadNoti');
        Route::get('/getMangerInRoom/{room_id}', [HelperController::class, 'getMangerInRoom'])->name('helper.getMangerInRoom');
        Route::get('/getTeams/{room_id}', [HelperController::class, 'getTeamByRommID'])->name('helper.getTeamByRommID');
        Route::get('/getPositions/{team_id}', [HelperController::class, 'getPositionByTeamID'])->name('helper.getPositionByTeamID');
        Route::get('/getStaff/{room_id}/{team_id}/{positon_id}', [HelperController::class, 'getStaffByConditons'])->name('helper.getStaffByConditons');
        Route::get('/getCriteriaByConditions/{room_id}/{team_id}/{positon_id}', [HelperController::class, 'getCriteriaByConditions'])->name('helper.getCriteriaByConditions');
    });

    //Tracking Dayoff
    Route::prefix('trackingdayoff')->group(function () {
        Route::get('/', [TrackingDayOffController::class, 'index'])->name('trackingDayoffIndex');
        Route::get('/getTrackingDayOffByBasicInfoId/{id}/{year}', [TrackingDayOffController::class, 'getTrackingDayOffByBasicInfoId'])->name('trackingDayoffGetByBasicInfoId');
    });

    //Personal
    Route::group(['prefix' => 'personal'], function () {
        Route::get('/', [PersonalController::class, 'personal'])->name('personal');
        Route::get('/add', [PersonalController::class, 'add'])->name('personalAdd')->middleware('permission:personal-create');
        Route::get('/edit/{id}', [PersonalController::class, 'editPersonal'])->name('editPersonal');
        Route::get('/view/{id}', [PersonalController::class, 'view'])->name('personal.view')->middleware('permission:personal-read-1');
        Route::post('/updatePersonal/{id}', [PersonalController::class, 'update'])->name('updatePersonal');
        Route::post('/createPersonal', [PersonalController::class, 'create'])->name('createPersonal');
        Route::get('/delete/{id}', [PersonalController::class, 'delete'])->name('deletePersonal');
    });

    //Criteria
    Route::prefix('criteria')->group(function () {
        Route::get('/', [CriteriaController::class, 'index'])->name('criteria.index');
        Route::get('/create', [CriteriaController::class, 'create'])->name('criteria.create')->middleware('permission:12_criteria-create');
        Route::post('/store', [CriteriaController::class, 'storeInsert'])->name('criteria.storeInsert');

        Route::get('/edit/{id}', [CriteriaController::class, 'edit'])->name('criteria.edit')->middleware('permission:12_criteria-edit');
        Route::PUT('/edit', [CriteriaController::class, 'storeUpdate'])->name('criteria.storeUpdate');

        Route::get('/view/{id}', [CriteriaController::class, 'view'])->name('criteria.view');
        Route::post('/updateStatus/{id}', [CriteriaController::class, 'updateStatus'])->name('criteria.updateStatus');
        Route::get('/delete/{id}', [CriteriaController::class, 'delete'])->name('criteria.delete');
    });

    //Description
    Route::prefix('description')->group(function () {
        Route::get('/', [App\Http\Controllers\Admin\Description\DescriptionController::class, 'description'])->name('description');
        Route::get('/view/{id}', [App\Http\Controllers\Admin\Description\DescriptionController::class, 'view'])->name('description.view');
    });

    //Recruit-Criteria
    Route::prefix('recruit-criteria')->group(function () {
        Route::get('/', [App\Http\Controllers\Admin\RecruitCriteria\RecruitCriteriaController::class, 'index'])->name('recruit-criteria.index')->middleware('permission:recruit_criteria-read');
        Route::get('/view/{room_id}/{team_id}/{position_id}', [App\Http\Controllers\Admin\RecruitCriteria\RecruitCriteriaController::class, 'view'])->name('recruit-criteria.view')->middleware('permission:recruit_criteria-read');
    });


    //TableKPI
    Route::get('/tablekpi', [App\Http\Controllers\Admin\TableKPI\TableKPIController::class, 'tablekpi'])->name('tablekpi');
    Route::get('tablekpi/data', [TableKPIController::class, 'loadData'])->name('tablekpi.data');
    Route::get('tablekpi/loadDataMyWork', [TableKPIController::class, 'loadDataMyWork'])->name('tablekpi.loadDataMyWork');
    Route::get('/tablekpi/edit/{id}', [App\Http\Controllers\Admin\TableKPI\TableKPIController::class, 'edit'])->name('editTablekpi');
    Route::get('/tablekpi/create', [App\Http\Controllers\Admin\TableKPI\TableKPIController::class, 'create'])->name('createTablekpi');
    Route::post('/tablekpi/store', [App\Http\Controllers\Admin\TableKPI\TableKPIController::class, 'storeInsert'])->name('tablekpi.storeInsert');
    Route::PUT('/tablekpi/edit', [App\Http\Controllers\Admin\TableKPI\TableKPIController::class, 'storeUpdate'])->name('tablekpi.storeUpdate');
    Route::delete('/tablekpi/delete/{id}', [App\Http\Controllers\Admin\TableKPI\TableKPIController::class, 'delete'])->name('tablekpi.delete');
    Route::get('/tablekpi/view/{id}', [App\Http\Controllers\Admin\TableKPI\TableKPIController::class, 'view'])->name('tablekpi.view');
    Route::post('/tablekpi/updateStatus/{id}', [App\Http\Controllers\Admin\TableKPI\TableKPIController::class, 'updateStatus'])->name('tablekpi.updateStatus');
    Route::get('/tablekpi/exportFile/{id}', [App\Http\Controllers\Admin\TableKPI\TableKPIController::class, 'exportFile'])->name('tablekpi.exportFile');

    //Room
    Route::get('/room', [App\Http\Controllers\Admin\RoomController::class, 'index'])->name('room');
    Route::post('/room/create', [App\Http\Controllers\Admin\RoomController::class, 'create'])->name('room.create');
    Route::get('/room/edit/{id}', [App\Http\Controllers\Admin\RoomController::class, 'edit'])->name('room.edit');
    Route::put('/room/update/{id}', [App\Http\Controllers\Admin\RoomController::class, 'update'])->name('room.update');
    Route::get('/room/delete/{id}', [App\Http\Controllers\Admin\RoomController::class, 'delete'])->name('room.delete');

    //Team
    Route::get('/team', [App\Http\Controllers\Admin\TeamController::class, 'index'])->name('team');
    Route::post('/team/create', [App\Http\Controllers\Admin\TeamController::class, 'create'])->name('team.create');
    Route::get('/team/edit/{id}', [App\Http\Controllers\Admin\TeamController::class, 'edit'])->name('team.edit');
    Route::put('/team/update/{id}', [App\Http\Controllers\Admin\TeamController::class, 'update'])->name('team.update');
    Route::get('/team/delete/{id}', [App\Http\Controllers\Admin\TeamController::class, 'delete'])->name('team.delete');

    //Position
    Route::get('/position', [App\Http\Controllers\Admin\PositionController::class, 'index'])->name('position');
    Route::post('/position/create', [App\Http\Controllers\Admin\PositionController::class, 'create'])->name('position.create');
    Route::get('/position/edit/{id}', [App\Http\Controllers\Admin\PositionController::class, 'edit'])->name('position.edit');
    Route::put('/position/update/{id}', [App\Http\Controllers\Admin\PositionController::class, 'update'])->name('position.update');
    Route::get('/position/delete/{id}', [App\Http\Controllers\Admin\PositionController::class, 'delete'])->name('position.delete');
    Route::get('/position/getUpperPositions', [App\Http\Controllers\Admin\PositionController::class, 'getUpperPositions'])->name('position.getUpperPositions');
    Route::get('/position/getLowerPositions', [App\Http\Controllers\Admin\PositionController::class, 'getLowerPositions'])->name('position.getLowerPositions');


    
    Route::prefix('chart')->group(function () {
        Route::get('tree', [App\Http\Controllers\TreeController::class, 'index'])->name('tree.index');
        Route::get('tree/getAvatar/{id}', [App\Http\Controllers\TreeController::class, 'getAvatar'])->name('tree.getAvatar');
        Route::get('clearSession/{key}', [App\Http\Controllers\TreeController::class, 'clearSession'])->name('tree.clearSession');
        Route::get('showFormEdit/{room_id}/{team_id}/{positon_id}/{rank}', [App\Http\Controllers\TreeController::class, 'showFormEdit'])->name('tree.showFormEdit');
        Route::get('showFormAdd/{room_id}/{team_id}/{positon_id}/{rank}', [App\Http\Controllers\TreeController::class, 'showFormAdd'])->name('tree.showFormAdd');

        Route::post('tree/add', [App\Http\Controllers\TreeController::class, 'add'])->name('tree.add');
        Route::post('tree/edit/{id}', [App\Http\Controllers\TreeController::class, 'edit'])->name('tree.edit');
        Route::post('tree/action', [App\Http\Controllers\TreeController::class, 'action'])->name('tree.action');

        Route::get('chartBasic', [App\Http\Controllers\TreeController::class, 'getChartBasic'])->name('tree.getChartBasic');
        Route::get('getChartRoom/{id}', [App\Http\Controllers\TreeController::class, 'getChartRoom'])->name('tree.getChartRoom');
        Route::get('getPerson/{id}', [App\Http\Controllers\TreeController::class, 'getPerson'])->name('tree.getPerson');

        Route::post('tree/storeUpdate', [App\Http\Controllers\TreeController::class, 'storeUpdate'])->name('tree.storeUpdate');
    });

    //RecruitmentProposal
    Route::prefix('recruitment-proposal')->group(function () {
        Route::get('/', [RecruitmentProposalController::class, 'index'])->name('recruitment-proposal.index')->middleware('permission:recruit_proposal-read');
        Route::get('/create', [RecruitmentProposalController::class, 'create'])->name('recruitment-proposal.create')->middleware('permission:recruit_proposal-create');
        Route::post('/store', [RecruitmentProposalController::class, 'storeInsert'])->name('recruitment-proposal.storeInsert');

        Route::get('/edit/{id}', [RecruitmentProposalController::class, 'edit'])->name('recruitment-proposal.edit')->middleware('permission:recruit_proposal-edit');
        Route::PUT('/edit', [RecruitmentProposalController::class, 'storeUpdate'])->name('recruitment-proposal.storeUpdate');

        Route::delete('/delete/{id}', [RecruitmentProposalController::class, 'delete'])->name('recruitment-proposal.delete')->middleware('permission:recruit_proposal-delete');
        Route::get('/view/{id}', [RecruitmentProposalController::class, 'view'])->name('recruitment-proposal.view');
        Route::post('/updateStatus/{id}', [RecruitmentProposalController::class, 'updateStatus'])->name('recruitment-proposal.updateStatus');
    });


    //Form-Dayoff
    Route::prefix('formdayoff')->group(function () {
        Route::get('/', [FormDayoffController::class, 'index'])->name('FormDayOffIndex');
        Route::get('/view/{id}', [FormDayoffController::class, 'view'])->name('FormDayOffView');
        Route::get('/edit/{id}', [FormDayoffController::class, 'edit'])->name('FormDayOffEdit')->middleware('permission:form_day_off-update');
        Route::get('/edit_admin/{id}', [FormDayoffController::class, 'edit_admin'])->name('FormDayOffEditAdmin')->middleware('permission:form_day_off-update_admin');
        Route::get('/create', [FormDayoffController::class, 'create'])->name('FormDayOffCreate')->middleware('permission:form_day_off-create');
        Route::get('/create_admin', [FormDayoffController::class, 'create_admin'])->name('FormDayOffCreateAdmin')->middleware('permission:form_day_off-create_admin');
        Route::post('/storeDataInsert', [FormDayoffController::class, 'storeDataInsert'])->name('FormDayOffStoreDataInsert');
        Route::post('/storeDataUpdate', [FormDayoffController::class, 'storeDataUpdate'])->name('FormDayOffStoreDataUpdate');
        Route::post('/updateStatus', [FormDayoffController::class, 'updateStatus'])->name('FormDayOffUpdateStatus');
        Route::delete('/delete/{id}', [FormDayoffController::class, 'delete'])->name('FormDayOffDelete')->middleware('permission:form_day_off-delete');
        Route::get('/getBasicInfoOfReplacementArrayByBasicInfoId/{id}', [FormDayoffController::class, 'getBasicInfoOfReplacementArrayByBasicInfoId'])->name('FormDayOffHelper1');
        Route::get('/getBasicInfoOfDirectBossArrayByBasicInfoId/{id}', [FormDayoffController::class, 'getBasicInfoOfDirectBossArrayByBasicInfoId'])->name('FormDayOffHelper2');
        Route::get('/getConfirmedFormDayOffsOfBasicInfoId/{id}', [FormDayoffController::class, 'getConfirmedFormDayOffsOfBasicInfoId'])->name('FormDayOffHelper3');
        Route::get('/getBasicInfoOfHighestBossArrayByBasicInfoId/{id}', [FormDayoffController::class, 'getBasicInfoOfHighestBossArrayByBasicInfoId'])->name('FormDayOffHelper4');
        Route::post('/checkForm', [FormDayoffController::class, 'checkForm'])->name('FormDayOffCheckForm');
    });

    //Timekeep Dayoff
    Route::prefix('timekeep')->group(function () {
        Route::get('/', [TimekeepController::class, 'index'])->name('timekeep.index')->middleware('permission:timekeep-index');
        Route::post('/loadData', [TimekeepController::class, 'loadData'])->name('timekeep.loadData');
        Route::post('/updateTimeKeep/{id}/{day}', [TimekeepController::class, 'edit'])->name('updateTimeKeep')->middleware('permission:timekeep-update');
        Route::post('/changeOtherFields', [TimekeepController::class, 'changeOtherFields'])->name('updateTimeKeepOtherFields')->middleware('permission:timekeep-update');
    });

    //Payslip
    Route::group(['prefix' => 'payslip'], function () {
        Route::get('/', [PayslipController::class, 'index'])->name('payslip.index');
        Route::post('/loadMyData', [PayslipController::class, 'loadMyData'])->name('payslip.loadMyData');
        Route::post('/loadData', [PayslipController::class, 'loadData'])->name('payslip.loadData');
    });

    //Salary
    Route::group(['prefix' => 'salary'], function () {
        Route::get('/', [SalaryController::class, 'index'])->name('salary.index')->middleware('permission:salary-index');
        Route::get('/getSalary/{id}', [SalaryController::class, 'getDetail'])->name('salary.getSalary');
        Route::post('/loadData', [SalaryController::class, 'loadData'])->name('salary.loadData');
        Route::post('/updateData', [SalaryController::class, 'edit'])->name('salary.updateData')->middleware('permission:salary-update');
        Route::post('/createSalaryList', [SalaryController::class, 'createSalaryList'])->name('salary.createSalaryList')->middleware('permission:salary-create');
        Route::get('/create', [SalaryController::class, 'create'])->name('salary.create')->middleware('permission:salary-create');
        Route::post('/confirmSalaryList', [SalaryController::class, 'confirmSalaryList'])->name('salary.confirmSalaryList');
        Route::post('/saveSalarySetting', [SalaryController::class, 'saveSalarySetting'])->name('salary.saveSalarySetting');

    });

    //internship
    Route::group(['prefix' => 'internship'], function () {
        Route::get('/', [InternshipController::class, 'index'])->name('internship.index');
        Route::get('/edit/{id}', [InternshipController::class, 'edit'])->name('internship.edit')->middleware('permission:internship-edit');
        Route::get('/create', [InternshipController::class, 'create'])->name('internship.create')->middleware('permission:internship-create');
        Route::post('/storeInsert', [InternshipController::class, 'storeInsert'])->name('internship.storeInsert')->middleware('permission:internship-create');
        Route::post('/storeUpdate', [InternshipController::class, 'storeUpdate'])->name('internship.storeUpdate')->middleware('permission:internship-edit');
        Route::post('/checkCreateInternship', [InternshipController::class, 'checkCreateInternship'])->name('internship.checkCreateIntership');
        Route::get('/view/{id}', [InternshipController::class, 'view'])->name('internship.view')->middleware('permission:internship-view');
        Route::delete('/delete/{id}', [InternshipController::class, 'delete'])->name('internship.delete')->middleware('permission:internship-delete');
    });

    //plant
    Route::get('/plan/oneyear', [OneYearController::class, 'index'])->name('planOneYear');
    Route::get('/plan/oneyear/edit', [OneYearController::class, 'edit'])->name('planOneYearEdit');

    Route::group(['prefix' => 'recruit'], function () {
        Route::prefix('result')->group(function () {
            Route::get('/', [ResultController::class, 'index'])->name('result.index');
            Route::get('/create', [ResultController::class, 'create'])->name('result.create');
            Route::post('/storeInsert', [ResultController::class, 'storeInsert'])->name('result.storeInsert');
            Route::get('/view/{id}', [ResultController::class, 'view'])->name('result.view');
            Route::get('/edit/{id}', [ResultController::class, 'edit'])->name('result.edit');
            Route::put('/storeUpdate', [ResultController::class, 'storeUpdate'])->name('result.storeUpdate');
            Route::delete('/delete/{id}', [ResultController::class, 'delete'])->name('result.delete');
            Route::get('/interview/{id}', [ResultController::class, 'interview'])->name('result.interview');
            Route::post('/interviewCandidate', [ResultController::class, 'interviewCandidate'])->name('result.interviewCandidate');
        });
    });


    //Blog
    Route::prefix('blog')->group(function () {
        Route::get('/index', [BlogController::class, 'index'])->name('blog.index');
        Route::get('/create', [BlogController::class, 'create'])->name('blog.create')->middleware('permission:blog-create');
        Route::post('/store', [BlogController::class, 'storeInsert'])->name('blog.storeInsert');
        Route::get('/edit/{id}', [BlogController::class, 'edit'])->name('blog.edit')->middleware('permission:blog-edit');
        Route::post('/edit', [BlogController::class, 'storeUpdate'])->name('blog.storeUpdate');
        Route::get('/post/{category}', [BlogController::class, 'viewPost'])->name('blog.viewPost');
        Route::get('/post/{category}/{id}', [BlogController::class, 'viewPostDetail'])->name('blog.viewPostDetail');
        Route::post('/post/{category}/search', [BlogController::class, 'viewPostSearch'])->name('blog.viewPostSearch');
        Route::post('/post/{category}/filter', [BlogController::class, 'viewPostFilter'])->name('blog.viewPostFilter');

        Route::get('/delete/{id}', [BlogController::class, 'delete'])->name('blog.delete')->middleware('permission:blog-delete');

        Route::get('/getTeams/{room_id}', [BlogController::class, 'getTeamByRommID'])->name('blog.getTeamByRommID');
        Route::get('/getPosition/{team_id}', [BlogController::class, 'getPositionByTeamID'])->name('blog.getPositionByTeamID');
        Route::get('/getStaff/{room_id}/{team_id}/{possition_id}', [BlogController::class, 'getStaffByConditons'])->name('blog.getStaffByConditons');
    });


    //Comment
    Route::prefix('comment')->group(function () {
        Route::post('/store', [CommentController::class, 'storeCommentPost'])->name('comment.store');
        Route::get('/delete/{id}', [CommentController::class, 'deleteCommentPost'])->name('comment.delete')->middleware('permission:user-delete-comment');

        //Reply Comment
        Route::post('/{id}/reply-comment/store', [CommentController::class, 'storeReplyComment'])->name('replycomment.store');
        Route::get('reply-comment/delete/{id}', [CommentController::class, 'deleteReplyComment'])->name('replycomment.delete')->middleware('permission:user-delete-comment');
    });

    //Plan_Quarter
    Route::prefix('plan_quarter')->group(function () {
        Route::get('/',                         [PlanQuarterController::class, 'index'])->name('plan_quarter.index');
        Route::get('/create',                   [PlanQuarterController::class, 'create'])->name('plan_quarter.create');
        Route::get('/edit/{id}',                [PlanQuarterController::class, 'view'])->name('plan_quarter.edit');
        Route::get('/view/{id}',                [PlanQuarterController::class, 'view'])->name('plan_quarter.view');
        Route::post('/storeInsert',             [PlanQuarterController::class, 'storeInsert'])->name('plan_quarter.storeInsert');
        Route::post('/storeUpdate',             [PlanQuarterController::class, 'storeUpdate'])->name('plan_quarter.storeUpdate');
        Route::post('/checkIfExistPlanQuarter', [PlanQuarterController::class, 'checkIfExistPlanQuarter'])->name('plan_quarter.checkIfExistPlanQuarter');
        Route::get('/getPlanQuarter',           [PlanQuarterController::class, 'getPlanQuarter'])->name('plan_quarter.getPlanQuarter');
        Route::get('/filterPlanQuarter',        [PlanQuarterController::class, 'filterPlanQuarter'])->name('plan_quarter.filterPlanQuarter');
        Route::delete('/delete/{id}',           [PlanQuarterController::class, 'delete'])->name('plan_quarter.delete');
    });

    //Plan_Week
    Route::prefix('plan_week')->group(function () {
        Route::get('/',                                [PlanWeekController::class, 'index'])->name('plan_week.index');
        Route::get('/create',                          [PlanWeekController::class, 'create'])->name('plan_week.create');
        Route::get('/edit/{id}',                       [PlanWeekController::class, 'edit'])->name('plan_week.edit');
        Route::get('/view/{id}',                       [PlanWeekController::class, 'view'])->name('plan_week.view');
        Route::post('/storeInsert',                    [PlanWeekController::class, 'storeInsert'])->name('plan_week.storeInsert');
        Route::post('/storeUpdate',                    [PlanWeekController::class, 'storeUpdate'])->name('plan_week.storeUpdate');
        Route::get('/filterPlanWeek',                  [PlanWeekController::class, 'filterPlanWeek'])->name('plan_week.filterPlanWeek');
        Route::delete('/delete/{id}',                  [PlanWeekController::class, 'delete'])->name('plan_week.delete');
        Route::post('/addPlanDayTimeslot',             [PlanWeekController::class, 'addPlanDayTimeslot'])->name('plan_week.addPlanDayTimeslot');
        Route::get('/getPlanDayTimeslotDataFromDates', [PlanWeekController::class, 'getPlanDayTimeslotDataFromDates'])->name('plan_week.getPlanDayTimeslotDataFromDates');
        Route::get('/getPlanDayIdFromDate',            [PlanWeekController::class, 'getPlanDayIdFromDate'])->name('plan_week.getPlanDayIdFromDate');
        Route::get('/getPlanWeekDataFromDates',        [PlanWeekController::class, 'getPlanWeekDataFromDates'])->name('plan_week.getPlanWeekDataFromDates');
        Route::post('/saveWeekAspects',                [PlanWeekController::class, 'saveWeekAspects'])->name('plan_week.saveWeekAspects');
        Route::delete('/deletePlanDayTimeslot',        [PlanWeekController::class, 'deletePlanDayTimeslot'])->name('plan_week.deletePlanDayTimeslot');
        Route::get('/getPlanColors',                   [PlanWeekController::class, 'getPlanColors'])->name('plan_week.getPlanColors');
        Route::post('/upsertPlanColor',                [PlanWeekController::class, 'upsertPlanColor'])->name('plan_week.upsertPlanColor');
        Route::post('/deletePlanColor',                [PlanWeekController::class, 'deletePlanColor'])->name('plan_week.deletePlanColor');




    });

    //Plan_Day
    Route::prefix('plan_day')->group(function () {
        Route::get('/',                     [PlanDayController::class, 'index'])->name('plan_day.index');
        Route::get('/create',               [PlanDayController::class, 'create'])->name('plan_day.create');
        Route::get('/edit/{id}',            [PlanDayController::class, 'edit'])->name('plan_day.edit');
        Route::get('/view/{id}',            [PlanDayController::class, 'view'])->name('plan_day.view');
        Route::post('/storeInsert',         [PlanDayController::class, 'storeInsert'])->name('plan_day.storeInsert');
        Route::post('/storeUpdate',         [PlanDayController::class, 'storeUpdate'])->name('plan_day.storeUpdate');
        Route::delete('/delete/{id}',       [PlanWeekController::class, 'delete'])->name('plan_day.delete');
        Route::get('/filterPlanDay',        [PlanDayController::class, 'filterPlanDay'])->name('plan_day.filterPlanDay');
        Route::get('/checkIfExistPlanWeek', [PlanDayController::class, 'checkIfExistPlanWeek'])->name('plan_day.checkIfExistPlanWeek');

    });


    // defauld index
    Route::get('{any}', [App\Http\Controllers\HomeController::class, 'index'])->name('index');
});
