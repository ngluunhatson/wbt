@extends('layouts.fe.chart')
@section('title') @lang('translation.dashboards') @endsection
@section('css')
<link href="{{ URL::asset('/assets/css/chart/diagram.css') }}" rel="stylesheet">
@endsection
<script>
    const listNation = {!!json_encode(config('constants.national')) !!}
</script>

@section('content')
<!-- <div class="zoomController form-group">Zoom:
    <select style="z-index: 0;"  id="zoomController">
        <option id="10">10%</option>
        <option id="20">20%</option>
        <option id="30">30%</option>
        <option id="40">40%</option>
        <option id="50">50%</option>
        <option id="60">60%</option>
        <option id="70">70%</option>
        <option id="80">80%</option>
        <option id="90">90%</option>
        <option id="100" selected>100%</option>
    </select>
</div> -->
<div id="btnBack" style="display: none;">
    <button type="button" class="btn btn-secondary">Trở lại</button>
</div>
<iframe id="framechart" src="/chart.html?csrf={{csrf_token()}}&role={{(auth()->user()->roles->count() > 0) ? auth()->user()->roles[0]->name : ''}}" style="width: 100%;height:100%;border:none;overflow:hidden;">

</iframe>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#btnBack').on('click', () => {
            $('#btnBack').hide();
            $('#framechart').attr('src', $('#framechart').attr('src'));
            // $('#framechart').on('load', () => {
            //     var pageContentW = $(parent.document.body).find('.page-content').width();
            //     var frmRatio = pageContentW * 100 / 2100;
            //     $('#framechart').contents().find('html').css('zoom', frmRatio + "%");
            // });
            
        });
        // var pageContentW = $(parent.document.body).find('.page-content').width();
        // var frmRatio = pageContentW * 100 / 2100;
        // $('#framechart').contents().find('html').css('zoom', frmRatio + "%");
        // console.log($('#framechart').contents().find('html'));
    })
</script>
@endsection