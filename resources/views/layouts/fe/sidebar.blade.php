<!-- ========== App Menu ========== -->
<div class="app-menu navbar-menu" style="border-color: white;">
    <!-- LOGO -->
    <div class="navbar-brand-box" style="background: white;">
        <!-- Dark Logo-->
        <a href="/" class="logo logo-dark">
            <span class="logo-sm">
                <img src="{{ URL::asset('assets/images/logo-ac.png') }}" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="{{ URL::asset('assets/images/logo-ac.png') }}" alt="" height="17">
            </span>
        </a>
        <!-- Light Logo-->
        <a href="/" class="logo logo-light">
            <span class="logo-sm">
                <img src="{{ URL::asset('assets/images/logo-ac.png') }}" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="{{ URL::asset('assets/images/logo-ac.png') }}" alt="" style="margin-bottom: 15px;margin-top: 15px;height: 60px;">
            </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">

            <div id="two-column-menu">
            </div>
            <ul class="navbar-nav" id="navbar-nav">
                <li class="menu-title"><span>QUẢN TRỊ NHÂN SỰ</span></li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/">
                        <i class=" bx bx-vector"></i> <span>Sơ đồ tổ chức</span>
                    </a>
                </li>
                @if (auth()->user()->hasRole('super-admin'))
                    @can('room-menu')
                    <li class="nav-item">
                        <a class="nav-link menu-link" href="/room">
                            <i class=" ri-subtract-line"></i> <span>Phòng ban</span>
                        </a>
                    </li>
                    @endcan
                    @can('team-menu')
                    <li class="nav-item">
                        <a class="nav-link menu-link" href="/team">
                            <i class=" ri-subtract-line"></i> <span>Bộ phận</span>
                        </a>
                    </li>
                    @endcan
                    @can('position-menu')
                    <li class="nav-item">
                        <a class="nav-link menu-link" href="/position">
                            <i class=" ri-subtract-line"></i> <span>Chức vụ</span>
                        </a>
                    </li>
                    @endcan
                @endif
                @can('personal-menu')
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/personal">
                        <i class="bx bx-data"></i> <span>Thông tin nhân sự</span>
                    </a>
                </li>
                @endcan
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/criteria">
                        <i class="ri-honour-line"></i> <span>12 tiêu chí cho 1 vị trí</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/description">
                        <i class="bx bx-building"></i> <span>Mô tả công việc</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/tablekpi">
                        <i class="bx bx-target-lock"></i> <span>Bảng KPIs</span>
                    </a>
                </li>
                @can('recruit_result-menu')
                <li class="nav-item">
                    <a class="nav-link menu-link" href="#tieuchituyendung" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="tieuchituyendung">
                        <i class="ri-check-double-line"></i> <span>Tuyển dụng</span>
                    </a>
                    <div class="collapse menu-dropdown" id="tieuchituyendung">
                        <ul class="nav nav-sm flex-column">
                            @can('recruit_proposal-menu')
                            <li class="nav-item">
                                <a href="/recruitment-proposal" class="nav-link" >Đề xuất tuyển dụng</a>
                            </li>
                            @endcan
                            @can('recruit_criteria-read')
                            <li class="nav-item">
                                <a href="/recruit-criteria " class="nav-link" >Tiêu chí tuyển dụng</a>
                            </li>
                            @endcan
                            <!-- <li class="nav-item">
                                <a href="/recruit/form" class="nav-link"> Phiếu đánh giá phỏng vấn</a>
                            </li> -->
                            @can('recruit_result-menu')
                            <li class="nav-item">
                                <a href="/recruit/result" class="nav-link">Kết quả phỏng vấn</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcan



                <li class="menu-title"><i class="ri-money-dollar-circle-line"></i> <span>Lương</span></li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/dayoff">
                        <i class="ri-calendar-event-line"></i> <span>Ngày nghỉ</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/trackingdayoff">
                        <i class="ri-flight-takeoff-line"></i> <span>Phép năm</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/formdayoff">
                        <i class="ri-newspaper-line"></i> <span>Đơn xin nghỉ phép</span>
                    </a>
                </li>
                @can('timekeep-index')
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/timekeep">
                        <i class="ri-task-line"></i> <span>Bảng chấm công</span>
                    </a>
                </li>
                @endcan

                @can('salary-index')
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/salary">
                        <i class="ri-coins-fill"></i> <span>Bảng lương </span>
                    </a>
                </li>
                @endcan
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/payslip">
                        <i class="ri-money-dollar-box-fill"></i> <span>Payslip</span>
                    </a>
                </li>


                <li class="menu-title"><i class="ri-briefcase-4-fill"></i> <span>Đào tạo</span></li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/internship">
                        <i class="ri-community-fill"></i> <span>Lộ trình thử việc</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/plan/oneyear">
                        <i class="bx bxs-paper-plane"></i> <span>Kế hoạch 1 năm</span>
                    </a>
                </li>
                <li class="menu-title"><i class="ri-briefcase-4-fill"></i> <span>Kế hoạch</span></li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/roadmap">
                        <i class="ri-send-plane-fill"></i> <span>Kế hoạch 10 năm</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/plan_quarter">
                        <i class="bx bx-paper-plane"></i> <span>Kế hoạch 90 ngày</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/plan_week">
                        <i class="ri-send-plane-line"></i> <span>Kế hoạch 1 tuần</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/plan_day">
                        <i class="ri-send-plane-2-line"></i> <span>Kế hoạch 1 ngày</span>
                    </a>
                </li>

                <li class="menu-title"><i class="ri-briefcase-4-fill"></i> <span>Tin tức</span></li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/blog/post/rules">
                        <i class="ri-error-warning-fill"></i> <span>Nội qui - Thỏa ước lao động</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/blog/post/media">
                        <i class="ri-camera-lens-line"></i> <span>Truyền thông nội bộ</span>
                    </a>
                </li>
                @if(auth()->user()->hasAnyPermission(['blog-manager']))
                <li class="nav-item">
                    <a class="nav-link menu-link" href="/blog/index">
                        <i class="ri-camera-lens-line"></i> <span>Quản lý bài viết</span>
                    </a>
                </li>
                @endif
                @if(auth()->user()->hasAnyPermission(['role-menu', 'permissions-menu', 'users-menu']))
                <li class="menu-title"><i class="ri-briefcase-4-fill"></i> <span>Quản lý</span></li>
                <li class="nav-item">
                    <ul class="nav nav-sm flex-column">
                        <li class="nav-item">
                            <a class="nav-link menu-link" href="#tieuchituyendung" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="tieuchituyendung">
                                <span>Phân quyền</span>
                            </a>
                            <div class="collapse menu-dropdown" id="tieuchituyendung">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="/permissions" class="nav-link">Chức năng</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/roles" class="nav-link">Vai trò</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="/users" class="nav-link"> Tài khoản</a>
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
    <div class="sidebar-background"></div>
</div>
<!-- Left Sidebar End -->
<!-- Vertical Overlay-->
<div class="vertical-overlay"></div>