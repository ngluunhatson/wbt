<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg" data-sidebar-image="none">

<head>
    <meta charset="utf-8" />
    <title>@yield('title') | ACTION COACH CBD FIRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="stylesheet" href="{{ asset('assets/css/default-assets/new/toastr.min.css') }}">
    <link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon-ac.png')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/default-assets/datatables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/default-assets/responsive.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    @include('layouts.head-css')
</head>

@section('body')
@include('layouts.body')
@show
<!-- Begin page -->
<div id="layout-wrapper">
    @include('layouts.topbar')
    @include('layouts.fe.sidebar')
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                @yield('content')
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
    </div>
    <!-- end main content-->
</div>
<!-- END layout-wrapper -->

@include('layouts.customizer')

<!-- JAVASCRIPT -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.1.min.js') }}"></script>
<script src="{{ asset('assets/js/bundle.js') }}"></script>
<script src="{{ asset('assets/js/default-assets/fullscreen.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert2.all.min.js') }}"></script>

<!-- Active JS -->
<script src="{{ asset('assets/js/canvas.js') }}" defer></script>
<script src="{{ asset('assets/js/collapse.js') }}" defer></script>
<script src="{{ asset('assets/js/settings.js') }}" defer></script>
<script src="{{ asset('assets/js/template.js') }}" defer></script>
<script src="{{ asset('assets/js/default-assets/active.js') }}" defer></script>
<script src="{{ asset('assets/js/default-assets/jquery.datatables.min.js') }}" defer></script>
<script src="{{ asset('assets/js/default-assets/datatables.bootstrap4.js') }}" defer></script>
<script src="{{ asset('assets/js/default-assets/datatable-responsive.min.js') }}" defer></script>
<script src="{{ asset('assets/js/default-assets/responsive.bootstrap4.min.js') }}" defer></script>
@include('layouts.vendor-scripts')
@include('frontend.common.notification')
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<script src="https://js.pusher.com/7.2/pusher.min.js"></script>

<script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('9d3bdb9571d31a065830', {
        cluster: 'ap1'
    });

    var channel = pusher.subscribe("my-channel-{{auth()->id()}}");
    channel.bind('my-event', function(response) {
        var abc = $('#countNoti').html();
        var def = $('#countNotiNew').html();
        $('#countNoti').html(Number.parseInt(abc) + 1)
        $('#countNotiNew').html(Number.parseInt(abc) + 1)
        var newNotificationHtml = `
             <div class="text-reset notification-item d-block dropdown-item position-relative">
                    <div class="d-flex">
                        <div class="avatar-xs me-3">
                            <span class="avatar-title bg-soft-info text-info rounded-circle fs-16">
                                <i class="bx bx-badge-check"></i>
                            </span>
                        </div>
                        <div class="flex-1">
                            <a href="${response.data.link}" class="stretched-link">
                                <h6 class="mt-0 mb-2 lh-base">${response.data.message}
                                </h6>
                            </a>
                            <p class="mb-0 fs-11 fw-medium text-uppercase text-muted">
                                <span><i class="mdi mdi-clock-outline"></i> Just 30 sec ago</span>
                            </p>
                        </div>
                        <div class="px-2 fs-15">
                            <input class="form-check-input" type="checkbox">
                        </div>
                    </div>
                </div>
                `;

        $('#notificationPerson').find('.simplebar-mask').find('.simplebar-content').prepend(newNotificationHtml);
    });

    $('#notiClick').on('click', (event) => {
        var id = $(event.currentTarget).data('id');
        $.ajax({
            url : "{{ route('helper.markAsReadNoti', '') }}" + '/' + id,
            method : 'GET',
        })
    })
</script>
</body>

</html>