@if(!empty($dataInit['mySalary']))
<div class = "row">
    <div style = "width:7%"></div>
    <div style = "width: 45%">
        <div class = "input-group">
            <div class ="input-group-prepend" style = "align-items:center; display:grid;">
                <p style = "font-weight:bold; height:1vh" >Nhân Viên</p>

            </div>

            <div class = "input-group-append" style = "align-items:center; display:grid;">
                 <input type="text" class="form-control" style="height:2vh; background:none; border:0cm" value="{{ $dataInit['mySalary']['staff']['full_name'] }}" readonly>
            </div>
        </div>
    </div>
    <div style = "width: 42%;">

        <div class = "input-group">
            <div class ="input-group-prepend" style = "align-items:center; display:grid;">
                <p style = "font-weight:bold; height:1vh" >Bộ phận</p>

            </div>

            <div style = "width:1.85vh"></div>

            <div class = "input-group-append" style = "align-items:center; display:grid;">
                 <input type="text" class="form-control" style="height:2vh; background:none; border:0cm;" value="{{  $dataInit['mySalary']['organizational']['team']['name']}}" readonly>
            </div>
        </div>
    </div>
    <div style = "width:7%"></div>

</div>
<div class = "row">
    <div style = "width:7%"></div>

    <div style = "width: 45%">
        <div class = "input-group">
            <div class ="input-group-prepend" style = "align-items:center; display:grid;">
                <p style = "font-weight:bold; height:1vh" >Ngày công</p>

            </div>

            <div class = "input-group-append" style = "align-items:center; display:grid; margin-bottom:0.5%">
                 <input type="text" class="form-control" style="height:2vh; background:none; border:0cm" value="{{ $dataInit['workNumber'] }}"  readonly>
            </div>
        </div>
    </div>

    <div style = "width: 40%">

        <div class = "input-group">
            <div class ="input-group-prepend" style = "align-items:center; display:grid;">
                <p style = "font-weight:bold; height:1vh" >Chức danh</p>

            </div>

            <div class = "input-group-append" style = "align-items:center; display:grid; margin-bottom:0.5%">
                <input type="text" class="form-control" style="height:2vh; background:none; border:0cm" value="{{  $dataInit['mySalary']['organizational']['position']['name']}}" readonly>
            </div>
        </div>
    </div>
    <div style = "width:7%"></div>

</div>

<div class="row mt-3 d-flex justify-content-center">
    @php
        $total_detail = $dataInit['mySalary']['support']['main_salary'] + ( $dataInit['mySalary']['other_support'] ?? 0 );
        $income_salary = $total_detail / $dataInit['workNumber'] *  $dataInit['mySalary']['timekeep']['total_salary'];

        $total_overtime = $dataInit['mySalary']['income']['overtime_salary'] + $dataInit['mySalary']['allowance']['overtime_salary'];
        $total_adjustments = $dataInit['mySalary']['income']['additional_adjustments'] + $dataInit['mySalary']['allowance']['additional_adjustments'];
        $total_deductions = $dataInit['mySalary']['income']['adjustment_of_deductions'] + $dataInit['mySalary']['allowance']['adjustment_of_deductions'];
        $total_other = $dataInit['mySalary']['income']['salary_kpis'] + $total_overtime + $total_adjustments - $total_deductions;
    
        $total_insurance = $dataInit['mySalary']['insurance']['social_insurance_nv'] +
                                        $dataInit['mySalary']['insurance']['health_insurance_nv'] +
                                        $dataInit['mySalary']['insurance']['unemployment_insurance_nv'] +
                                        $dataInit['mySalary']['tax']['total'];
        $lstValue = [
            number_format($total_detail), 
            number_format($dataInit['mySalary']['support']['main_salary']),
            number_format($dataInit['mySalary']['other_support']),
            number_format($income_salary),
            number_format($total_other),
            number_format($dataInit['mySalary']['income']['salary_kpis']),
            number_format($total_overtime),
            number_format($total_adjustments),
            number_format($total_deductions),
            number_format($income_salary + $total_other),
            number_format($total_insurance),
            number_format($dataInit['mySalary']['insurance']['social_insurance_nv']),
            number_format($dataInit['mySalary']['insurance']['health_insurance_nv']),
            number_format($dataInit['mySalary']['insurance']['unemployment_insurance_nv']),
            number_format($dataInit['mySalary']['tax']['total']),
            number_format($income_salary + $total_other - $total_insurance),
            number_format($dataInit['mySalary']['note'])

            ];
    @endphp
    <table class="table table-bordered table-nowrap" style = "width:85%">
        <thead class="text-center">
            <tr>
                <th>STT</th>
                <th colspan="2">NỘI DUNG</th>
                <th>SỐ TIỀN</th>
            </tr>
        </thead>
        <tbody>
            @for($i = 0;$i < 18; $i++)
            <tr>
                @if (!in_array($i, [0,3,4,9,10,15,16,17]))
                <td class="text-center" style = "font-style:italic;">{{ config('header.payslip_stt')[$i] }}</th>
                <td colspan ="2" style = "font-style:italic;">{{ config('header.payslip_content')[$i] }}</th>
                <td style = "font-style:italic; text-align:right;">{{ $lstValue[$i] }}</th>
                @elseif ($i == 3)
                <td class="text-center" style = "font-weight: bold;">{{ config('header.payslip_stt')[$i] }}</th>
                <td colspan ="1" style = "font-weight: bold;">{{ config('header.payslip_content')[$i] }}</th>
                <td colspan ="1" style = "font-weight: bold; text-align:center; color:red">{{ $dataInit['workNumber']  }}</th>
                <td style = "font-weight: bold; text-align:right;">{{ $lstValue[$i] }}</th>
                @elseif ($i == 16)
                <td class="text-center" style = "font-weight: bold;">{{ config('header.payslip_stt')[$i] }}</th>
                <td rowspan = "2" colspan ="2" style = "font-weight: bold;">{{ config('header.payslip_content')[$i] }}</th>
                <td rowspan = "2"style = "font-weight: bold; text-align:right;">{{ $lstValue[$i] }}</th>
                @elseif ($i == 17)
                <td></th>
                @elseif ($i < 17)
                <td class="text-center" style = "font-weight: bold;">{{ config('header.payslip_stt')[$i] }}</th>
                <td colspan ="2" style = "font-weight: bold;">{{ config('header.payslip_content')[$i] }}</th>
                <td style = "font-weight: bold; text-align:right;">{{ $lstValue[$i] }}</th>

                @endif

            </tr>
            @endfor
        </tbody>
    </table>
    <!-- <label class="col-md-9 form-label fw-bold">A. Chi tiết các khoản</label>
    <label class="col-md-3 form-label fw-bold">{{ number_format($total_detail) }}</label>
    <label class="col-md-9 form-label ps-4">A.1 Mức lương chính</label>
    <label class="col-md-3 form-label">{{ number_format($dataInit['mySalary']['support']['main_salary']) }}</label>
    <label class="col-md-9 form-label ps-4">A.2 Các khoảng hỗ trợ khác</label>
    <label class="col-md-3 form-label">{{ number_format($dataInit['mySalary']['other_support']) }}</label>
    <label class="col-md-9 form-label fw-bold">B. Thu nhập theo ngày công: <span style="color:red">{{ $dataInit['mySalary']['timekeep']['total_salary'] }}</span></label>
    <label class="col-md-3 form-label fw-bold">{{ $income_salary }}</label>
    <label class="col-md-9 form-label fw-bold">C. Các khoảng khác</label>
    <label class="col-md-3 form-label fw-bold">{{ $total_other }}</label>
    <label class="col-md-9 form-label ps-4">C1. Lương KPIs</label>
    <label class="col-md-3 form-label">{{ number_format($dataInit['mySalary']['income']['salary_kpis']) }}</label>
    <label class="col-md-9 form-label ps-4">C2. Lương ngoài giờ</label>
    <label class="col-md-3 form-label">{{ number_format($total_overtime) }}</label>
    <label class="col-md-9 form-label ps-4">C3. Bổ sung lương</label>
    <label class="col-md-3 form-label">{{ number_format($total_adjustments) }}</label>
    <label class="col-md-9 form-label ps-4">C4. Giảm trừ lương</label>
    <label class="col-md-3 form-label">{{ number_format($total_deductions) }}</label>
    <label class="col-md-9 form-label fw-bold">D. Tổng thu nhập (D = B + C )</label>
    <label class="col-md-3 form-label fw-bold">{{ number_format($income_salary + $total_other) }}</label>
    <label class="col-md-9 form-label fw-bold">E. Trích nhập BHXH và thuế TNCN</label>
    <label class="col-md-3 form-label fw-bold">{{ number_format($total_insurance) }}</label>
    <label class="col-md-9 form-label ps-4">E1. BHXH(8%)</label>
    <label class="col-md-3 form-label">{{ number_format($dataInit['mySalary']['insurance']['social_insurance_nv']) }}</label>
    <label class="col-md-9 form-label ps-4">E2. BHYT(1.5%)</label>
    <label class="col-md-3 form-label">{{ $dataInit['mySalary']['insurance']['health_insurance_nv'] }}</label>
    <label class="col-md-9 form-label ps-4">E3. BHTN(1%)</label>
    <label class="col-md-3 form-label">{{ number_format($dataInit['mySalary']['insurance']['unemployment_insurance_nv']) }}</label>
    <label class="col-md-9 form-label ps-4">E4. Thuế TNCN</label>
    <label class="col-md-3 form-label">{{ number_format($dataInit['mySalary']['tax']['total']) }}</label>
    <label class="col-md-9 form-label fw-bold">G. Thực lãnh (G = D - E )</label>
    <label class="col-md-3 form-label fw-bold">{{ number_format($income_salary + $total_other - $total_insurance) }}</label>
    <label class="col-md-9 form-label fw-bold">H. Ghi Chú</label>
    <label class="col-md-3 form-label fw-bold">{{ $dataInit['mySalary']['note'] }}</label> -->
</div>
@endif