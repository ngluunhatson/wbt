<table class="table table-bordered table-nowrap">
    <thead>
        <tr>
            <th scope="col">STT</th>
            <th scope="col">Họ và tên</th>
            <th scope="col">Bộ phận</th>
            <th scope="col">Ngày công</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($dataInit['dataList']))
        @php $i = 1; @endphp
        @foreach($dataInit['dataList'] as $detail)
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $detail['staff'] ? $detail['staff']['full_name'] : 'Chưa Có'}}</td>
            <td>{{ ($detail['staff'] && $detail['staff']['organizational'] && $detail['staff']['organizational']['position']) ? $detail['staff']['organizational']['position']['name'] : "Chưa Có" }}</td>
            <td>
                <input hidden id="workSalary_{{ $detail['id']}}" value="{{ $detail['timekeep']['total_salary'] }}">
                {{ $detail['timekeep']['total_salary']}}
            </td>
            <td>
                <i data-id="{{ $detail['id'] }}"
                    class="btn  btnView ri-eye-fill text-info fs-3">
                </i>
            </td>
        </tr>
        @php $i ++; @endphp
        @endforeach
        @endif
    </tbody>
</table>