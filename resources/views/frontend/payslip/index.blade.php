@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<style>
    table td {
        vertical-align: middle;
    }


    @media (min-width: 992px) {
        .modal-lg {
            max-width: 600px;
        }
    }
</style>
<div class="row">
    <ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">
        @if(auth()->user()->basic_info_id != null)
        <li class="nav-item">
            <a class="nav-link active" data-bs-toggle="tab" href="#myPayslip" role="tab" aria-selected="false">
                Payslip của tôi
            </a>
        </li>
        @endif
        @if(!auth()->user()->hasAnyRole(['specialist', 'staff']))
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="tab" href="#listPayslip" role="tab" aria-selected="false">
                Danh sách Payslip
            </a>
        </li>
        @endif
    </ul>
</div>

@php  
$url_logo = URL::asset('assets/images/logo-ac.png');
@endphp

<!-- Striped Rows -->
<div class="tab-content text-muted">
    <div class="tab-pane @if(auth()->user()->basic_info_id != null) active @endif" id="myPayslip" role="tabpanel">
        <form id="formSearchMyPayslip" method="POST" action="{{ route('payslip.index') }}">
            @csrf
            <div class = "row justify-content-center mb-3">
                <div class="col-xl-3" style = "width:12%">
                    <label for="datePicker" class="form-label">Tháng / năm</label>
                    <input type="text" class="form-control" name="monthYearMyPayslip" id="datePickerMyPayslip" value="{{ old('monthYearMyPayslip', date('m/Y', strtotime(now()) ) ) }}">
                </div>
            </div>
        </form>
        <div class = "row justify-content-center">
            <div class = "card" style = "width:50%">
                <div class = "card-body">
                    <div class = "row">
                        <div style = "width:50%;align-items: center;display: grid;margin-left: -2%;">
                            <h1 style = "font-size:3.5vh;">PHIẾU CHI TIẾT LƯƠNG</h1>
                        </div>
                        <div style = "width:50%">
                            <div class="row justify-content-end">
                                <a class="logo logo-dark"  style ="margin-right:-2%">
                                    <span class="logo-sm">
                                        <img src="{{ $url_logo }}" alt="" style="height: 12vh;">
                                    </span>
                                    <span class="logo-lg">
                                        <img src="{{ $url_logo }}" alt="" style="height: 12vh;">
                                    </span>
                                </a>
                            
                                <a class="logo logo-light" style ="margin-right:-2%">
                                    <span class="logo-sm">
                                        <img src="{{ $url_logo }}" alt="" style="height: 12vh;">
                                    </span>
                                    <span class="logo-lg">
                                        <img src="{{ $url_logo }}" alt="" style="height: 12vh;">
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div class = "contentMyPayslip">
                    @include('frontend.payslip.personal', ['dataInit' => $dataInit])
                </div>
        
                <div class="card-body mt-5 address">
                        <p class="fw-bold m-0">ActionCOACH CBD Firm</p>
                        <p class="m-0">Địa chỉ: CBD Building, 90 - 92 Lê Thị Riêng, Phường Bến Thành, Quận 1, TP.HCM</p>
                        <p class="m-0">Hotline: 1800 8087</p>
                </div>
            </div>
        </div>

             

            
    </div>

    <div class="tab-pane @if(auth()->user()->basic_info_id == null) active @endif" id="listPayslip" role="tabpanel">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <form id="formSearch" method="POST" action="{{ route('payslip.index') }}">
                            @csrf
                            <div class="row gy-3">
                                <div class="col-xl-3">
                                    <label for="borderInput" class="form-label">Phòng</label>
                                    <select name="room_id" id="selectRoom" class="form-select" aria-label="Default select example">
                                        <option value="-1">Tất Cả Các Phòng</option>
                                        @foreach($dataInit['rooms'] as $value)
                                        <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xl-3">
                                    <label for="datePicker" class="form-label">Tháng / năm</label>
                                    <input type="text" class="form-control" name="monthYear" id="datePicker" value="{{ old('monthYear', date('m/Y', strtotime(now()) ) ) }}">
                                </div>
                                <div class="col-xl-2">
                                    <label for="borderInput" class="form-label">Từ ngày:</label>
                                    <input type="text" class="form-control border-dashed" id="startDate" value="{{ $dataInit['startDate']->format('d/m/Y') }}" readonly>
                                </div>
                                <div class="col-xl-2">
                                    <label for="borderInput" class="form-label">Đến ngày:</label>
                                    <input type="text" class="form-control border-dashed" id="endDate" value="{{ $dataInit['endDate']->format('d/m/Y') }}" readonly>
                                </div>
                                <div class="col-xl-2">
                                    <label for="borderInput" class="form-label">Công chuẩn:</label>
                                    <input type="text" class="form-control border-dashed" id="workNumber" value="{{ $dataInit['workNumber'] }}" readonly>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body overflow-auto contentTable">
                        @include('frontend.payslip.detail', ['dataInit' => $dataInit])
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModalPayslip" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Payslip</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="row gy-3">
                    <div class="col-xl-6">
                        <label for="borderInput" class="form-label">Họ và tên</label>
                        <input type="text" class="form-control border-dashed" id="fullName" value="" readonly>
                    </div>
                    <div class="col-xl-6">
                        <label for="borderInput" class="form-label">Bộ phận</label>
                        <input type="text" class="form-control border-dashed" id="teamName" value="" readonly>
                    </div>
                    <div class="col-xl-6">
                        <label for="borderInput" class="form-label">Ngày công</label>
                        <input type="text" class="form-control border-dashed" id="workSalaryPerson" value="" readonly>
                    </div>
                    <div class="col-xl-6">
                        <label for="borderInput" class="form-label">Chức danh</label>
                        <input type="text" class="form-control border-dashed" id="positionName" value="" readonly>
                    </div>
                </div>
                <div class="row mt-3">
                    <label class="col-md-9 form-label fw-bold">A. Chi tiết các khoản</label>
                    <label id="total_detail" class="col-md-3 form-label fw-bold"></label>
                    <label class="col-md-9 form-label ps-4">A.1 Mức lương chính</label>
                    <label id="main_salary" class="col-md-3 form-label"></label>
                    <label class="col-md-9 form-label ps-4">A.2 Các khoảng hỗ trợ khác</label>
                    <label id="other_support" class="col-md-3 form-label"></label>
                    <label class="col-md-9 form-label fw-bold">B. Thu nhập theo ngày công: <span></span></label>
                    <label id="income_salary" class="col-md-3 form-label fw-bold"></label>
                    <label class="col-md-9 form-label fw-bold">C. Các khoảng khác</label>
                    <label id="total_other" class="col-md-3 form-label fw-bold"></label>
                    <label class="col-md-9 form-label ps-4">C1. Lương KPIs</label>
                    <label id="salary_kpi" class="col-md-3 form-label"></label>
                    <label class="col-md-9 form-label ps-4">C2. Lương ngoài giờ</label>
                    <label id="salary_overtime" class="col-md-3 form-label"></label>
                    <label class="col-md-9 form-label ps-4">C3. Bổ sung lương</label>
                    <label id="salary_adjustments" class="col-md-3 form-label"></label>
                    <label class="col-md-9 form-label ps-4">C4. Giảm trừ lương</label>
                    <label id="salary_reduce" class="col-md-3 form-label"></label>
                    <label class="col-md-9 form-label fw-bold">D. Tổng thu nhập (D = B + C )</label>
                    <label id="total_all" class="col-md-3 form-label fw-bold"></label>
                    <label class="col-md-9 form-label fw-bold">E. Trích nhập BHXH và thuế TNCN</label>
                    <label id="total_insurance" class="col-md-3 form-label fw-bold"></label>
                    <label class="col-md-9 form-label ps-4">E1. BHXH(8%)</label>
                    <label id="insurance_social" class="col-md-3 form-label"></label>
                    <label class="col-md-9 form-label ps-4">E2. BHYT(1.5%)</label>
                    <label id="insurance_medical" class="col-md-3 form-label"></label>
                    <label class="col-md-9 form-label ps-4">E3. BHTN(1%)</label>
                    <label id="insurance_unemployment" class="col-md-3 form-label"></label>
                    <label class="col-md-9 form-label ps-4">E4. Thuế TNCN</label>
                    <label id="income_tax" class="col-md-3 form-label"></label>
                    <label class="col-md-9 form-label fw-bold">G. Thực lãnh (G = D - E )</label>
                    <label id="real_salary" class="col-md-3 form-label fw-bold"></label>
                    <label class="col-md-9 form-label fw-bold">H. Ghi Chú</label>
                    <label id="note" class="col-md-3 form-label fw-bold"></label>
                </div>
                <div class="modal-footer mt-3">
                    <a href="javascript:void(0);" class="btn btn-link link-danger fw-medium bg-light" data-bs-dismiss="modal"><i class="ri-close-line me-1 align-middle"></i> Close</a>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    @endsection
    @section('script')
    <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            
            $('#datePicker').datepicker({
                format: 'mm/yyyy',
                startView: "months",
                minViewMode: "months",
            });

            $('#datePickerMyPayslip').datepicker({
                format: 'mm/yyyy',
                startView: "months",
                minViewMode: "months",
            });
            
            $('#datePickerMyPayslip').on('change', function() {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('payslip.loadMyData') }}",
                    data: $('#formSearchMyPayslip').serialize(),
                    success: function(data) {
                        $('.contentMyPayslip').empty();
                        $('.contentMyPayslip').append(data.html)
                        $('#startDateMyPayslip').val(data.startDate)
                        $('#endDateMyPayslip').val(data.endDate)
                        $('#workNumberMyPayslip').val(data.workNumber)
                    }
                });
            })


            getPaySlips();

            $('#selectRoom, #datePicker').on('change', function() {
                getPaySlips();
            })

            function getPaySlips() {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('payslip.loadData') }}",
                    data: $('#formSearch').serialize(),
                    success: function(data) {
                        $('.contentTable').empty();
                        $('.contentTable').append(data.html)
                        $('#startDate').val(data.startDate)
                        $('#endDate').val(data.endDate)
                        $('#workNumber').val(data.workNumber)

                        $('.btnView').on('click', (event) => {
                            var id = $(event.target).data('id');

                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            $.ajax({
                                type: 'GET',
                                url: "{{ route('salary.getSalary', '') }}" + '/' + id,
                                success: function(respone) {
                                  
                                    if (Object.keys(respone).length > 0) {

                                        var workSalary = $('#workSalary_' + id).val();
                                        var workNumber = $('#workNumber').val();

                                        $('#fullName').val(respone.staff.full_name);
                                        $('#workSalaryPerson').val(workSalary);
                                        $('#teamName').val(respone.organizational.team.name);
                                        $('#positionName').val(respone.organizational.position.name);

                                        $('#main_salary').text(respone.support.main_salary);
                                        $('#other_support').text(respone.other_support);
                                        var total_detail = respone.support.main_salary + respone.other_support;
                                        $('#total_detail').text(total_detail);

                                        var income_salary = total_detail / workSalary * workNumber;
                                        $('#income_salary').text(income_salary);

                                        $('#salary_kpi').text(respone.income.salary_kpis);
                                        var total_overtime = respone.income.overtime_salary + respone.allowance.overtime_salary;
                                        $('#salary_overtime').text(total_overtime);
                                        var total_adjustments = respone.income.additional_adjustments + respone.allowance.additional_adjustments;
                                        $('#salary_adjustments').text(total_adjustments);
                                        var total_deductions = respone.income.adjustment_of_deductions + respone.allowance.adjustment_of_deductions;
                                        $('#salary_reduce').text(total_deductions);
                                        var total_other = respone.income.salary_kpis + total_overtime + total_adjustments - total_deductions;
                                        $('#total_other').text(total_other);
                                        $('#total_all').text(income_salary + total_other);
                                        $('#insurance_social').text(respone.insurance.social_insurance_nv);
                                        $('#insurance_medical').text(respone.insurance.health_insurance_nv);
                                        $('#insurance_unemployment').text(respone.insurance.unemployment_insurance_nv);

                                        var total_insurance = respone.insurance.social_insurance_nv +
                                            respone.insurance.health_insurance_nv +
                                            respone.insurance.unemployment_insurance_nv +
                                            respone.tax.total;

                                        $('#income_tax').text(respone.tax.total);
                                        $('#total_insurance').text(total_insurance);


                                        $('#real_salary').text(income_salary + total_other - total_insurance);
                                        $('#note').text(respone.note);

                                        $('#myModalPayslip').modal('show');
                                    } else {
                                        swal.fire({
                                            icon: 'error',
                                            title: 'Bảng Lương chưa được Xác Nhận nên chưa có Payslip',
                                            confirmButtonColor: '#3085d6',
                                            showConfirmButton: false,
                                        });
                                    }
                                },
                            })
                        })
                    },
                });
            }
        });
    </script>
    @endsection