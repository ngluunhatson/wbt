<style>
    .spoiler {
        display: -webkit-box;
        -webkit-line-clamp: 5;
        -webkit-box-orient: vertical;
        overflow: hidden;
        max-height: 420px;
    }

    .hidden-obj {
        display: none;
        visibility: hidden;
    }

    .spoiler-comment {
        display: -webkit-box;
        -webkit-line-clamp: 5;
        -webkit-box-orient: vertical;
        overflow: hidden;
        max-height: 420px;
    }

    .content-comment {
        position: relative;
        padding: 7px 21px 7px 15px;
        max-width: 820px;
        border-radius: 23px;
        background-color: #edf1f3;
        box-sizing: border-box;
        overflow: hidden;
    }

    .spoiler img {
        max-width: 50%;
    }

    .spoiler-comment img {
        max-width: 50%;
    }
</style>
@foreach($blogs as $blog)
<div class="col-xl-12 col-md-8">
    <div class="card form-comment-flag">
        <div class="card-body fs-4 pe-4 ps-4 pt-4 pb-5 comment-check">
            <a href="{{ $blog->link }}" class="card-title fs-3">{{ $blog->title }}</a>
            <div class="col-12 border border-light border-1 p-2 d-flex justify-content-between mt-2 desc-group">
                <span class="d-flex align-middle fw-lighter"> <i class="ri-calendar-2-fill text-danger me-1"></i>{{ Carbon\Carbon::parse($blog->created_at)->isoFormat('DD') }} {{ Carbon\Carbon::parse($blog->created_at)->isoFormat('MMM') }} {{ Carbon\Carbon::parse($blog->created_at)->isoFormat('GGGG') }} {{ Carbon\Carbon::parse($blog->created_at)->toTimeString() }}</span>
                <span class="d-flex align-middle fw-lighter"><i class="ri-user-2-line text-danger me-1"></i>@if (!empty($blog->author)) {{ $blog->author }} @else {{ __('frontend.admin') }} @endif</span>
            </div>
            <div class="mt-3">
                <div class="entry-content" postId="{{ $blog->id }}" id="spoiler">
                    <p>@php echo html_entity_decode($blog->content); @endphp</p>
                </div>
            </div>
        </div>
        <!-- <div class="col-12" style="width: 100%"> -->
        <span class="mdi fs-2 mdi-chevron-down border-dark button-show" postId="{{ $blog->id }}" style="text-align:center; display:block;" type="button"></span>
        <!-- </div> -->
        <div class="ard-body fs-4 pe-4 ps-4">tag: {{ $blog->tag }}</div>
        <div class="card-header">
            <div>
                <ul class="nav nav-tabs-custom rounded card-header-tabs border-bottom-0" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" data-bs-toggle="tab" href="#home-1" role="tab" aria-selected="true">
                            Bình luận
                        </a>
                    </li>
                </ul>
                <!--end nav-->
            </div>
        </div>
        <div class="card-body">
            <div class="tab-content">
                @foreach ($blog->comments as $comment)
                @if($comment->delete_flag == 0)
                <div class="d-flex mb-4">
                    <div class="flex-shrink-0">
                        <img src="{{ asset('assets/images/users/avatar-1.jpg') }}" alt="" class="avatar-xs rounded-circle">
                    </div>

                    <div class="flex-grow-1 ms-3">
                        <div class="content-comment">
                            <h3 class="fs-13" style="color: #286cb3;">{{ $comment->user->name }} <small class="text-muted">{{ $comment->created_at }}</small></h3>
                            <div commentId="{{ $comment->id }}" class="comment-content">{!! $comment->content !!}</div>

                            <span commentId="{{ $comment->id }}" class="mdi fs-2 mdi-chevron-down border-dark button-show-comment" style="text-align:center; display:block;" type="button"></span>
                        </div>
                        <a commentId="{{ $comment->id }}" nameReply="{{ $comment->user->name }}" href="javascript: void(0);" class="badge text-muted bg-light reply-button"><i class="mdi mdi-reply"></i> Phản hồi</a>
                        @if (auth()->user()->hasAnyPermission(['user-delete-comment']) && auth()->user()->id == $comment->user->id)
                        <!-- <div class="badge text-muted bg-light"> -->
                        <a href="{{ route('comment.delete',$comment->id) }}" class="badge text-muted bg-light"><i class="mdi mdi-reply"></i> Xóa</a>
                        <!-- </div> -->
                        @endif
                        <div class="d-flex mt-4 flag">

                        </div>

                        <!-- Relationship: replyComment -->

                        @foreach ($comment->replyComment as $replyComment)
                        <!-- Check delete_flag -->
                        <!-- delete_flag = 0 => show -->

                        @if ($replyComment->delete_flag == 0)
                        <div class="d-flex mt-4">
                            <div class="flex-shrink-0">
                                <img src="{{ asset('assets/images/users/avatar-1.jpg') }}" alt="" class="avatar-xs rounded-circle">
                            </div>
                            <div class="flex-grow-1 ms-3" style="        
                                    position: relative;
                                    padding: 7px 21px 7px 15px;
                                    max-width: 820px;
                                    border-radius: 23px;
                                    background-color: #edf1f3;
                                    box-sizing: border-box;
                                    overflow: hidden;">
                                <h5 class="fs-13" style="color: #286cb3;">{{ $replyComment->user->name }}<small class="text-muted"> {{ $replyComment->created_at }}</small></h5>
                                <p class="text-muted comment-content">{!! $replyComment->content !!}</p>
                                <span commentId="{{ $replyComment->id }}" class="mdi fs-2 mdi-chevron-down border-dark button-show-comment" style="text-align:center; display:block;" type="button"></span>
                            </div>
                        </div>
                        <div class="d-flex mt-4">
                            <div class="flex-shrink-0">
                                <img style="visibility: hidden;" src="{{ asset('assets/images/users/avatar-1.jpg') }}" alt="" class="avatar-xs rounded-circle">
                            </div>
                            <div class="flex-grow-1 ms-3">
                                <a nameReply="{{ $replyComment->user->name }}" commentId="{{ $comment->id }}" href="javascript: void(0);" class="badge text-muted bg-light reply-button"><i class="mdi mdi-reply"></i> Phản hồi</a>
                                <!-- <a href="" class="badge text-muted bg-light"><i class="mdi mdi-reply"></i> Thích</a> -->
                                @if (auth()->user()->hasAnyPermission(['user-delete-comment']) && auth()->user()->id == $replyComment->user->id)
                                <a href="{{ route('replycomment.delete',$replyComment->id) }}" class="badge text-muted bg-light"><i class="mdi mdi-reply"></i> Xóa</a>
                                @endif
                                <div class="d-flex mt-4 flag">
                                </div>
                            </div>

                        </div>
                        <!-- <div class="d-flex mt-4 flag">

                        </div> -->
                        @endif
                        @endforeach

                        <!-- Relationship: replyComment -->
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
        <form method="POST" class="mt-4 formCreateComment" blog_id="{{ $blog->id }}">
            @csrf
            <div class="row g-3">
                <div class="col-lg-12">
                    <textarea name="content" class="form-control bg-light border-light summernote" rows="3" placeholder="Enter comments"></textarea>
                </div>
                <!--end col-->
                <div class="col-12">
                    <input blog_id="{{ $blog->id }}" type="submit" class="btn btn-success" value="Gửi bình luận">
                    <!-- <button type="button" class="btn btn-ghost-secondary btn-icon waves-effect me-1"><i class="ri-attachment-line fs-16"></i></button> -->
                </div>
            </div>
            <!--end row-->
        </form>
    </div>
</div>
@endforeach
<div class="d-flex justify-content-center">
    {{ $blogs->links() }}

</div>