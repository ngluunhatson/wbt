@extends('layouts.fe.master2')
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
@if (count($data['blogs']) > 0)
<div class="row mb-3 d-flex">
  <div class="d-flex align-middle position-relative col-xl-12" style="width: 40%">
    <input autocomplete="off" style="padding-left: 20px;" type="text" class="form-control" placeholder="Tìm kiếm..." autocomplete="off" id="searchText" name="searchPost">
  </div>
  <form id="filterPost" class="d-flex align-middle position-relative col-xl-12" style="width: 60%" enctype="multipart/form-data">
    @csrf
    <input style="width: 30%;  text-align: center;" autocomplete="off" placeholder="mm/yy" type="text" class="form-control data-month" name="month" id="datePicker">
    <!-- <input type="text" name="month" class="form-control active datepickers" style="padding-left: 20px; border-right: none; border-radius:0 !important; background-color:#fff;" id="ends" placeholder="Select month" readonly="readonly"> -->
    <!-- <button type="submit" id='filters' class="btn btn-primary w-100"> <i class="ri-equalizer-fill me-2 align-bottom"></i>Filters</button> -->
  </form>
</div>


<div class="row" id="listData">
  @include('frontend.blog.post.list',['blogs' => $data['blogs']])
</div>

</section>
@else
<div>

</div>
@endif
@endsection
@section('script')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script>
  $(document).ready(function() {
    $('#datePicker').datepicker({
      format: 'mm/yyyy',
      startView: "months",
      minViewMode: "months",
      // startDate: 'toDay',
    });


    $('.button-show').click(function() {
      var postId = $($(this)).attr("postId");
      $(this).remove();
      $(".spoiler").map(function() {
        if ($($(this)).attr("postId") == postId)
          $($(this)).removeClass()
      });
    });

    $('.button-show-comment').click(function() {
      var commentId = $($(this)).attr("commentId");

      $(this).remove();
      $(".spoiler-comment").map(function() {
        if ($($(this)).attr("commentId") == commentId)
          $($(this)).removeClass()
      });

    });

    var path = window.location.pathname.split('/')
    var category = path[path.length - 1]
    $('#searchText').on('keypress', function(event) {
      event.preventDefault();
      // $('#searchText')
      // console.log(event.key)
      $('#searchText').val($('#searchText').val() + event.key)
      var search = $('#searchText').val()
      var url = "{{ route('blog.viewPostSearch', ':category') }}"
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        method: "POST",
        url: url.replace(':category', category),
        data: {
          'searchPost': search
        },
        success: function(response) {
          $('#listData').empty();
          $('#listData').append(response);
          var obj = $('.button-show');
          // var obj = (height.parent().find('.mt-3 .spoiler'))
          $.each(obj, function(key, value) {
            // console.log($(this).parent().find('.mt-3 .entry-content'))
            if ($(this).parent().find('.mt-3 .entry-content').height() < 420) {
              $(this).addClass('hidden-obj');
            } else {
              $(this).parent().find('.mt-3 .entry-content').addClass('spoiler')
            }
          });

          var objj = $('.button-show-comment');
          // var obj = (height.parent().find('.mt-3 .spoiler'))
          $.each(objj, function(key, value) {
            console.log($(this).parent().find('.comment-conten'))
            if ($(this).parent().find('.comment-content').height() < 420) {
              $(this).addClass('hidden-obj');
            } else {
              $(this).parent().find('.comment-content').addClass('spoiler-comment')
            }
          });

        },
        error: function(error) {}
      })

    })

    $('#datePicker').on('change', function() {
         //   event.preventDefault()
      $filter = $('#datePicker').val()
      var url = "{{ route('blog.viewPostFilter', ':category') }}"
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        method: "POST",
        url: url.replace(':category', category),
        data: {
          'month': $filter
        },
        success: function(response) {
          // console.log(response)
          $('#listData').empty();
          $('#listData').append(response);
          var obj = $('.button-show');
          // var obj = (height.parent().find('.mt-3 .spoiler'))
          $.each(obj, function(key, value) {
            // console.log($(this).parent().find('.mt-3 .entry-content'))
            if ($(this).parent().find('.mt-3 .entry-content').height() < 420) {
              $(this).addClass('hidden-obj');
            } else {
              $(this).parent().find('.mt-3 .entry-content').addClass('spoiler')
            }
          });

          var objj = $('.button-show-comment');
          // var obj = (height.parent().find('.mt-3 .spoiler'))
          $.each(objj, function(key, value) {
            // console.log($(this).parent().find('.comment-conten'))
            if ($(this).parent().find('.comment-content').height() < 420) {
              $(this).addClass('hidden-obj');
            } else {
              $(this).parent().find('.comment-content').addClass('spoiler-comment')
            }
          });

        },
        error: function(error) {}
      })
    });

    $('#searchText').on('keydown', function(event) {
      if (event.key == 'Backspace') {
        if ($('#searchText').val().length > 0) {
          // console.log($('#searchText').val()[$('#searchText').val().length-1])
          // console.log($('#searchText').val().length -1)
          $('#searchText').val().slice(0, -1)
          // console.log('#'+$('#searchText').val())
          var search = $('#searchText').val()
          var url = "{{ route('blog.viewPostSearch', ':category') }}"
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
            method: "POST",
            url: url.replace(':category', category),
            data: {
              'searchPost': search
            },
            success: function(response) {
              $('#listData').empty();
              $('#listData').append(response);
              var obj = $('.button-show');
              // var obj = (height.parent().find('.mt-3 .spoiler'))
              $.each(obj, function(key, value) {
                // console.log($(this).parent().find('.mt-3 .entry-content'))
                if ($(this).parent().find('.mt-3 .entry-content').height() < 420) {
                  $(this).addClass('hidden-obj');
                } else {
                  $(this).parent().find('.mt-3 .entry-content').addClass('spoiler')
                }
              });

              var objj = $('.button-show-comment');
              // var obj = (height.parent().find('.mt-3 .spoiler'))
              $.each(objj, function(key, value) {
                // console.log($(this).parent().find('.comment-conten'))
                if ($(this).parent().find('.comment-content').height() < 420) {
                  $(this).addClass('hidden-obj');
                } else {
                  $(this).parent().find('.comment-content').addClass('spoiler-comment')
                }
              });

            },
            error: function(error) {}
          })
        }
      }
    });



    // $('#filterPost').on('submit', function(event) {
    //   event.preventDefault()

    //   var url = "{{ route('blog.viewPostFilter', ':category') }}"
    //   $.ajax({
    //     method: "POST",
    //     url: url.replace(':category', category),
    //     data: $('#filterPost').serialize(),
    //     success: function(response) {
    //       console.log(response)
    //       $('#listData').empty();
    //       $('#listData').append(response);
    //       var obj = $('.button-show');
    //       // var obj = (height.parent().find('.mt-3 .spoiler'))
    //       $.each(obj, function(key, value) {
    //         console.log($(this).parent().find('.mt-3 .entry-content'))
    //         if ($(this).parent().find('.mt-3 .entry-content').height() < 420) {
    //           $(this).addClass('hidden-obj');
    //         } else {
    //           $(this).parent().find('.mt-3 .entry-content').addClass('spoiler')
    //         }
    //       });

    //       var objj = $('.button-show-comment');
    //       // var obj = (height.parent().find('.mt-3 .spoiler'))
    //       $.each(objj, function(key, value) {
    //         console.log($(this).parent().find('.comment-conten'))
    //         if ($(this).parent().find('.comment-content').height() < 420) {
    //           $(this).addClass('hidden-obj');
    //         } else {
    //           $(this).parent().find('.comment-content').addClass('spoiler-comment')
    //         }
    //       });

    //     },
    //     error: function(error) {}
    //   })
    // });

    $('.summernote').summernote({
      height: 150, //set editable area's height
      codemirror: { // codemirror options
        theme: 'monokai'
      },
      toolbar: [
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen']],
      ],
    });

    var obj = $('.button-show');
    // var obj = (height.parent().find('.mt-3 .spoiler'))
    $.each(obj, function(key, value) {
      console.log($(this).parent().find('.mt-3 .entry-content'))
      if ($(this).parent().find('.mt-3 .entry-content').height() < 420) {
        $(this).addClass('hidden-obj');
      } else {
        $(this).parent().find('.mt-3 .entry-content').addClass('spoiler')
      }
    });

    var objj = $('.button-show-comment');
    // var obj = (height.parent().find('.mt-3 .spoiler'))
    $.each(objj, function(key, value) {
      if ($(this).parent().find('.comment-content').height() < 420) {
        $(this).addClass('hidden-obj');
      } else {
        $(this).parent().find('.comment-content').addClass('spoiler-comment')
      }
    });

    // $("#listData").on("load", ".entry-content", function() {
    //   alert('sddssdsddssd');
    //   console.log('da log ');
    //     var height = $('.entry-content').height();
    //     console.log(height)
    //     if(height >420) {
    //       $('.entry-content').addClass('.spoiler');
    //     }
    // });

    //After jquery ajax render page
    $("#listData").on("click", ".button-show", function() {
      var postId = $($(this)).attr("postId");
      $(this).remove();
      $(".spoiler").map(function() {
        if ($($(this)).attr("postId") == postId)
          $($(this)).removeClass()
      });
    });

    $("#listData").on("click", ".button-show-comment", function() {
      var commentId = $($(this)).attr("commentId");

      $(this).remove();
      $(".spoiler-comment").map(function() {
        if ($($(this)).attr("commentId") == commentId)
          $($(this)).removeClass()
      });
    });

    $("#listData").on("click", ".summernote", function() {
      $('.summernote').summernote({
        height: 150, //set editable area's height
        codemirror: { // codemirror options
          theme: 'monokai'
        },
        toolbar: [
          ['insert', ['link', 'picture', 'video']],
          ['view', ['fullscreen']],
        ],
      });
    });

    $("#listData").on("click", ".reply-button", function() {
      // $( ".reply-form" ).empty()
      var temp = $($(this)).parent().find('.flag:last');
      var formCreateComment = $($(this)).parent().find('.reply-form')
      // var formCreateComment = $( ".reply-form" )
      if (formCreateComment.length > 0)
        $(formCreateComment).remove();
      else {
        // Get commentId 
        var commentId = $($(this)).attr("commentId");
        var nameReply = '@' + $($(this)).attr("nameReply");
        // Insert data
        $(`
                        <form method="POST" action="{{url('comment/${commentId}/reply-comment/store')}}" class="mt-4 reply-form" id="formCreateReplyComment">
                            @csrf
                            <div class="row g-3">
                            <div class="col-lg-12">
                                    <textarea name="content" class="form-control bg-light border-light summernote" id="replyPosts" rows="3" placeholder="nội dung..."></textarea>
                                </div>
                                <div class="col-12">
                                    <input type="submit" class="btn btn-success" value="Phản hồi">
                                </div>
                            </div>
                        </form>`)
          .insertAfter(temp);
        $('.summernote').summernote({
          focus: true,
          height: 150, //set editable area's height
          codemirror: { // codemirror options
            theme: 'monokai'
          }
        });
        // console.log($(temp).parent().find(".summernote"))
        $(temp).parent().find(".summernote").summernote('pasteHTML', `<span style='color:red; font-weight:bold;'>${nameReply},</span>`);
        $(temp).parent().find(".summernote").summernote('pasteHTML', `<span style='color:black;'>&nbsp;</span>`);
        $('.note-toolbar').hide();
        // temp.scrollIntoView()
        // $('.note-resizebar').hide();
      }
    });

  })

  // $(".datepickers").datepicker({
  //     format: "mm-yyyy",
  //     startView: "months",
  //     minViewMode: "months"
  //   });
  $('#listData').on('submit', '.formCreateComment', function(event) {
    event.preventDefault()
    var data = new FormData($(this)[0]);
    var path = window.location.pathname.split('/')
    data.append('blog_id', $(this).attr('blog_id'))

    $.ajax({
      method: "POST",
      url: "{{ route('comment.store') }}",
      data: data,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response) {
        if (response.status) {
          toastr.success(response.message);
          window.location.reload();
        } else {
          toastr.error(response.message);
        }

      },
      error: function(error) {
        handleFails(error);
      }
    })
  })


  function handleFails(response) {
    $('#selectRoom, #selectTeam, #selectPosition, #locationEditor, #targetEditor, #recruitEditor, #capabilityEditor, #functionEditor, #missionEditor, #wageEditor, #kpisEditor, #reportEditor, #powerEditor, #bonusEditor, #discEditor').removeClass('is-invalid')
    if (typeof response.responseJSON.errors != "undefined") {
      var keys = Object.keys(response.responseJSON.errors);
      $('#formUpdatePost').find(".has-error").find(".help-block").remove();
      $('#formUpdatePost').find(".has-error").removeClass("has-error");

      for (var i = 0; i < keys.length; i++) {
        // Escape dot that comes with error in array fields
        var key = keys[i].replace(".", '\\.');
        var formarray = keys[i];

        // If the response has form array
        if (formarray.indexOf('.') > 0) {
          var array = formarray.split('.');
          response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
          key = array[0] + '[' + array[1] + ']';
        }

        var ele = $('#formUpdatePost').find("[name='" + key + "']");

        ele.addClass('is-invalid')

        var grp = ele.closest(".form-group");
        $(grp).find(".help-block").remove();

        //check if wysihtml5 editor exist
        var wys = $(grp).find(".note-editor note-frame card").length;

        if (wys > 0) {
          var helpBlockContainer = $(grp);
        } else {
          var helpBlockContainer = $(grp).find("div:first");
        }
        if ($(ele).is(':radio')) {
          helpBlockContainer = $(grp);
        }

        if (helpBlockContainer.length == 0) {
          helpBlockContainer = $(grp);
        }

        helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response.responseJSON.errors[keys[i]] + '</div>');
        $(grp).addClass("has-error");
      }

      if (keys.length > 0) {
        var element = $("[name='" + keys[0] + "']");
        if (element.length > 0) {
          $("html, body").animate({
            scrollTop: element.offset().top - 150
          }, 200);
        }
      }
    }
  }
</script>
@endsection