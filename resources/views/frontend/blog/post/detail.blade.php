<style>
    .spoiler {
        display: -webkit-box;
        -webkit-line-clamp: 5;
        -webkit-box-orient: vertical;
        overflow: hidden;
    }
</style>
@extends('layouts.fe.master2')
@section('content')
<div class="row">
    <div class="col-xl-12 col-md-12">
        <div class="row">
            <div class="col-12" style="height: 50vh; width: 100%">
                @if (!empty($data['blog']->blog_image))
                <img style="width: 100%; height: 100%" src="{{ url($data['blog']->blog_image) }}" class="img-fluid" alt="blog image">
                @else
                <img style="width: 100%; max-height: 100%" src="{{ asset('uploads/img/dummy/no-img.png') }}" class="img-fluid" alt="blog image">
                @endif
            </div>
            <div class="col-12">
                <div class="d-flex fs-4 mt-3">
                    <div class="d-flex align-middle me-2">
                        <i style="color:#B73232" class="ri-calendar-2-fill me-1"></i>
                        <span class="fw-light">{{ Carbon\Carbon::parse($data['blog']->created_at)->isoFormat('DD') }} {{ Carbon\Carbon::parse($data['blog']->created_at)->isoFormat('MMM') }} {{ Carbon\Carbon::parse($data['blog']->created_at)->isoFormat('GGGG') }}</span>
                    </div>
                    <div class="d-flex align-middle me-2">
                        <i style="color:#B73232" class="ri-user-2-line me-1"></i>
                        <span class="fw-light">{{$data['blog']->author}}</span>
                    </div>
                </div>
                <div class="mt-3">
                    <h3><a>{{$data['blog']->title}}</a></h3>
                    <div class="entry-content spoiler" id="spoiler">
                        <p>@php echo html_entity_decode($data['blog']->content); @endphp</p>
                    </div>
                </div>
                <div class="col-12" style="width: 100%">
                    <span class="mdi fs-2 mdi-chevron-down border-dark button-show" style="text-align:center; display:block;" type="button"></span>
                </div>

                <div class="card">
                    <div class="card-header">
                        <div>
                            <ul class="nav nav-tabs-custom rounded card-header-tabs border-bottom-0" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" data-bs-toggle="tab" href="#home-1" role="tab" aria-selected="true">
                                        Bình luận ({{ count($data['comment']) }})
                                    </a>
                                </li>
                            </ul>
                            <!--end nav-->
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="home-1" role="tabpanel">
                                @foreach ($data['comment'] as $comments)
                                <div class="d-flex mb-4">
                                    <div class="flex-shrink-0">
                                        <img src="{{ asset('assets/images/users/avatar-1.jpg') }}" alt="" class="avatar-xs rounded-circle">
                                    </div>
                                    <div class="flex-grow-1 ms-3">
                                        <h5 class="fs-13">{{ $comments['user']->name }} <small class="text-muted">{{ $comments->created_at }}</small></h5>
                                        <div class="text-muted">{!! $comments['content'] !!}</div>
                                        <a commentId="{{ $comments['id'] }}" href="javascript: void(0);" class="badge text-muted bg-light reply-button"><i class="mdi mdi-reply"></i> Phản hồi</a>
                                        @if (auth()->user()->hasAnyPermission(['user-delete-comment']) && auth()->user()->id == $comments['user']->id)
                                        <!-- <div class="badge text-muted bg-light"> -->
                                        <a href="{{ route('comment.delete',$comments['id']) }}" class="badge text-muted bg-light"><i class="mdi mdi-reply"></i> Xóa</a>
                                        <!-- </div> -->
                                        @endif
                                        <div class="d-flex mt-4 flag"></div>

                                        <!-- Relationship: replyComment -->

                                        @foreach ($comments['replyComment'] as $replyComment)
                                        <!-- Check delete_flag -->
                                        <!-- delete_flag = 0 => show -->

                                        @if ($replyComment['delete_flag'] == 0)
                                        <div class="d-flex mt-4 flag">
                                            <div class="flex-shrink-0">
                                                <img src="{{ asset('assets/images/users/avatar-1.jpg') }}" alt="" class="avatar-xs rounded-circle">
                                            </div>
                                            <div class="flex-grow-1 ms-3">
                                                <h5 class="fs-13">{{ $replyComment->user->name }}<small class="text-muted"> {{ $replyComment->created_at }}</small></h5>
                                                <p class="text-muted">{!! $replyComment['content'] !!}</p>
                                            </div>
                                        </div>
                                        <div class="d-flex mt-4 flag">
                                            <a href="{{ route('comment.delete',$comments['id']) }}" class="badge text-muted bg-light"><i class="mdi mdi-reply"></i> Phản hồi</a>
                                            <a href="{{ route('comment.delete',$comments['id']) }}" class="badge text-muted bg-light"><i class="mdi mdi-reply"></i> Thích</a>
                                            <a href="{{ route('comment.delete',$comments['id']) }}" class="badge text-muted bg-light"><i class="mdi mdi-reply"></i> Xóa</a>
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                                <form method="POST" class="mt-4" id="formCreateComment">
                                    @csrf
                                    <div class="row g-3">
                                        <div class="col-lg-12">
                                            <textarea name="content" class="form-control bg-light border-light" id="summernote" rows="3" placeholder="Enter comments"></textarea>
                                        </div>
                                        <!--end col-->
                                        <div class="col-12">
                                            <input type="submit" class="btn btn-success" value="Gửi bình luận">
                                            <!-- <button type="button" class="btn btn-ghost-secondary btn-icon waves-effect me-1"><i class="ri-attachment-line fs-16"></i></button> -->
                                        </div>
                                    </div>
                                    <!--end row-->
                                </form>
                            </div>
                            <!--end tab-pane-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('script')
    
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 150, //set editable area's height
                codemirror: { // codemirror options
                    theme: 'monokai'
                }
            });
            $('.note-toolbar').hide();
            // $('.note-resizebar').hide();

            // $('#replyPosts').summernote();


            // $('#replyPosts').summernote({
            //     toolbar: [
            //         // [groupName, [list of button]]
            //         ['style', ['bold', 'italic', 'underline', 'clear']],
            //         ['font', ['strikethrough', 'superscript', 'subscript']],
            //         ['fontsize', ['fontsize']],
            //         ['color', ['color']],
            //         ['para', ['ul', 'ol', 'paragraph']],
            //         ['height', ['height']]
            //     ]
            // });


            $('#formCreateComment').on('submit', function(event) {
                event.preventDefault()
                var data = new FormData($(this)[0]);
                var path = window.location.pathname.split('/')
                data.append('blog_id', path[path.length - 1])

                $.ajax({
                    method: "POST",
                    url: "{{ route('comment.store') }}",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if (response.status) {
                            toastr.success(response.message);
                            window.location.reload();
                        } else {
                            toastr.error(response.message);
                        }

                    },
                    error: function(error) {
                        handleFails(error);
                    }
                })
            })


            function handleFails(response) {
                $('#selectRoom, #selectTeam, #selectPosition, #locationEditor, #targetEditor, #recruitEditor, #capabilityEditor, #functionEditor, #missionEditor, #wageEditor, #kpisEditor, #reportEditor, #powerEditor, #bonusEditor, #discEditor').removeClass('is-invalid')
                if (typeof response.responseJSON.errors != "undefined") {
                    var keys = Object.keys(response.responseJSON.errors);
                    $('#formUpdatePost').find(".has-error").find(".help-block").remove();
                    $('#formUpdatePost').find(".has-error").removeClass("has-error");

                    for (var i = 0; i < keys.length; i++) {
                        // Escape dot that comes with error in array fields
                        var key = keys[i].replace(".", '\\.');
                        var formarray = keys[i];

                        // If the response has form array
                        if (formarray.indexOf('.') > 0) {
                            var array = formarray.split('.');
                            response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                            key = array[0] + '[' + array[1] + ']';
                        }

                        var ele = $('#formUpdatePost').find("[name='" + key + "']");

                        ele.addClass('is-invalid')

                        var grp = ele.closest(".form-group");
                        $(grp).find(".help-block").remove();

                        //check if wysihtml5 editor exist
                        var wys = $(grp).find(".note-editor note-frame card").length;

                        if (wys > 0) {
                            var helpBlockContainer = $(grp);
                        } else {
                            var helpBlockContainer = $(grp).find("div:first");
                        }
                        if ($(ele).is(':radio')) {
                            helpBlockContainer = $(grp);
                        }

                        if (helpBlockContainer.length == 0) {
                            helpBlockContainer = $(grp);
                        }

                        helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response.responseJSON.errors[keys[i]] + '</div>');
                        $(grp).addClass("has-error");
                    }

                    if (keys.length > 0) {
                        var element = $("[name='" + keys[0] + "']");
                        if (element.length > 0) {
                            $("html, body").animate({
                                scrollTop: element.offset().top - 150
                            }, 200);
                        }
                    }
                }
            }
            $('.button-show').click(function() {
                $(this).remove();
                $('.spoiler').removeClass();
            });
            $('.reply-button').click(function() {
                // $( ".reply-form" ).empty()
                var temp = $($(this)).parent().find('.flag:last');
                var formCreateComment = $($(this)).parent().find('.reply-form')
                // var formCreateComment = $( ".reply-form" )
                if (formCreateComment.length > 0)
                    $(formCreateComment).remove();
                else {
                    // Get commentId 
                    var commentId = $($(this)).attr("commentId");
                    // Insert data
                    $(`
                        <form method="POST" action="{{ url('comment/${commentId}/reply-comment/store') }}" class="mt-4 reply-form" id="formCreateReplyComment">
                            @csrf
                            <div class="row g-3">
                            <div class="col-lg-12">
                                    <textarea name="content" class="form-control bg-light border-light summernote" id="replyPosts" rows="3" placeholder="nội dung..."></textarea>
                                </div>
                                <div class="col-12">
                                    <input type="submit" class="btn btn-success" value="Phản hồi">
                                </div>
                            </div>
                        </form>`)
                        .insertAfter(temp);
                    $('.summernote').summernote({
                        height: 150, //set editable area's height
                        codemirror: { // codemirror options
                            theme: 'monokai'
                        }
                    });
                    $('.note-toolbar').hide();
                    // $('.note-resizebar').hide();
                }
            });
        })
    </script>
    @endsection