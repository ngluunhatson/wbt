@extends('layouts.fe.master2')



@section('content')

<style>
    .disabled {
        background-color: #898989;
        color: #ffffff;
        opacity: 0.5;
        pointer-events: none;
    }

    textarea {
        min-height: 250px;
    }
</style>

<!-- Include Alert Blade -->

<!-- Form row -->
<div class="row">
    <div class="col-xl-12 box-margin height-card">
        <div class="card card-body">
            <form id="formInsertPost" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="row">
                        <div class="card">
                            <div class="card-header">
                                <div class="form-check form-switch form-switch-lg" dir="ltr">
                                    <input name="all_company" type="checkbox" class="form-check-input" id="select_all">
                                    <h3 class="form-check-label" for="customSwitchsizelg">Toàn bộ công ty</h3>
                                </div>
                            </div>
                            <div class="row card-body group_select">
                                <div class="col-xl-3">
                                    <label class="form-label">Phòng</label>
                                    <select id="selectRoom" name="room_id" class="form-select mb-3 @error('room_id') is-invalid @enderror" aria-label="Default select example">
                                        <option value="">Vui lòng chọn phòng ban</option>
                                        @foreach($dataInit['rooms'] as $room)
                                        <option @if(old('room_id')==$room['id']) selected @endif value="{{ $room['id'] }}">{{ $room['name'] }}</option>
                                        @endforeach
                                    </select>
                                    @error('room_id')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                                <div class="col-xl-3">
                                    <label class="form-label">Bộ phận</label>
                                    <select id="selectTeam" name="team_id" class="form-select mb-3" aria-label="Default select example">
                                    </select>
                                </div>
                                <div class="col-xl-3">
                                    <label class="form-label">Chức danh</label>
                                    <select id="selectPosition" name="position_id" class="form-select mb-3" aria-label="Default select example">
                                    </select>
                                </div>
                                <div class="col-xl-3">
                                    <label class="form-label">Mã nhân viên | Họ và tên</label>
                                    <select id="selectStaff" name="basic_info_id" class="form-select mb-3" aria-label="Default select example">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-12">
                        <div class="form-group">
                            <label for="image">Hình ảnh (kích thước 775x575)(.svg, .png, .jpg, .jpeg)</label>
                            <input id="image" name="blog_image" type="file" class="form-control-file">
                            <!-- <small id="image" class="form-text text-muted">{{ __('content.recommended_size') }}</small> -->
                        <!--</div>
                    </div> -->
                    <div class="col-md-12 mt-4">
                        <div class="form-group form-group-default">
                            <label for="category">Thể loại <span class="text-red">*</span></label>
                            <select class="form-control" name="category_id" id="category" required>
                                @foreach($dataInit['categories'] as $category)
                                <option value="{{$category['id']}}">{{$category['category_name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="form-group">
                            <label for="title">Tiêu đề <span class="text-red">*</span></label>
                            <input id="title" name="title" type="text" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="form-group">
                            <label for="summernote">Mô tả <span class="text-red">*</span></label>
                            <textarea id="summernote" name="content" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="form-group">
                            <label for="tag">Tag</label>
                            <input name="tag" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="form-group">
                            <label for="author">Tác giả</label>
                            <input id="author" name="author" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="form-group form-group-default">
                            <label for="status">Trạng thái</label>
                            <select class="form-control" name="status">
                                <option value="1">Hiển thị</option>
                                <option value="0">khoá</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12 mt-5">
                        <button type="submit" class="btn btn-primary mr-2">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end row -->

@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#formInsertPost').on('submit', function(event) {
            event.preventDefault();

            var data = new FormData($(this)[0]);
            $.ajax({
                method: "POST",
                url: "{{ route('blog.storeInsert') }}",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                        window.location.href = response.link
                    } else {
                        toastr.error(response.message);
                    }

                },
                error: function(error) {
                    handleFails(error);
                }
            })
        })

        function handleFails(response) {
            $('#selectRoom, #selectTeam, #selectPosition, #locationEditor, #targetEditor, #recruitEditor, #capabilityEditor, #functionEditor, #missionEditor, #wageEditor, #kpisEditor, #reportEditor, #powerEditor, #bonusEditor, #discEditor').removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formInsert').find(".has-error").find(".help-block").remove();
                $('#formInsert').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formInsertPost').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response.responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }

        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            $('#selectTeam')
                .find('option')
                .remove();
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();
            if (id != '') {
                var url = "{!! route('blog.getTeamByRommID', ':id') !!}";

                $.ajax({
                    type: 'GET',
                    url: url.replace(':id', id),
                    success: function(data) {
                        if (data.length > 0) {
                            $('#selectTeam')
                                .find('option')
                                .remove()
                            $('#selectTeam').append($('<option>', {
                                value: 0,
                                text: 'Tất cả phòng ban'
                            }));
                            data.forEach(function(value) {
                                $('#selectTeam').append($('<option>', {
                                    value: value.id,
                                    text: value.name
                                }));
                            })
                        } else {
                            $('#selectTeam')
                                .find('option')
                                .remove()

                            if ($('#selectRoom').val() != 0) {
                                $('#selectTeam').append($('<option>', {
                                    value: 0,
                                    text: 'Tất cả phòng ban'
                                }));
                            }
                        }
                    }
                });
            }
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            var url = "{!! route('blog.getPositionByTeamID', ':id') !!}";
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();
            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectPosition')
                            .find('option')
                            .remove();
                        $('#selectPosition').append($('<option>', {
                            value: 0,
                            text: 'Tất cả chức danh'
                        }));

                        data.forEach(function(value) {
                            $('#selectPosition').append($('<option>', {
                                value: value.id,
                                text: value.name
                            }));
                        })
                    } else {
                        $('#selectPosition')
                            .find('option')
                            .remove();

                        if ($('#selectTeam').val() != 0) {
                            $('#selectPosition').append($('<option>', {
                                value: 0,
                                text: 'Tất cả chức danh'
                            }));
                        }

                    }
                }
            });
        });

        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();

            var url = "{!! route('blog.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";


            $('#selectStaff')
                .find('option')
                .remove();

            $.ajax({
                type: 'GET',
                url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectStaff').append($('<option>', {
                            value: 0,
                            text: 'Tất cả nhân viên'
                        }));
                        data.forEach(function(employee) {
                            $('#selectStaff').append($('<option>', {
                                value: employee['id'],
                                text: `${employee["employee_code"]} | ${employee['full_name']}`,
                            }));
                        })
                    } else {
                        $('#selectStaff')
                            .find('option')
                            .remove();

                        if ($('#selectPosition').val() != 0) {
                            $('#selectStaff').append($('<option>', {
                                value: 0,
                                text: 'Tất cả nhân viên'
                            }));
                        }
                    }
                }
            });
        });

        $('#select_all').change(function() {
            if ($(this).is(":checked")) {
                $('.group_select').addClass('disabled');
            } else {
                $('.group_select').removeClass('disabled');
            }
        });

    });
</script>
@endsection