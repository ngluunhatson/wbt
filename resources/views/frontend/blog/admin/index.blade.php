@extends('layouts.fe.master2')
@section('title') @lang('translation.starter') @endsection

@section('content')
<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h3 class="d-inline m-0">Danh sách bài viết</h3>
        <a href="/blog/create">
            <button type="button" class="btn btn-success btn-label waves-effect waves-light"><i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm</button>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <!-- <th>Hình ảnh</th> -->
                            <th>Tiêu đề</th>
                            <th>Loại</th>
                            <th>Ngày đăng</th>
                            <th>View</th>
                            <th>Trạng thái</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dataInit['blogs'] as $index =>$blog)
                        <tr>
                            <td class="dtr-control">{{ $index + 1 }}</td>
                            <!-- <td>
                                @if(!empty($blog->blog_image))
                                    <img style="width: 35%; height: 20%" src="{{ url($blog->blog_image) }}" />
                                @else
                                    <img style="width: 35%; height: 20%" src="{{ asset('uploads/img/dummy/no-img.png') }}" alt="blog image">
                                @endif
                            </td> -->
                            <td>{{ $blog->title }}</td>
                            <td>{{ $blog->category->category_name }}</td>
                            <td>{{ Carbon\Carbon::parse($blog->created_at)->format('d.m.Y') }}</td>
                            <td>10</td>
                            <td>
                                @if ($blog->status == 0)
                                <span class="badge bg-danger">disable</span>
                                @else
                                <span class="badge bg-success">enable</span>
                                @endif

                            </td>
                            <td>
                                <div class="dropdown d-inline-block">
                                    <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="ri-more-fill align-middle"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-end">
                                        <!-- @can('personal-read-1') -->
                                        <!-- <li><a href="#" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> Xem</a></li> -->
                                        <!-- @endcan -->
                                        <!-- @can('personal-edit') -->
                                        <li>
                                            <a href="/blog/edit/{{$blog->id}}" class="dropdown-item edit-item-btn">
                                                <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Sửa</a>
                                        </li>
                                        <!-- @endcan -->
                                        <!-- @can('personal-delete') -->
                                        <li>
                                            <a href="/blog/delete/{{ $blog->id }}" class="dropdown-item remove-item-btn">
                                                <i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Xóa
                                            </a>
                                        </li>
                                        <!-- @endcan -->
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--end col-->
</div>
<!--end row-->

@endsection
@section('script')
<script>
    $(function() {
        var myTable = $('#example').dataTable({
            fixedHeader: true
        });

        $("#checkAll").click(function() {
            $(".form-check-input").prop('checked', $(this).prop("checked"));
        });
    })
</script>
@endsection