@extends('layouts.fe.master2')



@section('content')
<style>
    .disabled {
        background-color: #898989;
        color: #ffffff;
        opacity: 0.5;
        pointer-events: none;
    }
</style>

<!-- Form row -->
<div class="row">
    <div class="col-xl-12 box-margin height-card">
        <div class="card card-body">
            <h4 class="card-title">Chỉnh sửa bài viết</h4>
            <form method="POST" id="formUpdatePost" enctype="multipart/form-data">
                @csrf
                <div class="row">
                <div class="row">
                            <div class="card">
                                <!-- <label for="blog_image">Quyền xem bài viết</label> -->
                                <div class="card-header">
                                    <div class="form-check form-switch form-switch-lg" dir="ltr">
                                        @if($dataInit['blog']->all_company == 1)
                                        <input name="all_company" type="checkbox" class="form-check-input" id="select_all" checked>
                                        @else
                                        <input name="all_company" type="checkbox" class="form-check-input" id="select_all" @endif <h3 class="form-check-label" for="customSwitchsizelg">Toàn bộ công ty</h3>
                                    </div>
                                </div>

                                @if($dataInit['blog']->all_company == 1)
                                <div class="row card-body group_select disabled">
                                    @elseif($dataInit['blog']->all_company == 0)
                                    <div class="row card-body group_select">
                                        @endif
                                        <div class="row card-body group_select">
                                            <div class="col-xl-3">
                                                <label class="form-label">Phòng</label>
                                                <select id="selectRoom" name="room_id" class="form-select mb-3 @error('room_id') is-invalid @enderror" aria-label="Default select example">
                                                <option @if(isset($dataInit['blog']->room_id) && $dataInit['blog']->room_id == 0) selected @endif value="{{ NULL }}">Tất cả phòng ban</option>    
                                                @foreach($dataInit['rooms'] as $room)
                                                    <option @if($dataInit['blog']->room_id == $room['id']) selected @endif value="{{ $room['id'] }}">{{ $room['name'] }}</option>
                                                    @endforeach
                                                </select>
                                                @error('room_id')
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror

                                            </div>
                                            <div class="col-xl-3">
                                                <label class="form-label">Bộ phận</label>
                                                <select id="selectTeam" name="team_id" class="form-select mb-3" aria-label="Default select example">
                                                    @if(isset($dataInit['blog']->team_id))
                                                    <option @if(isset($dataInit['blog']->team_id) && $dataInit['blog']->team_id == 0) selected @endif value="{{ 0 }}">Tất cả phòng ban</option>
                                                    @foreach($dataInit['teams'] as $team)
                                                    @if(isset($team))
                                                    <option @if($dataInit['blog']->team_id == $team['id']) selected @endif value="{{ $team['id'] }}">{{ $team['name'] }}</option>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-xl-3">
                                                <label class="form-label">Chức danh</label>
                                                <select id="selectPosition" name="position_id" class="form-select mb-3" aria-label="Default select example">
                                                    @if(isset($dataInit['blog']->position_id))
                                                    <option value="{{ 0 }}">Tất cả chức danh</option>
                                                    @foreach($dataInit['positions'] as $position)
                                                    @if(isset($position))
                                                    <option @if($dataInit['blog']->position_id == $position['id']) selected @endif value="{{ $position['id'] }}">{{ $position['name'] }}</option>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-xl-3">
                                                <label class="form-label">Mã nhân viên | Họ và tên</label>
                                                <select id="selectStaff" name="basic_info_id" class="form-select mb-3" aria-label="Default select example">
                                                    @if(isset($dataInit['blog']->basic_info_id))
                                                    <option value="{{ 0 }}">Tất cả nhân viên</option>
                                                    @foreach($dataInit['staffs'] as $staff)
                                                    @if(isset($staff))
                                                    <option @if($dataInit['blog']->basic_info_id == $staff['id']) selected @endif value="{{ $staff['id'] }}"> {{ $staff["employee_code"] .' | '. $staff['full_name'] }} </option>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end col-->
                            </div>
                        </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="category" class="col-form-label">Thể loại<span class="text-red">*</span></label>
                            <select class="form-control" name="category_id" id="category" required>
                                @foreach ($dataInit['categories'] as $category)
                                <option value="{{ $category['id']}}" {{ $category['id'] === $dataInit['blog']->category_id ? 'selected' : '' }}>{{ $category['category_name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="form-group">
                            <label for="title">Tiêu đề<span class="text-red">*</span></label>
                            <input id="title" name="title" type="text" class="form-control" value="{{ $dataInit['blog']->title }}" required>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="form-group">
                            <label for="summernote">Mô tả<span class="text-red">*</span></label>
                            <textarea id="summernote" name="content" class="form-control">@php echo html_entity_decode($dataInit['blog']->content); @endphp</textarea>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="form-group">
                            <label for="tag">Tag</label>
                            <input name="tag" type="text" class="form-control" value={{ $dataInit['blog']->tag }}>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="form-group">
                            <label for="author">Tác giả</label>
                            <input id="author" name="author" type="text" class="form-control" value="{{ $dataInit['blog']->author }}">
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <div class="form-group">
                            <label for="status" class="col-form-label">Trạng thái</label>
                            <select class="form-control" name="status">
                                <option value="1" {{ $dataInit['blog']->status === 1 ? 'selected' : '' }}>Hiển thị</option>
                                <option value="0" {{ $dataInit['blog']->status === 0 ? 'selected' : '' }}>Khoá</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 mt-4">
                        <!-- <div class="form-group">
                            <label for="blog_image">Hình ảnh (kích thước 775x575)(.svg, .png, .jpg, .jpeg)</label>
                            <input type="file" name="blog_image" class="form-control-file" id="blog_image">
                            <!-- <small id="blog_image" class="form-text text-muted">{{ __('content.recommended_size') }}</small> -->
                        <!--</div>
                        <div class="height-card box-margin mt-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="avatar-area text-center">
                                        <div class="media">
                                            @if (!empty($dataInit['blog']->blog_image))
                                            <a class="d-block mx-auto" href="#" data-toggle="tooltip" data-placement="top" data-original-title="Hình ảnh hiện tại">
                                                <img src="{{ url($dataInit['blog']->blog_image) }}" alt="blog image" class="rounded w-25">
                                            </a>
                                            @else
                                            <a class="d-block mx-auto" href="#" data-toggle="tooltip" data-placement="top" data-original-title="Không có hình ảnh được tạo">
                                                <img src="{{ asset('uploads/img/dummy/no-img.png') }}" alt="no image" class="rounded w-25">
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                    <!--end card-body-->
                                <!--</div>
                            </div>
                            <!--end card-->
                        <!--</div> -->

                        <div class="col-md-12 mt-4">
                                <button type="submit" class="btn btn-primary mr-2">Lưu</button>
                            </div>
            </form>
        </div>
    </div>
</div>
<!-- end row -->

@endsection

@section('script')
<script>
    $(document).ready(function() {
        // $('#locationEditor, #targetEditor, #recruitEditor, #capabilityEditor, #functionEditor, #missionEditor, #wageEditor, #kpisEditor, #reportEditor, #powerEditor, #bonusEditor, #discEditor')
        //     .summernote({
        //         toolbar: [
        //             ['fontsize', ['fontsize']],
        //             ['style', ['style']],
        //             ['font', ['bold', 'italic', 'underline', 'clear']],
        //             ['fontname', ['fontname']],
        //             ['color', ['color']],
        //             ['para', ['ul', 'ol', 'paragraph']],
        //             ['height', ['height']],
        //             ['table', ['table']],
        //             ['insert', ['link', 'picture', 'hr']],
        //             ['view', ['fullscreen', 'codeview']],
        //             ['help', ['help']]
        //         ],
        //         height: 400,
        //     });



        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            $('#selectTeam')
                .find('option')
                .remove();
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();
            if (id != '') {
                var url = "{!! route('blog.getTeamByRommID', ':id') !!}";

                $.ajax({
                    type: 'GET',
                    url: url.replace(':id', id),
                    success: function(data) {
                        if (data.length > 0) {
                            $('#selectTeam')
                                .find('option')
                                .remove()
                            $('#selectTeam').append($('<option>', {
                                value: 0,
                                text: 'Tất cả phòng ban'
                            }));
                            data.forEach(function(value) {
                                $('#selectTeam').append($('<option>', {
                                    value: value.id,
                                    text: value.name
                                }));
                            })
                        } else {
                            $('#selectTeam')
                                .find('option')
                                .remove()

                            if ($('#selectRoom').val() != 0) {
                                $('#selectTeam').append($('<option>', {
                                    value: 0,
                                    text: 'Tất cả phòng ban'
                                }));
                            }
                        }
                    }
                });
            }
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            var url = "{!! route('blog.getPositionByTeamID', ':id') !!}";
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();
            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectPosition')
                            .find('option')
                            .remove();
                        $('#selectPosition').append($('<option>', {
                            value: 0,
                            text: 'Tất cả chức danh'
                        }));

                        data.forEach(function(value) {
                            $('#selectPosition').append($('<option>', {
                                value: value.id,
                                text: value.name
                            }));
                        })
                    } else {
                        $('#selectPosition')
                            .find('option')
                            .remove();

                        if ($('#selectTeam').val() != 0) {
                            $('#selectPosition').append($('<option>', {
                                value: 0,
                                text: 'Tất cả chức danh'
                            }));
                        }

                    }
                }
            });
        });

        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();

            var url = "{!! route('blog.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";


            $('#selectStaff')
                .find('option')
                .remove();

            $.ajax({
                type: 'GET',
                url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectStaff').append($('<option>', {
                            value: 0,
                            text: 'Tất cả nhân viên'
                        }));
                        data.forEach(function(employee) {
                            $('#selectStaff').append($('<option>', {
                                value: employee['id'],
                                text: `${employee["employee_code"]} | ${employee['full_name']}`,
                            }));
                        })
                    } else {
                        $('#selectStaff')
                            .find('option')
                            .remove();

                        if ($('#selectPosition').val() != 0) {
                            $('#selectStaff').append($('<option>', {
                                value: 0,
                                text: 'Tất cả nhân viên'
                            }));
                        }
                    }
                }
            });
        });




        $('#formUpdatePost').on('submit', function(event) {
            event.preventDefault()
            var data = new FormData($(this)[0]);
            var path = window.location.pathname.split('/')
            data.append('id', path[path.length - 1])
            $.ajax({
                method: "POST",
                url: "{{ route('blog.storeUpdate') }}",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                        window.location.href = response.link
                    } else {
                        toastr.error(response.message);
                    }

                },
                error: function(error) {
                    handleFails(error);
                }
            })
        })

        $('#select_all').change(function() {
            if ($(this).is(":checked")) {
                $('.group_select').addClass('disabled');
            } else {
                $('.group_select').removeClass('disabled');
            }
        });

        function handleFails(response) {
            $('#selectRoom, #selectTeam, #selectPosition, #locationEditor, #targetEditor, #recruitEditor, #capabilityEditor, #functionEditor, #missionEditor, #wageEditor, #kpisEditor, #reportEditor, #powerEditor, #bonusEditor, #discEditor').removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formUpdatePost').find(".has-error").find(".help-block").remove();
                $('#formUpdatePost').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formUpdatePost').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response.responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }


    })
</script>
@endsection