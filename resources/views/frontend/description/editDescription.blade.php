@extends('layouts.fe.master2')
@section('title')
    @lang('translation.editDescription')
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/quill/quill.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">  {{-- thuộc tính redonly --}}
@endsection

@section('content')
    <div class="row ">
        <h3 class="text-center">MÔ TẢ CÔNG VIỆC</h3>
    </div>
    <!--end col Quốc tịch-->

    <div class="row ">
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <label for="room" class="form-label">Phòng</label>
                    <div class="col-xl-12">
                        <select class="form-select" aria-label="Default select example">
                            <option selected>QTKD</option>
                            <option value="3">QTKD</option>
                            <option value="1">IT</option>
                            <option value="2">3</option>
                            <option value="3">4</option>
                            <option value="3">5</option>
                            <option value="3">6</option>
                            <option value="3">7</option>
                            <option value="3">8</option>
                            <option value="3">9</option>
                            <option value="3">10</option>
                        </select>
    
                    </div>
                </div>
            </div>
            <!--end col Phòng-->
    
            <div class="col-4">
                <div class="mb-3">
                    <label for="part" class="form-label">Bộ phận</label>
                    <div class="col-xl-12">
                        <select class="form-select" aria-label="Default select example">
                            <option selected>Kinh doanh sản phẩm</option>
                            <option value="3">1</option>
                            <option value="1">2</option>
                            <option value="2">3</option>
                            <option value="3">4</option>
                            <option value="3">5</option>
                            <option value="3">6</option>
                            <option value="3">7</option>
                            <option value="3">8</option>
                            <option value="3">9</option>
                            <option value="3">10</option>
                        </select>
    
                    </div>
                </div>
            </div>
            <!--end col Bộ phận-->
    
            <div class="col-4">
                <div class="mb-3">
                    <label for="position" class="form-label">Chức danh</label>
                    <div class="col-xl-12">
                        <select class="form-select" aria-label="Default select example">
                            <option selected>Quản trị chăm sóc khách hàng</option>
                            <option value="3">1</option>
                            <option value="1">2</option>
                            <option value="2">3</option>
                            <option value="3">4</option>
                            <option value="3">5</option>
                            <option value="3">6</option>
                            <option value="3">7</option>
                            <option value="3">8</option>
                            <option value="3">9</option>
                            <option value="3">10</option>
                        </select>
    
                    </div>
                </div>
            </div>
            <!--end col Chức danh-->
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="nav flex-column nav-pills text-start" id="v-pills-tab" role="tablist"
                                    aria-orientation="vertical">
                                    <a class="nav-link mb-2  active" id="position-tab" data-bs-toggle="pill"
                                        href="#position" role="tab" aria-controls="position"
                                        aria-selected="true">1.&nbsp Vị
                                        trí</a>
                                    <a class="nav-link mb-2" id="function-tab" data-bs-toggle="pill" href="#function"
                                        role="tab" aria-controls="function" aria-selected="false">2. Chức năng</a>
                                    <a class="nav-link mb-2" id="mission-tab" data-bs-toggle="pill" href="#mission"
                                        role="tab" aria-controls="mission" aria-selected="false">3. Nhiệm vụ</a>
                                    <a class="nav-link mb-2" id="power-tab" data-bs-toggle="pill" href="#power"
                                        role="tab" aria-controls="power" aria-selected="false">4. Quyền hạn</a>
                                </div>
                            </div><!-- end col -->
                            <div class="col-md-10">
                                <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="position" role="tabpanel"
                                        aria-labelledby="position-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body">
                                                <div class="bubble-editor readonly" style="height: 400px;">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                                                        ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                                        aliquip ex ea commodo consequat. Duis aute irure dolor in
                                                        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                                        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                                        culpa qui officia deserunt mollit anim id est laborum</p>
                                                </div> <!-- end bubble-editor-->
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="function" role="tabpanel"
                                        aria-labelledby="function-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body">
                                                <div class="bubble-editor readonly" style="height: 400px;">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                                                        ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                                        aliquip ex ea commodo consequat. Duis aute irure dolor in
                                                        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                                        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                                        culpa qui officia deserunt mollit anim id est laborum</p>
                                                </div> <!-- end bubble-editor-->
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="mission" role="tabpanel"
                                        aria-labelledby="mission-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body">
                                                <div class="bubble-editor readonly" style="height: 400px;">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                                                        ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                                        aliquip ex ea commodo consequat. Duis aute irure dolor in
                                                        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                                        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                                        culpa qui officia deserunt mollit anim id est laborum</p>
                                                </div> <!-- end bubble-editor-->
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="power" role="tabpanel"
                                        aria-labelledby="power-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Văn bản </h4>
                                            </div><!-- end card header -->
                                            <div class="card-body">
                                                <div class="bubble-editor readonly" style="height: 400px;">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
                                                        ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                                        aliquip ex ea commodo consequat. Duis aute irure dolor in
                                                        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                                        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                                        culpa qui officia deserunt mollit anim id est laborum</p>
                                                </div> <!-- end bubble-editor-->
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                </div>
                            </div><!--  end col -->
                        </div>
                        <!--end row-->
                    </div><!-- end card-body -->
                </div><!-- end card -->
            </div>
            <!--end col-->
        </div>
        <!--end row-->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row ">
                            <div class="col-3 d-flex justify-content-center">
                                <div class="row  ">
                                    <div class="col-12 text-center">
                                        <h5>Người giao việc</h5>
                                    </div>
                                    <div class="col-12 text-center">
                                        <label for="firstNameinput" class="form-label mb-0">Nguyễn Văn A</label>
                                    </div>
                                    <div class="col-12 text-center mt-2">
                                        <label for="firstNameinput" class="form-label">Đã xác nhận</label>
                                    </div>
                                </div>
                            </div>
                            <!--end col Người giao việc-->
                
                            <div class="col-3 d-flex justify-content-center">
                                <div class="row  ">
                                    <div class="col-12 text-center">
                                        <h5>Người nhận việc</h5>
                                    </div>
                                    <div class="col-12 text-center">
                                        <label for="firstNameinput" class="form-label mb-0">Nguyễn Văn A</label>
                                    </div>
                                    <div class="col-12 text-center mt-2">
                                        <label for="firstNameinput" class="form-label">Chờ xác nhận</label>
                                    </div>
                                </div>
                            </div>
                            <!--end col Người nhận việc-->
                
                            <div class="col-3 d-flex justify-content-center">
                                <div class="row  ">
                                    <div class="col-12 text-center">
                                        <h5>P.QT HC-NS</h5>
                                    </div>
                                    <div class="col-12 text-center">
                                        <label for="firstNameinput" class="form-label mb-0">Nguyễn Văn A</label>
                                    </div>
                                    <div class="col-12 text-center mt-2">
                                        <label for="firstNameinput" class="form-label">Đã xác nhận</label>
                                    </div>
                                </div>
                            </div>
                            <!--end col P.QT HC-NS-->
                
                            <div class="col-3 d-flex justify-content-center">
                                <div class="row  ">
                                    <div class="col-12 text-center">
                                        <h5>Ban Giám Đốc</h5>
                                    </div>
                                    <div class="col-12 text-center">
                                        <label for="firstNameinput" class="form-label mb-0">Nguyễn Văn A</label>
                                    </div>
                                    <div class="col-12 text-center mt-2">
                                        <label for="firstNameinput" class="form-label">Chờ xác nhận</label>
                                    </div>
                                </div>
                            </div>
                            <!--end col Ban giám đốc-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/pages/pricing.init.js') }}"></script>

    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>

    <script src="{{ URL::asset('assets/libs/@ckeditor/@ckeditor.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/quill/quill.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-editor.init.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
