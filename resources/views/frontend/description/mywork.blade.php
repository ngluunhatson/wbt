<style>
    .table td p {
        margin: auto;
    }

    .table>:not(caption)>*>* {
        padding: 6px;
    }

    .table th {
        font-weight: 700;
        text-align: center;
    }

    @print {
        @page :footer {
            display: none
        }

        @page :header {
            display: none
        }
    }

    @media print {
        .address {
            margin-top: 72% !important;
        }
    }
</style>
<div class="d-flex justify-content-between">
    <div>
        <h3 class="text-center mt-4 fw-bold">12 TIÊU CHÍ CHO MỘT VỊ TRÍ</h3>
        <div class="">
            <label for="room" class="form-label col-4">Phòng</label>
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['criteria']['room']) ? $dataInit['criteria']['room']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div>
            <label for="room" class="form-label col-4">Bộ phận</label>
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['criteria']['team']) ? $dataInit['criteria']['team']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div>
            <label for="room" class="form-label col-4">Chức danh</label>
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['criteria']['position']) ? $dataInit['criteria']['position']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div class="col-sm-5">
        </div>
    </div>
    <div style="width:35% ; height: fit-content;">
        <img style="object-fit: contain; width: 100%;height: 100%;" src="{{ url('/uploads/common/logo.png') }}">
    </div>
</div>
<div class="row card-body">
    <table class="table table-bordered border-dark dt-responsive nowrap align-middle" style="width:100%; background: white;">
        <thead>
            <tr>
                <th>STT</th>
                <th>HẠNG MỤC</th>
                <th>NỘI DUNG</th>
            </tr>
        </thead>
        <tbody>
            @php
            $i = 1;
            $arrayHeader = ['location', 'function', 'mission', 'power'];
            @endphp
            @foreach(config('constants.header_criteria') as $key => $value)
            <tr>
                @if(in_array($key, $arrayHeader))
                <td class="text-center" style="width: 7%;">{{ $i }}</td>
                <td style="width: 20%;">{{ $value }}</td>
                <td>{!! $dataInit['criteria'][$key] !!}</td>
                @php $i ++; @endphp
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-3 d-flex justify-content-center">
                        <div class="row ">
                            <div class="col-12 text-center">
                                <h5 class="fw-bold">Người giao việc</h5>
                            </div>
                            <div class="col-12 text-center">
                                <label for="firstNameinput" class="form-label mb-0">{{ $dataInit['criteria']['user']['name'] }}</label>
                            </div>
                            <div class="col-12 text-center mt-2">
                                <label for="firstNameinput" class="form-label">{{ $dataInit['criteria']['status'] != 1 ? 'Đã xác nhận' : 'Chưa xác nhận' }}</label>
                            </div>
                        </div>
                    </div>
                    <!--end col Người giao việc-->
                    @php
                    $idConfirmer = [];
                    $id;
                    $idConfirmerNext;
                    $type;
                    @endphp
                    @foreach( $dataInit['criteria']['confirms'] as $key => $value)
                    <input name="confirm_id_{{$key+1}}" hidden value="{{ $value['id'] }}">
                    <div class="col-3 d-flex justify-content-center">
                        <div class="row">
                            <div class="col-12 text-center">
                                @switch($value['confirm']['type_confirm'])
                                @case(1)
                                <h5 class="fw-bold">Người nhận việc</h5>
                                @break
                                @case(2)
                                @if ($value['confirm']['status'] == 0)
                                @php
                                $idConfirmer[] = $value['confirm']['basic_info_id'];
                                $id = $value['confirm']['id'];
                                $type = 2;
                                @endphp
                                @endif
                                <h5 class="fw-bold">P.QT HC-NS</h5>
                                @break
                                @case(3)
                                @php
                                $idConfirmerNext = $value['confirm']['basic_info_id'];
                                @endphp
                                @if (empty($idConfirmer) && $value['confirm']['status'] == 0)
                                @php
                                $idConfirmer[] = $value['confirm']['basic_info_id'];
                                $id = $value['confirm']['id'];
                                $type = 3;
                                $idConfirmerNext = '';
                                @endphp
                                @endif
                                <h5 class="fw-bold">Ban Giám Đốc</h5>
                                @break
                                @endswitch
                            </div>
                            <div class="col-12 text-center">
                                @switch($value['confirm']['type_confirm'])
                                @case(1)
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] ?? 'Chưa có' }}</label>
                                @break
                                @case(2)
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] }}</label>
                                @break
                                @case(3)
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] }}</label>
                                @break
                                @endswitch
                            </div>
                            <div class="col-12 text-center mt-2">
                                @if($value['confirm']['type_confirm'] != 1)
                                @switch($value['confirm']['status'])
                                @case(0)
                                <label for="firstNameinput" class="form-label mb-0">{{ 'Chưa xác nhận' }}</label>
                                @break
                                @case(1)
                                <label for="firstNameinput" class="form-label mb-0">{{ 'Đã xác nhận' }}</label>
                                @break
                                @case(2)
                                <label for="firstNameinput" class="form-label mb-0">{{ 'Từ chối' }}</label>
                                <div>
                                    <label>Lý do : {{ $value['confirm']['note']}}</label>
                                </div>
                                @break
                                @endswitch
                                @else
                                <label for="firstNameinput" class="form-label mb-0"></label>
                                @endif
                            </div>
                        </div>
                    </div>


                    @endforeach
                    @if (in_array(auth()->user()->basic_info_id, $idConfirmer))
                    <form method="POST" action="{{ route('criteria.updateStatus', $dataInit['criteria']['id']) }}" id="formConfirm">
                        @csrf
                        <input hidden name="confirmID" value="{{ $id }}">
                        <input hidden name="confimerIDNext" value="{{ $idConfirmerNext }}">
                        <input hidden name="reason" id="reasonConfirm">
                        <input hidden name="statusConfirm" id="statusConfirm" value="1">
                        <input hidden name="type" value="{{ $type }}">
                        <div class="row justify-content-center pe-3 mb-3 mt-3">
                            <div class="col-3 justify-content-between d-flex">
                                <button data-id="1" type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content">
                                    <i class="ri-check-double-line label-icon align-middle fs-16 me-2"></i> Duyệt</button>
                                <button data-id="2" type="submit" id="btnDenie" class="btn btn-danger waves-effect waves-light" style="width: fit-content">
                                    <i class="ri-error-warning-line label-icon align-middle rounded-pill fs-16 me-2 "></i> Không Duyệt</button>
                            </div>
                        </div>
                    </form>
                    @endif
                    <!--end col Ban giám đốc-->
                </div>
            </div>
            <div class="card-body mt-5 address">
                <p class="fw-bold m-0">ActionCOACH CBD Firm</p>
                <p class="m-0">Địa chỉ: CBD Building, 90 - 92 Lê Thị Riêng, Phường Bến Thành, Quận 1, TP.HCM</p>
                <p class="m-0">Hotline: 1800 8087</p>
            </div>
        </div>
    </div>
</div>