@extends('layouts.fe.master2')
@section('title')
@lang('translation.description')
@endsection
@section('css')
@endsection
@section('content')
<style>
    @print {
        @page :footer {
            display: none
        }

        @page :header {
            display: none
        }
    }

    @media print {
        .btn {
            display: none
        }

        .nav {
            display: none;
        }

        #titlePage {
            display: none;
        }

        .address {
            margin-top: 10% !important;
        }

        #btnPrint {
            display: none;
        }
    }
</style>
<div class="row" id="titlePage">
    <h3>MÔ TẢ CÔNG VIỆC</h3>
</div>
<!--end row-->

<div class="card-body">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">
        @if (!auth()->user()->hasRole('super-admin'))
        <li class="nav-item">
            <a class="nav-link active" data-bs-toggle="tab" href="#my-work" role="tab" aria-selected="false">
                Công việc của tôi
            </a>
        </li>
        @endif
        @if(!auth()->user()->hasAnyRole(['specialist', 'staff']))
        <li class="nav-item">
            @if (auth()->user()->hasRole('super-admin'))
            <a class="nav-link active" data-bs-toggle="tab" href="#assigned-work" role="tab" aria-selected="false">
                Công việc đã giao
            </a>
            @else
            <a class="nav-link" data-bs-toggle="tab" href="#assigned-work" role="tab" aria-selected="false">
                Công việc đã giao
            </a>
            @endif
        </li>
        @endif
    </ul>
    <div class="tab-content text-muted">
        @if (!auth()->user()->hasRole('super-admin'))
        <div class="tab-pane active" id="my-work" role="tabpanel">
            @if (array_key_exists('myWork', $dataInit) && !empty($dataInit['myWork']))
            <div class="col-12 d-flex justify-content-between mb-3">
                <h3 class="d-inline m-0"></h3>
                <a href="#">
                    <button type="button" id="btnPrint" class="btn btn-primary btn-label waves-effect waves-light">
                        <i class="ri-printer-line label-icon align-middle fs-16 me-2"></i>IN</button>
                </a>
            </div>
            <div class="row">
                <div class="card">
                    <div id="myWork" class="card-body">
                        @include('frontend.description.mywork', ['dataInit' => $dataInit['myWork']])
                    </div>
                </div>
            </div>
            @endif
        </div>
        @endif
        @if(!auth()->user()->hasAnyRole(['specialist', 'staff']))
        @if (auth()->user()->hasRole('super-admin'))
        <div class="tab-pane active" id="assigned-work" role="tabpanel">
        @else
        <div class="tab-pane" id="assigned-work" role="tabpanel">
        @endif
            <div class="row">
                <div class="card">
                    <div class="card-body">
                        <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 10px;">
                                        <div class="form-check">
                                            <input class="form-check-input fs-15" type="checkbox" id="checkAll" value="option">
                                        </div>
                                    </th>
                                    <th>STT</th>
                                    <th>Phòng</th>
                                    <th>Bộ Phận</th>
                                    <th>Chức Danh</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dataInit['listData'] as $key => $value)
                                @if($value -> status == 4)
                                <tr>
                                    <th scope="row">
                                        <div class="form-check">
                                            <input class="form-check-input fs-15" type="checkbox" name="checkAll" value="option1">
                                        </div>
                                    </th>
                                    <td class="dtr-control">{{ $key + 1 }}</td>
                                    <td>{{ $value->room->name }}</td>
                                    <td>{{ $value->team->name }}</td>
                                    <td>{{ $value->position->name }}</td>
                                    <td>
                                        <div class="dropdown d-inline-block">
                                            <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                <i class="ri-more-fill align-middle"></i>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-end">
                                                <li><a href={{ route('description.view', $value->id ) }} class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
<!-- end row -->
@endsection
{{-- @section('script')
    <script src="{{ URL::asset('assets/js/pages/pricing.init.js') }}"></script>
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<!-- listjs init -->
<script src="{{ URL::asset('assets/js/pages/listjs.init.js') }}"></script>
<script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>

@endsection --}}
@section('script')
<script>
    $(function() {
        $('#example').dataTable({
            fixedHeader: true
        });
        $('#btnPrint').on('click', function() {
            window.print()
        })
    })
</script>
@endsection