@php
    
    $colorList = [
        (object) [
            'color' => 'alert-yellow',
            'border' => 'border-yellow',
        ],
        (object) [
            'color' => 'alert-warning',
            'border' => 'border-warning',
        ],
        (object) [
            'color' => 'alert-danger',
            'border' => 'border-danger',
        ],
        (object) [
            'color' => 'alert-info',
            'border' => 'border-info',
        ],
        (object) [
            'color' => 'alert-success',
            'border' => 'border-success',
        ],
    ];
    
    $array = [
        [
            (object) [
                'title' => 'CÁC CƠ HỘI',
                'row' => 6,
            ],
            (object) [
                'title' => 'CHIẾN LƯỢC TRONG 1 CÂU',
                'row' => 6,
            ],
            (object) [
                'title' => 'MỤC TIÊU 1 NĂM',
                'row' => 3,
            ],
            (object) [
                'title' => 'CÁC ƯU TIÊN TRONG 1 NĂM',
                'row' => 6,
            ],
            (object) [
                'title' => 'MỤC TIÊU QUÝ',
                'row' => 3,
            ],
            (object) [
                'title' => 'CÁC ƯU TIÊN TRONG QUÝ',
                'row' => 6,
            ],
        ],
    
        [
            (object) [
                'title' => 'KHÁCH HÀNG CỐT LÕI',
                'row' => 6,
            ],
            (object) [
                'title' => 'USP/LỜI HỨA THƯƠNG HIỆU',
                'row' => 20,
            ],
            (object) [
                'title' => 'TIÊU CHUẨN KHÁCH HÀNG TRUNG THÀNH',
                'row' => 20,
            ],
        ],
    
        [
            (object) [
                'title' => 'KẾ HOẠCH PHÁT TRIỂN BẢN THÂN',
                'row' => 30,
            ],
            (object) [
                'title' => 'KẾ HOẠCH PHÁT TRIỂN ĐỘI NGỦ',
                'row' => 21,
            ],
        ],
    
        [
            (object) [
                'title' => 'CHỈ SỐ KPI CỦA TÔI',
                'row' => 6,
            ],
            (object) [
                'title' => 'KPI DÀNH CHO ĐỘI NGŨ',
                'row' => 10,
            ],
            (object) [
                'title' => 'LỊCH LÀM VIỆC CÁ NHÂN',
                'row' => 11,
            ],
            (object) [
                'title' => 'LỊCH LÀM VIỆC CỦA ĐỘI NGŨ',
                'row' => 13,
            ],
        ],
    
        [
            (object) [
                'title' => 'MỤC ĐÍCH',
                'row' => 14,
            ],
            (object) [
                'title' => 'GIÁ TRỊ CỐT LÕI',
                'row' => 26,
            ],
            (object) [
                'title' => 'ĐÓNG GÓP CHO CỘNG ĐỒNG',
                'row' => 6,
            ],
        ],
    ];
@endphp

@extends('layouts.fe.master2')
@section('title')
    @lang('translation.animation')
@endsection
@section('content')
    <style>
        .img-contain {
            width: 100%;
            height: 100%;
            object-fit: contain;
        }

        .roadmap-img {
            max-height: 350px;
        }

        .roadmap-group {
            justify-content: space-between;
        }

        .alert-yellow {
            background-color: #fad057;
            border-color: #fad057;
            color: #fff;
        }

        .border-yellow {
            border-color: #fad057 !important;
        }
    </style>
    <form action="{{ route('roadmap.update') }}" method="post">
        @csrf
        <div class="col-12 d-flex justify-content-between mb-4">
            <h3 class="d-inline m-0"></h3>
            <button type="submit" class="btn btn-success btn-label waves-effect waves-light edit-btn"><i
                    class="ri-pencil-fill label-icon align-middle fs-16 me-2"></i>Sửa</button>
        </div>
        <div class="row gy-4">
            <div class="col-xxl-4 d-flex flex-column roadmap-group">
                <div class="col-xxl-12 roadmap-img">
                    <img src="/assets/images/roadmap/roadmapcircle.png" class="img-contain">
                </div>
                <h3 class="col-xxl-12 alert alert-dark text-center mb-0" role="alert">
                    ROAD MAP FOR SUCCESS
                </h3>
            </div>
            <div class="col-xxl-8">
                <div class="row gy-4">
                    <div class="col-xxl-6 col-md-12">
                        <div>
    
                            <label for="labelInput" class="form-label">Họ và tên</label>
                            <select class="form-control" name="name" id="selectName" required>
                             
                                @foreach ($dataInit['directorate'] as $directorate)

                                    <option @if(isset($dataInit['plan10y'][0]->full_name) && $dataInit['plan10y'][0]->full_name == $directorate->staff->full_name) selected @endif position="{{ $directorate->position->name }}"
                                        valuePosition="{{ $directorate->position->name }}"
                                        company="{{ $directorate->staff->company }}"
                                        valueCompany="{{ $directorate->staff->company }}"
                                        value="{{ $directorate->staff->full_name }}"
                                        start="{{ isset($directorate->laborContract->first()->form_date_labor) ? \Carbon\Carbon::createFromFormat('Y-m-d', $directorate->laborContract->first()->form_date_labor)->format('d/m/Y') : 'hiện tại'}}"
                                        end="{{ isset($directorate->laborContract->last()->from_date_qdtv) ? \Carbon\Carbon::createFromFormat('Y-m-d', $directorate->laborContract->last()->from_date_qdtv)->format('d/m/Y') : 'hiện tại'  }}"
                                        >
                                        {{ $directorate->staff->full_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xxl-6 col-md-12">
                        <div>
                            <label for="labelInput" class="form-label">Công ty</label>
                            <select class="form-control" name="company" id="selectCompany" required>
                                @foreach ($dataInit['directorate'] as $directorate)
                                    <!-- if() -->
                                    <option value="{{ $directorate->staff->company }}">{{ $directorate->staff->company }}
                                    </option>
                                @break
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xxl-6 col-md-6">
                    <div>
                        <label for="labelInput" class="form-label">Chức vụ</label>
                        <select class="form-control" name="position" id="selectPosition" required>
                            @foreach ($dataInit['directorate'] as $directorate)
                                <option  value="{{ $directorate->position->name }}">{{ $directorate->position->name }}
                                </option>
                                @break
                            @endforeach
                        </select>
                    </div>
                </div>
                  
                <!-- @foreach ($dataInit['directorate'][0]->laborContract as $labor)
                    {{ $labor->from_date_labor }}
                @endforeach -->
                <div class="col-xxl-3 col-md-6">
                    <div>
                        <label for="exampleInputdate" class="form-label">Từ năm</label>
                        @if (isset($dataInit['directorate'][0]->laborContract->first()->form_date_labor))
                            <input type="text" name="start" class="form-control" id="selectStart"
                                value="{{ \Carbon\Carbon::createFromFormat('Y-m-d', $dataInit['directorate'][0]->laborContract->first()->form_date_labor)->format('d/m/Y') ?? 'hiện tại'}}"
                                readonly>
                        @else
                            <input type="text" name="start"  class="form-control" id="selectStart"
                                value="hiện tại" readonly>
                        @endif
                    </div>
                </div>
                <div class="col-xxl-3 col-md-6">
                    <div>
                        <label for="exampleInputdate" class="form-label">Đến năm</label>
                        @php
                            $flag = 0;
                        @endphp
                        {{-- {{dd($dataInit['plan10y'])}} --}}
                        {{-- {{ dd($dataInit['directorate'][0]->laborContract->last()->no_qdtv) }} --}}
                        {{-- @foreach ($dataInit['directorate'][0]->laborContract->reverse() as $labor) --}}
                        @if ( isset($dataInit['directorate'][0]->laborContract->last()->from_date_qdtv))
                            <input name="end" class="form-control" id="selectEnd"
                                value="{{ \Carbon\Carbon::createFromFormat('Y-m-d',$labor->from_date_qdtv)->format('d/m/Y') }}" readonly>
                            @php
                                $flag = 1;
                            @endphp
                        @endif
                        @if ($flag == 0)
                            <input name="end" class="form-control" id="selectEnd"
                                value="hiện tại" readonly>
                        @endif
                    </div>
                </div>
                {{-- {{dd($dataInit['plan10y']['0'])}} --}}
                <div class="col-xxl-4 col-md-6 ">
                    <h5 class="alert alert-warning alert-solid text-center" role="alert">
                        MỤC TIÊU 10 NĂM (BHAG:)
                    </h5>
                    <textarea class="form-control" name="ten_year_goal" id="exampleFormControlTextarea5" rows="8">{{ $dataInit['plan10y']['0']->ten_year_goal ?? '' }}</textarea>
                </div>
                <div class="col-xxl-4 col-md-6 ">
                    <h5 class="alert alert-warning alert-solid text-center" role="alert">
                        MỤC TIÊU 3 NĂM
                    </h5>
                    <textarea class="form-control" name="three_year_goal" id="exampleFormControlTextarea5" rows="8">{{ $dataInit['plan10y']['0']->three_year_goal ?? '' }}</textarea>
                </div>
                <div class="col-xxl-4 col-md-6 ">
                    <h5 class="alert alert-warning alert-solid text-center" role="alert">
                        3 - 5 ƯU TIÊN TRONG 3 NĂM:
                    </h5>
                    <textarea class="form-control" name="priority_in_three_years" id="exampleFormControlTextarea5" rows="8">{{ $dataInit['plan10y']['0']->priority_in_three_years ?? '' }}</textarea>
                </div>
            </div>
        </div>
        <div class=" row row-col-12 mt-3">
            <img class="img-contain" src="/assets/images/roadmap/roadmap.png" alt="image-contain">
        </div>

        <div class="row row-cols-5 row-cols-sm-3 row-cols-lg-5 mt-3">
            @foreach ($array as $key => $arr)
                <div class="col">
                    @foreach ($arr as $a)
                        <h5 class="alert {{ $colorList[$key]->color }} alert-solid text-center mt-3 mb-1"
                            role="alert"
                            style="height: 66.6px; display: flex;align-items: center;justify-content: center;">
                            {{ $a->title }}
                        </h5>
                        @if ($a->title == 'CÁC CƠ HỘI')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="opportunities"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->opportunity->opportunities ?? '' }}</textarea>
                        @elseif($a->title == 'CHIẾN LƯỢC TRONG 1 CÂU')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="strategy_in_one_sentence"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->opportunity->strategy_in_one_sentence ?? '' }}</textarea>
                        @elseif($a->title == 'MỤC TIÊU 1 NĂM')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="one_year_goal"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->opportunity->one_year_goal ?? '' }}</textarea>
                        @elseif($a->title == 'CÁC ƯU TIÊN TRONG 1 NĂM')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="priorities_for_one_yea"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->opportunity->priorities_for_one_year ?? '' }}</textarea>
                        @elseif($a->title == 'MỤC TIÊU QUÝ')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="quarterly_goals"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->opportunity->quarterly_goals ?? '' }}</textarea>
                        @elseif($a->title == 'CÁC ƯU TIÊN TRONG QUÝ')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="priorities_for_the_quarter"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->opportunity->priorities_for_the_quarter ?? '' }}</textarea>
                        @elseif($a->title == 'KHÁCH HÀNG CỐT LÕI')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="core_customers"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->businessDevelopment->core_customers ?? '' }}</textarea>
                        @elseif($a->title == 'USP/LỜI HỨA THƯƠNG HIỆU')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="brand_promise"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->businessDevelopment->brand_promise ?? '' }}</textarea>
                        @elseif($a->title == 'TIÊU CHUẨN KHÁCH HÀNG TRUNG THÀNH')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="customer_standards" style="height: 410px;"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->businessDevelopment->customer_standards ?? '' }}</textarea>
                        @elseif($a->title == 'USP/LỜI HỨA THƯƠNG HIỆU')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="brand_promise"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->businessDevelopment->brand_promise ?? '' }}</textarea>
                        @elseif($a->title == 'KẾ HOẠCH PHÁT TRIỂN BẢN THÂN')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="personal_development_plan"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->personnel->personal_development_plan ?? '' }}</textarea>
                        @elseif($a->title == 'KẾ HOẠCH PHÁT TRIỂN ĐỘI NGỦ')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="team_development_plan" style="height: 437px"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->personnel->language_development_plan ?? '' }}</textarea>
                        @elseif($a->title == 'CHỈ SỐ KPI CỦA TÔI')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="my_kpis"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->enforcement->my_kpis ?? '' }}</textarea>
                        @elseif($a->title == 'KPI DÀNH CHO ĐỘI NGŨ')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="team_kpis"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->enforcement->team_kpis ?? '' }}</textarea>
                        @elseif($a->title == 'LỊCH LÀM VIỆC CÁ NHÂN')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="personal_work_schedul"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->enforcement->personal_work_schedule ?? '' }}</textarea>
                        @elseif($a->title == 'LỊCH LÀM VIỆC CỦA ĐỘI NGŨ')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="team_work_schedule" style="height: 286px;"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->enforcement->team_work_schedule ?? '' }}</textarea>
                        @elseif($a->title == 'MỤC ĐÍCH')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="purpose"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->mission->purpose ?? '' }}</textarea>
                        @elseif($a->title == 'GIÁ TRỊ CỐT LÕI')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="core_values"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->mission->core_values ?? '' }}</textarea>
                        @elseif($a->title == 'ĐÓNG GÓP CHO CỘNG ĐỒNG')
                            <textarea class="form-control border {{ $colorList[$key]->border }} " name="community_contribution"
                                id="exampleFormControlTextarea5" rows={{ $a->row }}>{{ $dataInit['plan10y']['0']->mission->community_contribution ?? '' }}</textarea>
                        @endif
                    @endforeach
                </div>
            @endforeach
            {{-- enforcement
        mission --}}
        </div>
</form>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        @if (!auth()->user()->hasAnyPermission(['roadmap-update']))
            $('textarea').attr('readonly', true);
            $('select').attr('disabled', true);
            $('.edit-btn').hide();
        @endif

        var company = $('#selectName').find(":selected").attr('company')
            var valueCompany = $('#selectName').find(":selected").attr('valueCompany')
            var valuePosition = $('#selectName').find(":selected").attr('valuePosition')
            var position = $('#selectName').find(":selected").attr('position')
            var start = $('#selectName').find(":selected").attr('start')
            var end = $('#selectName').find(":selected").attr('end')

            $('#selectCompany').find('option').remove()
            $('#selectCompany').append($('<option>', {
                value: valueCompany,
                text: company,
            }));

            $('#selectPosition').find('option').remove()
            $('#selectPosition').append($('<option>', {
                value: valuePosition,
                text: position,
            }));

            $('#selectStart').val(start)

            $('#selectEnd').val(end)

        $('#selectName').on('change', function() {

            var company = $('#selectName').find(":selected").attr('company')
            var valueCompany = $('#selectName').find(":selected").attr('valueCompany')
            var valuePosition = $('#selectName').find(":selected").attr('valuePosition')
            var position = $('#selectName').find(":selected").attr('position')
            var start = $('#selectName').find(":selected").attr('start')
            var end = $('#selectName').find(":selected").attr('end')

            $('#selectCompany').find('option').remove()
            $('#selectCompany').append($('<option>', {
                value: valueCompany,
                text: company,
            }));

            $('#selectPosition').find('option').remove()
            $('#selectPosition').append($('<option>', {
                value: valuePosition,
                text: position,
            }));

            $('#selectStart').val(start)

            $('#selectEnd').val(end)

        });
    });
</script>
@endsection
