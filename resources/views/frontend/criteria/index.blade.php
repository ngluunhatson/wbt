@extends('layouts.fe.master2')
@section('title')
    @lang('translation.criteria')
@endsection
@section('content')
    <style>
        @print {
            @page :footer {
                display: none
            }

            @page :header {
                display: none
            }
        }

        @media print {
            .btn {
                display: none
            }

            .nav {
                display: none;
            }

            #titlePage {
                display: none;
            }

            .address {
                margin-top: 18% !important;
            }

            #btnPrint {
                display: none;
            }
        }
    </style>
    <div class="row">
        <h3 id="titlePage">12 TIÊU CHÍ QUẢN LÝ MỘT VỊ TRÍ</h3>
    </div>

    <div class="row">
        <div class="col-12 d-flex justify-content-between mb-3">
            <h3 class="d-inline m-0"></h3>
            @can('12_criteria-create')
                <a href="/criteria/create">
                    <button type="button" class="btn btn-success btn-label waves-effect waves-light"><i
                            class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm</button>
                </a>
            @endcan
        </div>
        <div class="card-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">
                @if (!auth()->user()->hasRole('super-admin'))
                    <li class="nav-item">
                        <a class="nav-link active" data-bs-toggle="tab" href="#my-work" role="tab"
                            aria-selected="false">
                            Công việc của tôi
                        </a>
                    </li>
                @endif
                @if (!auth()->user()->hasAnyRole(['specialist', 'staff']))
                    <li class="nav-item">
                        @if (auth()->user()->hasRole('super-admin'))
                            <a class="nav-link active" data-bs-toggle="tab" href="#assigned-work" role="tab"
                                aria-selected="false">
                                Công việc đã giao
                            </a>
                        @else
                            <a class="nav-link" data-bs-toggle="tab" href="#assigned-work" role="tab"
                                aria-selected="false">
                                Công việc đã giao
                            </a>
                        @endif
                    </li>
                @endif
            </ul>
            <div class="tab-content text-muted">
                @if (!auth()->user()->hasRole('super-admin'))
                    <div class="tab-pane active" id="my-work" role="tabpanel">
                        @if (array_key_exists('myWork', $dataInit) && !empty($dataInit['myWork']))
                            <div class="col-12 d-flex justify-content-between mb-3">
                                <h3 class="d-inline m-0"></h3>
                                <a href="#">
                                    <button type="button" id="btnPrint"
                                        class="btn btn-primary btn-label waves-effect waves-light">
                                        <i class="ri-printer-line label-icon align-middle fs-16 me-2"></i>IN</button>
                                </a>
                            </div>
                            <div class="row">
                                <div class="card">
                                    <div id="myWork" class="card-body">
                                        @include('frontend.criteria.mywork', [
                                            'dataInit' => $dataInit['myWork'],
                                        ])
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                @endif
                @if (!auth()->user()->hasAnyRole(['specialist', 'staff']))
                    @if (auth()->user()->hasRole('super-admin'))
                        <div class="tab-pane active" id="assigned-work" role="tabpanel">
                        @else
                            <div class="tab-pane" id="assigned-work" role="tabpanel">
                    @endif
                    <div class="row">
                        <div class="card">
                            <div class="card-body">
                                <table id="example"
                                    class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                    style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Phòng</th>
                                            <th>Bộ Phận</th>
                                            <th>Chức Danh</th>
                                            <th>Nhân Viên Được Giao</th>
                                            <th>Người tạo</th>
                                            <th>Trạng thái</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($dataInit['listData'] as $key => $value)
                                            <tr>
                                                <td class="dtr-control">{{ $key + 1 }}</td>
                                                <td>{{ $value->room->name }}</td>
                                                <td>{{ $value->team->name }}</td>
                                                <td>{{ $value->position->name }}</td>
                                                @foreach ($value->confirms as $data)
                                                    @if (!empty($data->confirmReceive))
                                                        <td>{{ !empty($data->confirmReceive->staff) ? $data->confirmReceive->staff->full_name : 'Chưa có' }}
                                                        </td>
                                                    @endif
                                                @endforeach
                                                <td>{{ $value->user->name }}</td>
                                                <td>{{ config('constants.status_confirm.criteria')[$value->status] }}
                                                </td>
                                                <td>
                                                    <div class="dropdown d-inline-block">
                                                        <button class="btn btn-soft-secondary btn-sm dropdown"
                                                            type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <i class="ri-more-fill align-middle"></i>
                                                        </button>
                                                        <ul class="dropdown-menu dropdown-menu-end">
                                                            @can('12_criteria-read')
                                                                <li><a href="/criteria/view/{{ $value->id }}"
                                                                        class="dropdown-item"><i
                                                                            class="ri-eye-fill align-bottom me-2 text-muted"></i>
                                                                        View</a></li>
                                                            @endcan
                                                            @can('12_criteria-edit')
                                                                @if (auth()->id() == $value->created_by && !in_array($value->status, [3, 4]))
                                                                    <li><a href="/criteria/edit/{{ $value->id }}"
                                                                            class="dropdown-item edit-item-btn"><i
                                                                                class="ri-pencil-fill align-bottom me-2 text-muted"></i>
                                                                            Edit</a></li>
                                                                @endif
                                                            @endcan
                                                            @can('12_criteria-delete')
                                                                @if (auth()->id() == $value->created_by && !in_array($value->status, [3, 4]))
                                                                    <li>
                                                                        <a href="{{ route('criteria.delete', $value->id) }}"
                                                                            class="dropdown-item remove-item-btn">
                                                                            <i
                                                                                class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>
                                                                            Delete
                                                                        </a>
                                                                    </li>
                                                                @endif
                                                            @endcan
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
            @endif
        </div><!-- end card-body -->
    </div>
@endsection
@section('script')
    <script>
        $(function() {
            $('#example').dataTable({
                fixedHeader: true
            });

            $('#btnPrint').on('click', function() {
                window.print()
            })
        })
    </script>
@endsection
