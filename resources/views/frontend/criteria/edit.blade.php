@extends('layouts.fe.master2')
@section('title')
@lang('translation.personal')
@endsection
@section('css')
<link href="{{ URL::asset('/assets/css/summernote-bs4.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="row ">
    <h3 class="text-center">12 TIÊU CHÍ QUẢN LÝ MỘT VỊ TRÍ</h3>
</div>
<!--end col Quốc tịch-->
<form method="PUT" id="formUpdate" enctype="multipart/form-data">
    @csrf
    <input hidden name="criteria_id" value="{{ $dataInit['criteria']['id'] }}">
    <div class="row justify-content-between pe-3 mb-3">
        <h3 style="width: fit-content" class="m-0"></h3>
        <button type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content"><i class="ri-save-3-line label-icon  fs-16 "></i> Lưu</button>
    </div>
    <div class="row ">
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <label for="firstNameinput" class="form-label">Phòng</label>
                    <div class="col-xl-12">
                        <select name="room_id" id="selectRoom" class="form-select" aria-label="Default select example">
                            <option value="">Vui lòng chọn phòng ban</option>
                            @foreach($dataInit['room'] as $value)
                            <option @if($dataInit['criteria']['room_id']==$value['id']) selected @endif value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!--end col Phòng-->

            <div class="col-4">
                <div class="mb-3">
                    <label for="firstNameinput" class="form-label">Bộ phận</label>
                    <div class="col-xl-12">
                        <select name="team_id" id="selectTeam" class="form-select" aria-label="Default select example">
                        </select>
                    </div>
                </div>
            </div>
            <!--end col Bộ phận-->

            <div class="col-4">
                <div class="mb-3">
                    <label for="firstNameinput" class="form-label">Chức danh</label>
                    <div class="col-xl-12">
                        <select name="position_id" id="selectPosition" class="form-select" aria-label="Default select example">
                        </select>
                    </div>
                </div>
            </div>
            <!--end col Chức danh-->
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="nav flex-column nav-pills text-start" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link mb-2  active" id="position-tab" data-bs-toggle="pill" href="#position" role="tab" aria-controls="position" aria-selected="true">1. Vị
                                        trí</a>
                                    <a class="nav-link mb-2" id="aim-tab" data-bs-toggle="pill" href="#aim" role="tab" aria-controls="aim" aria-selected="false">2. Mục tiêu</a>
                                    <a class="nav-link mb-2" id="recruit-tab" data-bs-toggle="pill" href="#recruit" role="tab" aria-controls="recruit" aria-selected="false">3. Yêu cầu tuyển
                                        dụng</a>
                                    <a class="nav-link mb-2" id="capacity-tab" data-bs-toggle="pill" href="#capacity" role="tab" aria-controls="capacity" aria-selected="false">4. Yêu cầu năng
                                        lực</a>
                                    <a class="nav-link mb-2" id="function-tab" data-bs-toggle="pill" href="#function" role="tab" aria-controls="function" aria-selected="false">5. Chức năng</a>
                                    <a class="nav-link mb-2" id="mission-tab" data-bs-toggle="pill" href="#mission" role="tab" aria-controls="mission" aria-selected="false">6. Nhiệm vụ</a>
                                    <a class="nav-link mb-2" id="power-tab" data-bs-toggle="pill" href="#power" role="tab" aria-controls="power" aria-selected="false">7. Quyền hạn</a>
                                    <a class="nav-link mb-2" id="wage-tab" data-bs-toggle="pill" href="#wage" role="tab" aria-controls="wage" aria-selected="false">8. Lương</a>
                                    <a class="nav-link mb-2" id="kpis-tab" data-bs-toggle="pill" href="#kpis" role="tab" aria-controls="kpis" aria-selected="false">9. KPIs</a>
                                    <a class="nav-link mb-2" id="bonus-tab" data-bs-toggle="pill" href="#bonus" role="tab" aria-controls="bonus" aria-selected="false">10. Thưởng</a>
                                    <a class="nav-link mb-2" id="disc-tab" data-bs-toggle="pill" href="#disc" role="tab" aria-controls="disc" aria-selected="false">11. DISC</a>
                                    <a class="nav-link mb-2" id="report-tab" data-bs-toggle="pill" href="#report" role="tab" aria-controls="report" aria-selected="false">12. Báo cáo</a>
                                </div>
                            </div><!-- end col -->
                            <div class="col-md-10">
                                <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="position" role="tabpanel" aria-labelledby="position-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="locationEditor" name="location">{{ old('location',$dataInit['criteria']['location']) }}</textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="aim" role="tabpanel" aria-labelledby="aim-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="targetEditor" name="target">{{ old('target',$dataInit['criteria']['target']) }}</textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="recruit" role="tabpanel" aria-labelledby="recruit-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="recruitEditor" name="recruitment_requirements">
                                                {{ old('recruitment_requirements',$dataInit['criteria']['recruitment_requirements']) }}</textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="capacity" role="tabpanel" aria-labelledby="capacity-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="capabilityEditor" name="capability_requirements">
                                                {{ old('capability_requirements',$dataInit['criteria']['capability_requirements']) }}
                                                </textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="function" role="tabpanel" aria-labelledby="function-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="functionEditor" name="function">
                                                {{ old('function',$dataInit['criteria']['function']) }}
                                                </textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="mission" role="tabpanel" aria-labelledby="mission-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="missionEditor" name="mission">
                                                {{ old('mission',$dataInit['criteria']['mission']) }}
                                                </textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="power" role="tabpanel" aria-labelledby="power-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="powerEditor" name="power">
                                                {{ old('power',$dataInit['criteria']['power']) }}
                                                </textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="wage" role="tabpanel" aria-labelledby="wage-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="wageEditor" name="wage">
                                                {{ old('wage',$dataInit['criteria']['wage']) }}
                                                </textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="kpis" role="tabpanel" aria-labelledby="kpis-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="kpisEditor" name="kpis">
                                                {{ old('kpis',$dataInit['criteria']['kpis']) }}
                                                </textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="bonus" role="tabpanel" aria-labelledby="bonus-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="bonusEditor" name="bonus">
                                                {{ old('bonus',$dataInit['criteria']['bonus']) }}
                                                </textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="disc" role="tabpanel" aria-labelledby="disc-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="discEditor" name="disc">
                                                {{ old('disc',$dataInit['criteria']['disc']) }}
                                                </textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                    <div class="tab-pane fade" id="report" role="tabpanel" aria-labelledby="report-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4 class="card-title mb-0">Chỉnh sửa văn bản</h4>
                                            </div><!-- end card header -->
                                            <div class="card-body form-group">
                                                <textarea id="reportEditor" name="report">
                                                {{ old('report',$dataInit['criteria']['report']) }}
                                                </textarea>
                                            </div><!-- end card-body -->
                                        </div><!-- end card -->
                                    </div>
                                </div>
                            </div><!--  end col -->
                        </div>
                        <!--end row-->
                    </div><!-- end card-body -->
                </div><!-- end card -->
            </div>
            <!--end col-->
        </div>
        <!--end row-->

        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">

                        <div class="row mb-2">
                            <div class="col-12">
                                <h4 class="text-center">Hướng dẫn sử dụng "12 TIÊU CHÍ QUẢN LÝ MỘT VỊ TRÍ" như sau:</h4>
                                <div class="d-flex flex-column m-auto mt-2" style="width: fit-content">
                                    <p class="text-start">1. Mô tả công việc sử dụng các mục gồm: 1, 5, 6, 7.</p>
                                    <p class="text-start">2. Đánh giá KPIs sử dụng các mục gồm: 2, 9.</p>
                                    <p class="text-start">3. Tuyển dụng sử dụng các mục gồm: 1, 3, 4, 5, 6, 8, 10, 11.</p>
                                    <p class="text-start">4. Đào tạo nhân sự sử dụng các mục gồm: 4, 6, 7, 9, 11.</p>
                                </div>
                            </div>
                        </div>


                        <div class="row mb-5">
                            <div class="col-3 d-flex justify-content-center">
                                <div class="row  ">
                                    <div class="col-12 text-center">
                                        <h5>Người giao việc</h5>
                                    </div>
                                    <div class="col-12 text-center">
                                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ $dataInit['criteria']['user']['name'] }}</label>
                                    </div>
                                    <div class="col-12 text-center">
                                        @if($dataInit['criteria']['status'] != 1)
                                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">Đã xác nhận</label>
                                        @else
                                        <div class="col-12 d-flex justify-content-center mt-4">
                                            <div class=" form-check" style="width: fit-content;">
                                                <input class="form-check-input fs-15" type="checkbox" id="assign" value="1" name="assign">
                                            </div>
                                            <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác
                                                nhận</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--end col Người giao việc-->

                            @foreach( $dataInit['criteria']['confirms'] as $key => $value)
                            <input name="confirm_id_{{$key+1}}" value="{{ $value['confirm']['id'] }}" hidden>
                            <div class="col-3 d-flex justify-content-center">
                                <div class="row">
                                    <div class="col-12 text-center">
                                        @switch($value['confirm']['type_confirm'])
                                        @case(1)
                                        <h5>Người nhận việc</h5>
                                        @break
                                        @case(2)
                                        <h5>P.QT HC-NS</h5>
                                        @break
                                        @case(3)
                                        <h5>Ban Giám Đốc</h5>
                                        @break
                                        @endswitch
                                    </div>
                                    <div class="col-12">
                                        @switch($value['confirm']['type_confirm'])
                                        @case(1)
                                        <input hidden name="type_confirm_1" value="1">
                                        <select data-old="{{ $value['confirm']['basic_info_id'] }}" class="form-select text-center mt-2" id="selectStaff" name="selectConfirm[]" aria-label="Default select example">
                                        </select>
                                        @break
                                        @case(2)
                                        @if(!empty($dataInit['hr']))
                                        <input hidden name="type_confirm_2" value="2">
                                        <select name="selectConfirm[]" class="form-select text-center mt-2" aria-label="Default select example">
                                            @foreach($dataInit['hr'] as $hr)
                                            @if(!empty($hr['basic_info_id']))
                                            <option @if($hr->basic_info_id == $value['confirm']['basic_info_id']) selected @endif
                                                value="{{ $hr->basic_info_id}}">{{ $hr->staff->full_name }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @endif
                                        @break
                                        @case(3)
                                        @if(!empty($dataInit['manager']))
                                        <input hidden name="type_confirm_3" value="3">
                                        <select name="selectConfirm[]" class="form-select text-center mt-2" aria-label="Default select example">
                                            @foreach($dataInit['manager'] as $manager)
                                            @if(!empty($manager['basic_info_id']))
                                            <option @if($manager->basic_info_id == $value['confirm']['basic_info_id']) selected @endif
                                                value="{{ $manager->basic_info_id }}">{{ $manager->staff->full_name }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @endif
                                        @break
                                        @endswitch
                                    </div>
                                    <div class="col-12 text-center">
                                        @if($value['confirm']['type_confirm'] != 1)
                                        @switch($value['confirm']['status'])
                                        @case(0)
                                        <label for="firstNameinput" class="form-label mb-0">{{ 'Chưa xác nhận' }}</label>
                                        @break
                                        @case(1)
                                        <label for="firstNameinput" class="form-label mb-0">{{ 'Đã xác nhận' }}</label>
                                        @break
                                        @case(2)
                                        <label for="firstNameinput" class="form-label mb-0">{{ 'Từ chối' }}</label>
                                        <div>
                                            <label>Lý do : {{ $value['confirm']['note']}}</label>
                                        </div>
                                        @break
                                        @endswitch
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!--end col Ban giám đốc-->
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</form>
@endsection
@section('script')
<script src="{{ asset('assets/js/summernote-bs4.min.js') }}" defer></script>
<script>
    $(document).ready(function() {
        $('#locationEditor, #targetEditor, #recruitEditor, #capabilityEditor, #functionEditor, #missionEditor, #wageEditor, #kpisEditor, #reportEditor, #powerEditor, #bonusEditor, #discEditor')
            .summernote({
                toolbar: [
                    ['fontsize', ['fontsize']],
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'hr']],
                    ['view', ['fullscreen', 'codeview']],
                    ['help', ['help']]
                ],
                height: 400,
            });

        $('#formUpdate').on('submit', function(event) {
            event.preventDefault()
            $.ajax({
                method: "PUT",
                url: "{{ route('criteria.storeUpdate') }}",
                data: $('#formUpdate').serialize(),
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                        window.location.href = response.link
                    } else {
                        toastr.error(response.message);
                    }

                },
                error: function(error) {
                    handleFails(error);
                }
            })
        })

        function handleFails(response) {
            $('#selectRoom, #selectTeam, #selectPosition, #locationEditor, #targetEditor, #recruitEditor, #capabilityEditor, #functionEditor, #missionEditor, #wageEditor, #kpisEditor, #reportEditor, #powerEditor, #bonusEditor, #discEditor').removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formUpdate').find(".has-error").find(".help-block").remove();
                $('#formUpdate').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formUpdate').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response.responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }

        var roomID = $('#selectRoom').find(":selected").val();
        var teamID = "{!! $dataInit['criteria']['team_id'] !!}";
        var positionID = "{!! $dataInit['criteria']['position_id'] !!}";
        var staffID = $('#selectStaff').data('old');

        if (roomID != '') {
            loadTeam(roomID, teamID)
        }

        if (teamID != '') {
            loadPosition(teamID, positionID)
        }

        if (roomID != '' && teamID != '' && positionID != '') {
            loadStaff(roomID, teamID, positionID, staffID)
        }

        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            loadTeam(id);
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            loadPosition(id);
        });

        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();

            loadStaff(room_id, team_id, position_id);

            if (position_id == "0") {
                $('#selectStaff')
                    .find('option')
                    .remove()
            }
        })
    });

    function loadTeam(id, teamID = null) {
        if (id != '') {
            var url = "{!! route('helper.getTeamByRommID', ':id') !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (Object.keys(data).length > 0) {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectPosition')
                            .find('option')
                            .remove()
                        $('#selectTeam').append($('<option>', {
                            value: '',
                            text: "Vui lòng chọn bộ phận",
                            disabled: true,
                            selected: true,
                        }));
                        Object.keys(data).forEach(function(index) {
                            if (data[index]) {
                                if (Number.parseInt(teamID) == data[index].id) {
                                    $('#selectTeam').append($('<option>', {
                                        value: data[index].id,
                                        text: data[index].name,
                                        selected: true
                                    }));
                                } else {
                                    $('#selectTeam').append($('<option>', {
                                        value: data[index].id,
                                        text: data[index].name,
                                    }));
                                }
                            }
                        })
                    } else {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                    }
                }
            });
        } else {
            $('#selectTeam')
                .find('option')
                .remove()
        }
    }

    function loadPosition(id, poisitionID = null) {
        var url = "{!! route('helper.getPositionByTeamID', ':id') !!}";

        $.ajax({
            type: 'GET',
            url: url.replace(':id', id),
            success: function(data) {
                if (Object.keys(data).length > 0) {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    $('#selectPosition').append($('<option>', {
                        value: '',
                        text: "Vui lòng chọn chức danh",
                        disabled: true,
                        selected: true,
                    }));
                    Object.keys(data).forEach(function(index) {
                        if (data[index] != null) {
                            if (Number.parseInt(poisitionID) == data[index].id) {
                                $('#selectPosition').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name,
                                    selected: true,
                                }));
                            } else {
                                $('#selectPosition').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name
                                }));
                            }
                        }
                    })
                } else {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                }
            }
        });
    }

    function loadStaff(room_id, team_id, position_id, staffID = null) {
        var url = "{!! route('helper.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";

        $.ajax({
            type: 'GET',
            url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
            success: function(data) {
                if (Object.keys(data).length > 0) {
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    Object.keys(data).forEach(function(index) {
                        if (data[index].staff != null) {
                            if (Number.parseInt(staffID) == data[index].staff.id) {
                                $('#selectStaff').append($('<option>', {
                                    value: data[index].staff.id,
                                    text: data[index].staff.full_name,
                                    selected: true,
                                }));
                            } else {
                                $('#selectStaff').append($('<option>', {
                                    value: data[index].staff.id,
                                    text: data[index].staff.full_name
                                }));
                            }
                        }

                    })
                } else {
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    $('#selectStaff').append($('<option>', {
                        value: '',
                        text: 'Chưa có'
                    }));
                }
            }
        }).done(function() {
            if ($('#selectStaff').find('option').length == 0) {
                $('#selectStaff').append($('<option>', {
                    value: '',
                    selected: true,
                    text: 'Chưa có'
                }));
            }
        });

        if (position_id == "0") {
            $('#selectStaff')
                .find('option')
                .remove()
        }

        if (!staffID) {
            $('#selectStaff').append($('<option>', {
                value: '',
                text: 'Chưa có'
            }));
        }
    }
</script>
@endsection