@extends('layouts.fe.master2')
@section('title')
@lang('translation.personal')
@endsection
@section('css')
<link href="{{ URL::asset('/assets/css/summernote-bs4.min.css') }}" rel="stylesheet">
@endsection
@section('content')

<style>
    .table td p {
        margin: auto;
    }

    .table>:not(caption)>*>* {
        padding: 6px;
    }

    .table th {
        font-weight: 700;
        text-align: center;
    }

    @print {
        @page :footer {
            display: none
        }

        @page :header {
            display: none
        }
    }

    @media print {
        /* .address {
            margin-top: 77% !important;
        } */

        #btnPrint {
            display: none;
        }
    }
</style>
<div id="btnPrint" class="justify-content-between pe-3 mb-3 d-flex">
    <h3></h3>
    <a href="#">
        <button type="button" id="btnPrint" class="btn btn-primary btn-label waves-effect waves-light">
            <i class="ri-printer-line label-icon align-middle fs-16 me-2"></i>IN</button>
    </a>
</div>
<div class="d-flex justify-content-between">
    <div style="width: 55%;">
        <h3 class="text-left mt-4 fw-bold">12 TIÊU CHÍ CHO MỘT VỊ TRÍ</h3>
        <div class="">
            <label for="room" class="form-label col-3">Phòng</label>
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['criteria']['room']) ? $dataInit['criteria']['room']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div>
            <label for="room" class="form-label col-3">Bộ phận</label>
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['criteria']['team']) ? $dataInit['criteria']['team']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div>
            <label for="room" class="form-label col-3">Chức danh</label>
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['criteria']['position']) ? $dataInit['criteria']['position']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div class="col-sm-5">
        </div>
    </div>
    <div style="width:35% ; height: fit-content;">
        <img style="object-fit: contain; width: 100%;height: 100%;" src="{{ url('/uploads/common/logo.png') }}">
    </div>
</div>
<!-- <div class="row">
    <div class="d-flex justify-content-left">
        <div class="col-sm-1">
            <label for="room" class="form-label">Phòng</label>
        </div>
        <div class="col-sm-2">
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['criteria']['room']) ? $dataInit['criteria']['room']['name'] : 'Chưa có' }} </span>
        </div>
    </div>
    <div class="d-flex justify-content-left">
        <div class="col-sm-1">
            <label for="room" class="form-label">Bộ phận</label>
        </div>
        <div class="col-sm-2">
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['criteria']['team']) ? $dataInit['criteria']['team']['name'] : 'Chưa có' }} </span>
        </div>
    </div>
    <div class="d-flex justify-content-left">
        <div class="col-sm-1">
            <label for="room" class="form-label">Chức danh</label>
        </div>
        <div class="col-sm-2">
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['criteria']['position']) ? $dataInit['criteria']['position']['name'] : 'Chưa có' }} </span>
        </div>
    </div>
</div> -->
<div class="row card-body">
    <table class="table table-bordered border-dark dt-responsive nowrap align-middle" style="width:100%; background: white;">
        <thead>
            <tr>
                <th>STT</th>
                <th>HẠNG MỤC</th>
                <th>NỘI DUNG</th>
            </tr>
        </thead>
        <tbody>
            @php $i = 1; @endphp
            @foreach(config('constants.header_criteria') as $key => $value)
            <tr>
                <td class="text-center" style="width: 7%;">{{ $i }}</td>
                <td style="width: 20%;">{{ $value }}</td>
                <td>{!! $dataInit['criteria'][$key] !!}</td>
            </tr>
            @php $i ++; @endphp
            @endforeach
        </tbody>
    </table>
</div>
<!-- <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="nav flex-column nav-pills text-start" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link mb-2  active" id="position-tab" data-bs-toggle="pill" href="#position" role="tab" aria-controls="position" aria-selected="true">1. Vị
                                    trí</a>
                                <a class="nav-link mb-2" id="aim-tab" data-bs-toggle="pill" href="#aim" role="tab" aria-controls="aim" aria-selected="false">2. Mục tiêu</a>
                                <a class="nav-link mb-2" id="recruit-tab" data-bs-toggle="pill" href="#recruit" role="tab" aria-controls="recruit" aria-selected="false">3. Yêu cầu tuyển
                                    dụng</a>
                                <a class="nav-link mb-2" id="capacity-tab" data-bs-toggle="pill" href="#capacity" role="tab" aria-controls="capacity" aria-selected="false">4. Yêu cầu năng
                                    lực</a>
                                <a class="nav-link mb-2" id="function-tab" data-bs-toggle="pill" href="#function" role="tab" aria-controls="function" aria-selected="false">5. Chức năng</a>
                                <a class="nav-link mb-2" id="mission-tab" data-bs-toggle="pill" href="#mission" role="tab" aria-controls="mission" aria-selected="false">6. Nhiệm vụ</a>
                                <a class="nav-link mb-2" id="power-tab" data-bs-toggle="pill" href="#power" role="tab" aria-controls="power" aria-selected="false">7. Quyền hạn</a>
                                <a class="nav-link mb-2" id="wage-tab" data-bs-toggle="pill" href="#wage" role="tab" aria-controls="wage" aria-selected="false">8. Lương</a>
                                <a class="nav-link mb-2" id="kpis-tab" data-bs-toggle="pill" href="#kpis" role="tab" aria-controls="kpis" aria-selected="false">9. KPIs</a>
                                <a class="nav-link mb-2" id="bonus-tab" data-bs-toggle="pill" href="#bonus" role="tab" aria-controls="bonus" aria-selected="false">10. Thưởng</a>
                                <a class="nav-link mb-2" id="disc-tab" data-bs-toggle="pill" href="#disc" role="tab" aria-controls="disc" aria-selected="false">11. DISC</a>
                                <a class="nav-link mb-2" id="report-tab" data-bs-toggle="pill" href="#report" role="tab" aria-controls="report" aria-selected="false">12. Báo cáo</a>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="position" role="tabpanel" aria-labelledby="position-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="bubble-editor readonly" style="height: 400px;">
                                                {!! old('location',$dataInit['criteria']['location']) !!}
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="aim" role="tabpanel" aria-labelledby="aim-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body form-group">
                                            <div class="bubble-editor readonly" style="height: 400px;" id="targetEditor" name="target">{!! old('target',$dataInit['criteria']['target']) !!}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="recruit" role="tabpanel" aria-labelledby="recruit-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body form-group">
                                            <div class="bubble-editor readonly" style="height: 400px;" id="recruitEditor" name="recruitment_requirements">
                                                {!! old('recruitment_requirements',$dataInit['criteria']['recruitment_requirements']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="capacity" role="tabpanel" aria-labelledby="capacity-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body form-group">
                                            <div class="bubble-editor readonly" style="height: 400px;" id="capabilityEditor" name="capability_requirements">
                                                {!! old('capability_requirements',$dataInit['criteria']['capability_requirements']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="function" role="tabpanel" aria-labelledby="function-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body form-group">
                                            <div class="bubble-editor readonly" style="height: 400px;" id="functionEditor" name="function">
                                                {!! old('function',$dataInit['criteria']['function']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="mission" role="tabpanel" aria-labelledby="mission-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body form-group">
                                            <div class="bubble-editor readonly" style="height: 400px;" id="missionEditor" name="mission">
                                                {!! old('mission',$dataInit['criteria']['mission']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="power" role="tabpanel" aria-labelledby="power-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body form-group">
                                            <div class="bubble-editor readonly" style="height: 400px;" id="powerEditor" name="power">
                                                {!! old('power',$dataInit['criteria']['power']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="wage" role="tabpanel" aria-labelledby="wage-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body form-group">
                                            <div class="bubble-editor readonly" style="height: 400px;" id="wageEditor" name="wage">
                                                {!! old('wage',$dataInit['criteria']['wage']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="kpis" role="tabpanel" aria-labelledby="kpis-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body form-group">
                                            <div class="bubble-editor readonly" style="height: 400px;" id="kpisEditor" name="kpis">
                                                {!! old('kpis',$dataInit['criteria']['kpis']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="bonus" role="tabpanel" aria-labelledby="bonus-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body form-group">
                                            <div class="bubble-editor readonly" style="height: 400px;" id="bonusEditor" name="bonus">
                                                {!! old('bonus',$dataInit['criteria']['bonus']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="disc" role="tabpanel" aria-labelledby="disc-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body form-group">
                                            <div class="bubble-editor readonly" style="height: 400px;" id="discEditor" name="disc">
                                                {!! old('disc',$dataInit['criteria']['disc']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="report" role="tabpanel" aria-labelledby="report-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title mb-0">Nội dung của tiêu chí</h4>
                                        </div>
                                        <div class="card-body form-group">
                                            <div class="bubble-editor readonly" style="height: 400px;" id="reportEditor" name="report">
                                                {!! old('report',$dataInit['criteria']['report']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!
                </div>
            </div>
        </div>
    </div> -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row mb-2">
                    <div class="col-12 mb-2">
                        <p>Hướng dẫn sử dụng <span class="fw-bold">"12 TIÊU CHÍ QUẢN LÝ MỘT VỊ TRÍ"</span> như sau:</p>
                        <div class="d-flex flex-column m-auto mt-2" style="width: fit-content">
                            <p class="text-start mb-1">1. Mô tả công việc sử dụng các mục gồm: <span class="fw-bold">1, 5, 6, 7.</span></p>
                            <p class="text-start mb-1">2. Đánh giá KPIs sử dụng các mục gồm: <span class="fw-bold">2, 9.</span></p>
                            <p class="text-start mb-1">3. Tuyển dụng sử dụng các mục gồm: <span class="fw-bold">1, 3, 4, 5, 6, 8, 10, 11.</span></p>
                            <p class="text-start mb-1">4. Đào tạo nhân sự sử dụng các mục gồm: <span class="fw-bold">4, 6, 7, 9, 11.</span></p>
                        </div>
                        <div class="col-12 d-flex mt-2 justify-content-end"  style="padding-right: 2%;">
                            @if (!empty($dataInit['criteria']['review_date']))
                            <label for="confirm" class=" form-label mb-0 me-5 " style="margin-top: 2px">
                                @php
                                $date = \Carbon\Carbon::parse($dataInit['criteria']['review_date']);
                                @endphp
                                Ngày duyệt : Ngày {{ $date->day }} Tháng {{ $date->month }} Năm {{ $date->year }}</label>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-3 d-flex justify-content-center">
                        <div class="row ">
                            <div class="col-12 text-center">
                                <h5 class="fw-bold">Người giao việc</h5>
                            </div>
                            <div class="col-12 text-center">
                                <label for="firstNameinput" class="form-label mb-0">{{ $dataInit['criteria']['user']['name'] }}</label>
                            </div>
                            <div class="col-12 text-center mt-2">
                                <label for="firstNameinput" class="form-label">{{ $dataInit['criteria']['status'] != 1 ? 'Đã xác nhận' : 'Chưa xác nhận' }}</label>
                            </div>
                        </div>
                    </div>
                    <!--end col Người giao việc-->
                    @php
                    $idConfirmer = [];
                    $id;
                    $idConfirmerNext;
                    $type;
                    @endphp
                    @foreach( $dataInit['criteria']['confirms'] as $key => $value)
                    <input name="confirm_id_{{$key+1}}" hidden value="{{ $value['id'] }}">
                    <div class="col-3 d-flex justify-content-center">
                        <div class="row">
                            <div class="col-12 text-center">
                                @switch($value['confirm']['type_confirm'])
                                @case(1)
                                <h5 class="fw-bold">Người nhận việc</h5>
                                @break
                                @case(2)
                                @if ($value['confirm']['status'] == 0)
                                @php
                                $idConfirmer[] = $value['confirm']['basic_info_id'];
                                $id = $value['confirm']['id'];
                                $type = 2;
                                @endphp
                                @endif
                                <h5 class="fw-bold">P.QT HC-NS</h5>
                                @break
                                @case(3)
                                @php
                                $idConfirmerNext = $value['confirm']['basic_info_id'];
                                @endphp
                                @if (empty($idConfirmer) && $value['confirm']['status'] == 0)
                                @php
                                $idConfirmer[] = $value['confirm']['basic_info_id'];
                                $id = $value['confirm']['id'];
                                $type = 3;
                                $idConfirmerNext = '';
                                @endphp
                                @endif
                                <h5 class="fw-bold">Ban Giám Đốc</h5>
                                @break
                                @endswitch
                            </div>
                            <div class="col-12 text-center">
                                @switch($value['confirm']['type_confirm'])
                                @case(1)
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] ?? 'Chưa có' }}</label>
                                @break
                                @case(2)
                                @if(!empty($dataInit['hr']))
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] }}</label>
                                @endif
                                @break
                                @case(3)
                                @if(!empty($dataInit['manager']))
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] }}</label>
                                @endif
                                @break
                                @endswitch
                            </div>
                            <div class="col-12 text-center mt-2">
                                @if($value['confirm']['type_confirm'] != 1)
                                @switch($value['confirm']['status'])
                                @case(0)
                                <label for="firstNameinput" class="form-label mb-0">{{ 'Chưa xác nhận' }}</label>
                                @break
                                @case(1)
                                <label for="firstNameinput" class="form-label mb-0">{{ 'Đã xác nhận' }}</label>
                                @break
                                @case(2)
                                <label for="firstNameinput" class="form-label mb-0">{{ 'Từ chối' }}</label>
                                <div>
                                    <label>Lý do : {{ $value['confirm']['note']}}</label>
                                </div>
                                @break
                                @endswitch
                                @else
                                <label for="firstNameinput" class="form-label mb-0"></label>
                                @endif
                            </div>
                        </div>
                    </div>


                    @endforeach
                    @if (in_array(auth()->user()->basic_info_id, $idConfirmer))
                    <form method="POST" action="{{ route('criteria.updateStatus', $dataInit['criteria']['id']) }}" id="formConfirm">
                        @csrf
                        <input hidden name="confirmID" value="{{ $id }}">
                        <input hidden name="confimerIDNext" value="{{ $idConfirmerNext }}">
                        <input hidden name="reason" id="reasonConfirm">
                        <input hidden name="statusConfirm" id="statusConfirm" value="1">
                        <input hidden name="type" value="{{ $type }}">
                        <div class="row justify-content-center pe-3 mb-3 mt-3">
                            <div class="col-3 justify-content-between d-flex">
                                <button data-id="1" type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content">
                                    <i class="ri-check-double-line label-icon align-middle fs-16 me-2"></i> Duyệt</button>
                                <button data-id="2" type="submit" id="btnDenie" class="btn btn-danger waves-effect waves-light" style="width: fit-content">
                                    <i class="ri-error-warning-line label-icon align-middle rounded-pill fs-16 me-2 "></i> Không Duyệt</button>
                            </div>
                        </div>
                    </form>
                    @endif
                    <!--end col Ban giám đốc-->
                </div>
            </div>
            <div class="card-body mt-5">
                <p class="fw-bold m-0">ActionCOACH CBD Firm</p>
                <p class="m-0">Địa chỉ: CBD Building, 90 - 92 Lê Thị Riêng, Phường Bến Thành, Quận 1, TP.HCM</p>
                <p class="m-0">Hotline: 1800 8087</p>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#btnPrint').on('click', function() {
            window.print()
        })
        $('#btnDenie').on('click', function(event) {
            var status = $(this).data('id');
            var form = $(this).closest("form");
            event.preventDefault();
            swal.fire({
                    title: 'Lý do không duyệt đơn',
                    html: `<input class="form-control" type="text" id="reason"  placeholder="Lý do" required>`,
                    icon: 'warning',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Đồng ý',
                    showDenyButton: true,
                    denyButtonText: 'Hủy',
                    preConfirm: () => {
                        const reason = Swal.getPopup().querySelector('#reason').value
                        if (!reason) {
                            Swal.showValidationMessage(`Vui lòng nhập lý do`)
                        }
                        return {
                            reason: reason,
                        }
                    }
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        $('#reasonConfirm').val(result.value.reason);
                        $('#statusConfirm').val(2);
                        form.submit();
                    } else if (result.isDenied) {
                        Swal.fire('Your record is safe', '', 'info')
                    }

                });
        });

        $('#formUpdate').on('submit', function(event) {
            event.preventDefault()
            $.ajax({
                method: "PUT",
                url: "{{ route('criteria.storeUpdate') }}",
                data: $('#formUpdate').serialize(),
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                        window.location.href = response.link
                    } else {
                        toastr.error(response.message);
                    }

                },
                error: function(error) {
                    handleFails(error);
                }
            })
        })

        function handleFails(response) {
            $('#selectRoom, #selectTeam, #selectPosition, #locationEditor, #targetEditor, #recruitEditor, #capabilityEditor, #functionEditor, #missionEditor, #wageEditor, #kpisEditor, #reportEditor, #powerEditor, #bonusEditor, #discEditor').removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formUpdate').find(".has-error").find(".help-block").remove();
                $('#formUpdate').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formUpdate').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response.responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }
    });
</script>
@endsection