@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('content')

@php
$trackingDayOffX = $dataInit['trackingDayOff'];

$dayUsedInFirst3Months = $trackingDayOffX -> cur_year_pto[0] + $trackingDayOffX -> cur_year_pto[1] + $trackingDayOffX -> cur_year_pto[2];

$dayToBeReset = ($trackingDayOffX -> total_prev_year_pto > $dayUsedInFirst3Months ) ? ($trackingDayOffX -> total_prev_year_pto - $dayUsedInFirst3Months) : 0;

@endphp

<div class="card">
  <div class="card-body">
    <div class="row gy-4">
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Công ty</label>
        <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> company }}" disabled readonly>
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Phòng</label>
        <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> organizational -> room -> name }}" disabled readonly>
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Bộ phận</label>
        <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> organizational -> team -> name }}" disabled readonly>
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Chức danh</label>
        <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> organizational -> position -> name }}" disabled readonly>
      </div>

      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Họ và tên</label>
        <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> full_name }}" disabled readonly>
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Tên tiếng anh</label>
        <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> english_name }}" disabled readonly>
      </div>
      <div class="col-xl-3">
        <label for="exampleInputdate" class="form-label">Ngày vào làm</label>
        <input name="start_date" type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime( $trackingDayOffX -> staff -> date_of_entry_to_work )) }}" disabled>
      </div>
      <div class="col-xl-3">
        <label for="exampleInputdate" class="form-label">Ngày nghỉ việc</label>
        @if ($trackingDayOffX -> staff -> larborBbt -> from_date_qdtv)
        <input name="end_date" type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime( $trackingDayOffX -> staff -> date_of_stop_work )) }}" disabled>
        @else
        <input type="text" class="form-control border-dashed" id="borderInput" value="N/A" disabled readonly>
        @endif
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Số phép còn của năm trước</label>
        <input type="text" class="form-control" id="borderInput" value=" {{ $trackingDayOffX -> total_prev_year_pto }}" disabled>
      </div>

    </div>
  </div>
</div>

<div class="card">
  <div class="card-header">
    <h3>Năm nay</h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-xl-4">
        <label for="exampleInputdate" class="form-label">Từ ngày</label>
        @if (strtotime($trackingDayOffX -> staff -> date_of_entry_to_work) > strtotime("2022-01-01"))
        <input type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime( $trackingDayOffX -> staff -> date_of_entry_to_work )) }}" disabled>
        @else
        <input type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime("2022-01-01")) }}" disabled>
        @endif
      </div>
      <div class="col-xl-4">
        <label for="exampleInputdate" class="form-label">Đến ngày</label>
        @if ($trackingDayOffX -> staff -> larborBbt -> from_date_qdtv && (strtotime($trackingDayOffX -> staff -> larborBbt -> from_date_qdtv) < strtotime(now())) ) <input type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime( $trackingDayOffX -> staff -> larborBbt -> from_date_qdtv )) }}" disabled>
          @else
          <input type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime(now())) }}" disabled>
          @endif
      </div>
      <div class="col-xl-4 d-flex justify-content-center align-items-end">
        <h3 class="fw-bold">Tổng Còn Lại Trong Năm Nay: <span> {{ $trackingDayOffX -> total_cur_year_pto }}</span> ngày</h3>
      </div>
    </div>
  </div>
</div>

<div class="card">
  <div class="card-header">
    <h3>Đã sử dụng theo tháng trong Năm {{ date('Y', strtotime('-1 year'.now()))  }}</h3>
  </div>
  <div class="card-body">
    <div class="row gy-3">
      @for ($i = 1; $i < 13; $i++) <div class="col-xl-2">
        <label class="form-label">Tháng {{ $i }}</label>
        <input type="text" class="form-control" id="mth{{ $i }}" value="{{ $trackingDayOffX -> prev_year_pto[$i - 1] }}" disabled>
    </div>
    @endfor
  </div>
</div>
</div>

<div class="card">
  <div class="card-header">
    <h3>Đã sử dụng theo tháng trong Năm {{ date('Y', strtotime(now()))  }}</h3>
  </div>
  <div class="card-body">
    <div class="row gy-3">
      @for ($i = 1; $i < 13; $i++) <div class="col-xl-2">
        <label class="form-label">Tháng {{ $i }}</label>
        <input type="text" class="form-control" id="mth{{ $i }}" value="{{ $trackingDayOffX -> cur_year_pto[$i - 1] }}" disabled>
    </div>
    @endfor
  </div>
</div>
</div>

<div class="card">
  <div class="card-header">
    <h3>Năm trước</h3>
  </div>
  <div class="card-body">
    <div class="row">
      @if (intval(date('m', strtotime( now() ))) < 4) <div class="col-xl-4">
        <h3 class="text-center">Phép năm từ năm trước: <span>{{ $trackingDayOffX -> total_prev_year_pto }}</span> ngày</h3>
    </div>
    <div class="col-xl-4">
      <h3 class="text-center">Đã sử dụng đến hết tháng <span>Ba</span>: <span>{{ $dayUsedInFirst3Months }}</span> ngày</h3>
    </div>
    <div class="col-xl-4">
      <h3 class="text-center">Không sử dụng hết bị Reset: <span> {{ $dayToBeReset }}</span> ngày</h3>
    </div>
    @else
    <h3 class="text-center"> Đã quá hạn để dùng phép năm của Năm {{ date('Y', strtotime('-1 year'.now()))  }}</h3>
    @endif
  </div>
</div>
</div>
</div>

@if (intval(date('m', strtotime( now() ))) < 4) <h2 class="text-center text-danger my-3">Số ngày phép hiện tại được dùng <span> {{ $trackingDayOffX -> total_cur_year_pto + $trackingDayOffX -> total_prev_year_pto }}</span> ngày</h2>
  @else
  <h2 class="text-center text-danger my-3">Số ngày phép hiện tại được dùng <span> {{ $trackingDayOffX -> total_cur_year_pto  }}</span> ngày</h2>
  @endif

  <div class="card">
    <div class="card-header">
      <h3>Ghi chú</h3>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <textarea class="form-control" id="exampleFormControlTextarea5" rows="3" placeholder="Ghi chú..." disabled> {{ $trackingDayOffX -> note }}</textarea>
        </div>
      </div>
    </div>
  </div>

  @endsection