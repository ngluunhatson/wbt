@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('content')



<div class="row">
  <div class="col-12 d-flex justify-content-between mb-3">
    <h3 class="d-inline m-0">Chỉnh Sửa Phép Năm</h3>
    <a href="#" noref>
      <button type="button" id="editTrackingDayoff" class="btn btn-success btn-label waves-effect waves-light">
        <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Lưu</button>
    </a>
  </div>
</div>

<form id="formUpdate" method="POST" action="{{ route('trackingDayOffStoreUpdate') }}">
  @csrf
  @php
  $trackingDayOffX = $dataInit['trackingDayOff'];

  $dayUsedInFirst3Months = $trackingDayOffX -> cur_year_pto[0] + $trackingDayOffX -> cur_year_pto[1] + $trackingDayOffX -> cur_year_pto[2];

  $dayToBeReset = ($trackingDayOffX -> total_prev_year_pto > $dayUsedInFirst3Months ) ? ($trackingDayOffX -> total_prev_year_pto - $dayUsedInFirst3Months) : 0;

  @endphp
  <div class="card">
    <div class="card-body">
      <div class="row gy-4">
        <div class="col-xl-3">
          <label for="borderInput" class="form-label">Công ty</label>
          <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> company }}" disabled readonly>
        </div>
        <div class="col-xl-3">
          <label for="borderInput" class="form-label">Phòng</label>
          <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> organizational -> room -> name }}" disabled readonly>
        </div>
        <div class="col-xl-3">
          <label for="borderInput" class="form-label">Bộ phận</label>
          <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> organizational -> team -> name }}" disabled readonly>
        </div>
        <div class="col-xl-3">
          <label for="borderInput" class="form-label">Chức danh</label>
          <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> organizational -> position -> name }}" disabled readonly>
        </div>

        <div class="col-xl-3">
          <label for="borderInput" class="form-label">Họ và tên</label>
          <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> full_name }}" disabled readonly>
        </div>
        <div class="col-xl-3">
          <label for="borderInput" class="form-label">Tên tiếng anh</label>
          <input type="text" class="form-control border-dashed" id="borderInput" value="{{ $trackingDayOffX -> staff -> english_name }}" disabled readonly>
        </div>
        <div class="col-xl-3">
          <label for="exampleInputdate" class="form-label">Ngày vào làm</label>
          <input name="start_date" type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime( $trackingDayOffX -> staff -> date_of_entry_to_work )) }}" disabled>
        </div>
        <div class="col-xl-3">
          <label for="exampleInputdate" class="form-label">Ngày nghỉ việc</label>
          @if ($trackingDayOffX -> staff -> date_of_stop_work)
          <input name="end_date" type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime( $trackingDayOffX -> staff -> date_of_stop_work )) }}" disabled>
          @else
          <input type="text" class="form-control border-dashed" id="borderInput" value="N/A" disabled readonly>
          @endif
        </div>
        <div class="col-xl-3">
          <label for="borderInput" class="form-label">Số phép còn của năm trước</label>
          <input type="text" class="form-control" id="borderInput" value=" {{ $trackingDayOffX -> total_prev_year_pto }}" disabled>
        </div>

      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <h3>Năm nay</h3>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-xl-4">
          <label for="exampleInputdate" class="form-label">Từ ngày</label>
          @if ( strtotime($trackingDayOffX -> staff -> date_of_entry_to_work) > strtotime("2022-01-01"))
          <input type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime( $trackingDayOffX -> staff -> date_of_entry_to_work )) }}" disabled>
          @else
          <input type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime("2022-01-01")) }}" disabled>
          @endif
        </div>
        <div class="col-xl-4">
          <label for="exampleInputdate" class="form-label">Đến ngày</label>
          @if ($trackingDayOffX -> staff -> date_of_stop_work && (strtotime($trackingDayOffX -> staff -> date_of_stop_work) < strtotime(now())) ) <input type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime( $trackingDayOffX -> staff -> date_of_stop_work )) }}" disabled>
            @else
            <input type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime(now())) }}" disabled>
            @endif
        </div>
        <div class="col-xl-4 d-flex justify-content-center align-items-end">
          <h2 class="fw-bold">Tổng Trong Năm Nay: <span id="total_cur_year_pto"> </span> ngày</h2>
        </div>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <h3>Đã sử dụng theo tháng trong Năm {{ date('Y', strtotime('-1 year'.now()))  }}</h3>
    </div>
    <div class="card-body">
      <div class="row gy-3">
        @for ($i = 1; $i < 13; $i++) <div class="col-xl-2">
          <label class="form-label">Tháng {{ $i }}</label>
          <input type="number" class="form-control" id="prev_mth{{ $i }}" name="prev_mth{{ $i }}" value={{ $trackingDayOffX -> prev_year_pto[$i - 1] }}>
      </div>
      @endfor
    </div>
  </div>
  </div>

  <div class="card">
    <div class="card-header">
      <h3>Đã sử dụng theo tháng trong Năm {{ date('Y', strtotime(now()))  }}</h3>
    </div>
    <div class="card-body">
      <div class="row gy-3">
        @for ($i = 1; $i < 13; $i++) <div class="col-xl-2">
          <label class="form-label">Tháng {{ $i }}</label>
          <input type="number" class="form-control" id="cur_mth{{ $i }}" name="cur_mth{{ $i }}" value={{ $trackingDayOffX -> cur_year_pto[$i - 1] }}>
      </div>
      @endfor
    </div>
  </div>
  </div>

  <div class="card">
    <div class="card-header">
      <h3>Năm trước</h3>
    </div>
    <div class="card-body">
      <div class="row">
        @if($trackingDayOffX -> total_prev_year_pto > 0)

        <div class="col-xl-3">
          <h3 class="text-center">Phép năm để dùng ở tháng 1-3 của năm trước: <span id="total_prev_prev_year_pto"></span> ngày</h3>
        </div>
        <div class="col-xl-3">
          <h3 class="text-center">Phép năm từ năm trước để dùng trong năm nay: <span id="total_prev_year_pto"></span> ngày</h3>
        </div>
        <div class="col-xl-3">
          <h3 class="text-center">Đã sử dụng đến hết tháng <span>Ba</span>: <span id="day_used_in_first_3_months"></span> ngày</h3>
        </div>
        <div class="col-xl-3">
          <h3 class="text-center">Không sử dụng hết bị Reset: <span id="day_to_be_reset"></span> ngày</h3>
        </div>
        @else
        <h3 class="text-center"> Đã sử dụng hết ngày phép của Năm {{ date('Y', strtotime('-1 year'.now()))  }}</h3>
        @endif
      </div>
    </div>
  </div>

  <h2 class="text-center text-danger my-3">Số ngày phép hiện tại được dùng <span id="remaining_tracking_day_off"> </span> ngày</h2>

  <div class="card">
    <div class="card-header">
      <h3>Ghi chú</h3>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-12">
          <textarea name="note" class="form-control" id="exampleFormControlTextarea5" rows="3"> {{ $trackingDayOffX -> note }}</textarea>
        </div>
      </div>
    </div>
  </div>

  <input hidden name="tracking_day_off_id" value={{ $trackingDayOffX -> id }}>
</form>
@endsection
@section('script')
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script>
  $(document).ready(function() {

      const trackingDayOffX = (<?php echo json_encode($trackingDayOffX); ?>);

      $('#editTrackingDayoff').on('click', function(e) {
        e.preventDefault();
        $('#formUpdate').submit();
      });


      $(function() {

        $('#total_cur_year_pto').text(trackingDayOffX['total_cur_year_pto']);
        $("#total_prev_year_pto").text(trackingDayOffX['total_prev_year_pto']);
        $('#day_used_in_first_3_months').text(<?php echo $dayUsedInFirst3Months; ?>);
        $('#day_to_be_reset').text(<?php echo $dayToBeReset; ?>);
        $('#remaining_tracking_day_off').text(trackingDayOffX['total_cur_year_pto'] + trackingDayOffX['total_prev_year_pto']);
        $('#total_prev_prev_year_pto').text(trackingDayOffX['to_be_used_in_first_3_months_prev']);

      });


      // for (let i = 1; i < 13; i++) {
      //   $('#cur_mth'.concat(i.toString())).on('change', function() {
      //     getTrackingDayOff();
      //   });

      //   $('#prev_mth'.concat(i.toString())).on('change', function() {
      //     getTrackingDayOff();
      //   });
      // }

      // function getTrackingDayOff() {


      //   const max_cur_year_pto = trackingDayOffX['total_cur_year_pto'];
      //   const max_prev_year_pto = trackingDayOffX['total_prev_year_pto'];

      //   let to_be_used_in_first_3_months_cur = trackingDayOffX['to_be_used_in_first_3_months_cur'];
      //   let to_be_used_in_first_3_months_prev = trackingDayOffX['to_be_used_in_first_3_months_prev'];

      //   let real_time_cur_year_pto = 0;
      //   let real_time_prev_year_pto = 0;
      //   let real_time_cur_year_pto_first_3_months = 0;
      //   let real_time_prev_year_pto_first_3_months = 0;

      //   let current_total_prev_year_pto = 0;
      //   let current_total_cur_year_pto = 0;

      //   let isChangedPrev = false;

      //   for (let i = 1; i < 13; i++) {

      //     const strCur = '#cur_mth'.concat(i.toString());
      //     const strPrev = '#prev_mth'.concat(i.toString());


      //     const cur_mth_val = parseInt($(strCur).val());
      //     const prev_mth_val = parseInt($(strPrev).val());


      //     if (i < 4) {
      //       real_time_cur_year_pto_first_3_months += cur_mth_val;
      //       real_time_prev_year_pto_first_3_months += prev_mth_val;

      //       const remaining_to_be_used_in_first_3_months_prev = to_be_used_in_first_3_months_prev - real_time_prev_year_pto_first_3_months;
      //       let remaining_to_be_used_in_first_3_months_cur = 0;

      //       if (remaining_to_be_used_in_first_3_months_prev >= 0) {
      //         $('#total_prev_prev_year_pto').text(remaining_to_be_used_in_first_3_months_prev);
      //         $('#total_prev_year_pto').text(max_prev_year_pto);

      //       } else {
      //         $('#total_prev_prev_year_pto').text(0);
      //         if ((max_prev_year_pto + remaining_to_be_used_in_first_3_months_prev >= 0)) {

      //           $('#total_prev_year_pto').text(max_prev_year_pto + remaining_to_be_used_in_first_3_months_prev);
      //           remaining_to_be_used_in_first_3_months_cur = max_prev_year_pto + remaining_to_be_used_in_first_3_months_prev;

      //         } else {
      //           $('total_prev_year_pto').text(0);

      //         }
      //       }

       

      //       if (real_time_prev_year_pto_first_3_months > (max_prev_year_pto + to_be_used_in_first_3_months_prev)) {
      //         $(strPrev).val(0);
      //         real_time_prev_year_pto_first_3_months -= prev_mth_val;
      //       }

      //       if (real_time_cur_year_pto_first_3_months > (max_cur_year_pto + remaining_to_be_used_in_first_3_months_cur)) {
      //         $(strCur).val(0);
      //         real_time_cur_year_pto_first_3_months -= cur_mth_val;
      //       }

      //       $('#total_cur_year_pto').text();

      //     } else {
      //       real_time_cur_year_pto += cur_mth_val;
      //       real_time_prev_year_pto += prev_mth_val;


      //     }

      //   }



      // }
    }

  );
</script>
@endsection