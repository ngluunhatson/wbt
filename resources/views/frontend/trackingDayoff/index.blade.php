@extends('layouts.fe.master2')
@section('title')
@endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

@endsection
@section('content')
<style>
    .table th {
        text-align: center;
    }

    .table td {
        text-align: center;

    }
</style>
<div class="row">
    <h3 id="titlePage">DANH SÁCH PHÉP NĂM</h3>
</div>
@php
$myTrackingDayOffsDict = $dataInit['myTrackingDayOffsDict'];
$allTrackingDayOffsDict = $dataInit['allTrackingDayOffsDict'];

$myTrackingDayOffsDictKeys = array_keys($myTrackingDayOffsDict);
$allTrackingDayOffsDictKeys = array_keys($allTrackingDayOffsDict);

@endphp

<div class="row">
    <div class="card-body">
        <ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">
            @if(count($myTrackingDayOffsDictKeys) > 0)
            <li class="nav-item">
                <a class="nav-link active" data-bs-toggle="tab" href="#my-tracking-day-offs" role="tab" aria-selected="false">
                    Phép Năm của Tôi
                </a>
            </li>
            @endif

            @can('trackingdayoff-admin')
            <li class="nav-item">
                <a class="nav-link @if(count($myTrackingDayOffsDictKeys) <= 0) active @endif" data-bs-toggle="tab" href="#all-tracking-day-offs" role="tab" aria-selected="false">
                    Phép Năm của Công Ty
                </a>
            </li>
            @endcan
        </ul>
        @if(count($myTrackingDayOffsDictKeys) > 0)
        <div class="tab-content text-muted">
            <div class="tab-pane active" id="my-tracking-day-offs" role="tabpanel">
                <div class="col-xl-1">
                    <label for="datePicker" class="form-label"> Năm</label>
                    <input type="text" class="form-control mb-3" name="year" id="datePicker" value="{{ old('year', date('Y', strtotime(now()) ) ) }}">
                </div>
                @foreach($myTrackingDayOffsDictKeys as $index => $year)
                <div class="row" id="my-tracking-day-offs-div{{$year}}">
                    <div class="card">
                        <div class="card-body overflow-auto">
                            <table id="my-tracking-day-offs-table{{$year}}" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">Công Ty</th>
                                        <th rowspan="2">Mã Nhân Viên </th>
                                        <th rowspan="2">Họ Tên</th>
                                        <th rowspan="2">Tên Tiếng Anh</th>
                                        <th rowspan="2">Phòng</th>
                                        <th rowspan="2">Chức Danh</th>
                                        <th rowspan="2">Ngày Vào Làm</th>
                                        <th rowspan="2">Ngày Nghỉ Việc</th>
                                        <th rowspan="2">Số Phép Năm Còn Của Năm Trước</th>
                                        <th rowspan="1" colspan="3">Năm Nay</th>
                                        <th rowspan="1" colspan="13">Đã Sử Dụng Theo Tháng</th>
                                        <th rowspan="1" colspan="2">Năm Trước</th>
                                        <th rowspan="2">Số Ngày Phép Hiện Tại Được Dùng</th>

                                    </tr>
                                    <tr>
                                        <td rowspan="1">Từ Ngày</td>
                                        <td rowspan="1">Đến Ngày</td>
                                        <td rowspan="1">Tổng</td>
                                        @for($i=1; $i <=12; $i++) <td rowspan="1">Tháng {{ $i }}</td>
                                            @endfor
                                            <td rowspan="1">Tổng</td>
                                            <td rowspan="1">Đã Sử Dụng Đến Hết Tháng 3</td>
                                            <td rowspan="1">Không Sử Dụng Bị Reset</td>

                                    </tr>
                                </thead>
                                <tbody>
                                    @php

                                    $trackingDayOff_X = $myTrackingDayOffsDict[$year];


                                    $totalPTOUsed = 0;

                                    foreach($trackingDayOff_X -> year_pto_array as $monthly_pto)
                                        $totalPTOUsed += $monthly_pto;


                                    $ptoUsedInFirst3Months = $trackingDayOff_X -> year_pto_array[0] + $trackingDayOff_X -> year_pto_array[1] + $trackingDayOff_X -> year_pto_array[2];

                                    $now_first_strtotime = strtotime('first day of january '. $trackingDayOff_X -> year);

                                    $this_year_from_date = strtotime( $trackingDayOff_X -> staff -> date_of_entry_to_work ) > $now_first_strtotime
                                        ? $trackingDayOff_X -> staff -> date_of_entry_to_work : date('Y-m-d', $now_first_strtotime);

                                    
                                    $now_carbon = now();
                        
                                    $now_strtotime = $now_carbon->year > $trackingDayOff_X-> year ? strtotime('last day of december '. $trackingDayOff_X -> year) : strtotime(now());

                                    $this_year_to_date = date('Y-m-d', $now_strtotime);

                                    if ($trackingDayOff_X -> staff -> larborBbt -> from_date_qdtv && strtotime($trackingDayOff_X -> staff -> larborBbt -> from_date_qdtv) < $now_strtotime) $this_year_to_date=date('Y-m-d', strtotime($trackingDayOff_X -> staff -> larborBbt -> from_date_qdtv));

                                        @endphp
                                        <tr>
                                            <td> {{ $trackingDayOff_X -> staff -> company }}</td>
                                            <td> {{ $trackingDayOff_X -> staff -> employee_code }}</td>
                                            <td> {{ $trackingDayOff_X -> staff -> full_name }}</td>
                                            <td> {{ $trackingDayOff_X -> staff -> english_name }}</td>
                                            <td> {{ $trackingDayOff_X -> staff -> organizational -> room -> name }} </td>
                                            <td> {{ $trackingDayOff_X -> staff -> organizational -> position -> name }} </td>
                                            <td> {{ $trackingDayOff_X -> staff -> date_of_entry_to_work }}</td>
                                            <td> {{ $trackingDayOff_X -> staff -> larborBbt -> from_date_qdtv ?   $trackingDayOff_X -> staff -> larborBbt -> from_date_qdtv : "N/A" }}</td>
                                            <td> {{ ($trackingDayOff_X -> prevYearTrackingDayOff) ? $trackingDayOff_X -> prevYearTrackingDayOff -> year_pto_count : "Không Đi Làm Trong Năm " . $year-1 }} </td>

                                            <td> {{ $this_year_from_date }}</td>
                                            <td> {{ $this_year_to_date }}</td>


                                            <td> {{ $trackingDayOff_X -> year_pto_count + $totalPTOUsed }}</td>
                                            @for($i = 0; $i < 12; $i++) <td>{{ $trackingDayOff_X -> year_pto_array[$i] }} </td>
                                                @endfor

                                                <td> {{ $totalPTOUsed }}</td>


                                                <td> {{ $ptoUsedInFirst3Months }}</td>
                                                @if ($trackingDayOff_X -> prevYearTrackingDayOff)
                                                @php $ptoToBeReset = $trackingDayOff_X -> prevYearTrackingDayOff -> year_pto_count - $ptoUsedInFirst3Months @endphp
                                                <td> {{ $ptoToBeReset }}</td>
                                                @else
                                                <td> Không Đi Làm Trong Năm {{ $year - 1 }}</td>
                                                @endif

                                                @if (intval(date('m', strtotime(now()))) < 4 && $trackingDayOff_X -> prevYearTrackingDayOff)
                                                    <td> {{ $trackingDayOff_X -> year_pto_count + $trackingDayOff_X -> prevYearTrackingDayOff -> year_pto_count }}</td>
                                                    @else
                                                    <td> {{ $trackingDayOff_X -> year_pto_count }} </td>
                                                    @endif


                                        </tr>
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            @endif
            @can('trackingdayoff-admin')
            <div class="tab-pane @if(count($myTrackingDayOffsDictKeys) <= 0) active @endif" id="all-tracking-day-offs" role="tabpanel">
                <div class="col-xl-1">
                    <label for="datePickerAll" class="form-label"> Năm</label>
                    <input type="text" class="form-control mb-3" name="yearAll" id="datePickerAll" value="{{ old('yearAll', date('Y', strtotime(now()) ) ) }}">
                </div>
                @foreach($allTrackingDayOffsDictKeys as $index => $year)
                <div class="row" id=all-tracking-day-offs-div{{$year}}>
                    <div class="card">
                        <div class="card-body overflow-auto">
                            <table id="all-tracking-day-offs-table{{$year}}" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">STT</th>
                                        <th rowspan="2">Công Ty</th>
                                        <th rowspan="2">Mã Nhân Viên </th>
                                        <th rowspan="2">Họ Tên</th>
                                        <th rowspan="2">Tên Tiếng Anh</th>
                                        <th rowspan="2">Phòng</th>
                                        <th rowspan="2">Chức Danh</th>
                                        <th rowspan="2">Ngày Vào Làm</th>
                                        <th rowspan="2">Ngày Nghỉ Việc</th>
                                        <th rowspan="2">Số Phép Năm Còn Của Năm Trước</th>
                                        <th rowspan="1" colspan="3">Năm Nay</th>
                                        <th rowspan="1" colspan="13">Đã Sử Dụng Theo Tháng</th>
                                        <th rowspan="1" colspan="2">Năm Trước</th>
                                        <th rowspan="2">Số Ngày Phép Hiện Tại Được Dùng</th>

                                    </tr>
                                    <tr>
                                        <td rowspan="1">Từ Ngày</td>
                                        <td rowspan="1">Đến Ngày</td>
                                        <td rowspan="1">Tổng</td>
                                        @for($i=1; $i <=12; $i++) <td rowspan="1">Tháng {{ $i }}</td>
                                            @endfor
                                            <td rowspan="1">Tổng</td>
                                            <td rowspan="1">Đã Sử Dụng Đến Hết Tháng 3</td>
                                            <td rowspan="1">Không Sử Dụng Bị Reset</td>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($allTrackingDayOffsDict[$year] as $key => $trackingDayOff_X)
                                    @php


                                    $totalPTOUsed = 0;

                                    foreach($trackingDayOff_X -> year_pto_array as $monthly_pto)
                                        $totalPTOUsed += $monthly_pto;


                                    $ptoUsedInFirst3Months = $trackingDayOff_X -> year_pto_array[0] + $trackingDayOff_X -> year_pto_array[1] + $trackingDayOff_X -> year_pto_array[2];

                                    $now_first_strtotime = strtotime('first day of january '. $trackingDayOff_X -> year);

                                    $this_year_from_date = strtotime( $trackingDayOff_X -> staff -> date_of_entry_to_work ) > $now_first_strtotime
                                    ? $trackingDayOff_X -> staff -> date_of_entry_to_work : date('Y-m-d', $now_first_strtotime);

                                    $now_carbon = now();
                        
                                    $now_strtotime = $now_carbon->year > $trackingDayOff_X-> year ? strtotime('last day of december '. $trackingDayOff_X -> year) : strtotime(now());

                                    $this_year_to_date = date('Y-m-d', $now_strtotime);

                                    if ($trackingDayOff_X -> staff -> larborBbt -> from_date_qdtv && strtotime($trackingDayOff_X -> staff -> larborBbt -> from_date_qdtv) < $now_strtotime) $this_year_to_date=date('Y-m-d', strtotime($trackingDayOff_X -> staff -> larborBbt -> from_date_qdtv));

                                        @endphp
                                        <tr>
                                            <td class="dtr-control">{{ $key + 1 }}</td>
                                            <td> {{ $trackingDayOff_X -> staff -> company }}</td>
                                            <td> {{ $trackingDayOff_X -> staff -> employee_code }}</td>
                                            <td> {{ $trackingDayOff_X -> staff -> full_name }}</td>
                                            <td> {{ $trackingDayOff_X -> staff -> english_name }}</td>
                                            <td> {{ $trackingDayOff_X -> staff -> organizational -> room -> name }} </td>
                                            <td> {{ $trackingDayOff_X -> staff -> organizational -> position -> name }} </td>
                                            <td> {{ $trackingDayOff_X -> staff -> date_of_entry_to_work }}</td>
                                            <td> {{ $trackingDayOff_X -> staff -> larborBbt -> from_date_qdtv ?   $trackingDayOff_X -> staff -> larborBbt -> from_date_qdtv : "N/A" }}</td>
                                            <td> {{ ($trackingDayOff_X -> prevYearTrackingDayOff) ? $trackingDayOff_X -> prevYearTrackingDayOff -> year_pto_count : "Không Đi Làm Trong Năm " . $year-1 }} </td>

                                            <td> {{ $this_year_from_date }}</td>
                                            <td> {{ $this_year_to_date }}</td>


                                            <td> {{ $trackingDayOff_X -> year_pto_count + $totalPTOUsed }}</td>
                                            @for($i = 0; $i < 12; $i++) <td>{{ $trackingDayOff_X -> year_pto_array[$i] }} </td>
                                                @endfor

                                                <td> {{ $totalPTOUsed }}</td>


                                                <td> {{ $ptoUsedInFirst3Months }}</td>
                                                @if ($trackingDayOff_X -> prevYearTrackingDayOff)
                                                @php $ptoToBeReset = $trackingDayOff_X -> prevYearTrackingDayOff -> year_pto_count - $ptoUsedInFirst3Months @endphp
                                                <td> {{ $ptoToBeReset }}</td>
                                                @else
                                                <td> Không Đi Làm Trong Năm {{ $year - 1 }}</td>
                                                @endif

                                                @if (intval(date('m', strtotime(now()))) < 4 && $trackingDayOff_X -> prevYearTrackingDayOff)
                                                    <td> {{ $trackingDayOff_X -> year_pto_count + $trackingDayOff_X -> prevYearTrackingDayOff -> year_pto_count }}</td>
                                                    @else
                                                    <td> {{ $trackingDayOff_X -> year_pto_count }} </td>
                                                    @endif


                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endforeach
                @endcan

            </div>
        </div>
        @endsection
        @section('script')
        <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
        <script>
            $(document).ready(function() {

                const myTrackingDayOffsDictKeys = <?php echo json_encode($myTrackingDayOffsDictKeys); ?>;
                const allTrackingDayOffsDictKeys = <?php echo json_encode($allTrackingDayOffsDictKeys); ?>;

                $(function() {

                    myTrackingDayOffsDictKeys.forEach(function(y) {
                        $(`#my-tracking-day-offs-div${y}`).hide();
                        $(`#my-tracking-day-offs-table${y}`).dataTable();

                    });

                    allTrackingDayOffsDictKeys.forEach(function(y) {
                        $(`#all-tracking-day-offs-div${y}`).hide();
                        $(`#all-tracking-day-offs-table${y}`).dataTable();

                    })

                });

                $(function() {
                    $('#datePicker').datepicker({
                        format: 'yyyy',
                        startView: "years",
                        minViewMode: "years",
                    });

                    const date_year = $(`#datePicker`).val();

                    $(`#my-tracking-day-offs-div${date_year}`).show();

                });

                $(function() {
                    $('#datePickerAll').datepicker({
                        format: 'yyyy',
                        startView: "years",
                        minViewMode: "years",
                    });

                    const date_year = $(`#datePickerAll`).val();

                    $(`#all-tracking-day-offs-div${date_year}`).show();


                });

                $('#datePicker').on('change', function() {
                    const date_year = $(`#datePicker`).val();
                    $(`#my-tracking-day-offs-div${date_year}`).show();

                    myTrackingDayOffsDictKeys.forEach(function(y) {
                        if (y != date_year)
                            $(`#my-tracking-day-offs-div${y}`).hide();

                    });
                });

                $('#datePickerAll').on('change', function() {
                    const date_year = $(`#datePickerAll`).val();
                    $(`#all-tracking-day-offs-div${date_year}`).show();

                    allTrackingDayOffsDictKeys.forEach(function(y) {
                        if (y != date_year)
                            $(`#all-tracking-day-offs-div${y}`).hide();

                    });
                });


            });
        </script>
        @endsection