@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('content')

<div class="card">
  <div class="card-body">
    <div class="row gy-4">
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Công ty</label>
        <input type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Phòng</label>
        <input type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Bộ phận</label>
        <input type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Chức danh</label>
        <input type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
      </div>

      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Họ và tên</label>
        <select class="form-select mb-3" aria-label=".form-select-lg example">
          <option selected>Open this select menu</option>
          <option value="1">One</option>
          <option value="2">Two</option>
          <option value="3">Three</option>
        </select>
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Tên tiếng anh</label>
        <input type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
      </div>
      <div class="col-xl-3">
        <label for="exampleInputdate" class="form-label">Ngày vào làm</label>
        <input type="date" class="form-control" id="exampleInputdate" readonly>
      </div>
      <div class="col-xl-3">
        <label for="exampleInputdate" class="form-label">Ngày nghỉ việc</label>
        <input type="date" class="form-control" id="exampleInputdate">
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Số phép còn của năm trước</label>
        <input type="text" class="form-control" id="borderInput" value="Enter your name">
      </div>

    </div>
  </div>
</div>

<div class="card">
  <div class="card-header">
    <h3>Năm nay</h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-xl-4">
        <label for="exampleInputdate" class="form-label">Từ ngày</label>
        <input type="date" class="form-control" id="exampleInputdate" readonly>
      </div>
      <div class="col-xl-4">
        <label for="exampleInputdate" class="form-label">Đến ngày</label>
        <input type="date" class="form-control" id="exampleInputdate">
      </div>
      <div class="col-xl-4 d-flex justify-content-center align-items-end">
        <h2 class="fw-bold">Tổng: <span>9.0</span> ngày</h2>
      </div>
    </div>
  </div>
</div>

<div class="card">
  <div class="card-header">
    <h3>Đã sử dụng theo tháng</h3>
  </div>
  <div class="card-body">
    <div class="row gy-3">
      @for ($i = 1; $i < 13; $i++) <div class="col-xl-2">
        <label class="form-label">Tháng {{ $i }}</label>
        <input type="text" class="form-control" name="mth{{ $i }}" id="mth{{ $i }}" value="0.0" maxlength="3">
    </div>
    @endfor
  </div>
</div>
</div>

<div class="card">
  <div class="card-header">
    <h3>Năm trước</h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-xl-6">
        <h3 class="text-center">Đã sử dụng đến hết tháng <span>0.3</span>: <span>4.0</span> ngày</h3>
      </div>
      <div class="col-xl-6">
        <h3 class="text-center">Không sử dụng hết bị Reset: <span>4.0</span> ngày</h3>
      </div>
    </div>
  </div>
</div>

<h2 class="text-center text-danger my-3">Số ngày phép hiện tại được dùng <span>4.0</span> ngày</h2>

<div class="card">
  <div class="card-header">
    <h3>Ghi chú</h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-12">
        <textarea class="form-control" id="exampleFormControlTextarea5" rows="3" placeholder="Ghi chú..."></textarea>
      </div>
    </div>
  </div>
</div>

@endsection