@extends('layouts.fe.master2')
@section('title')
    @lang('translation.team.index')
@endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')

    <div class="row">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h5 class="card-title mb-0">Chức Danh </h5>
                <div class="btn-group w-50">
                    <button type="button " class="btn btn-info dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Xem Mã Bộ Phận</button>
                    <div class="dropdown-menu w-100">
                        @foreach ($teams as $team )
                        <div class="row ">
                            <a class="dropdown-item w-50 text-center" href="#">{{ $team->id }}</a>
                            <a class="dropdown-item w-50 text-center" href="#">{{ $team->name }}</a>
                            </div>                            
                        @endforeach
                    </div>
                    </div><!-- /btn-group -->
                <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#exampleModalgrid">
                    Thêm chức danh
                </button>
            </div>




            <!-- Modals Add-->
            <form action="{{ route('position.create') }}" method="post" id = "formCreatePosition">
                @csrf
                <div class="modal fade" id="exampleModalgrid" tabindex="-1" aria-labelledby="exampleModalgridLabel"
                    aria-modal="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalgridLabel">Thêm Chức Danh</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="javascript:void(0);">
                                    <div class="row g-3">
                                        <div class="col-xxl-12">
                                            <div class = "mb-3">
                                                <label for="name" class="form-label">Tên chức danh:</label>
                                                <input type="text" class="form-control" id="name" name="name"
                                                    placeholder="(Điền thông tin)">
                                            </div>
                                            <div class = "mb-3">
                                                <label for="code_team" class="form-label">Chọn Bộ Phận:</label>
                                                <select class="form-select"  name="team_id" id = "team_id">
                                                    @foreach($teams as $team)
                                                    <option value="{{ $team->id }}">{{ $team->name }}</option> 
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class = "mb-3">
                                                <label for="name" class="form-label">Số lượng:</label>
                                                <input type="number" class="form-control" id="amount" name="amount"
                                                    placeholder="(Điền thông tin)">
                                            </div>
                                            <div class = "mb-3">
                                                <label for="name" class="form-label">Năm:</label>
                                                <input type="text" class="form-control datepicker" id="year" name="year" value = "{{ now()->format('Y') }}"
                                                    placeholder="(Điền thông tin)">
                                            </div>
                                            <div class = "mb-3">
                                                <label for="code_team" class="form-label">Cấp bậc:</label>
                                                <select class="form-select"  name="rank" id = "rankSelect">
                                                    <option value="">Vui lòng chọn cấp bậc</option>
                                                    @foreach(config('constants.rank_chart') as $key => $rank)
                                                    <option value="{{ $key }}">{{ $rank }}</option> 
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div id = "parentPositionSelectBody"> </div>
                                            <div id = "childPositionSelectBody"> </div>

                                        
                                            <div class="form-check form-switch form-switch-lg" dir="ltr">
                                                <label for="name" class="form-label">Hiện trên sơ đồ</label>
                                                <input type="checkbox" class="form-check-input" value="1" name="flag_chart"
                                                    placeholder="(Điền thông tin)">
                                            </div>
                                            <div class="form-check form-switch form-switch-lg" dir="ltr">
                                                <label for="name" class="form-label">Hiện trên filter</label>
                                                <input type="checkbox" class="form-check-input" value="1" name="flag_filter"
                                                    placeholder="(Điền thông tin)">
                                            </div>

                                          
                                        </div>
                                        <!--end col-->
                                        <div class="col-lg-12">
                                            <div class="hstack gap-2 justify-content-end">
                                                <button type="submit" class="btn btn-primary"  id = "savePositionButton">Lưu</button>
                                            </div>
                                        </div>
                                        <!--end col-->
                                    </div>
                                    <!--end row-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </form>



            <div class="card-body">
                <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle"
                    style="width:100%">

                    <thead>
                        <tr>
                            <th>Mã Chức Danh</th>
                            <th>Tên Chức Danh</th>
                            <th>Mã Bộ Phận</th>
                            <th>Số lượng</th>
                            <th>Năm</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($positions as $key => $position)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $position->name }}</td>
                                <td>{{ $position->team ? $position->team->name : ''}}</td>
                                <td>{{ $position->amount }}</td>
                                <td>{{ $position->year }}</td>
                                <td>
                                    <div class="dropdown d-inline-block">
                                        <button class="btn btn-soft-secondary btn-sm dropdown" type="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="ri-more-fill align-middle"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-end">
                                            <a href="{{ route('position.edit', $position->id) }}"
                                                class="dropdown-item remove-item-btn">
                                                <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Sửa
                                            </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('position.delete', $position->id) }}"
                                                    class="dropdown-item remove-item-btn">
                                                    <i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Xóa
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- end row -->
@endsection
@section('script')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script>
    $(function(){
        $('#example').dataTable();
        $('#year').datepicker({
            format: 'yyyy',
            startView: "years",
            minViewMode: "years",
            startDate: 'toDay',
            orientation: 'bottom',
            clearBtn: true
        });

        $('#amount').on('change', function(){
            if($(this)[0].value <= 0)
                $(this)[0].value = 1;
        });

        $('#rankSelect, #team_id').on('change', function(){
            const rank_selected = $('#rankSelect').find(':selected').val();
            $('#parentPositionSelectBody').empty();
            $('#childPositionSelectBody').empty();

            if (parseInt(rank_selected) <= 5 || parseInt(rank_selected) == 9 ) {
                $('#parentPositionSelectBody').html( `
                    <div class = "mb-3">
                        <label  class="form-label">Chọn vị trí cấp trên</label>
                        <select class="form-select"  name="parent_position_id" id = "parent_position_id">
                        </select>
                    </div>
                `);
            }
            if (rank_selected != '') {
                $('#childPositionSelectBody').html( `
                    <div class = "mb-3">
                        <label  class="form-label">Chọn vị trí cấp dưới</label>
                        <select class="form-select"  name="child_position_id" id = "child_position_id">
                        </select>
                    </div>
                `);
                

                $.ajax({
                    method: 'GET',
                    url: "{{ route('position.getUpperPositions') }}",
                    data: {
                        team_id: $('#team_id').find(':selected').val(),
                        rank: rank_selected,
                    },
                    success: response => {
                        if (response.status) {
                             $('#parent_position_id').append($('<option>', {
                                value: '',
                                text: 'Không Chọn'
                            }));

                            $('#child_position_id').append($('<option>', {
                                value: '',
                                text: 'Không Chọn'
                            }));
                            response.data.forEach(position => {
                                $('#parent_position_id').append($('<option>', {
                                    value: position['id'],
                                    text: position['name']
                                }));
                            });
                            // $('#child_position_id').append($('<option>', {
                            //     value: null,
                            //     text: 'Không Chọn'
                            // }));
                            // response.data.lowerPositions.forEach(position => {
                            //     $('#child_position_id').append($('<option>', {
                            //         value: position['id'],
                            //         text: position['name']
                            //     }));
                            // });
                        } else 
                            swal.fire({
                                icon: 'error',
                                title: response.message,
                                showConfirmButton: true,
                            }).then( r => {
                                $('#parentPositionSelectBody').empty();
                                $('#rankSelect').find(':selected')[0].selected = false;
                                $('#rankSelect option[value=""]')[0].selected = true;
                            });
                    }
                });
            
                $('#parent_position_id').on('change', function() {
                    $('#child_position_id')
                        .find('option')
                        .remove();

                    $('#child_position_id').append($('<option>', {
                            value: '',
                            text: 'Không Chọn'
                        }));
                    
                    $.ajax({
                        method: 'GET',
                        url: "{{ route('position.getLowerPositions') }}",
                        data: {
                            parent_position_id: $('#parent_position_id').find(':selected').val(),
                            rank: rank_selected
                        },
                        success: res => {
                            res.data.forEach(position => {
                                $('#child_position_id').append($('<option>', {
                                    value: position['id'],
                                    text: position['name']
                                }));
                            });
                        }
                    });
                });
            }
        });

        $('#savePositionButton').on('click', function(e){
            e.preventDefault();

            $.ajax({
                method: 'POST',
                url: "{{ route('position.create') }}",
                data: $('#formCreatePosition').serialize(),
                success: res => {
                    swal.fire({
                        icon  : res.status ? 'success' : 'error',
                        title : res.message,
                        showConfirmButton: false,
                    }).then(r => {
                        if (res.status)
                            window.location.reload();
                    });
                } 
            });

        });
    });
</script>
@endsection
