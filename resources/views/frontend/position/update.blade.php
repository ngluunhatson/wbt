@extends('layouts.fe.master2')
@section('title')
    @lang('translation.position.update')
@endsection
@section('css')
    <link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    {{-- @component('components.breadcrumb')
@slot('li_1') Pages @endslot
@slot('title') Starter  @endslot
@endcomponent --}}

    <div class="row">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h5 class="card-title mb-0">Sửa </h5>

            </div>

            <form action="{{ route('position.update', $positions->id) }}" method="post">
                @method('put')
                @csrf
                <div class="card-body">
                    <div class="row g-3">
                        <div class="col-xxl-12">
                            <div>
                                <label for="name" class="form-label">Tên chức danh</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    placeholder="(Điền thông tin)" value="{{ $positions->name }}">
                            </div>
                            <div>
                                <label for="team_id" class="form-label">Chọn bộ phận</label>
                                <select class="form-control" name="team_id" id="team_id">
                                    @foreach ($teams as $team)
                                        <option @if ($team->id == $positions->team_id) selected @endif
                                            value="{{ $team->id }}">{{ $team->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div>
                                <label for="name" class="form-label">Số lượng :</label>
                                <input type="number" class="form-control" id="amount" value="{{ $positions->amount }}"
                                    name="amount" placeholder="(Điền thông tin)">
                            </div>
                            <div>
                                <label for="name" class="form-label">Năm :</label>
                                <input type="text" class="form-control datepicker" value="{{ $positions->year }}"
                                    id="year" name="year" value="{{ $positions->year }}"
                                    placeholder="(Điền thông tin)">
                            </div>
                            <div>
                                <label for="code_team" class="form-label">Cấp bậc :</label>
                                <select class="form-control" name="rank" id="rankSelect">
                                    <option value="">Vui lòng chọn cấp bậc</option>
                                    @foreach (config('constants.rank_chart') as $key => $rank)
                                        <option @if ($positions->rank == $key) selected @endif
                                            value="{{ $key }}">{{ $rank }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-check form-switch form-switch-lg" dir="ltr">
                                <label for="name" class="form-label">Hiện trên sơ đồ</label>
                                <input type="checkbox" class="form-check-input" value="1" name="flag_chart"
                                    placeholder="(Điền thông tin)" @checked($positions->flag_chart == 1)>
                            </div>
                            <div class="form-check form-switch form-switch-lg" dir="ltr">
                                <label for="name" class="form-label">Hiện trên filter</label>
                                <input type="checkbox" class="form-check-input" @checked($positions->flag_filter == 1) value="1"
                                    name="flag_filter" placeholder="(Điền thông tin)">
                            </div>
                        </div>

                        <!--end col-->
                        <div class="col-lg-12">
                            <div class="hstack gap-2 justify-content-end">
                                <button type="submit" class="btn btn-primary">Lưu</button>
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->
                </div>
            </form>





        </div>
    </div>
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(function() {
            $('#year').datepicker({
                format: 'yyyy',
                startView: "years",
                minViewMode: "years",
                startDate: 'toDay',
                orientation: 'bottom',
                clearBtn: true
            });

           
        });
    </script>
@endsection
