@extends('layouts.fe.master2')
@section('title') @lang('translation.starter') @endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<style>
   .disabled {
      background-color: #898989;
      color: #ffffff;
      opacity: 0.5;
      pointer-events: none;
   }
</style>
<div class="row">
   <div class="col-12 d-flex justify-content-between mb-3">
      <h3 class="d-inline m-0">@lang('translation.dayoff.insert.page_title')</h3>
      <a href="#" noref>
         <button type="button" id="saveDayoff" class="btn btn-success btn-label waves-effect waves-light">
            <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Lưu</button>
      </a>
   </div>
</div>

<form id="formInsert" method="POST" action="{{ route('dayoff.store') }}">
   @csrf
   <div class="row">
      <div class="card">
         <div class="card-header">
            <div class="form-check form-switch form-switch-lg" dir="ltr">
               <input name="all_company" type="checkbox" class="form-check-input" id="select_all">
               <h3 class="form-check-label" for="customSwitchsizelg">Toàn bộ công ty</h3>
            </div>
         </div>
         <div class="row card-body group_select">
            <div class="col-xl-3">
               <label class="form-label">Phòng</label>
               <select id="selectRoom" name="room_id" class="form-select mb-3 @error('room_id') is-invalid @enderror" aria-label="Default select example">
                  <option value="">Vui lòng chọn phòng ban</option>
                  @foreach($dataInit['rooms'] as $room)
                  <option @if(old('room_id')==$room['id']) selected @endif value="{{ $room['id'] }}">{{ $room['name'] }}</option>
                  @endforeach
               </select>
               @error('room_id')
               <span class="text-danger" role="alert">
                  <strong>{{ $message }}</strong>
               </span>
               @enderror
            </div>
            <div class="col-xl-3">
               <label class="form-label">Bộ phận</label>
               <select id="selectTeam" name="team_id" class="form-select mb-3" aria-label="Default select example">
               </select>
            </div>
            <div class="col-xl-3">
               <label class="form-label">Chức danh</label>
               <select id="selectPosition" name="position_id" class="form-select mb-3" aria-label="Default select example">
               </select>
            </div>
            <div class="col-xl-3">
               <label class="form-label">Mã nhân viên | Họ và tên</label>
               <select id="selectStaff" name="basic_info_id" class="form-select mb-3" aria-label="Default select example">
               </select>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="card">
         <div class="card-header">
            <h3 class="mt-3">Thời gian nghỉ</h3>
         </div>
         <div class="row card-body">
            <div class="col-xl-3">
               <label for="start_date" class="form-label">Ngày bắt đầu</label>
               <input name="start_date" type="text" class="form-control datepicker" id="start_date" value="{{ old('start_date', now()->format('Y-m-d')) }}">
               <div>
                  <label for="number_of_tracking_day_off" class="form-label">Phép Năm Sử Dụng cho Cả Công Ty</label>
                  <input class="form-control group_select_inversed" name="number_of_tracking_day_offs" id="NumOfTrackingDayOff" type="number" value=0>
               </div>
            </div>

            <div class="col-xl-3">
               <div>
                  <label for="start_time" class="form-label">Giờ bắt đầu</label>
                  <select id="start_time" name="start_time" class="form-select mb-3">
                     <option @if(old('start_time')=='8:00' ) selected @endif value="8:00"> 8:00 </option>
                     <option @if(old('start_time')=='13:00' ) selected @endif value="13:00"> 13:00 </option>
                  </select>
               </div>
            </div>
            <div class="col-xl-3">
               <label for="end_date" class="form-label">Ngày kết thúc</label>
               <input name="end_date" type="text" class="form-control datepicker" id="end_date" value="{{ old('end_date', date('Y-m-d', strtotime('+1 day' . now())) ) }}">
            </div>
            <div class=" col-xl-3">
               <div>
                  <label for="end_time" class="form-label">Giờ kết thúc</label>
                  <select id="end_time" name="end_time" class="form-select mb-3">
                     <option @if(old('end_time')=='12:00' ) selected @endif value="12:00"> 12:00 </option>
                     <option @if(old('end_time')=='17:00' ) selected @endif value="17:00"> 17:00 </option>
                  </select>
               </div>



            </div>


            <input hidden name='total_day_off' id='total_day_off'>
            <h3 class="mt-3 text-center text-danger">Tổng số ngày: <span id="countDateOff"></span> ngày</h3>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="card">
         <div class="card-body">
            <div class="col-12">
               <div class="form-group">
                  <label for="basicInput" class="form-label">Tiêu đề</label>
                  <input name="title" type="text" value="{{ old('title') }}" class="form-control @error('title') is-invalid @enderror" id="basicInput">
                  @error('title')
                  <span class="text-danger" role="alert">
                     <strong>{{ $message }}</strong>
                  </span>
                  @enderror
               </div>
            </div>
            <div class="col-12 mt-3">
               <label for="exampleFormControlTextarea5" class="form-label">Ghi chú</label>
               <textarea name="note" class="form-control @error('note') is-invalid @enderror" id="exampleFormControlTextarea5" rows="3">{{ old('note') }}</textarea>
               @error('note')
               <span class="text-danger" role="alert">
                  <strong>{{ $message }}</strong>
               </span>
               @enderror
            </div>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="card">
         <div class="row card-body justify-content-around">
            <div class="col-xl-3">
               <div class="row ">
                  <div class="col-12 text-center">
                     <h5>Người Tạo Ngày Nghỉ</h5>
                  </div>
                  <div class="col-12 text-center">
                     <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ auth()->user()->name }}</label>
                  </div>
                  <!-- <div class="col-12 text-center">
                                    <label for="firstNameinput" class="form-label mb-0 mt-4 ">Đã xác nhận</label>
                                </div> -->

                  <div class="col-12 d-flex justify-content-center mt-4">
                     <div class=" form-check" style="width: fit-content;">
                        <input class="form-check-input fs-15" type="checkbox" id="assign" value="1" name="assign">
                     </div>
                     <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác
                        nhận</label>
                  </div>

               </div>
            </div>
            <div class="col-xl-3">
               <div class="row  ">
                  <div class="col-12 text-center">
                     <h5>P.QT HC-NS</h5>
                  </div>
                  <input hidden name="type_confirm_2" value="2">
                  <div class="col-12">
                     <select id="selectHr" name="selectConfirm[]" class="form-select text-center mt-3" aria-label="Default select example">
                        @if(!empty($dataInit['hr']))
                        @foreach($dataInit['hr'] as $value)
                        @if(!empty($value['basic_info_id']))
                        <option value="{{ $value->staff->id }}">{{ $value->staff->full_name }}
                        </option>
                        @endif
                        @endforeach
                        @endif
                     </select>
                  </div>
               </div>
               <!-- <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6> -->
               <!-- <div class="row">
               <div class="col-6 form-check mb-2">
                  <div class="d-flex justify-content-center">
                     <input class="form-check-input" type="radio" name="department_resource_accept" value="1" id="department_resource_accept1">
                     <label class="form-check-label ms-1" for="department_resource_accept1">Duyệt</label>
                  </div>
               </div>
               <div class="col-6 form-check">
                  <div class="d-flex justify-content-center">
                     <input class="form-check-input" type="radio" name="department_resource_accept" value="0" id="department_resource_accept2" checked>
                     <label class="form-check-label " for="department_resource_accept2">Từ chối</label>
                  </div>
               </div>
            </div>
            <div class="col-12 mt-3">
               <label for="department_resource_notes" class="form-label">Lý do</label>
               <textarea name="department_resource_notes" class="form-control" id="department_resource_notes" rows="3"></textarea>
            </div> -->
            </div>
            <div class="col-xl-3">
               <div class="row  ">
                  <div class="col-12 text-center">
                     <h5>Ban Giám Đốc</h5>
                  </div>
                  <input hidden name="type_confirm_3" value="3">
                  <div class="col-12">
                     <select name="selectConfirm[]" class="form-select text-center mt-3" aria-label="Default select example">
                        @if(!empty($dataInit['manager']))
                        @foreach($dataInit['manager'] as $value)
                        @if(!empty($value['basic_info_id']))
                        <option value="{{ $value->staff->id }}">{{ $value->staff->full_name}}
                        </option>
                        @endif
                        @endforeach
                        @endif
                     </select>
                  </div>
               </div>
               <!-- <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
            <div class="row">
               <div class="col-6 form-check mb-2">
                  <div class="d-flex justify-content-center">
                     <input class="form-check-input" type="radio" name="department_manager_accept" id="department_manager_accept1">
                     <label class="form-check-label ms-1" for="department_manager_accept1">Duyệt</label>
                  </div>
               </div>
               <div class="col-6 form-check">
                  <div class="d-flex justify-content-center">
                     <input class="form-check-input" type="radio" name="department_manager_accept" id="department_manager_accept2" checked>
                     <label class="form-check-label " for="department_manager_accept2">Từ chối</label>
                  </div>
               </div>
            </div>
            <div class="col-12 mt-3">
               <label for="department_manager_notes" class="form-label">Lý do</label>
               <textarea name="department_manager_notes" class="form-control" id="department_manager_notes" rows="3"></textarea>
            </div> -->

            </div>
         </div>
      </div>
</form>
@endsection
@section('script')
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script>
   $(document).ready(function() {

      $('#saveDayoff').on('click', function(e) {
         e.preventDefault();
         $('#formInsert').submit();
      })

      $('#start_date').datepicker({
         locale: 'vi',
         format: 'yyyy-mm-dd',
         todayHighlight: true,
         daysOfWeekDisabled: [0]
      });

      $('#end_date').datepicker({
         locale: 'vi',
         format: 'yyyy-mm-dd',
         todayHighlight: true,
         startDate: $('#start_date').val(),
         daysOfWeekDisabled: [0]
      });

      $('#start_date').on('change', function() {
         if ($(this)[0].value > $('#end_date')[0].value) {
            $('#end_date').datepicker('clearDates');
         }

         $('#end_date').datepicker('destroy');
         $('#end_date').datepicker({
            locale: 'vi',
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            daysOfWeekDisabled: [0],
            startDate: $(this)[0].value
         });

      });

      $('#NumOfTrackingDayOff').on('change', function(e) {
         let x = $(this).val();
         const x_string = x.toString();

         if (x_string[0] == '0' && x_string[1] != ".") {
            x = parseInt(x_string.slice(1));
            $(this).val(x);
         }

         if (x < 0 || x % (0.5) != 0 || x > $('#total_day_off').val())
            $(this).val(0);


      });

      $('#selectRoom').on('change', function() {
         var id = $('#selectRoom').find(":selected").val();
         $('#selectTeam')
            .find('option')
            .remove();
         $('#selectPosition')
            .find('option')
            .remove();
         $('#selectStaff')
            .find('option')
            .remove();
         if (id != '') {
            var url = "{!! route('dayoff.getTeamByRommID', ':id') !!}";

            $.ajax({
               type: 'GET',
               url: url.replace(':id', id),
               success: function(data) {
                  if (data.length > 0) {
                     $('#selectTeam')
                        .find('option')
                        .remove()
                     $('#selectTeam').append($('<option>', {
                        value: 0,
                        text: 'Tất cả phòng ban'
                     }));
                     data.forEach(function(value) {
                        $('#selectTeam').append($('<option>', {
                           value: value.id,
                           text: value.name
                        }));
                     })
                  } else {
                     $('#selectTeam')
                        .find('option')
                        .remove()

                     if ($('#selectRoom').val() != 0) {
                        $('#selectTeam').append($('<option>', {
                           value: 0,
                           text: 'Tất cả phòng ban'
                        }));
                     }
                  }
               }
            });
         }
      });


      $('#selectTeam').on('change', function() {
         var id = $('#selectTeam').find(":selected").val();
         var url = "{!! route('dayoff.getPositionByRommID', ':id') !!}";
         $('#selectPosition')
            .find('option')
            .remove();
         $('#selectStaff')
            .find('option')
            .remove();
         $.ajax({
            type: 'GET',
            url: url.replace(':id', id),
            success: function(data) {
               if (data.length > 0) {
                  $('#selectPosition')
                     .find('option')
                     .remove();
                  $('#selectPosition').append($('<option>', {
                     value: 0,
                     text: 'Tất cả chức danh'
                  }));

                  data.forEach(function(value) {
                     $('#selectPosition').append($('<option>', {
                        value: value.id,
                        text: value.name
                     }));
                  })
               } else {
                  $('#selectPosition')
                     .find('option')
                     .remove();

                  if ($('#selectTeam').val() != 0) {
                     $('#selectPosition').append($('<option>', {
                        value: 0,
                        text: 'Tất cả chức danh'
                     }));
                  }

               }
            }
         });
      });

      $('#selectPosition').on('change', function() {
         var room_id = $('#selectRoom').find(":selected").val();
         var team_id = $('#selectTeam').find(":selected").val();
         var position_id = $('#selectPosition').find(":selected").val();

         var url = "{!! route('dayoff.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";


         $('#selectStaff')
            .find('option')
            .remove();

         $.ajax({
            type: 'GET',
            url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
            success: function(data) {
               if (data.length > 0) {
                  $('#selectStaff')
                     .find('option')
                     .remove()
                  $('#selectStaff').append($('<option>', {
                     value: 0,
                     text: 'Tất cả nhân viên'
                  }));
                  data.forEach(function(employee) {
                     $('#selectStaff').append($('<option>', {
                        value: employee['id'],
                        text: `${employee["employee_code"]} | ${employee['full_name']}`,
                     }));
                  })
               } else {
                  $('#selectStaff')
                     .find('option')
                     .remove();

                  if ($('#selectPosition').val() != 0) {
                     $('#selectStaff').append($('<option>', {
                        value: 0,
                        text: 'Tất cả nhân viên'
                     }));
                  }
               }
            }
         });
      });

      $(function() {
         if ($('#select_all').is(":checked")) {
            $('.group_select').addClass('disabled');
         } else {
            $('.group_select_inversed').addClass('disabled');
         }
      });

      $('#select_all').change(function() {
         if ($(this).is(":checked")) {
            $('.group_select').addClass('disabled');
            $('.group_select_inversed').removeClass('disabled');
         } else {
            $('.group_select').removeClass('disabled');
            $('.group_select_inversed').addClass('disabled');
         }
      });

      getDayOff();

      $('#start_date, #end_date, #start_time, #end_time').on('change', function() {
         getDayOff();
      })
   })

   function getDayOff() {
      var dateStart = $('#start_date').val();
      var timeStart = $('#start_time').val();
      var dateEnd = $('#end_date').val();
      var day2 = new Date(dateEnd);

      if (day2.getDay() == 6) {
         $("#end_time").find('option').removeAttr("selected");
         $('#end_time').val('12:00');
         $(`#end_time option[value="12:00"]`).attr('selected', true)
      }
      var timeEnd = $('#end_time').val();


      var day1 = new Date(dateStart);
      var time1 = Number.parseInt(timeStart)

      var time2 = Number.parseInt(timeEnd)

      var countDayDiff = 0;
      var flgExceptSta = true;

      let countDay = 0

      let sundayCount = 0;

      let saturdayCount = 0;






      if (day1 <= day2) {
         var diffDays = Math.ceil(Math.abs(day2 - day1) / (1000 * 60 * 60 * 24));
         for (i = day1; i <= day2; i.setDate(i.getDate() + 1)) {
            if (i.getDay() == 0) {
               sundayCount += 1;
            }

            if (i.getDay() == 6 && i < day2) {
               saturdayCount += 1;
            }
         }


         //    if (i.getDay() == 6 && i < day2) {
         //       countDayDiff += 0.5;
         //    }
         // }
         // var countDay = diffDays - countDayDiff;
         // var diffTime = time2 - time1;

         // if (diffTime > 8) {
         //    countDay = countDay + 1;
         // } else if (diffTime < 5) {
         //    countDay = countDay + 0.5
         // }

         if (time1 == 13) {
            diffDays -= 0.5;
         }

         if (time2 == 12) {
            diffDays += 0.5;
         } else {
            diffDays += 1;
         }


         countDay = diffDays;
         countDay -= sundayCount;
         if (saturdayCount > 0)
            countDay -= saturdayCount * 0.5;

      }

      if (countDay == 0 && ($('#start_date').val() == $('#end_date').val())) {
         $("#start_time").find('option').removeAttr("selected");
         $('#start_time').val('8:00');
         $(`#start_time option[value="8:00"]`).attr('selected', true)
         countDay += 0.5;
      }

      $('#countDateOff').text(countDay);
      $('#total_day_off').val(countDay);
   }
</script>
@endsection