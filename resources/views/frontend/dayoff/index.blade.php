@extends('layouts.fe.master2')
@section('title')
@endsection
@section('content')
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="border-radius: 10px !important;
    overflow: hidden !important;">
    
    </div>
  </div>
</div>
<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h3 id="titlePage">DANH SÁCH NGÀY NGHỈ</h3>
        @can('dayoff-create')
        <a href="/dayoff/create">
            <button type="button" class="btn btn-success btn-label waves-effect waves-light"><i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm</button>
        </a>
        @endcan
    </div>
</div>

<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h3 class="d-inline m-0"></h3>
    </div>

    <div class="card-body">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">
            @can('dayoff-index')
            <li class="nav-item">
                <a class="nav-link active" data-bs-toggle="tab" href="#my-day-offs" role="tab" aria-selected="false">
                    Ngày Nghỉ của Tôi
                </a>
            </li>
            @endcan

            @can('dayoff-index_calendar')
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#calendar-day-offs" role="tab" aria-selected="false">
                    Lịch của Tôi
                </a>
            </li>
            @endcan
            
            @can('dayoff-index_admin')
            <li class="nav-item">
                <a class="nav-link @if(!auth() -> user() -> hasAnyPermission(['dayoff-index'])) active @endif" data-bs-toggle="tab" href="#all-day-offs" role="tab" aria-selected="false">
                    Ngày Nghỉ của Công Ty
                </a>
            </li>
            @endcan
        </ul>
        <div class="tab-content text-muted">
            @can('dayoff-index')
            <div class="tab-pane active overflow-auto" id="my-day-offs" role="tabpanel">
                <div class="col-12 d-flex justify-content-between mb-3">
                    <h3 class="d-inline m-0"></h3>
                </div>
                <div class="row">
                    <div class="card">
                        <div id="my-day-offs" class="card-body">
                            <table id="my-day-offs-table" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Phòng</th>
                                        <th>Bộ Phận</th>
                                        <th>Vị trí </th>
                                        <th>Nhân Viên</th>
                                        <th>Từ Ngày</th>
                                        <th>TGBĐ</th>
                                        <th>Đến Ngày</th>
                                        <th>TGKT</th>
                                        <th>Tổng Ngày Nghỉ</th>
                                        <th>Người tạo</th>
                                        <th>Trạng thái</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataInit['myDayOffs'] as $key => $myDayOffs_X)
                                    <tr>
                                        <td class="dtr-control">{{ $key + 1 }}</td>

                                        <td>{{ $myDayOffs_X -> all_company == 1    ? "TOÀN BỘ CÔNG TY"                 : ($myDayOffs_X -> room != null       ? $myDayOffs_X -> room -> name   : "N/A")  }}</td>
                                        <td>{{ $myDayOffs_X -> all_company == 1    ? "N/A"                             : ($myDayOffs_X -> team_id == 0       ? "TOÀN BỘ BỘ PHẬN"           : ( ($myDayOffs_X -> team)      ? $myDayOffs_X -> team -> name           : "N/A" )) }}</td>
                                        <td>{{ $myDayOffs_X -> all_company == 1    ? "N/A"                             : ($myDayOffs_X -> position_id == 0   ? "TOÀN BỘ VỊ TRÍ"            : ( ($myDayOffs_X -> position)  ? $myDayOffs_X -> position -> name       : "N/A" )) }}</td>
                                        <td>{{ $myDayOffs_X -> all_company == 1    ? "N/A"                             : ($myDayOffs_X -> basic_info_id == 0 ? "TOÀN BỘ NHÂN VIÊN"         : ( ($myDayOffs_X -> basicInfo) ? $myDayOffs_X -> basicInfo -> full_name : "N/A" )) }}</td>

                                        <td>{{ date('Y-m-d', strtotime($myDayOffs_X -> detailDayOff -> start_date)) }}</td>
                                        <td>{{ $myDayOffs_X -> detailDayOff -> start_time }}</td>
                                        <td>{{ date('Y-m-d', strtotime($myDayOffs_X -> detailDayOff -> end_date)) }} </td>
                                        <td>{{ $myDayOffs_X -> detailDayOff -> end_time }} </td>
                                        <td>{{ $myDayOffs_X -> detailDayOff -> total_day_off }} </td>

                                        <td>{{ $myDayOffs_X -> user -> name }} </td>
                                        <td>{{ config('constants.status_confirm.dayoff')[$myDayOffs_X -> status] }} </td>

                                        <td>
                                            <div class="dropdown d-inline-block">
                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <i class="ri-more-fill align-middle"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-end">
                                                    <li><a href="/dayoff/view/{{$myDayOffs_X->id}}" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>

                                                    @can('dayoff-update')
                                                    @if (auth() -> id() == $myDayOffs_X -> created_by && (!in_array($myDayOffs_X -> status, [3, 4])))
                                                    <li><a href="/dayoff/edit/{{$myDayOffs_X->id}}" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                                                    @endif
                                                    @endcan('dayoff-update')

                                                    @can('dayoff-delete')
                                                    @if (auth() -> id() == $myDayOffs_X -> created_by && (!in_array($myDayOffs_X -> status, [3, 4])))
                                                    <li><a href="#" id="formDeleteButton{{ $myDayOffs_X -> id }}" class="dropdown-item remove-item-btn"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Delete</a>
                                                    </li>
                                                    <form method="post" id="formDelete{{ $myDayOffs_X -> id }}" action="{{  route('dayoffDelete', $myDayOffs_X->id) }}" style="display:inline">
                                                        @method('delete')
                                                        @csrf
                                                    </form>
                                                    @endif
                                                    @endcan
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endcan

            @can('dayoff-index_calendar')
            <div style="background-color: white;padding: 20px;
                    border-radius: 10px;box-shadow: rgba(50, 50, 93, 0.25) 0px 6px 12px -2px, rgba(0, 0, 0, 0.3) 0px 3px 7px -3px;" class="tab-pane" id="calendar-day-offs" role="tabpanel">
                <div class="col-12 d-flex justify-content-between mb-3">
                    <h3 class="d-inline m-0"></h3>
                </div>
                <div class="row">
                   <div>
                   <div id='calendar'></div> 
                   </div>
                </div>
            </div>
            @endcan

            @can('dayoff-index_admin')
            <div class="tab-pane @if(!auth() -> user() -> hasAnyPermission(['dayoff-index'])) active @endif overflow-auto"" id="all-day-offs" role="tabpanel">
                <div class="row">
                    <div class="card">
                        <div class="card-body overflow-auto">
                            <table id="all-day-offs-table" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Phòng</th>
                                        <th>Bộ Phận</th>
                                        <th>Vị trí </th>
                                        <th>Nhân Viên</th>
                                        <th>Từ Ngày</th>
                                        <th>TGBĐ</th>
                                        <th>Đến Ngày</th>
                                        <th>TGKT</th>
                                        <th>Tổng Ngày Nghỉ</th>
                                        <th>Người tạo</th>
                                        <th>Trạng thái</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataInit['listData'] as $key => $dayOff_X)
                                    <tr>
                                        <td class="dtr-control">{{ $key + 1 }}</td>

                                        <td>{{ $dayOff_X -> all_company == 1    ? "TOÀN BỘ CÔNG TY"                 : ($dayOff_X -> room != null       ? $dayOff_X -> room -> name   : "N/A")  }}</td>
                                        <td>{{ $dayOff_X -> all_company == 1    ? "N/A"                             : ($dayOff_X -> team_id == 0       ? "TOÀN BỘ BỘ PHẬN"           : ( ($dayOff_X -> team)      ? $dayOff_X -> team -> name           : "N/A" )) }}</td>
                                        <td>{{ $dayOff_X -> all_company == 1    ? "N/A"                             : ($dayOff_X -> position_id == 0   ? "TOÀN BỘ VỊ TRÍ"            : ( ($dayOff_X -> position)  ? $dayOff_X -> position -> name       : "N/A" )) }}</td>
                                        <td>{{ $dayOff_X -> all_company == 1    ? "N/A"                             : ($dayOff_X -> basic_info_id == 0 ? "TOÀN BỘ NHÂN VIÊN"         : ( ($dayOff_X -> basicInfo) ? $dayOff_X -> basicInfo -> full_name : "N/A" )) }}</td>

                                        <td>{{ date('Y-m-d', strtotime($dayOff_X -> detailDayOff -> start_date)) }}</td>
                                        <td>{{ $dayOff_X -> detailDayOff -> start_time }}</td>
                                        <td>{{ date('Y-m-d', strtotime($dayOff_X -> detailDayOff -> end_date)) }} </td>
                                        <td>{{ $dayOff_X -> detailDayOff -> end_time }} </td>
                                        <td>{{ $dayOff_X -> detailDayOff -> total_day_off }} </td>

                                        <td>{{ $dayOff_X -> user -> name }} </td>
                                        <td>{{ config('constants.status_confirm.dayoff')[$dayOff_X -> status] }} </td>

                                        <td>
                                            <div class="dropdown d-inline-block">
                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <i class="ri-more-fill align-middle"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-end">
                                                    <li><a href="/dayoff/view/{{$dayOff_X->id}}" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>

                                                    @can('dayoff-update')
                                                    @if (auth() -> id() == $dayOff_X -> created_by && (!in_array($dayOff_X -> status, [3, 4])))
                                                    <li><a href="/dayoff/edit/{{$dayOff_X->id}}" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                                                    @endif
                                                    @endcan('dayoff-update')

                                                    @can('dayoff-delete')
                                                    @if (auth() -> id() == $dayOff_X -> created_by && (!in_array($dayOff_X -> status, [3, 4])))
                                                    <li><a href="#" id="formDeleteAllButton{{ $dayOff_X -> id }}" class="dropdown-item remove-item-btn"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Delete</a>
                                                    </li>
                                                    <form method="post" id="formDeleteAll{{ $dayOff_X -> id }}" action="{{  route('dayoffDelete', $dayOff_X->id) }}" style="display:inline">
                                                        @method('delete')
                                                        @csrf
                                                    </form>
                                                    @endif
                                                    @endcan
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endcan
        </div><!-- end card-body -->
    </div>

    @endsection
    @section('script')
    <script>
      
        $(function() {
            $('#my-day-offs-table').dataTable();
            $('#all-day-offs-table').dataTable();

            const listData = <?php echo json_encode($dataInit['listData']); ?>;
            const myDayOffs = <?php echo json_encode($dataInit['myDayOffs']); ?>;
            console.log('myDayOffs',myDayOffs)
            Object.keys(listData).forEach(
                function(key) {
                    const strDeleteFormAllButton = '#formDeleteAllButton'.concat(listData[key]['id'].toString());
                    const strDeleteFormAll = '#formDeleteAll'.concat(listData[key]['id'].toString());
                    $(strDeleteFormAllButton).on('click', function(event) {
                        event.preventDefault();
                        swal.fire({
                                title: 'Bạn muốn xóa dữ liệu này ?',
                                text: "Lưu ý : dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                                icon: 'warning',
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Đồng ý',
                                showDenyButton: true,
                                denyButtonText: 'Hủy bỏ',
                            })
                            .then((result) => {
                                if (result.isConfirmed) {
                                    $(strDeleteFormAll).submit();
                                } else if (result.isDenied) {
                                    Swal.fire('Dữ liệu được giữ lại', '', 'info')
                                }
                            });
                    });
                }
            );

            Object.keys(myDayOffs).forEach(
                function(key) {
                    const strDeleteFormButton = '#formDeleteButton'.concat(myDayOffs[key]['id'].toString());
                    const strDeleteForm = '#formDelete'.concat(myDayOffs[key]['id'].toString());
                    $(strDeleteFormButton).on('click', function(event) {
                        event.preventDefault();
                        swal.fire({
                                title: 'Bạn muốn xóa dữ liệu này ?',
                                text: "Lưu ý : dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                                icon: 'warning',
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Đồng ý',
                                showDenyButton: true,
                                denyButtonText: 'Hủy bỏ',
                            })
                            .then((result) => {
                                if (result.isConfirmed) {
                                    $(strDeleteForm).submit();
                                } else if (result.isDenied) {
                                    Swal.fire('Dữ liệu được giữ lại', '', 'info')
                                }
                            });
                    });
                }
            );
        })
       
     
      
        $(document).ready(function() {
                // calendar        
               
                setTimeout(() => {
                let removeTime = document.getElementsByClassName('fc-time')
                Array.from(removeTime).map(el => $(el).remove())
                // console.log('removeTime',Array.from(removeTime))
                $('.fc-view-container').css('padding', '10px')
                }, 1000);
               
            
        var calendar = $('#calendar').fullCalendar({
        editable:false,
        header:{
            left:'prev,next today',
            center:'title',
            right:'month,agendaWeek,agendaDay'
        },
       
        events: function(start, end, timezone, callback) {
        
        $.ajax({
            method:'GET',
            url: "{{route('dayoffCalendar')}}",
            // dataType: 'xml',
            data: {
                start: start.unix(),
                end: end.unix()
            },
            success: function(doc) {
                
                callback(doc);      
                setTimeout(() => {
                var checkin = document.querySelectorAll('.fc-content-skeleton table tbody tr td')
                Array.from(checkin).map(el => {
                    if(el.className ==='') {
                        el.innerHTML = '&#10003';
                        $(el).css({'vertical-align': 'unset', 'color': 'green', 'font-size': '20px','text-align': 'center', 'padding-top': '10px'})
                    }
                })
                let removeTime = document.getElementsByClassName('fc-time')
                Array.from(removeTime).map(el => $(el).remove())
                }, 500);         
            }
        })
    },
        selectable:true,
        selectHelper: true,
        select:function(start, end, allDay)
        {
            // var title = prompt('Event Title:');

            // if(title)
            // {
            //     var start = $.fullCalendar.formatDate(start, 'Y-MM-DD HH:mm:ss');

            //     var end = $.fullCalendar.formatDate(end, 'Y-MM-DD HH:mm:ss');

            //     $.ajax({
            //         url:"/full-calender/action",
            //         type:"POST",
            //         data:{
            //             title: title,
            //             start: start,
            //             end: end,
            //             type: 'add'
            //         },
            //         success:function(data)
            //         {
            //             calendar.fullCalendar('refetchEvents');
            //             alert("Event Created Successfully");
            //         }
            //     })
            // }
        },
        eventResize: function(event, delta)
        {
            
        },
        eventDrop: function(event, delta)
        {
           // drag drop event 

           
        },

        eventClick:function(event)
        {   
            $('#exampleModal').modal('toggle')
            $('.modal-content').html(`<div style="border-bottom: 1px solid #e3e3e3;
            padding-bottom: 15px; background-color: #D5EDF8;"class="modal-header">
                    <h5 style="color: steelblue" class="modal-title" id="exampleModalLabel">Chi tiết ngày nghỉ</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div style="display: flex; align-items: center; margin-bottom: 15px; margin-top: 10px;">
                    <i style="margin-right: 10px" class="ri-calendar-event-line text-muted fs-16"></i>
                    <span style="font-size: 14px; color: #495057; font-weight: 600;">Từ ${event.start._i.split('T')[0]} đến ${event.end._i.split('T')[0]}</span>
                    </div>
                    <div style="display: flex; align-items: center; margin-bottom: 15px;">
                        <i style="margin-right: 10px" class="ri-time-line text-muted fs-16"></i>
                        <span style="font-size: 14px; color: #495057; font-weight: 600;">- ${event.start_time} - ${event.end_time}</span>
                    </div>
                    <div style="display: flex; align-items: center; margin-bottom: 15px;">
                        <i style="margin-right: 10px" class="bx bx-notepad"></i>
                        <span style="font-size: 14px; color: #495057; font-weight: 600;">${event.title}</span>
                    </div>

                    <div style="display: flex; align-items: center;">
                        <i style="margin-right: 10px" class="ri-discuss-line text-muted fs-16"></i>
                        <span style="font-size: 14px; color: #495057; font-weight: 600;">${event.note}</span>
                    </div>
                </div>
                <div class="modal-footer">
                <button data-bs-dismiss="modal" type="button" class="btn btn-soft-danger" id="btn-delete-event"><i class="ri-close-line align-bottom"></i> Delete</button>
                </div>`)
           
        }
    });


        });
        
    </script>

    @endsection