@extends('layouts.fe.master2')
@section('title') @lang('translation.starter') @endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<style>
    .disabled {
        background-color: #898989;
        color: #ffffff;
        opacity: 0.5;
        pointer-events: none;
    }
</style>

<div>
    @csrf
    @php

    $dayOff_X = $dataInit['dayOff'];
    $room = $dayOff_X -> room;

    $team_id = $dayOff_X -> team_id;
    $position_id = $dayOff_X -> position_id;
    $basic_info_id = $dayOff_X -> basic_info_id;

    $allTeamFlag = false;
    $allPositionFlag = false;
    $allBasicInfoFlag = false;

    if ($team_id === 0) {
    $allTeamFlag = true;
    }

    if ($position_id === 0) {
    $allPositionFlag = true;
    }

    if ($basic_info_id === 0) {
    $allBasicInfoFlag = true;
    }

    $team = $dayOff_X -> team;
    $position = $dayOff_X -> position;
    $basic_info = $dayOff_X -> basicInfo;


    @endphp
    <div class="card">
        <div class="card-header" style="margin:0 auto;">
            <h1 class="mt-3">Ngày Nghỉ</h1>
        </div>
    </div>
    <div class="row">
        <div class="card">
            <div class="card-header">
                <div class="form-check form-switch form-switch-lg" dir="ltr">
                    <input name="all_company" type="checkbox" class="form-check-input" @checked($dayOff_X['all_company']==1) disabled readonly>
                    <h3 class="form-check-label" for="customSwitchsizelg">Toàn bộ công ty</h3>
                </div>
            </div>

            <div class="row card-body">
                <div class="col-xl-3">
                    <label class="form-label">Phòng</label>
                    <label class="form-select mb-3" disabled>
                        {{$room -> name ?? 'N/A'}}
                    </label>
                </div>
                <div class="col-xl-3">
                    <label class="form-label">Bộ phận</label>
                    <label class="form-select mb-3" disabled>
                        {{ $allTeamFlag ? "Tất Cả Bộ Phận" : ($team -> name ?? 'N/A') }}
                    </label>
                </div>
                <div class="col-xl-3">
                    <label class="form-label">Chức danh</label>
                    <label class="form-select mb-3" disabled>
                        {{ $allPositionFlag ? "Tất Cả Chức Danh" : ($position -> name ?? 'N/A') }}
                    </label>
                </div>
                <div class="col-xl-3">
                    <label class="form-label">Mã nhân viên | Họ và tên</label>
                    <label class="form-select mb-3" disabled>
                        {{ $allBasicInfoFlag ? "Tất Cả Nhân Viên" : ($basic_info -> full_name ?? 'N/A') }}
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h3 class="mt-3">Thời gian nghỉ</h3>
            </div>
            <div class="row card-body">
                <div class="col-xl-3">
                    <label for="start_date" class="form-label">Ngày bắt đầu</label>
                    <input name="start_date" type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime($dayOff_X -> detailDayOff -> start_date)) }}" disabled>
                    @if($dayOff_X -> all_company)

                    <div>
                        <label for="number_of_tracking_day_off" class="form-label">Phép Năm Sử Dụng cho Cả Công Ty</label>
                        <input disabled readonly class="form-control group_select_inversed" name="number_of_tracking_day_offs" id="NumOfTrackingDayOff" type="number" value= "{{ $dayOff_X -> detailDayOff -> number_of_tracking_day_offs }}">
                    </div>

                    @endif
                </div>
                <div class="col-xl-3">
                    <div>
                        <label for="start_time" class="form-label">Giờ bắt đầu</label>
                        <input name="start_date" type="text" class="form-control datepicker" value="{{ $dayOff_X -> detailDayOff -> start_time }}" disabled>
                    </div>
                </div>
                <div class="col-xl-3">
                    <label for="end_date" class="form-label">Ngày kết thúc</label>
                    <input name="start_date" type="text" class="form-control datepicker" value="{{ date('Y-m-d', strtotime($dayOff_X -> detailDayOff -> end_date)) }}" disabled>
                </div>
                <div class=" col-xl-3">
                    <div>
                        <label for="end_time" class="form-label">Giờ kết thúc</label>
                        <input name="start_date" type="text" class="form-control datepicker" value="{{ $dayOff_X -> detailDayOff -> end_time }}" disabled>
                    </div>
                </div>


                <input hidden name='total_day_off' id='total_day_off'>
                <h3 class="mt-3 text-center text-danger">Tổng số ngày: <span> {{ $dayOff_X -> detailDayOff -> total_day_off }}</span> ngày</h3>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="col-12">
                    <div class="form-group">
                        <label for="basicInput" class="form-label">Tiêu đề</label>
                        <input type="text" value="{{ $dayOff_X -> title }}" class="form-control" disabled readonly>
                    </div>
                </div>
                <div class="col-12 mt-3">
                    <label for="exampleFormControlTextarea5" class="form-label">Ghi chú</label>
                    <textarea class="form-control" rows="3" disabled readonly>{{ $dayOff_X -> note }}</textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="card">
            <div class="col-12 d-flex mt-2 justify-content-end" style="padding-right: 2%;">
                @if ($dayOff_X -> review_date != null)
                <label for="confirm" class=" form-label mb-0 me-5 " style="margin-top: 2px">
                    @php
                    $date = \Carbon\Carbon::parse($dayOff_X -> review_date);
                    @endphp
                    Ngày duyệt : Ngày {{ $date->day }} Tháng {{ $date->month }} Năm {{ $date->year }}</label>
                @endif
            </div>
            <div class="row card-body justify-content-around">
                @php
                $confirmInfo = [];
                $idConfirmer = [];
                foreach( ($dayOff_X -> confirms) as $dayOffConfirm ) {
                $confirmInfo[] = $dayOffConfirm -> confirm;
                $idConfirmer[] = $dayOffConfirm -> confirm -> staff -> id;
                }

                $hr_confirm_and_info = $confirmInfo[0];
                $manager_confirm_and_info = $confirmInfo[1];

                @endphp

                <div class="col-xl-3">
                    <div class="row  ">
                        <div class="col-12 text-center">
                            <h5>Người Tạo Ngày Nghỉ</h5>
                        </div>
                        <div class="col-12 text-center">
                            <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ $dayOff_X -> user -> name }}</label>
                        </div>
                        <div class="col-12 text-center">
                            @if($dayOff_X -> status != 1)
                            <label for="firstNameinput" class="form-label mb-0 mt-4 ">Đã Xác Nhận</label>
                            @else
                            <label for="firstNameinput" class="form-label mb-0 mt-4 ">Chưa Xác Nhận</label>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="row  ">
                        <div class="col-12 text-center">
                            <h5>P.QT HC-NS</h5>
                        </div>
                        <div class="col-12 text-center">
                            <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ ($hr_confirm_and_info -> staff -> full_name) }}</label>
                        </div>
                        <div class="col-12 text-center">
                            @switch($hr_confirm_and_info -> status)
                            @case(0)
                            <label for="firstNameinput" class="form-label mb-0 mt-4">{{ $manager_confirm_and_info -> staff  == null ? 'Không cần xác nhận' : 'Chưa xác nhận' }}</label>
                            @break
                            @case(1)
                            <label for="firstNameinput" class="form-label mb-0 mt-4">{{ $hr_confirm_and_info -> staff == null ? 'Không cần xác nhận' : 'Đã xác nhận' }}</label>
                            @break
                            @case(2)
                            <label for="firstNameinput" class="form-label mb-0 mt-4">{{ $hr_confirm_and_info -> staff == null ? 'Không cần từ chối' : 'Từ chối' }}</label>
                            <div class="col-12 text-center">
                                <label>Lý do : {{ $hr_confirm_and_info -> note }}</label>
                            </div>
                            @break
                            @endswitch
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="row  ">
                        <div class="col-12 text-center">
                            <h5>Ban Giám Đốc</h5>
                        </div>
                        <div class="col-12 text-center">
                            <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ ($manager_confirm_and_info -> staff -> full_name) }}</label>
                        </div>
                        <div class="col-12 text-center">
                            @switch($manager_confirm_and_info -> status)
                            @case(0)
                            <label for="firstNameinput" class="form-label mb-0 mt-4">{{ $manager_confirm_and_info -> staff  == null ? 'Không cần xác nhận' : 'Chưa xác nhận' }}</label>
                            @break
                            @case(1)
                            <label for="firstNameinput" class="form-label mb-0 mt-4">{{ $manager_confirm_and_info -> staff == null ? 'Không cần xác nhận' : 'Đã xác nhận' }}</label>
                            @break
                            @case(2)
                            <label for="firstNameinput" class="form-label mb-0 mt-4">{{ $manager_confirm_and_info -> staff == null ? 'Không cần từ chối' : 'Từ chối' }}</label>
                            <div class="col-12 text-center">
                                <label>Lý do : {{ $manager_confirm_and_info -> note }}</label>
                            </div>
                            @break
                            @endswitch
                        </div>
                    </div>
                </div>

            </div>

        </div>
        @if (($dayOff_X -> status == 2 && auth() -> user() -> basic_info_id == $hr_confirm_and_info -> staff -> id) ||
        ($dayOff_X -> status == 3 && auth() -> user() -> basic_info_id == $manager_confirm_and_info -> staff -> id))
        @php
        $next_confirm_staff_id = $manager_confirm_and_info -> staff -> id;
        $confirm_id = $hr_confirm_and_info -> id;
        $hr_staff_id = null;
        if (auth() -> user() -> basic_info_id == $manager_confirm_and_info -> staff -> id) {
        $next_confirm_staff_id = null;
        $hr_staff_id = $hr_confirm_and_info -> staff -> id;
        $confirm_id = $manager_confirm_and_info -> id;
        }

        @endphp
        <form method="POST" action="{{ route('dayoff.updateStatus') }}" id="formConfirm">
            @csrf
            <input hidden name="confirm_id" value="{{ $confirm_id }}">
            <input hidden name="day_off_creator_user_id" value=" {{ $dayOff_X -> user -> id }} ">
            <input hidden name="next_confirm_staff_basic_id" value="{{ $next_confirm_staff_id }}">
            <input hidden name="hr_staff_basic_id" value=" {{  $hr_staff_id }}">
            <input hidden name="reason" id="reasonConfirm">
            <input hidden name="confirm_flag" id="confirm_flag" value="1">
            <input hidden name="day_off_id" value="{{ $dayOff_X -> id }}">
            <div class="row justify-content-center pe-3 mb-3 mt-3">
                <div class="col-3 justify-content-between d-flex">
                    <button data-id="1" type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content">
                        <i class="ri-check-double-line label-icon align-middle fs-16 me-2"></i> Duyệt</button>
                    <button data-id="2" type="submit" id="btnDeny" class="btn btn-danger waves-effect waves-light" style="width: fit-content">
                        <i class="ri-error-warning-line label-icon align-middle rounded-pill fs-16 me-2 "></i> Không Duyệt</button>
                </div>
            </div>
        </form>
        @endif
    </div>
    @endsection
    @section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            $('#btnDeny').on('click', function(event) {
                var status = $(this).data('id');
                var form = $(this).closest("form");
                event.preventDefault();
                swal.fire({
                        title: 'Lý do không duyệt đơn',
                        html: `<input class="form-control" type="text" id="reason"  placeholder="Lý do" required>`,
                        icon: 'warning',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Đồng ý',
                        showDenyButton: true,
                        denyButtonText: 'Hủy',
                        preConfirm: () => {
                            const reason = Swal.getPopup().querySelector('#reason').value
                            if (!reason) {
                                Swal.showValidationMessage(`Vui lòng nhập lý do`)
                            }
                            return {
                                reason: reason,
                            }
                        }
                    })
                    .then((result) => {
                        if (result.isConfirmed) {
                            $('#reasonConfirm').val(result.value.reason);
                            $('#confirm_flag').val(2);
                            form.submit();
                        } else if (result.isDenied) {
                            Swal.fire('Your record is safe', '', 'info')
                        }

                    });
            });
        });
    </script>
    @endsection