@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')

@php
$rooms = $dataInit['rooms'];
$formDayOff_X = $dataInit['current_form_day_off'];
$basic_info_X = $formDayOff_X -> basicInfo;
$room_X = $basic_info_X -> organizational -> room;
$team_X = $basic_info_X -> organizational -> team;
$position_X = $basic_info_X -> organizational -> position;

@endphp
<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h3 class="d-inline m-0">Sửa Đơn Xin Nghỉ Phép</h3>
        <a href="#" noref>
            <button type="button" id="updateFormDayoff" class="btn btn-success btn-label waves-effect waves-light">
                <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Lưu</button>
        </a>
    </div>
</div>
<form id="formUpdate" method="POST" action="{{ route('FormDayOffStoreDataUpdate') }}">
    @csrf
    <div class="card">
        <div class="card-body">
            <div class="row mt-3">
                <div class="col-xl-3">
                    <label for="borderInput" class="form-label">Phòng</label>
                    <select id="selectRoom" name="room_id" class="form-select mb-3" aria-label="Default select example">
                        <option value="{{ $room_X  != null ? $room_X -> id : '' }}"> {{ $room_X != null ? $room_X -> name : 'Vui lòng chọn phòng ban' }}</option>
                        @foreach($dataInit['rooms'] as $room)
                        @if($room -> id != $room_X -> id)
                        <option @if(old('room_id')==$room['id']) selected @endif value="{{ $room['id'] }}">{{ $room['name'] }}</option>
                        @endif
                        @endforeach
                    </select>

                </div>
                <div class="col-xl-3">
                    <label class="form-label">Bộ phận</label>
                    <select id="selectTeam" name="team_id" class="form-select mb-3" aria-label="Default select example">
                    </select>
                </div>
                <div class="col-xl-3">
                    <label class="form-label">Chức danh</label>
                    <select id="selectPosition" name="position_id" class="form-select mb-3" aria-label="Default select example">
                    </select>
                </div>
                <div class="col-xl-3">
                    <label class="form-label">Mã nhân viên | Họ và tên</label>
                    <select id="selectStaff" name="basic_info_id" class="form-select mb-3 @error('basic_info_id') is-invalid @enderror" aria-label="Default select example">
                    </select>
                    @error('basic_info_id')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="row mt-4">
                    <div class="col-xl-6">
                        @if (intval(date ('m', strtotime(now()))) < 4 ) <h4 class="text-center" id="prevYearPTOLabel">Số phép còn của năm trước: <span id="current_total_prev_year_pto_span"></span> ngày </h4>
                            @else
                            <h4 class="text-center" id="prevYearPTOLabel">Phép Năm {{ (date ('Y', strtotime('-1 year'. now()))) }} đã hết hạn sử dụng</h4>
                            @endif

                            <h4 class="text-center" id="noWorkPrevYearLabel">Không Đi Làm vào Năm {{ (date ('Y', strtotime('-1 year'. now()))) }}</h4>
                    </div>
                    <div class="col-xl-6">
                        <h4 class="text-center">Phép Năm {{ (date ('Y', strtotime( now()))) }} còn được dùng: <span id="current_total_cur_year_pto_span"></span> ngày</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="card" id="FormOfTimeFrames">
            <div class="card-header">
                <h2>Thời gian xin nghỉ</h2>
            </div>

        </div>
        <div class='row justify-content-between'>
            <div class="col-2">

            </div>

            <div class="col-1 mx-auto">
                <button id="AddTimeFrameButton" type="button" class="btn btn-success btn-label waves-effect waves-light">
                    <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm</button>
            </div>

            <div class="col-2">
                <button id="RestoreTimeFrameButton" type="button" class="btn btn-danger btn-label waves-effect waves-light"> <i class="ri-arrow-go-back-line label-icon align-middle fs-16 me-2"></i> Hoàn Tác</button>

            </div>

        </div>

        <input hidden name='basic_info_id_of_form' id='basic_info_id_of_form'>
        <input hidden name='index_time_frame' id='index_time_frame'>
        <input hidden name='request_type' value='edit_admin'>
        <input hidden name='form_day_off_id' value="{{$formDayOff_X -> id}}">

        <div class="row">
            <div class="col-xl-4">
                <h3 id="CompanyDayOffLabel" class="text-center">Tổng số ngày Công Ty Cho nghỉ: <span id="company_day_off_span"></span> ngày</h3>
            </div>
            <div class="col-xl-4">

                <h3 id='TotalDayOffAskedForLabel' class="text-center">Tổng số ngày xin nghỉ: <span id="total_day_off_asked_for_span"></span> ngày</h3>
            </div>
            <div class="col-xl-4">
                <h3 id="DayOffNeedToAskForLabel" class="text-center">Tổng số ngày cần xin: <span id="total_day_off_need_to_ask_for_span"></span> ngày</h3>
            </div>
        </div>


        <div class="row d-flex justify-content-center pt-4">
            <div class="col-3 mx-auto">
                <h3 id="TrackingDayOffLeftLabel" class="text-danger">Số ngày phép năm còn lại: <span id="tracking_day_off_left_span"></span> ngày</h3>
            </div>
        </div>

        <div class="row">
            <div class="card">
                <div class="row card-body justify-content-around">
                    <div class="col-xl-2">
                        <div class="row ">
                            <div class="col-12 text-center">
                                <h4 class="form-label text-center">Người Tạo Đơn</h4>
                            </div>
                            <div class="col-12 text-center">
                                <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ auth()->user()->name }}</label>
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-4">
                                <div class=" form-check" style="width: fit-content;">
                                    <input class="form-check-input fs-15" type="checkbox" id="assign" value="1" name="assign">
                                </div>
                                <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác nhận</label>
                            </div>

                        </div>
                    </div>
                    @php
                    $replacement_basic_info = null;
                    $replacement_confirm = null;
                    $direct_boss_basic_info = null;
                    $direct_boss_confirm = null;
                    $hr_basic_info = null;
                    $hr_confirm = null;

                    $basic_info_of_replacement_array = $dataInit['basic_info_of_replacement_array'];
                    $basic_info_of_direct_boss_array = $dataInit['basic_info_of_direct_boss_array'];

                    $formDayOff_X_Confirms = $formDayOff_X -> confirms;

                    foreach ($formDayOff_X_Confirms as $formDayOffConfirm) {
                    $confirmX = $formDayOffConfirm -> confirm;
                    switch ($confirmX -> type_confirm) {
                    case 7:
                    $replacement_basic_info = $confirmX -> staff;
                    $replacement_confirm = $confirmX;
                    break;
                    case 8:
                    $direct_boss_basic_info = $confirmX -> staff;
                    $direct_boss_confirm = $confirmX;
                    break;
                    case 2:
                    $hr_basic_info = $confirmX -> staff;
                    $hr_confirm = $confirmX;
                    break;
                    }
                    }
                    @endphp
                    <div class="col-xl-2">
                        <h4 class="form-label text-center">Người Thay Thế</h4>
                        <select name="confirm_basic_info_ids[]" class="form-select" id="selectReplacement" aria-label="Default select example">
                        </select>
                        @if($replacement_confirm)
                        <input hidden name="confirm_ids[]" value=" {{ $replacement_confirm -> id }}">
                        @switch($replacement_confirm -> status)
                        @case(0)
                        <h6 class="form-label text-center fst-italic mt-2" id="replacementConfirmMessage">Chưa xác nhận</h6>
                        @break
                        @case(1)
                        <h6 class="form-label text-center fst-italic mt-2" id="replacementConfirmMessage">Đã xác nhận</h6>
                        @break
                        @case(2)
                        <h6 class="form-label text-center fst-italic mt-2" id="replacementConfirmMessage">Từ Chối</h6>
                        <label class="col-12 text-center" id="replacementConfirmMessageReason">Lý do : {{ $replacement_confirm -> note }}</label>
                        @break
                        @endswitch
                        @else
                        <h6 class="form-label text-center fst-italic mt-2" id="replacementConfirmMessage">Không Cần xác nhận</h6>
                        @endif

                    </div>
                    <div class="col-xl-2">
                        <input hidden name="confirm_ids[]" value=" {{ $direct_boss_confirm -> id }}">
                        <h4 class="form-label text-center">Người Quản Lý Trực Tiếp</h4>
                        <select name="confirm_basic_info_ids[]" class="form-select" id="selectDirectBoss" aria-label="Default select example">

                        </select>
                        @if($direct_boss_confirm)
                        @switch($direct_boss_confirm -> status)
                        @case(0)
                        <h6 class="form-label text-center fst-italic mt-2" id="directBossConfirmMessage">Chưa xác nhận</h6>
                        @break
                        @case(1)
                        <h6 class="form-label text-center fst-italic mt-2" id="directBossConfirmMessage">Đã xác nhận</h6>
                        @break
                        @case(2)
                        <h6 class="form-label text-center fst-italic mt-2" id="directBossConfirmMessage">Từ Chối</h6>

                        <label class="col-12 text-center" id="directBossConfirmMessageReason">Lý do : {{ $direct_boss_confirm -> note }}</label>

                        @break
                        @endswitch
                        @else
                        <h6 class="form-label text-center fst-italic mt-2" id="directBossConfirmMessage">Không Cần xác nhận</h6>
                        @endif
                    </div>
                    <div class="col-xl-2">
                        <input hidden name="confirm_ids[]" value=" {{ $hr_confirm -> id }}">
                        <h4 class="form-label text-center">P. QT HC-NS</h4>
                        <select name="confirm_basic_info_ids[]" id="selectHR" class="form-select" aria-label="Default select example">
                            @if(!empty($dataInit['hr']))
                            @foreach($dataInit['hr'] as $value)
                            @if(!empty($value['basic_info_id']))
                            <option @if($hr_basic_info -> id == $value->staff->id ) selected @endif value="{{ $value->staff->id }}">{{ $value->staff->full_name }}
                            </option>
                            @endif
                            @endforeach
                            @endif
                        </select>
                        @if($hr_confirm)
                        @switch($hr_confirm -> status)
                        @case(0)
                        <h6 class="form-label text-center fst-italic mt-2" id="hrConfirmMessage">Chưa xác nhận</h6>
                        @break
                        @case(1)
                        <h6 class="form-label text-center fst-italic mt-2" id="hrConfirmMessage">Đã xác nhận</h6>
                        @break
                        @case(2)
                        <h6 class="form-label text-center fst-italic mt-2" id="hrConfirmMessage">Từ Chối</h6>
                        <label class="col-12 text-center" id="hrConfirmMessageReason">Lý do : {{ $hr_confirm -> note }}</label>
                        @break
                        @endswitch
                        @else
                        <h6 class="form-label text-center fst-italic mt-2" id="hrConfirmMessage">Không Cần xác nhận</h6>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</form>
@endsection
@section('script')
<script src="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/form-pickers.init.js') }}"></script>
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>

<script>
    $(document).ready(function() {
        let index_time_frame = 1;
        let time_frame_dict = {};
        let cur_prev_tracking_day_off = [-1, -1];
        let company_day_offs = [];
        let confirmed_form_day_offs = [];
        const form_day_off_details = (<?php echo json_encode($formDayOff_X->detailForms); ?>);
        const form_day_off_X_id = (<?php echo json_encode($formDayOff_X->id); ?>);

        const cur_year = new Date().getFullYear();

        let second_load = false;


        $('#updateFormDayoff').on('click', function(e) {
            e.preventDefault();
            $(`#index_time_frame`).val(index_time_frame);
            var basic_info_id = $('#selectStaff').find(":selected").val();
            if (parseInt(basic_info_id) > 0)
                $('#basic_info_id_of_form').val(basic_info_id);

            var url = "{!! route('FormDayOffCheckForm') !!}";
            $.ajax({
                method: 'POST',
                url: url,
                data: $('#formUpdate').serialize(),
                success: function(response) {
                    if (response.success)
                        $('#formUpdate').submit();
                    else {
                        swal.fire({
                            icon: 'error',
                            title: `${response.message}`,
                            confirmButtonColor: '#3085d6',
                            showConfirmButton: false,
                        });
                    }

                }
            });
        });

        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            $('#selectTeam')
                .find('option')
                .remove();
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();

            if (id != '') {
                var url = "{!! route('dayoff.getTeamByRommID', ':id') !!}";

                $.ajax({
                    type: 'GET',
                    url: url.replace(':id', id),
                    success: function(data) {
                        if (data.length > 0) {
                            $('#selectTeam')
                                .find('option')
                                .remove()
                            $('#selectTeam').append($('<option>', {
                                value: 0,
                                text: 'Không Chọn'
                            }));
                            data.forEach(function(value) {
                                $('#selectTeam').append($('<option>', {
                                    value: value.id,
                                    text: value.name
                                }));
                            })
                        } else {
                            $('#selectTeam')
                                .find('option')
                                .remove()

                            if ($('#selectRoom').val() != 0) {
                                $('#selectTeam').append($('<option>', {
                                    value: 0,
                                    text: 'Không Chọn'
                                }));
                            }
                        }
                    }
                });
            }
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            var url = "{!! route('dayoff.getPositionByRommID', ':id') !!}";
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();
            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectPosition')
                            .find('option')
                            .remove();
                        $('#selectPosition').append($('<option>', {
                            value: 0,
                            text: 'Không Chọn'
                        }));

                        data.forEach(function(value) {
                            $('#selectPosition').append($('<option>', {
                                value: value.id,
                                text: value.name
                            }));
                        })
                    } else {
                        $('#selectPosition')
                            .find('option')
                            .remove();
                        $('#selectPosition').append($('<option>', {
                            value: 0,
                            text: 'Không Chọn'
                        }));

                    }
                }
            });
        });

        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();


            var url = "{!! route('dayoff.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";


            $('#selectStaff')
                .find('option')
                .remove();

            $.ajax({
                type: 'GET',
                url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectStaff').append($('<option>', {
                            value: 0,
                            text: `Không Có`,

                        }));

                        data.forEach(function(employee) {
                            $('#selectStaff').append($('<option>', {
                                value: employee['id'],
                                text: `${employee['full_name']}`,

                            }));
                        })
                    } else {
                        $('#selectStaff')
                            .find('option')
                            .remove();

                        $('#selectStaff').append($('<option>', {
                            value: 0,
                            text: `Không Có`,

                        }));


                    }
                }
            });
        });

        $('#selectStaff').on('change', function() {
            checkDirectBossAndReplacement();
            checkTrackingDayOff();
            checkConfirmedFormDayOffs();
            checkCompanyDayOff();
            checkRemainingPTO();
        });

        $('#selectReplacement, #selectDirectBoss, #selectHR').on('change', function() {
            checkForConfirmMessage();
        });

        $('#AddTimeFrameButton').on('click', function() {
            AddTimeFrame();
            index_time_frame++;
        });

        $(`#RestoreTimeFrameButton`).on('click', function() {
            index_time_frame = 1;
            second_load = false;

            Object.keys(time_frame_dict).forEach(
                function(key) {
                    $(`#FormOfTimeFrameHeader${key}`).remove();
                    $(`#FormOfTimeFrameBody${key}`).remove();
                    delete time_frame_dict[key];
                }
            )

            form_day_off_details.forEach(
                function(detailForm) {
                    AddTimeFrame(detailForm);
                    index_time_frame++;
                }
            );
            checkCompanyDayOff();
            checkRemainingPTO();
            second_load = true;
        });

        $(function() {
            $(`#TotalDayOffAskedForLabel`).hide();
            $(`#CompanyDayOffLabel`).hide();
            $(`#DayOffNeedToAskForLabel`).hide()
            $(`#noWorkPrevYearLabel`).hide();


            const room_X = (<?php echo json_encode($room_X); ?>);
            const team_X = (<?php echo json_encode($team_X); ?>);
            const position_X = (<?php echo json_encode($position_X); ?>);
            const basic_info_X = (<?php echo json_encode($basic_info_X); ?>);

            const original_replacement_basic_info = (<?php echo json_encode($replacement_basic_info); ?>);
            const original_direct_boss_basic_info = (<?php echo json_encode($direct_boss_basic_info); ?>);

            const basic_info_of_replacement_array = (<?php echo json_encode($basic_info_of_replacement_array); ?>);
            const basic_info_of_direct_boss_array = (<?php echo json_encode($basic_info_of_direct_boss_array); ?>);


            if (basic_info_X != null) {

                $('#selectStaff').append($('<option>', {
                    value: basic_info_X.id,
                    text: `${basic_info_X.full_name}`
                }));

                $('#selectDirectBoss').append($('<option>', {
                    value: original_direct_boss_basic_info.id,
                    text: original_direct_boss_basic_info.full_name
                }));


                if (original_replacement_basic_info) {
                    $('#selectReplacement').append($('<option>', {
                        value: original_replacement_basic_info.id,
                        text: original_replacement_basic_info.full_name
                    }))

                }
                $('#selectReplacement').append($('<option>', {
                    value: 0,
                    text: 'Không Có'
                }));

                basic_info_of_replacement_array.forEach(function(replacement_b_info) {
                    if ((original_replacement_basic_info && original_replacement_basic_info.id != replacement_b_info.id) || (!original_replacement_basic_info))
                        $('#selectReplacement').append($('<option>', {
                            value: replacement_b_info.id,
                            text: replacement_b_info.full_name
                        }));

                });

                basic_info_of_direct_boss_array.forEach(function(direct_boss_b_info) {
                    if (original_direct_boss_basic_info.id != direct_boss_b_info.id)
                        $('#selectDirectBoss').append($('<option>', {
                            value: direct_boss_b_info.id,
                            text: direct_boss_b_info.full_name
                        }));

                })


            }

            if (position_X != null) {
                $('#selectPosition').append($('<option>', {
                    value: position_X.id,
                    text: position_X.name
                }));
                var room_id = room_X.id;
                var team_id = team_X.id;
                var position_id = position_X.id;

                var url = "{!! route('dayoff.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";

                $.ajax({
                    type: 'GET',
                    url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
                    success: function(data) {
                        if (data.length > 0) {
                            data.forEach(function(employee) {
                                if (basic_info_X == null || employee.id != basic_info_X.id) {
                                    $('#selectStaff').append($('<option>', {
                                        value: employee['id'],
                                        text: `${employee['full_name']}`,
                                    }));
                                }
                            })
                            $('#selectStaff').append($('<option>', {
                                value: 0,
                                text: `Không Chọn`,
                            }));
                        } else {
                            $('#selectStaff')
                                .find('option')
                                .remove();
                            $('#selectStaff').append($('<option>', {
                                value: 0,
                                text: `Không Chọn`,
                            }));
                        }
                    }
                });
            }

            if (team_X != null) {
                $('#selectTeam').append($('<option>', {
                    value: team_X.id,
                    text: team_X.name
                }));

                var id = $('#selectTeam').find(":selected").val();
                var url = "{!! route('dayoff.getPositionByRommID', ':id') !!}";

                $.ajax({
                    type: 'GET',
                    url: url.replace(':id', id),
                    success: function(data) {
                        if (data.length > 0) {
                            data.forEach(function(value) {
                                if (position_X == null || value.id != position_X.id) {
                                    $('#selectPosition').append($('<option>', {
                                        value: value.id,
                                        text: value.name
                                    }));
                                }
                            })
                        } else {
                            $('#selectPosition')
                                .find('option')
                                .remove();
                        }
                    }
                });
            }


            if (room_X != null) {
                var id = $('#selectRoom').find(":selected").val();
                if (id != '') {
                    var url = "{!! route('dayoff.getTeamByRommID', ':id') !!}";

                    $.ajax({
                        type: 'GET',
                        url: url.replace(':id', id),
                        success: function(data) {
                            if (data.length > 0) {

                                data.forEach(function(value) {
                                    if (team_X == null || value.id != team_X.id) {
                                        $('#selectTeam').append($('<option>', {
                                            value: value.id,
                                            text: value.name
                                        }));
                                    }
                                })
                            } else {
                                $('#selectTeam')
                                    .find('option')
                                    .remove()
                            }
                        }
                    });
                }
            }

            checkTrackingDayOff();

            form_day_off_details.forEach(
                function(detailForm) {
                    AddTimeFrame(detailForm);
                    index_time_frame++;
                }
            );
            second_load = true;

            checkConfirmedFormDayOffs();
            console.log(confirmed_form_day_offs);


            checkCompanyDayOff();
            checkRemainingPTO();
        });

        function checkForConfirmMessage() {
            const replacement_basic_info_id = <?php echo json_encode(($replacement_basic_info) ? $replacement_basic_info->id : 0); ?>;
            const direct_boss_basic_info_id = <?php echo json_encode($direct_boss_basic_info->id); ?>;
            const hr_basic_info_id = <?php echo json_encode($hr_basic_info->id); ?>;

            $('#replacementConfirmMessage').hide();
            $('#directBossConfirmMessage').hide();
            $('#hrConfirmMessage').hide();
            $('#replacementConfirmMessageReason').hide()
            $('#directBossConfirmMessageReason').hide();
            $('#hrConfirmMessageReason').hide();

            const selected_replacement_basic_info_id = $('#selectReplacement').find(":selected").val();
            const selected_direct_boss_basic_info_id = $('#selectDirectBoss').find(":selected").val();
            const selected_hr_basic_info_id = $('#selectHR').find(":selected").val();


            if (selected_replacement_basic_info_id == replacement_basic_info_id) {
                $('#replacementConfirmMessage').show();
                $('#replacementConfirmMessageReason').show()
            }

            if (selected_direct_boss_basic_info_id == direct_boss_basic_info_id) {
                $('#directBossConfirmMessage').show();
                $('#directBossConfirmMessageReason').show();
            }

            if (selected_hr_basic_info_id == hr_basic_info_id) {
                $('#hrConfirmMessage').show();
                $('#hrConfirmMessageReason').show();
            }

        }

        function checkDirectBossAndReplacement() {
            var basic_info_id = $('#selectStaff').find(":selected").val();
            var url_get_replacement = "{!! route('FormDayOffHelper1', [':basic_info_id']) !!}";
            var url_get_direct_boss = "{!! route('FormDayOffHelper2', [':basic_info_id']) !!}";

            $.ajax({
                type: 'GET',
                url: url_get_replacement.replace(':basic_info_id', basic_info_id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectReplacement')
                            .find('option')
                            .remove()
                        $('#selectReplacement').append($('<option>', {
                            value: 0,
                            text: `Không Có`,

                        }));

                        data.forEach(function(replacement_basic_info) {
                            $('#selectReplacement').append($('<option>', {
                                value: replacement_basic_info['id'],
                                text: `${replacement_basic_info['full_name']}`,

                            }));
                        })
                    } else {
                        $('#selectReplacement')
                            .find('option')
                            .remove();

                        $('#selectReplacement').append($('<option>', {
                            value: 0,
                            text: `Không Có`,

                        }));


                    }
                }
            });

            $.ajax({
                type: 'GET',
                url: url_get_direct_boss.replace(':basic_info_id', basic_info_id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectDirectBoss')
                            .find('option')
                            .remove()

                        data.forEach(function(direct_boss_basic_info) {
                            $('#selectDirectBoss').append($('<option>', {
                                value: direct_boss_basic_info['id'],
                                text: `${direct_boss_basic_info['full_name']}`,

                            }));
                        })
                    } else {
                        $('#selectDirectBoss')
                            .find('option')
                            .remove();

                    }

                }
            });



        }

        function checkConfirmedFormDayOffs() {
            const basic_info_id = $(`#selectStaff`).find(`:selected`).val();
            const url_get_confirmed_form_day_offs = "{!! route('FormDayOffHelper3', [':basic_info_id']) !!}";

            if (parseInt(basic_info_id) > 0) {
                $('#basic_info_id_of_form').val(basic_info_id);

                $.ajax({
                    type: 'GET',
                    url: url_get_confirmed_form_day_offs.replace(':basic_info_id', basic_info_id),
                    success: function(data) {
                        confirmed_form_day_offs = [];
                        data.forEach(
                            function(form_day_off) {
                                if (form_day_off['id'] != form_day_off_X_id)
                                    confirmed_form_day_offs.push(form_day_off);
                            }
                        )

                        Object.keys(time_frame_dict).forEach(
                            function(key) {
                                checkDayOffInfo(key);
                            }
                        );


                    }
                });
            } else {
                confirmed_form_day_offs = [];

                Object.keys(time_frame_dict).forEach(
                    function(key) {
                        checkDayOffInfo(key);
                    }
                );

            }

        }

        function checkCompanyDayOff() {
            const basic_info_id = $('#selectStaff').find(":selected").val();
            const url_get_day_off = "{!! route('dayoff.getByBasicInfoId', [':basic_info_id']) !!}";

            if (parseInt(basic_info_id) > 0) {
                $('#basic_info_id_of_form').val(basic_info_id);
                $.ajax({
                    type: 'GET',
                    url: url_get_day_off.replace(':basic_info_id', basic_info_id),
                    success: function(data) {
                        company_day_offs = [];
                        data.forEach(
                            function(day_off) {
                                company_day_offs.push(day_off);
                            }
                        )

                        Object.keys(time_frame_dict).forEach(
                            function(key) {
                                checkDayOffInfo(key);
                            }
                        );


                    }
                });
            } else {
                company_day_offs = [];
                Object.keys(time_frame_dict).forEach(
                    function(key) {
                        checkDayOffInfo(key);
                    }
                );
            }

        }

        function checkTrackingDayOff() {

            var basic_info_id = $('#selectStaff').find(":selected").val();
            var url_get_tracking_day_off = "{!! route('trackingDayoffGetByBasicInfoId', [':id', ':year']) !!}";


            if (parseInt(basic_info_id) > 0) {

                $.ajax({
                    type: 'GET',
                    url: url_get_tracking_day_off.replace(':id', basic_info_id).replace(':year', cur_year),
                    success: function(data) {
                        let data_total_prev_year_pto = -1;
                        if (data['prev_year_tracking_day_off']) {
                            $('#prevYearPTOLabel').show();
                            $('#noWorkPrevYearLabel').hide();
                            data_total_prev_year_pto = data['prev_year_tracking_day_off']['year_pto_count'];
                        } else {
                            $('#prevYearPTOLabel').hide();
                            $('#noWorkPrevYearLabel').show();
                        }

                        const data_total_cur_year_pto = data['year_pto_count'];

                        cur_prev_tracking_day_off[0] = data_total_cur_year_pto;
                        cur_prev_tracking_day_off[1] = data_total_prev_year_pto;
                        $('#current_total_prev_year_pto_span').text(data_total_prev_year_pto);
                        $('#current_total_cur_year_pto_span').text(data_total_cur_year_pto);

                    }
                });

            } else {
                $(`#TrackingDayOffLeftLabel`).hide();
                $(`#current_total_prev_year_pto_span`).text(``);
                $('#current_total_cur_year_pto_span').text(``);
            }

        }

        //Not Used
        function formTotalDayOffLabelChanger() {
            let total_company_day_off = 0;
            let total_day_off_asked_for = 0;

            $(`#CompanyDayOffLabel`).hide();
            $(`#DayOffNeedToAskForLabel`).hide();

            Object.keys(time_frame_dict).forEach(
                function(key) {
                    const company_day_off_text = $(`#company_day_off_span${key}`).text();
                    const total_day_off_asked_for_text = $(`#total_day_off_asked_for_span${key}`).text();

                    if (company_day_off_text != '') {
                        total_company_day_off += parseFloat(company_day_off_text);
                    }

                    total_day_off_asked_for += parseFloat(total_day_off_asked_for_text);
                }
            );



            if (Object.keys(time_frame_dict).length == 1)
                $(`#TotalDayOffAskedForLabel`).hide();
            else {
                $(`#TotalDayOffAskedForLabel`).show();
                if (total_company_day_off > 0) {
                    $(`#CompanyDayOffLabel`).show();
                    $(`#DayOffNeedToAskForLabel`).show();
                    $(`#company_day_off_span`).text(total_company_day_off);
                    $(`#total_day_off_need_to_ask_for_span`).text(total_day_off_asked_for - total_company_day_off > 0 ? total_day_off_asked_for - total_company_day_off : 0);
                }
            }
            $(`#total_day_off_asked_for_span`).text(total_day_off_asked_for);

        }

        function checkTimeFrame(key) {
            const dateStart = $(`#start_date${key}`).val();
            const dateEnd = $(`#end_date${key}`).val();

            const day1 = new Date(dateStart);
            const day2 = new Date(dateEnd);

            if (day1.getDay() == 0) {
                day1.setDate(day1.getDate() + 1);
                const temp_date_string = day1.toISOString().slice(0, 10);
                $(`#start_date${key}`).datepicker('clearDates');
                $(`#start_date${key}`).val(temp_date_string);
            }

            if (day2.getDay() == 0) {

                day2.setDate(day2.getDate() + 1);
                const temp_date_string = day2.toISOString().slice(0, 10);
                $(`#end_date${key}`).datepicker('clearDates');
                $(`#end_date${key}`).val(temp_date_string);
            }

            if (day1.getDay() == 6) {
                $(`#start_time${key}`).find('option').removeAttr("selected");
                $(`#start_time${key}`).val('8:00');
                $(`#start_time${key} option[value="8:00"]`).attr('selected', true)
            }

            if (day2.getDay() == 6) {
                $(`#end_time${key}`).find('option').removeAttr("selected");
                $(`#end_time${key}`).val('12:00');
                $(`#end_time${key} option[value="12:00"]`).attr('selected', true)
            }

            time_frame_dict[key] = [$(`#start_date${key}`).val(), $(`#end_date${key}`).val(), $(`#start_time${key}`).val(), $(`#end_time${key}`).val(), $(`#typeDayOffChanger${key}`).val(), $(`#total_day_off_in_timeframe${key}`).val()];

        }



        function AddTimeFrame(detailForm = null) {

            let input_string = `
            <div class="row card-body justify-content-between" id="FormOfTimeFrameBody${index_time_frame}">
            <div class="col-xl-1">
            <label for="start_date${index_time_frame}" class="form-label">Ngày bắt đầu</label>`;


            if (detailForm != null) {
                const start_date_string = (new Date(detailForm['start_date'])).toISOString().slice(0, 10);
                input_string = input_string.concat(`
                <input name="start_date${index_time_frame}" type="text" class="form-control datepicker" id="start_date${index_time_frame}" value="${start_date_string}">
                `);
            } else
                input_string = input_string.concat(`
                <input name="start_date${index_time_frame}" type="text" class="form-control datepicker" id="start_date${index_time_frame}" value="{{ now()->format('Y-m-d') }}">
                `);


            input_string = input_string.concat(`
                </div>
                <div class="col-xl-1">
                    <label for="start_time${index_time_frame}" class="form-label">Giờ bắt đầu</label>
                    <select id="start_time${index_time_frame}" name="start_time${index_time_frame}" class="form-select mb-3">
                    `);


            if (detailForm != null) {
                if (detailForm['start_time'] == '8:00')
                    input_string = input_string.concat(`          
                    <option selected value="8:00"> 8:00 </option>
                    <option value="13:00"> 13:00 </option>
                    `);
                else
                    input_string = input_string.concat(`          
                    <option value="8:00"> 8:00 </option>
                    <option selected value="13:00"> 13:00 </option>
                    `);
            } else
                input_string = input_string.concat(`          
                <option value="8:00"> 8:00 </option>
                <option value="13:00"> 13:00 </option>
                `);

            input_string = input_string.concat(`
                </select>
                </div>
                <div class="col-xl-1">
                    <label for="end_date${index_time_frame}" class="form-label">Ngày kết thúc</label>
            `);

            if (detailForm != null) {
                const end_date_string = (new Date(detailForm['end_date'])).toISOString().slice(0, 10);
                input_string = input_string.concat(`
                <input name="end_date${index_time_frame}" type="text" class="form-control datepicker @error('end_date${index_time_frame}') is-invalid @enderror" id="end_date${index_time_frame}" 
                    value="${end_date_string}">
                `);
            } else
                input_string = input_string.concat(`
                <input name="end_date${index_time_frame}" type="text" class="form-control datepicker @error('end_date${index_time_frame}') is-invalid @enderror" id="end_date${index_time_frame}" 
                    value="{{  date('Y-m-d', strtotime( now()))  }}">
                `);

            input_string = input_string.concat(`
                @error('end_date${index_time_frame}')
                <span class="text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            
            <div class=" col-xl-1">
                <label for="end_time${index_time_frame}" class="form-label">Giờ kết thúc</label>
                <select id="end_time${index_time_frame}" name="end_time${index_time_frame}" class="form-select mb-3">
            `);

            if (detailForm != null) {

                if (detailForm['end_time'] == '12:00')
                    input_string = input_string.concat(`          
                    <option selected value="12:00"> 12:00 </option>
                    <option value="17:00"> 17:00 </option>
                    `);
                else
                    input_string = input_string.concat(`          
                    <option value="12:00"> 12:00 </option>
                    <option selected value="17:00"> 17:00 </option>
                    `);

            } else
                input_string = input_string.concat(`
                    <option value="12:00"> 12:00 </option>
                    <option value="17:00"> 17:00 </option>
                `)
            input_string = input_string.concat(`
            </select>
            </div>
            <div class="col-xl-3">
                <label class="form-label">Loại Nghỉ Phép</label>
                <select class="form-select mb-3" name="type${index_time_frame}" id="typeDayOffChanger${index_time_frame}">
                </select>
            </div>
            <input hidden name ="total_day_off_in_timeframe${index_time_frame}" id="total_day_off_in_timeframe${index_time_frame}">

            `);

            if (detailForm != null)

                input_string = input_string.concat(`
                <div class="col-xl-4">
                    <label for="exampleInputdate" class="form-label">Lý do</label>
                    <textarea name="reason${index_time_frame}" class="form-control" id="reason${index_time_frame}" rows="4" placeholder="Ghi chú...">${detailForm['reason']}</textarea>
                </div>

                `);
            else
                input_string = input_string.concat(`
                <div class="col-xl-4">
                    <label for="exampleInputdate" class="form-label">Lý do</label>
                    <textarea name="reason${index_time_frame}" class="form-control" id="reason${index_time_frame}" rows="4" placeholder="Ghi chú..."></textarea>
                </div>

                `);


            if (index_time_frame > 1)
                input_string = input_string.concat(` 
                <div class="col-xl-1 align-self-center">
                    <a class="dropdown-item remove-item-btn" id = "deleteTimeFrameButton${index_time_frame}"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Xóa</a>
                </div>
                <div>
                `);
            else
                input_string = input_string.concat(`
                    <div class="col-xl-1">
                    </div>
                    <div>
                `);

            input_string = input_string.concat(`
            <div class="row justify-content-center">
                <div class="col-xl-4">
                    <h6 id="CompanyDayOffLabel${index_time_frame}" class="text-center">Tổng số ngày Công Ty Cho nghỉ: <span id="company_day_off_span${index_time_frame}"></span> ngày</h6>
                </div>
                <div class="col-xl-4">
                    <h6 class="text-center">Tổng số ngày xin: <span id="total_day_off_asked_for_span${index_time_frame}"></span> ngày</h6>
                </div>
                <div class="col-xl-4">
                    <h6 id="DayOffNeedToAskForLabel${index_time_frame}" class="text-center">Tổng số ngày cần xin: <span id="total_day_off_need_to_ask_for_span${index_time_frame}"></span> ngày</h6>
                </div>
            </div>
            `);

            input = jQuery(input_string);
            jQuery('#FormOfTimeFrames').append(input);

            if (index_time_frame == 1) {
                $(`#start_date${index_time_frame}`).datepicker({
                    locale: 'vi',
                    format: 'yyyy-mm-dd',
                    todayHighlight: true,
                    daysOfWeekDisabled: [0]
                });

                $(`#end_date${index_time_frame}`).datepicker({
                    locale: 'vi',
                    format: 'yyyy-mm-dd',
                    todayHighlight: true,
                    startDate: $(`#start_date${index_time_frame}`).val(),
                    daysOfWeekDisabled: [0]
                });

            }

            time_frame_dict[index_time_frame] = [$(`#start_date${index_time_frame}`).val(), $(`#end_date${index_time_frame}`).val(), $(`#start_time${index_time_frame}`).val(), $(`#end_time${index_time_frame}`).val(), $(`#typeDayOffChanger${index_time_frame}`).val(), $(`#total_day_off_in_timeframe${index_time_frame}`).val()];

            const time_frame_dict_keys = Object.keys(time_frame_dict);
            time_frame_dict_keys.forEach(
                function(key, temp_key_index) {

                    checkTimeFrame(key);
                    checkDayOffInfo(key);

                    $(`#deleteTimeFrameButton${key}`).on('click', function() {
                        $(`#FormOfTimeFrameHeader${key}`).remove();
                        $(`#FormOfTimeFrameBody${key}`).remove();

                        delete time_frame_dict[key];
                        if (temp_key_index > 0)
                            checkTimeFrameDownwardsWithEndDate(temp_key_index - 1);

                        Object.keys(time_frame_dict).forEach(function(keyX) {
                            checkDayOffInfo(keyX);

                        });
                        checkRemainingPTO();

                        // formTotalDayOffLabelChanger();
                    });

                    $(`#start_date${key}`).on('change', function() {
                        $(`#end_date${key}`).datepicker('destroy');
                        $(`#end_date${key}`).datepicker({
                            locale: 'vi',
                            format: 'yyyy-mm-dd',
                            todayHighlight: true,
                            daysOfWeekDisabled: [0],
                            startDate: $(this)[0].value
                        });

                        if ($(this)[0].value > $(`#end_date${key}`)[0].value) {
                            $(`#end_date${key}`).datepicker('clearDates');
                            $(`#end_date${key}`).val($(this)[0].value);
                            checkTimeFrameDownwardsWithEndDate(temp_key_index);
                        }

                        checkTimeFrame(key);

                        Object.keys(time_frame_dict).forEach(function(keyX) {
                            checkDayOffInfo(keyX);

                        });

                        checkRemainingPTO();
                        // formTotalDayOffLabelChanger()


                    });

                    $(`#end_date${key}`).on('change', function() {
                        checkTimeFrame(key);
                        checkTimeFrameDownwardsWithEndDate(temp_key_index);

                        Object.keys(time_frame_dict).forEach(function(keyX) {
                            checkDayOffInfo(keyX);
                        });
                        checkRemainingPTO();
                        // formTotalDayOffLabelChanger();
                    });

                    $(`#start_time${key}, #end_time${key}, #typeDayOffChanger${key}`).on('change', function() {
                        checkTimeFrame(key);
                        Object.keys(time_frame_dict).forEach(function(keyX) {
                            checkDayOffInfo(keyX);

                        });
                        checkRemainingPTO();
                        // formTotalDayOffLabelChanger();


                    });

                }
            );

            if (second_load) checkTimeFrameDownwardsWithEndDate(time_frame_dict_keys.length - 2 > 0 ? time_frame_dict_keys.length - 2 : 0);
            checkRemainingPTO();
            // formTotalDayOffLabelChanger();
        }

        function checkTimeFrameDownwardsWithEndDate(initial_index = 0) {
            const time_frame_dict_keys = Object.keys(time_frame_dict);
            for (let i = initial_index; i < time_frame_dict_keys.length; i += 1) {
                const current_key = time_frame_dict_keys[i];
                const next_key = time_frame_dict_keys[i + 1]

                const temp_date = new Date($(`#end_date${current_key}`).val());
                temp_date.setDate(temp_date.getDate() + 1);

                if (temp_date.getDay() == 0)
                    temp_date.setDate(temp_date.getDate() + 1);

                const temp_date_string = temp_date.toISOString().slice(0, 10);

                $(`#start_date${next_key}`).datepicker('destroy');
                $(`#end_date${next_key}`).datepicker('destroy');

                $(`#start_date${next_key}`).val(temp_date_string);
                $(`#end_date${next_key}`).val(temp_date_string);
                $(`#start_date${next_key}`).datepicker({
                    locale: 'vi',
                    format: 'yyyy-mm-dd',
                    todayHighlight: true,
                    startDate: temp_date_string,
                    daysOfWeekDisabled: [0]
                });


                $(`#end_date${next_key}`).datepicker({
                    locale: 'vi',
                    format: 'yyyy-mm-dd',
                    todayHighlight: true,
                    startDate: $(`#start_date${next_key}`).val(),
                    daysOfWeekDisabled: [0]
                });

            }


            for (let i = 0; i < time_frame_dict_keys.length; i += 1) {
                const current_key = time_frame_dict_keys[i];
                time_frame_dict[current_key] = [$(`#start_date${current_key}`).val(), $(`#end_date${current_key}`).val(), $(`#start_time${current_key}`).val(), $(`#end_time${current_key}`).val(), $(`#typeDayOffChanger${current_key}`).val(), $(`#total_day_off_in_timeframe${current_key}`).val()];
            }
        }

        //Not Used
        function checkTimeFrameUpwardsWithStartDate(initial_index = (Object.keys(time_frame_dict).length) - 1) {
            const time_frame_dict_keys = Object.keys(time_frame_dict);
            for (let i = initial_index; i > 0; i -= 1) {
                const current_key = time_frame_dict_keys[i];
                const previous_key = time_frame_dict_keys[i - 1]

                const temp_date = new Date($(`#start_date${current_key}`).val());
                temp_date.setDate(temp_date.getDate() - 1);

                if (temp_date.getDay() == 0)
                    temp_date.setDate(temp_date.getDate() - 1);

                const temp_date_string = temp_date.toISOString().slice(0, 10);

                if (i > 1) {
                    $(`#start_date${previous_key}`).val(temp_date_string);
                    $(`#start_date${previous_key}`).datepicker({
                        locale: 'vi',
                        format: 'yyyy-mm-dd',
                        todayHighlight: true,
                        startDate: temp_date_string,
                        daysOfWeekDisabled: [0]
                    });
                }
                $(`#end_date${previous_key}`).val(temp_date_string);
                $(`#end_date${previous_key}`).datepicker({
                    locale: 'vi',
                    format: 'yyyy-mm-dd',
                    todayHighlight: true,
                    startDate: $(`#start_date${previous_key}`).val(),
                    daysOfWeekDisabled: [0]
                });

            }



            for (let i = 0; i < time_frame_dict_keys.length; i += 1) {
                const current_key = time_frame_dict_keys[i];
                time_frame_dict[current_key] = [$(`#start_date${current_key}`).val(), $(`#end_date${current_key}`).val(), $(`#start_time${current_key}`).val(), $(`#end_time${current_key}`).val(), $(`#typeDayOffChanger${current_key}`).val(), $(`#total_day_off_in_timeframe${current_key}`).val()];
            }
        }

        function checkDirectBossAndReplacement() {
            var basic_info_id = $('#selectStaff').find(":selected").val();
            var url_get_replacement = "{!! route('FormDayOffHelper1', [':basic_info_id']) !!}";
            var url_get_direct_boss = "{!! route('FormDayOffHelper2', [':basic_info_id']) !!}";

            $.ajax({
                type: 'GET',
                url: url_get_replacement.replace(':basic_info_id', basic_info_id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectReplacement')
                            .find('option')
                            .remove()
                        $('#selectReplacement').append($('<option>', {
                            value: 0,
                            text: `Không Có`,

                        }));

                        data.forEach(function(replacement_basic_info) {
                            $('#selectReplacement').append($('<option>', {
                                value: replacement_basic_info['id'],
                                text: `${replacement_basic_info['full_name']}`,

                            }));
                        })
                    } else {
                        $('#selectReplacement')
                            .find('option')
                            .remove();

                        $('#selectReplacement').append($('<option>', {
                            value: 0,
                            text: `Không Có`,

                        }));


                    }
                }
            });

            $.ajax({
                type: 'GET',
                url: url_get_direct_boss.replace(':basic_info_id', basic_info_id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectDirectBoss')
                            .find('option')
                            .remove()

                        data.forEach(function(direct_boss_basic_info) {
                            $('#selectDirectBoss').append($('<option>', {
                                value: direct_boss_basic_info['id'],
                                text: `${direct_boss_basic_info['full_name']}`,

                            }));
                        })
                    } else {
                        $('#selectDirectBoss')
                            .find('option')
                            .remove();

                    }

                }
            });

        }

        function resetRooms() {
            const rooms = <?php echo json_encode($rooms); ?>;
            $('#selectRoom')
                .find('option')
                .remove();
            $('#selectRoom').append($('<option>', {
                text: 'Vui lòng chọn phòng ban'
            }));

            rooms.forEach(function(room) {

                $('#selectRoom').append($('<option>', {
                    value: room.id,
                    text: room.name
                }));

            });

        }

        function getCountDayInTimeFrame(dateStart, dateEnd, timeStart, timeEnd) {


            const day1 = new Date(dateStart);
            const day2 = new Date(dateEnd);
            const time1 = Number.parseInt(timeStart);
            const time2 = Number.parseInt(timeEnd);

            let countDay = 0
            let sundayCount = 0;
            let saturdayCount = 0;

            if (day1 <= day2) {
                var diffDays = Math.ceil(Math.abs(day2 - day1) / (1000 * 60 * 60 * 24));
                for (i = day1; i <= day2; i.setDate(i.getDate() + 1)) {
                    if (i.getDay() == 0) {
                        sundayCount += 1;
                    }

                    if (i.getDay() == 6 && i < day2) {
                        saturdayCount += 1;
                    }
                }

                if (time1 == 13) {
                    diffDays -= 0.5;
                }

                if (time2 == 12) {
                    diffDays += 0.5;
                } else {
                    diffDays += 1;
                }


                countDay = diffDays;
                countDay -= sundayCount;
                if (saturdayCount > 0)
                    countDay -= saturdayCount * 0.5;


            }

            return countDay;
        }

        function checkRemainingPTO() {
            const dayNow = new Date();
            var basic_info_id = $('#selectStaff').find(":selected").val();
            var url_get_tracking_day_off = "{!! route('trackingDayoffGetByBasicInfoId', [':id', ':year']) !!}";
            $(`#TrackingDayOffLeftLabel`).hide();


            if (parseInt(basic_info_id) > 0 && cur_prev_tracking_day_off[0] >= 0) {
                const data_total_cur_year_pto = cur_prev_tracking_day_off[0];
                const data_total_prev_year_pto = cur_prev_tracking_day_off[1];
                const data_total_pto = (dayNow.getMonth() + 1 < 4) ? data_total_cur_year_pto + data_total_prev_year_pto : data_total_cur_year_pto;


                let form_pto = 0;

                Object.values(time_frame_dict).forEach(
                    function(value) {
                        if (parseInt(value[4]) == 1)
                            form_pto += parseFloat(value[5]);
                    }
                );

                $(`#tracking_day_off_left_span`).text(data_total_pto - form_pto > 0 ? data_total_pto - form_pto : 0);
                $(`#TrackingDayOffLeftLabel`).show();
            } else {
                if (parseInt(basic_info_id) > 0) {
                    $.ajax({
                        type: 'GET',
                        url: url_get_tracking_day_off.replace(':id', basic_info_id).replace(':year', cur_year),
                        success: function(data) {
                            let data_total_prev_year_pto = -1;
                            if (data['prev_year_tracking_day_off'])
                                data_total_prev_year_pto = data['prev_year_tracking_day_off']['year_pto_count'];

                            const data_total_cur_year_pto = data['year_pto_count'];

                            cur_prev_tracking_day_off[0] = data_total_cur_year_pto;
                            cur_prev_tracking_day_off[1] = data_total_prev_year_pto;

                            const data_total_pto = (dayNow.getMonth() + 1 < 4) ? data_total_cur_year_pto + data_total_prev_year_pto : data_total_cur_year_pto;

                            let form_pto = 0;

                            Object.values(time_frame_dict).forEach(
                                function(value) {
                                    if (parseInt(value[4]) == 1)
                                        form_pto += parseFloat(value[5]);
                                }
                            );

                            $(`#tracking_day_off_left_span`).text(data_total_pto - form_pto > 0 ? data_total_pto - form_pto : 0);
                            $(`#TrackingDayOffLeftLabel`).show();

                        }
                    });

                }
            }
        }


        function checkForPossiblePTO(total_day_asked_for, key) {
            dayNow = new Date();
            var basic_info_id = $('#selectStaff').find(":selected").val();

            if (basic_info_id > 0) {
                const data_total_cur_year_pto = parseFloat($('#current_total_cur_year_pto_span').text());
                const data_total_prev_year_pto = parseFloat($('#current_total_prev_year_pto_span').text());
                let total_pto = (dayNow.getMonth() + 1 < 4) ? data_total_cur_year_pto + data_total_prev_year_pto : data_total_cur_year_pto

                Object.keys(time_frame_dict).forEach(
                    function(keyX) {
                        if (keyX != key && parseInt(time_frame_dict[keyX][4]) == 1)
                            total_pto -= parseFloat(time_frame_dict[keyX][5]);

                    }
                )

                return total_day_asked_for <= total_pto
            } else
                return false;
        }

        function formDayOffKeyLabelChanger(key, countDay, dayOffDifference = 0) {

            if (dayOffDifference == 0) {
                $(`#CompanyDayOffLabel${key}`).hide();
                $(`#DayOffNeedToAskForLabel${key}`).hide();
            }


            if (countDay == 0 && $(`#start_date${key}`).val() == $(`#end_date${key}`).val() && dayOffDifference == 0) {
                $(`#start_time${key}`).find('option').removeAttr("selected");
                $(`#start_time${key}`).val('8:00');
                $(`#start_time${key} option[value="8:00"]`).attr('selected', true)
                countDay += 0.5;
            }


            if (dayOffDifference > 0) {
                $(`#company_day_off_span${key}`).text(dayOffDifference);
                $(`#total_day_off_need_to_ask_for_span${key}`).text(countDay > dayOffDifference ? countDay - dayOffDifference : 0)
                $(`#CompanyDayOffLabel${key}`).show();
                $(`#DayOffNeedToAskForLabel${key}`).show();
            }


            $(`#total_day_off_asked_for_span${key}`).text(countDay);
            $(`#total_day_off_in_timeframe${key}`).val(countDay > dayOffDifference ? countDay - dayOffDifference : 0);

            time_frame_dict[key] = [$(`#start_date${key}`).val(), $(`#end_date${key}`).val(), $(`#start_time${key}`).val(), $(`#end_time${key}`).val(), $(`#typeDayOffChanger${key}`).val(), $(`#total_day_off_in_timeframe${key}`).val()];



            let initial_type = null;
            if (!second_load)
                initial_type = form_day_off_details[key - 1]['type'];
            else
                initial_type = (typeof time_frame_dict[key] != undefined && typeof time_frame_dict[key][4] != undefined) ? time_frame_dict[key][4] : null;


            $(`#typeDayOffChanger${key}`).find('option').remove();
            if (countDay - dayOffDifference <= 0) {
                $(`#typeDayOffChanger${key}`).append(
                    $('<option>', {
                        value: 0,
                        text: 'Không Cần Tạo Đơn'
                    })
                );
            } else {
                const options = {

                    2: {
                        value: 2,
                        text: 'Nghỉ Không Lương',
                    },

                    3: {
                        value: 3,
                        text: 'Nghỉ Ốm Hưởng BHXH',
                    },

                    4: {
                        value: 4,
                        text: 'Thai Sản',
                    },

                    5: {
                        value: 5,
                        text: 'Nghỉ Hưởng Nguyên Lương Theo Quy Định',
                    },

                }

                if (checkForPossiblePTO(countDay - dayOffDifference, key))
                    options[1] = {
                        value: 1,
                        text: `Phép Năm - Dùng ${countDay - dayOffDifference} Ngày`,
                    }

                if (initial_type != '' && initial_type != null) {
                    $(`#typeDayOffChanger${key}`).append($('<option>', options[initial_type]));
                    delete options[initial_type];
                }

                for (let i = 1; i < 6; i++) {
                    if (options.hasOwnProperty(i))
                        $(`#typeDayOffChanger${key}`).append($('<option>', options[i]));
                }

            }

            time_frame_dict[key] = [$(`#start_date${key}`).val(), $(`#end_date${key}`).val(), $(`#start_time${key}`).val(), $(`#end_time${key}`).val(), $(`#typeDayOffChanger${key}`).val(), $(`#total_day_off_in_timeframe${key}`).val()];

        }

        function checkDayOffInfo(key) {
            $(`#ExistConfirmedFormLabel${key}`).hide();


            const dateStart = $(`#start_date${key}`).val();
            const dateEnd = $(`#end_date${key}`).val();
            const timeStart = $(`#start_time${key}`).val();
            const timeEnd = $(`#end_time${key}`).val();


            let day1 = new Date(dateStart);
            let day2 = new Date(dateEnd);

            day1.setHours(0, 0, 0, 0);
            day2.setHours(0, 0, 0, 0);


            const day1Constant = day1.getDate() + (day1.getMonth() + 1) * 100 + day1.getFullYear() * 10000;
            const day2Constant = day2.getDate() + (day2.getMonth() + 1) * 100 + day2.getFullYear() * 10000;

            const dayOffDetails = {};
            let dayOffDifference = 0;
            let i = day1;

            time_frame_dict[key] = [$(`#start_date${key}`).val(), $(`#end_date${key}`).val(), $(`#start_time${key}`).val(), $(`#end_time${key}`).val(), $(`#typeDayOffChanger${key}`).val(), $(`#total_day_off_in_timeframe${key}`).val()];
            const currentCountDay = getCountDayInTimeFrame(dateStart, dateEnd, timeStart, timeEnd);

            if (confirmed_form_day_offs.length > 0) {
                console.log(confirmed_form_day_offs);
                let existForm = false;

                if (existForm) {
                    $(`#NoExistConfirmedFormLabel${key}`).hide();
                    $(`#ExistConfirmedFormLabel${key}`).show();
                    return;
                } else {
                    $(`#NoExistConfirmedFormLabel${key}`).show();
                    $(`#ExistConfirmedFormLabel${key}`).hide();
                }

            }

            if (company_day_offs.length > 0) {

                company_day_offs.forEach(
                    function(day_off) {
                        const detail_day_off = day_off['detail_day_off'];
                        const day_off_start = new Date(detail_day_off['start_date']);
                        const day_off_end = new Date(detail_day_off['end_date']);

                        day_off_start.setHours(0, 0, 0, 0);
                        day_off_end.setHours(0, 0, 0, 0);

                        const day_off_start_constant = day_off_start.getDate() + (day_off_start.getMonth() + 1) * 100 + day_off_start.getFullYear() * 10000;
                        const day_off_end_constant = day_off_end.getDate() + (day_off_end.getMonth() + 1) * 100 + day_off_end.getFullYear() * 10000;


                        if ((day1Constant >= day_off_start_constant && day1Constant <= day_off_end_constant) || (day_off_start_constant >= day1Constant && day_off_start_constant <= day2Constant))
                            if (!dayOffDetails.hasOwnProperty(day_off_start_constant) || dayOffDetails[day_off_start_constant] < detail_day_off['total_day_off'])
                                dayOffDetails[day_off_start_constant] = detail_day_off['total_day_off'];


                    }

                );
                while (i <= day2) {
                    const iConstant = (i.getDate() + (i.getMonth() + 1) * 100 + i.getFullYear() * 10000).toString();
                    if (dayOffDetails.hasOwnProperty(iConstant)) {

                        dayOffDifference += dayOffDetails[iConstant]
                        if (dayOffDetails[iConstant] % 0.5 == 0)
                            i.setDate(i.getDate() + (dayOffDetails[iConstant] - 0.5));
                        else
                            i.setDate(i.getDate() + dayOffDetails[iConstant]);
                    }
                    i.setDate(i.getDate() + 1);
                }
            } else {
                $(`#CompanyDayOffLabel${key}`).hide();
                $(`#DayOffNeedToAskForLabel${key}`).hide();

            }

            formDayOffKeyLabelChanger(key, currentCountDay, dayOffDifference);
        }
    });
</script>
@endsection