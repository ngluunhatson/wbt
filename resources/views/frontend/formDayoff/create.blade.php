@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')

@php
$tracking_day_off_of_user = $dataInit['tracking_day_off_of_user'];
$confirmed_form_day_offs = $dataInit['confirmed_form_day_offs'];
$basic_info_of_user = $tracking_day_off_of_user -> staff;
$user_day_off_array = $dataInit['user_day_off_array'];
$basic_info_of_replacement_array = $dataInit['basic_info_of_replacement_array'];
$basic_info_of_direct_boss_array = $dataInit['basic_info_of_direct_boss_array'];

@endphp
<div class="row">
  <div class="col-12 d-flex justify-content-between mb-3">
    <h3 class="d-inline m-0">Tạo Đơn Xin Nghỉ Phép</h3>

    <a href="#" noref>
      <button type="button" id="saveFormDayoff" class="btn btn-success btn-label waves-effect waves-light">
        <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Lưu</button>
    </a>
  </div>
</div>
<form id="formInsert" method="POST" action="{{ route('FormDayOffStoreDataInsert') }}">
  @csrf
  <div class="card">
    <div class="card-body">
      <div class="row mt-3">
        <div class="col-xl-3">
          <label for="borderInput" class="form-label">Họ và tên</label>
          <input type="text" class="form-control" value="{{ ($basic_info_of_user) ? $basic_info_of_user -> full_name : N/A }}" disabled readonly>
        </div>
        <div class="col-xl-1">
          <label for="borderInput" class="form-label">Mã nhân viên</label>
          <input type="text" class="form-control" value="{{ ($basic_info_of_user) ? $basic_info_of_user -> employee_code : N/A }}" disabled readonly>
        </div>
        <div class="col-xl-2">
          <label for="borderInput" class="form-label">Phòng</label>
          <input type="text" class="form-control" value="{{ ($basic_info_of_user) ? $basic_info_of_user -> organizational -> room -> name : N/A }}" disabled readonly>
        </div>
        <div class="col-xl-3">
          <label for="borderInput" class="form-label">Bộ phận</label>
          <input type="text" class="form-control" value="{{ ($basic_info_of_user) ? $basic_info_of_user -> organizational -> team -> name : N/A }}" disabled readonly>
        </div>
        <div class="col-xl-3">
          <label for="borderInput" class="form-label">Chức danh</label>
          <input type="text" class="form-control" value="{{ ($basic_info_of_user) ? $basic_info_of_user -> organizational -> position -> name : N/A }}" disabled readonly>
        </div>
      </div>


      <div class="row mt-4">
        <div class="col-xl-6">
          @if ($tracking_day_off_of_user -> prevYearTrackingDayOff )
          @if (intval(date ('m', strtotime(now()))) < 4 ) <h4 class="text-center">Số phép còn của năm trước: <span>{{ $tracking_day_off_of_user -> prevYearTrackingDayOff -> year_pto_count }}</span> ngày </h4>
            @else
            <h4 class="text-center">Phép Năm {{ (date ('Y', strtotime('-1 year'. now()))) }} đã hết hạn sử dụng</h4>
            @endif
            @else
            <h4 class="text-center"> Không Đi Làm vào Năm {{ ($tracking_day_off_of_user -> year) -1 }} </h4>
            @endif
        </div>
        <div class="col-xl-6">
          <h4 class="text-center">Phép Năm {{ $tracking_day_off_of_user -> year }} còn được dùng: <span>{{ $tracking_day_off_of_user -> year_pto_count }}</span> ngày</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="card" id="FormOfTimeFrames">
    <div class="card-header">
      <h2>Thời Gian Xin Nghỉ</h2>
    </div>

  </div>

  <div class='row justify-content-between'>
    <div class="col-2">

    </div>

    <div class="col-1 mx-auto">
      <button id="AddTimeFrameButton" type="button" class="btn btn-success btn-label waves-effect waves-light">
        <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm</button>
    </div>

    <div class="col-2">
      <button id="RestoreTimeFrameButton" type="button" class="btn btn-danger btn-label waves-effect waves-light"> <i class="ri-arrow-go-back-line label-icon align-middle fs-16 me-2"></i> Hoàn Tác</button>

    </div>

  </div>

  <input hidden name='basic_info_id_of_form' id='basic_info_id_of_form' value="{{ $basic_info_of_user -> id }}">
  <input hidden name='index_time_frame' id='index_time_frame'>
  <input hidden name='request_type' value='create'>


  <div class="row">
    <div class="col-xl-4">
      <h3 id="CompanyDayOffLabel" class="text-center">Tổng số ngày Công Ty Cho nghỉ: <span id="company_day_off_span"></span> ngày</h3>
    </div>
    <div class="col-xl-4">

      <h3 id='TotalDayOffAskedForLabel' class="text-center">Tổng số ngày xin nghỉ: <span id="total_day_off_asked_for_span"></span> ngày</h3>
    </div>
    <div class="col-xl-4">
      <h3 id="DayOffNeedToAskForLabel" class="text-center">Tổng số ngày cần xin: <span id="total_day_off_need_to_ask_for_span"></span> ngày</h3>
    </div>
  </div>

  <div class="row d-flex justify-content-center pt-4">
    <div class="col-3 mx-auto">
      <h3 id="TrackingDayOffLeftLabel" class="text-danger">Số ngày phép năm còn lại: <span id="tracking_day_off_left_span"></span> ngày</h3>
    </div>
  </div>


  <div class="row">
    <div class="card">
      <div class="row card-body justify-content-around">
        <div class="col-xl-2">
          <div class="row ">
            <div class="col-12 text-center">
              <h4 class="form-label text-center">Người Tạo Đơn</h4>
            </div>
            <div class="col-12 text-center">
              <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ auth()->user()->name }}</label>
            </div>
            <div class="col-12 d-flex justify-content-center mt-4">
              <div class=" form-check" style="width: fit-content;">
                <input class="form-check-input fs-15" type="checkbox" id="assign" value="1" name="assign">
              </div>
              <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác nhận</label>
            </div>

          </div>
        </div>
        <div class="col-xl-2">
          <input hidden name="type_confirm_1" value="7">
          <h4 class="form-label text-center">Người Thay Thế</h4>
          <select name="replacement_basic_info_id" class="form-select" aria-label="Default select example">
            <option value="0"> Không Có</option>
            @foreach($basic_info_of_replacement_array as $basic_info)
            <option value="{{ $basic_info -> id }}"> {{ $basic_info -> full_name }}</option>
            @endforeach
          </select>

        </div>
        <div class="col-xl-2">
          <input hidden name="type_confirm_2" value="8">
          <h4 class="form-label text-center">Người Quản Lý Trực Tiếp</h4>
          <select name="direct_boss_basic_info_id" class="form-select" aria-label="Default select example">
            @foreach($basic_info_of_direct_boss_array as $basic_info)
            <option value="{{ $basic_info -> id }}"> {{ $basic_info -> full_name }} </option>
            @endforeach
          </select>
        </div>
        <div class="col-xl-2">
          <input hidden name="type_confirm_3" value="2">
          <h4 class="form-label text-center">P. QT HC-NS</h4>
          <select name="hr_basic_info_id" class="form-select" aria-label="Default select example">
            @if(!empty($dataInit['hr']))
            @foreach($dataInit['hr'] as $value)
            @if(!empty($value['basic_info_id']))
            <option value="{{ $value->staff->id }}">{{ $value->staff->full_name }}
            </option>
            @endif
            @endforeach
            @endif
          </select>
        </div>
      </div>
    </div>
</form>
@endsection
@section('script')
<script src="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/form-pickers.init.js') }}"></script>
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>

<script>
  $(document).ready(function() {

    let index_time_frame = 1;
    let time_frame_dict = {};
    const company_day_offs = <?php echo json_encode($user_day_off_array); ?>;
    const confirmed_form_day_offs = <?php echo json_encode($confirmed_form_day_offs); ?>;
    const data_total_cur_year_pto = <?php echo json_encode($tracking_day_off_of_user->year_pto_count); ?>;
    const data_total_prev_year_pto = <?php echo json_encode($tracking_day_off_of_user->prevYearTrackingDayOff ? $tracking_day_off_of_user->prevYearTrackingDayOff->year_pto_count : 0); ?>;


    $(function() {

      AddTimeFrame();
    });

    $(`#RestoreTimeFrameButton`).on('click', function() {
      index_time_frame = 1;

      Object.keys(time_frame_dict).forEach(
        function(key) {
          $(`#FormOfTimeFrameHeader${key}`).remove();
          $(`#FormOfTimeFrameBody${key}`).remove();
          delete time_frame_dict[key];
        }
      )

      AddTimeFrame();
      checkRemainingPTO();
    });


    $('#AddTimeFrameButton').on('click', function() {
      index_time_frame++;
      AddTimeFrame();
    });


    $('#saveFormDayoff').on('click', function(e) {
      e.preventDefault();
      $(`#index_time_frame`).val(index_time_frame);
      var url = "{!! route('FormDayOffCheckForm') !!}";
      $.ajax({
        method: 'POST',
        url: url,
        data: $('#formInsert').serialize(),
        success: function(response) {
          if (response.success)
            $('#formInsert').submit();
          else {
            swal.fire({
              icon: 'error',
              title: `${response.message}`,
              confirmButtonColor: '#3085d6',
              showConfirmButton: false,
            });
          }

        }
      });

    });

    function checkForPossiblePTO(total_day_asked_for, key) {
      dayNow = new Date();
      let total_pto = (dayNow.getMonth() + 1 < 4) ? data_total_cur_year_pto + data_total_prev_year_pto : data_total_cur_year_pto

      Object.keys(time_frame_dict).forEach(
        function(keyX) {
          if (keyX != key && parseInt(time_frame_dict[keyX][4]) == 1)
            total_pto -= parseFloat(time_frame_dict[keyX][5]);

        }
      );
      return total_day_asked_for <= total_pto

    }

    function checkRemainingPTO() {
      const dayNow = new Date();
      const data_total_pto = (dayNow.getMonth() + 1 < 4) ? data_total_cur_year_pto + data_total_prev_year_pto : data_total_cur_year_pto;
      let form_pto = 0;

      Object.values(time_frame_dict).forEach(
        function(value) {
          if (parseInt(value[4]) == 1)
            form_pto += parseFloat(value[5]);
        }
      );

      $(`#tracking_day_off_left_span`).text(data_total_pto - form_pto > 0 ? data_total_pto - form_pto : 0);
      $(`#TrackingDayOffLeftLabel`).show();
    }

    function getCountDayInTimeFrame(dateStart, dateEnd, timeStart, timeEnd) {


      const day1 = new Date(dateStart);
      const day2 = new Date(dateEnd);
      const time1 = Number.parseInt(timeStart);
      const time2 = Number.parseInt(timeEnd);

      let countDay = 0
      let sundayCount = 0;
      let saturdayCount = 0;

      if (day1 <= day2) {
        var diffDays = Math.ceil(Math.abs(day2 - day1) / (1000 * 60 * 60 * 24));
        for (i = day1; i <= day2; i.setDate(i.getDate() + 1)) {
          if (i.getDay() == 0) {
            sundayCount += 1;
          }

          if (i.getDay() == 6 && i < day2) {
            saturdayCount += 1;
          }
        }

        if (time1 == 13) {
          diffDays -= 0.5;
        }

        if (time2 == 12) {
          diffDays += 0.5;
        } else {
          diffDays += 1;
        }


        countDay = diffDays;
        countDay -= sundayCount;
        if (saturdayCount > 0)
          countDay -= saturdayCount * 0.5;


      }

      return countDay;
    }

    function checkDayOffInfo(key) {
      $(`#ExistConfirmedFormLabel${key}`).hide();

      const dateStart = $(`#start_date${key}`).val();
      const dateEnd = $(`#end_date${key}`).val();
      const timeStart = $(`#start_time${key}`).val();
      const timeEnd = $(`#end_time${key}`).val();


      let day1 = new Date(dateStart);
      let day2 = new Date(dateEnd);

      day1.setHours(0, 0, 0, 0);
      day2.setHours(0, 0, 0, 0);


      const day1Constant = day1.getDate() + (day1.getMonth() + 1) * 100 + day1.getFullYear() * 10000;
      const day2Constant = day2.getDate() + (day2.getMonth() + 1) * 100 + day2.getFullYear() * 10000;

      const dayOffDetails = {};
      let dayOffDifference = 0;
      let i = day1;

      time_frame_dict[key] = [$(`#start_date${key}`).val(), $(`#end_date${key}`).val(), $(`#start_time${key}`).val(), $(`#end_time${key}`).val(), $(`#typeDayOffChanger${key}`).val(), $(`#total_day_off_in_timeframe${key}`).val()];
      const currentCountDay = getCountDayInTimeFrame(dateStart, dateEnd, timeStart, timeEnd);

      if (confirmed_form_day_offs.length > 0) {
        console.log(confirmed_form_day_offs);

        let existForm = false;

        if (existForm) {
          $(`#NoExistConfirmedFormLabel${key}`).hide();
          $(`#ExistConfirmedFormLabel${key}`).show();
          return;
        } else {
          $(`#NoExistConfirmedFormLabel${key}`).show();
          $(`#ExistConfirmedFormLabel${key}`).hide();
        }

      }

      if (company_day_offs.length > 0) {

        company_day_offs.forEach(
          function(day_off) {
            const detail_day_off = day_off['detail_day_off'];
            const day_off_start = new Date(detail_day_off['start_date']);
            const day_off_end = new Date(detail_day_off['end_date']);

            day_off_start.setHours(0, 0, 0, 0);
            day_off_end.setHours(0, 0, 0, 0);

            const day_off_start_constant = day_off_start.getDate() + (day_off_start.getMonth() + 1) * 100 + day_off_start.getFullYear() * 10000;
            const day_off_end_constant = day_off_end.getDate() + (day_off_end.getMonth() + 1) * 100 + day_off_end.getFullYear() * 10000;


            if ((day1Constant >= day_off_start_constant && day1Constant <= day_off_end_constant) || (day_off_start_constant >= day1Constant && day_off_start_constant <= day2Constant))
              if (!dayOffDetails.hasOwnProperty(day_off_start_constant) || dayOffDetails[day_off_start_constant] < detail_day_off['total_day_off'])
                dayOffDetails[day_off_start_constant] = detail_day_off['total_day_off'];


          }

        );
        while (i <= day2) {
          const iConstant = (i.getDate() + (i.getMonth() + 1) * 100 + i.getFullYear() * 10000).toString();
          if (dayOffDetails.hasOwnProperty(iConstant)) {

            dayOffDifference += dayOffDetails[iConstant]
            if (dayOffDetails[iConstant] % 0.5 == 0)
              i.setDate(i.getDate() + (dayOffDetails[iConstant] - 0.5));
            else
              i.setDate(i.getDate() + dayOffDetails[iConstant]);
          }
          i.setDate(i.getDate() + 1);
        }
      } else {
        $(`#CompanyDayOffLabel${key}`).hide();
        $(`#DayOffNeedToAskForLabel${key}`).hide();

      }

      formDayOffKeyLabelChanger(key, currentCountDay, dayOffDifference);
    }

    function formDayOffKeyLabelChanger(key, countDay, dayOffDifference = 0) {

      if (dayOffDifference == 0) {
        $(`#CompanyDayOffLabel${key}`).hide();
        $(`#DayOffNeedToAskForLabel${key}`).hide();
      }


      if (countDay == 0 && $(`#start_date${key}`).val() == $(`#end_date${key}`).val() && dayOffDifference == 0) {
        $(`#start_time${key}`).find('option').removeAttr("selected");
        $(`#start_time${key}`).val('8:00');
        $(`#start_time${key} option[value="8:00"]`).attr('selected', true)
        countDay += 0.5;
      }


      if (dayOffDifference > 0) {
        $(`#company_day_off_span${key}`).text(dayOffDifference);
        $(`#total_day_off_need_to_ask_for_span${key}`).text(countDay > dayOffDifference ? countDay - dayOffDifference : 0)
        $(`#CompanyDayOffLabel${key}`).show();
        $(`#DayOffNeedToAskForLabel${key}`).show();
      }


      $(`#total_day_off_asked_for_span${key}`).text(countDay);
      $(`#total_day_off_in_timeframe${key}`).val(countDay > dayOffDifference ? countDay - dayOffDifference : 0);

      time_frame_dict[key] = [$(`#start_date${key}`).val(), $(`#end_date${key}`).val(), $(`#start_time${key}`).val(), $(`#end_time${key}`).val(), $(`#typeDayOffChanger${key}`).val(), $(`#total_day_off_in_timeframe${key}`).val()];



      const initial_type = (typeof time_frame_dict[key] != undefined && typeof time_frame_dict[key][4] != undefined) ? time_frame_dict[key][4] : null;


      $(`#typeDayOffChanger${key}`).find('option').remove();
      if (countDay - dayOffDifference <= 0) {
        $(`#typeDayOffChanger${key}`).append(
          $('<option>', {
            value: 0,
            text: 'Không Cần Tạo Đơn'
          })
        );
      } else {
        const options = {

          2: {
            value: 2,
            text: 'Nghỉ Không Lương',
          },

          3: {
            value: 3,
            text: 'Nghỉ Ốm Hưởng BHXH',
          },

          4: {
            value: 4,
            text: 'Thai Sản',
          },

          5: {
            value: 5,
            text: 'Nghỉ Hưởng Nguyên Lương Theo Quy Định',
          },

        }

        if (checkForPossiblePTO(countDay - dayOffDifference, key))
          options[1] = {
            value: 1,
            text: `Phép Năm - Dùng ${countDay - dayOffDifference} Ngày`,
          }

        if (initial_type != '' && initial_type != null) {
          $(`#typeDayOffChanger${key}`).append($('<option>', options[initial_type]));
          delete options[initial_type];
        }

        for (let i = 1; i < 6; i++) {
          if (options.hasOwnProperty(i))
            $(`#typeDayOffChanger${key}`).append($('<option>', options[i]));
        }

      }

      time_frame_dict[key] = [$(`#start_date${key}`).val(), $(`#end_date${key}`).val(), $(`#start_time${key}`).val(), $(`#end_time${key}`).val(), $(`#typeDayOffChanger${key}`).val(), $(`#total_day_off_in_timeframe${key}`).val()];

    }

    function checkTimeFrame(key) {
      const dateStart = $(`#start_date${key}`).val();
      const dateEnd = $(`#end_date${key}`).val();

      const day1 = new Date(dateStart);
      const day2 = new Date(dateEnd);

      if (day1.getDay() == 0) {
        day1.setDate(day1.getDate() + 1);
        const temp_date_string = day1.toISOString().slice(0, 10);
        $(`#start_date${key}`).datepicker('clearDates');
        $(`#start_date${key}`).val(temp_date_string);
      }

      if (day2.getDay() == 0) {

        day2.setDate(day2.getDate() + 1);
        const temp_date_string = day2.toISOString().slice(0, 10);
        $(`#end_date${key}`).datepicker('clearDates');
        $(`#end_date${key}`).val(temp_date_string);
      }

      if (day1.getDay() == 6) {
        $(`#start_time${key}`).find('option').removeAttr("selected");
        $(`#start_time${key}`).val('8:00');
        $(`#start_time${key} option[value="8:00"]`).attr('selected', true)
      }

      if (day2.getDay() == 6) {
        $(`#end_time${key}`).find('option').removeAttr("selected");
        $(`#end_time${key}`).val('12:00');
        $(`#end_time${key} option[value="12:00"]`).attr('selected', true)
      }

      time_frame_dict[key] = [$(`#start_date${key}`).val(), $(`#end_date${key}`).val(), $(`#start_time${key}`).val(), $(`#end_time${key}`).val(), $(`#typeDayOffChanger${key}`).val(), $(`#total_day_off_in_timeframe${key}`).val()];

    }



    function AddTimeFrame() {
      $(`#TotalDayOffAskedForLabel`).hide();
      $(`#CompanyDayOffLabel`).hide();
      $(`#DayOffNeedToAskForLabel`).hide();


      input_string = `
              <div class="row card-body justify-content-between" id="FormOfTimeFrameBody${index_time_frame}">
                    <div class="col-xl-1">
                        <label for="start_date${index_time_frame}" class="form-label">Ngày bắt đầu</label>
                        <input name="start_date${index_time_frame}" type="text" class="form-control datepicker @error('start_date${$index_time_frame}') is-invalid @enderror" id="start_date${index_time_frame}" 
                            value="{{  old('start_date${index_time_frame}', date('Y-m-d', strtotime( now()))) }}">
                    </div>

                    <div class="col-xl-1">
                        <label for="start_time${index_time_frame}" class="form-label">Giờ bắt đầu</label>
                        <select id="start_time${index_time_frame}" name="start_time${index_time_frame}" class="form-select mb-3 @error('start_time${index_time_frame}') is-invalid @enderror">
                            <option @if(old('start_time${index_time_frame}')=='8:00' ) selected @endif value="8:00"> 8:00 </option>
                            <option @if(old('start_time${index_time_frame}')=='13:00' ) selected @endif value="13:00"> 13:00 </option>
                        </select>
                    </div>
                    <div class="col-xl-1">
                        <label for="end_date${index_time_frame}" class="form-label">Ngày kết thúc</label>
                        <input name="end_date${index_time_frame}" type="text" class="form-control datepicker @error('end_date${index_time_frame}') is-invalid @enderror" id="end_date${index_time_frame}" 
                            value="{{  old('end_date${index_time_frame}', date('Y-m-d', strtotime( now())))  }}">
                        @error('end_date${index_time_frame}')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    
                    <div class=" col-xl-1">
                        <label for="end_time${index_time_frame}" class="form-label">Giờ kết thúc</label>
                        <select id="end_time${index_time_frame}" name="end_time${index_time_frame}" class="form-select mb-3 @error('end_time${index_time_frame}') is-invalid @enderror">
                            <option @if(old('end_time${index_time_frame}')=='12:00' ) selected @endif value="12:00"> 12:00 </option>
                            <option @if(old('end_time${index_time_frame}')=='17:00' ) selected @endif value="17:00"> 17:00 </option>
                        </select>
                    </div>
                    <div class="col-xl-3">
                        <label class="form-label">Loại Nghỉ Phép</label>
                        <select class="form-select mb-3 @error('type${index_time_frame}') is-invalid @enderror" name="type${index_time_frame}" id="typeDayOffChanger${index_time_frame}">
                        </select>
                    </div>
                    <input hidden name ="total_day_off_in_timeframe${index_time_frame}" id="total_day_off_in_timeframe${index_time_frame}">
                    <div class="col-xl-4">
                      <label for="exampleInputdate" class="form-label">Lý do</label>
                      <textarea name="reason${index_time_frame}" class="form-control @error('reason${index_time_frame}') is-invalid @enderror" id="reason${index_time_frame}" rows="4" placeholder="Ghi chú..."
                        value="{{ old('reason${index_time_frame}') }}"
                      ></textarea>
                    </div>
                  
            `;

      if (index_time_frame > 1)
        input_string = input_string.concat(` 
                    <div class="col-xl-1 align-self-center">
                        <a class="dropdown-item remove-item-btn" id = "deleteTimeFrameButton${index_time_frame}"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Xóa</a>
                    </div>
                <div>`);
      else
        input_string = input_string.concat(`
                    <div class="col-xl-1">
                    </div>
                <div>
                
  
                `);

      input_string = input_string.concat(`
                <div class="row justify-content-center">
                    <div class="col-xl-4">
                        <h6 id="CompanyDayOffLabel${index_time_frame}" class="text-center">Tổng số ngày Công Ty Cho nghỉ: <span id="company_day_off_span${index_time_frame}"></span> ngày</h6>
                        <h6 class="text-danger" id = "ExistConfirmedFormLabel${index_time_frame}">Đã Có Đơn Tồn Tại Trong Khoảng Thời Gian Này</h6>
                    </div>
                    <div class="col-xl-4">
                        <h6 class="text-center" id ="NoExistConfirmedFormLabel${index_time_frame}">Tổng số ngày xin: <span id="total_day_off_asked_for_span${index_time_frame}"></span> ngày</h6>
                    </div>
                    <div class="col-xl-4">
                        <h6 id="DayOffNeedToAskForLabel${index_time_frame}" class="text-center">Tổng số ngày cần xin: <span id="total_day_off_need_to_ask_for_span${index_time_frame}"></span> ngày</h6>
                    </div>
                </div>
            `);


      input = jQuery(input_string);
      jQuery('#FormOfTimeFrames').append(input);

      if (index_time_frame == 1) {
        $(`#start_date${index_time_frame}`).datepicker({
          locale: 'vi',
          format: 'yyyy-mm-dd',
          todayHighlight: true,
          daysOfWeekDisabled: [0]
        });

        $(`#end_date${index_time_frame}`).datepicker({
          locale: 'vi',
          format: 'yyyy-mm-dd',
          todayHighlight: true,
          startDate: $(`#start_date${index_time_frame}`).val(),
          daysOfWeekDisabled: [0]
        });

      }

      time_frame_dict[index_time_frame] = [$(`#start_date${index_time_frame}`).val(), $(`#end_date${index_time_frame}`).val(), $(`#start_time${index_time_frame}`).val(), $(`#end_time${index_time_frame}`).val(), $(`#typeDayOffChanger${index_time_frame}`).val(), $(`#total_day_off_in_timeframe${index_time_frame}`).val()];

      const time_frame_dict_keys = Object.keys(time_frame_dict);
      time_frame_dict_keys.forEach(
        function(key, temp_key_index) {

          checkTimeFrame(key);
          checkDayOffInfo(key);

          $(`#deleteTimeFrameButton${key}`).on('click', function() {
            $(`#FormOfTimeFrameHeader${key}`).remove();
            $(`#FormOfTimeFrameBody${key}`).remove();

            delete time_frame_dict[key];
            if (temp_key_index > 0)
              checkTimeFrameDownwardsWithEndDate(temp_key_index - 1);

            Object.keys(time_frame_dict).forEach(function(keyX) {
              checkDayOffInfo(keyX);

            });
            checkRemainingPTO();

            // formTotalDayOffLabelChanger();
          });

          $(`#start_date${key}`).on('change', function() {
            $(`#end_date${key}`).datepicker('destroy');
            $(`#end_date${key}`).datepicker({
              locale: 'vi',
              format: 'yyyy-mm-dd',
              todayHighlight: true,
              daysOfWeekDisabled: [0],
              startDate: $(this)[0].value
            });

            if ($(this)[0].value > $(`#end_date${key}`)[0].value) {
              $(`#end_date${key}`).datepicker('clearDates');
              $(`#end_date${key}`).val($(this)[0].value);
              checkTimeFrameDownwardsWithEndDate(temp_key_index);
            }

            checkTimeFrame(key);

            Object.keys(time_frame_dict).forEach(function(keyX) {
              checkDayOffInfo(keyX);

            });

            checkRemainingPTO();
            // formTotalDayOffLabelChanger()


          });

          $(`#end_date${key}`).on('change', function() {
            checkTimeFrame(key);
            checkTimeFrameDownwardsWithEndDate(temp_key_index);

            Object.keys(time_frame_dict).forEach(function(keyX) {
              checkDayOffInfo(keyX);
            });
            checkRemainingPTO();
            // formTotalDayOffLabelChanger();
          });

          $(`#start_time${key}, #end_time${key}, #typeDayOffChanger${key}`).on('change', function() {
            checkTimeFrame(key);
            Object.keys(time_frame_dict).forEach(function(keyX) {
              checkDayOffInfo(keyX);

            });
            checkRemainingPTO();
            // formTotalDayOffLabelChanger();


          });

        }
      );

      checkTimeFrameDownwardsWithEndDate(time_frame_dict_keys.length - 2 > 0 ? time_frame_dict_keys.length - 2 : 0);
      checkRemainingPTO();
      // formTotalDayOffLabelChanger();
    }

    function checkTimeFrameDownwardsWithEndDate(initial_index = 0) {
      const time_frame_dict_keys = Object.keys(time_frame_dict);
      for (let i = initial_index; i < time_frame_dict_keys.length; i += 1) {
        const current_key = time_frame_dict_keys[i];
        const next_key = time_frame_dict_keys[i + 1]

        const temp_date = new Date($(`#end_date${current_key}`).val());
        temp_date.setDate(temp_date.getDate() + 1);

        if (temp_date.getDay() == 0)
          temp_date.setDate(temp_date.getDate() + 1);

        const temp_date_string = temp_date.toISOString().slice(0, 10);

        $(`#start_date${next_key}`).datepicker('destroy');
        $(`#end_date${next_key}`).datepicker('destroy');

        $(`#start_date${next_key}`).val(temp_date_string);
        $(`#end_date${next_key}`).val(temp_date_string);
        $(`#start_date${next_key}`).datepicker({
          locale: 'vi',
          format: 'yyyy-mm-dd',
          todayHighlight: true,
          startDate: temp_date_string,
          daysOfWeekDisabled: [0]
        });


        $(`#end_date${next_key}`).datepicker({
          locale: 'vi',
          format: 'yyyy-mm-dd',
          todayHighlight: true,
          startDate: $(`#start_date${next_key}`).val(),
          daysOfWeekDisabled: [0]
        });

      }


      for (let i = 0; i < time_frame_dict_keys.length; i += 1) {
        const current_key = time_frame_dict_keys[i];
        time_frame_dict[current_key] = [$(`#start_date${current_key}`).val(), $(`#end_date${current_key}`).val(), $(`#start_time${current_key}`).val(), $(`#end_time${current_key}`).val(), $(`#typeDayOffChanger${current_key}`).val(), $(`#total_day_off_in_timeframe${current_key}`).val()];
      }
    }



  });
</script>
@endsection