@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('content')

@php
$formDayOff_X = $dataInit['current_form_day_off'];
$tracking_day_off_of_form = $dataInit['tracking_day_off_of_form'];
$basic_info_of_user = $formDayOff_X -> basicInfo;

$current_form_day_off_user_id = $dataInit['current_form_day_off_user_id'];

$total_pto_asked_for = 0;
foreach($formDayOff_X -> detailForms as $detailForm) {
if ($detailForm -> type == 1)
$total_pto_asked_for += $detailForm -> total_day_off_in_timeframe;
}
@endphp

<div class="row">
  <div class="col-12 d-flex justify-content-between mb-3">
    <h3 class="d-inline m-0">Xem Đơn Xin Nghỉ Phép</h3>
  </div>
</div>
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Họ và tên</label>
        <input type="text" class="form-control" value="{{ ($basic_info_of_user) ? $basic_info_of_user -> full_name : N/A }}" disabled readonly>
      </div>
      <div class="col-xl-1">
        <label for="borderInput" class="form-label">Mã nhân viên</label>
        <input type="text" class="form-control" value="{{ ($basic_info_of_user) ? $basic_info_of_user -> employee_code : N/A }}" disabled readonly>
      </div>

      <div class="col-xl-2">
        <label for="borderInput" class="form-label">Phòng</label>
        <input type="text" class="form-control" value="{{ ($basic_info_of_user) ? $basic_info_of_user -> organizational -> room -> name : N/A }}" disabled readonly>
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Bộ phận</label>
        <input type="text" class="form-control" value="{{ ($basic_info_of_user) ? $basic_info_of_user -> organizational -> team -> name : N/A }}" disabled readonly>
      </div>
      <div class="col-xl-3">
        <label for="borderInput" class="form-label">Chức danh</label>
        <input type="text" class="form-control" value="{{ ($basic_info_of_user) ? $basic_info_of_user -> organizational -> position -> name : N/A }}" disabled readonly>
      </div>
    </div>
  </div>

  @php
  $pto_prev_buffer = $formDayOff_X -> status == 6 ? $total_pto_asked_for : 0;
  $pto_cur_buffer = $formDayOff_X -> status == 6 ? $total_pto_asked_for : 0;
  @endphp
  @if(auth() -> id() == $formDayOff_X -> user -> id || auth() -> user() -> hasAnyPermission(['form_day_off-view_admin']) || auth() -> id() == $current_form_day_off_user_id)
  <div class="row mt-4">
    <div class="col-xl-6">
      @if ($tracking_day_off_of_form -> prevYearTrackingDayOff)
      @if (intval(date ('m', strtotime(now()))) < 4 ) <h4 class="text-center">Phép Năm {{ (date ('Y', strtotime('-1 year'. now()))) }} còn được dùng:<span>{{ $tracking_day_off_of_form -> prevYearTrackingDayOff -> year_pto_count + $pto_prev_buffer }}</span> ngày </h3>
        @else
        <h4 class="text-center">Phép Năm {{ (date ('Y', strtotime('-1 year'. now()))) }} đã hết hạn sử dụng</h3>
          @endif
          @else
          <h4 class="text-center"> Không Đi Làm vào Năm {{ ($tracking_day_off_of_form -> year) -1 }} </h4>
          @endif
    </div>
    <div class="col-xl-6">
      <h4 class="text-center">Phép Năm {{ (date ('Y', strtotime( now()))) }} còn được dùng: <span>{{ $tracking_day_off_of_form -> year_pto_count  + $pto_cur_buffer }}</span> ngày</h4>
    </div>
  </div>
  @endif
</div>

<div class="card">
  <div class="card-header">
    <h3>Thời gian xin nghỉ</h3>
    @foreach($formDayOff_X -> detailForms as $key => $detailForm)
    <div class="row card-body justify-content-between">
      <div class="col-xl-2">
        <label class="form-label">Ngày bắt đầu</label>
        <input type="text" class="form-control" value=" {{ $detailForm -> start_date -> format('Y-m-d')}}" disabled readonly>
      </div>

      <div class="col-xl-1">
        <label class="form-label">Giờ bắt đầu</label>
        <input type="text" class="form-control" value="{{ $detailForm -> start_time }}" disabled readonly>
      </div>
      <div class="col-xl-2">
        <label class="form-label">Ngày kết thúc</label>
        <input type="text" class="form-control" value="{{ $detailForm -> end_date-> format('Y-m-d') }}" disabled readonly>
      </div>

      <div class=" col-xl-1">
        <label class="form-label">Giờ kết thúc</label>
        <input type="text" class="form-control" value="{{ $detailForm -> end_time }}" disabled readonly>
      </div>

      <div class="col-xl-3">
        <label class="form-label">Loại Nghỉ Phép</label>
        @if ($detailForm -> type == 1)
        <input type="text" class="form-control" value="Phép Năm - Dùng {{ $detailForm -> total_day_off_in_timeframe }} Ngày" disabled readonly>
        @else
        <input type="text" class="form-control" value="{{ config('constants.type_form_day_off')[$detailForm -> type]  }}" disabled readonly>
        @endif
      </div>

      <div class="col-xl-3">
        <label class="form-label">Lý Do</label>
        <textarea type="text" class="form-control" rows="4" disabled readonly> {{ $detailForm -> reason }} </textarea>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-xl-4">
        <h6 class="text-center">Tổng số ngày cần xin: {{ $detailForm -> total_day_off_in_timeframe }} ngày</h6>
      </div>
    </div>

    @endforeach
    <div class="row">
  <div class="col-1"></div>
  <div class="col-3">
    @if(auth() -> id() == $formDayOff_X -> user -> id || auth() -> user() -> hasAnyPermission(['form_day_off-view_admin']) || auth() -> id() == $current_form_day_off_user_id)
      @if ($formDayOff_X -> status == 6)
        @if(intval(date ('m', strtotime(now()))) < 4 && $tracking_day_off_of_form -> prevYearTrackingDayOff)
        <h4 class="text-danger">Số ngày phép năm còn lại: <span> {{ $tracking_day_off_of_form -> year_pto_count + $tracking_day_off_of_form -> prevYearTrackingDayOff -> year_pto_count + $pto_prev_buffer - $total_pto_asked_for  }}</span> ngày</h3>
        @else
        <h4 class="text-danger">Số ngày phép năm còn lại: <span> {{ $tracking_day_off_of_form -> year_pto_count + $pto_cur_buffer - $total_pto_asked_for }}</span> ngày</h4>
        @endif
      @else      
          <h4 class="text-danger">Chưa Được P. QT HC-NS Duyệt Đơn</h4>
          @if ($total_pto_asked_for > 0)
          <h4 class="text">Đang Xin {{ $total_pto_asked_for }} Ngày Phép Năm</h4>
          @else
          <h4 class="text">Không Có Xin Phép Năm</h4>
          @endif
        
      @endif
    @endif

  </div>
  </div>
</div>
</div>


  
<div class="row">
  <div class="card">
    <div class="col-12 d-flex mt-2 justify-content-end" style="padding-right: 2%;">
      @if ($formDayOff_X -> review_date != null)
      @php
      $date = \Carbon\Carbon::parse($formDayOff_X -> review_date);
      @endphp
      <label class=" form-label mb-0 me-5 " style="margin-top: 2px">
        Ngày duyệt : Ngày {{ $date->day }} Tháng {{ $date->month }} Năm {{ $date->year }}</label>
      @endif
    </div>
    <div class="row card-body justify-content-around">
      <div class="col-xl-2">
        <div class="row">
          <div class="col-12 text-center">
            <h4 class="form-label text-center">Người Tạo Đơn</h4>
          </div>
          <div class="col-12 text-center">
            <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ $formDayOff_X -> user -> name }}</label>
          </div>
          @if($formDayOff_X -> status != 1)
          <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
          @else
          <h6 class="form-label text-center fst-italic mt-2">Chưa xác nhận</h6>
          @endif

        </div>
      </div>

      @php
      $replacement_basic_info = null;
      $replacement_confirm = null;
      $direct_boss_basic_info = null;
      $direct_boss_confirm = null;
      $hr_basic_info = null;
      $hr_confirm = null;

      $formDayOff_X_Confirms = $formDayOff_X -> confirms;

      foreach ($formDayOff_X_Confirms as $formDayOffConfirm) {
      $confirmX = $formDayOffConfirm -> confirm;
      switch ($confirmX -> type_confirm) {
      case 7:
      $replacement_basic_info = $confirmX -> staff;
      $replacement_confirm = $confirmX;
      break;
      case 8:
      $direct_boss_basic_info = $confirmX -> staff;
      $direct_boss_confirm = $confirmX;
      break;
      case 2:
      $hr_basic_info = $confirmX -> staff;
      $hr_confirm = $confirmX;
      break;
      }
      }
      @endphp
      <div class="col-xl-2">
        <h4 class="form-label text-center">Người Thay Thế</h4>
        <div class="col-x1-6 text-center">
          <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ ($replacement_basic_info) ? $replacement_basic_info -> full_name : "Không Có" }}</label>
        </div>
        @if($replacement_confirm)
        @switch($replacement_confirm -> status)
        @case(0)
        <h6 class="form-label text-center fst-italic mt-2">Chưa xác nhận</h6>
        @break
        @case(1)
        <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
        @break
        @case(2)
        <h6 class="form-label text-center fst-italic mt-2">Từ Chối</h6>
        <div class="col-12 text-center">
          <label>Lý do : {{ $replacement_confirm -> note }}</label>
        </div>
        @break
        @endswitch
        @else
        <h6 class="form-label text-center fst-italic mt-2">Không Cần xác nhận</h6>
        @endif
      </div>
      <div class="col-xl-2">
        <h4 class="form-label text-center">Người Quản Lý Trực Tiếp</h4>
        <div class="col-x1-6 text-center">
          <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ ($direct_boss_basic_info) ? $direct_boss_basic_info -> full_name : "Chưa Có" }}</label>
        </div>
        @if($direct_boss_confirm)
        @switch($direct_boss_confirm -> status)
        @case(0)
        <h6 class="form-label text-center fst-italic mt-2">Chưa xác nhận</h6>
        @break
        @case(1)
        <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
        @break
        @case(2)
        <h6 class="form-label text-center fst-italic mt-2">Từ Chối</h6>
        <div class="col-12 text-center">
          <label>Lý do : {{ $direct_boss_confirm -> note }}</label>
        </div>
        @break
        @endswitch
        @else
        <h6 class="form-label text-center fst-italic mt-2">Không Cần xác nhận</h6>
        @endif
      </div>
      <div class="col-xl-2">
        <h4 class="form-label text-center">P. QT HC-NS</h4>
        <div class="col-x1-6 text-center">
          <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ ($hr_basic_info) ? $hr_basic_info -> full_name : "Chưa Có" }}</label>
        </div>
        @if($hr_confirm)
        @switch($hr_confirm -> status)
        @case(0)
        <h6 class="form-label text-center fst-italic mt-2">Chưa xác nhận</h6>
        @break
        @case(1)
        <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
        @break
        @case(2)
        <h6 class="form-label text-center fst-italic mt-2">Từ Chối</h6>
        <div class="col-12 text-center">
          <label>Lý do : {{ $hr_confirm -> note }}</label>
        </div>
        @break
        @endswitch
        @else
        <h6 class="form-label text-center fst-italic mt-2">Không Cần xác nhận</h6>
        @endif
      </div>
    </div>
  </div>
  @if ( ( $formDayOff_X -> status == 2 && auth() -> user() -> basic_info_id == $replacement_basic_info -> id ) ||
  ( ($formDayOff_X -> status == 3 || $formDayOff_X -> status == 4 ) && auth() -> user() -> basic_info_id == $direct_boss_basic_info -> id ) ||
  ( $formDayOff_X -> status == 5 && auth() -> user() -> basic_info_id == $hr_basic_info -> id ) )

  @php

  $next_confirm_basic_info_id = null;
  $confirm_id_to_update_status = null;

  switch ($formDayOff_X -> status ) {
  case(2):
  $next_confirm_basic_info_id = $direct_boss_basic_info -> id;
  $confirm_id_to_update_status = $replacement_confirm -> id;
  break;

  case(3):
  case(4):
  $next_confirm_basic_info_id = $hr_basic_info -> id;
  $confirm_id_to_update_status = $direct_boss_confirm -> id;
  break;

  case(5):
  $confirm_id_to_update_status = $hr_confirm -> id;
  break;
  }
  @endphp
  <form method="POST" action="{{ route('FormDayOffUpdateStatus') }}" id="formConfirm">
    @csrf
    <input hidden name="confirm_id_to_update_status" value="{{ $confirm_id_to_update_status }}">
    <input hidden name="next_confirm_basic_info_id" value="{{ $next_confirm_basic_info_id }}">
    <input hidden name="reason" id="reasonConfirm">
    <input hidden name="confirm_flag" id="confirm_flag" value="1">
    <input hidden name="form_day_off_id" value="{{ $formDayOff_X -> id }}">
    <div class="row justify-content-center pe-3 mb-3 mt-3">
      <div class="col-3 justify-content-between d-flex">
        <button data-id="1" type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content">
          <i class="ri-check-double-line label-icon align-middle fs-16 me-2"></i> Duyệt</button>
        <button data-id="2" type="submit" id="btnDeny" class="btn btn-danger waves-effect waves-light" style="width: fit-content">
          <i class="ri-error-warning-line label-icon align-middle rounded-pill fs-16 me-2 "></i> Không Duyệt</button>
      </div>
    </div>
  </form>
  @endif
</div>
@endsection
@section('script')
<script src="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/form-pickers.init.js') }}"></script>
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<script>
  $(document).ready(function() {
    $('#btnDeny').on('click', function(event) {
      event.preventDefault();
      swal.fire({
          title: 'Lý do không duyệt đơn',
          html: `<input class="form-control" type="text" id="reason"  placeholder="Lý do" required>`,
          icon: 'warning',
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Đồng ý',
          showDenyButton: true,
          denyButtonText: 'Hủy',
          preConfirm: () => {
            const reason = Swal.getPopup().querySelector('#reason').value
            if (!reason) {
              Swal.showValidationMessage(`Vui lòng nhập lý do`)
            }
            return {
              reason: reason,
            }
          }
        })
        .then((result) => {
          if (result.isConfirmed) {
            $('#reasonConfirm').val(result.value.reason);
            $('#confirm_flag').val(2);
            $('#formConfirm').submit();
          } else if (result.isDenied) {
            Swal.fire('Your record is safe', '', 'info')
          }

        });
    });
  });
</script>
@endsection