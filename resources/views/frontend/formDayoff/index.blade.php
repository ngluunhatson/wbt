@extends('layouts.fe.master2')
@section('title')
@endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<style>
    table th {
        white-space: nowrap;
        text-align: center;
        vertical-align: middle;
    }
</style>
<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h3 id="titlePage">DANH SÁCH ĐƠN XIN NGHỈ PHÉP</h3>
    </div>
</div>


@php
$allFormDayOffsDict = $dataInit['allFormDayOffsDict'];
$myFormDayOffsDict = $dataInit['myFormDayOffsDict'];

$myFormDayOffsDict_YearKeys = array_keys($myFormDayOffsDict);
$allFormDayOffsDict_YearKeys = array_keys($allFormDayOffsDict);

$current_year_int = intval(date('Y', strtotime(now())));
$current_month_int = intval(date('m', strtotime(now())));


@endphp

<div class="row">
    <div class="card-body">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">

        @can('form_day_off-index')

            <li class="nav-item">
                <a class="nav-link active" data-bs-toggle="tab" href="#my-form-day-offs" role="tab" aria-selected="false">
                    Đơn Xin của Tôi
                </a>
            </li>
        @endcan
            <li class="nav-item">
                <a class="nav-link @if(!auth() -> user() -> hasAnyPermission(['form_day_off-index'])) active @endif" data-bs-toggle="tab" href="#all-form-day-offs" role="tab" aria-selected="false">
                    Đơn Xin của Team
                </a>
            </li>
        </ul>
        @can('form_day_off-index')
        <div class="tab-content text-muted">

            <div class="tab-pane active" id="my-form-day-offs" role="tabpanel">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between mb-3">
                        <div class="col-xl-1">
                            <label for="datePicker" class="form-label">Tháng / Năm</label>
                            <input type="text" class="form-control" name="monthYear" id="datePicker" value="{{ old('monthYear', date('m/Y', strtotime(now()) ) ) }}">
                        </div>
                        @can('form_day_off-create')
                        <a href="/formdayoff/create">
                            <button type="button" class="btn btn-success btn-label waves-effect waves-light"><i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Tạo Đơn</button>
                        </a>
                        @endcan
                    </div>

                </div>
                @foreach($myFormDayOffsDict_YearKeys as $index => $year)
                @for($month = 1; $month < 13; $month ++) 
                <div class="row" id="my-day-offs-div-year{{$year}}-month{{$month}}">
                    <div class="card">
                        <div class="card-body overflow-auto">
                            <table id="my-day-offs-table-year{{$year}}-month{{$month}}" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Từ Ngày</th>
                                        <th>TGBĐ</th>
                                        <th>Đến Ngày</th>
                                        <th>TGKT</th>
                                        <th>Tổng Ngày Xin Phép </th>
                                        <th>Trạng Thái</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(array_key_exists($month, $myFormDayOffsDict[$year]))

                                    @foreach($myFormDayOffsDict[$year][$month] as $key => $myFormDayOff_X)
                                    @php
                                    $total_day_off_asked_for = 0;
                                    $initial_form_start_date = '';
                                    $initial_form_start_time = '';
                                    $last_form_end_date = '';
                                    $last_form_end_time = '';

                                    $detailForms = $myFormDayOff_X -> detailForms;

                                    $firstDetailForm = $detailForms[0];
                                    $lastDetailForm = $detailForms[count($detailForms) - 1];

                                    $initial_form_start_date = $firstDetailForm -> start_date;
                                    $initial_form_start_time = $firstDetailForm -> start_time;

                                    $last_form_end_date = $lastDetailForm -> end_date;
                                    $last_form_end_time = $lastDetailForm -> end_time;

                                    @endphp

                                    <tr>
                                        <td class="dtr-control">{{ $key + 1 }}</td>
                                        <td> {{ date('Y-m-d', strtotime($initial_form_start_date)) }}</td>
                                        <td> {{ $initial_form_start_time }}</td>
                                        <td> {{ date('Y-m-d', strtotime($last_form_end_date)) }}</td>
                                        <td> {{ $last_form_end_time }}</td>
                                        <td> {{ $myFormDayOff_X -> total_day_off_asked_for }}</td>
                                        <td> {{ config('constants.status_confirm.form_dayoff')[$myFormDayOff_X -> status] }} </td>
                                        <td>
                                            <div class="dropdown d-inline-block">
                                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <i class="ri-more-fill align-middle"></i>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-end">
                                                    <li><a href="/formdayoff/view/{{$myFormDayOff_X->id}}" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>

                                                    @can('form_day_off-update')
                                                    @if (auth() -> id() == $myFormDayOff_X -> created_by && (!in_array($myFormDayOff_X -> status, [4,5,6])))
                                                    <li><a href="/formdayoff/edit/{{$myFormDayOff_X->id}}" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                                                    @endif
                                                    @endcan

                                                    @can('form_day_off-delete')
                                                    @if (auth() -> id() == $myFormDayOff_X -> created_by && (!in_array($myFormDayOff_X -> status, [4, 5, 6])))
                                                    <li><a href="#" id="formDeleteButton{{ $myFormDayOff_X -> id }}" class="dropdown-item remove-item-btn"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Delete</a>
                                                    </li>
                                                    <form method="post" id="formDelete{{ $myFormDayOff_X -> id }}" action="{{  route('FormDayOffDelete', $myFormDayOff_X->id) }}" style="display:inline">
                                                        @method('delete')
                                                        @csrf
                                                    </form>
                                                    @endif
                                                    @endcan
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endfor
                @endforeach
        </div>
        @endcan



        <div class="tab-pane @if(!auth() -> user() -> hasAnyPermission(['form_day_off-index'])) active @endif" id="all-form-day-offs" role="tabpanel">
            <div class="row">
                <div class="col-12 d-flex justify-content-between mb-3">
                    <div class="col-xl-1">
                        <label for="datePickerAll" class="form-label">Tháng / Năm</label>
                        <input type="text" class="form-control" name="monthYear" id="datePickerAll" value="{{ old('monthYear', date('m/Y', strtotime(now()) ) ) }}">
                    </div>

                    @can('form_day_off-create_admin')
                    <a href="/formdayoff/create_admin">
                        <button type="button" class="btn btn-success btn-label waves-effect waves-light"><i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm Đơn Cho Người Khác</button>
                    </a>
                    @endcan
                </div>
            </div>

            @foreach($allFormDayOffsDict_YearKeys as $index => $year)
            @for($month = 1; $month < 13; $month ++) 
            <div class="row" id="all-day-offs-div-year{{$year}}-month{{$month}}">
                <div class="card">
                    <div class="card-body overflow-auto">
                        <table id="all-day-offs-table-year{{$year}}-month{{$month}}" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Mã Nhân Viên </th>
                                    <th>Tên Tiếng Anh</th>
                                    <th>Phòng</th>
                                    <th>Từ Ngày</th>
                                    <th>TGBĐ</th>
                                    <th>Đến Ngày</th>
                                    <th>TGKT</th>
                                    <th>Tổng Ngày Xin Phép </th>
                                    <th>Người Tạo</th>
                                    <th>Trạng thái</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(array_key_exists($month, $allFormDayOffsDict[$year]))

                                @foreach($allFormDayOffsDict[$year][$month] as $key => $formDayOff_X)
                                @php

                                $total_day_off_asked_for = 0;
                                $initial_form_start_date = '';
                                $initial_form_start_time = '';
                                $last_form_end_date = '';
                                $last_form_end_time = '';

                                $detailForms = $formDayOff_X -> detailForms;

                                $firstDetailForm = $detailForms[0];
                                $lastDetailForm = $detailForms[count($detailForms) - 1];

                                $initial_form_start_date = $firstDetailForm -> start_date;
                                $initial_form_start_time = $firstDetailForm -> start_time;

                                $last_form_end_date = $lastDetailForm -> end_date;
                                $last_form_end_time = $lastDetailForm -> end_time;


                                @endphp

                                <tr>
                                    <td class="dtr-control">{{ $key + 1 }}</td>
                                    <td> {{ $formDayOff_X -> basicInfo -> employee_code  }}</td>
                                    <td> {{ $formDayOff_X -> basicInfo -> english_name  }}</td>
                                    <td> {{ $formDayOff_X -> basicInfo -> organizational -> room -> name  }}</td>
                                    <td> {{ date('Y-m-d', strtotime($initial_form_start_date)) }}</td>
                                    <td> {{ $initial_form_start_time }}</td>
                                    <td> {{ date('Y-m-d', strtotime($last_form_end_date)) }}</td>
                                    <td> {{ $last_form_end_time }}</td>
                                    <td> {{ $formDayOff_X -> total_day_off_asked_for}}</td>
                                    <td> {{ $formDayOff_X -> user -> name }} </td>
                                    <td> {{ config('constants.status_confirm.form_dayoff')[$formDayOff_X -> status] }} </td>


                                    <td>
                                        <div class="dropdown d-inline-block">
                                            <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                <i class="ri-more-fill align-middle"></i>
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-end">
                                                <li><a href="/formdayoff/view/{{$formDayOff_X->id}}" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>

                                                @can('form_day_off-update_admin')
                                                @if (!in_array($formDayOff_X -> status, [4,5,6]))
                                                <li><a href="/formdayoff/edit_admin/{{$formDayOff_X->id}}" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                                                @endif
                                                @endcan

                                                @can('form_day_off-delete')
                                                @if (!in_array($formDayOff_X -> status, [4, 5, 6]))
                                                <li><a href="#" id="formDeleteButton{{ $formDayOff_X -> id }}" class="dropdown-item remove-item-btn"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Delete</a>
                                                </li>
                                                <form method="post" id="formDelete{{ $formDayOff_X -> id }}" action="{{  route('FormDayOffDelete', $formDayOff_X->id) }}" style="display:inline">
                                                    @method('delete')
                                                    @csrf
                                                </form>
                                                @endif
                                                @endcan
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            @endfor
            @endforeach

    </div>
</div><!-- end card-body -->

@endsection
@section('script')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script>
    $(document).ready(function() {
        const myFormDayOffsDict_YearKeys = <?php echo json_encode($myFormDayOffsDict_YearKeys); ?>;
        const allFormDayOffsDict_YearKeys = <?php echo json_encode($allFormDayOffsDict_YearKeys); ?>;

        $(function() {

            $('#datePickerAll').datepicker({
                format: 'mm/yyyy',
                startView: "months",
                minViewMode: "months",
            });



            allFormDayOffsDict_YearKeys.forEach(
                function(y) {
                    for (let m = 1; m < 13; m++) {

                        $(`#all-day-offs-div-year${y}-month${m}`).hide();
                        $(`#all-day-offs-table-year${y}-month${m}`).dataTable();

                    }
                }
            );

            let value_date = $(`#datePickerAll`).val();
            value_date = value_date.slice(0, 3).concat('01/', value_date.slice(3));

            date_object = new Date(value_date);
            const date_year = (date_object.getFullYear());
            const date_month = (date_object.getMonth()) + 1;

            $(`#all-day-offs-div-year${date_year}-month${date_month}`).show();
        });

        $(function() {

            $('#datePicker').datepicker({
                format: 'mm/yyyy',
                startView: "months",
                minViewMode: "months",
            });


            myFormDayOffsDict_YearKeys.forEach(
                function(y) {
                    for (let m = 1; m < 13; m++) {

                        $(`#my-day-offs-div-year${y}-month${m}`).hide();
                        $(`#my-day-offs-table-year${y}-month${m}`).dataTable();

                    }
                }
            );

            let value_date = $(`#datePicker`).val();
            value_date = value_date.slice(0, 3).concat('01/', value_date.slice(3));

            date_object = new Date(value_date);
            const date_year = (date_object.getFullYear());
            const date_month = (date_object.getMonth()) + 1;

            $(`#my-day-offs-div-year${date_year}-month${date_month}`).show();
        });


        $(function() {

            const allFormDayOffs = <?php echo json_encode($dataInit['allFormDayOffs']); ?>;
            const myFormDayOffs = <?php echo json_encode($dataInit['myFormDayOffs']); ?>;

            Object.keys(allFormDayOffs).forEach(
                function(key) {
                    const strDeleteFormAllButton = '#formDeleteAllButton'.concat(allFormDayOffs[key]['id'].toString());
                    const strDeleteFormAll = '#formDeleteAll'.concat(allFormDayOffs[key]['id'].toString());
                    $(strDeleteFormAllButton).on('click', function(event) {
                        event.preventDefault();
                        swal.fire({
                                title: 'Bạn muốn xóa dữ liệu này ?',
                                text: "Lưu ý : dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                                icon: 'warning',
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Đồng ý',
                                showDenyButton: true,
                                denyButtonText: 'Hủy bỏ',
                            })
                            .then((result) => {
                                if (result.isConfirmed) {
                                    $(strDeleteFormAll).submit();
                                } else if (result.isDenied) {
                                    Swal.fire('Dữ liệu được giữ lại', '', 'info')
                                }
                            });
                    });
                }
            );

            Object.keys(myFormDayOffs).forEach(
                function(key) {
                    const strDeleteFormButton = '#formDeleteButton'.concat(myFormDayOffs[key]['id'].toString());
                    const strDeleteForm = '#formDelete'.concat(myFormDayOffs[key]['id'].toString());
                    $(strDeleteFormButton).on('click', function(event) {
                        event.preventDefault();
                        swal.fire({
                                title: 'Bạn muốn xóa dữ liệu này ?',
                                text: "Lưu ý : dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                                icon: 'warning',
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Đồng ý',
                                showDenyButton: true,
                                denyButtonText: 'Hủy bỏ',
                            })
                            .then((result) => {
                                if (result.isConfirmed) {
                                    $(strDeleteForm).submit();
                                } else if (result.isDenied) {
                                    Swal.fire('Dữ liệu được giữ lại', '', 'info')
                                }
                            });
                    });
                }
            );

        });

        $(`#datePickerAll`).on('change', function() {
            let value_date = $(`#datePickerAll`).val();
            value_date = value_date.slice(0, 3).concat('01/', value_date.slice(3));

            date_object = new Date(value_date);
            const date_year = (date_object.getFullYear());
            const date_month = (date_object.getMonth()) + 1;

            $(`#all-day-offs-div-year${date_year}-month${date_month}`).show();

            allFormDayOffsDict_YearKeys.forEach(
                function(y) {
                    for (let m = 1; m < 13; m++) {
                        if ((m != date_month) && (y == date_year))
                            $(`#all-day-offs-div-year${y}-month${m}`).hide();

                    }
                }
            );
        });

        $(`#datePicker`).on('change', function() {
            let value_date = $(`#datePicker`).val();
            value_date = value_date.slice(0, 3).concat('01/', value_date.slice(3));

            date_object = new Date(value_date);
            const date_year = (date_object.getFullYear());
            const date_month = (date_object.getMonth()) + 1;

            $(`#my-day-offs-div-year${date_year}-month${date_month}`).show();

            allFormDayOffsDict_YearKeys.forEach(
                function(y) {
                    for (let m = 1; m < 13; m++) {
                        if ((m != date_month) && (y == date_year))
                            $(`#my-day-offs-div-year${y}-month${m}`).hide();

                    }
                }
            );
        });


    });
</script>
@endsection