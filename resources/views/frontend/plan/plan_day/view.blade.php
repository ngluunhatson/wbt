@extends('layouts.fe.master2')

@section('title')
@lang('translation.starter')
@endsection

@section('css')

@endsection


@section('content')
    @include('frontend.plan.plan_day.buttons', ['dataInit' => $dataInit])
    @include('frontend.plan.plan_day.view_detail', ['dataInit' => $dataInit])
@endsection


@section('script')

@endsection