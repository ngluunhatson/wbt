@extends('layouts.fe.master2')

@section('title')
@lang('translation.starter')
@endsection

@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection


@php
$url_logo_left = URL::asset('assets/images/logo-ac.png');
$url_logo_right = URL::asset('assets/images/logo-ac.png');
$rooms = $dataInit['rooms'];
$staff = auth() -> user() -> staff;

@endphp

@section('content')

<ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">
    @if($staff)
    <li class="nav-item">
        <a class="nav-link active" data-bs-toggle="tab" href="#current_plan_day" role="tab" aria-selected="false">
            Kế Hoạch của Hôm Nay
        </a>
    </li>
    @endif
    
    <li class="nav-item">
        <a class="nav-link @if(!$staff) active @endif" data-bs-toggle="tab" href="#list_plan_day" role="tab" aria-selected="false">
            Danh Sách Kế Hoạch 1 Ngày
        </a>
    </li>
</ul>

@if ($staff)
<div class="tab-content text-muted">
    <div class="tab-pane active" id="current_plan_day" role="tabpanel">
    @if ($dataInit['planDay'])
        @include('frontend.plan.plan_day.view_detail', ['dataInit' => $dataInit])
    @else
        <h2> Chưa Có Kế Hoạch Hôm Nay, tạo ở Kế Hoạch Tuần</h2>
    @endif
</div>
@endif

<div class="tab-pane @if(!$staff) active @endif" id="list_plan_day" role="tabpanel">
    <div class ="card">
        <div class ="card-body">
            <div class="row justify-content-between">
                <!-- Dark Logo-->
                <div class = "col-xl-2">
                    <a class="logo logo-dark">
                        <span class="logo-sm">
                            <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                        </span>
                        <span class="logo-lg">
                            <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;;">
                        </span>
                    </a>
                <!-- Light Logo-->
                    <a class="logo logo-light">
                        <span class="logo-sm">
                            <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                        </span>
                        <span class="logo-lg">
                            <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                        </span>
                    </a>
                </div>

                <div class = "col-xl-8 text-center align-items-center" style = "display:grid">   
                    <h3 style="color:#005091; font-size:170%;">
                        <p>DANH SÁCH KẾ HOẠCH LÀM VIỆC TRONG NGÀY</p>
                        @if(!$staff)
                        <p class ="text-center" style="margin-top:-1%">CỦA CÔNG TY</p>
                        @endif
                    </h3>
                </div>


                <!-- Dark Logo-->
                <div class = "col-xl-2">
                    <a class="logo logo-dark">
                        <span class="logo-sm">
                            <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                        </span>
                        <span class="logo-lg">
                            <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                        </span>
                    </a>
                    <!-- Light Logo-->
                    <a class="logo logo-light" >
                        <span class="logo-sm">
                            <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                        </span>
                        <span class="logo-lg">
                            <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                        </span>
                    </a>
                </div>
            </div>

            <div class="row justify-content-start">
                <div style = "width:10%">
                    <label for="start_date" class="form-label">Từ Ngày</label>
                    <input type = 'text' name = 'start_date' id = 'start_date' class = 'form-control datepicker'  value = "{{ now() -> format('Y-m-d') }}">
                </div>  
                <div style = "width:0.5%">
                </div>

                <div style = "width:10%">
                    <label for="start_date" class="form-label">Đến Ngày</label>
                    <input type = 'text' name = 'end_date'  id = 'end_date' class = 'form-control datepicker'  value = "{{ now() -> format('Y-m-d') }}">
                </div>
            </div>

            <div class="row justify-content-start mt-3" style = "width:103.5%">
                <div style = "width:50%">
                    <div class="input-group">
                        <div class="input-group-prepend" style ="margin-right:2%; width:47%;">
                            <label for="borderInput" class="form-label">Phòng</label>
                            @if ($staff)
                            <input hidden name ="room_id" id = "room_id" value ="{{ $staff -> organizational -> room -> id }}" />
                            <input disabled class = "form-control mb-3"style="margin-left:-4%; background:none; border:0cm"  value ="{{ $staff -> organizational -> room -> name }}"/>
                            @else
                            <select id="selectRoom" name="room_id" class="form-select mb-3" aria-label="Default select example">
                                    <option value ="-1">Tất Cả Các Phòng</option>
                                    @foreach($rooms as $room)
                                    <option value="{{ $room -> id }}">{{ $room -> name }}</option>
                                    @endforeach
                               
                            </select>

                            @endif

                        </div>
                        <div class="input-group-append" style = "width:50%">
                            <label class="form-label">Bộ phận</label>

                            @if ($staff)
                            <input hidden name ="team_id" id ="team_id" value ="{{ $staff -> organizational -> team -> id }}" />
                            <input disabled class = "form-control mb-3"  style="margin-left:-4%; background:none; border:0cm"  value ="{{ $staff -> organizational -> team -> name }}"/>
                            @else
                            <select id="selectTeam" name="team_id" @if ($staff) style="margin-left:-4%; background:none; border:0cm" @endif
                                class="form-select mb-3" aria-label="Default select example">
                            </select>
                            @endif

                        </div>
                    </div>
                </div>
                <div style = "width:49%">
                    <div class="input-group">
                        <div class="input-group-prepend" style ="width: 50%; margin-left: -3%; margin-right: 2%;">
                            <label class="form-label">Chức danh</label>
                            @if ($staff)
                            <input hidden name ="position_id" id ="position_id" value ="{{ $staff -> organizational -> position -> id }}"/>
                            <input disabled class = "form-control mb-3" style="margin-left:-4%; background:none; border:0cm"  value ="{{ $staff -> organizational -> position -> name }}"/>
                            @else
                            <select id="selectPosition" name="position_id"  class="form-select mb-3" aria-label="Default select example">
                                </select>
                            @endif
                        </div>
                        <div class="input-group-append" style ="width:50%">
                            <label class="form-label">Họ và tên</label>

                            @if ($staff)
                            <input hidden name = "basic_info_id"  id ="basic_info_id" value ="{{ $staff -> id }}"/>
                            <input disabled class = "form-control mb-3" style="margin-left:-4%; background:none; border:0cm"  value ="{{ $staff -> full_name }}"/>
                            @else
                            <select id="selectStaff" name="basic_info_id" class="form-select mb-3" aria-label="Default select example">
                                </select>
                            @endif
                        </div>
                    </div>
                </div>  
            </div>
           
        </div>
    </div>

    <div class="card" style = "margin-top:-1%">
        <div class="card-body overflow-auto">
            <table id="all_plan_day_table" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Phòng</th>
                        <th>Bộ Phận</th>
                        <th>Chức Danh</th>
                        <th>Nhân Viên</th>
                        <th>Ngày</th>
                        <th>Người tạo</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>


@endsection


@push('scripts-planday-index')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script>
     $(document).ready(function() {

        var plan_day_table = $(`#all_plan_day_table`).DataTable({
            responsive: false

        });

        $('#start_date').datepicker({
            locale: 'vi',
            format: 'yyyy-mm-dd',
            todayHighlight: true,
        });

        $('#end_date').datepicker({
            locale: 'vi',
            format: 'yyyy-mm-dd',
            startDate: $("#start_date").val(),
        });

        $('#start_date').on('change', function() {
            if ($(this)[0].value > $('#end_date')[0].value) {
                $('#end_date').datepicker('clearDates');
            }

            $('#end_date').datepicker('destroy');
            $('#end_date').datepicker({
                locale: 'vi',
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                startDate: $(this)[0].value
            });

        });

        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            $('#selectTeam')
                .find('option')
                .remove();
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();

            if (id != '') {
                var url = "{!! route('dayoff.getTeamByRommID', ':id') !!}";

                $.ajax({
                    type: 'GET',
                    url: url.replace(':id', id),
                    success: function(data) {
                        if (data.length > 0) {
                            $('#selectTeam')
                                .find('option')
                                .remove()
                            $('#selectTeam').append($('<option>', {
                                value: -1,
                                text: 'Tất Cả Bộ Phận'
                            }));
                            data.forEach(function(value) {
                                $('#selectTeam').append($('<option>', {
                                    value: value.id,
                                    text: value.name
                                }));
                            })
                        } else {
                            $('#selectTeam')
                                .find('option')
                                .remove()

                            if ($('#selectRoom').val() != -1) {
                                $('#selectTeam').append($('<option>', {
                                    value: -1,
                                    text: 'Tất Cả Bộ Phận'
                                }));
                            }
                        }

                    filterPlanDay();
                    }
                });

            }
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            var url = "{!! route('dayoff.getPositionByRommID', ':id') !!}";
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();
            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectPosition')
                            .find('option')
                            .remove();
                        $('#selectPosition').append($('<option>', {
                            value: -1,
                            text: 'Tất Cả Chức Danh'
                        }));

                        data.forEach(function(value) {
                            $('#selectPosition').append($('<option>', {
                                value: value.id,
                                text: value.name
                            }));
                        })
                    } else {
                        $('#selectPosition')
                            .find('option')
                            .remove();
                        if ($('#selectTeam').val() != -1) {
                            $('#selectPosition').append($('<option>', {
                                value: -1,
                                text: 'Tất Cả Chức Danh'
                            })); 
                        }

                    }
                    filterPlanDay();
                }
            });
        });

        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();

            var url = "{!! route('dayoff.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";


            $('#selectStaff')
                .find('option')
                .remove();

            $.ajax({
                type: 'GET',
                url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectStaff').append($('<option>', {
                            value: -1,
                            text: `Tất Cả Nhân Viên`,

                        }));

                        data.forEach(function(employee) {
                            $('#selectStaff').append($('<option>', {
                                value: employee['id'],
                                text: `${employee['full_name']}`,

                            }));
                        })
                    } else {
                        $('#selectStaff')
                            .find('option')
                            .remove();
                        if ($('#selectPosition').val() != -1) {
                            $('#selectStaff').append($('<option>', {
                                value: -1,
                                text: `Tất Cả Nhân Viên`,

                            }));
                        }

                    }
                    filterPlanDay();
                }
            });

        });

        filterPlanDay();

        $(`#selectStaff, #start_date, #end_date`).on('change', function(){
            filterPlanDay();
        });

        function filterPlanDay() {

            plan_day_table.clear().draw();

            var basic_info_id = 0;
            var room_id = 0;
            var team_id = 0;
            var position_id = 0;

            if ($('#selectStaff')[0] != undefined) {
                basic_info_id   = $(`#selectStaff`).find(":selected").val();
                room_id         = $('#selectRoom').find(":selected").val();        
                team_id         = $('#selectTeam').find(":selected").val();
                position_id     = $('#selectPosition').find(":selected").val();

            } else {
                basic_info_id = $('#basic_info_id').val();
                room_id = $('#room_id').val();
                team_id = $('#team_id').val();
                position_id = $('#position_id').val();

            }

            var start_date  = $('#start_date').val();
            var end_date  = $('#end_date').val();


            $.ajax({
                type: 'GET',
                url: "{{ route('plan_day.filterPlanDay') }}",
                data: {
                    start_date: start_date,
                    end_date : end_date,
                    room_id: room_id,
                    team_id: team_id,
                    position_id: position_id,
                    basic_info_id: basic_info_id,

                },
                success: function(data) {

                    if (data.length > 0) {
                        data.forEach( (plan_day, $index) => {
                            plan_day_table.rows.add([
                                {
                                    "0": $index + 1, 
                                    "1": plan_day['staff']['organizational']['room']['name'], 
                                    "2": plan_day['staff']['organizational']['team']['name'],  
                                    "3": plan_day['staff']['organizational']['position']['name'],  
                                    "4": plan_day['staff']['full_name'], 
                                    "5": plan_day['date'].split('T')[0], 
                                    "6": plan_day['user']['name'], 
                                    "7": getActionInputString(plan_day['id']) 
                                },
                            ]);
                        });
                    }

                    plan_day_table.draw();
                }
            });
        }

        function getActionInputString(plan_day_id) {
            input_str = `
                <div class="dropdown d-inline-block">
                    <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="ri-more-fill align-middle"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-end">
                        @can('plan_day-view')
                        <li><a href="/plan_day/view/${plan_day_id}" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
                        @endcan

                        @can('plan_day-edit')
                        <li><a href="/plan_day/edit/${plan_day_id}" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                        @endcan
                        
                        @can('plan_day-delete')
                        <li><a href="#" id="${plan_day_id}" class="dropdown-item remove-item-btn"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Delete</a>
                        </li>
                        <form method="post" id="formDelete${plan_day_id}" action="/plan_day/delete/${plan_day_id}" style="display:inline">
                            @method('delete')
                            @csrf
                        </form>
                        @endcan
                    </ul>
                </div>
            `;

            return input_str;
           
        }

        $('#all_plan_day_table tbody').on('click', '.remove-item-btn', function(event) {
            const id = $(this)[0].id;
            event.preventDefault();
            swal.fire({
                    title: 'Bạn muốn xóa dữ liệu này ?',
                    text: "Lưu ý : Dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                    icon: 'warning',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Đồng ý',
                    showDenyButton: true,
                    denyButtonText: 'Hủy bỏ',
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        $(`#formDelete${id}`).submit();
                    } else if (result.isDenied) {
                        Swal.fire('Dữ liệu được giữ lại', '', 'info')
                    }
            });

         });
    });
</script>
@endpush

@section('script')
    @stack('scripts-planday-index')
@endsection