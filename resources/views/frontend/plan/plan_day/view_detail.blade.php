@php
$url_logo_left = URL::asset('assets/images/logo-ac.png');
$url_logo_right = URL::asset('assets/images/logo-ac.png');
$planDayX = $dataInit['planDay'];
$staff = $planDayX -> staff;

@endphp

<div class="card">
    <div class="card-body">
        <div class="row justify-content-between">
            <!-- Dark Logo-->
            <div class="col-xl-2">
                <a class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;;">
                    </span>
                </a>
                <!-- Light Logo-->
                <a class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                </a>
            </div>

            <div class="col-xl-8 text-center align-items-center" style="display:grid">
                <h3 style="color:#005091; font-size:170%;">
                    <p>NHẬT KÝ NGÀY</p>
                </h3>
            </div>


            <!-- Dark Logo-->
            <div class="col-xl-2">
                <a class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                </a>
                <!-- Light Logo-->
                <a class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                </a>
            </div>
        </div>

        <div class="row justify-content-start">
            <div style="width:10%">
                <label for="start_date" class="form-label">Ngày</label>
                <input type='text' disabled class='form-control datepicker' value="{{ $planDayX -> date -> format('Y-m-d')  }}">
            </div>
            
        </div>

        <div class="row justify-content-start mt-3" style="width:103.5%">
            <div style="width:50%">
                <div class="input-group">
                    <div class="input-group-prepend" style="margin-right:2%; width:47%;">
                        <label for="borderInput" class="form-label">Phòng</label>
                        <input hidden name="room_id" id="room_id" value="{{ $staff -> organizational -> room -> id }}" />
                        <input disabled class="form-control mb-3" style="margin-left:-4%; background:none; border:0cm" value="{{ $staff -> organizational -> room -> name }}" />
                    </div>
                    <div class="input-group-append" style="width:50%">
                        <label class="form-label">Bộ phận</label>

                        <input hidden name="team_id" id="team_id" value="{{ $staff -> organizational -> team -> id }}" />
                        <input disabled class="form-control mb-3" style="margin-left:-4%; background:none; border:0cm" value="{{ $staff -> organizational -> team -> name }}" />

                    </div>
                </div>
            </div>
            <div style="width:49%">
                <div class="input-group">
                    <div class="input-group-prepend" style="width: 50%; margin-left: -3%; margin-right: 2%;">
                        <label class="form-label">Chức danh</label>
           
                        <input hidden name="position_id" id="position_id" value="{{ $staff -> organizational -> position -> id }}" />
                        <input disabled class="form-control mb-3" style="margin-left:-4%; background:none; border:0cm" value="{{ $staff -> organizational -> position -> name }}" />
                       
                    </div>
                    <div class="input-group-append" style="width:50%">
                        <label class="form-label">Họ và tên</label>
                        <input hidden name="basic_info_id" id="basic_info_id" value="{{ $staff -> id }}" />
                        <input disabled class="form-control mb-3" style="margin-left:-4%; background:none; border:0cm" value="{{ $staff -> full_name }}" />
                    
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class = "row justify-content-center">
    <p class = "text-center" style="color:#005091; font-size:150%;">
        Trước khi rời Công ty, <b>TÔI</b> luôn cập nhật các công việc phải làm ngày mai
    </p>
</div>

<div class = "row justify-content-center" style = "margin-top:1%">
    <div style ="width:50%">
        <div class ="card">
            <div class = "card-header">
                <h5>CÔNG VIỆC PHẢI LÀM</h5>
            </div>
            <div class = "card-body">
                @for ($i = 1; $i  <= (6 > count($planDayX->planDayTimeslots) ? 6 : count($planDayX->planDayTimeslots) ); $i ++)
                @php $planDayTimeslotX = array_key_exists($i-1, $planDayX -> planDayTimeslots->toArray() )  ? $planDayX -> planDayTimeslots[$i-1] : null; @endphp
                <div class = "row">
                    <div style = "width: 85%">
                        <div class="row">
                            <div class ="text-center"  style = "width:10%; margin-top:1%">
                                <span style ="font-size:1.5em">{{ $i }}</span>
                            </div>
                            <div style ="width:90%; margin-bottom:3%">
                                <input disabled type = "text" class = "form-control " value = "{{ $planDayTimeslotX ? $planDayTimeslotX -> title : '' }}">
                            </div>
                        </div>
                    </div>
                    <div style = "width: 15%;">
                        <input disabled type = "checkbox" style ="width:2.35rem; height:2.35rem;" @if($planDayTimeslotX && $planDayTimeslotX -> isDone) checked @endif>
                    </div>
                </div>
                @endfor
                
            </div>
        </div>
    </div>
</div>

<div class = "row justify-content-center" style = "margin-top:-1%">
    <div style = "width:50%;">

        <div class ="card">
            <div class = "card-header">
                <h5>CÁC THÓI QUEN MỚI</h5>
            </div>
            <div class = "card-body">
                @for ($i = 1; $i  <= (3 > count($planDayX->three_new_habits) ? 3 : count($planDayX->three_new_habits) ); $i ++)
        
                    <div class="row">
                        <div class ="text-center"  style = "width:10%; margin-top:1.5%">
                            <span style ="font-size:1.5em">{{ $i }}</span>
                        </div>
                        <div style ="width:90%; margin-bottom:3%">
                            <textarea class ="form-control" row ="1" disabled>{{ array_key_exists($i-1, $planDayX->three_new_habits) ? $planDayX->three_new_habits[$i-1] : '' }}</textarea> 
                        </div>
                    </div>
                @endfor
            </div>
        </div>
    </div>
</div>

<div class = "row justify-content-center" style = "margin-top:-1%">
    <div style = "width:50%;">
        <div class = "card">
            <div class = "card-body">
                <label for ="lesson_main" class = "form-label">BÀI HỌC HÔM NAY CỦA TÔI LÀ</label>
                <input disabled type ="text" class ="form-control mb-3" name ="lesson_main" value = "{{$planDayX -> lesson_main}}"/>

                        
                <label for ="lesson_failure" class = "form-label">ĐIỀU LÀM TÔI MẤT TẬP TRUNG LÀ</label>
                <input disabled type ="text" class ="form-control mb-3" name ="lesson_failure" value = "{{$planDayX -> lesson_failure}}"/>

                        
                <label for ="lesson_success" class = "form-label">VÀ THÀNH CÔNG CỦA TÔI TRONG NGÀY LÀ</label>
                <input disabled type ="text" class ="form-control mb-3" name ="lesson_success" value = "{{$planDayX -> lesson_success}}"/>

                        
                <label for ="lesson_thankful" class = "form-label">ĐIỀU/NGƯỜI LÀM TÔI CẢM THẤY BIẾT ƠN LÀ</label>
                <input disabled type ="text" class ="form-control mb-3" name ="lesson_thankful" value = "{{$planDayX -> lesson_thankful}}"/>
            </div>
        </div>
    </div>
</div>
