<div class="row">
    <div class="col-2 mb-3" style="width:13.9%">
        <a href="{{ route('plan_day.index') }}">
            <button id="returnButton" type="button" class="btn btn-danger waves-effect waves-light">
                <i class="ri-arrow-go-back-line label-icon align-middle fs-16 me-2"></i> Về Danh Sách KH Ngày</button>
        </a>
    </div>

    <div class="col-2 mb-3" style="width:12%; margin-left:-1.5%">
        <a href="{{ route('plan_week.view', $dataInit['planDay'] -> plan_week_id) }}">
            <button id="returnButton" type="button" class="btn btn-secondary waves-effect waves-light">
                <i class="ri-arrow-go-back-line label-icon align-middle fs-16 me-2"></i> Về Kế Hoạch Tuần</button>
        </a>
    </div>

    <div style= width:65%></div>

    @if ($dataInit['modeEdit']) 
    <div class="col-2 mb-3" style = "width:10.6%">
        <a href = "{{ route('plan_day.view', $dataInit['planDay'] -> id) }}">
        <button id="returnButton" type="button" class="btn btn-success waves-effect waves-light">
                <i class="ri-arrow-go-back-line label-icon align-middle fs-16 me-2"></i> Về Chế Độ Xem</button>
        </a>
    </div>

    @else 
    <div class="col-2 mb-3" style = "width:10.5%">
        <a href = "{{ route('plan_day.edit', $dataInit['planDay'] -> id) }}">
        <button id="returnButton" type="button" class="btn btn-success waves-effect waves-light">
                <i class="ri-arrow-go-back-line label-icon align-middle fs-16 me-2"></i> Về Chế Độ Sửa</button>
        </a>
    </div>
    @endif
</div>