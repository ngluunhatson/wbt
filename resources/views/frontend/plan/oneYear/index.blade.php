@extends('layouts.fe.master2')
@section('title') @lang('translation.starter') @endsection
@section('content')
<div class="row">
 <div class="col-12 d-flex justify-content-between mb-3">
  <h3 class="d-inline m-0">Kế hoạch 1 năm</h3>
  <a href="/trackingdayoff/create">
   <button type="button" class="btn btn-success btn-label waves-effect waves-light"><i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm</button>
  </a>
 </div>
</div>
<div class="row">
 <div class="col-lg-12">
  <div class="card">
   <div class="card-body">
    <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
     <thead>
      <tr>
       <th>STT</th>
       <th>Mã nhân viên</th>
       <th>Họ tên</th>
       <th>Phòng</th>
       <th>Bộ phận</th>
       <th>Chức danh</th>
       <th>Thời gian thử việc</th>
       <th>Action</th>
      </tr>
     </thead>
     <tbody>
      <tr>
       <td>01</td>
       <td>VLZ1400087402</td>
       <td>Post launch reminder/ post list</td>
       <td>Joseph Parker</td>
       <td>Joseph Parker</td>
       <td>03 Oct, 2021</td>
       <td>High</td>
       <td>
        <a href="/plan/oneyear/edit" class="btn bg-light m-auto d-block"><i class="ri-pencil-fill align-bottom text-muted"></i></a>
       </td>
      </tr>
     </tbody>
    </table>
   </div>
  </div>
 </div>
 <!--end col-->
</div>
<!--end row-->

@endsection
@section('script')
<script>
 $(function() {
  var myTable = $('#example').dataTable();

  $("#checkAll").click(function() {
   $(".form-check-input").prop('checked', $(this).prop("checked"));
  });
 })
</script>
@endsection