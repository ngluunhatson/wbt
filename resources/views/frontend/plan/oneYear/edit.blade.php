@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('content')
<div class="card">
  <div class="card-header">
    <div class="col-12 d-flex justify-content-between">
      <h3 class="d-inline m-0">Kế hoạch 1 năm</h3>
      <a href="/trackingdayoff/create">
        <button type="button" class="btn btn-success btn-label waves-effect waves-light"><i class="ri-save-line label-icon align-middle fs-16 me-2"></i>Lưu</button>
      </a>
    </div>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-xl-4">
        <label for="borderInput" class="form-label">Họ và tên</label>
        <select class="form-select mb-3" aria-label=".form-select-lg example">
          <option selected>Open this select menu</option>
          <option value="1">One</option>
          <option value="2">Two</option>
          <option value="3">Three</option>
        </select>
      </div>
      <div class="col-xl-4">
        <label for="borderInput" class="form-label">Phòng</label>
        <select class="form-select mb-3" aria-label=".form-select-lg example">
          <option selected>Open this select menu</option>
          <option value="1">One</option>
          <option value="2">Two</option>
          <option value="3">Three</option>
        </select>
      </div>
      <div class="col-xl-4">
        <label for="borderInput" class="form-label">Bộ phận</label>
        <select class="form-select mb-3" aria-label=".form-select-lg example">
          <option selected>Open this select menu</option>
          <option value="1">One</option>
          <option value="2">Two</option>
          <option value="3">Three</option>
        </select>
      </div>
      <div class="col-xl-4">
        <label for="borderInput" class="form-label">Chức danh</label>
        <select class="form-select mb-3" aria-label=".form-select-lg example">
          <option selected>Open this select menu</option>
          <option value="1">One</option>
          <option value="2">Two</option>
          <option value="3">Three</option>
        </select>
      </div>
      <div class="col-xl-4">
        <label for="exampleInputdate" class="form-label">Thời gian thử việc</label>
        <input type="date" class="form-control" id="exampleInputdate">
      </div>
      <div class="col-xl-4">
        <label for="exampleInputdate" class="form-label">Đến</label>
        <input type="date" class="form-control" id="exampleInputdate">
      </div>
    </div>
  </div>
</div>


<div class="card">
  <div class="card-header">
    <h3>CHƯƠNG TRÌNH ĐÀO TẠO CBD FIRM</h3>
  </div>
  <div class="card-body">
    <!-- Collapse -->
    <div class="row gy-4">
      @php $i = 1; @endphp
      @foreach(config('header.cbdfirm') as $key => $value)
      <div class="col-12">
        <div class="accordion-item">
          <h2 class="accordion-header">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#s1-child-{{$i}}" aria-expanded="false" aria-controls="s1-child-{{$i}}">
              {{ $i }}. {{ $key }}
            </button>
          </h2>
          <div id="s1-child-{{$i}}" class="accordion-collapse collapse">
            <div class="accordion-body">
              @php $j = 0; @endphp
              @foreach ($value as $a)
              <div class="card">
                <div class="card-header">
                  <h4 class="m-0">{{ $a['title'] }}</h4>
                </div>
                <div class="card-body">
                  <div class="row align-items-end">
                    <div class="col-xl-2">
                      <label class="form-label text-center">Thời gian thực hiện</label>
                      <textarea name="{{ $a['name'] }}_1" class="form-control" id="exampleFormControlTextarea5" rows="2"></textarea>
                    </div>
                    <div class="col-xl-5">
                      <label class="form-label text-center">Bạn đã học được những gì</label>
                      <textarea name="{{ $a['name'] }}_2" class="form-control" id="exampleFormControlTextarea5" rows="2"></textarea>
                    </div>
                    <div class="col-xl-5">
                      <label class="form-label text-center">Điều bạn áp dụng ngay là gì</label>
                      <textarea name="{{ $a['name'] }}_3" class="form-control" id="exampleFormControlTextarea5" rows="2"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              @php $j++; @endphp
              @endforeach
            </div>
          </div>
        </div>
      </div>
      @php $i++; @endphp
      @endforeach
    </div>
  </div>
</div>

@endsection
@section('script')
<script src="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/form-pickers.init.js') }}"></script>
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection