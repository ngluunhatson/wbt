@extends('layouts.fe.master2')

@section('title')
@lang('translation.starter')
@endsection

@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

@endsection



@section('content')

    @include('frontend.plan.plan_quarter.index_detail', ['dataInit' => $dataInit])

@endsection


@section('script')
    @stack('scripts-plan_quarter-index_detail')
@endsection