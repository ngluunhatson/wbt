

@php
$url_logo_left = URL::asset('assets/images/logo-ac.png');
$url_logo_right = URL::asset('assets/images/logo-ac.png');
$rooms = $dataInit['rooms'];
@endphp

@php
    $day_int = intval(now() -> format('d'));
    $month_int = intval(now() -> format('m'));
    $year_int = intval(now() -> format('Y'));

    $isLeapYear = ($year_int % 4 == 0) && ( ($year_int % 100 != 0) || ($year_int % 400 ==0) );
    $month_day_list = [31, ($isLeapYear ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];


    $current_total_day_int = 0;
    for ($i = 0; $i <= ($month_int-2); $i++)
        $current_total_day_int += $month_day_list[$i];
    
    $current_total_day_int += $day_int;

    $current_week_int = intval(($current_total_day_int - 1) / 7) + 1;

    $current_quarter_int  = intval(($current_week_int - 1) / 13) + 1; 

    $staff = auth() -> user() -> staff;

@endphp

<div class="card">
    <div class="card-body">
        <div class="row justify-content-start">
            <!-- Dark Logo-->

            <div class = "col-xl-2">
                <a class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;;">
                    </span>
                </a>
            <!-- Light Logo-->
                <a class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                </a>
            </div>
            <div class = "col-xl-8 text-center align-items-center" style = "display:grid">   
                <h3 style="color:#005091; font-size:170%;">
                    <p class = "text-center">DANH SÁCH KẾ HOẠCH HÀNH ĐỘNG CỦA CÔNG TY</p>
                </h3>
            </div>


            <!-- Dark Logo-->
            <div class = "col-xl-2">
                <a class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                </a>
                <!-- Light Logo-->
                <a class="logo logo-light" >
                    <span class="logo-sm">
                        <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                </a>
            </div>
        </div>

        <div class="row justify-content-between" style="margin-top:1%; height:70px">
            <div class="input-group" style="width:20%;">
                <div class="input-group-prepend" style ="width:30%; margin-right:2%">
                    <label for="year" class="form-label">Năm</label>
                    <input name="year" type="text" class="form-control datepicker" 
                        id="year_datepicker" value="{{ old('year', now() -> format('Y')) }}">
                </div>
                <div class="input-group-append">
                    <label for="quarter" class="form-label">Quý</label>
                    <select name="quarter" type="text" class="form-select" id="quarter_select">
                        <option value = "-1">Tất Cả</option>
                        @for ($i = 1; $i <= 4; $i ++) 
                        <option @if(intval(old('quarter')) == $i || $current_quarter_int == $i) selected @endif
                            value="{{ $i }}">{{   $i == 1 ? 'I' :  ($i == 2 ? 'II' : ($i == 3 ? 'III' : 'IV'))  }}
                        </option>
                        @endfor
                    </select>
                </div>
            </div>

            @can('plan_quarter-create')
            <a href="/plan_quarter/create" style ="width:10%">
                <button style="margin-top:15%; margin-left:15%" type="button" class="btn btn-success btn-label waves-effect waves-light"><i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm</button>
            </a>
            @endcan
        </div>

        <div class="row justify-content-start mt-3" style = "width:103.5%">
            <div style = "width:50%">
                <div class="input-group">
                    <div class="input-group-prepend" style ="margin-right:2%; width:47%;">
                        <label for="borderInput" class="form-label">Phòng</label>

                        @if ($staff && $staff -> rank < 7) 
                        <input hidden name ="room_id" id ="room_id-input" value = "{{ $staff -> organizational -> room -> id }}">
                        <input disabled class ="form-control mb-3"  value = "{{ $staff -> organizational -> room -> name }}">

                        @else 
                        <select id="selectRoom" name="room_id" class="form-select mb-3" aria-label="Default select example">
                            <option value ="-1">Tất Cả Các Phòng</option>
                            @foreach($rooms as $room)
                            <option value="{{ $room -> id }}">{{ $room -> name }}</option>
                            @endforeach
                        </select>
                        @endif

                    </div>
                    <div class="input-group-append" style = "width:50%">
                        <label class="form-label">Bộ phận</label>

                        @if ($staff &&  $staff -> rank < 6) 
                        <input hidden name ="team_id" id ="team_id-input" value = "{{ $staff -> organizational -> team -> id }}">
                        <input disabled class ="form-control mb-3"  value = "{{ $staff -> organizational -> team -> name }}">

                        @else 
                        <select id="selectTeam" name="team_id" class="form-select mb-3" aria-label="Default select example">
                        </select>
                        @endif
                        
                    </div>
                </div>
            </div>
            <div style = "width:49%">
                <div class="input-group">
                    <div class="input-group-prepend" style ="width: 50%; margin-left: -3%; margin-right: 2%;">
                        <label class="form-label">Chức danh</label>
                        <select id="selectPosition" name="position_id" class="form-select mb-3" aria-label="Default select example">
                        </select>
                    </div>
                    <div class="input-group-append" style ="width:50%">
                        <label class="form-label">Họ và tên</label>
                        <select id="selectStaff" name="basic_info_id" class="form-select mb-3" aria-label="Default select example">
                        </select>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>

<div class="card" style = "margin-top:-1%">
    <div class="card-body overflow-auto">
        <table id="all_plan_quarter_table" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Phòng</th>
                    <th>Bộ Phận</th>
                    <th>Chức Danh</th>
                    <th>Nhân Viên</th>
                    <th>Năm</th>
                    <th>Quý</th>
                    <th>Người tạo</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            
            </tbody>
        </table>
    </div>
</div>


@push('scripts-plan_quarter-index_detail')
    <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            var plan_quarter_table = $(`#all_plan_quarter_table`).DataTable({
                responsive: false

            });
        
            $('#year_datepicker').datepicker({
                locale: 'vi',
                format: 'yyyy',
                startView: "years",
                minViewMode: "years",
            });


            $(`#selectStaff, #quarter_select, #year_datepicker`).on('change', function(){
                filterPlanQuarter();
            });

            $('#selectRoom').on('change', function() {
                var id = $('#selectRoom').find(":selected").val();
                $('#selectTeam')
                    .find('option')
                    .remove();
                $('#selectPosition')
                    .find('option')
                    .remove();
                $('#selectStaff')
                    .find('option')
                    .remove();

                if (id != '') {
                    var url = "{!! route('dayoff.getTeamByRommID', ':id') !!}";

                    $.ajax({
                        type: 'GET',
                        url: url.replace(':id', id),
                        success: function(data) {
                            if (data.length > 0) {
                                $('#selectTeam')
                                    .find('option')
                                    .remove()
                                $('#selectTeam').append($('<option>', {
                                    value: -1,
                                    text: 'Tất Cả Bộ Phận'
                                }));
                                data.forEach(function(value) {
                                    $('#selectTeam').append($('<option>', {
                                        value: value.id,
                                        text: value.name
                                    }));
                                })
                            } else {
                                $('#selectTeam')
                                    .find('option')
                                    .remove()

                                if ($('#selectRoom').val() != -1) {
                                    $('#selectTeam').append($('<option>', {
                                        value: -1,
                                        text: 'Tất Cả Bộ Phận'
                                    }));
                                }
                            }

                        filterPlanQuarter();
                        }
                    });

                }
            });

            $('#selectTeam').on('change', function() {
                var id = $('#selectTeam').find(":selected").val();
                var url = "{!! route('dayoff.getPositionByRommID', ':id') !!}";
                $('#selectPosition')
                    .find('option')
                    .remove();
                $('#selectStaff')
                    .find('option')
                    .remove();
                $.ajax({
                    type: 'GET',
                    url: url.replace(':id', id),
                    success: function(data) {
                        if (data.length > 0) {
                            $('#selectPosition')
                                .find('option')
                                .remove();
                            $('#selectPosition').append($('<option>', {
                                value: -1,
                                text: 'Tất Cả Chức Danh'
                            }));

                            data.forEach(function(value) {
                                $('#selectPosition').append($('<option>', {
                                    value: value.id,
                                    text: value.name
                                }));
                            })
                        } else {
                            $('#selectPosition')
                                .find('option')
                                .remove();
                            if ($('#selectTeam').val() != -1) {
                                $('#selectPosition').append($('<option>', {
                                    value: -1,
                                    text: 'Tất Cả Chức Danh'
                                })); 
                            }

                        }
                        filterPlanQuarter();
                    }
                });
            });

            $('#selectPosition').on('change', function() {
                var room_id = 0;
                var team_id = 0;

                if ($('#room_id-input')[0] != undefined) 
                    room_id = $('#room_id-input').val();
                else
                    room_id       = $('#selectRoom').find(":selected").val();   

                if ($('#team_id-input')[0] != undefined) 
                    team_id = $('#team_id-input').val();
                else
                    team_id = $('#selectTeam').find(":selected").val();

                var position_id = $('#selectPosition').find(":selected").val();
                var url = "{!! route('dayoff.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";


                $('#selectStaff')
                    .find('option')
                    .remove();

                $.ajax({
                    type: 'GET',
                    url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
                    success: function(data) {
                        if (data.length > 0) {
                            $('#selectStaff')
                                .find('option')
                                .remove()
                            $('#selectStaff').append($('<option>', {
                                value: -1,
                                text: `Tất Cả Nhân Viên`,

                            }));

                            data.forEach(function(employee) {
                                $('#selectStaff').append($('<option>', {
                                    value: employee['id'],
                                    text: `${employee['full_name']}`,

                                }));
                            })
                        } else {
                            $('#selectStaff')
                                .find('option')
                                .remove();
                            if ($('#selectPosition').val() != -1) {
                                $('#selectStaff').append($('<option>', {
                                    value: -1,
                                    text: `Tất Cả Nhân Viên`,

                                }));
                            }

                        }
                        filterPlanQuarter();
                    }
                });

            });

            initializeComboBox();
            
            function getActionInputString(plan_quarter_id) {
                input_str = `
                    <div class="dropdown d-inline-block">
                        <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="ri-more-fill align-middle"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-end">
                            @can('plan_quarter-view')
                            <li><a href="/plan_quarter/view/${plan_quarter_id}" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
                            @endcan

                            @can('plan_quarter-view')
                            <li><a href="/plan_quarter/view/${plan_quarter_id}" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                            @endcan
                            
                            @can('plan_quarter-delete')
                            <li><a href="#" id="${plan_quarter_id}" class="dropdown-item remove-item-btn"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Delete</a>
                            </li>
                            <form method="post" id="formDelete${plan_quarter_id}" action="/plan_quarter/delete/${plan_quarter_id}" style="display:inline">
                                @method('delete')
                                @csrf
                            </form>
                            @endcan
                        </ul>
                    </div>
                `;

                return input_str;
            }

            function filterPlanQuarter() {
                plan_quarter_table.clear().draw();

                var quarter       = $('#quarter_select').find(":selected").val();        
                var year          = $('#year_datepicker')[0].value;
                var basic_info_id = $(`#selectStaff`).find(":selected").val();

                var room_id = 0;
                var team_id = 0;

                if ($('#room_id-input')[0] != undefined) 
                    room_id = $('#room_id-input').val();
                else
                    room_id = $('#selectRoom').find(":selected").val();   

                if ($('#team_id-input')[0] != undefined) 
                    team_id = $('#team_id-input').val();
                else
                    team_id = $('#selectTeam').find(":selected").val();   

                var position_id   = $('#selectPosition').find(":selected").val();
                
                $.ajax({
                    type: 'GET',
                    url: "{{ route('plan_quarter.filterPlanQuarter') }}",
                    data: {
                        quarter : quarter,
                        year: year,
                        room_id: room_id,
                        team_id: team_id,
                        position_id: position_id,
                        basic_info_id: basic_info_id,

                    },
                    success: function(data) {
                        if (data.length > 0) {
                            data.forEach( (plan_quarter, $index) => {
                                plan_quarter_table.rows.add([
                                    {
                                        "0": $index + 1, 
                                        "1": plan_quarter['staff']['organizational']['room']['name'], 
                                        "2": plan_quarter['staff']['organizational']['team']['name'],  
                                        "3": plan_quarter['staff']['organizational']['position']['name'],  
                                        "4": plan_quarter['staff']['full_name'], 
                                        "5": plan_quarter['year'], 
                                        "6": plan_quarter['quarter'], 
                                        "7": plan_quarter['user']['name'], 
                                        "8": getActionInputString(plan_quarter['id']) 
                                    },
                                ]);
                            });
                        }

                        plan_quarter_table.draw();
                    }
                });
            }

            function initializeComboBox() {
                if ($('#selectRoom')[0] == undefined && $('#selectTeam')[0] != undefined) {
                    var id = room_id = $('#room_id-input').val();
                    if (id != '') {
                        var url = "{!! route('dayoff.getTeamByRommID', ':id') !!}";
                        $.ajax({
                            type: 'GET',
                            url: url.replace(':id', id),
                            success: function(data) {
                                if (data.length > 0) {
                                    $('#selectTeam')
                                        .find('option')
                                        .remove()
                                    $('#selectTeam').append($('<option>', {
                                        value: -1,
                                        text: 'Tất Cả Bộ Phận'
                                    }));
                                    data.forEach(function(value) {
                                        $('#selectTeam').append($('<option>', {
                                            value: value.id,
                                            text: value.name
                                        }));
                                    })
                                } else {
                                    $('#selectTeam')
                                        .find('option')
                                        .remove()

                                    if ($('#selectRoom').val() != -1) {
                                        $('#selectTeam').append($('<option>', {
                                            value: -1,
                                            text: 'Tất Cả Bộ Phận'
                                        }));
                                    }
                                }

                            }
                        });

                    }
                }

                if ($('#selectTeam')[0] == undefined) {
                    var id = $('#team_id-input').val();
                    if (id != '') {
                        var url = "{!! route('dayoff.getPositionByRommID', ':id') !!}";
                        $('#selectPosition')
                            .find('option')
                            .remove();
                        $('#selectStaff')
                            .find('option')
                            .remove();
                        $.ajax({
                            type: 'GET',
                            url: url.replace(':id', id),
                            success: function(data) {
                                if (data.length > 0) {
                                    $('#selectPosition')
                                        .find('option')
                                        .remove();
                                    $('#selectPosition').append($('<option>', {
                                        value: -1,
                                        text: 'Tất Cả Chức Danh'
                                    }));

                                    data.forEach(function(value) {
                                        $('#selectPosition').append($('<option>', {
                                            value: value.id,
                                            text: value.name
                                        }));
                                    })
                                } else {
                                    $('#selectPosition')
                                        .find('option')
                                        .remove();
                                    if ($('#selectTeam').val() != -1) {
                                        $('#selectPosition').append($('<option>', {
                                            value: -1,
                                            text: 'Tất Cả Chức Danh'
                                        })); 
                                    }

                                }
                              
                            }
                        });
                    }
                }
                filterPlanQuarter();
            }

            $('#all_plan_quarter_table tbody').on('click', '.remove-item-btn', function(event) {
                const id = $(this)[0].id;
                event.preventDefault();
                swal.fire({
                        title: 'Bạn muốn xóa dữ liệu này ?',
                        text: "Lưu ý : Dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                        icon: 'warning',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Đồng ý',
                        showDenyButton: true,
                        denyButtonText: 'Hủy bỏ',
                    })
                    .then((result) => {
                        if (result.isConfirmed) {
                            $(`#formDelete${id}`).submit();
                        } else if (result.isDenied) {
                            Swal.fire('Dữ liệu được giữ lại', '', 'info')
                        }
                });
            });
        });
    </script>
@endpush