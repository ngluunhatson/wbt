@extends('layouts.fe.master2')

@section('title')
@lang('translation.starter')
@endsection

@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection

@php
$url_logo_left = URL::asset('assets/images/logo-ac.png');
$url_logo_right = URL::asset('assets/images/logo-ac.png');
$rooms = $dataInit['rooms'];
@endphp


@php
    $day_int = intval(now() -> format('d'));
    $month_int = intval(now() -> format('m'));
    $year_int = intval(now() -> format('Y'));

    $isLeapYear = ($year_int % 4 == 0) && ( ($year_int % 100 != 0) || ($year_int % 400 ==0) );
    $month_day_list = [31, ($isLeapYear ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];


    $current_total_day_int = 0;
    for ($i = 0; $i <= ($month_int-2); $i++)
        $current_total_day_int += $month_day_list[$i];
    
    $current_total_day_int += $day_int;

    $current_week_int = intval(($current_total_day_int - 1) / 7) + 1;

    $current_quarter_int  = intval(($current_week_int - 1) / 13) + 1; 

@endphp

@section('content')

<style> 
.table {
    table-layout: fixed;
}

.table th {
    text-align: center;
    vertical-align: middle;
}

.table td {
    text-align: center;

}
</style>

<form id="formInsert" method="POST" action="{{ route('plan_quarter.storeInsert') }}">
    @csrf
    <div class="card">
        <div class="card-body">
            <div class="row justify-content-between">
                <!-- Dark Logo-->
                <a class="logo logo-dark" style="width:200px; margin-left:2%;">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_left }}" alt="" height="22" style="width:200px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_left }}" alt="" height="17" style="width:200px;">
                    </span>
                </a>
                <!-- Light Logo-->
                <a class="logo logo-light" style="width:200px; margin-left:2%;">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_left }}" alt="" height="22" style="width:200px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 15px;margin-top: 15px;height: 60px; width:200px;">
                    </span>
                </a>

                <h3 style="color:#005091; font-size:170%; width:45%;">
                    <p style ="margin-top: 7.8%;margin-left: 4.7%;">KẾ HOẠCH HÀNH ĐỘNG DÀNH CHO NHÂN VIÊN</p>
                </h3>


                <!-- Dark Logo-->
                <a class="logo logo-dark" style="width:200px; margin-right:3%;">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_right }}" alt="" height="22" style="width:200px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_right }}" alt="" height="17" style="width:200px;">
                    </span>
                </a>
                <!-- Light Logo-->
                <a class="logo logo-light" style="width:200px; margin-right:3%;">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_right }}" alt="" height="22" style="width:200px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 15px;margin-top: 15px;height: 60px; width:200px;">
                    </span>
                </a>
            </div>
            <div class="row justify-content-between" style="margin-top:1%; height:70px">
                <div class="input-group" style="width:20%;">
                    <div class="input-group-prepend" style ="width:30%; margin-right:2%">
                        <label for="year" class="form-label">Năm</label>
                        <input name="year" type="text" class="form-control datepicker @error('year') is-invalid @enderror" 
                            id="year_datepicker" value="{{ old('year', now() -> format('Y')) }}">
                        @error('year')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    <div class="input-group-append">
                        <label for="quarter" class="form-label">Quý</label>
                        <select name="quarter" type="text" class="form-select @error('quarter') is-invalid @enderror" id="quarter_select">
                            @for ($i = 1; $i <= 4; $i ++) 
                            <option @if(intval(old('quarter')) == $i || $current_quarter_int == $i) selected @endif
                                value="{{ $i }}">{{   $i == 1 ? 'I' :  ($i == 2 ? 'II' : ($i == 3 ? 'III' : 'IV'))  }}
                            </option>
                            @endfor
                        </select>
                        @error('quarter')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <a style="width:9%;" href="#" noref>
                    <button type="button" id="savePlanQuarter" class="btn btn-success btn-label waves-effect waves-light">
                        <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Lưu</button>
                </a>


            </div>
          
            <div class="row justify-content-start mt-3" style = "width:103.5%">
                <div style = "width:50%">
                    <div class="input-group">
                        <div class="input-group-prepend" style ="margin-right:2%; width:47%;">
                            <label for="borderInput" class="form-label">Phòng</label>
                            <select id="selectRoom" name="room_id" class="form-select mb-3" aria-label="Default select example">
                                <option value ="-1">Tất Cả Các Phòng</option>
                                @foreach($rooms as $room)
                                <option value="{{ $room -> id }}">{{ $room -> name }}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="input-group-append" style = "width:50%">
                            <label class="form-label">Bộ phận</label>
                            <select id="selectTeam" name="team_id" class="form-select mb-3" aria-label="Default select example">
                            </select>
                        </div>
                    </div>
                </div>
                <div style = "width:49%">
                    <div class="input-group">
                        <div class="input-group-prepend" style ="width: 50%; margin-left: -3%; margin-right: 2%;">
                            <label class="form-label">Chức danh</label>
                            <select id="selectPosition" name="position_id" class="form-select mb-3" aria-label="Default select example">
                            </select>
                        </div>
                        <div class="input-group-append" style ="width:50%">
                            <label class="form-label">Họ và tên</label>
                            <select id="selectStaff" name="basic_info_id" class="form-select mb-3 @error('basic_info_id') is-invalid @enderror" aria-label="Default select example">
                            </select>
                            @error('basic_info_id')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>  
            </div>

            <div class ="row justify-content-start" style = "width:102.3%">
                <label for="quarter_goal" class="form-label">Mục Tiêu Quý</label>
                <textarea name="quarter_goal" class="form-control @error('quarter_goal') is-invalid @enderror" 
                        rows="3" style ="margin-left:1%; width:98%">{{ old('quarter_goal') }}</textarea>
                @error('quarter_goal')
                <span class="text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

        </div>
    </div>

    <div class="card" style ="margin-top:-1.2%">
        <div class="card-body overflow-auto">
            <table id="plan_quarter_table" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:250%">
                <thead>
                    <tr>
                        <th rowspan="2" style = "width:4%">Mục Tiêu #</th>
                        @for ($i = 1; $i <= 13; $i++) 
                            <th colspan="3">     
                                <label for="borderInput" class="form-label fw-bold">Tuần {{$i}}</label>
                                <br>
                                <label for="borderInput" class="form-label" style="white-space: nowrap; font-weight:200">
                                    <span id = "week{{$i}}-from_date-span"></span> - <span id = "week{{$i}}-to_date-span"></span>
                                </label>

                                <input hidden name = "week{{ $i }}-from_date" id ="week{{ $i }}-from_date-input">
                                <input hidden name = "week{{ $i }}-to_date" id ="week{{ $i }}-to_date-input">

                            </th>
                        @endfor
                    </tr>
                </thead>
                <tbody>
                @for($g = 1; $g <= 5; $g++)
                    <tr>
                        <td>Mục Tiêu {{ $g }}</td>
                        @for ($i = 1; $i <= 13; $i++) 
                        <td colspan ="3">
                            <textarea type='text' name="week{{ $i }}-goal{{ $g }}" 
                                class="form-control">{{ old('week'.$i.'-goal'.$g) }} </textarea>
                        </td>
                        @endfor
                    </tr>
                    @endfor
                </tbody>
            </table>
        </div>
    </div>


</form>

@endsection


@section('script')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>

<script>
     $(document).ready(function() {
        calculateDates();

        $(`#savePlanQuarter`).on('click', function(e) {
            e.preventDefault();
            var url = "{!! route('plan_quarter.checkIfExistPlanQuarter') !!}";

            $.ajax({
                method: 'POST',
                url: url,
                data: $('#formInsert').serialize(),
                success: function(response) {
                    if (response.success)
                        $('#formInsert').submit();
                    else {
                        swal.fire({
                            icon: 'error',
                            title: `${response.message}`,
                            confirmButtonColor: '#3085d6',
                            showConfirmButton: false,
                        });
                    }

                }
            });
        });
      
        $('#year_datepicker').datepicker({
            locale: 'vi',
            format: 'yyyy',
            startView: "years",
            minViewMode: "years",
        }); 
    });
</script>

<script>
     $(document).ready(function() {
        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            $('#selectTeam')
                .find('option')
                .remove();
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();

            if (id != '') {
                var url = "{!! route('dayoff.getTeamByRommID', ':id') !!}";

                $.ajax({
                    type: 'GET',
                    url: url.replace(':id', id),
                    success: function(data) {
                        if (data.length > 0) {
                            $('#selectTeam')
                                .find('option')
                                .remove()
                            $('#selectTeam').append($('<option>', {
                                value: 0,
                                text: 'Không Chọn'
                            }));
                            data.forEach(function(value) {
                                $('#selectTeam').append($('<option>', {
                                    value: value.id,
                                    text: value.name
                                }));
                            })
                        } else {
                            $('#selectTeam')
                                .find('option')
                                .remove()

                            if ($('#selectRoom').val() != 0) {
                                $('#selectTeam').append($('<option>', {
                                    value: 0,
                                    text: 'Không Chọn'
                                }));
                            }
                        }
                    }
                });
            }
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            var url = "{!! route('dayoff.getPositionByRommID', ':id') !!}";
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();
            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectPosition')
                            .find('option')
                            .remove();
                        $('#selectPosition').append($('<option>', {
                            value: 0,
                            text: 'Không Chọn'
                        }));

                        data.forEach(function(value) {
                            $('#selectPosition').append($('<option>', {
                                value: value.id,
                                text: value.name
                            }));
                        })
                    } else {
                        $('#selectPosition')
                            .find('option')
                            .remove();
                        $('#selectPosition').append($('<option>', {
                            value: 0,
                            text: 'Không Chọn'
                        }));

                    }
                }
            });
        });

        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();

            var url = "{!! route('dayoff.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";


            $('#selectStaff')
                .find('option')
                .remove();

            $.ajax({
                type: 'GET',
                url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectStaff').append($('<option>', {
                            value: 0,
                            text: `Không Có`,

                        }));

                        data.forEach(function(employee) {
                            $('#selectStaff').append($('<option>', {
                                value: employee['id'],
                                text: `${employee['full_name']}`,

                            }));
                        })
                    } else {
                        $('#selectStaff')
                            .find('option')
                            .remove();

                        $('#selectStaff').append($('<option>', {
                            value: 0,
                            text: `Không Có`,

                        }));


                    }
                }
            });
        });

        $(`#quarter_select, #year_datepicker`).on('change', function() {
            calculateDates();
        });
    });
    
</script>


<script>
    function calculateDates() {
        var quarter = parseInt($('#quarter_select').find(":selected").val());
        var year    = parseInt($('#year_datepicker')[0].value) 
        
        const fixed_date    = new Date('2022-10-03');
        const fixed_year    = 2022;
        const fixed_quarter = 4;

        const diff_year         = year - fixed_year;
        let diff_quarter        = 0;
        let buffer_diff_quarter = diff_year <= 0 ? -1 : 1;

        diff_quarter = fixed_quarter - ( 4 * diff_year +  quarter);
        
        if (diff_year > 0) 
            diff_quarter = -diff_quarter;        

        const calculated_date = fixed_date;

        calculated_date.setDate(calculated_date.getDate() + (diff_quarter * buffer_diff_quarter * 13 * 7) );

        for (let w = 1; w <= 13; w++) {
            if (quarter > 0) {
                let date_str = new Intl.DateTimeFormat('en-GB').format(calculated_date);
                let date_str_input = calculated_date.toISOString().slice(0,10);
           
                $(`#week${w}-from_date-span`).text(date_str);
                $(`#week${w}-from_date-input`).val(date_str_input);
    
                calculated_date.setDate(calculated_date.getDate() + 6);

                date_str = new Intl.DateTimeFormat('en-GB').format(calculated_date);
                date_str_input = calculated_date.toISOString().slice(0,10);

                $(`#week${w}-to_date-span`).text(date_str);
                $(`#week${w}-to_date-input`).val(date_str_input);


                calculated_date.setDate(calculated_date.getDate() + 1);
            } else {
                $(`#week${w}-from_date-span`).text('');
                $(`#week${w}-from_date-input`).val(null);
                $(`#week${w}-to_date-span`).text('');
                $(`#week${w}-to_date-input`).val(null);

            }
        }
    }
</script>

@endsection