@extends('layouts.fe.master2')

@section('title')
@lang('translation.starter')
@endsection

@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection


@section('content')

<style>

    .table th {
        text-align: center;
        vertical-align: middle;
    }

    .table td {
        text-align: center;

    }
</style>

@php

    $day_int = intval(now() -> format('d'));
    $month_int = intval(now() -> format('m'));
    $year_int = intval(now() -> format('Y'));

    $isLeapYear = ($year_int % 4 == 0) && ( ($year_int % 100 != 0) || ($year_int % 400 ==0) );
    $month_day_list = [31, ($isLeapYear ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];


    $current_total_day_int = 0;
    for ($i = 0; $i <= ($month_int-2); $i++)
        $current_total_day_int += $month_day_list[$i];
    
    $current_total_day_int += $day_int;

    $current_week_int = intval(($current_total_day_int - 1) / 7) + 1;

    $current_quarter_int  = intval(($current_week_int - 1) / 13) + 1; 


    $url_logo_left = URL::asset('assets/images/logo-ac.png');
    $url_logo_right = URL::asset('assets/images/logo-ac.png');

    $planQuarterX = null;
    $staff = auth() -> user() -> staff;

    if ($dataInit['planQuarter']) {
        $planQuarterX = $dataInit['planQuarter'];
        $staff = $planQuarterX -> staff;
    }


    $indexView = $dataInit['indexView'];

@endphp

@if($indexView && $staff && $staff -> rank > 3) 
<ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-bs-toggle="tab" href="#my_plan_quarter" role="tab" aria-selected="false">
            Kế Hoạch Quý Của Tôi
        </a>
    </li>
    
    <li class="nav-item">
        <a class="nav-link @if(!$staff) active @endif" data-bs-toggle="tab" href="#team_plan_quarter" role="tab" aria-selected="false">
            Kế Hoạch Quý Của Team
        </a>
    </li>
</ul>
@endif

@if($indexView && $staff && $staff -> rank > 3)
<div class="tab-content text-muted">
    <div class="tab-pane active" id="my_plan_quarter" role="tabpanel">
@endif
        <form id="formUpdate" method="POST" action="{{ route('plan_quarter.storeUpdate') }}">
            @csrf
            <input hidden name = 'plan_quarter_id' id = 'plan_quarter_id' 
                @if ($planQuarterX) value ="{{$planQuarterX -> id}}" @endif>
            <div class="card">
                <div class="card-body">
                    <div class="row justify-content-between">
                        <!-- Dark Logo-->
                        <div class = "col-xl-2">
                            <a class="logo logo-dark">
                                <span class="logo-sm">
                                    <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                                </span>
                                <span class="logo-lg">
                                    <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;;">
                                </span>
                            </a>
                        <!-- Light Logo-->
                            <a class="logo logo-light">
                                <span class="logo-sm">
                                    <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                                </span>
                                <span class="logo-lg">
                                    <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                                </span>
                            </a>
                        </div>

                        <div class = "col-xl-8 text-center align-items-center" style = "display:grid">   
                            <h3 style="color:#005091; font-size:170%;">
                                <p class = "text-center">KẾ HOẠCH HÀNH ĐỘNG DÀNH CHO NHÂN VIÊN</p>
                            </h3>
                        </div>




                        <!-- Dark Logo-->
                        <div class = "col-xl-2">
                            <a class="logo logo-dark">
                                <span class="logo-sm">
                                    <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                                </span>
                                <span class="logo-lg">
                                    <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                                </span>
                            </a>
                            <!-- Light Logo-->
                            <a class="logo logo-light" >
                                <span class="logo-sm">
                                    <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                                </span>
                                <span class="logo-lg">
                                    <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="row justify-content-between" style="margin-top:1%; height:70px">
                        <div class="input-group" style="width:20%;">
                            <div class="input-group-prepend" style ="width:30%; margin-right:2%">
                                <label for="year" class="form-label">Năm</label>
                                <input name="year" type="text" class="form-control datepicker @error('year') is-invalid @enderror" 
                                    id="year_view_datepicker" value="{{ old('year', now() -> format('Y')) }}">
                                @error('year')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            
                            <div class="input-group-append">
                                <label for="quarter" class="form-label">Quý</label>
                                <select name="quarter" type="text" class="form-select @error('quarter') is-invalid @enderror" id="quarter_view_select">
                                    @for ($i = 1; $i <= 4; $i ++) 
                                    <option @if(intval(old('quarter')) == $i || $current_quarter_int == $i) selected @endif
                                        value="{{ $i }}">{{   $i == 1 ? 'I' :  ($i == 2 ? 'II' : ($i == 3 ? 'III' : 'IV'))  }}
                                    </option>
                                    @endfor
                                </select>
                                @error('quarter')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        @can('plan_quarter-edit')
                        <button hidden style=" width:9.5%; height:52%; margin-left:50%; margin-right:-0.8%" type="button" id="restorePlanQuarter" class="btn btn-danger btn-label waves-effect waves-light">
                            <i class="ri-arrow-go-back-line label-icon align-middle fs-16 me-2"></i> Hoàn Tác</button>


                        <button style="width:7%; height:52%; margin-right:1%" type="button" id="editPlanQuarterChoice" class="btn btn-success btn-label waves-effect waves-light">
                            <i class="ri-pencil-fill label-icon align-middle fs-16 me-2"></i>Sửa</button>

                        <button hidden style="width:7%;  height:52%; margin-right:1%" type="button" id="editPlanQuarter" class="btn btn-success btn-label waves-effect waves-light">
                            <i class="ri-save-3-line label-icon align-middle fs-16 me-2"></i>Lưu</button>
                        @endcan
                    

                    </div>

                    <div class="row justify-content-start mt-3 mb-3" style="width:103.5%">
                        <div style = "width:50%">
                            <div class="input-group">
                                <div class="input-group-prepend" style ="margin-right:2%; width:47%;">
                                    <label for="borderInput" class="form-label">Phòng</label>
                                    <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> organizational -> room -> name}}">
                                    <input hidden name="room_id" value="{{$staff -> organizational -> room -> id}}">

                                </div>
                                <div class="input-group-append" style="width:50%">
                                    <label class="form-label">Bộ phận</label>
                                    <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> organizational -> team -> name}}">
                                    <input hidden name="team_id" value="{{$staff -> organizational -> team -> id}}">

                                </div>
                            </div>
                        </div>
                        <div style = "width:49%">
                            <div class="input-group">
                                <div class="input-group-prepend" style ="width: 50%; margin-left: -3%; margin-right: 2%;">
                                    <label class="form-label">Chức danh</label>
                                    <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> organizational -> position -> name}}">
                                    <input hidden name="position_id" value="{{$staff -> organizational -> position -> id}}">
                                </div>
                                <div class="input-group-append" style="width:50%">
                                    <label class="form-label">Họ và tên</label>
                                    <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> full_name}}">
                                    <input hidden id ="basic_info_id" name="basic_info_id" value="{{ $staff -> id }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-start" style="width:102.3%">
                        <label for="quarter_goal" class="form-label">Mục Tiêu Quý</label>
                        @if ($planQuarterX)
                        <textarea disabled class="form-control @error('quarter_goal') is-invalid @enderror" name="quarter_goal" id="quarter_goal" rows="3" 
                            style="margin-left:1%; width:98%">{{ $planQuarterX -> quarter_goal }}
                        </textarea>
                        @else
                        <textarea disabled name="quarter_goal" class="form-control @error('quarter_goal') is-invalid @enderror" id="quarter_goal" rows="3" 
                            style="margin-left:1%; width:98%">{{ old('quarter_goal') }}</textarea>
                        @endif
                        @error('quarter_goal')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                </div>
            </div>

            <div class="card" style="margin-top:-1.2%">
                <div class="card-body overflow-auto">
                    <table id="plan_quarter_table" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:250%">
                        <thead>
                            <tr>
                                <th rowspan="2" style="width:4%">Mục Tiêu #</th>
                                @for ($i = 1; $i <= 13; $i++) 
                                <th colspan="3" id = "weekHeader{{ $i }}">
                                    <label for="borderInput" class="form-label fw-bold">Tuần {{$i}}</label>
                                    <br>
                                
                                    <label for="borderInput" class="form-label" style="none-space: nowrap; font-weight:200">
                                        <span id="week{{$i}}-from_date-span"></span> - <span id="week{{$i}}-to_date-span"></span>
                                    </label>

                                    <input hidden name = "week{{ $i }}-from_date" id ="week{{ $i }}-from_date-input">
                                    <input hidden name = "week{{ $i }}-to_date" id ="week{{ $i }}-to_date-input">
                                    
                                </th>
                                @endfor
                            </tr>
                        </thead>
                        <tbody>
                            @for($g = 1; $g <= 5; $g++) 
                                <tr>
                                    <td>Mục Tiêu {{ $g }}</td>
                                    @for ($i = 1; $i <= 13; $i++) <td colspan="3">
                                        <textarea disabled type='text' id="week{{ $i }}-goal{{ $g }}" name="week{{ $i }}-goal{{ $g }}" class="form-control">{{ old('week'.$i.'-goal'.$g, 
                                                $planQuarterX && array_key_exists($g-1, $planQuarterX -> planWeeks[$i-1] ->five_weekly_goals) 
                                                    ? $planQuarterX -> planWeeks[$i-1] ->five_weekly_goals[$g-1] 
                                                    : null) }} </textarea>
                                        </td>
                                    @endfor
                                </tr>
                            @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </form>

@if($indexView && $staff && $staff -> rank > 3) 
</div>
    <div class="tab-pane" id="team_plan_quarter" role="tabpanel">
        @include('frontend.plan.plan_quarter.index_detail', ['dataInit' => $dataInit])

@endif            

@endsection


@push('scripts-plan_quarter-view')
    <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            calculateDates();
            getPlanQuarterAndPopulateData();

            $(`#editPlanQuarterChoice`).on('click', function() {
                var quarter = parseInt($('#quarter_view_select').find(":selected").val());
                if($(`#quarter_input`)[0] != undefined)
                    quarter = $(`#quarter_input`)[0].value;
                var year = parseInt($('#year_view_datepicker')[0].value)

                if (quarter > 0 && year > 0) {
                    changeInputFields(true);
                }
            });

            $(`#restorePlanQuarter`).on('click', function() {
                changeInputFields(false);
                getPlanQuarterAndPopulateData();

            })

            $(`#editPlanQuarter`).on('click', function(e) {
                e.preventDefault();
                $(`#formUpdate`).submit();
            })

            $(`#quarter_view_select, #year_view_datepicker`).on('change', function(){
                var quarter = parseInt($('#quarter_view_select').find(":selected").val());
                var year = parseInt($('#year_view_datepicker')[0].value);

                if (quarter > 0 && year > 0) 
                    getPlanQuarterAndPopulateData();
                else {
                    populateData(null);
                }
            }); 
        });
    </script>

    <script>
        $(document).ready(function() {
        $('#year_view_datepicker').datepicker({
                locale: 'vi',
                format: 'yyyy',
                startView: "years",
                minViewMode: "years",
            }); 
        });

    </script>


    <script>
        function getPlanQuarterAndPopulateData() {
            var quarter = $('#quarter_view_select').find(":selected").val();
            if($(`#quarter_input`)[0] != undefined)
                    quarter = $(`#quarter_input`)[0].value;
            
            var year = $('#year_view_datepicker')[0].value;
            var basic_info_id = $(`#basic_info_id`).val();
            $.ajax({
                type: 'GET',
                url: "{{ route('plan_quarter.getPlanQuarter') }}",
                data: {
                    quarter : quarter,
                    year: year,
                    basic_info_id: basic_info_id,
                },
                success: function(data) {
                    if (Object.keys(data).length > 0) {
                        $(`#plan_quarter_id`).val(data['id']);
                        populateData(data);
                    } else {
                        $(`#plan_quarter_id`).val(null);
                        populateData(null);
                    }
                }
            });
        }
        
        function populateData(plan_quarter) {
            $(`#quarter_goal`).val('');
            $(`#quarter_goal`).val(plan_quarter ? plan_quarter['quarter_goal']: '');
            calculateDates();

            for (let w = 1; w <= 13; w++) {
                if (plan_quarter) {
                    const plan_week_id = plan_quarter['plan_week_ids'][w-1];
                    $(`#weekHeader${w}`).on('click', function() {
                        var url = "{!! route('plan_week.view', ':id') !!}";
                        window.location.href = url.replace(':id', plan_week_id);
                    });
                } else {
                    $(`#weekHeader${w}`).off();
                }

                for (let g = 1; g <= 5; g++) {
                    $(`#week${w}-goal${g}`).val('');
                    $(`#week${w}-goal${g}`).val(plan_quarter 
                            ? plan_quarter['plan_weeks'][w-1]['five_weekly_goals'][g-1] : '');
                }
            }
        
        }

        function calculateDates() {
            var quarter = parseInt($('#quarter_view_select').find(":selected").val());            
            var year    = parseInt($('#year_view_datepicker')[0].value) 
            
            const fixed_date    = new Date('2022-10-03');
            const fixed_year    = 2022;
            const fixed_quarter = 4;

            const diff_year         = year - fixed_year;
            let diff_quarter        = 0;
            let buffer_diff_quarter = diff_year <= 0 ? -1 : 1;

            diff_quarter = fixed_quarter - ( 4 * diff_year +  quarter);
            
            if (diff_year > 0) 
                diff_quarter = -diff_quarter;        

            const calculated_date = fixed_date;

            calculated_date.setDate(calculated_date.getDate() + (diff_quarter * buffer_diff_quarter * 13 * 7) );

            for (let w = 1; w <= 13; w++) {
                if (quarter > 0) {
                    let date_str = new Intl.DateTimeFormat('en-GB').format(calculated_date);
                    let date_str_input = calculated_date.toISOString().slice(0,10);
            
                    $(`#week${w}-from_date-span`).text(date_str);
                    $(`#week${w}-from_date-input`).val(date_str_input);
        
                    calculated_date.setDate(calculated_date.getDate() + 6);

                    date_str = new Intl.DateTimeFormat('en-GB').format(calculated_date);
                    date_str_input = calculated_date.toISOString().slice(0,10);

                    $(`#week${w}-to_date-span`).text(date_str);
                    $(`#week${w}-to_date-input`).val(date_str_input);


                    calculated_date.setDate(calculated_date.getDate() + 1);
                } else {
                    $(`#week${w}-from_date-span`).text('');
                    $(`#week${w}-from_date-input`).val(null);
                    $(`#week${w}-to_date-span`).text('');
                    $(`#week${w}-to_date-input`).val(null);

                }
            }
        }

        function changeInputFields(isInputEnabled) {
            $(`#editPlanQuarter`)[0].hidden = !isInputEnabled;
            $(`#restorePlanQuarter`)[0].hidden = !isInputEnabled;
            $(`#editPlanQuarterChoice`)[0].hidden = isInputEnabled;
            $(`#quarter_goal`)[0].disabled = !isInputEnabled;

            for (let w = 1; w <= 13; w++) {
                for (let g = 1; g <= 5; g++) {
                    $(`#week${w}-goal${g}`)[0].disabled = !isInputEnabled;
                }
            }
        }
    </script>
@endpush

@section('script')
    @stack('scripts-plan_quarter-view')
    @if($indexView && $staff && $staff -> rank > 3)     
        @stack('scripts-plan_quarter-index_detail')
    @endif
@endsection