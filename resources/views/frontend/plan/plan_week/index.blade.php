@extends('layouts.fe.master2')

@section('title')
@lang('translation.starter')
@endsection

@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.css') }}" />

<style> 
.fc-timegrid-slot {
    height: 2.59em !important;
    border-bottom: 0 !important;
}


</style>
@endsection

@php
$url_logo_left = URL::asset('assets/images/logo-ac.png');
$url_logo_right = URL::asset('assets/images/logo-ac.png');
$rooms = $dataInit['rooms'];
$staff = auth() -> user() -> staff;

@endphp

@php
    $day_int = intval(now() -> format('d'));
    $month_int = intval(now() -> format('m'));
    $year_int = intval(now() -> format('Y'));

    $isLeapYear = ($year_int % 4 == 0) && ( ($year_int % 100 != 0) || ($year_int % 400 ==0) );
    $month_day_list = [31, ($isLeapYear ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    $current_total_day_int = 0;
    for ($i = 0; $i <= ($month_int-2); $i++)
        $current_total_day_int += $month_day_list[$i];
    
    $current_total_day_int += $day_int;

    $current_week_int = intval(($current_total_day_int - 1) / 7) + 1;

    $current_quarter_int = intval(($current_week_int - 1) / 13) + 1;

    $current_week_in_quarter_int = $current_week_int - (($current_quarter_int - 1) * 13);
@endphp

@section('content')

<ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">
    @if($staff)
    <li class="nav-item">
        <a class="nav-link active" data-bs-toggle="tab" href="#current_plan_week" role="tab" aria-selected="false">
            Kế Hoạch của Tuần Này
        </a>
    </li>
    @endif
    
    <li class="nav-item">
        <a class="nav-link @if(!$staff) active @endif" data-bs-toggle="tab" href="#list_plan_week" role="tab" aria-selected="false">
            Danh Sách Kế Hoạch Tuần
        </a>
    </li>
</ul>



@if ($staff)
<div class="tab-content text-muted">
    <div class="tab-pane active" id="current_plan_week" role="tabpanel">
    @if ($dataInit['planWeek'])
        @include('frontend.plan.plan_week.view_detail', ['dataInit' => $dataInit])
    @else
        <h2> Chưa Có Kế Hoạch Tuần Này, Tạo Kế Hoạch Quý Trước</h2>
    @endif
</div>
@endif
    
<div class="tab-pane @if(!$staff) active @endif" id="list_plan_week" role="tabpanel">
    <div class="card">
        <div class="card-body">
            
            <div class="row justify-content-between">
                <!-- Dark Logo-->
                <div class = "col-xl-2">
                    <a class="logo logo-dark">
                        <span class="logo-sm">
                            <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                        </span>
                        <span class="logo-lg">
                            <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;;">
                        </span>
                    </a>
                <!-- Light Logo-->
                    <a class="logo logo-light">
                        <span class="logo-sm">
                            <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                        </span>
                        <span class="logo-lg">
                            <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                        </span>
                    </a>
                </div>

                <div class = "col-xl-8 text-center align-items-center" style = "display:grid">   
                    <h3 style="color:#005091; font-size:170%;">
                        <p>DANH SÁCH KẾ HOẠCH LÀM VIỆC TRONG TUẦN</p>
                        @if(!$staff)
                        <p class ="text-center" style="margin-top:-1%">CỦA CÔNG TY</p>
                        @endif
                    </h3>
                </div>


                <!-- Dark Logo-->
                <div class = "col-xl-2">
                    <a class="logo logo-dark">
                        <span class="logo-sm">
                            <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                        </span>
                        <span class="logo-lg">
                            <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                        </span>
                    </a>
                    <!-- Light Logo-->
                    <a class="logo logo-light" >
                        <span class="logo-sm">
                            <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                        </span>
                        <span class="logo-lg">
                            <img src="{{ $url_logo_right }}" alt="" style ="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                        </span>
                    </a>
                </div>
            </div>

            <div class="row justify-content-start">
                <div class="input-group" style="width:30%;">
                    <div class="input-group-prepend" style ="width:20%; margin-right:2%">
                        <label for="year" class="form-label">Năm</label>
                        <input name="year" type="text" class="form-control datepicker" 
                            id="year_datepicker" value="{{ old('year', now() -> format('Y')) }}">
                    </div>
                    <div class="input-group-append" style ="width:30%; margin-right:2%">
                        <label for="quarter" class="form-label">Quý</label>
                        <select name="quarter" type="text" class="form-select" id="quarter_select">
                            <option value = "-1">Tất Cả</option>
                            @for ($i = 1; $i <= 4; $i ++) 
                            <option @if(intval(old('quarter')) == $i || $current_quarter_int == $i) selected @endif
                                value="{{ $i }}">{{   $i == 1 ? 'I' :  ($i == 2 ? 'II' : ($i == 3 ? 'III' : 'IV'))  }}
                            </option>
                            @endfor
                        </select>
                    </div>

                    <div style = "width:30%;"> 
                        <label for="week_in_quarter" class="form-label">Tuần</label>
                        <select name="week_in_quarter" type="text" class="form-select" id="week_in_quarter_select">
                            <option value = "-1">Tất Cả</option>
                            @for ($w = 1; $w <= 13; $w ++) 
                            <option @if(intval(old('week')) == $w || $current_week_in_quarter_int == $w) selected @endif
                                value="{{ $w }}"> {{ $w }}
                            </option>
                            @endfor
                        </select>
                    </div>
                </div>
                

            </div>

            <div class="row justify-content-start mt-3" style = "width:103.5%">
                <div style = "width:50%">
                    <div class="input-group">
                        <div class="input-group-prepend" style ="margin-right:2%; width:47%;">
                            <label for="borderInput" class="form-label">Phòng</label>
                            @if ($staff)
                            <input hidden name ="room_id" id = "room_id" value ="{{ $staff -> organizational -> room -> id }}" />
                            <input disabled class = "form-control mb-3"style="margin-left:-4%; background:none; border:0cm"  value ="{{ $staff -> organizational -> room -> name }}"/>
                            @else
                            <select id="selectRoom" name="room_id" class="form-select mb-3" aria-label="Default select example">
                                    <option value ="-1">Tất Cả Các Phòng</option>
                                    @foreach($rooms as $room)
                                    <option value="{{ $room -> id }}">{{ $room -> name }}</option>
                                    @endforeach
                               
                            </select>

                            @endif

                        </div>
                        <div class="input-group-append" style = "width:50%">
                            <label class="form-label">Bộ phận</label>

                            @if ($staff)
                            <input hidden name ="team_id" id ="team_id" value ="{{ $staff -> organizational -> team -> id }}" />
                            <input disabled class = "form-control mb-3"  style="margin-left:-4%; background:none; border:0cm"  value ="{{ $staff -> organizational -> team -> name }}"/>
                            @else
                            <select id="selectTeam" name="team_id" @if ($staff) style="margin-left:-4%; background:none; border:0cm" @endif
                                class="form-select mb-3" aria-label="Default select example">
                            </select>
                            @endif

                        </div>
                    </div>
                </div>
                <div style = "width:49%">
                    <div class="input-group">
                        <div class="input-group-prepend" style ="width: 50%; margin-left: -3%; margin-right: 2%;">
                            <label class="form-label">Chức danh</label>
                            @if ($staff)
                            <input hidden name ="position_id" id ="position_id" value ="{{ $staff -> organizational -> position -> id }}"/>
                            <input disabled class = "form-control mb-3" style="margin-left:-4%; background:none; border:0cm"  value ="{{ $staff -> organizational -> position -> name }}"/>
                            @else
                            <select id="selectPosition" name="position_id"  class="form-select mb-3" aria-label="Default select example">
                                </select>
                            @endif
                        </div>
                        <div class="input-group-append" style ="width:50%">
                            <label class="form-label">Họ và tên</label>

                            @if ($staff)
                            <input hidden name = "basic_info_id"  id ="basic_info_id" value ="{{ $staff -> id }}"/>
                            <input disabled class = "form-control mb-3" style="margin-left:-4%; background:none; border:0cm"  value ="{{ $staff -> full_name }}"/>
                            @else
                            <select id="selectStaff" name="basic_info_id" class="form-select mb-3" aria-label="Default select example">
                                </select>
                            @endif
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>

    <div class="card" style = "margin-top:-1%">
        <div class="card-body overflow-auto">
            <table id="all_plan_week_table" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Phòng</th>
                        <th>Bộ Phận</th>
                        <th>Chức Danh</th>
                        <th>Nhân Viên</th>
                        <th>Năm</th>
                        <th>Quý</th>
                        <th>Tuần trong Quý</th>
                        <th>Người tạo</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>


@endsection


@push('scripts-index')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script>
     $(document).ready(function() {

        var plan_week_table = $(`#all_plan_week_table`).DataTable({
            responsive: false

        });
      
        $('#year_datepicker').datepicker({
            locale: 'vi',
            format: 'yyyy',
            startView: "years",
            minViewMode: "years",
        });


        $(`#selectStaff, #quarter_select, #year_datepicker, #week_in_quarter_select`).on('change', function(){
            filterPlanWeek();
        });

        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            $('#selectTeam')
                .find('option')
                .remove();
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();

            if (id != '') {
                var url = "{!! route('dayoff.getTeamByRommID', ':id') !!}";

                $.ajax({
                    type: 'GET',
                    url: url.replace(':id', id),
                    success: function(data) {
                        if (data.length > 0) {
                            $('#selectTeam')
                                .find('option')
                                .remove()
                            $('#selectTeam').append($('<option>', {
                                value: -1,
                                text: 'Tất Cả Bộ Phận'
                            }));
                            data.forEach(function(value) {
                                $('#selectTeam').append($('<option>', {
                                    value: value.id,
                                    text: value.name
                                }));
                            })
                        } else {
                            $('#selectTeam')
                                .find('option')
                                .remove()

                            if ($('#selectRoom').val() != -1) {
                                $('#selectTeam').append($('<option>', {
                                    value: -1,
                                    text: 'Tất Cả Bộ Phận'
                                }));
                            }
                        }

                    filterPlanWeek();
                    }
                });

            }
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            var url = "{!! route('dayoff.getPositionByRommID', ':id') !!}";
            $('#selectPosition')
                .find('option')
                .remove();
            $('#selectStaff')
                .find('option')
                .remove();
            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectPosition')
                            .find('option')
                            .remove();
                        $('#selectPosition').append($('<option>', {
                            value: -1,
                            text: 'Tất Cả Chức Danh'
                        }));

                        data.forEach(function(value) {
                            $('#selectPosition').append($('<option>', {
                                value: value.id,
                                text: value.name
                            }));
                        })
                    } else {
                        $('#selectPosition')
                            .find('option')
                            .remove();
                        if ($('#selectTeam').val() != -1) {
                            $('#selectPosition').append($('<option>', {
                                value: -1,
                                text: 'Tất Cả Chức Danh'
                            })); 
                        }

                    }
                    filterPlanWeek();
                }
            });
        });

        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();

            var url = "{!! route('dayoff.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";


            $('#selectStaff')
                .find('option')
                .remove();

            $.ajax({
                type: 'GET',
                url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectStaff').append($('<option>', {
                            value: -1,
                            text: `Tất Cả Nhân Viên`,

                        }));

                        data.forEach(function(employee) {
                            $('#selectStaff').append($('<option>', {
                                value: employee['id'],
                                text: `${employee['full_name']}`,

                            }));
                        })
                    } else {
                        $('#selectStaff')
                            .find('option')
                            .remove();
                        if ($('#selectPosition').val() != -1) {
                            $('#selectStaff').append($('<option>', {
                                value: -1,
                                text: `Tất Cả Nhân Viên`,

                            }));
                        }

                    }
                    filterPlanWeek();
                }
            });

        });

        filterPlanWeek();
        
        function getActionInputString(plan_week_id) {
            input_str = `
                <div class="dropdown d-inline-block">
                    <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="ri-more-fill align-middle"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-end">
                        @can('plan_week-view')
                        <li><a href="/plan_week/view/${plan_week_id}" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
                        @endcan

                        @can('plan_week-edit')
                        <li><a href="/plan_week/edit/${plan_week_id}" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                        @endcan
                        
                        @can('plan_week-delete')
                        <li><a href="#" id="${plan_week_id}" class="dropdown-item remove-item-btn"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Delete</a>
                        </li>
                        <form method="post" id="formDelete${plan_week_id}" action="/plan_week/delete/${plan_week_id}" style="display:inline">
                            @method('delete')
                            @csrf
                        </form>
                        @endcan
                    </ul>
                </div>
            `;

            return input_str;
        }

        function filterPlanWeek() {
            plan_week_table.clear().draw();

            var quarter         = $('#quarter_select').find(":selected").val();        
            var year            = $('#year_datepicker')[0].value;

            var basic_info_id = 0;
            var room_id = 0;
            var team_id = 0;
            var position_id = 0;

            if ($('#selectStaff')[0] != undefined) {
                basic_info_id   = $(`#selectStaff`).find(":selected").val();
                room_id         = $('#selectRoom').find(":selected").val();        
                team_id         = $('#selectTeam').find(":selected").val();
                position_id     = $('#selectPosition').find(":selected").val();

            } else {
                basic_info_id = $('#basic_info_id').val();
                room_id = $('#room_id').val();
                team_id = $('#team_id').val();
                position_id = $('#position_id').val();

            }

            var week_in_quarter = $('#week_in_quarter_select').find(":selected").val();
            
            
            $.ajax({
                type: 'GET',
                url: "{{ route('plan_week.filterPlanWeek') }}",
                data: {
                    week_in_quarter: week_in_quarter,
                    quarter : quarter,
                    year: year,
                    room_id: room_id,
                    team_id: team_id,
                    position_id: position_id,
                    basic_info_id: basic_info_id,

                },
                success: function(data) {

                    if (data.length > 0) {
                        data.forEach( (plan_week, $index) => {
                            plan_week_table.rows.add([
                                {
                                    "0": $index + 1, 
                                    "1": plan_week['staff']['organizational']['room']['name'], 
                                    "2": plan_week['staff']['organizational']['team']['name'],  
                                    "3": plan_week['staff']['organizational']['position']['name'],  
                                    "4": plan_week['staff']['full_name'], 
                                    "5": plan_week['year'], 
                                    "6": plan_week['quarter'], 
                                    "7": plan_week['week_in_quarter'],
                                    "8": plan_week['user']['name'], 
                                    "9": getActionInputString(plan_week['id']) 
                                },
                            ]);
                        });
                    }

                    plan_week_table.draw();
                }
            });
        }

        $('#all_plan_week_table tbody').on('click', '.remove-item-btn', function(event) {
            const id = $(this)[0].id;
            event.preventDefault();
            swal.fire({
                    title: 'Bạn muốn xóa dữ liệu này ?',
                    text: "Lưu ý : Dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                    icon: 'warning',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Đồng ý',
                    showDenyButton: true,
                    denyButtonText: 'Hủy bỏ',
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        $(`#formDelete${id}`).submit();
                    } else if (result.isDenied) {
                        Swal.fire('Dữ liệu được giữ lại', '', 'info')
                    }
            });
        });
    });
</script>
@endpush

@section('script')
    @stack('scripts-view_detail')
    @stack('scripts-index')
@endsection