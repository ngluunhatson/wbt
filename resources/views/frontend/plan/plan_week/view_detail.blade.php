@php 
    $planWeekX = $dataInit['planWeek'];
    $url_logo_left = URL::asset('assets/images/logo-ac.png');
    $url_logo_right = URL::asset('assets/images/logo-ac.png');
    $staff = $planWeekX -> staff;
@endphp



<div class="modal fade" id="modalView" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="border-radius: 10px !important;
    overflow: hidden !important;">
    
    </div>
  </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="row justify-content-between">
            <!-- Dark Logo-->
            <div class="col-xl-2">
                <a class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;;">
                    </span>
                </a>
                <!-- Light Logo-->
                <a class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                </a>
            </div>

            <div class="col-xl-8 text-center align-items-center" style="display:grid">
                <h3 style="color:#005091; font-size:170%;">
                    <p class="text-center">KẾ HOẠCH TUẦN DÀNH CHO NHÂN VIÊN</p>
                </h3>
            </div>

            <!-- Dark Logo-->
            <div class="col-xl-2">
                <a class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                </a>
                <!-- Light Logo-->
                <a class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                </a>
            </div>
        </div>
        <div class="row justify-content-start">
            <div class="input-group" style="width:30%;">
                <div class="input-group-prepend" style ="width:20%; margin-right:2%">
                    <label for="year" class="form-label">Năm</label> 
                    <input disabled name="year" type="text" class="form-control datepicker" style="margin-left:-16%; background:none; border:0cm" 
                        id="year_datepicker" value="{{ $planWeekX -> year }}">
                </div>
                <div class="input-group-append" style ="width:15%; margin-right:2%">
                    <label for="quarter" class="form-label">Quý</label>
                    <input disabled name="quarter" id = "quarter" type="text" class="form-control" style="margin-left:-15%; background:none; border:0cm" 
                        value ="{{   $planWeekX -> quarter == 1 ? 'I' :  ($planWeekX -> quarter == 2 ? 'II' : ($planWeekX -> quarter == 3 ? 'III' : 'IV'))  }}" />

                </div>

                <div style = "width:30%;"> 
                    <label for="week_in_quarter" class="form-label">Tuần</label>
                    <input disabled name="week_in_quarter" id = "week_in_quarter" type="text" class="form-control" style="margin-left:-8%; background:none; border:0cm" 
                        value ="{{ $planWeekX -> week_in_quarter }}" />
                </div>
            </div>
          
        </div>

        <div class="row justify-content-start mt-3 mb-3" style="width:103.5%">
            <div style="width:50%">
                <div class="input-group">
                    <div class="input-group-prepend" style="margin-right:2%; width:47%;">
                        <label for="borderInput" class="form-label">Phòng</label>
                        <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> organizational -> room -> name}}">
                        <input hidden name="room_id" value="{{$staff -> organizational -> room -> id}}">

                    </div>
                    <div class="input-group-append" style="width:50%">
                        <label class="form-label">Bộ phận</label>
                        <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> organizational -> team -> name}}">
                        <input hidden name="team_id" value="{{$staff -> organizational -> team -> id}}">

                    </div>
                </div>
            </div>
            <div style="width:49%">
                <div class="input-group">
                    <div class="input-group-prepend" style="width: 50%; margin-left: -3%; margin-right: 2%;">
                        <label class="form-label">Chức danh</label>
                        <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> organizational -> position -> name}}">
                        <input hidden name="position_id" value="{{$staff -> organizational -> position -> id}}">
                    </div>
                    <div class="input-group-append" style="width:50%">
                        <label class="form-label">Họ và tên</label>
                        <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> full_name}}">
                        <input hidden id="basic_info_id" name="basic_info_id" value="{{ $staff -> id }}">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class = "row" style = "margin-top:-0.5%">
    <div style = "width:30%;">
        <div class = "card"  style = "height:fit-content;">  
            <div class = "card-header">
                <h5>5 Khía Cạnh và Mục Tiêu</h5>
            </div>
            <div class ="card-body" >
                <div class = "row mt-1">
                    <div style ="width:2%"></div>
                    <div class = "row" style ="width:96%">
                        <div style ="width:22%; height:fit-content;">
                            <h6 class ="text-center">Khía Cạnh</h6>
                            @for($i = 1; $i <= 5; $i ++)
                            <div class ="row mb-2"> 
                                <textarea disabled type ="text" class ="form-control text-center" rows ="3" id ="week_aspect{{ $i }}">{{ 
                                    $planWeekX -> five_weekly_aspects && array_key_exists($i-1, $planWeekX -> five_weekly_aspects) ? $planWeekX -> five_weekly_aspects[$i-1] : '' 
                                }}</textarea>
                            </div>
                            @endfor
                        </div>
                        <div style ="width:5%"></div>
                        <div style ="width:10%;">
                            <div style = "height:2em"></div>
                            @for($i = 1; $i <= 5; $i ++)
                            <div class ="row" style = "height:6em"> 
                                <span class="mdi mdi-arrow-right-bold" style ="font-size:3em; margin-left:-15%"></span>
                            </div>
                            <div style = "height:0.5em"></div>

                            @endfor
                        
                        </div>
                        <div style ="width:5%"></div>
                        <div style ="width:50%">
                            <h6 class ="text-center">Mục Tiêu</h6>
                            @for($i = 1; $i <= 5; $i ++)
                            <div class ="row mb-2" style = "height:5.8755em;">
                                <input disabled type ="text" class ="form-control text-center" id = "week_goal{{ $i }}"
                                    value = "{{ $planWeekX -> five_weekly_goals && array_key_exists($i-1, $planWeekX -> five_weekly_goals) ?  $planWeekX -> five_weekly_goals[$i-1] : '' }}" />
                            </div>
                            @endfor
                        </div>
                    
                    </div>   

                    <div style ="width:2%"></div>   
                </div>
            </div>
        </div>

        <div class = "card" style ="margin-top:-3%" >
            <div class = "card-header">
                <h5>Tổng Kết Công Việc</h5>
            </div>
            <div class = "card-body">
                <div id = "pie_chart" class="e-charts"></div>
            </div>
        </div>
    </div>
    <div style ="width:70%">
        <div class = "card" style = "margin-left:-1.5%;">
            <div class = "card-header">
                <h5>Thông Tin Kế Hoạch</h5>
            </div>
            <div class = "card-body">
                <div id = "calendar"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalViewDayTask" tabindex="-1"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content border-0" id = "modalViewDayTaskContent">
          
        </div> <!-- end modal-content-->
    </div> <!-- end modal dialog-->
</div>

@push('scripts-view_detail')
<script src="{{ URL::asset('assets/libs/echarts/echarts.min.js') }}"></script>
<script>
    $(document).ready(function() {
        const DefaultPlanColorHexCode = '#3577F1';
        const defaultDate = <?php echo json_encode($planWeekX -> start_date) ?>;
        const basic_info_id = <?php echo json_encode($planWeekX -> basic_info_id) ?>;
        const earliestTime     = "6:00";
        const latestTime       = "18:00";
        
        var calendar = new FullCalendar.Calendar($('#calendar')[0], {
            initialDate: defaultDate,
            timeZone: 'local',
            editable: false,
            droppable: false,
            firstDay: 1,
            selectable: false,
            navLinks: true,
            initialView: 'timeGridWeek',
            themeSystem: 'bootstrap',
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'timeGridWeek'
            },
            slotMinTime: earliestTime,
            slotMaxTime: latestTime,
            contentHeight:'auto',
            contentHeight: 848,
            allDaySlot: false,

            slotLabelFormat: {
                hour: 'numeric',
                minute: '2-digit',
                omitZeroMinute: false,
                hour12: false,
            },
            slotEventOverlap: true,


            eventSources: [
                {
                    events: function(info, successCallback, failureCallback) {
                        $.ajax({
                            method: "GET",
                            url: "{{ route('plan_week.getPlanDayTimeslotDataFromDates') }}",
                            data: {
                                start: info.start.toISOString().slice(0,10),
                                end:   info.end.toISOString().slice(0,10),
                                basic_info_id: basic_info_id
                            },
                            success: data => {
                                echarts.dispose($('#pie_chart')[0]);

                                const planColorDict = {};
                                planColorDict[DefaultPlanColorHexCode] = {
                                    'name'          : 'Mặc Định',
                                    'value'         :  0,
                                    'plan_color_id' : null,
                                };

                                var events = [];
                                data.forEach(plan_day_timeslot => {

                                    if (plan_day_timeslot.plan_color == null) {
                                        planColorDict[DefaultPlanColorHexCode].value += (plan_day_timeslot.end_time - plan_day_timeslot.start_time);
                                    } else {
                                        if (plan_day_timeslot.plan_color.hex_code in planColorDict) {
                                            planColorDict[plan_day_timeslot.plan_color.hex_code].value += (plan_day_timeslot.end_time - plan_day_timeslot.start_time);
                                        } else {
                                            planColorDict[plan_day_timeslot.plan_color.hex_code] = {
                                                'name'          : plan_day_timeslot.plan_color.category_name,
                                                'value'         : (plan_day_timeslot.end_time - plan_day_timeslot.start_time),
                                                'plan_color_id' : plan_day_timeslot.plan_color_id,
                                            }
                                        }
                                    }
                                    const dateObject = new Date(plan_day_timeslot['date']);

                                 
                                    events.push(
                                        {
                                            "id"            : plan_day_timeslot['id'],
                                            "title"         : plan_day_timeslot['title'],
                                            "start"         : new Date(dateObject.getFullYear(),
                                                                  dateObject.getMonth(),dateObject.getUTCDate(), parseInt(plan_day_timeslot['start_time']), 
                                                                  (plan_day_timeslot['start_time'] - parseInt(plan_day_timeslot['start_time'])) * 60),
                                            "end"           :  new Date(dateObject.getFullYear(),
                                                                  dateObject.getMonth(),dateObject.getUTCDate(), parseInt(plan_day_timeslot['end_time']), 
                                                                  (plan_day_timeslot['end_time'] - parseInt(plan_day_timeslot['end_time'])) * 60),
                                            "location"      : plan_day_timeslot['location'],
                                            'description'   : plan_day_timeslot['description'],
                                            'color'         : plan_day_timeslot.plan_color ? plan_day_timeslot.plan_color.hex_code : DefaultPlanColorHexCode,
                                            'plan_color_id' : plan_day_timeslot.plan_color_id,
                                            
                                        },
                                        
                                    );
                                    
                                });

                                if (planColorDict[DefaultPlanColorHexCode].value != 0 || Object.keys(planColorDict).length > 1) 
                                    initializePieChart(planColorDict);
                                    
                                successCallback(events);
                            }
                        });
                    },
                }
            ],

            navLinkDayClick: function(date, jsEvent) {
                $.ajax({
                    method: "GET",
                    url: "{{ route('plan_week.getPlanDayIdFromDate') }}",
                    data: {
                        date: moment(date).format('Y-MM-DD'),
                        basic_info_id: basic_info_id,
                    },
                    success: data => {
                       if (data.success) {
                            var url = "{!! route('plan_day.view', ':id') !!}";
                            window.location.href = url.replace(':id', data['plan_day_id']);
                       } else {
                            swal.fire({
                                icon: 'error',
                                title: `${data.message}`,
                                confirmButtonColor: '#3085d6',
                                showConfirmButton: false,
                            });
                       }
                    }
            
                });
               
            },

            eventClick: event => {
               toggleModalView(event);
            },
            
            datesSet: function(dateInfo) {

                $.ajax({
                    method: "GET",
                    url: "{{ route('plan_week.getPlanWeekDataFromDates') }}",
                    data: {
                        start: dateInfo.startStr.slice(0,10),
                        end:   dateInfo.endStr.slice(0,10),
                        basic_info_id: basic_info_id
                    },
                    success: data => {
                        if (Object.keys(data).length > 0) 
                            populateForm(data, moment(dateInfo.startStr));
                        else 
                            populateForm(null, moment(dateInfo.startStr));
                    }
                });
            
            }
        });

        calendar.render();


        function toggleModalView(eventClickInfo) {
           
            const event = eventClickInfo.event;

            const event_date_str    = event.start.toISOString().slice(0,10);
            const event_start_time  = event.start.toTimeString().slice(0,5);
            const event_end_time    = event.end.toTimeString().slice(0,5);

            $(`#modalViewDayTask`).modal('toggle');
            input_str = `
                <div class="modal-header p-3 bg-soft-info">
                    <h5 class="modal-title" id="modal-title">${event.title}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body p-4" >
                    <div class="event-details">
                        <div class="d-flex mb-2">
                            <div class="flex-grow-1 d-flex align-items-center">
                                <div class="flex-shrink-0 me-3">
                                    <i class="ri-calendar-event-line text-muted fs-16"></i>
                                </div>
                                <div class="flex-grow-1">
                                    <h6 class="d-block fw-semibold mb-0" id="event-start-date-tag">${event_date_str}</h6>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mb-2">
                            <div class="flex-shrink-0 me-3">
                                <i class="ri-time-line text-muted fs-16"></i>
                            </div>
                            <div class="flex-grow-1">
                                <h6 class="d-block fw-semibold mb-0"><span id="event-timepicker1-tag">${event_start_time}</span> - <span id="event-timepicker2-tag">${event_end_time}</span></h6>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mb-2">
                            <div class="flex-shrink-0 me-3">
                                <i class="ri-map-pin-line text-muted fs-16"></i>
                            </div>
                            <div class="flex-grow-1">
                                <h6 class="d-block fw-semibold mb-0"> <span id="event-location-tag">${event.extendedProps.location ?? 'Chưa Có'}</span></h6>
                            </div>
                        </div>
                        <div class="d-flex mb-3">
                            <div class="flex-shrink-0 me-3">
                                <i class="ri-discuss-line text-muted fs-16"></i>
                            </div>
                            <div class="flex-grow-1">
                                <p class="d-block text-muted mb-0" id="event-description-tag">${event.extendedProps.description ?? 'Chưa Có'}</p>
                            </div>
                        </div>
                    </div>
                    
                   
                </div>
            `
            $(`#modalViewDayTaskContent`).html(input_str);
        }

        function populateForm(plan_week, moment_week_start) {

            const quarter = moment_week_start.quarter();
            const week_in_quarter = moment_week_start.isoWeeks() - 13 * (quarter - 1);
            const year = moment_week_start.year();

            $('#year_datepicker').val(year);
            $('#quarter').val(quarter == 1 ? 'I' : (quarter == 2 ? 'II' : (quarter == 3 ? 'III' : "IV")));
            $('#week_in_quarter').val(week_in_quarter);

            for (let i = 1; i <= 5; i ++) {
                $(`#week_aspect${i}`).val('');
                $(`#week_aspect${i}`).val(plan_week ? plan_week['five_weekly_aspects'][i-1] : '');
                $(`#week_goal${i}`).val(plan_week ? plan_week['five_weekly_goals'][i-1] : '');


            }


        }

        function initializePieChart(planColorDict) {
            var chartPieColors = Object.keys(planColorDict);
            var chartDom = document.getElementById('pie_chart');

            data_chart_pie = [];
            data_color_hex_code = [];
            chartPieColors.forEach( color_hex_code => {
                if (planColorDict[color_hex_code].value > 0) {
                    data_chart_pie.push({
                        value: planColorDict[color_hex_code].value,
                        name:  planColorDict[color_hex_code].name,
                        plan_color_id: planColorDict[color_hex_code].plan_color_id,
                    });
                    
                    data_color_hex_code.push(color_hex_code);
                }
            });
            var myChart = echarts.init(chartDom);
            var option;
            option = {
                tooltip: {
                    trigger: 'item'
                },
                legend: {
                    orient: 'vertical',
                    textStyle: {
                        //The style of the legend text
                        color: '#858d98'
                    },
                    height:"35%",
                    bottom:'bottom',
                },
                color: data_color_hex_code,

                series: [{
                    name: 'Thời Gian (h)',
                    type: 'pie',
                    radius: '95%',
                    bottom: "50%",
                    stillShowZeroSum: false,
                    data: data_chart_pie,
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }],
                textStyle: {
                    fontFamily: 'Poppins, sans-serif'
                }
            };
            option && myChart.setOption(option);

       }

       
    });
   
</script>

@endpush