@extends('layouts.fe.master2')

@section('title')
@lang('translation.starter')
@endsection

@section('css')

<link rel="stylesheet" href="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.css') }}" />

<style> 
.fc-timegrid-slot {
    height: 2.59em !important;
    border-bottom: 0 !important;
}


</style>
@endsection

@section('content')

    @include('frontend.plan.plan_week.buttons', ['dataInit' => $dataInit])
    @include('frontend.plan.plan_week.view_detail', ['dataInit' => $dataInit])
@endsection


@section('script')
    @stack('scripts-view_detail')
@endsection