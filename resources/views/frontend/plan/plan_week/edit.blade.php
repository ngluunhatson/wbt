@extends('layouts.fe.master2')

@section('title')
@lang('translation.starter')
@endsection

@section('css')
<link rel="stylesheet" href="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.css') }}" />
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">


<style> 
.fc-timegrid-slot {
    height: 2.4205em !important;
    border-bottom: 0 !important;
}


</style>
@endsection

@section('content')


@include('frontend.plan.plan_week.buttons', ['dataInit' => $dataInit])


@php 
    $planWeekX = $dataInit['planWeek'];
    $url_logo_left = URL::asset('assets/images/logo-ac.png');
    $url_logo_right = URL::asset('assets/images/logo-ac.png');
    $staff = $planWeekX -> staff;
@endphp

<meta name="csrf-token" content="<?= csrf_token() ?>">

<style type="text/css">
        body {
            margin: 0;
            padding: 0;
            font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
            font-size: 14px;
        }

        #top {
            background: #eee;
            border-bottom: 1px solid #ddd;
            padding: 0 10px;
            line-height: 40px;
            font-size: 12px;
        }

</style>


<div class="card">
    <div class="card-body">
        <div class="row justify-content-between">
            <!-- Dark Logo-->
            <div class="col-xl-2">
                <a class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;;">
                    </span>
                </a>
                <!-- Light Logo-->
                <a class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_left }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;">
                    </span>
                </a>
            </div>

            <div class="col-xl-8 text-center align-items-center" style="display:grid">
                <h3 style="color:#005091; font-size:170%;">
                    <p class="text-center">KẾ HOẠCH TUẦN DÀNH CHO NHÂN VIÊN</p>
                </h3>
            </div>

            <!-- Dark Logo-->
            <div class="col-xl-2">
                <a class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                </a>
                <!-- Light Logo-->
                <a class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ $url_logo_right }}" alt="" style="margin-bottom: 10%;margin-top: 10%;height: 60px;margin-left: -9%;">
                    </span>
                </a>
            </div>
        </div>
        <div class="row justify-content-start">
            <div class="input-group" style="width:30%;">
                <div class="input-group-prepend" style ="width:20%; margin-right:2%">
                    <label for="year" class="form-label">Năm</label> 
                    <input disabled name="year" type="text" class="form-control datepicker" style="margin-left:-16%; background:none; border:0cm" 
                        id="year_datepicker" value="{{ $planWeekX -> year }}">
                </div>
                <div class="input-group-append" style ="width:15%; margin-right:2%">
                    <label for="quarter" class="form-label">Quý</label>
                    <input disabled name="quarter" id = "quarter" type="text" class="form-control" style="margin-left:-15%; background:none; border:0cm" 
                        value ="{{   $planWeekX -> quarter == 1 ? 'I' :  ($planWeekX -> quarter == 2 ? 'II' : ($planWeekX -> quarter == 3 ? 'III' : 'IV'))  }}" />

                </div>

                <div style = "width:30%;"> 
                    <label for="week_in_quarter" class="form-label">Tuần</label>
                    <input disabled name="week_in_quarter" id ="week_in_quarter" type="text" class="form-control" style="margin-left:-8%; background:none; border:0cm" 
                        value ="{{ $planWeekX -> week_in_quarter }}" />
                </div>
            </div>
          
        </div>

        <div class="row justify-content-start mt-3 mb-3" style="width:103.5%">
            <div style="width:50%">
                <div class="input-group">
                    <div class="input-group-prepend" style="margin-right:2%; width:47%;">
                        <label for="borderInput" class="form-label">Phòng</label>
                        <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> organizational -> room -> name}}">
                        <input hidden name="room_id" value="{{$staff -> organizational -> room -> id}}">

                    </div>
                    <div class="input-group-append" style="width:50%">
                        <label class="form-label">Bộ phận</label>
                        <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> organizational -> team -> name}}">
                        <input hidden name="team_id" value="{{$staff -> organizational -> team -> id}}">

                    </div>
                </div>
            </div>
            <div style="width:49%">
                <div class="input-group">
                    <div class="input-group-prepend" style="width: 50%; margin-left: -3%; margin-right: 2%;">
                        <label class="form-label">Chức danh</label>
                        <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> organizational -> position -> name}}">
                        <input hidden name="position_id" value="{{$staff -> organizational -> position -> id}}">
                    </div>
                    <div class="input-group-append" style="width:50%">
                        <label class="form-label">Họ và tên</label>
                        <input disabled class="form-control" style="margin-left:-4%; background:none; border:0cm" value="{{$staff -> full_name}}">
                        <input hidden id="basic_info_id" name="basic_info_id" value="{{ $staff -> id }}">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class = "row" style = "margin-top:-0.5%">
    <div style = "width:30%;">
        <div class = "card" style = "height:fit-content;">  
            <div class = "card-header">
                <div class = "row">
                    <h5 style = "width:75%">5 Khía Cạnh và Mục Tiêu</h5>
                    <a style="width:15%;" href="#" noref>
                        <button type="button" id="saveWeekAspectButton" class="btn btn-success btn-label waves-effect waves-light">
                            <i class="ri-save-3-line label-icon align-middle fs-16 me-2"></i>Lưu</button>
                    </a>
                    <div style ="width:10%"></div>
                </div>
            </div>
            <form id="formSaveWeekAspects" method="POST" action="">
                @csrf
                <input type="hidden" id="plan_week_id_week_aspect_form" name="plan_week_id_week_aspect_form" value="{{ $planWeekX -> id }}">
                <div class ="card-body" >
                    <div class = "row mt-1">
                        <div style ="width:2%"></div>
                        <div class = "row" style ="width:96%">
                            <div style ="width:22%; height:fit-content;">
                                <h6 class ="text-center">Khía Cạnh</h6>
                                   @for($i = 1; $i <= 5; $i ++)
                                    <div class ="row mb-2"> 
                                        <textarea name = "week_aspect{{ $i }}"  id = "week_aspect{{ $i }}"  type ="text" class ="form-control text-center" rows ="3">{{ 
                                            $planWeekX -> five_weekly_aspects && array_key_exists($i-1, $planWeekX -> five_weekly_aspects) ? $planWeekX -> five_weekly_aspects[$i-1] : '' 
                                        }}</textarea>
                                    </div>
                                  
                                    @endfor

                            </div>
                            <div style ="width:5%"></div>
                            <div style ="width:10%;">
                                <div style = "height:2em"></div>
                                @for($i = 1; $i <= 5; $i ++)
                                <div class ="row" style = "height:6em"> 
                                    <span class="mdi mdi-arrow-right-bold" style ="font-size:3em; margin-left:-15%"></span>
                                </div>
                                @endfor
                            
                            </div>
                            <div style ="width:5%"></div>
                            <div style ="width:50%">
                                <h6 class ="text-center">Mục Tiêu</h6>
                                @for($i = 1; $i <= 5; $i ++)
                                <div class ="row mb-2" style = "height:5.45em;">
                                    <input disabled type ="text" class ="form-control text-center" id = "week_goal{{ $i }}"
                                        value = "{{ $planWeekX -> five_weekly_goals && array_key_exists($i-1, $planWeekX -> five_weekly_goals) ?  $planWeekX -> five_weekly_goals[$i-1] : '' }}" />
                                </div>
                                @endfor
                            </div>
                        
                        </div>   

                        <div style ="width:2%"></div>   
                    </div>
                </div>
            </form>
        </div>

        <div class = "card" style ="margin-top:-3%" >
            <div class = "card-header">
                <h5>Tổng Kết Công Việc</h5>
            </div>
            <div class = "card-body">
                <div id = "pie_chart" class="e-charts"></div>
            </div>
        </div>
    </div>
    <div style ="width:70%">
        <div class = "card" style = "margin-left:-1.5%;">
            <div class = "card-header">
                <h5>Thông Tin Kế Hoạch</h5>
            </div>
            <div class = "card-body">
                <div id = "calendar"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAddDayTask" tabindex="-1"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content border-0">
            <div class="modal-header p-3 bg-soft-info">
                <h5 class="modal-title" id="modal-title">Thêm Công Việc</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body p-4" id = "modalAddDayTaskBody">
                
            </div>
        </div> <!-- end modal-content-->
    </div> <!-- end modal dialog-->
</div>

<div class="modal fade" id="modalEditDayTask" tabindex="-1"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content border-0">
            <div class="modal-header p-3 bg-soft-info">
                <h5 class="modal-title" id="modal-title">Sửa Công Việc</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body p-4" id = "modalEditDayTaskBody">
                
            </div>
        </div> <!-- end modal-content-->
    </div> <!-- end modal dialog-->
</div>


<div class="modal fade" id="modalViewDayTask" tabindex="-1"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content border-0" id = "modalViewDayTaskContent">
          
        </div> <!-- end modal-content-->
    </div> <!-- end modal dialog-->
</div>

<div class="modal fade" id="modalPlanColor" tabindex="-1"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content border-0" id = "modalPlanColorContent">
          
        </div> <!-- end modal-content-->
    </div> <!-- end modal dialog-->
</div>



@endsection


@section('script')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/echarts/echarts.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.js') }}"></script>
<script>
    $(document).ready(function() {
       
        const DefaultPlanColorHexCode = '#3577F1';
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        });

        const defaultDate = <?php echo json_encode($planWeekX -> start_date) ?>;
        const basic_info_id = <?php echo json_encode($planWeekX -> basic_info_id) ?>;

        const minute_increment = 15;
        const earliestTime     = "6:00";
        const latestTime       = "18:00";

        var calendar = new FullCalendar.Calendar($('#calendar')[0], {
            initialDate: defaultDate,
            timeZone: 'local',
            editable: false,
            droppable: false,
            firstDay: 1,
            selectable: true,
            navLinks: true,
            initialView: 'timeGridWeek',
            themeSystem: 'bootstrap',
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'timeGridWeek'
            },
            slotMinTime: earliestTime,
            slotMaxTime: latestTime,
            contentHeight: 855,
            allDaySlot: false,

            slotLabelFormat: {
                hour: 'numeric',
                minute: '2-digit',
                omitZeroMinute: false,
                hour12: false,
            },
            slotEventOverlap: true,


            eventSources: [
                {
                    events: function(info, successCallback, failureCallback) {
                        $.ajax({
                            method: "GET",
                            url: "{{ route('plan_week.getPlanDayTimeslotDataFromDates') }}",
                            data: {
                                start: info.start.toISOString().slice(0,10),
                                end:   info.end.toISOString().slice(0,10),
                                basic_info_id: basic_info_id
                            },
                            success: data => {
                                echarts.dispose($('#pie_chart')[0]);

                                const planColorDict = {};
                                planColorDict[DefaultPlanColorHexCode] = {
                                    'name'          : 'Mặc Định',
                                    'value'         :  0,
                                    'plan_color_id' : null,
                                };

                              
                                var events = [];
                                data.forEach(plan_day_timeslot => {

                                    if (plan_day_timeslot.plan_color == null) {
                                        planColorDict[DefaultPlanColorHexCode].value += (plan_day_timeslot.end_time - plan_day_timeslot.start_time);
                                    } else {
                                        if (plan_day_timeslot.plan_color.hex_code in planColorDict) {
                                            planColorDict[plan_day_timeslot.plan_color.hex_code].value += (plan_day_timeslot.end_time - plan_day_timeslot.start_time);
                                        } else {
                                            planColorDict[plan_day_timeslot.plan_color.hex_code] = {
                                                'name'          : plan_day_timeslot.plan_color.category_name,
                                                'value'         : (plan_day_timeslot.end_time - plan_day_timeslot.start_time),
                                                'plan_color_id' : plan_day_timeslot.plan_color_id,
                                            }
                                        }
                                    }
                                    const dateObject = new Date(plan_day_timeslot['date']);

                                 
                                    events.push(
                                        {
                                            "id"            : plan_day_timeslot['id'],
                                            "title"         : plan_day_timeslot['title'],
                                            "start"         : new Date(dateObject.getFullYear(),
                                                                  dateObject.getMonth(),dateObject.getUTCDate(), parseInt(plan_day_timeslot['start_time']), 
                                                                  (plan_day_timeslot['start_time'] - parseInt(plan_day_timeslot['start_time'])) * 60),
                                            "end"           :  new Date(dateObject.getFullYear(),
                                                                  dateObject.getMonth(),dateObject.getUTCDate(), parseInt(plan_day_timeslot['end_time']), 
                                                                  (plan_day_timeslot['end_time'] - parseInt(plan_day_timeslot['end_time'])) * 60),
                                            "location"      : plan_day_timeslot['location'],
                                            'description'   : plan_day_timeslot['description'],
                                            'color'         : plan_day_timeslot.plan_color ? plan_day_timeslot.plan_color.hex_code : DefaultPlanColorHexCode,
                                            'plan_color_id' : plan_day_timeslot.plan_color_id,
                                            
                                        },
                                        
                                    );
                                    
                                });
                                
                                
                                if (planColorDict[DefaultPlanColorHexCode].value != 0 || Object.keys(planColorDict).length > 1) 
                                    initializePieChart(planColorDict);
                                
                                successCallback(events);

                            }
                        });
                    }, 
                }
            ],

            navLinkDayClick: function(date, jsEvent) {
                $.ajax({
                    method: "GET",
                    url: "{{ route('plan_week.getPlanDayIdFromDate') }}",
                    data: {
                        date: moment(date).format('Y-MM-DD'),
                        basic_info_id: basic_info_id,
                    },
                    success: data => {
                       if (data.success) {
                            var url = "{!! route('plan_day.view', ':id') !!}";
                            window.location.href = url.replace(':id', data['plan_day_id']);
                       } else {
                            swal.fire({
                                icon: 'error',
                                title: `${data.message}`,
                                confirmButtonColor: '#3085d6',
                                showConfirmButton: false,
                            });
                       }
                    }
            
                });
               
            },

            select: (range) => {
               toggleModalAdd(range.startStr, range.endStr);
            },

            eventClick: event => {
               toggleModalView(event);
            },
            
            datesSet: function(dateInfo) {

                $.ajax({
                    method: "GET",
                    url: "{{ route('plan_week.getPlanWeekDataFromDates') }}",
                    data: {
                        start: dateInfo.startStr.slice(0,10),
                        end:   dateInfo.endStr.slice(0,10),
                        basic_info_id: basic_info_id
                    },
                    success: data => {
                        if (Object.keys(data).length > 0) 
                            populateForm(data, moment(dateInfo.startStr));
                        else 
                            populateForm(null, moment(dateInfo.startStr));
                    }
                });
            
            }
        });

        calendar.render();


        $(`#saveWeekAspectButton`).on('click', function(e) {
            e.preventDefault();

            $.ajax({
                method: "POST",
                url: "{{ route('plan_week.saveWeekAspects') }}",
                data: $(`#formSaveWeekAspects`).serialize(),
                success: response => {
                    if (response.success) {
                        swal.fire({
                            icon: 'success',
                            title: `${response.message}`,
                            confirmButtonColor: '#3085d6',
                            showConfirmButton: true,
                            confirmButtonText: 'Xác Nhận',
                        })
                    }
                    else {
                        swal.fire({
                            icon: 'error',
                            title: `${response.message}`,
                            confirmButtonColor: '#3085d6',
                            showConfirmButton: false,
                        });
                    }
                }
            });
        });

       function initializePieChart(planColorDict) {
            var chartPieColors = Object.keys(planColorDict);
            var chartDom = document.getElementById('pie_chart');

            data_chart_pie = [];
            data_color_hex_code = [];
            chartPieColors.forEach( color_hex_code => {
                if (planColorDict[color_hex_code].value > 0) {
                    data_chart_pie.push({
                        value: planColorDict[color_hex_code].value,
                        name:  planColorDict[color_hex_code].name,
                        plan_color_id: planColorDict[color_hex_code].plan_color_id,
                    });
                    
                    data_color_hex_code.push(color_hex_code);
                }
            });
            var myChart = echarts.init(chartDom);
            var option;
            option = {
                tooltip: {
                    trigger: 'item'
                },
                legend: {
                    orient: 'vertical',
                    textStyle: {
                        //The style of the legend text
                        color: '#858d98'
                    },
                    height:"35%",
                    bottom:'bottom',
                },
                color: data_color_hex_code,

                series: [{
                    name: 'Thời Gian (h)',
                    type: 'pie',
                    radius: '95%',
                    bottom: "50%",
                    stillShowZeroSum: false,
                    data: data_chart_pie,
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }],
                textStyle: {
                    fontFamily: 'Poppins, sans-serif'
                }
            };
            option && myChart.setOption(option);

            myChart.on('click', params => {
                if (params.data.plan_color_id) {
                    setUpModalPlanColor(true, {
                        id: params.data.plan_color_id,
                        hex_code: params.color,
                        category_name: params.data.name,
                    });
                    $(`#modalPlanColor`).modal('toggle');

                }
            });
       }

        function setUpModalPlanColor(modeEdit, color_info = null) {
            $(`#modalPlanColorContent`).empty();
            input_str = 
            `
                <div class="modal-header p-3 bg-soft-info">
                    <h5 class="modal-title" id="modal-title">${modeEdit ? "Sửa Màu" : "Tạo Màu"}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
                </div>

                <div class="modal-body p-4">
                    
                    ${!modeEdit ? '' : 
                        `
                        <div class = "row justify-content-end" style = "margin-top:-4%; margin-bottom:-2%">
                            <a id = "deletePlanColorButtonInEdit" style="width:7%;" href="#" noref>
                                <i class="ri-delete-bin-line align-middle fs-16 me-2"></i>
                            </a>
                        </div>
                        `
                    } 
                    <div class="col-12">
                        <div class="mb-3">
                            <label class="form-label"> Tên</label>
                            <input class="form-control d-block" placeholder="Nhập Tên" type="text" id="plan_color_category_name" value = "${modeEdit ? color_info.category_name : ""}">
                        </div>

                    </div>

                    <div class="col-12">
                        <div class="mb-3">
                            <label class="form-label">Chọn Màu</label>
                            <input hidden id = "plan_color_hex_code_value">
                            <input hidden id = "mode_edit" value = ${modeEdit ? '1' : '0'}>
                            <div class = "row">
                                <div style = "width:50%"> 
                                    <div id="classic-colorpicker"></div>
                                </div>
                                <div class = "row justify-content-end" style = "width:50%"> 
                                    <a style="width:35%" href="#" noref>
                                        <button type="button" id="savePlanColorButton" class="btn btn-success btn-label waves-effect waves-light">
                                            <i class="ri-save-3-line label-icon align-middle fs-16 me-2"></i>Lưu</button>
                                    </a>

                                
                                </div>
                            </div>    

                        </div>

                    </div>
                </div>
            `
           
            $(`#modalPlanColorContent`).append(input_str);

            var color_picker = Pickr.create({ el: "#classic-colorpicker",
                theme: "classic", // or 'monolith', or 'nano'
                default: modeEdit ? color_info.hex_code : DefaultPlanColorHexCode,
                swatches: [
                    "rgba(244, 67, 54, 1)",
                    "rgba(233, 30, 99, 0.95)",
                    "rgba(156, 39, 176, 0.9)",
                    "rgba(103, 58, 183, 0.85)",
                    "rgba(63, 81, 181, 0.8)",
                    "rgba(33, 150, 243, 0.75)",
                    "rgba(3, 169, 244, 0.7)",
                    "rgba(0, 188, 212, 0.7)",
                    "rgba(0, 150, 136, 0.75)",
                    "rgba(76, 175, 80, 0.8)",
                    "rgba(139, 195, 74, 0.85)",
                    "rgba(205, 220, 57, 0.9)",
                    "rgba(255, 235, 59, 0.95)",
                    "rgba(255, 193, 7, 1)",
                ],

                components: {
                    // Main components
                    preview: true,
                    opacity: true,
                    hue: true,

                    // Input / output Options
                    interaction: {
                        hex: true,
                        rgba: true,
                        hsva: true,
                        input: true,
                        clear: true,
                        save: true,
                    },
                },
            });

            color_picker.on('save', function(color, instance) {
                $(`#plan_color_hex_code_value`).val(color == null ? null : color.toHEXA().toString());
            });

            color_picker.on('init', instance => {
                $(`#plan_color_hex_code_value`).val(instance._color == null ? null : instance._color.toHEXA().toString());
            });

            $(`#savePlanColorButton`).on('click', function(e) {
                e.preventDefault();

                if ($('#plan_color_hex_code_value').val() == null || $('#plan_color_hex_code_value').val() == DefaultPlanColorHexCode)
                    swal.fire({
                        icon: 'error',
                        title: `Vui lòng Chọn Màu Khác!`,
                        confirmButtonColor: '#3085d6',
                        showConfirmButton: false,
                    });
                else if ($('#plan_color_category_name').val() == '')
                    swal.fire({
                        icon: 'error',
                        title: `Vui lòng Nhập Tên!`,
                        confirmButtonColor: '#3085d6',
                        showConfirmButton: false,
                    });
                else
                    $.ajax({
                        method: 'POST',
                        url: "{{  route('plan_week.upsertPlanColor') }}",
                        data: {
                            hex_code : $('#plan_color_hex_code_value').val(),
                            basic_info_id: basic_info_id,
                            category_name : $('#plan_color_category_name').val(),
                            mode_edit: $('#mode_edit').val(),
                            plan_color_id : modeEdit ? color_info.id : null,
                        },
                        success: response => {
                            if (response.success) {
                                swal.fire({
                                    icon: 'success',
                                    title: `${response.message}`,
                                    confirmButtonColor: '#3085d6',
                                    showConfirmButton: false,
                                }).then(r => {
                                    $(`#modalPlanColor`).modal('hide');
                                    calendar.refetchEvents();
                                });
                            } else {
                                swal.fire({
                                    icon: 'error',
                                    title: `${response.message}`,
                                    confirmButtonColor: '#3085d6',
                                    showConfirmButton: false,
                                });
                            }
                        }
                    });
            });

            $(`#deletePlanColorButtonInEdit`).on('click', function(e) {
                e.preventDefault();
                deletePlanColor(color_info.id);
            });
        }

        function getPlanColorListAndSetUpModalTask(modeEdit, selected_plan_color_id) {
            $.ajax({
                method: 'GET',
                url: "{{  route('plan_week.getPlanColors') }}",
                data : {
                    'basic_info_id' : basic_info_id,
                },
                success: data => {
                   
                    const LimitStringLength = 7;
                    let input_str = ' <div class = "row">';
                             
                    data.forEach( plan_color => {
                        let temp_category_name = plan_color.category_name;

                        if (temp_category_name.length > LimitStringLength) {
                            temp_category_name = temp_category_name.slice(0, LimitStringLength);
                            temp_category_name += '...';
                        }

                        input_str += 
                        `
                        <div style = "width:20%">
                            <div>   
                                <input type ="radio" name ="task_color_id" value = ${plan_color.id} ${selected_plan_color_id == plan_color.id ? 'checked' : ''}
                                    data_plan_color_id = "${plan_color.id}" data_hex_code = "${plan_color.hex_code}" data_category_name = "${plan_color.category_name}">
                                <span> <i style = "color:${plan_color.hex_code}" class="  bx bxs-square-rounded label-icon fs-16 me-2"></i> </span>
                                <p>${temp_category_name}</p>
                            </div> 
                        </div>
                        `
                    });

                    input_str += 
                    `
                        <div style = "width:20%">
                            <div>   
                                <input type = "radio" name ="task_color_id" value = "-1" ${modeEdit  && selected_plan_color_id == null ? 'checked' : ''}>
                                    <span> <i style = "color:${DefaultPlanColorHexCode}" class="bx bxs-square-rounded label-icon fs-16 me-2"></i> </span>
                                    <p>Mặc Định</p>
                            </div> 
                        </div>
                    </div>
                    `;
                    if (modeEdit) {
                        $(`#PlanColorListInEditModal`).empty();
                        $(`#PlanColorListInEditModal`).html(input_str);
                    }
                    else {
                        $(`#PlanColorListInModal`).empty();
                        $(`#PlanColorListInModal`).html(input_str);
                    }
                }
            });
        }

        function appendModalBodyColorStr(modeEdit = false, selected_plan_color_id = null) {
            input_str = 
            `
                <div class="col-12">
                    <div class="mb-3">
                        <label for="task-color">Chọn Màu</label>
                            <div id = ${modeEdit ? "PlanColorListInEditModal" : "PlanColorListInModal" }>
                             
                            </div>
                            <div style = "width:22%">
                            
                                <div>   
                                    <a class = "addPlanColorButton" href="#" noref>
                                        <i class=" ri-add-circle-line label-icon align-middle fs-16 me-2"></i>
                                    </a> 
                                    <a class = "editPlanColorButton" href="#" noref>
                                        <i class="ri-edit-2-line label-icon align-middle fs-16 me-2"></i>
                                    </a> 
                                    <a class = "deletePlanColorButton" href="#" noref>
                                        <i class="ri-delete-bin-line label-icon align-middle fs-16 me-2"></i>
                                    </a> 
                                </div> 
                        
                            </div>
                        </div>
                    </div>
                </div>
            `;

            if (modeEdit) {
                $(`#editModalBodyColor`).empty();
                $(`#editModalBodyColor`).html(input_str);
            }
            else {
                $(`#addModalBodyColor`).empty();
                $(`#addModalBodyColor`).html(input_str);
            }
            
            getPlanColorListAndSetUpModalTask(modeEdit, selected_plan_color_id)


            $('.addPlanColorButton').on('click', function(e) {
                e.preventDefault();
                setUpModalPlanColor(false);

                if (!modeEdit)
                    $(`#modalAddDayTask`).modal('hide');
                else 
                    $(`#modalEditDayTask`).modal('hide');

                $(`#modalPlanColor`).modal('toggle');

                $(`#modalPlanColor`).on('hidden.bs.modal', function() {
                    if (!modeEdit) {
                        appendModalBodyColorStr();
                        $(`#modalAddDayTask`).modal('show');
                    } else {
                        appendModalBodyColorStr(true, selected_plan_color_id);
                        $(`#modalEditDayTask`).modal('show');
                    }
                    $(`#modalPlanColor`).off();
                });
            });

            $('.editPlanColorButton').on('click', function(e) {
                e.preventDefault();
                task_color_input_html = document.querySelector('input[name="task_color_id"]:checked');
                if (task_color_input_html == null || task_color_input_html.getAttribute('data_plan_color_id') == undefined) 
                     swal.fire({
                        icon: 'error',
                        title: `Vui lòng Chọn Màu Khác!`,
                        confirmButtonColor: '#3085d6',
                        showConfirmButton: false,
                    });
                else {
                    setUpModalPlanColor(true, {
                        id : task_color_input_html.getAttribute('data_plan_color_id'),
                        hex_code : task_color_input_html.getAttribute('data_hex_code'),
                        category_name : task_color_input_html.getAttribute('data_category_name'),
                    });
                    if (!modeEdit)
                        $(`#modalAddDayTask`).modal('hide');
                    else 
                        $(`#modalEditDayTask`).modal('hide');

                    $(`#modalPlanColor`).modal('toggle');

                    $(`#modalPlanColor`).on('hidden.bs.modal', function() {
                        if (!modeEdit) {
                            appendModalBodyColorStr();
                            $(`#modalAddDayTask`).modal('show');
                        } else {
                            appendModalBodyColorStr(true, selected_plan_color_id);
                            $(`#modalEditDayTask`).modal('show');
                        }
                        $(`#modalPlanColor`).off();
                    });
                }
            });

            $('.deletePlanColorButton').on('click', e => {
                e.preventDefault();
                task_color_input_html = document.querySelector('input[name="task_color_id"]:checked');
                if (task_color_input_html == null || task_color_input_html.getAttribute('data_plan_color_id') == undefined) 
                     swal.fire({
                        icon: 'error',
                        title: `Vui lòng Chọn Màu Khác!`,
                        confirmButtonColor: '#3085d6',
                        showConfirmButton: false,
                    });
                else {
                    deletePlanColor(task_color_input_html.getAttribute('data_plan_color_id'), modeEdit, selected_plan_color_id);
                }
            });
        }

        function deletePlanColor(plan_color_id, modeEdit = false, selected_plan_color_id = null) {
            swal.fire({
                title: 'Bạn muốn xóa dữ liệu này ?',
                text: "Lưu ý : dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                icon: 'warning',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Đồng ý',
                showDenyButton: true,
                denyButtonText: 'Hủy bỏ',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        method : 'POST',    
                        url: "{{  route('plan_week.deletePlanColor') }}",
                        data: {
                            plan_color_id : plan_color_id,
                        },
                        success: response => {
                            Swal.fire({
                                icon: 'success',
                                title: `${response.message}`,
                                confirmButtonColor: '#3085d6',
                                showConfirmButton: true,
                                confirmButtonText: 'Xác Nhận',
                            })

                            getPlanColorListAndSetUpModalTask(modeEdit, selected_plan_color_id);
                            $('#modalPlanColor').modal('hide');
                            calendar.refetchEvents()
                        }
                    });
                } else if (result.isDenied) {
                    Swal.fire('Dữ liệu được giữ lại', '', 'info')
                }
            });
        }
    

        function toggleModalView(eventClickInfo ) {

            const LimitEventLongTitle = 50;
            const event = eventClickInfo.event;

            const event_date_str    = event.start.toISOString().slice(0,10);
            const event_start_time  = event.start.toTimeString().slice(0,5);
            const event_end_time    = event.end.toTimeString().slice(0,5);

            let temp_event_title = event.title;

            if (temp_event_title.length > LimitEventLongTitle) {
                temp_event_title = temp_event_title.slice(0, LimitEventLongTitle) + '...';
            }
            input_str = `
                <div class="modal-header p-3 bg-soft-info">
                    <h5 class="modal-title" id="modal-title">${temp_event_title}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body p-4" >
                    <div class="text-end">
                        <a href="#" class="btn btn-sm btn-soft-primary" id="eventEditButton" role="button">Edit</a>
                    </div>
                    <div class="event-details">
                        <div class="d-flex mb-2">
                            <div class="flex-grow-1 d-flex align-items-center">
                                <div class="flex-shrink-0 me-3">
                                    <i class="ri-calendar-event-line text-muted fs-16"></i>
                                </div>
                                <div class="flex-grow-1">
                                    <h6 class="d-block fw-semibold mb-0" id="event-start-date-tag">${event_date_str}</h6>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mb-2">
                            <div class="flex-shrink-0 me-3">
                                <i class="ri-time-line text-muted fs-16"></i>
                            </div>
                            <div class="flex-grow-1">
                                <h6 class="d-block fw-semibold mb-0"><span id="event-timepicker1-tag">${event_start_time}</span> - <span id="event-timepicker2-tag">${event_end_time}</span></h6>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mb-2">
                            <div class="flex-shrink-0 me-3">
                                <i class="ri-map-pin-line text-muted fs-16"></i>
                            </div>
                            <div class="flex-grow-1">
                                <h6 class="d-block fw-semibold mb-0"> <span id="event-location-tag">${event.extendedProps.location ?? 'Chưa Có'}</span></h6>
                            </div>
                        </div>
                        <div class="d-flex mb-3">
                            <div class="flex-shrink-0 me-3">
                                <i class="ri-discuss-line text-muted fs-16"></i>
                            </div>
                            <div class="flex-grow-1">
                                <p class="d-block text-muted mb-0" id="event-description-tag">${event.extendedProps.description ?? 'Chưa Có'}</p>
                            </div>
                        </div>
                    </div>
                    
                    <!--end row-->
                    <div class="hstack gap-2 justify-content-end">
                        <button type="button" class="btn btn-soft-danger" id="eventDeleteButton"><i class="ri-close-line align-bottom"></i> Delete</button>
                    </div>
                </div>
            `
            $(`#modalViewDayTaskContent`).html(input_str);

            $(`#eventEditButton`).on('click', function(e) {
                e.preventDefault();
                $(`#modalViewDayTask`).modal('toggle');
                toggleModalEdit(event);
            });3

            $('#eventDeleteButton').on('click', function(e) {
                e.preventDefault();
                swal.fire({
                        title: 'Bạn muốn xóa dữ liệu này ?',
                        text: "Lưu ý : dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                        icon: 'warning',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Đồng ý',
                        showDenyButton: true,
                        denyButtonText: 'Hủy bỏ',
                    })
                    .then((result) => {
                        if (result.isConfirmed) {

                            $.ajax({
                                method: "DELETE",
                                url: "{{  route('plan_week.deletePlanDayTimeslot') }}",
                                data: {
                                    plan_day_timeslot_id: event.id 
                                },
                                success: response => {
                                    Swal.fire({
                                        icon: 'success',
                                        title: `${response.message}`,
                                        confirmButtonColor: '#3085d6',
                                        showConfirmButton: true,
                                        confirmButtonText: 'Xác Nhận',
                                    }).then(r => {
                                        $(`#modalViewDayTask`).modal('toggle');
                                    });
                                    calendar.refetchEvents()
                                }
                            });
                           
                        } else if (result.isDenied) {
                            Swal.fire('Dữ liệu được giữ lại', '', 'info')
                        }
                    });
            });
            
            $(`#modalViewDayTask`).modal('toggle');
        }

        function toggleModalAdd(startDateStr, endDateStr = '') {

            const start_date_str   = startDateStr.slice(0,10);
            const startDateObject  = new Date(startDateStr);
            const endDateObject    = new Date(endDateStr);   

            const start_time_intial_value = startDateObject.toTimeString().slice(0,5);
            const end_time_intial_value   = endDateObject.toTimeString().slice(0,5);

            const input_str = 
            `
                <form id="formAddDayTask" method="POST" action="">
                    @csrf
                    <input type="hidden" id="basic_info_id" name="basic_info_id" value="${basic_info_id}">
                    <div class="row event-form">
                        <!--end col-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label class="form-label">Tên của Công Việc</label>
                                <input class="form-control d-block" placeholder="Nhập Tên Task" type="text" name="task_title" id="task-title">
                            </div>

                        </div>
                        <!--end col-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label>Ngày Của Công Việc</label>
                                <div class="input-group">
                                    <input type="text" id="task_date" name = "task_date" class="form-control datepicker" placeholder="Chọn Ngày" >
                                    <span class="input-group-text"><i class="ri-calendar-event-line"></i></span>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                        <div class="col-12" id="task-time">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label class="form-label">Giờ Bắt Đầu</label>
                                        <div class="input-group">
                                            <input id="start_time_picker" name ="task_start_time" type="text" class="form-control flatpickr flatpickr-input" placeholder="Chọn Giờ Bắt Đầu">
                                            <span class="input-group-text"><i class="ri-time-line"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label class="form-label">Giờ Kết Thúc</label>
                                        <div class="input-group">
                                            <input id="end_time_picker" name ="task_end_time" type="text" class="form-control flatpickr flatpickr-input" placeholder="Chọn Giờ Kết Thúc">
                                            <span class="input-group-text"><i class="ri-time-line"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="task-location">Địa Điểm</label>
                                <div>
                                    <input type="text" class="form-control d-block" name="task_location" id="task-location" placeholder="Nhập Địa Điểm">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3">
                                <label class="form-label">Ghi Chú</label>
                                <textarea class="form-control d-block" name = "task_description" id="task_description" placeholder="Nhập Ghi Chú" rows="3" spellcheck="false"></textarea>
                            </div>
                        </div>

                    
                        <div id = "addModalBodyColor">
                         
                        </div>
                   
                    </div>
                    <!--end row-->
                    <div class="hstack gap-2 justify-content-end">
                        <button id ="saveDayTaskButton" type="submit" class="btn btn-success" id="btn-save-event">Thêm</button>
                    </div>
                </form>
            `;
            $(`#modalAddDayTaskBody`).html(input_str);

            appendModalBodyColorStr();

            $(`#task_date`).datepicker({
                locale: 'vi',
                format: 'yyyy-mm-dd',
            });
            $(`#task_date`).val(start_date_str);
        
            var start_time_picker = intializeTimePicker(earliestTime, 
                addMinutesToTime(-minute_increment, latestTime), 
                startDateObject.getHours(), startDateObject.getMinutes(), minute_increment, true);

            var end_time_picker = intializeTimePicker(end_time_intial_value, latestTime, 
                endDateObject.getHours(), endDateObject.getMinutes(), minute_increment, false);
            
            $(`#start_time_picker`).val(start_time_intial_value);
            $(`#end_time_picker`).val(end_time_intial_value);

            $(`#start_time_picker`).on('change', function() {
                validateTime(end_time_picker);
            });

            $(`#saveDayTaskButton`).on('click', function(e) {
                e.preventDefault();
                $.ajax({
                    method: 'POST',
                    url: "{{ route('plan_week.addPlanDayTimeslot') }}",
                    data: $(`#formAddDayTask`).serialize(),
                    success: response => {
                        if (response.success) {
                            swal.fire({
                                icon: 'success',
                                title: `${response.message}`,
                                confirmButtonColor: '#3085d6',
                                showConfirmButton: true,
                                confirmButtonText: 'Xác Nhận',
                            }).then( result => {
                                    $(`#modalAddDayTask`).modal('toggle');
                                }
                            );
                            calendar.refetchEvents()

                        }
                        else {
                            swal.fire({
                                icon: 'error',
                                title: `${response.message}`,
                                confirmButtonColor: '#3085d6',
                                showConfirmButton: false,
                            });
                        }
                    }
                });

            });
            
            $(`#modalAddDayTask`).modal('toggle');
        }

        function toggleModalEdit(event) {
       
            const event_date_str    = event.start.toISOString().slice(0,10)

    
            const eventStartObject  = new Date(event.start);
            const eventEndObject    = new Date(event.end);
            
            const event_start_time  = event.start.toTimeString().slice(0,5);
            const event_end_time    = event.end.toTimeString().slice(0,5);

            const input_str = 
            `
                <form id="formEditDayTask" method="POST" action="">
                    @csrf
                    <div class="row event-form">
                        <!--end col-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label class="form-label">Tên của Công Việc</label>
                                <input class="form-control d-block" placeholder="Nhập Tên Task" type="text" name="task_title" id="task-title" value = "${event.title}">
                            </div>

                        </div>
                        <!--end col-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label>Ngày Của Công Việc</label>
                                <div class="input-group">
                                    <input type="text" id="task_date-edit" name = "task_date" class="form-control datepicker" placeholder="Chọn Ngày" value = "${event_date_str}">
                                    <span class="input-group-text"><i class="ri-calendar-event-line"></i></span>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                        <div class="col-12" id="task-time">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label class="form-label">Giờ Bắt Đầu</label>
                                        <div class="input-group">
                                            <input id="start_time_picker-edit" name ="task_start_time" type="text" class="form-control flatpickr flatpickr-input" placeholder="Chọn Giờ Bắt Đầu" value = ${event_start_time} >
                                            <span class="input-group-text"><i class="ri-time-line"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label class="form-label">Giờ Kết Thúc</label>
                                        <div class="input-group">
                                            <input id="end_time_picker-edit" name ="task_end_time" type="text" class="form-control flatpickr flatpickr-input" placeholder="Chọn Giờ Kết Thúc" value = ${event_end_time}>
                                            <span class="input-group-text"><i class="ri-time-line"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="task-location">Địa Điểm</label>
                                <div>
                                    <input type="text" class="form-control d-block" name="task_location" id="task-location" placeholder="Nhập Địa Điểm" value = ${event.extendedProps.location ?? ''}>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                        <input type="hidden" id="basic_info_id" name="basic_info_id" value="${basic_info_id}">
                        <input type="hidden" id="plan_day_timeslot_id" name="plan_day_timeslot_id" value="${event.id}">

                        <div class="col-12">
                            <div class="mb-3">
                                <label class="form-label">Ghi Chú</label>
                                <textarea class="form-control d-block" name = "task_description" id="task_description" placeholder="Nhập Ghi Chú" rows="3" spellcheck="false">${event.extendedProps.description ?? ''}</textarea>
                            </div>
                        </div>

                        <div id = "editModalBodyColor">
                         
                         </div>
                     
                    </div>

                    <div class="hstack gap-2 justify-content-end">
                        <button id ="editDayTaskButton" type="submit" class="btn btn-success" id="btn-save-event">Sửa</button>
                    </div>
                </form>
            `;
            $(`#modalEditDayTaskBody`).html(input_str);

            appendModalBodyColorStr(true, event.extendedProps.plan_color_id);

            $(`#task_date-edit`).datepicker({
                locale: 'vi',
                format: 'yyyy-mm-dd',
            });

            const initialLimitEndTimePicker = addMinutesToTime(minute_increment, event_start_time);


            var start_time_picker = intializeTimePicker(earliestTime, 
                addMinutesToTime(-minute_increment, latestTime), 
                eventStartObject.getHours(), eventStartObject.getMinutes(), minute_increment, true, true);

            var end_time_picker = intializeTimePicker(initialLimitEndTimePicker, latestTime, 
                eventEndObject.getHours(), eventEndObject.getMinutes(), minute_increment, false, true);

            $('#start_time_picker-edit').on('change', function() {
                validateTime(end_time_picker, true);
            });

            $(`#editDayTaskButton`).on('click', function(e) {
                e.preventDefault();
   
                $.ajax({
                    method: 'POST',
                    url: "{{ route('plan_week.addPlanDayTimeslot') }}",
                    data: $(`#formEditDayTask`).serialize(),
                    success: response => {
                        if (response.success) {
                            swal.fire({
                                icon: 'success',
                                title: `${response.message}`,
                                confirmButtonColor: '#3085d6',
                                showConfirmButton: true,
                                confirmButtonText: 'Xác Nhận',
                            }).then( result => {
                                    $(`#modalEditDayTask`).modal('toggle');
                                }
                            );
                            calendar.refetchEvents()

                        }
                        else {
                            swal.fire({
                                icon: 'error',
                                title: `${response.message}`,
                                confirmButtonColor: '#3085d6',
                                showConfirmButton: false,
                            });
                        }
                    }
                });

            });

            $(`#modalEditDayTask`).modal('toggle');


        }

        function addMinutesToTime(added_minutes, time_str) {
            let hours   = parseInt(time_str.slice(0,2));
            let minutes = parseInt(time_str.slice(3));

            minutes += added_minutes;
            if (minutes >= 60) { 
                hours   += 1;
                minutes -= 60;
            } else if (minutes < 0) {
                hours   -= 1;
                minutes += 60;
            }
            return ("0" + hours).slice(-2) + ':' + ("0" + minutes).slice(-2);
        }

        function compareTime(time_str_1, time_str_2) {
            let hours_1   =  parseInt(time_str_1.slice(0,2));
            let minutes_1 = parseInt(time_str_1.slice(3));

            let hours_2   =  parseInt(time_str_2.slice(0,2));
            let minutes_2 = parseInt(time_str_2.slice(3));

            return (hours_1 * 60 + minutes_1) >=  (hours_2 * 60 + minutes_2);

        }

        function intializeTimePicker(minTime, maxTime, defaultHour, defaultMinute, minute_increment, create_mode, mode_edit = false) {

            let idStr = create_mode ? '#start_time_picker' : '#end_time_picker';
            idStr += (mode_edit ? '-edit' : '');
            var time_picker = $(idStr).flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                minTime: minTime,
                maxTime: maxTime,
                time_24hr: true,
                allowInput: true,
                defaultHour: defaultHour,
                defaultMinute: defaultMinute,
                minuteIncrement: minute_increment 
            });

            return time_picker;
        }

        function validateTime(end_time_picker, mode_edit = false ) {
            const idStartTimePicker  = '#start_time_picker' + ((mode_edit) ? '-edit' : '');
            const idEndimePicker  = '#end_time_picker' + ((mode_edit) ? '-edit' : '');

            end_time_picker.destroy();
            const temp_time = addMinutesToTime(minute_increment, $(idStartTimePicker)[0].value);

            if (compareTime($(idStartTimePicker)[0].value, $(idEndimePicker)[0].value)) {
                end_time_picker.destroy();
                $(idEndimePicker).val(temp_time);
            }

            
            end_time_picker = intializeTimePicker(temp_time, latestTime, 
                    parseInt(temp_time.slice(0,2)), parseInt(temp_time.slice(3)), minute_increment, false, mode_edit);
        }

        function populateForm(plan_week, moment_week_start) {

            const quarter = moment_week_start.quarter();
            const week_in_quarter = moment_week_start.isoWeeks() - 13 * (quarter - 1);
            const year = moment_week_start.year();
            
            $('#year_datepicker').val(year);
            $('#quarter').val(quarter == 1 ? 'I' : (quarter == 2 ? 'II' : (quarter == 3 ? 'III' : "IV")));
            $('#week_in_quarter').val(week_in_quarter);

            for (let i = 1; i <= 5; i ++) {
                $(`#week_aspect${i}`).val('');
                $(`#week_aspect${i}`).val(plan_week ? plan_week['five_weekly_aspects'][i-1] : '');
                $(`#week_goal${i}`).val(plan_week ? plan_week['five_weekly_goals'][i-1] : '');


            }
            

        }
    });

   
</script>

@endsection