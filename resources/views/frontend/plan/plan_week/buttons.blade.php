<div class = "row">
    <div class="col-2 mb-3" style = "width:14.4%">
        <a href = "{{ route('plan_week.index') }}">
        <button id="returnButton" type="button" class="btn btn-danger waves-effect waves-light">
                <i class="ri-arrow-go-back-line label-icon align-middle fs-16 me-2"></i> Về Danh Sách KH Tuần</button>
        </a>
    </div>

    <div class="col-2 mb-3" style = "width:12%; margin-left:-2%">
        <a href = "{{ route('plan_quarter.view', $dataInit['planWeek'] -> plan_quarter_id) }}">
        <button id="returnButton" type="button" class="btn btn-secondary waves-effect waves-light">
                <i class="ri-arrow-go-back-line label-icon align-middle fs-16 me-2"></i> Về Kế Hoạch Quý</button>
        </a>
    </div>

    <div style= width:65%></div>

    @if ($dataInit['modeEdit']) 
    <div class="col-2 mb-3" style = "width:10.6%;">
        <a href = "{{ route('plan_week.view', $dataInit['planWeek'] -> id) }}">
        <button id="returnButton" type="button" class="btn btn-success waves-effect waves-light">
                <i class="ri-arrow-go-back-line label-icon align-middle fs-16 me-2"></i> Về Chế Độ Xem</button>
        </a>
    </div>

    @else 
    <div class="col-2 mb-3" style = "width:10.5%;">
        <a href = "{{ route('plan_week.edit', $dataInit['planWeek'] -> id) }}">
        <button id="returnButton" type="button" class="btn btn-success waves-effect waves-light">
                <i class="ri-arrow-go-back-line label-icon align-middle fs-16 me-2"></i> Về Chế Độ Sửa</button>
        </a>
    </div>
    @endif
</div>