@extends('layouts.fe.master2')
@section('title')
    @lang('translation.team.index')
@endsection
@section('css')
    <!--datatable css-->
    <link href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" rel="stylesheet" type="text/css" />
    <!--datatable responsive css-->
    <link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" rel="stylesheet"
        type="text/css" />
    <link href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')

    <div class="row">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <h5 class="card-title mb-0">BỘ PHẬN </h5>
                <div class="btn-group w-50">
                    <button type="button " class="btn btn-info dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Xem Mã Phòng Ban</button>
                    <div class="dropdown-menu w-100">
                      
                        @foreach ($rooms as $room )
                        <div class="row ">
                            <a class="dropdown-item w-50 text-center" href="#">{{ $room->id }}</a>
                            <a class="dropdown-item w-50 text-center" href="#">{{ $room->name }}</a>
                        </div>                            
                        @endforeach
                    </div>
                    </div><!-- /btn-group -->
                <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#exampleModalgrid">
                    Thêm bộ phận
                </button>
            </div>




            <!-- Modals Add-->
            <form action="{{ route('team.create') }}" method="post">
                @csrf
                <div class="modal fade" id="exampleModalgrid" tabindex="-1" aria-labelledby="exampleModalgridLabel"
                    aria-modal="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalgridLabel">Thêm Bộ Phận</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="javascript:void(0);">
                                    <div class="row g-3">
                                        <div class="col-xxl-12">
                                            <div>
                                                <label for="name" class="form-label">Tên bộ phận</label>
                                                <input type="text" class="form-control" id="name" name="name"
                                                    placeholder="(Điền thông tin)">
                                            </div>
                                            <div>
                                                <label for="room_id" class="form-label">Tên Phòng</label>
                                                <select class="form-control" id = "room_id" name="room_id">
                                                    @foreach ($rooms as $room)
                                                    <option value="{{ $room->id }}">{{ $room->name }}</option> 
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-check form-switch form-switch-lg" dir="ltr">
                                            <label for="name" class="form-label">Bộ phận Quản trị chính của Phòng?</label>
                                            <input type="checkbox" class="form-check-input" name="is_main_flag"
                                                placeholder="(Điền thông tin)">
                                        </div>

                                        <!--end col-->
                                        <div class="col-lg-12">
                                            <div class="hstack gap-2 justify-content-end">
                                                <button type="submit" class="btn btn-primary">Lưu</button>
                                            </div>
                                        </div>
                                        <!--end col-->
                                    </div>
                                    <!--end row-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </form>



            <div class="card-body">
                <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle"
                    style="width:100%">

                    <thead>
                        <tr>
                            <!-- <th scope="col" style="width: 10px;"> -->
                                <!-- <div class="form-check"> -->
                                    <!-- <input class="form-check-input fs-15" type="checkbox" id="checkAll" value="option"> -->
                                <!-- </div> -->
                            <!-- </th> -->
                            <th>Mã Bộ Phận</th>
                            <th>Tên Bộ Phận</th>
                            <th>Tên Phòng</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($teams as $team)
                            <tr>
                                <!-- <th scope="row"> -->
                                    <!-- <div class="form-check"> -->
                                        <!-- <input class="form-check-input fs-15" type="checkbox" name="checkbox_1" -->
                                            <!-- value="option1"> -->
                                    <!-- </div> -->
                                <!-- </th> -->
                             
                             
                                <td>{{ $team->id }}</td>
                                <td>{{ $team->name }}</td>
                                
                                <td>{{ $team->room->name}}</td>
                                
                                <td>
                                    <div class="dropdown d-inline-block">
                                        <button class="btn btn-soft-secondary btn-sm dropdown" type="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="ri-more-fill align-middle"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-end">
                                            <a href="{{ route('team.edit', $team->id) }}"
                                                class="dropdown-item remove-item-btn">
                                                <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Sửa
                                            </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('team.delete', $team->id) }}"
                                                    class="dropdown-item remove-item-btn">
                                                    <i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Xóa
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>


    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection
