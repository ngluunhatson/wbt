@extends('layouts.fe.master2')
@section('title')
@lang('translation.personal')
@endsection
@section('css')
<link href="{{ URL::asset('assets/libs/quill/quill.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<style>
    .table td p {
        margin: auto;
    }

    .table>:not(caption)>*>* {
        padding: 6px;
    }

    .table th {
        font-weight: 700;
        text-align: center;
    }

    @print {
        @page :footer {
            display: none
        }

        @page :header {
            display: none
        }
    }

    @media print {

        /* .address {
            margin-top: 25% !important;
        } */
        body {
            margin-bottom: 0 !important;
        }

        #btnPrint {
            display: none;
        }

        .title {
            width: 400px;
        }

        .btn-icon {
            display: none;
        }

        .result-confirm {
            margin-top: 25%;
        }
    }
</style>
<div class="row justify-content-end pe-3 mb-3 mt-3">
    <div class="col-3 justify-content-between d-flex">
    <a href="#" class="mr-4">
        <button type="button" id="btnPrint" class="btn btn-primary btn-label waves-effect waves-light">
            <i class="ri-printer-line label-icon align-middle fs-16 me-2"></i>IN</button>
    </a>

    <!-- <a href="{{route('tablekpi.exportFile', $dataInit['kpi']['id'])}}">
        <button type="button" id="btnPrint" class="btn btn-success btn-label waves-effect waves-light">
            <i class="ri-printer-line label-icon align-middle fs-16 me-2"></i>Xuất File</button>
    </a> -->
    </div>
</div>
<div class="justify-content-end pe-3 mb-3 d-flex">
    <h3></h3>
    
</div>
<div class="d-flex justify-content-between">
    <div style="width: 55%;">
        <h2 class="text-left mt-4 fw-bold" style="width:400px">Bảng KPIS</h2>
        <div class="p-4">
            <div class="">
                <label for="room" class="form-label col-2">Thời gian</label>
                <span class="form-label" style="font-size: 15px;">
                    Quý {{ !empty($dataInit['kpi']['quarter']) ? $dataInit['kpi']['quarter'] : 'Chưa có' }} Năm {{ !empty($dataInit['kpi']['year']) ? $dataInit['kpi']['year'] : 'Chưa có' }}
                </span>
            </div>
            <div>
                <label for="room" class="form-label col-2">Phòng</label>
                <span class="form-label" style="font-size: 15px;">
                    {{ !empty($dataInit['kpi']['room']) ? $dataInit['kpi']['room']['name'] : 'Chưa có' }}
                </span>
            </div>
            <div>
                <label for="room" class="form-label col-2">Bộ phận</label>
                <span class="form-label" style="font-size: 15px;">
                    {{ !empty($dataInit['kpi']['team']) ? $dataInit['kpi']['team']['name'] : 'Chưa có' }}
                </span>
            </div>
            <div>
                <label for="room" class="form-label col-2">Chức danh</label>
                <span class="form-label" style="font-size: 15px;">
                    {{ !empty($dataInit['kpi']['position']) ? $dataInit['kpi']['position']['name'] : 'Chưa có' }}
                </span>
            </div>
        </div>
    </div>
    <div style="width:35% ; height: fit-content;">
        <img style="object-fit: contain; width: 100%;height: 100%;" src="{{ url('/uploads/common/logo.png') }}">
    </div>
</div>
<!--end col Quốc tịch-->
<div class="row ">
    <div class="row ">
        <div class="col-12 p-4">
            <div class="card">
                <div class="card-body">
                    <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                        <colgroup>
                            <col width="5%">
                            <col width="20%">
                            <col width="23%">
                            <col width="8%">
                            <col width="23%">
                            <col width="8%">
                            <col width="23%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Hạng Mục</th>
                                <th>Nội Dung KPIs</th>
                                <th>Tỷ Trọng</th>
                                <th>Thực Tế</th>
                                <th>Kết Quả</th>
                                <th>Ghi Chú</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1;
                            $totalPro = 0;
                            $totalReal = 0 ?>
                            @for ($i; $i <= 5; $i++) <tr>
                                <td class="text-center">{{ $i }}</td>
                                @switch($i)
                                @case(1)
                                <input hidden name="categories_{{ $i }}" value="Deadline">
                                <td class="text-center">Deadline</td>
                                @break

                                @case(2)
                                <input hidden name="categories_{{ $i }}" value="Số lượng">
                                <td class="text-center">Số lượng</td>
                                @break

                                @case(3)
                                <input hidden name="categories_{{ $i }}" value="Văn hóa">
                                <td class="text-center">Văn hóa</td>
                                @break

                                @case(4)
                                <input hidden name="categories_{{ $i }}" value="Chất lượng">
                                <td class="text-center">Chất lượng</td>
                                @break
                                @case(5)
                                <td class="text-center" style="font-size: 15px;" colspan="2">Kết quả KPIs</td>
                                @break
                                @endswitch
                                @if(array_key_exists($i - 1, $dataInit['detailKpi']))
                                @php
                                $totalPro += $dataInit['detailKpi'][$i - 1]['proportion'];
                                $totalReal += $dataInit['detailKpi'][$i - 1]['result'];
                                @endphp
                                <td>
                                    {!! $dataInit['detailKpi'][$i - 1]['content_kpis'] !!}
                                </td>

                                <td class="text-center">
                                    {{ $dataInit['detailKpi'][$i - 1]['proportion'] }} %
                                </td>
                                <td>
                                    {{ $dataInit['detailKpi'][$i - 1]['reality'] }}
                                </td>
                                <td class="text-center">
                                    {{ $dataInit['detailKpi'][$i - 1]['result'] }} %
                                </td>
                                <td>
                                    {{ $dataInit['detailKpi'][$i - 1]['note'] }}
                                </td>
                                @else
                                <td class="text-center" id="totalProportion">{{ $totalPro }} %</td>
                                <td></td>
                                <td class="text-center" id="totalReal">{{ $totalReal }} %</td>
                                <td></td>
                                @endif
                                </tr>
                                @endfor

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body result-confirm">

                    <div class="row mb-5">
                        <div class="col-12 ms-5">
                            <h4 class="">Kết quả bạn đạt được là: </h4>
                            <div class="col-12 d-flex mt-2">
                                <div class=" form-check" style="width: fit-content;">
                                    <input disabled class="form-check-input fs-15" type="checkbox" id="low" value="low" name="low">
                                </div>
                                <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Nếu đạt
                                    từ
                                    50% - 69%: xét duyệt lại việc tái ký hợp đồng lao động.</label>
                            </div>
                            <div class="col-12 d-flex mt-2">
                                <div class=" form-check" style="width: fit-content;">
                                    <input disabled class="form-check-input fs-15" type="checkbox" id="medium" value="medium" name="medium">
                                </div>
                                <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Nếu đạt
                                    từ
                                    70% - 89%: tái ký hợp đồng lao động và được đào tạo.</label>
                            </div>
                            <div class="col-12 d-flex mt-2">
                                <div class=" form-check" style="width: fit-content;">
                                    <input disabled class="form-check-input fs-15" type="checkbox" id="hight" value="hight" name="hight">
                                </div>
                                <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Nếu đạt
                                    từ
                                    90% trở lên: tái ký hợp đồng lao động và xét duyệt vị trí, tăng lương.</label>
                            </div>
                        </div>
                        <div class="col-12 d-flex mt-2 justify-content-end">
                            @if (!empty($dataInit['kpi']['delivery_date']))
                            <label for="confirm" class=" form-label mb-0 me-5 " style="margin-top: 2px">
                                @php
                                $date = \Carbon\Carbon::parse($dataInit['kpi']['delivery_date']);
                                @endphp
                                Ngày duyệt giao KPIs : Ngày {{ $date->day }} Tháng {{ $date->month }} Năm {{ $date->year }}</label>
                            @endif
                        </div>
                        <div class="col-12 d-flex mt-2 justify-content-end">
                            @if (!empty($dataInit['kpi']['review_date']))
                            <label for="confirm" class=" form-label mb-0 me-5 " style="margin-top: 2px">
                                @php
                                $date = \Carbon\Carbon::parse($dataInit['kpi']['review_date']);
                                @endphp
                                Ngày duyệt kết quả KPIs : Ngày {{ $date->day }} Tháng {{ $date->month }} Năm {{ $date->year }}</label>
                            @endif
                        </div>
                    </div>

                    <div class="row mb-5">
                        <div class="col-3 d-flex justify-content-center">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <h5 class="fw-bold">Người giao KPIs</h5>
                                </div>
                                <div class="col-12 text-center">
                                    <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ $dataInit['kpi']['user']['name'] }}</label>
                                </div>
                                <div class="col-12 text-center mt-2">
                                    @if (!in_array($dataInit['kpi']['status'], [1, 5, 11]))
                                    <label for="firstNameinput" class="form-label mb-0 mt-4 ">Đã xác nhận</label>
                                    @elseif ($dataInit['kpi']['status'] == 11)
                                    <label for="firstNameinput" class="form-label mb-0 mt-4 ">Từ chối</label>
                                    <div>
                                        <label>Lý do : {{ $dataInit['kpi']['reason'] }}</label>
                                    </div>
                                    @else
                                    <label for="firstNameinput" class="form-label mb-0 mt-4 ">Chưa xác nhận</label>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--end col Người giao KPIs-->
                        @php
                        $idConfirmer = [];
                        $id= '';
                        $idConfirmerNext;
                        $type='';
                        if (in_array($dataInit['kpi']['status'], [2,3,4,9,10])) {
                        $typeconfirm = 1;
                        } else {
                        $typeconfirm = 2;
                        }

                        @endphp
                        @foreach ($dataInit['kpi']['confirms'] as $key => $value)
                        @if($value['confirm']['type_confirm'] != 99)
                        <input name="confirm_id_{{ $key + 1 }}" value="{{ $value['confirm']['id'] }}" hidden>
                        <div class="col-3 d-flex justify-content-center">
                            <div class="row">
                                <div class="col-12 text-center">
                                    @switch($value['confirm']['type_confirm'])
                                    @case(1)
                                    <h5 class="fw-bold">Người nhận KPI</h5>
                                    @break
                                    @case(2)
                                    @if($dataInit['kpi']['status'] == 5)
                                    @php
                                    $idConfirmerNext =$value['confirm']['basic_info_id'];
                                    @endphp
                                    @endif
                                    @if ($value['confirm']['status'] == 0 && $dataInit['kpi']['status'] == 2 || $dataInit['kpi']['status'] == 6)
                                    @php
                                    $idConfirmer[] = $value['confirm']['basic_info_id'];
                                    $id = $value['confirm']['id'];
                                    $type = 2;
                                    @endphp
                                    @endif
                                    <h5 class="fw-bold">P.QT HC-NS</h5>
                                    @break
                                    @case(3)
                                    @if(empty($idConfirmerNext))
                                    @php
                                    $idConfirmerNext = $value['confirm']['basic_info_id'];
                                    @endphp
                                    @endif
                                    @if (empty($idConfirmer) && $value['confirm']['status'] == 0 && $dataInit['kpi']['status'] == 3 || $dataInit['kpi']['status'] == 7)
                                    @php
                                    $idConfirmer[] = $value['confirm']['basic_info_id'];
                                    $id = $value['confirm']['id'];
                                    $type = 3;
                                    $idConfirmerNext = '';
                                    @endphp
                                    @endif
                                    <h5 class="fw-bold">Ban Giám Đốc</h5>
                                    @break
                                    @endswitch
                                </div>
                                <div class="col-12 text-center">
                                    @switch($value['confirm']['type_confirm'])
                                    @case(1)
                                    <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] ?? 'Chưa có' }}</label>
                                    @break
                                    @case(2)
                                    @if(!empty($dataInit['hr']))
                                    <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] }}</label>
                                    @endif
                                    @break
                                    @case(3)
                                    @if(!empty($dataInit['manager']))
                                    <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] }}</label>
                                    @endif
                                    @break
                                    @endswitch
                                </div>
                                <div class="col-12 text-center mt-2">
                                    @if($value['confirm']['type_confirm'] != 1)
                                    @switch($value['confirm']['status'])
                                    @case(0)
                                    <label for="firstNameinput" class="form-label mb-0">{{ 'Chưa xác nhận' }}</label>
                                    @break
                                    @case(1)
                                    <label for="firstNameinput" class="form-label mb-0">{{ 'Đã xác nhận' }}</label>
                                    @break
                                    @case(2)
                                    <label for="firstNameinput" class="form-label mb-0">{{ 'Từ chối' }}</label>
                                    <div>
                                        <label>Lý do : {{ $value['confirm']['note']}}</label>
                                    </div>
                                    @break
                                    @endswitch
                                    @else
                                    <label for="firstNameinput" class="form-label mb-0"></label>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @elseif($value['confirm']['status'] == 0 && $dataInit['kpi']['status'] == 5)
                        @php
                        $idConfirmer[] = $value['confirm']['basic_info_id'];
                        $id = $value['confirm']['id'];
                        $type = 99;
                        @endphp
                        @endif
                        @endforeach
                        <!--end col Ban giám đốc-->
                        @if (in_array(auth()->user()->basic_info_id ?? auth()->id(), $idConfirmer))
                        <form method="POST" action="{{ route('tablekpi.updateStatus', $dataInit['kpi']['id']) }}" id="formConfirm">
                            @csrf
                            <input hidden name="confirmID" value="{{ $id }}">
                            <input hidden name="confimerIDNext" value="{{ $idConfirmerNext }}">
                            <input hidden name="reason" id="reasonConfirm">
                            <input hidden name="statusConfirm" id="statusConfirm" value="1">
                            <input hidden name="type" value="{{ $type }}">
                            <input hidden name="typeconfirm" value="{{ $typeconfirm }}">
                            <div class="row justify-content-center pe-3 mb-3 mt-3">
                                <div class="col-3 justify-content-between d-flex">
                                    <button data-id="1" type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content">
                                        <i class="ri-check-double-line label-icon align-middle fs-16 me-2"></i> Duyệt</button>
                                    <button data-id="2" type="submit" id="btnDenie" class="btn btn-danger waves-effect waves-light" style="width: fit-content">
                                        <i class="ri-error-warning-line label-icon align-middle rounded-pill fs-16 me-2 "></i> Không Duyệt</button>
                                </div>
                            </div>
                        </form>
                        @endif
                    </div>



                </div>
                <div class="card-body mt-5 address">
                    <p class="fw-bold m-0">ActionCOACH CBD Firm</p>
                    <p class="m-0">Địa chỉ: CBD Building, 90 - 92 Lê Thị Riêng, Phường Bến Thành, Quận 1, TP.HCM</p>
                    <p class="m-0">Hotline: 1800 8087</p>
                </div>
            </div>


        </div>
    </div>
</div>
{{-- </form> --}}
@endsection
@section('script')
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<!-- jQuery Library -->

<script src="{{ asset('/assets/js/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Tree plugin js -->
<script>
    const csrf = "{{ csrf_token() }}";
    $(document).ready(function() {
        $('#datepicker').datepicker({
            format: 'yyyy',
            startView: "years",
            minViewMode: "years",
        });


    });
</script>

<script>
    $(document).ready(function() {
        $('#btnPrint').on('click', function() {
            window.print()
        })
        var result_1 = $('#result_1').val()
        var result_2 = $('#result_2').val()
        var result_3 = $('#result_3').val()
        var result_4 = $('#result_4').val()
        total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
        $('#total').val(total)
        if (total) {
            $('#totalReal').text(total + '%')
        }
        checkbox("{!!$totalReal!!}")

        function checkbox(sum) {
            if (sum >= 50 && sum < 70) {
                document.getElementById("low").checked = true;
                document.getElementById("medium").checked = false;
                document.getElementById("hight").checked = false;
            } else if (sum >= 70 && sum < 90) {
                document.getElementById("low").checked = false;
                document.getElementById("medium").checked = true;
                document.getElementById("hight").checked = false;
            } else if (sum >= 90) {
                document.getElementById("low").checked = false;
                document.getElementById("medium").checked = false;
                document.getElementById("hight").checked = true;
            } else {
                document.getElementById("low").checked = false;
                document.getElementById("medium").checked = false;
                document.getElementById("hight").checked = false;
            }
        }

        $('#result_1').change(function() {
            var result_1 = $('#result_1').val();
            var result_2 = $('#result_2').val();
            var result_3 = $('#result_3').val();
            var result_4 = $('#result_4').val();
            $('#total').val(Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4))
            var total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
            checkbox(total);
        })

        $('#result_2').on('change', function() {
            var result_1 = $('#result_1').val();
            var result_2 = $('#result_2').val();
            var result_3 = $('#result_3').val();
            var result_4 = $('#result_4').val();
            $('#total').val(Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4))
            var total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
            checkbox(total);
        })

        $('#result_3').on('change', function() {
            var result_1 = $('#result_1').val();
            var result_2 = $('#result_2').val();
            var result_3 = $('#result_3').val();
            var result_4 = $('#result_4').val();
            $('#total').val(Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4))
            var total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
            checkbox(total);
        })

        $('#result_4').on('change', function() {
            var result_1 = $('#result_1').val();
            var result_2 = $('#result_2').val();
            var result_3 = $('#result_3').val();
            var result_4 = $('#result_4').val();
            $('#total').val(Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4))
            var total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
            checkbox(total);
        })

        $('#btnDenie').on('click', function(event) {
            var status = $(this).data('id');
            var form = $(this).closest("form");
            event.preventDefault();
            swal.fire({
                    title: 'Lý do không duyệt đơn',
                    html: `<input class="form-control" type="text" id="reason"  placeholder="Lý do" required>`,
                    icon: 'warning',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Đồng ý',
                    showDenyButton: true,
                    denyButtonText: 'Hủy',
                    preConfirm: () => {
                        const reason = Swal.getPopup().querySelector('#reason').value
                        if (!reason) {
                            Swal.showValidationMessage(`Vui lòng nhập lý do`)
                        }
                        return {
                            reason: reason,
                        }
                    }
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        $('#reasonConfirm').val(result.value.reason);
                        $('#statusConfirm').val(2);
                        form.submit();
                    } else if (result.isDenied) {
                        Swal.fire('Your record is safe', '', 'info')
                    }

                });
        });

        $('#formUpdate').on('submit', function(event) {
            event.preventDefault()
            $.ajax({
                method: "PUT",
                url: "{{ route('tablekpi.storeUpdate') }}",
                data: $('#formUpdate').serialize(),
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                        window.location.href = response.link
                    } else {
                        toastr.error(response.message);
                    }

                },
                error: function(error) {
                    handleFails(error);
                }
            })
        })

        function handleFails(response) {
            $('#selectPerson, #selectRoom, #selectTeam, #selectPosition, #assign').removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formUpdate').find(".has-error").find(".help-block").remove();
                $('#formUpdate').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formUpdate').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response
                        .responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }

        // var basicInfoID = $('#selectPerson').find(":selected").val();
        var roomID = $('#selectRoom').find(":selected").val();
        var teamID = "{!! $dataInit['kpi']['team_id'] !!}";
        var positionID = "{!! $dataInit['kpi']['position_id'] !!}";
        var staffID = $('#selectStaff').data('old');
        // var nameID = $('#selectName').data('old');

        if (roomID != '') {
            loadTeam(roomID, teamID)
        }

        if (teamID != '') {
            loadPosition(teamID, positionID)
        }

        if (roomID != '' && teamID != '' && positionID != '') {
            loadStaff(roomID, teamID, positionID, staffID)
        }

        // $('#selectPerson').on('change', function() {
        //     var id = $('#selectPerson').find(":selected").val();
        //     loadRoom(id);
        // });

        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            loadTeam(id);
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            loadPosition(id);
        });

        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();

            loadStaff(room_id, team_id, position_id);

            if (position_id == "0") {
                $('#selectStaff')
                    .find('option')
                    .remove()
                // $('#selectName')
                //     .find('option')
                //     .remove()
            }
        })
    });

    function loadTeam(id, teamID = null) {
        if (id != '') {
            var url = "{!! route('helper.getTeamByRommID', ':id') !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (Object.keys(data).length > 0) {

                        $('#selectTeam')
                            .find('option')
                            .remove()
                        // $('#selectName')
                        //     .find('option')
                        //     .remove()
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectPosition')
                            .find('option')
                            .remove()
                        $('#selectTeam').append($('<option>', {
                            value: '',
                            text: "Vui lòng chọn bộ phận",
                            disabled: true,
                            selected: true,
                        }));
                        Object.keys(data).forEach(function(index) {
                            if (data[index]) {
                                if (Number.parseInt(teamID) == data[index].id) {
                                    $('#selectTeam').append($('<option>', {
                                        value: data[index].id,
                                        text: data[index].name,
                                        selected: true
                                    }));
                                } else {
                                    $('#selectTeam').append($('<option>', {
                                        value: data[index].id,
                                        text: data[index].name,
                                    }));
                                }
                            }
                        })
                    } else {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                    }
                }
            });
        } else {
            $('#selectTeam')
                .find('option')
                .remove()
        }
    }

    function loadPosition(id, poisitionID = null) {
        var url = "{!! route('helper.getPositionByTeamID', ':id') !!}";

        $.ajax({
            type: 'GET',
            url: url.replace(':id', id),
            success: function(data) {
                if (Object.keys(data).length > 0) {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                    // $('#selectName')
                    //     .find('option')
                    //     .remove()
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    $('#selectPosition').append($('<option>', {
                        value: '',
                        text: "Vui lòng chọn chức danh",
                        disabled: true,
                        selected: true,
                    }));
                    Object.keys(data).forEach(function(index) {
                        if (data[index] != null) {
                            if (Number.parseInt(poisitionID) == data[index].id) {
                                $('#selectPosition').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name,
                                    selected: true,
                                }));
                            } else {
                                $('#selectPosition').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name
                                }));
                            }
                        }
                    })
                } else {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                }
            }
        });
    }

    function loadStaff(room_id, team_id, position_id, staffID = null) {
        var url = "{!! route('helper.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";

        $.ajax({
            type: 'GET',
            url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id',
                position_id),
            success: function(data) {
                if (Object.keys(data).length > 0) {
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    // $('#selectName')
                    //     .find('option')
                    //     .remove()
                    // $('#selectName').append($('<option>', {
                    //     text: "Vui lòng chọn người nhận KPIs",
                    //     value: '',
                    //     disabled: true,
                    //     selected: true,
                    // }));
                    // $('#selectStaff').append($('<option>', {
                    //     value: '',
                    //     text: "Vui lòng chọn người nhận KPIs",
                    //     disabled: true,
                    //     selected: true,
                    // }));
                    Object.keys(data).forEach(function(index) {
                        if (data[index].staff != null) {
                            if (Number.parseInt(staffID) == data[index].staff.id) {
                                $('#selectStaff').append($('<option>', {
                                    value: data[index].staff.id,
                                    text: data[index].staff.full_name,
                                    selected: true,
                                }));
                                // $('#selectName').append($('<option>', {
                                //     value: data[index].staff.id,
                                //     text: data[index].staff.full_name,
                                //     selected: true,
                                // }));
                            } else {
                                $('#selectStaff').append($('<option>', {
                                    value: data[index].staff.id,
                                    text: data[index].staff.full_name
                                }));
                                // $('#selectName').append($('<option>', {
                                //     value: data[index].staff.id,
                                //     text: data[index].staff.full_name,
                                // }));
                            }
                        }
                    })
                } else {
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    $('#selectStaff').append($('<option>', {
                        value: '',
                        text: 'Chưa có'
                    }));
                }
            }
        }).done(function() {
            if ($('#selectStaff').find('option').length == 0) {
                $('#selectStaff').append($('<option>', {
                    value: '',
                    selected: true,
                    text: 'Chưa có'
                }));
            }
        });

        if (position_id == "0") {
            $('#selectStaff')
                .find('option')
                .remove()
        }

        if (!staffID) {
            $('#selectStaff').append($('<option>', {
                value: '',
                text: 'Chưa có'
            }));
        }

    }
</script>
@endsection