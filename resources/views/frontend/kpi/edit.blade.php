@extends('layouts.fe.master2')
@section('title')
@lang('translation.personal')
@endsection
@section('css')
<link href="{{ URL::asset('assets/libs/quill/quill.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="row ">
    <h3 class="text-center">THÔNG TIN KPIs</h3>
</div>
<!--end col Quốc tịch-->

<form method="PUT" id="formUpdate" enctype="multipart/form-data">
    @csrf
    <input hidden name="kpis_id" value="{{ $dataInit['kpi']['id'] }}">
    <input hidden name="quarter" value="{{ $dataInit['kpi']['quarter'] }}">
    <input hidden name="year" value="{{ $dataInit['kpi']['year'] }}">
    <input hidden name="room_id" value="{{ $dataInit['kpi']['room']['id'] }}">
    <input hidden name="room_id" value="{{ $dataInit['kpi']['room']['id'] }}">
    <input hidden name="team_id" value="{{ $dataInit['kpi']['team']['id'] }}">
    <input hidden name="position_id" value="{{ $dataInit['kpi']['position']['id'] }}">
    <div class="row justify-content-between pe-5 mb-3">
        <h3 style="width: fit-content" class="m-0"></h3>
        <button type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content"><i class="ri-save-3-line label-icon  fs-16 "></i> Lưu</button>
    </div>
    <div class="row ">
        @if(!$dataInit['editByRecive'])
        <div class="row">
            <div class="col-1 mb-3">
                <label for="precious" class="form-label">Quý</label>
                <select name="quarter" class="form-select @error('quarter') is-invalid @enderror" aria-label="Default select example">
                    <option @if($dataInit['kpi']['quarter']==1) selected @endif value="1">I</option>
                    <option @if($dataInit['kpi']['quarter']==2) selected @endif value="2">II</option>
                    <option @if($dataInit['kpi']['quarter']==3) selected @endif value="3">III</option>
                    <option @if($dataInit['kpi']['quarter']==4) selected @endif value="4">IV</option>
                </select>
                @error('quarter')
                <small class="help-block text-danger">{{ $message }} *</small>
                @enderror
            </div>
            <!--end col Quý-->

            <div class="col-1 mb-3">
                <label for="year" class="form-label">Năm</label>
                <input rule type="text" id="datepicker" name="year" value="{{ $dataInit['kpi']['year'] }}" class="form-control @error('room_id') is-invalid @enderror datepicker ">
                @error('year')
                <small class="help-block text-danger">{{ $message }} *</small>
                @enderror
            </div>
            <!--end col Năm-->
        </div>
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <label for="room" class="form-label">Phòng</label>
                    <div class="col-xl-12">
                        <select rule name="room_id" id="selectRoom" class="form-select @error('room_id') is-invalid @enderror" aria-label="Default select example">
                            @foreach ($dataInit['room'] as $value)
                            <option @if ($dataInit['kpi']['room_id']==$value['id']) selected @endif value="{{ $value['id'] }}">
                                {{ $value['name'] }}
                            </option>
                            @endforeach
                        </select>
                        @error('room_id')
                        <small class="help-block text-danger">{{ $message }} *</small>
                        @enderror
                        {{-- <select name="room_id" id="selectRoom" class="form-select" aria-label="Default select example">
                            </select> --}}
                    </div>
                </div>
            </div>
            <!--end col Phòng-->

            <div class="col-4">
                <div class="mb-3">
                    <label for="part" class="form-label">Bộ phận</label>
                    <div class="col-xl-12">
                        {{-- <select rule name="team_id" id="selectTeam"
                                class="form-select @error('team_id') is-invalid @enderror"
                                aria-label="Default select example">
                                @foreach ($dataInit['room'] as $value)
                                    <option @if ($dataInit['kpi']['team_id'] == $value[->team->id]) selected @endif
                                        value="{{ $value->team->id }}">
                        {{ $value->team->name }}</option>
                        @endforeach
                        </select> --}}
                        <select name="team_id" id="selectTeam" class="form-select" aria-label="Default select example">
                        </select>
                        @error('team_id')
                        <small class="help-block text-danger">{{ $message }} *</small>
                        @enderror
                    </div>
                </div>
            </div>
            <!--end col Bộ phận-->

            <div class="col-4">
                <div class="mb-3">
                    <label for="position" class="form-label">Chức danh</label>
                    <div class="col-xl-12">
                        {{-- <select name="position_id" id="selectPosition"
                                class="form-select @error('position_id') is-invalid @enderror"
                                aria-label="Default select example">
                                @foreach ($dataInit['room'] as $value)
                                    <option @if ($dataInit['kpi']['position_id'] == $value->position->id) selected @endif
                                        value="{{ $value->position->id }}">
                        {{ $value->position->name }}</option>
                        @endforeach
                        </select> --}}
                        <select name="position_id" id="selectPosition" class="form-select" aria-label="Default select example">
                        </select>
                        @error('position_id')
                        <small class="help-block text-danger">{{ $message }} *</small>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-4 mb-3">
                <label for="precious" class="form-label">Quý</label>
                <span class="form-label" style="font-size: 15px;">
                    {{ !empty($dataInit['kpi']['quarter']) ? $dataInit['kpi']['quarter'] : 'Chưa có' }}
                </span>
            </div>
            <!--end col Quý-->

            <div class="col-8 mb-3">
                <label for="year" class="form-label">Năm</label>
                <span class="form-label" style="font-size: 15px;">
                    {{ !empty($dataInit['kpi']['year']) ? $dataInit['kpi']['year'] : 'Chưa có' }}
                </span>
            </div>
            <!--end col Năm-->
        </div>

        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <label for="room" class="form-label">Phòng</label>
                    <span class="form-label" style="font-size: 15px;">
                        {{ !empty($dataInit['kpi']['room']) ? $dataInit['kpi']['room']['name'] : 'Chưa có' }}
                    </span>
                </div>
            </div>

            <div class="col-4">
                <div class="mb-3">
                    <label for="part" class="form-label">Bộ phận</label>
                    <span class="form-label" style="font-size: 15px;">
                        {{ !empty($dataInit['kpi']['team']) ? $dataInit['kpi']['team']['name'] : 'Chưa có' }}
                    </span>
                </div>
            </div>

            <div class="col-4">
                <div class="mb-3">
                    <label for="position" class="form-label">Chức danh</label>
                    <span class="form-label" style="font-size: 15px;">
                        {{ !empty($dataInit['kpi']['position']) ? $dataInit['kpi']['position']['name'] : 'Chưa có' }}
                    </span>
                </div>
            </div>
        </div>
        @endif

        <div class="row ">
            <div class="col-12">
                <div class="card">
                    {{-- <div class="card-header">
                        <h5 class="card-title mb-0">Basic Datatables</h5>
                    </div> --}}
                    <div class="card-body">
                        <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                            <colgroup>
                                <col width="5%">
                                <col width="10%">
                                <col width="23%">
                                <col width="8%">
                                <col width="23%">
                                <col width="8%">
                                <col width="23%">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Hạng Mục</th>
                                    <th>Nội Dung KPIs</th>
                                    <th>Tỷ Trọng</th>
                                    <th>Thực Tế</th>
                                    <th>Kết Quả</th>
                                    <th>Ghi Chú</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @for ($i; $i <= 5; $i++) <tr>
                                    <td>{{ $i }}</td>
                                    @switch($i)
                                    @case(1)
                                    <input hidden name="categories_{{ $i }}" value="Deadline">
                                    <td>Deadline</td>
                                    @break

                                    @case(2)
                                    <input hidden name="categories_{{ $i }}" value="Số lượng">
                                    <td>Số lượng</td>
                                    @break

                                    @case(3)
                                    <input hidden name="categories_{{ $i }}" value="Văn hóa">
                                    <td>Văn hóa</td>
                                    @break

                                    @case(4)
                                    <input hidden name="categories_{{ $i }}" value="Chất lượng">
                                    <td>Chất lượng</td>
                                    @break
                                    @case(5)
                                    <td class="text-center" colspan="2">Kết quả KPIs</td>
                                    @break
                                    @endswitch
                                    @if(array_key_exists($i - 1, $dataInit['detailKpi']))
                                    <td>
                                        <textarea @if($dataInit['editByRecive']) disabled @endif class="form-control" name="content_{{ $i }}" id="content_{{ $i }}" value="{{ $dataInit['detailKpi'][$i - 1]['content_kpis'] }}" rows="5"> {{ $dataInit['detailKpi'][$i - 1]['content_kpis'] }} </textarea>
                                    </td>

                                    <td>
                                        <input @if($dataInit['editByRecive']) disabled @endif type="text" class="form-control" name="proportion_{{ $i }}" id="proportion_{{ $i }}" value="{{ $dataInit['detailKpi'][$i - 1]['proportion'] }}">
                                    </td>
                                    <td>
                                        <textarea class="form-control" name="reality_{{ $i }}" value="{{ $dataInit['detailKpi'][$i - 1]['reality'] }}" rows="5">{{ $dataInit['detailKpi'][$i - 1]['reality'] }}</textarea>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="result_{{ $i }}" maxlength="6" value="{{ $dataInit['detailKpi'][$i - 1]['result'] }}" id="result_{{ $i }}">
                                    </td>
                                    <td>
                                        <textarea class="form-control" name="note_{{ $i }}" value={{ $dataInit['detailKpi'][$i - 1]['note'] }} rows="5"> {{ $dataInit['detailKpi'][$i - 1]['note'] }}</textarea>
                                    </td>
                                    @else
                                    <td class="text-center" id="totalProportion"></td>
                                    <td></td>
                                    <td class="text-center" id="totalReal"></td>
                                    <td></td>
                                    @endif
                                    </tr>
                                    @endfor

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">

                        <div class="row mb-5">
                            <div class="col-12 ms-5">
                                <h4 class="">Kết quả bạn đạt được là: </h4>
                                <div class="col-12 d-flex mt-2">
                                    <div class=" form-check" style="width: fit-content;">
                                        <input disabled class="form-check-input fs-15" type="checkbox" id="low" value="low" name="low">
                                    </div>
                                    <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Nếu đạt
                                        từ
                                        50% - 69%: xét duyệt lại việc tái ký hợp đồng lao động.</label>
                                </div>
                                <div class="col-12 d-flex mt-2">
                                    <div class=" form-check" style="width: fit-content;">
                                        <input disabled class="form-check-input fs-15" type="checkbox" id="medium" value="medium" name="medium">
                                    </div>
                                    <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Nếu đạt
                                        từ
                                        70% - 89%: tái ký hợp đồng lao động và được đào tạo.</label>
                                </div>
                                <div class="col-12 d-flex mt-2">
                                    <div class=" form-check" style="width: fit-content;">
                                        <input disabled class="form-check-input fs-15" type="checkbox" id="hight" value="hight" name="hight">
                                    </div>
                                    <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Nếu đạt
                                        từ
                                        90% trở lên: tái ký hợp đồng lao động và xét duyệt vị trí, tăng lương.</label>
                                </div>
                            </div>
                            <div class="col-12 d-flex mt-2 justify-content-end">
                                @if (!empty($dataInit['kpi']['delivery_date']))
                                <label for="confirm" class=" form-label mb-0 me-5 " style="margin-top: 2px">
                                    @php
                                    $date = \Carbon\Carbon::parse($dataInit['kpi']['delivery_date']);
                                    @endphp
                                    Ngày duyệt giao KPIs : Ngày {{ $date->day }} Tháng {{ $date->month }} Năm {{ $date->year }}</label>
                                @endif
                            </div>
                            <div class="col-12 d-flex mt-2 justify-content-end">
                                @if (!empty($dataInit['kpi']['review_date']))
                                <label for="confirm" class=" form-label mb-0 me-5 " style="margin-top: 2px">
                                    @php
                                    $date = \Carbon\Carbon::parse($dataInit['kpi']['review_date']);
                                    @endphp
                                    Ngày duyệt kết quả KPIs : Ngày {{ $date->day }} Tháng {{ $date->month }} Năm {{ $date->year }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-5">
                            <input name="job_assignor" value={{ $dataInit['kpi']['basic_info_id'] }} hidden>
                            <div class="col-3 d-flex justify-content-center">
                                <div class="row  ">
                                    <div class="col-12 text-center">
                                        <h5>Người giao việc</h5>
                                    </div>
                                    <div class="col-12 text-center">
                                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ $dataInit['kpi']['user']['name'] }}</label>
                                    </div>
                                    <div class="col-12 text-center">
                                        @if (!in_array($dataInit['kpi']['status'], [1, 5, 11]))
                                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">Đã xác nhận</label>
                                        @elseif ($dataInit['kpi']['status'] == 11)
                                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">Từ chối</label>
                                        <div>
                                            <label>Lý do : {{ $dataInit['kpi']['reason'] }}</label>
                                        </div>
                                        @elseif($dataInit['kpi']['status'] == 1)
                                        <div class="col-12 d-flex justify-content-center mt-4">
                                            <div class=" form-check" style="width: fit-content;">
                                                <input class="form-check-input fs-15" type="checkbox" id="assign" value="1" name="assign">
                                            </div>
                                            <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác
                                                nhận</label>
                                        </div>
                                        @else
                                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">Chưa xác nhận</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--end col Người giao việc-->

                            @foreach ($dataInit['kpi']['confirms'] as $key => $value)
                            @if($value['confirm']['type_confirm'] != 99)
                            <input name="confirm_id_{{ $key + 1 }}" value="{{ $value['confirm']['id'] }}" hidden>
                            <div class="col-3 d-flex justify-content-center">
                                <div class="row">
                                    <div class="col-12 text-center">
                                        @switch($value['confirm']['type_confirm'])
                                        @case(1)
                                        <h5>Người nhận việc</h5>
                                        @break

                                        @case(2)
                                        <h5>P.QT HC-NS</h5>
                                        @break

                                        @case(3)
                                        <h5>Ban Giám Đốc</h5>
                                        @break
                                        @endswitch
                                    </div>
                                    <div class="col-12">
                                        @switch($value['confirm']['type_confirm'])
                                        @case(1)
                                        <input hidden name="type_confirm_1" value="1">
                                        <select data-old="{{ $value['confirm']['basic_info_id'] }}" class="form-select text-center mt-2" id="selectStaff" name="selectStaff" aria-label="Default select example">
                                        </select>
                                        @break

                                        @case(2)
                                        @if (!empty($dataInit['hr']))
                                        <input hidden name="type_confirm_2" value="2">
                                        <select name="selectConfirm[]" class="form-select text-center mt-2" aria-label="Default select example">
                                            @foreach ($dataInit['hr'] as $hr)
                                            @if (!empty($hr['basic_info_id']))
                                            <option @if ($hr->basic_info_id == $value['confirm']['basic_info_id']) selected @endif
                                                value="{{ $hr->basic_info_id }}">
                                                {{ $hr->staff->full_name }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @endif
                                        @break

                                        @case(3)
                                        @if (!empty($dataInit['manager']))
                                        <input hidden name="type_confirm_3" value="3">
                                        <select name="selectConfirm[]" class="form-select text-center mt-2" aria-label="Default select example">
                                            @foreach ($dataInit['manager'] as $manager)
                                            @if (!empty($manager['basic_info_id']))
                                            <option @if ($manager->basic_info_id == $value['confirm']['basic_info_id']) selected @endif
                                                value="{{ $manager->basic_info_id }}">
                                                {{ $manager->staff->full_name }}
                                            </option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @endif
                                        @break
                                        @endswitch
                                    </div>
                                    <div class="col-12 text-center">
                                        @if ($value['confirm']['type_confirm'] != 1)
                                        @switch($value['confirm']['status'])
                                        @case(0)
                                        <label for="firstNameinput" class="form-label mb-0">{{ 'Chưa xác nhận' }}</label>
                                        @break

                                        @case(1)
                                        <label for="firstNameinput" class="form-label mb-0">{{ 'Đã xác nhận' }}</label>
                                        @break

                                        @case(2)
                                        <label for="firstNameinput" class="form-label mb-0">{{ 'Từ chối' }}</label>
                                        <div>
                                            <label>Lý do : {{ $value['confirm']['note'] }}</label>
                                        </div>
                                        @break
                                        @endswitch
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                            <!--end col Ban giám đốc-->
                        </div>



                    </div>
                </div>


            </div>
        </div>
    </div>
</form>
@endsection
@section('script')
<script src="{{ URL::asset('assets/libs/cleave.js/cleave.js.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Tree plugin js -->
<script>
    const csrf = "{{ csrf_token() }}";
    $(document).ready(function() {
        $('#datepicker').datepicker({
            format: 'yyyy',
            startView: "years",
            minViewMode: "years",
        });
        var result1 = new Cleave('#result_1', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var result2 = new Cleave('#result_2', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var result3 = new Cleave('#result_3', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var result4 = new Cleave('#result_4', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true
        });

        var proportion1 = new Cleave('#proportion_1', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var proportion2 = new Cleave('#proportion_2', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var proportion3 = new Cleave('#proportion_3', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var proportion4 = new Cleave('#proportion_4', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true
        });

        function loadProportion() {
            var result_1 = proportion1.getRawValue();
            var result_2 = proportion2.getRawValue();
            var result_3 = proportion3.getRawValue();
            var result_4 = proportion4.getRawValue();

            var total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)

            $('#totalProportion').text(total + "%")
        }
        loadProportion();
        var result_1 = result1.getRawValue()
        var result_2 = result2.getRawValue()
        var result_3 = result3.getRawValue()
        var result_4 = result4.getRawValue()
        total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
        $('#totalReal').text(total > 0 ? total + '%' : 0)
        checkbox(total)

        function checkbox(sum) {
            if (sum >= 50 && sum < 70) {
                document.getElementById("low").checked = true;
                document.getElementById("medium").checked = false;
                document.getElementById("hight").checked = false;
            } else if (sum >= 70 && sum < 90) {
                document.getElementById("low").checked = false;
                document.getElementById("medium").checked = true;
                document.getElementById("hight").checked = false;
            } else if (sum >= 90) {
                document.getElementById("low").checked = false;
                document.getElementById("medium").checked = false;
                document.getElementById("hight").checked = true;
            } else {
                document.getElementById("low").checked = false;
                document.getElementById("medium").checked = false;
                document.getElementById("hight").checked = false;
            }
        }

        $('#result_1, #result_2, #result_3, #result_4').change(function() {
            var result_1 = result1.getRawValue()
            var result_2 = result2.getRawValue()
            var result_3 = result3.getRawValue()
            var result_4 = result4.getRawValue()
            var total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
            $('#total').val(total)
            $('#totalReal').text(total + "%")
            checkbox(total);
        })

        $('#proportion_1, #proportion_2, #proportion_3, #proportion_4').change(function(event) {
            loadProportion(event);
        })

        $('#formUpdate').on('submit', function(event) {
            event.preventDefault()

            var pro1 = $('#proportion_1').val().split('%')[0]
            var pro2 = $('#proportion_2').val().split('%')[0]
            var pro3 = $('#proportion_3').val().split('%')[0]
            var pro4 = $('#proportion_4').val().split('%')[0]

            $('#proportion_1').val(pro1)
            $('#proportion_2').val(pro2)
            $('#proportion_3').val(pro3)
            $('#proportion_4').val(pro4)

            var real1 = $('#result_1').val().split('%')[0]
            var real2 = $('#result_2').val().split('%')[0]
            var real3 = $('#result_3').val().split('%')[0]
            var real4 = $('#result_4').val().split('%')[0]

            $('#result_1').val(real1)
            $('#result_2').val(real2)
            $('#result_3').val(real3)
            $('#result_4').val(real4)
            
            $.ajax({
                method: "PUT",
                url: "{{ route('tablekpi.storeUpdate') }}",
                data: $('#formUpdate').serialize(),
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                        window.location.href = response.link
                    } else {
                        toastr.error(response.message);
                    }

                },
                error: function(error) {
                    handleFails(error);
                }
            })
        })

        function handleFails(response) {
            $('#selectPerson, #selectRoom, #selectTeam, #selectPosition, #assign').removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formUpdate').find(".has-error").find(".help-block").remove();
                $('#formUpdate').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formUpdate').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response
                        .responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }

        var roomID = $('#selectRoom').find(":selected").val() ?? "{!! $dataInit['kpi']['room_id'] !!}";
        var teamID = "{!! $dataInit['kpi']['team_id'] !!}";
        var positionID = "{!! $dataInit['kpi']['position_id'] !!}";
        var staffID = $('#selectStaff').data('old');

        if (roomID != '') {
            loadTeam(roomID, teamID)
        }

        if (teamID != '') {
            loadPosition(teamID, positionID)
        }

        if (roomID != '' && teamID != '' && positionID != '') {
            loadStaff(roomID, teamID, positionID, staffID)
        }

        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            loadTeam(id);
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            loadPosition(id);
        });

        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();

            loadStaff(room_id, team_id, position_id);

            if (position_id == "0") {
                $('#selectStaff')
                    .find('option')
                    .remove()
            }
        })
    });


    function loadTeam(id, teamID = null) {
        if (id != '') {
            var url = "{!! route('helper.getTeamByRommID', ':id') !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (Object.keys(data).length > 0) {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectPosition')
                            .find('option')
                            .remove()
                        $('#selectTeam').append($('<option>', {
                            value: '',
                            text: "Vui lòng chọn bộ phận",
                            disabled: true,
                            selected: true,
                        }));
                        Object.keys(data).forEach(function(index) {
                            if (data[index]) {
                                if (Number.parseInt(teamID) == data[index].id) {
                                    $('#selectTeam').append($('<option>', {
                                        value: data[index].id,
                                        text: data[index].name,
                                        selected: true
                                    }));
                                } else {
                                    $('#selectTeam').append($('<option>', {
                                        value: data[index].id,
                                        text: data[index].name,
                                    }));
                                }
                            }
                        })
                    } else {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                    }
                }
            });
        } else {
            $('#selectTeam')
                .find('option')
                .remove()
        }
    }

    function loadPosition(id, poisitionID = null) {
        var url = "{!! route('helper.getPositionByTeamID', ':id') !!}";

        $.ajax({
            type: 'GET',
            url: url.replace(':id', id),
            success: function(data) {
                if (Object.keys(data).length > 0) {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    $('#selectPosition').append($('<option>', {
                        value: '',
                        text: "Vui lòng chọn chức danh",
                        disabled: true,
                        selected: true,
                    }));
                    Object.keys(data).forEach(function(index) {
                        if (data[index] != null) {
                            if (Number.parseInt(poisitionID) == data[index].id) {
                                $('#selectPosition').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name,
                                    selected: true,
                                }));
                            } else {
                                $('#selectPosition').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name
                                }));
                            }
                        }
                    })
                } else {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                }
            }
        });
    }

    function loadStaff(room_id, team_id, position_id, staffID = null) {
        var url = "{!! route('helper.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";

        $.ajax({
            type: 'GET',
            url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
            success: function(data) {
                if (Object.keys(data).length > 0) {
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    Object.keys(data).forEach(function(index) {
                        if (data[index].staff != null) {
                            if (Number.parseInt(staffID) == data[index].staff.id) {
                                $('#selectStaff').append($('<option>', {
                                    value: data[index].staff.id,
                                    text: data[index].staff.full_name,
                                    selected: true,
                                }));
                            } else {
                                $('#selectStaff').append($('<option>', {
                                    value: data[index].staff.id,
                                    text: data[index].staff.full_name
                                }));
                            }
                        }

                    })
                } else {
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    $('#selectStaff').append($('<option>', {
                        value: '',
                        text: 'Chưa có'
                    }));
                }
            }
        }).done(function() {
            if ($('#selectStaff').find('option').length == 0) {
                $('#selectStaff').append($('<option>', {
                    value: '',
                    selected: true,
                    text: 'Chưa có'
                }));
            }
        });

        if (position_id == "0") {
            $('#selectStaff')
                .find('option')
                .remove()
        }

        if (!staffID) {
            $('#selectStaff').append($('<option>', {
                value: '',
                text: 'Chưa có'
            }));
        }
    }
</script>
@endsection