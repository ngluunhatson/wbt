@extends('layouts.fe.master2')
@section('title')
@lang('translation.personal')
@endsection
@section('css')
<link href="{{ URL::asset('assets/libs/quill/quill.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="row ">
    <h3 class="text-center">THÔNG TIN KPIs</h3>
</div>
<!--end col Quốc tịch-->

<form method="POST" id="formInsert" enctype="multipart/form-data">
    @csrf
    <div class="row justify-content-between pe-5 mb-3">
        <h3 style="width: fit-content" class="m-0"></h3>
        <button type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content"><i class="ri-save-3-line label-icon  fs-16 "></i> Lưu</button>
    </div>
    <div class="row ">

        <div class="row">
            <div class="col-1 mb-3">
                <label for="precious" class="form-label">Quý</label>
                <select name="quarter" class="form-select @error('quarter') is-invalid @enderror" aria-label="Default select example">
                    <option value="1">I</option>
                    <option value="2">II</option>
                    <option value="3">III</option>
                    <option value="4">IV</option>
                </select>
                @error('quarter')
            <small class="help-block text-danger">{{ $message }} *</small>
                @enderror
            </div>
            <!--end col Quý-->

            <div class="col-1 mb-3">
                <label for="year" class="form-label">Năm</label>
                <input autocomplete="off" type="text" id="datepicker" name="year" class="form-control @error('room_id') is-invalid @enderror datepicker ">
                @error('year')
                <small class="help-block text-danger">{{ $message }} *</small>
                @enderror
            </div>
            <!--end col Năm-->
        </div>

        <div class="row">


            <div class="col-4">
                <div class="mb-3">
                    <label for="room" class="form-label">Phòng</label>
                    <div class="col-xl-12">
                        <select @if(!auth()->user()->hasRole('super-admin')) style="pointer-events: none;backgroud-color: '#eff2f7';" @endif rule name="room_id" id="selectRoom" class="form-select @error('room_id') is-invalid @enderror" aria-label="Default select example">
                        @if(auth()->user()->hasRole('super-admin'))    
                        <option value="" >Vui lòng chọn phòng ban</option>
                            @foreach ($dataInit['room'] as $value)
                            <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                            @endforeach
                            @else
                            @foreach($dataInit['room'] as $value)
                            @if(auth()->user()->organizational->room_id == $value['id'])
                            <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                            @endif
                            @endforeach
                            @endif
                        </select>
                        @error('room_id')
                        <small class="help-block text-danger">{{ $message }} *</small>
                        @enderror
                        {{-- <select name="room_id" id="selectRoom" class="form-select" aria-label="Default select example">
                            </select> --}}
                    </div>
                </div>
            </div>
            <!--end col Phòng-->

            <div class="col-4">
                <div class="mb-3">
                    <label for="part" class="form-label">Bộ phận</label>
                    <div class="col-xl-12" @if(isset(auth()->user()->staff->rank) && auth()->user()->staff->rank < 6) style="pointer-events: none;" @endif>
                        <select rule name="team_id" id="selectTeam" class="form-select @error('team_id') is-invalid @enderror" aria-label="Default select example">
                        </select>
                        @error('team_id')
                        <small class="help-block text-danger">{{ $message }} *</small>
                        @enderror
                    </div>
                </div>
            </div>
            <!--end col Bộ phận-->

            <div class="col-4">
                <div class="mb-3">
                    <label for="position" class="form-label">Chức danh</label>
                    <div class="col-xl-12">
                        <select name="position_id" id="selectPosition" class="form-select @error('position_id') is-invalid @enderror" aria-label="Default select example">
                        </select>
                        @error('position_id')
                        <small class="help-block text-danger">{{ $message }} *</small>
                        @enderror
                    </div>
                </div>
            </div>
            <!--end col Chức danh-->

            {{-- <div class="col-6">
                    <div class="mb-3">
                        <label for="basic_info_id" class="form-label">Họ và tên</label>
                        <div class="col-xl-12">
                            <select class="form-select @error('basic_info_id') is-invalid @enderror" id="selectName"
                                name="basic_info_id" aria-label="Default select example">
                            </select>
                            @error('basic_info_id')
                                <small class="help-block text-danger">{{ $message }} *</small>
            @enderror
        </div>
    </div>
    </div> --}}
    <!--end col Họ và tên-->
    </div>

    <div class="row ">
        <div class="col-12">
            <div class="card">
                {{-- <div class="card-header">
                        <h5 class="card-title mb-0">Basic Datatables</h5>
                    </div> --}}
                <div class="card-body">
                    <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                        <colgroup>
                            <col width="5%">
                            <col width="10%">
                            <col width="23%">
                            <col width="8%">
                            <col width="23%">
                            <col width="8%">
                            <col width="23%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Hạng Mục</th>
                                <th>Nội Dung KPIs</th>
                                <th>Tỷ Trọng</th>
                                <th>Thực Tế</th>
                                <th>Kết Quả</th>
                                <th>Ghi Chú</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @for ($i; $i <= 5; $i++) <tr>
                                <td>{{ $i }}</td>
                                @switch($i)
                                @case(1)
                                <input hidden name="categories_{{ $i }}" value="Deadline">
                                <td>Deadline</td>
                                @break

                                @case(2)
                                <input hidden name="categories_{{ $i }}" value="Số lượng">
                                <td>Số lượng</td>
                                @break

                                @case(3)
                                <input hidden name="categories_{{ $i }}" value="Văn hóa">
                                <td>Văn hóa</td>
                                @break

                                @case(4)
                                <input hidden name="categories_{{ $i }}" value="Chất lượng">
                                <td>Chất lượng</td>
                                @break
                                @case(5)
                                <td class="text-center" colspan="2">Kết quả KPIs</td>
                                @break
                                @endswitch
                                @if($i != 5)
                                <td>
                                    <textarea class="form-control" name="content_{{ $i }}" id="content_{{ $i }}" value="{{ old('content_' . $i) }}" rows="5"></textarea>
                                </td>

                                <td>
                                    <input type="text" class="form-control" name="proportion_{{ $i }}" id="proportion_{{ $i }}" value="">
                                </td>
                                <td>
                                    <textarea class="form-control" name="reality_{{ $i }}" value="{{ old('reality_' . $i) }}" rows="5"></textarea>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="result_{{ $i }}" value="" id="result_{{ $i }}">
                                </td>
                                <td>
                                    <textarea class="form-control" name="note_{{ $i }}" value="{{ old('note_' . $i) }}" rows="5"></textarea>
                                </td>
                                @else
                                <td class="text-center" id="totalProportion"></td>
                                <td></td>
                                <td class="text-center" id="totalReal"></td>
                                <td></td>
                                @endif
                                </tr>
                                @endfor
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-body">

                    <div class="row mb-5">
                        <div class="col-12 ms-5">
                            <h4 class="">Kết quả bạn đạt được là:</h4>
                            <div class="col-12 d-flex mt-2">
                                <div class=" form-check" style="width: fit-content;">
                                    <input disabled class="form-check-input fs-15" type="checkbox" id="low" value="low" name="low">
                                </div>
                                <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Nếu đạt
                                    từ
                                    50% - 69%: xét duyệt lại việc tái ký hợp đồng lao động.</label>
                            </div>
                            <div class="col-12 d-flex mt-2">
                                <div class=" form-check" style="width: fit-content;">
                                    <input disabled class="form-check-input fs-15" type="checkbox" id="medium" value="medium" name="medium">
                                </div>
                                <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Nếu đạt
                                    từ
                                    70% - 89%: tái ký hợp đồng lao động và được đào tạo.</label>
                            </div>
                            <div class="col-12 d-flex mt-2">
                                <div class=" form-check" style="width: fit-content;">
                                    <input disabled class="form-check-input fs-15" type="checkbox" id="hight" value="hight" name="hight">
                                </div>
                                <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Nếu đạt
                                    từ
                                    90% trở lên: tái ký hợp đồng lao động và xét duyệt vị trí, tăng lương.</label>
                            </div>
                        </div>
                        {{-- <div class="col-12 d-flex mt-2 justify-content-end">
                                    <label for="confirm" class=" form-label mb-0 me-5 " style="margin-top: 2px">Ngày
                                        giao
                                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input for="" class="delivery_date "
                                            id="delivery_date" name="delivery_date"></label>
                                </div>
                                <div class="col-12 d-flex mt-2 justify-content-end">
                                    <label for="confirm" class=" form-label mb-0 me-5" style="margin-top: 2px">Ngày
                                        duyệt
                                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input for="" class="review_date "
                                            id="review_date" name="review_date"></label>
                                </div> --}}
                    </div>

                    <div class="row mb-5">
                        <div class="col-3 d-flex justify-content-center">
                            <div class="row  ">
                                <div class="col-12 text-center">
                                    <h5>Người giao KPI</h5>
                                </div>
                                <div class="col-12 text-center">
                                    <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ auth()->user()->name }}</label>
                                </div>
                                <div class="col-12 d-flex justify-content-center mt-4">
                                    <div class=" form-check" style="width: fit-content;">
                                        <input class="form-check-input fs-15" type="checkbox" id="assign" value="1" name="assign">
                                    </div>
                                    <label for="assign" class=" form-label mb-0 " style="margin-top: 2px">Xác
                                        nhận</label>
                                </div>

                            </div>
                        </div>

                        <div class="col-3 d-flex justify-content-center">
                            <div class="row  form-group">
                                <div class="col-12 text-center">
                                    <h5>Người nhận KPI</h5>
                                </div>
                                <input hidden name="type_confirm_1" value="1">
                                <div class="col-12">
                                    <select class="form-select text-center" id="selectStaff" name="selectStaff" aria-label="Default select example">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-3 d-flex justify-content-center">
                            <div class="row  ">
                                <div class="col-12 text-center">
                                    <h5>P.QT HC-NS</h5>
                                </div>
                                <input hidden name="type_confirm_2" value="2">
                                <div class="col-12">
                                    <select id="selectHr" name="selectConfirm[]" class="form-select text-center" aria-label="Default select example">
                                        @if (!empty($dataInit['hr']))
                                        @foreach ($dataInit['hr'] as $value)
                                        @if (!empty($value['basic_info_id']))
                                        <option value="{{ $value->staff->id }}">
                                            {{ $value->staff->full_name }}
                                        </option>
                                        @endif
                                        @endforeach
                                        @endif
                                    </select>
                                </div>                               
                            </div>
                        </div>
                        <!--end col P.QT HC-NS-->

                        <div class="col-3 d-flex justify-content-center">
                            <div class="row  ">
                                <div class="col-12 text-center">
                                    <h5>Ban Giám Đốc</h5>
                                </div>
                                <input hidden name="type_confirm_3" value="3">
                                <div class="col-12">
                                    <select name="selectConfirm[]" class="form-select text-center" aria-label="Default select example">
                                        @if (!empty($dataInit['manager']))
                                        @foreach ($dataInit['manager'] as $value)
                                        @if (!empty($value['basic_info_id']))
                                        <option value="{{ $value->staff->id }}">
                                            {{ $value->staff->full_name }}
                                        </option>
                                        @endif
                                        @endforeach
                                        @endif
                                    </select>
                                </div>                               
                            </div>
                        </div>
                        <!--end col Ban giám đốc-->
                    </div>
                </div>
            </div>


        </div>
    </div>
    </div>
</form>
@endsection
@section('script')
<script src="{{ URL::asset('assets/libs/cleave.js/cleave.js.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script>
    const csrf = "{{ csrf_token() }}";
    $(document).ready(function() {
        $('#datepicker').datepicker({
            format: 'yyyy',
            startView: "years",
            minViewMode: "years",
            startDate: 'toDay',
        });


    });
</script>

<script>
    $(document).ready(function() {
        var result1 = new Cleave('#result_1', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var result2 = new Cleave('#result_2', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var result3 = new Cleave('#result_3', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var result4 = new Cleave('#result_4', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true
        });

        var proportion1 = new Cleave('#proportion_1', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var proportion2 = new Cleave('#proportion_2', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var proportion3 = new Cleave('#proportion_3', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true,
        });
        var proportion4 = new Cleave('#proportion_4', {
            numeral: true,
            prefix: '%',
            tailPrefix: true,
            numeralDecimalMark: ',',
            delimiter: '.',
            numeralIntegerScale: 2,
            rawValueTrimPrefix: true
        });

        function checkbox(sum) {
            if (sum >= 50 && sum < 70) {
                document.getElementById("low").checked = true;
                document.getElementById("medium").checked = false;
                document.getElementById("hight").checked = false;
            } else if (sum >= 70 && sum < 90) {
                document.getElementById("low").checked = false;
                document.getElementById("medium").checked = true;
                document.getElementById("hight").checked = false;
            } else if (sum >= 90) {
                document.getElementById("low").checked = false;
                document.getElementById("medium").checked = false;
                document.getElementById("hight").checked = true;
            } else {
                document.getElementById("low").checked = false;
                document.getElementById("medium").checked = false;
                document.getElementById("hight").checked = false;
            }
        }

        $('#result_1').change(function() {
            var result_1 = $('#result_1').val();
            var result_2 = $('#result_2').val();
            var result_3 = $('#result_3').val();
            var result_4 = $('#result_4').val();
            $('#total').val(Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4))
            var total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
            checkbox(total);
        })

        $('#result_2').on('change', function() {
            var result_1 = $('#result_1').val();
            var result_2 = $('#result_2').val();
            var result_3 = $('#result_3').val();
            var result_4 = $('#result_4').val();
            $('#total').val(Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4))
            var total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
            checkbox(total);
        })

        $('#result_3').on('change', function() {
            var result_1 = $('#result_1').val();
            var result_2 = $('#result_2').val();
            var result_3 = $('#result_3').val();
            var result_4 = $('#result_4').val();
            $('#total').val(Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4))
            var total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
            checkbox(total);
        })

        $('#result_4').on('change', function() {
            var result_1 = $('#result_1').val();
            var result_2 = $('#result_2').val();
            var result_3 = $('#result_3').val();
            var result_4 = $('#result_4').val();
            $('#total').val(Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4))
            var total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
            checkbox(total);
        })

        $('#formInsert').on('submit', function(event) {
            event.preventDefault()
            var totalProportion = $('#totalProportion').text().split('%')[0]
            var pro1 = $('#proportion_1').val().split('%')[0]
            var pro2 = $('#proportion_2').val().split('%')[0]
            var pro3 = $('#proportion_3').val().split('%')[0]
            var pro4 = $('#proportion_4').val().split('%')[0]

            $('#proportion_1').val(pro1)
            $('#proportion_2').val(pro2)
            $('#proportion_3').val(pro3)
            $('#proportion_4').val(pro4)


            var real1 = $('#result_1').val().split('%')[0]
            var real2 = $('#result_2').val().split('%')[0]
            var real3 = $('#result_3').val().split('%')[0]
            var real4 = $('#result_4').val().split('%')[0]

            $('#result_1').val(real1)
            $('#result_2').val(real2)
            $('#result_3').val(real3)
            $('#result_4').val(real4)

            if (totalProportion <= 100)
            {
                $.ajax({
                    method: "POST",
                    url: "{{ route('tablekpi.storeInsert') }}",
                    data: $('#formInsert').serialize(),
                    success: function(response) {
                        if (response.status) {
                            toastr.success(response.message);
                            window.location.href = response.link
                        } else {
                            toastr.error(response.message);
                        }
    
                    },
                    error: function(error) {
                        handleFails(error);
                        proportion1.setRawValue(pro1)
                        proportion2.setRawValue(pro2)
                        proportion3.setRawValue(pro3)
                        proportion4.setRawValue(pro4)

                        result1.setRawValue(real1)
                        result2.setRawValue(real2)
                        result3.setRawValue(real3)
                        result4.setRawValue(real4)
                    }
                })
            } else {
                alert('Vui lòng điều chỉnh lại Tỷ Trọng')
            }
        })

        function handleFails(response) {
            $('#selectPerson, #selectRoom, #selectTeam, #selectPosition, #assign').removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formInsert').find(".has-error").find(".help-block").remove();
                $('#formInsert').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formInsert').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response
                        .responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }

        @if(!auth()->user()->hasRole('super-admin'))
        var id = $('#selectRoom').find(":selected").val();
        if (id != '') {
            var url = "{!! route('helper.getTeamByRommID', ':id') !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (Object.keys(data).length > 0) {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectPosition')
                            .find('option')
                            .remove()
                        @if(auth()->user()->staff->rank == 6)
                        $('#selectTeam').append($('<option>', {
                            value: '',
                            text: "Vui lòng chọn bộ phận",
                            disabled: true,
                            selected: true,
                        }));    
                        Object.keys(data).forEach(function(index) {
                            $('#selectTeam').append($('<option>', {
                                value: data[index].id,
                                text: data[index].name
                            }));
                        @else
                        Object.keys(data).forEach(function(index) {
                            if(data[index].id == {{ auth()->user()->organizational->team_id }}) {
                            $('#selectTeam').append($('<option>', {
                                value: data[index].id,
                                text: data[index].name
                        }));
                        }
                        @endif    
                        })
                    } else {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                    }
                }
            });
        } else {
            $('#selectTeam')
                .find('option')
                .remove()
        }
        @if(auth()->user()->staff->rank < 6)
        var id = {{auth()->user()->organizational->team_id}};
        var url = "{!! route('helper.getPositionByTeamID', ':id') !!}";

        $.ajax({
            type: 'GET',
            url: url.replace(':id', id),
            success: function(data) {
                if (Object.keys(data).length > 0) {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    $('#selectPosition').append($('<option>', {
                        value: '',
                        text: "Vui lòng chọn chức danh",
                        disabled: true,
                        selected: true,
                    }));
                    Object.keys(data).forEach(function(index) {
                        if(data[index].id != {{auth()->user()->organizational->position_id}} && data[index].rank < {{auth()->user()->staff->rank}}) {
                        $('#selectPosition').append($('<option>', {
                            value: data[index].id,
                            text: data[index].name
                        }));
                        }
                    })
                } else {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                }
            }
        });
        @endif
    @endif    

        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            if (id != '') {
                var url = "{!! route('helper.getTeamByRommID', ':id') !!}";

                $.ajax({
                    type: 'GET',
                    url: url.replace(':id', id),
                    success: function(data) {
                        if (Object.keys(data).length > 0) {
                            $('#selectTeam')
                                .find('option')
                                .remove()
                            $('#selectStaff')
                                .find('option')
                                .remove()
                            $('#selectPosition')
                                .find('option')
                                .remove()
                            $('#selectTeam').append($('<option>', {
                                value: '',
                                text: "Vui lòng chọn bộ phận",
                                disabled: true,
                                selected: true,
                            }));
                            Object.keys(data).forEach(function(index) {
                                $('#selectTeam').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name
                                }));
                            })
                        } else {
                            $('#selectTeam')
                                .find('option')
                                .remove()
                        }
                    }
                });
            } else {
                $('#selectTeam')
                    .find('option')
                    .remove()
            }
        });


        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            var url = "{!! route('helper.getPositionByTeamID', ':id') !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (Object.keys(data).length > 0) {
                        $('#selectPosition')
                            .find('option')
                            .remove()
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectPosition').append($('<option>', {
                            value: '',
                            text: "Vui lòng chọn chức danh",
                            disabled: true,
                            selected: true,
                        }));
                        Object.keys(data).forEach(function(index) {
                            @if(!auth()->user()->hasRole('super-admin'))
                            if(data[index].id != {{auth()->user()->organizational->position_id}} && data[index].rank < {{auth()->user()->staff->rank}}) {
                                $('#selectPosition').append($('<option>', {
                                value: data[index].id,
                                text: data[index].name
                            }));
                            }
                            @else
                            $('#selectPosition').append($('<option>', {
                                value: data[index].id,
                                text: data[index].name
                            }));
                            @endif
                        })
                    } else {
                        $('#selectPosition')
                            .find('option')
                            .remove()
                    }
                }
            });
        });


        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();

            var url = "{!! route('helper.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(
                    ':positon_id', position_id),
                success: function(data) {
                    if (Object.keys(data).length > 0) {
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectStaff').append($('<option>', {
                            value: '',
                            text: 'Vui lòng chọn nhân viên'
                        }));
                        Object.keys(data).forEach(function(index) {
                            if (data[index].staff != null) {
                                $('#selectStaff').append($('<option>', {
                                    value: data[index].staff.id,
                                    text: data[index].staff.full_name
                                }));
                            }

                        })
                    } else {
                        $('#selectStaff')
                            .find('option')
                            .remove()
                    }
                }
            });

            if (position_id == "0") {
                $('#selectStaff')
                    .find('option')
                    .remove()
            }
        })

        function loadProportion(event) {
            var result_1 = proportion1.getRawValue()
            var result_2 = proportion2.getRawValue()
            var result_3 = proportion3.getRawValue()
            var result_4 = proportion4.getRawValue()
            var total = Number(result_1) + Number(result_2) + Number(result_3) + Number(result_4)
            $('#totalProportion').text(total + "%")
        }

        $('#proportion_1, #proportion_2, #proportion_3, #proportion_4').change(function(event) {
            loadProportion(event);
        })

    });

</script>
@endsection