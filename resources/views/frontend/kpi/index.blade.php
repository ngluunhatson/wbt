@extends('layouts.fe.master2')
@section('title')
    @lang('translation.tablekpi')
@endsection
@section('css')
    <link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="row justify-content-between mb-3">
        <h3 style="width: fit-content">QUẢN LÝ KPIs NHÂN VIÊN</h3>
        @can('table_kpi-create')
            <a type="button" class="btn btn-success btn-label waves-effect waves-light" style="width: fit-content"
                href={{ route('createTablekpi') }}>
                <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i> Tạo KPI </a>
        @endcan
    </div>

    <!--end row-->

    <div class="row">
        <div class="card-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">
                @if (!auth()->user()->hasRole('super-admin'))
                    <li class="nav-item">
                        <a class="nav-link active" data-bs-toggle="tab" href="#my-work" role="tab"
                            aria-selected="false">
                            KPI của tôi
                        </a>
                    </li>
                @endif
                @if (!auth()->user()->hasAnyRole(['specialist', 'staff']))
                    <li class="nav-item">
                        @if (auth()->user()->hasRole('super-admin'))
                        <a class="nav-link active" data-bs-toggle="tab" href="#assigned-work" role="tab" aria-selected="false">
                            KPI đã giao
                        </a>
                        @else
                        <a class="nav-link" data-bs-toggle="tab" href="#assigned-work" role="tab" aria-selected="false">
                            KPI đã giao
                        </a>
                        @endif
                    </li>
                @endif
            </ul>

            <div class="tab-content text-muted">
                @if (!auth()->user()->hasAnyRole(['specialist', 'staff']))
                @if (!auth()->user()->hasRole('super-admin'))
                    <div class="tab-pane active" id="my-work" role="tabpanel">
                        <div class="row">
                            <div class="row">
                                <div class="col-1 mb-3">
                                    <label for="precious" class="form-label">Quý</label>
                                    <select id="quarterWork" name="quarter"
                                        class="form-select @error('quarter') is-invalid @enderror"
                                        aria-label="Default select example">
                                        <option value="">Tất cả</option>
                                        <option value="1">I</option>
                                        <option value="2">II</option>
                                        <option value="3">III</option>
                                        <option value="4">IV</option>
                                    </select>
                                    @error('quarter')
                                        <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                                <!--end col Quý-->

                                <div class="col-1 mb-3">
                                    <label for="year" class="form-label">Năm</label>
                                    <input autocomplete="off" type="text" id="datepickerWork" value=""
                                        name="year"
                                        class="form-control @error('room_id') is-invalid @enderror datepicker ">
                                    @error('year')
                                        <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                                <!--end col Năm-->
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <table id="example_mywork"
                                        class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Quý</th>
                                                <th>Năm</th>
                                                <th>Người nhận KPI</th>
                                                <th>Người giao KPI</th>
                                                <th>Phòng</th>
                                                <th>Bộ Phận</th>
                                                <th>Chức Danh</th>
                                                <th>Trạng thái</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endif

                @if (!auth()->user()->hasAnyRole(['specialist', 'staff']))
                    @if (auth()->user()->hasRole('super-admin'))
                        <div class="tab-pane active" id="assigned-work" role="tabpanel">
                        @else
                            <div class="tab-pane" id="assigned-work" role="tabpanel">
                    @endif
                    <div class="row">
                        <div class="row">
                            <div class="col-1 mb-3">
                                <label for="precious" class="form-label">Quý</label>
                                <select id="quarterAssign" name="quarter"
                                    class="form-select @error('quarter') is-invalid @enderror"
                                    aria-label="Default select example">
                                    <option value="">Tất cả</option>
                                    <option value="1">I</option>
                                    <option value="2">II</option>
                                    <option value="3">III</option>
                                    <option value="4">IV</option>
                                </select>
                                @error('quarter')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                            <!--end col Quý-->

                            <div class="col-1 mb-3">
                                <label for="year" class="form-label">Năm</label>
                                <input autocomplete="off" type="text" id="datepickerAssign"
                                    value="{{ old('year', now()->format('Y')) }}" name="year"
                                    class="form-control @error('room_id') is-invalid @enderror datepicker ">
                                @error('year')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                            <!--end col Năm-->
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <table id="example_assign"
                                    class="table table-bordered dt-responsive nowrap table-striped align-middle"
                                    style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Quý</th>
                                            <th>Năm</th>
                                            <th>Người nhận KPI</th>
                                            <th>Người giao KPI</th>
                                            <th>Phòng</th>
                                            <th>Bộ Phận</th>
                                            <th>Chức Danh</th>
                                            <th>Trạng thái</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
            @endif
        </div><!-- end card-body -->
    </div>
    {{-- </div> --}}
    </div>



    <!-- end row -->
@endsection
@section('script')
    <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(function() {
            tableLoad('init', '#example_assign', $('#quarterAssign').val(), $('#datepickerAssign').val());
            tableLoad('init', '#example_mywork', $('#quarterWork').val(), $('#datepickerWork').val());
            $('#datepickerAssign').datepicker({
                format: 'yyyy',
                startView: "years",
                minViewMode: "years",
                startDate: 'toDay',
                orientation: 'bottom',
                clearBtn: true
            });
            $('#datepickerWork').datepicker({
                format: 'yyyy',
                startView: "years",
                minViewMode: "years",
                startDate: 'toDay',
                orientation: 'bottom',
                clearBtn: true
            });
            $('#quarterWork, #datepickerWork').on('change', () => {
                quarter = $('#quarterWork').val();
                year = $('#datepickerWork').val();
                tableLoad('filter', '#example_mywork', quarter, year);
            })
            $('#quarterAssign, #datepickerAssign').on('change', () => {
                quarter = $('#quarterAssign').val();
                year = $('#datepickerAssign').val();
                tableLoad('filter', '#example_assign', quarter, year);
            })
            var myTable = $('#example_mywork').dataTable();

            $('#example_assign tbody').on('click', '.remove-item-btn', function(event) {
                eventDeleteKPI(event);
            });

            $('#example_mywork tbody').on('click', '.remove-item-btn', function(event) {
                eventDeleteKPI(event);
            });
        })

        function tableLoad(type, element, quarter, year) {
            var url;
            if (element == '#example_assign') {
                url = "{!! route('tablekpi.data') !!}";
            } else {
                url = "{!! route('tablekpi.loadDataMyWork') !!}"
            }
            $(element).dataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                destroy: true,
                ajax: {
                    url: url,
                    method: "GET",
                    data: {
                        quarter: quarter,
                        year: year
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        width: '10%',
                    },
                    {
                        data: 'quarter',
                        name: 'quarter',
                        width: '10%',
                        orderable: true,
                    },
                    {
                        data: 'year',
                        name: 'year',
                        width: '10%',
                        orderable: true,
                    },
                    {
                        data: 'staff_kpi',
                        name: 'staff_kpi',
                        width: '10%',
                        orderable: true,
                    },
                    {
                        data: 'staff_assign_kpi',
                        name: 'staff_assign_kpi',
                        width: '10%',
                        orderable: true,
                    },
                    {
                        data: 'room',
                        name: 'room',
                        width: '10%',
                        orderable: true,
                    },
                    {
                        data: 'team',
                        name: 'team',
                        width: '15%',
                        orderable: true,
                    },
                    {
                        data: 'position',
                        name: 'position',
                        width: '5%',
                    },
                    {
                        data: 'status',
                        name: 'status',
                        width: '5%',
                        orderable: true,
                    },
                    {
                        data: 'action',
                        name: 'action',
                        width: '7%',
                    }
                ]
            });
        }

        function eventDeleteKPI(event) {
            var form = $(this).closest(".formDelete");
            event.preventDefault();
            swal.fire({
                title: 'Bạn muốn xóa dữ liệu này ?',
                text: "Lưu ý : dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                icon: 'warning',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Đồng ý',
                showDenyButton: true,
                denyButtonText: 'Hủy bỏ',
            })
            .then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                } else if (result.isDenied) {
                    Swal.fire('Dữ liệu được giữ lại', '', 'info')
                }

            });
        }
    </script>
@endsection
