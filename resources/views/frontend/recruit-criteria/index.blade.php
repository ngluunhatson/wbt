@extends('layouts.fe.master2')
@section('title')
@lang('translation.recruit-criteria')
@endsection
@section('css')
<!--datatable css-->
@endsection
@section('content')

<div class="row">
    <h3>TIÊU CHÍ TUYỂN DỤNG</h3>
</div>

<div class="row">
    <div class="card">
        <div class="card-body">
            <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Phòng</th>
                        <th>Bộ Phận</th>
                        <th>Chức Danh</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($dataInit['listData'] as $key => $value)
                    <tr>
                        <td class="dtr-control">{{ $key + 1 }}</td>
                        <td>{{ $value->room != NULL ? $value->room->name : "NULL" }}</td>
                        <td>{{ $value->team != NULL ? $value->team->name : "NULL" }}</td>
                        <td>{{ $value->position != NULL ? $value->position->name : "NULL" }}</td>
                        <td>
                            <div class="dropdown d-inline-block">
                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-fill align-middle"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    @can('recruit_proposal-read')
                                    <li><a href="/recruit-criteria/view/{{$value->room->id}}/{{$value->team->id}}/{{$value->position->id}}" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
                                    @endcan
                                    <!-- <li>
                                        <a href="/recruitment-proposal/delete/{{$value->id}}" class="dropdown-item remove-item-btn">
                                            <i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Delete
                                        </a>
                                    </li> -->
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
@section('script')
<script>
    $(function() {
        $('#example').dataTable();
    })
</script>
@endsection