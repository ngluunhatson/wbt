@extends('layouts.fe.master2')
@section('title')
@lang('translation.personal')
@endsection
@section('css')

@endsection
@section('content')

<style>
    .table td p {
        margin: auto;
    }

    .table>:not(caption)>*>* {
        padding: 6px;
    }

    .table th {
        font-weight: 700;
        text-align: center;
    }

    @print {
        @page :footer {
            display: none
        }

        @page :header {
            display: none
        }
    }

    @media print {
        .address {
            margin-top: 77% !important;
        }

        #btnPrint {
            display: none;
        }
    }
</style>
<div id="btnPrint" class="justify-content-between pe-3 mb-3 d-flex">
    <h3></h3>
    <a href="#">
        <button type="button" id="btnPrint" class="btn btn-primary btn-label waves-effect waves-light">
            <i class="ri-printer-line label-icon align-middle fs-16 me-2"></i>IN</button>
    </a>
</div>
<div class="d-flex justify-content-between" >
    <div>
        <h3 class="mt-4 fw-bold">TIÊU CHÍ TUYỂN DỤNG</h3>
        <div style = "width:26em;">
            <label for="room" class="form-label col-4">Phòng</label>
            <span style="font-size: 15px;">
                {{ !empty($dataInit['recruit']['room']) ? $dataInit['recruit']['room']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div style = "width:26em;">
            <label for="room" class="form-label col-4">Bộ phận</label>
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['recruit']['team']) ? $dataInit['recruit']['team']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div style = "width:26em;">
            <label for="room" class="form-label col-4">Chức danh</label>
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['recruit']['position']) ? $dataInit['recruit']['position']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div class="col-sm-5">
        </div>
    </div>
    <div style="width:35% ; height: fit-content;">
        <img style="object-fit: contain; width: 100%;height: 100%;" src="{{ url('/uploads/common/logo.png') }}">
    </div>
</div>
<!-- <div class="row">
    <div class="d-flex justify-content-left">
        <div class="col-sm-1">
            <label for="room" class="form-label">Phòng</label>
        </div>
        <div class="col-sm-2">
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['recruit']['room']) ? $dataInit['recruit']['room']['name'] : 'Chưa có' }} </span>
        </div>
    </div>
    <div class="d-flex justify-content-left">
        <div class="col-sm-1">
            <label for="room" class="form-label">Bộ phận</label>
        </div>
        <div class="col-sm-2">
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['recruit']['team']) ? $dataInit['recruit']['team']['name'] : 'Chưa có' }} </span>
        </div>
    </div>
    <div class="d-flex justify-content-left">
        <div class="col-sm-1">
            <label for="room" class="form-label">Chức danh</label>
        </div>
        <div class="col-sm-2">
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['recruit']['position']) ? $dataInit['recruit']['position']['name'] : 'Chưa có' }} </span>
        </div>
    </div>
</div> -->
<div class="row card-body">
    <table class="table table-bordered border-dark dt-responsive nowrap align-middle" style="width:100%; background: white;">
        <thead>
            <tr>
                <th>STT</th>
                <th>HẠNG MỤC</th>
                <th>NỘI DUNG</th>
            </tr>
        </thead>
        <tbody>
            @php
            $i = 1;
            $arrayHeaderCriteria = ['location', 'recruitment_requirements', 'capability_requirements', 'function', 'wage', 'bonus'];
            $arrayHeaderRecruit = ['job_duties', 'disc'];
            @endphp
            @foreach(config('constants.header_recruit_criteria') as $key => $value)
            <tr>
                @if(in_array($key, $arrayHeaderCriteria))
                <td class="text-center" style="width: 7%;">{{ $i }}</td>
                <td style="width: 20%;">{{ $value }}</td>
                <td> {{ !empty($dataInit['criteria']) ? $dataInit['criteria'][$key] : '' }}</td>
                @php $i ++; @endphp
                @endif
                @if(in_array($key, $arrayHeaderRecruit))
                <td class="text-center" style="width: 7%;">{{ $i }}</td>
                <td style="width: 20%;">{{ $value }}</td>
                <td>{{ !empty($dataInit['recruit']) ? $dataInit['recruit'][$key] : '' }}</td>
                @php $i ++; @endphp
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row mb-5">
                    <div class="col-3 d-flex justify-content-center">
                        <div class="row  ">
                            <div class="col-sm-12 text-center">
                                <h5 class="fw-bold">Người Đề Xuất</h5>
                            </div>
                            <div class="col-12 text-center">
                                <label for="firstNameinput" class="form-label mb-0">{{ $dataInit['recruit']['user']['name'] }}</label>
                            </div>
                            <div class="col-12 text-center">
                                @if($dataInit['recruit']['status'] != 1)
                                <label for="firstNameinput" class="form-label mb-0 mt-4 ">Đã xác nhận</label>
                                @else
                                <div class="col-12 d-flex justify-content-center mt-4">
                                    <div class=" form-check" style="width: fit-content;">
                                        <input disabled class="form-check-input fs-15" type="checkbox" id="assign" value="1" name="assign">
                                    </div>
                                    <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác
                                        nhận</label>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--end col Người giao việc-->
                    @php
                    $idConfirmer = [];
                    $id;
                    $idConfirmerNext;
                    $type;
                    @endphp
                    @foreach( $dataInit['recruit']['confirms'] as $key => $value)
                    <input name="confirm_id_{{$key+1}}" hidden>
                    <div class="col-3 d-flex justify-content-center">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                @switch($value['confirm']['type_confirm'])
                                @case(4)
                                @php
                                if ($dataInit['recruit']['status'] == 2) {
                                $idConfirmer[] = $value['confirm']['basic_info_id'];
                                }
                                $id = $value['confirm']['id'];
                                $type = 4;
                                @endphp
                                <h5 class="fw-bold">Quản Lý Phòng</h5>
                                @break
                                @case(2)
                                @php
                                $idConfirmerNext = $dataInit['recruit']['status'] == 2 ? $value['confirm']['basic_info_id'] : '';
                                @endphp
                                @if ($dataInit['recruit']['status'] == 3 && empty($idConfirmer) && $value['confirm']['status'] == 0)
                                @php
                                $idConfirmer[] = $value['confirm']['basic_info_id'];
                                $id = $value['confirm']['id'];
                                $type = 2;
                                @endphp
                                @endif
                                <h5 class="fw-bold">P.QT HC-NS</h5>
                                @break
                                @case(3)
                                @php
                                if ($dataInit['recruit']['status'] == 3) {
                                $idConfirmerNext = $value['confirm']['basic_info_id'];
                                }
                                @endphp
                                @if ($dataInit['recruit']['status'] == 4 && empty($idConfirmer) && $value['confirm']['status'] == 0)
                                @php
                                $idConfirmer[] = $value['confirm']['basic_info_id'];
                                $id = $value['confirm']['id'];
                                $type = 3;
                                @endphp
                                @endif
                                <h5 class="fw-bold">Ban Giám Đốc</h5>
                                @break
                                @endswitch
                            </div>
                            <div class="col-sm-12 text-center">
                                @switch($value['confirm']['type_confirm'])
                                @case(1)
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] ?? 'Chưa có' }}</label>
                                @break
                                @case(2)
                                @if(!empty($dataInit['hr']))
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] }}</label>
                                @endif
                                @break
                                @case(3)
                                @if(!empty($dataInit['manager']))
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] }}</label>
                                @endif
                                @break
                                @case(4)
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff']['full_name'] }}</label>
                                @break
                                @endswitch
                            </div>
                            <div class="col-sm-12 text-center">
                            <div class="col-12 text-center mt-2">
                                @if($value['confirm']['type_confirm'] != 1)
                                @switch($value['confirm']['status'])
                                @case(0)
                                <label for="firstNameinput" class="form-label mb-0">{{ 'Chưa xác nhận' }}</label>
                                @break
                                @case(1)
                                <label for="firstNameinput" class="form-label mb-0">{{ 'Đã xác nhận' }}</label>
                                @break
                                @case(2)
                                <label for="firstNameinput" class="form-label mb-0">{{ 'Từ chối' }}</label>
                                <div>
                                    <label>Lý do : {{ $value['confirm']['note']}}</label>
                                </div>
                                @break
                                @endswitch
                                @else
                                <label for="firstNameinput" class="form-label mb-0"></label>
                                @endif
                            </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="card-body mt-5">
                <p class="fw-bold m-0">ActionCOACH CBD Firm</p>
                <p class="m-0">Địa chỉ: CBD Building, 90 - 92 Lê Thị Riêng, Phường Bến Thành, Quận 1, TP.HCM</p>
                <p class="m-0">Hotline: 1800 8087</p>
            </div>
        </div>
    </div>
</div>






























































































































@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#btnPrint').on('click', function() {
            window.print()
        })
        // $('#btnDenie').on('click', function(event) {
        //     var status = $(this).data('id');
        //     var form = $(this).closest("form");
        //     event.preventDefault();
        //     swal.fire({
        //             title: 'Lý do không duyệt đơn',
        //             html: `<input class="form-control" type="text" id="reason"  placeholder="Lý do" required>`,
        //             icon: 'warning',
        //             confirmButtonColor: '#3085d6',
        //             cancelButtonColor: '#d33',
        //             confirmButtonText: 'Đồng ý',
        //             showDenyButton: true,
        //             denyButtonText: 'Hủy',
        //             preConfirm: () => {
        //                 const reason = Swal.getPopup().querySelector('#reason').value
        //                 if (!reason) {
        //                     Swal.showValidationMessage(`Vui lòng nhập lý do`)
        //                 }
        //                 return {
        //                     reason: reason,
        //                 }
        //             }
        //         })
        //         .then((result) => {
        //             if (result.isConfirmed) {
        //                 $('#reasonConfirm').val(result.value.reason);
        //                 $('#statusConfirm').val(2);
        //                 form.submit();
        //             } else if (result.isDenied) {
        //                 Swal.fire('Your record is safe', '', 'info')
        //             }

        //         });
        // });
    });
</script>
@endsection