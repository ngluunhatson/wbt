@extends('layouts.fe.master2')
@section('title')
@endsection
@section('css')
@endsection
@section('content')
<!-- general form elements -->
<div class="col-md-12">
    <div class="card card-default">
        <div class="card-header">
            <h2 class="card-title">Edit User</h2>
            <div class="card-tools">
                <a class="btn btn-success" href="{{ route('users.index') }}"><i class="fa fa-angle-double-left"></i>  Back To User List</a>
            </div>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="POST" action="{{ route('users.update', $user->id) }}">
        @csrf
        <div class="card-body">        
            <div class="row">
                <div class="col-md-6">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Tên:</strong>
                            <input  type="text" name="name" value="{{ old('name', $user->name) }}" placeholder="Name" class="form-control">
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Email:</strong>
                            <input type="email" name="email" value="{{ old('email', $user->email)  }}" placeholder="Email" class="form-control">
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Mật khẩu:</strong>
                            <input name="password" type="password" placeholder="Password" class="form-control">
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Xác nhận mật khẩu:</strong>
                            <input name="confirm-password" type="password" placeholder="Confirm Password" class="form-control">
                            <span class="text-danger">{{ $errors->first('confirm-password') }}</span>
                        </div>
                    </div>
                </div>   
                <div class="col-md-6">
                    <div class="form-group">
                        <strong>Role:</strong>
                        <select name="roles" value="{{ old('roles') }}" class="form-control">
                        <option value="">Vui lòng chọn Role</option>
                        @foreach($roles as $role)
                            <option @selected(in_array($role, $userRole)) value="{{ $role }}">{{$role}}</option>
                        @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('roles') }}</span>
                    </div>
                </div>     
            </div>        
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>        
        </div>
        </form>
    </div>
    <!-- /.card -->
</div>
@endsection