@extends('layouts.fe.master2')
@section('title')
@endsection
@section('css')
@endsection
@section('content')


<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Users Management</h2>
            <div class="card-tools">
                @can('user-create')
                <a class="btn btn-success" href="{{ route('users.create') }}"><i class="fas fa-plus-square"></i> Add User</a>
                @endcan
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive">
            <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                <thead>
                    <tr class="bg-blue text-center">
                        <th width="50px">No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Roles</th>
                        <th width="150px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $key => $user)
                    <tr>
                        <td class="text-center">{{ ++$key }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td class="text-center">
                            @if(!empty($user->getRoleNames()))
                            @foreach($user->getRoleNames() as $v)
                            <label class="badge bg-success">{{ $v }}</label>
                            @endforeach
                            @endif
                        </td>
                        <td class="text-center">
                            @can('user-edit')
                            <a class="btn btn-sm btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                            @endcan
                            @can('user-delete')
                            <form method="post" id="formDelete" action="{{ route('users.destroy', $user->id) }}" style="display:inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-sm btn-danger delete_confirm">Delete</button>
                            </form>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.card -->
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('#example').dataTable();
        $('.delete_confirm').click(function(event) {
            var form = $(this).closest("form");
            event.preventDefault();
            swal.fire({
                    title: 'Are you sure you want to delete this record?',
                    text: "If you delete this, it will be gone forever.",
                    icon: 'warning',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    showDenyButton: true,
                    denyButtonText: 'Cancel',
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        form.submit();
                    } else if (result.isDenied) {
                        Swal.fire('Your record is safe', '', 'info')
                    }

                });
        });
    });
</script>
@endsection