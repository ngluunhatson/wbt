@extends('layouts.fe.master2')
@section('title')
@endsection
@section('css')
@endsection
@section('content')

<div class="col-md-12">
<form method="POST" id="formInsert" enctype="multipart/form-data">
    @csrf
    <div class="row g-2">
        <div class="col-lg-4">
            <div class="form-group">
                <label for="oldpasswordInput" class="form-label">Mật khẩu hiện tại*</label>
                <input id="oldpasswordInput" type="password" class="form-control" name="old_password" placeholder="Enter current password">
            </div>
        </div>
        <!--end col-->
        <div class="col-lg-4">
            <div class="form-group">
                <label for="newpasswordInput" class="form-label">Mật khẩu mới*</label>
                <input id="newpasswordInput" type="password" class="form-control" name="new_password" placeholder="Enter new password">
            </div>
        </div>
        <!--end col-->
        <div class="col-lg-4">
            <div class="form-group">
                <label for="confirmpasswordInput" class="form-label">Nhập lại mật khẩu*</label>
                <input id="confirmpasswordInput" type="password" class="form-control" name="confirm_password" placeholder="Confirm password">
            </div>
        </div>
        <!--end col-->
        <!-- <div class="col-lg-12">
            <div class="mb-3">
                <a href="javascript:void(0);" class="link-primary text-decoration-underline">Forgot Password ?</a>
            </div>
        </div> -->
        <!--end col-->
        <div class="col-lg-12">
            <div class="text-end">
                <button type="submit" class="btn btn-success">Đổi mật khẩu</button>
            </div>
        </div>
        <!--end col-->
    </div>
</form>
   
</div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
           
        $('#formInsert').on('submit', function(event) {
            event.preventDefault()
            console.log('form', $('#formInsert')[0])
            var form = $("form").serializeArray().filter(item => item.name !== '_token')
            form = form.map(item => ({
                ...item,
                idElement: document.getElementsByName(`${item.name}`)[0].id
            }))
            var emptyElement = form.filter(item => !item.value)
            if(emptyElement.length > 0) {
                var message = emptyElement[0].name === 'old_password' ? 'Mật khẩu hiện tại' :  emptyElement[0].name === 'new_password' ? 'Mật khẩu mới' : 'Mật khẩu xác nhận'
               $(`#${emptyElement[0].idElement}`).focus()
               toastr.error(`Vui lòng nhập ${message}`);
            } else {

                var new_pass = $('#newpasswordInput').val();
                var confirm_pass = $('#confirmpasswordInput').val();
                if(new_pass !== confirm_pass) {
                    toastr.error("Mật khẩu xác nhận không khớp");
                } else {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('user.changePassword') }}",
                        data: $('#formInsert').serialize(),
                        success: function(response) {
                            if (response.status) {
                                toastr.success(response.message);
                                window.location.href = response.link
                            } else {
                                toastr.error(response.message);
                            }
        
                        },
                        error: function(error) {
                            handleFails(error);
                        }
                    })
                }
            }
        })
        function handleFails(response) {
            $('#oldpasswordInput, #newpasswordInput, #confirmpasswordInput').removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formInsert').find(".has-error").find(".help-block").remove();
                $('#formInsert').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formInsert').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response.responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }
    })
</script>
@endsection