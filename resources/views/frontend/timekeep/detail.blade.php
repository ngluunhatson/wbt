<table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
    <thead>
        <tr>
            <th>STT</th>
            <th>Công ty</th>
            <th>Mã nhân viên</th>
            <th>Họ tên</th>
            <th>Tên tiếng Anh</th>
            <th>Phòng</th>
            @foreach($dataInit['header_day'] as $headerDay)
            <th class="cus_th">
                <p>{{ $headerDay['days'] }}</p>
                <p>{{ $headerDay['date'] }}</p>
            </th>
            @endforeach
            <th>Có <br> hưởng <br> lương</th>
            <th>PN</th>
            <th>Không <br> hưởng <br> lương</th>
            <th>Khác</th>
            <th>Tổng công <br> có hưởng lương</th>
            <th>Ghi chú</th>
            <!-- <th>Action</th> -->
        </tr>
    </thead>
    <tbody>
        @if(!empty($dataInit['dataList']))
        @php $i = 1; @endphp
        @foreach($dataInit['dataList'] as $detail)
    
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $detail['staff']['company'] }}</td>
            <td>{{ $detail['staff']['employee_code'] }}</td>
            <td>{{ $detail['staff']['full_name'] }}</td>
            <td>{{ $detail['staff']['english_name'] }}</td>
            <td>{{ $detail['staff']['organizational'] ? $detail['staff']['organizational']['room']['name'] : 'Chưa Có'}}</td>
            @foreach($dataInit['header_day'] as $index => $headerDay)
            @php 
                $detailDay =  config('constants.type_timekeep')[$detail['day_'.($index+1)]];
            @endphp
            <td data-id="{{ $detail['id'] }}" class="{{ $detailDay['class'] }}" data-dayID="{{ $headerDay['date'] }}">
                {{ $detailDay['value'] }}
            </td>
            @endforeach
            <td class="fw-700">{{ $detail['with_salary'] }}</td>
            <td class="fw-700">{{ $detail['paid_holidays'] }}</td>
            <td class="fw-700">{{ $detail['no_salary'] }}</td>
            <td data-id="{{ $detail['id'] }}" class="fw-700">{{ $detail['other'] }}</td>
            <td class="fw-700">{{ $detail['total_salary'] }}</td>
            <td data-id="{{ $detail['id'] }}" class="fw-700">{{ $detail['note'] }}</td>
        </tr>
        @php $i++; 
        @endphp
        @endforeach
        @endif
    </tbody>
</table>