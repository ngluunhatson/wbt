@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<style>
  .cus_th {
    padding: 0 !important;
  }

  .cus_th p {
    padding: 2px;
    text-align: center;
    margin: 0;
  }

  .badge {
    display: block;
    margin: 3px;
    color: white;
  }

  .table th,
  td {
    white-space: nowrap;
    text-align: center;
    vertical-align: middle;
  }

  .select-cus {
    background-image: none;
    background-image: none;
    padding: 5px 8px;
    /* width: fit-content; */
    cursor: pointer;
    text-align: center;
    border: 1px solid;
  }

  /* .select-cus.foo,
  option[value="c1"] {
    background: red;
  } */

  .c1 {
    background: white !important;
    color: black !important;
    font-weight: 700 !important;
  }

  .c2 {
    background: white !important;
    color: black !important;
    font-weight: 700 !important;
  }

  .c3 {
    background: white !important;
    color: red !important;
    font-weight: 700 !important;
  }

  .c4 {
    background: white !important;
    color: red !important;
    font-weight: 700 !important;
  }

  .c5 {
    background: #B2A1C7 !important;
    color: black !important;
    font-weight: 700 !important;
  }

  .c6 {
    background: #A5A5A5 !important;
    color: black !important;
    font-weight: 700 !important;
  }

  .c7 {
    background: #4BACC6 !important;
    color: black !important;
    font-weight: 700 !important;
  }

  .c8 {
    background: #F1C232 !important;
    color: black !important;
    font-weight: 700 !important;
  }

  .c9 {
    background: #92D050 !important;
    color: black !important;
    font-weight: 700 !important;
  }

  .c10 {
    background: #92D050 !important;
    color: black !important;
    font-weight: 700 !important;
  }

  .c11 {
    background: #FFE599 !important;
    color: black !important;
    font-weight: 700 !important;
  }

  .c12 {
    background: #DD7E6B !important;
    color: black !important;
    font-weight: 700 !important;
  }

  .c13 {
    background: white !important;
    color: black !important;
    font-weight: 700 !important;
  }

  .fw-700 {
    font-weight: 700 !important;
  }

  .fw-700-red {
    font-weight: 700 !important;
    color: red;
  }

  .my-confirm-class {
    padding: 3px 6px;
    font-size: 12px;
    color: white;
    text-align: center;
    vertical-align: middle;
    border-radius: 4px;
    background-color: #337ab7;
    text-decoration: none;
  }

  .my-cancel-class {
    padding: 3px 6px;
    font-size: 12px;
    color: white;
    text-align: center;
    vertical-align: middle;
    border-radius: 4px;
    background-color: #a94442;
    text-decoration: none;
  }
</style>

@php
$canUpdate = auth() -> user() -> hasAnyPermission(['timekeep-update']);
@endphp

<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <form id="formSearch" method="POST" action="{{ route('timekeep.index') }}">
          @csrf
          <div class="row gy-3">
            <div class="col-xl-3">
              <label for="borderInput" class="form-label">Phòng</label>
              <select name="room_id" id="selectRoom" class="form-select" aria-label="Default select example">
                <option value="0">Tất Cả Các Phòng</option>

                @foreach($dataInit['rooms'] as $value)
                <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-xl-3">
              <label for="datePicker" class="form-label">Tháng / năm</label>
              <input type="text" class="form-control" name="monthYear" id="datePicker" value="{{ old('monthYear', date('m/Y', strtotime(now()) ) ) }}">
            </div>
            <div class="col-xl-2">
              <label for="borderInput" class="form-label">Từ ngày:</label>
              <input type="text" class="form-control border-dashed" id="startDate" value="{{ $dataInit['startDate']->format('d/m/Y') }}" readonly>
            </div>
            <div class="col-xl-2">
              <label for="borderInput" class="form-label">Đến ngày:</label>
              <input type="text" class="form-control border-dashed" id="endDate" value="{{ $dataInit['endDate']->format('d/m/Y') }}" readonly>
            </div>
            <div class="col-xl-2">
              <label for="borderInput" class="form-label">Công chuẩn:</label>
              <input type="text" class="form-control border-dashed" id="workNumber" value="{{ $dataInit['workNumber'] }}" readonly>
            </div>
          </div>
        </form>
      </div>
      <div class="card-body overflow-auto contentTable">
        @include('frontend.timekeep.detail', ['dataInit' => $dataInit])
      </div>
    </div>
  </div>
  <!--end col-->
</div>
<!--end row-->

@endsection
@section('script')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script>
  var lstValue = <?php echo  json_encode(array_slice(config('constants.type_timekeep'), 1)) ?>;
  const canUpdate = <?php echo json_encode($canUpdate) ?>;

  $(document).ready(function() {

    $(function() {
      $.ajax({
        type: 'POST',
        url: "{{ route('timekeep.loadData') }}",
        data: $('#formSearch').serialize(),
        success: function(data) {
          $('.contentTable').empty();
          $('.contentTable').append(data.html)
          $('#startDate').val(data.startDate)
          $('#endDate').val(data.endDate)
          $('#workNumber').val(data.workNumber)
          settingEditTable(lstValue, data)
        },
      })
    });


    $('#datePicker').datepicker({
      format: 'mm/yyyy',
      startView: "months",
      minViewMode: "months",
    });

    $('#selectRoom, #datePicker').on('change', function() {
      $.ajax({
        type: 'POST',
        url: "{{ route('timekeep.loadData') }}",
        data: $('#formSearch').serialize(),
        success: function(data) {
          $('.contentTable').empty();
          $('.contentTable').append(data.html)
          $('#startDate').val(data.startDate)
          $('#endDate').val(data.endDate)
          $('#workNumber').val(data.workNumber)
          settingEditTable(lstValue, data)
        },
      })
    })
  });
</script>
<script>
  function selectChangeBackGround() {
    $('.select-cus').off('change');
    $('.select-cus').on('change', function(ev) {
      $(this).attr('class', 'form-select select-cus').addClass($(this).children(':selected').attr('class'));
    });
  }

  if ($('#select1').val() != '') {
    var name = $('#select1').children(':selected').attr('class');
    $('#select1').addClass(name);
  }

  function saveOtherFields(id, newValue, field_name) {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
      type: "POST",
      url: "{{ route('updateTimeKeepOtherFields') }}",
      data: {
        newValue: newValue,
        timeKeepId: id,
        fieldName: field_name,
      },
      success: function(response) {
        if (response.status) {
          $.ajax({
            type: 'POST',
            url: "{{ route('timekeep.loadData') }}",
            data: $('#formSearch').serialize(),
            success: function(data) {
              $('.contentTable').empty();
              $('.contentTable').append(data.html)
              settingEditTable(lstValue, data)
            },
          });
          toastr.success(response.message);
        } else {
          toastr.error(response.message);
        }
      }
    });
  }

  function saveStatusDay(id, day, newValue) {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      type: "POST",
      url: "{{ route('updateTimeKeep', ['', '']) }}" + '/' + id + '/' + day,
      data: {
        newValue: newValue,
        room: $('#selectRoom').val(),
        monthYear: $('#datePicker').val(),
        workNumber: $('#workNumber').val()
      },
      success: function(response) {
        if (response.status) {
          $.ajax({
            type: 'POST',
            url: "{{ route('timekeep.loadData') }}",
            data: $('#formSearch').serialize(),
            success: function(data) {
              $('.contentTable').empty();
              $('.contentTable').append(data.html)
              settingEditTable(lstValue, data)
            },
          })
          toastr.success(response.message);
        } else {
          toastr.error(response.message);
        }

      },
    });
  }

  function getSunDayColumn(startDateString, endDateString) {

    const startDayInt = parseInt(startDateString.slice(0, 2));
    const startMonthInt = parseInt(startDateString.slice(3, 5)) - 1;
    const startYearInt = parseInt(startDateString.slice(6));

    const endDayInt = parseInt(endDateString.slice(0, 2));
    const endMonthInt = parseInt(endDateString.slice(3, 5)) - 1;
    const endYearInt = parseInt(endDateString.slice(6));


    const dateStart = new Date(startYearInt, startMonthInt, startDayInt);
    const dateEnd = new Date(endYearInt, endMonthInt, endDayInt);

    let i = dateStart;
    let SunColumns = [];
    let date = 1;
    while (i <= dateEnd) {
      if (i.getDay() == 0)
        SunColumns.push(date);
      i.setDate(i.getDate() + 1);
      date += 1;
    }

    return SunColumns;
  }

  function settingEditTable(lstValue, data) {
    var lstOption = [];
    var lstInput = [];
    var lstColumn = [];

    const sunday_column = getSunDayColumn(data.startDate, data.endDate);

    Object.keys(lstValue).forEach((index) => {
      lstOption.push({
        "value": index,
        "class": lstValue[index]['class'],
        "display": lstValue[index]['value']
      })
    })
    for (i = 6; i <= (data.daysInMonth + 5); i++) {
      if (!sunday_column.includes(i - 5)) {
        lstColumn.push(i)
        lstInput.push({
          "column": i,
          "type": "list-timekeep",
          "options": lstOption
        })
      }
    }

    lstColumn.push(data.daysInMonth + 9);
    lstColumn.push(data.daysInMonth + 11);

    lstInput.push({
      "column": data.daysInMonth + 9,
      "type": "number",
      "options": null
    });

    lstInput.push({
      "column": data.daysInMonth + 11,
      "type": "text",
      "options": null
    });


    var table = $('#example').DataTable({
      responsive: false,
      columnDefs: [{
        orderable: false,
        targets: lstColumn
      }]
    });

    function myCallbackFunction(updatedCell, updatedRow, newValue, oldValue) {
      const column_num = updatedCell.node()._DT_CellIndex.column;
      var id = $(updatedCell.node()).data('id');
      var day = parseInt($(updatedCell.node()).data('dayid'));

      if (column_num == data.daysInMonth + 9 || column_num == data.daysInMonth + 11)
        if (column_num == data.daysInMonth + 9)
          saveOtherFields(id, newValue, "other");
        else
          saveOtherFields(id, newValue, "note");
      else
        saveStatusDay(id, day, newValue);
    }

    if (canUpdate)
      table.MakeCellsEditable({
        "onUpdate": myCallbackFunction,
        "columns": lstColumn,
        "inputCss": "form-select select-cus",
        "type": 'timekeep',
        "lstTypeSelect": lstValue,
        "inputTypes": lstInput,
        "confirmationButton": {
          "confirmCss": 'my-confirm-class',
          "cancelCss": 'my-cancel-class'
        },
      });


    $('#example tbody').on('click', 'td', function() {
      selectChangeBackGround();
    });
  }
</script>
@endsection