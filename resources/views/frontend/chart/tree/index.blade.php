@extends('layouts.fe.master2')
@section('title') @lang('translation.animation') @endsection
@section('css')
<link href="{{ URL::asset('/assets/libs/aos/aos.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('/assets/css/extra.css') }}" rel="stylesheet">
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

@endsection
@section('content')

@component('components.breadcrumb')
@slot('li_1') Advance UI @endslot
@slot('title') Animation @endslot
@endcomponent
<div class="row">
   <label for="basiInput" class="form-label">Chọn phòng ban</label>
   <form id="formSubmit" action="{{ route('tree.index') }}" method="GET">
      <select id="roomSelect" name="room_id" class="form-select form-select-lg" aria-label=".form-select-lg example">
         <option value="0">Vui lòng chọn phòng ban</option>
         @foreach($dataOfSelect['rooms'] as $room)
         <option @if(session()->has('room_id')) @if(session('room_id') == $room['id']) selected @endif @endif value="{{ $room['id'] }}">{{ $room['name'] }}</option>
         @endforeach
      </select>
   </form>
</div>
<div class='overflow'>
   <div>
      <ul class="tree" style="display: none;" rel="1">
         @foreach($tree as $data)
         <li class="">
            <div data-id="{{ $data['id'] }}">
               <div class='img'>
                  <img id="avatarView" src="{{ !empty($data['staff']) ? url($data['staff']['avatar']) : '/assets/chartImg/user.jpg' }}">
               </div>
               <h6 class='name'>{{ !empty($data['staff']) ? $data['staff']['full_name'] : 'Chưa có' }}</h6> <!-- Tên -->
               <span class='employeeCode'>{{ !empty($data['staff']) ? $data['staff']['employee_code'] : 'Chưa có' }}</span> <!-- Mã nv -->
               <h6 class='position'>{{ !empty($data['position']) ? $data['position']['name'] : 'Chưa có' }}</h6> <!-- Chức danh -->
               <span class='room'>{{ !empty($data['team']) ? $data['team']['name'] : 'Chưa có' }}</span> <!-- Phòng -->
               <!-- <small class='view_btn'>Thông tin</small> -->
            </div>
            @if (!empty($data['children']))
            <ul id="parent" data-parentid="{{ $data['id'] }}">
               @foreach ($data['children'] as $childCategory)
               @include('frontend.chart.tree.child', ['data' => $childCategory])
               @endforeach
            </ul>
            @endif
         </li>
         @endforeach
      </ul>
   </div>
</div>
<!-- <section id="insertMode" class="form_box" style="display: none;">
   <form class="add_data" method="post" action="" enctype="multipart/form-data">
      @csrf
      <img class="close" id="insertClose" src="/assets/chartImg/close.png" />
      <div class="img avatar"><img id="avatarInsert" src="/assets/chartImg/user.jpg" /></div>
      <input accept="image/png, image/gif, image/jpeg" class="form-control" type="file" id="myfile" name="myfile">
      <label class="title">Nhân viên</label>
   
      <select class="form-select form-select-sm  mb-3" name="title" aria-label=".form-select-sm example">
         <option value="LOPMASE">LOPMASE</option>
         <option value="LOPMASE1">LOPMASE1</option>
         <option value="LOPMASE2">LOPMASE2</option>
         <option value="LOPMASE3">LOPMASE3</option>
      </select>
   
      <div>
         <label for="readonlyInput" class="form-label">Phòng</label>
         <input type="text" class="form-control" id="readonlyInputRoom" value="Phòng công nghệ" readonly>
      </div>
      <div>
         <label for="readonlyInput" class="form-label">Bộ phận</label>
         <input type="text" class="form-control" id="readonlyInput" value="QUẢN TRỊ KINH DOANH" readonly>
      </div>
      <div>
         <label for="readonlyInput" class="form-label">Chức danh</label>
         <input type="text" class="form-control" id="readonlyInput" value="COACHING 1:1" readonly>
      </div>
   
      <div>
         <label for="datepicker" class="form-label">Thời gian</label>
         <input type="date" id="datepicker" class="form-control">
      </div>
   
      <div class="form-check form-switch form-switch-secondary mt-2">
         <input class="form-check-input" type="checkbox" role="switch" name="showhideval" id="hide">
         <label class="form-check-label" for="hide">Hide Child Branch</label>
      </div>
      <input type="submit" class="submit mt-2" name="submit" value="Submit">
   </form>
</section> -->
<section id="editMode" class="form_box" style="display: none;">
   <form id="formEdit" class="edit_data" method="post" enctype="multipart/form-data">
      @csrf
      <input id="editId" name="id" hidden>
      <img class="close" id="editClose" src="/assets/chartImg/close.png" />
      <div class="img avatar">
         <img id="avatarEdit" src="/assets/chartImg/user.jpg" />
      </div>
      <label class="title">Nhân viên</label>
      <select id="title" name="basic_info_id" class="form-select form-select-sm  mb-3" aria-label=".form-select-sm example">
      </select>

      <div>
         <label for="readonlyInput" class="form-label">Bộ phận</label>
         <select id="teamEdit" name="team_id" class="form-select form-select-sm  mb-3" aria-label=".form-select-sm example">
            <option value=""> Vui lòng chọn bộ phận </option>

         </select>
      </div>
      <div>
         <label for="readonlyInput" class="form-label">Chức danh</label>
         <select id="positionEdit" name="position_id" class="form-select form-select-sm  mb-3" aria-label=".form-select-sm example">
            <option value=""> Vui lòng chọn chức danh </option>

         </select>
      </div>
      <div>
         <label for="datepicker" class="form-label">Thời gian</label>
         <input type="text" id="datepicker" name="year" class="form-control datepicker">
      </div>

      <!-- <div class="form-check form-switch form-switch-secondary mt-2">
         <input class="form-check-input" type="checkbox" role="switch" name="showhideval" id="hideEdit">
         <label class="form-check-label" for="hide">Hide Child Branch</label>
      </div> -->
      <input type="submit" class="edit" id="editSubmit" name="submit" value="Update">
   </form>
</section>


@endsection
@section('script')
<script src="{{ asset('/assets/libs/aos/aos.min.js') }}"></script>
<script src="{{ asset('/assets/libs/prismjs/prismjs.min.js') }}"></script>
<script src="{{ asset('/assets/js/pages/animation-aos.init.js') }}"></script>
<script src="{{ asset('/assets/js/app.min.js') }}"></script>

<!-- jQuery Library -->

<script src="{{ asset('/assets/js/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery-migrate-3.3.2.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Tree plugin js -->
<script>
   const csrf = "{{ csrf_token() }}";
   $(document).ready(function() {
      $('#datepicker').datepicker({
         format: 'yyyy',
         startView: "years",
         minViewMode: "years",
      });

      if ($('#roomSelect').val() != 0) {
         $('.tree').show();
         $('.tree').tree_structure({
            'add_option': true,
            'edit_option': true,
            'delete_option': true,
            'confirm_before_delete': true,
            'fullwidth_option': false,
            'align_option': 'center',
            'draggable_option': true
         });
      } else {
         $('.tree').hide();
         // $.fn.tree_structure.destroy()
      }
      // Tree plugin call
      $('#roomSelect').on('change', function() {
         if ($('#roomSelect').val() == 0) {
            $.ajax({
               type: 'GET',
               url: '/chart/clearSession/room_id',
            });
         }
         $('#formSubmit').submit();
      })

      $('#teamEdit').on('change', function() {
         var id = $('#teamEdit').find(":selected").val();
         loadPosition(id);
      });

      $('#positionEdit').on('change', function() {
         var room_id = $('#selectRoom').find(":selected").val();
         var team_id = $('#teamEdit').find(":selected").val();
         var position_id = $('#positionEdit').find(":selected").val();

         loadStaff(room_id, team_id, position_id);

         if (position_id == "0") {
            $('#title')
               .find('option')
               .remove()
         }
      })
   });

   function loadTeam(id, teamID = null) {
      if (id != '') {
         var url = "{!! route('helper.getTeamByRommID', ':id') !!}";

         $.ajax({
            type: 'GET',
            url: url.replace(':id', id),
            success: function(data) {
               if (Object.keys(data).length > 0) {
                  $('#teamEdit')
                     .find('option')
                     .remove()
                  $('#positionEdit')
                     .find('option')
                     .remove()
                  $('#teamEdit').append($('<option>', {
                     value: '',
                     text: "Vui lòng chọn bộ phận",
                     disabled: true,
                     selected: true,
                  }));
                  Object.keys(data).forEach(function(index) {
                     if (Number.parseInt(teamID) == data[index].id) {
                        $('#teamEdit').append($('<option>', {
                           value: data[index].id,
                           text: data[index].name,
                           selected: true
                        }));
                     } else {
                        $('#teamEdit').append($('<option>', {
                           value: data[index].id,
                           text: data[index].name,
                        }));
                     }


                  })
               } else {
                  $('#teamEdit')
                     .find('option')
                     .remove()
               }
            }
         });
      } else {
         $('#teamEdit')
            .find('option')
            .remove()
      }
   }

   function loadPosition(id, poisitionID = null) {
      var url = "{!! route('helper.getPositionByTeamID', ':id') !!}";

      $.ajax({
         type: 'GET',
         url: url.replace(':id', id),
         success: function(data) {
            if (Object.keys(data).length > 0) {
               $('#positionEdit')
                  .find('option')
                  .remove()
               $('#positionEdit').append($('<option>', {
                  value: '',
                  text: "Vui lòng chọn chức danh",
                  disabled: true,
                  selected: true,
               }));
               Object.keys(data).forEach(function(index) {
                  if (Number.parseInt(poisitionID) == data[index].id) {
                     $('#positionEdit').append($('<option>', {
                        value: data[index].id,
                        text: data[index].name,
                        selected: true,
                     }));
                  } else {
                     $('#positionEdit').append($('<option>', {
                        value: data[index].id,
                        text: data[index].name
                     }));
                  }
               })
            } else {
               $('#positionEdit')
                  .find('option')
                  .remove()
            }
         }
      });
   }

   function loadStaff(room_id, team_id, position_id, staffID = null) {
      var url = "{!! route('helper.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";

      $.ajax({
         type: 'GET',
         url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
         success: function(data) {
            if (Object.keys(data).length > 0) {
               $('#title')
                  .find('option')
                  .remove()
               $('#title').append($('<option>', {
                  value: '',
                  text: "Vui lòng chọn nhân viên",
                  selected: true,
               }));
               Object.keys(data).forEach(function(index) {
                  if (staffID != null && Number.parseInt(staffID) == data[index].staff.id) {
                     $('#title').append($('<option>', {
                        value: data[index].staff.id,
                        text: data[index].staff.full_name,
                        selected: true,
                     }));
                  } else if (data[index].staff) {
                     $('#title').append($('<option>', {
                        value: data[index].staff.id,
                        text: data[index].staff.full_name
                     }));
                  }

               })
            } else {
               $('#title')
                  .find('option')
                  .remove()
               $('#title').append($('<option>', {
                  value: '',
                  text: 'Chưa có'
               }));
            }
         }
      });

      if (position_id == "0") {
         $('#title')
            .find('option')
            .remove()
      }
   }
</script>
<script src="{{ URL::asset('/assets/js/jquery.tree.js') }}"></script>

@endsection