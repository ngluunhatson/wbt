<li class="">
    <div data-id="{{ $data['id'] }}">
        <div class='img'>
            <img id="avatarView" src="{{ !empty($data['staff']) ? $data['staff']['avatar'] : '/assets/chartImg/user.jpg' }}">
        </div>
        <h6 class='employeeCode'>{{ !empty($data['staff']) ? $data['staff']['full_name'] : 'Chưa có' }}</h6> <!-- Tên -->
        <span class='id'>{{ !empty($data['staff']) ? $data['staff']['employee_code'] : 'Chưa có' }}</span> <!-- Mã nv -->
        <h6 class='position'>{{ !empty($data['position']) ? $data['position']['name'] : 'Chưa có' }}</h6> <!-- Chức danh -->
        <span class='room'>{{ !empty($data['team']) ? $data['team']['name'] : 'Chưa có' }}</span> <!-- Phòng -->
        <!-- <small class='view_btn'>Thông tin</small> -->
    </div>
    @if (!empty($data['children']))
    <ul id="parent" data-parentid="{{ $data['id'] }}">
        @foreach ($data['children'] as $childCategory)
        @include('frontend.chart.tree.child', ['data' => $childCategory])
        @endforeach
    </ul>
    @endif
</li>