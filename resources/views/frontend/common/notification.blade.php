<script src="{{ asset('assets/js/default-assets/toastr.min.js') }}" defer></script>
<script>
    $(function() {
        @if(Session::has('success'))
        toastr.options = {
            "positionClass" : "toast-top-center",
            "closeButton": true,
            "progressBar": true
        }
        toastr.success("{{ session('success') }}");
        @endif

        @if(Session::has('error'))
        toastr.options = {
            "positionClass" : "toast-top-center",
            "closeButton": true,
            "progressBar": true
        }
        toastr.error("{{ session('error') }}");
        @endif
    })
</script>