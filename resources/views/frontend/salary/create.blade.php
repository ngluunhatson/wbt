@extends('layouts.fe.master2')

@section('title')
@lang('translation.starter')
@endsection

@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')

<div class="row mb-3">
    <h3 class="d-inline m-0">Tạo Bảng lương</h3>
</div>

<form id ="formInsert" action = "">
    <div class="row justify-content-between mt-3">
        <!-- Table --->
        <div style="width:15%">

            <label for="datePicker" class="form-label">Tháng / năm</label>
            <input type="text" class="form-control datepicker" name="monthYear" id="datePicker" value="{{ old('monthYear', date('m/Y', strtotime(now()) ) ) }}">




        </div>
        <a href="#" style="width:10%">
            <button type="button" id="createSalaryListButton" class="btn btn-success btn-label waves-effect waves-light">
                <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Tạo</button>
        </a>
    </div>

    <div class="row mt-3">
        <div class="card">
            <div class="row card-body justify-content-around">
                <div class="col-xl-3">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h5>Người Lập Biểu</h5>
                        </div>
                        <div class="col-12 text-center">
                            <label class="form-label mb-0 mt-4 ">{{ auth()->user()->name }}</label>
                            <input hidden name="creator_user_id" value="{{ auth() -> user() -> id }}" />
                        </div>
                      
                    </div>
                </div>

                <div class="col-xl-3">
                    <div class="row ">
                        <div class="col-12 text-center">
                            <h5>TP. Nhân sự</h5>
                        </div>
                        <input hidden name="type_confirms[]" value="9">
                        <div class="col-12">
                            <select name="select_confirms_basic_info_ids[]" class="form-select text-center mt-3" aria-label="Default select example">
                                @if(!empty($dataInit['hr_room_leaders']))
                                @foreach($dataInit['hr_room_leaders'] as $value)
                                <option value="{{ $value->id }}">{{ $value->full_name}}
                                </option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3">
                    <div class="row ">
                        <div class="col-12 text-center">
                            <h5>TP. Tài chính - Kế toán</h5>
                        </div>
                        <input hidden name="type_confirms[]" value="10">
                        <div class="col-12">
                            <select name="select_confirms_basic_info_ids[]" class="form-select text-center mt-3" aria-label="Default select example">
                                @if(!empty($dataInit['finance_room_leaders']))
                                @foreach($dataInit['finance_room_leaders'] as $value)
                                <option value="{{ $value->id }}">{{ $value->full_name}}
                                </option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="row ">
                        <div class="col-12 text-center">
                            <h5>Ban Giám Đốc</h5>
                        </div>
                        <input hidden name="type_confirms[]" value="3">
                        <div class="col-12">
                            <select name="select_confirms_basic_info_ids[]" class="form-select text-center mt-3" aria-label="Default select example">
                                @if(!empty($dataInit['ceo']))
                                @foreach($dataInit['ceo'] as $value)
                                @if(!empty($value['basic_info_id']))
                                <option value="{{ $value->staff->id }}">{{ $value->staff->full_name}}
                                </option>
                                @endif
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script>
    $(document).ready(function() {

        $('#datePicker').datepicker({
            format: 'mm/yyyy',
            startView: "months",
            minViewMode: "months",
        });

        $('#createSalaryListButton').on('click', function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                method: "POST",
                url: "{{ route('salary.createSalaryList') }}",
                data: $('#formInsert').serialize(),
                success: response => {
                    if (response.success) {
                        swal.fire({
                            icon: 'success',
                            title: `${response.message}`,
                            confirmButtonColor: '#3085d6',
                            showConfirmButton: true,
                            confirmButtonText: 'Xác Nhận',
                        }).then(r => {
                            window.location.href = response.link;
                        });
                    } else {
                        swal.fire({
                            icon: 'error',
                            title: `${response.message}`,
                            confirmButtonColor: '#3085d6',
                            showConfirmButton: false,
                        });
                    }
                }
            });
        });
    });
</script>

@endsection
