@php $salaryList = null; @endphp

@php   
    $totalRowInfo = [];
    for ($j = 1; $j < 55; $j++){ 
        $totalRowInfo[$j] = 0;
    }
@endphp
<div class = "row">
    <div class = "card">
        <div class = "card-body overflow-auto" id = "salaryTableCardBody">
            <div class = 'mb-3'>
                <table id="salary" class="table table-bordered dt-responsive nowrap table-striped align-middle">
                    <thead>
                        <tr>
                            <th rowspan="3" colspan="1">STT</th>
                            <th rowspan="3" colspan="1">Mã NV</th>
                            <th rowspan="3" colspan="1">Họ tên</th>
                            <th rowspan="3" colspan="1">Phòng</th>
                            <th rowspan="3" colspan="1">Cấp bậc</th>
                            <th rowspan="3" colspan="1">Chức danh</th>
                            <th rowspan="3" colspan="1">Ngày vào làm<br>(nội bộ)</th>
                            <th rowspan="3" colspan="1">Ngày vào làm<br>(ký HĐLĐ)</th>
                            <th rowspan="3" colspan="1">Hợp đồng</th>
                            <th rowspan="3" colspan="1">TỔNG<br>GROSS<BR>THOẢ<br>THUẬN</th>
                            <th rowspan="3" colspan="1">Hỗ trợ khác<br>(không khai thuế)</th>
                            <th rowspan="2" colspan="1">Ngày<br>công</th>
                            <th colspan="6" colspan="1">Thu nhập</th>
                            <th colspan="5" colspan="1">Chi tiết phân bổ</th>
                            <th rowspan="2" colspan="1">Ngày công</th>
                            <th colspan="4" colspan="1">Chịu thuế</th>
                            <th colspan="2" colspan="1">Không chịu thuế</th>
                            <th rowspan="3" colspan="1">Lương<br>ngoài giờ</th>
                            <th rowspan="3" colspan="1">Điều<br>chỉnh<br>bổ<br>sung</th>
                            <th rowspan="3" colspan="1">Điều<br>chỉnh<br>giảm trừ</th>
                            <th rowspan="3" colspan="1">Tổng thu nhập</th>
                            <th rowspan="3" colspan="1">Tổng thu nhập<br>chịu thuế</th>
                            <th rowspan="1" colspan="7">Các khoản giảm trừ</th>
                            <th rowspan="1" colspan="5">BHBB (Công ty)</th>
                            <th rowspan="3" colspan="1">Thu nhập<br>tính thuế</th>
                            <th rowspan="2" colspan="4">Thuế TNCN</th>
                            <th rowspan="3" colspan="1">Các<br>khoản<br>thu hồi<br>khác</th>
                            <th rowspan="3" colspan="1">Các<br>khoản<br>được<br>hoàn<br>khác</th>
                            <th rowspan="1" colspan="3">Thực lãnh</th>
                            <th rowspan="1" colspan="2">Thanh toán lần 01</th>
                            <th rowspan="1" colspan="2">Thanh toán lần 02</th>
                            <th rowspan="1" colspan="2">Còn lại</th>
                            <th rowspan="3" colspan="1">Ghi chú</th>
                        </tr>
                        <tr>
                            <th rowspan="2" colspan="1">Hỗ trợ khác</th>
                            <th rowspan="2" colspan="1">Lương<br>KPIs<br>(xét quý)</th>
                            <th rowspan="2" colspan="1">Lương ngoài giờ</th>
                            <th rowspan="2" colspan="1">Điều<br>chỉnh bổ<br> sung</th>
                            <th rowspan="2" colspan="1">Điều<br>chỉnh<br>giảm trừ</th>
                            <th rowspan="2" colspan="1">Tổng cộng</th>
                            <th rowspan="2" colspan="1">Mức lương <br>chính</th>
                            <th rowspan="2" colspan="1">Hỗ trợ<br>tiền ăn</th>
                            <th rowspan="2" colspan="1">Hỗ trợ<br>đồng<br>phục</th>
                            <th rowspan="2" colspan="1">Hiệu quả<br>công việc</th>
                            <th rowspan="2" colspan="1">Tổng</th>
                            <th rowspan="2" colspan="1">Mức lương<br>chính</th>
                            <th rowspan="2" colspan="1">Tiền ăn</th>
                            <th rowspan="2" colspan="1">Đồng phục</th>
                            <th rowspan="2" colspan="1">Hiệu quả<br>công việc</th>
                            <th rowspan="2" colspan="1">Tiền ăn</th>
                            <th rowspan="2" colspan="1">Đồng phục<br>(tối đa <br>5tr/người<br>năm)</th>
                            <th rowspan="1" colspan="1">Giảm trừ<br>bản thân</th>
                            <th rowspan="1" colspan="2">NPT</th>
                            <th rowspan="1" colspan="1">BHXH</th>
                            <th rowspan="1" colspan="1">BHYT</th>
                            <th rowspan="1" colspan="1">BHTN</th>
                            <th rowspan="2" colspan="1">Tổng BHBB<br>(NV đóng)</th>
                            <th rowspan="1" colspan="1">BHXH</th>
                            <th rowspan="1" colspan="1">BHYT</th>
                            <th rowspan="1" colspan="1">BHTN</th>
                            <th rowspan="1" colspan="1">BTNLĐ-<br>BNN</th>
                            <th rowspan="2" colspan="1">Tổng<br>(Cty đóng)</th>
                            <th rowspan="2" colspan="1">TK Công ty</th>
                            <th rowspan="2" colspan="1">TK Cá nhân</th>
                            <th rowspan="2" colspan="1">Tổng</th>
                            <th rowspan="2" colspan="1">TK Công ty</th>
                            <th rowspan="2" colspan="1">TK Cá nhân</th>
                            <th rowspan="2" colspan="1">TK Công ty</th>
                            <th rowspan="2" colspan="1">TK Cá nhân</th>
                            <th rowspan="2" colspan="1">TK Công ty</th>
                            <th rowspan="2" colspan="1">TK Cá nhân</th>
                        </tr>
                        <tr>
                            <th class="text-danger">{{ $dataInit['workNumber'] }}</th>
                            <th rowspan="1" colspan="1" class="text-danger">{{ $dataInit['workNumber'] }}</th>
                            <th class = "input_column_27">11.000.000</th>
                            <th>SL</th>
                            <th class = "input_column_29">4.400.000</th>
                            <th class = "input_column_30">8%</th>
                            <th class = "input_column_31">1.5%</th>
                            <th class = "input_column_32" >1%</th>
                            <th class = "input_column_34" >17%</th>
                            <th class = "input_column_35">3%</th>
                            <th class = "input_column_36">0%</th>
                            <th class = "input_column_37">0.5%</th>
                            <th>Luỹ tiến</th>
                            <th>10%</th>
                            <th>20%</th>
                            <th>Cộng</th>
                        </tr>
                        <tr>
                            <th>A</th>
                            <th>B</th>
                            <th>C</th>
                            <th>D</th>
                            <th>E</th>
                            <th>F</th>
                            <th>G</th>
                            <th></th>
                            <th>H</th>
                            <th>(1=2+13)</th>
                            @for ($i = 2; $i < 56; $i++)<th>{{$i}}</th> @endfor
                        </tr>

                       
                    </thead>
                    <tbody>
                        @if(!empty($dataInit['dataList']))
                            @php $i = 1;  
                                $firstSalary = $dataInit['dataList'] -> first();
                                $salaryList = $firstSalary ? $firstSalary -> salaryList : null;
                            
                            
                            @endphp
                            @foreach($dataInit['dataList'] as $detail)
                                @php 
                                   $totalRowInfo[1] += $detail['total_gross'];
                                   $totalRowInfo[2] += $detail['other_support'];
                                   $totalRowInfo[3] += $detail['timekeep']['total_salary'];
                                   $totalRowInfo[4] += $detail['income']['other_support'];
                                   $totalRowInfo[5] += $detail['income']['salary_kpis'];
                                   $totalRowInfo[6] += $detail['income']['overtime_salary'];
                                   $totalRowInfo[7] += $detail['income']['additional_adjustments'];
                                   $totalRowInfo[8] += $detail['income']['adjustment_of_deductions'];
                                   $totalRowInfo[9] += $detail['income']['total'];
                                   $totalRowInfo[10] += $detail['support']['main_salary'];
                                   $totalRowInfo[11] += $detail['support']['food_support'];
                                   $totalRowInfo[12] += $detail['support']['support_uniforms'];
                                   $totalRowInfo[13] += $detail['support']['job_performance'];
                                   $totalRowInfo[14] += $detail['support']['total'];
                                   $totalRowInfo[15] += $detail['timekeep']['total_salary'];
                                   $totalRowInfo[16] += $detail['allowance']['main_salary_with_tax'];
                                   $totalRowInfo[17] += $detail['allowance']['money_for_meals_with_tax'];
                                   $totalRowInfo[18] += $detail['allowance']['uniform_with_tax'];
                                   $totalRowInfo[19] += $detail['allowance']['job_performance_with_tax'];
                                   $totalRowInfo[20] += $detail['allowance']['money_for_meals_without_tax'];
                                   $totalRowInfo[21] += $detail['allowance']['uniform_without_tax'];
                                   $totalRowInfo[22] += $detail['allowance']['overtime_salary'];
                                   $totalRowInfo[23] += $detail['allowance']['additional_adjustments'];
                                   $totalRowInfo[24] += $detail['allowance']['adjustment_of_deductions'];
                                   $totalRowInfo[25] += $detail['allowance']['total_income'];
                                   $totalRowInfo[26] += $detail['allowance']['total_taxable_income'];
                                   $totalRowInfo[27] += $detail['insurance']['reduce_yourself'];
                                   $totalRowInfo[28] += $detail['insurance']['number_of_dependents'];
                                   $totalRowInfo[29] += $detail['insurance']['dependent_person'];
                                   $totalRowInfo[30] += $detail['insurance']['social_insurance_nv'];
                                   $totalRowInfo[31] += $detail['insurance']['health_insurance_nv'];
                                   $totalRowInfo[32] += $detail['insurance']['unemployment_insurance_nv'];
                                   $totalRowInfo[33] += $detail['insurance']['total_nv'];
                                   $totalRowInfo[34] += $detail['insurance']['social_insurance_cty'];
                                   $totalRowInfo[35] += $detail['insurance']['health_insurance_cty'];
                                   $totalRowInfo[36] += $detail['insurance']['unemployment_insurance_cty'];
                                   $totalRowInfo[37] += $detail['insurance']['occupational_accident_insurance'];
                                   $totalRowInfo[38] += $detail['insurance']['total_cty'];
                                   $totalRowInfo[39] += $detail['tax']['taxable_income'];
                                   $totalRowInfo[40] += $detail['tax']['progressive'];
                                   $totalRowInfo[41] += $detail['tax']['tax_10'];
                                   $totalRowInfo[42] += $detail['tax']['tax_20'];
                                   $totalRowInfo[43] += $detail['tax']['total'];
                                   $totalRowInfo[44] += $detail['tax']['other_recoveries'];
                                   $totalRowInfo[45] += $detail['tax']['other_refunds'];
                                   $totalRowInfo[46] += $detail['pay']['company_account_1'];
                                   $totalRowInfo[47] += $detail['pay']['personal_account_1'];
                                   $totalRowInfo[48] += $detail['pay']['total'];
                                   $totalRowInfo[49] += $detail['pay']['company_account_2'];
                                   $totalRowInfo[50] += $detail['pay']['personal_account_2'];
                                   $totalRowInfo[51] += $detail['pay']['company_account_3'];
                                   $totalRowInfo[52] += $detail['pay']['personal_account_3'];
                                   $totalRowInfo[53] += $detail['pay']['company_account_4'];
                                   $totalRowInfo[54] += $detail['pay']['personal_account_4'];
                                @endphp
                                <input id="work_salary_{{$detail['id']}}" name="work_salary" value="{{ $detail['timekeep']['total_salary'] }}" hidden>
                                <input id="work_number" name="work_number" value="{{ $dataInit['workNumber'] }}" hidden>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $detail['staff']['employee_code'] }}</td>
                                    <td>{{ $detail['staff']['full_name'] }}</td>
                                    <td>{{ $detail -> staff -> organizational ?  $detail -> staff ->organizational -> room -> name : 'Chưa Có' }}</td>
                                    <td>{{ config('constants.rank')[$detail['staff']['rank']] }}</td>
                                    <td>{{ $detail -> staff -> organizational   ?  $detail -> staff -> organizational -> position -> name : 'Chưa Có'}}</td>
                                    <td>{{ $detail['staff']['date_of_entry_to_work'] }}</td>
                                    <td data-id="{{ $detail['id'] }}" data-type = "salary" data-type_contact = "{{ $detail['type_contact'] }}"
                                        data-column = "official_contract_date" data-entry_date = "{{ $detail['staff']['date_of_entry_to_work'] }}">{{
                                        $detail['official_contract_date']    
                                    }}</td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" 
                                        data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="salary" data-column="type_contact"
                                        data-official_contract_date  = "{{ $detail['official_contract_date']  }}">
                                        {{ $detail['type_contact'] != 0 ? config('constants.type_contact')[$detail['type_contact']] : '' }}
                                    </td>
                                    <td style="font-weight: 700;">{{ number_format($detail['total_gross']) }}</td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" 
                                        data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" 
                                        data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="salary" data-column="other_support">{{ number_format($detail['other_support']) }}</td>
                                    <td>{{ $detail['timekeep']['total_salary'] }}
                                    </td>
                                    <!-- income -->
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="income" data-column="other_support">
                                        {{ number_format($detail['income']['other_support']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="income" data-column="salary_kpis">
                                        {{ number_format($detail['income']['salary_kpis']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="income" data-column="overtime_salary">
                                        {{ number_format($detail['income']['overtime_salary']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="income" data-column="additional_adjustments">
                                        {{ number_format($detail['income']['additional_adjustments']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="income" data-column="adjustment_of_deductions">
                                        {{ number_format($detail['income']['adjustment_of_deductions']) }}
                                    </td>
                                    <td style="font-weight: 700;">
                                        {{ number_format($detail['income']['total']) }}
                                    </td>
                                    <!-- support -->
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="support" data-column="main_salary">
                                        {{ number_format($detail['support']['main_salary']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="support" data-column="food_support">
                                        {{ number_format($detail['support']['food_support']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="support" data-column="support_uniforms">
                                        {{ number_format($detail['support']['support_uniforms']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="support" data-column="job_performance">
                                        {{ number_format($detail['support']['job_performance'], 0) }}
                                    </td>
                                    <td>{{ number_format($detail['support']['total']) }}</td>
                                    <td>{{ $detail['timekeep']['total_salary'] }}</td>
                                    <!-- allowance -->
                                    <td>
                                        {{ number_format($detail['allowance']['main_salary_with_tax']) }}
                                    </td>
                                    <td>
                                        {{ number_format($detail['allowance']['money_for_meals_with_tax']) }}
                                    </td>
                                    <td>
                                        {{ number_format($detail['allowance']['uniform_with_tax']) }}
                                    </td>
                                    <td>
                                        {{ number_format($detail['allowance']['job_performance_with_tax']) }}
                                    </td>
                                    <td>
                                        {{ number_format($detail['allowance']['money_for_meals_without_tax']) }}
                                    </td>
                                    <td>
                                        {{ number_format($detail['allowance']['uniform_without_tax']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="allowance" data-column="overtime_salary">
                                        {{ number_format($detail['allowance']['overtime_salary']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="allowance" data-column="additional_adjustments">
                                        {{ number_format($detail['allowance']['additional_adjustments']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="allowance" data-column="adjustment_of_deductions">
                                        {{ number_format($detail['allowance']['adjustment_of_deductions']) }}
                                    </td>
                                    <td style="font-weight: 700;">
                                        {{ number_format($detail['allowance']['total_income']) }}
                                    </td>
                                    <td>
                                        {{ number_format($detail['allowance']['total_taxable_income']) }}
                                    </td>
                                    <!-- insurance -->
                                    <td>{{ number_format($detail['insurance']['reduce_yourself']) }}</td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="insurance" data-column="number_of_dependents">
                                        {{ number_format($detail['insurance']['number_of_dependents']) }}
                                    </td>
                                    <td>{{ number_format($detail['insurance']['dependent_person']) }}</td>
                                    <td>{{ number_format($detail['insurance']['social_insurance_nv']) }}</td>
                                    <td>{{ number_format($detail['insurance']['health_insurance_nv']) }}</td>
                                    <td>{{ number_format($detail['insurance']['unemployment_insurance_nv']) }}</td>
                                    <td style="font-weight: 700;">{{ number_format($detail['insurance']['total_nv']) }}</td>
                                    <td>{{ number_format($detail['insurance']['social_insurance_cty']) }}</td>
                                    <td>{{ number_format($detail['insurance']['health_insurance_cty']) }}</td>
                                    <td>{{ number_format($detail['insurance']['unemployment_insurance_cty']) }}</td>
                                    <td>{{ number_format($detail['insurance']['occupational_accident_insurance']) }}</td>
                                    <td style="font-weight: 700;">{{ number_format($detail['insurance']['total_cty']) }}</td>
                                    <!-- tax -->
                                    <td>{{ number_format($detail['tax']['taxable_income']) }}</td>
                                    <td>{{ number_format($detail['tax']['progressive']) }}</td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="tax" data-column="tax_10">
                                        {{ number_format($detail['tax']['tax_10']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="tax" data-column="tax_20">
                                        {{ number_format($detail['tax']['tax_20']) }}
                                    </td>
                                    <td>{{ number_format($detail['tax']['total']) }}</td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="tax" data-column="other_recoveries">
                                        {{ number_format($detail['tax']['other_recoveries']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="tax" data-column="other_refunds">
                                        {{ number_format($detail['tax']['other_refunds']) }}
                                    </td>
                                    <!-- pay -->
                                    <td>{{ number_format($detail['pay']['company_account_1']) }}</td>
                                    <td>{{ number_format($detail['pay']['personal_account_1']) }}</td>
                                    <td style="color: red;font-weight: 700;">{{ number_format($detail['pay']['total']) }}</td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="pay" data-column="company_account_2">
                                        {{ number_format($detail['pay']['company_account_2']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="pay" data-column="personal_account_2">
                                        {{ number_format($detail['pay']['personal_account_2']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="pay" data-column="company_account_3">
                                        {{ number_format($detail['pay']['company_account_3']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="pay" data-column="personal_account_3">
                                        {{ number_format($detail['pay']['personal_account_3']) }}
                                    </td>
                                    <td style="font-weight: 700;">
                                        {{ number_format($detail['pay']['company_account_4']) }}
                                    </td>
                                    <td style="font-weight: 700;">
                                        {{ number_format($detail['pay']['personal_account_4']) }}
                                    </td>
                                    <td data-id="{{ $detail['id'] }}" data-income="{{ $detail['income']['id'] }}" data-support="{{ $detail['support']['id'] }}" data-allowance="{{ $detail['allowance']['id'] }}" data-insurance="{{ $detail['insurance']['id'] }}" data-tax="{{ $detail['tax']['id'] }}" data-pay="{{ $detail['pay']['id'] }}" data-type="salary" data-column="note">
                                        {{ $detail['note'] }}
                                    </td>
                                </tr>
                                @php $i ++;@endphp
                            @endforeach
                            <tr>
                                <td style = "display:none">{{ $i + 1 }}</td> 
                                <td style = "font-weight:700;" colspan = "9">
                                    Tổng Cộng
                                </td>
                                @for ($j = 1; $j <= 9; $j++)<td style = "display:none"></td>@endfor
                                @for ($j = 1; $j < 55; $j++)<td><input readonly class = "form-control text-center" 
                                    style = "@if(in_array($j, [1, 9, 25, 33, 38, 48, 53, 54])) font-weight:700; @endif  
                                        @if($j == 48) color:red; @endif background:none; border:0cm;" value="{{ number_format($totalRowInfo[$j]) }}"/></td> @endfor
                                <td></td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@if($salaryList)
    @php 
        $firstSalary = null;
        $salaryList = null;
        if (array_key_exists('dataList', $dataInit)) {
            $firstSalary =  $dataInit['dataList'] -> first();
            if ($firstSalary)
                $salaryList = $firstSalary -> salaryList;
        }
        $canUpdate = auth() -> user() -> hasAnyPermission(['salary-update']) && !in_array($salaryList->status, [2,3,4,5]);
    @endphp
    @php
        $hr_room_lead_basic_info = null;
        $hr_room_lead_confirm = null;
        $finance_room_lead_basic_info = null;
        $finance_room_lead_confirm = null;
        $ceo_basic_info = null;
        $ceo_confirm = null;

        $salaryListConfirms = $salaryList -> salaryConfirms;

        foreach ($salaryListConfirms as $salaryListConfirm) {
        $confirmX = $salaryListConfirm -> confirm;
        switch ($confirmX -> type_confirm) {
        case 9:
        $hr_room_lead_basic_info = $confirmX -> staff;
        $hr_room_lead_confirm = $confirmX;
        break;
        case 10:
        $finance_room_lead_basic_info = $confirmX -> staff;
        $finance_room_lead_confirm = $confirmX;
        break;
        case 3:
        $ceo_basic_info = $confirmX -> staff;
        $ceo_confirm = $confirmX;
        break;
        }
        }
    @endphp

    @php
        $next_confirm_basic_info_id = null;
        $confirm_id_to_update_status = null;

        switch ($salaryList -> status ) {
        case(1):
            $next_confirm_basic_info_id = $hr_room_lead_basic_info -> id;
            break;
        case(2):
            $next_confirm_basic_info_id  = $finance_room_lead_basic_info -> id;
            $confirm_id_to_update_status = $hr_room_lead_confirm -> id;
            break;

        case(3):
            $next_confirm_basic_info_id  = $ceo_basic_info -> id;
            $confirm_id_to_update_status = $finance_room_lead_confirm -> id;
            break;
        case(4):
            $confirm_id_to_update_status = $ceo_confirm -> id;
            break;

        }
    @endphp
    <div class="row" > 
        <div class="card">
            <div class="col-12 d-flex mt-2 justify-content-end" style="padding-right: 2%;">
                @if ($salaryList -> review_date != null)
                @php
                $date = \Carbon\Carbon::parse($salaryList -> review_date);
                @endphp
                <label class=" form-label mb-0 me-5 " style="margin-top: 2px">
                    Ngày duyệt : Ngày {{ $date->day }} Tháng {{ $date->month }} Năm {{ $date->year }}</label>
                @endif
            </div>
            <div class="row card-body justify-content-around">
                <div class="col-xl-2">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h4 class="form-label text-center">Người lập biểu</h4>
                        </div>
                        <div class="col-12 text-center">
                            <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ $salaryList -> user -> name }}</label>
                        </div>
                        @if(in_array($salaryList -> status,[2,3,4,5]) )
                        <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
                        @else
                        <h6 class="form-label text-center fst-italic mt-2">Chưa xác nhận</h6>
                        @endif

                    </div>

                    @if(auth()->user() -> id == $salaryList -> user -> id && !in_array($salaryList->status, [2,3,4,5]))
                    <div class="row justify-content-center mb-3 mt-3">
                        <button id ="creator_submit_button" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content">
                        <i class="ri-check-double-line label-icon align-middle fs-16 me-2"></i> Xác Nhận</button>
                    </div>
                    @endif
                </div>
                <div class="col-xl-2">
                    <h4 class="form-label text-center">TP. Nhân sự</h4>
                    <div class="col-x1-6 text-center">
                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ ($hr_room_lead_basic_info) ? $hr_room_lead_basic_info -> full_name : "Không Có" }}</label>
                    </div>
                    @if($hr_room_lead_confirm)
                        @switch($hr_room_lead_confirm -> status)
                        @case(0)
                            <h6 class="form-label text-center fst-italic mt-2">Chưa xác nhận</h6>
                        @break
                        @case(1)
                            <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
                        @break
                        @case(2)
                            <h6 class="form-label text-center fst-italic mt-2">Từ Chối</h6>
                            <div class="col-12 text-center">
                                <label>Lý do : {{ $hr_room_lead_confirm -> note }}</label>
                            </div>
                        @break
                        @endswitch

                     

                    @else
                        <h6 class="form-label text-center fst-italic mt-2">Không Cần xác nhận</h6>
                    @endif
                </div>
                <div class="col-xl-2">
                    <h4 class="form-label text-center">TP. Tài chính - Kế toán</h4>
                    <div class="col-x1-6 text-center">
                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ ($finance_room_lead_basic_info) ? $finance_room_lead_basic_info -> full_name : "Chưa Có" }}</label>
                    </div>
                    @if($finance_room_lead_confirm)
                        @switch($finance_room_lead_confirm -> status)
                        @case(0)
                            <h6 class="form-label text-center fst-italic mt-2">Chưa xác nhận</h6>
                        @break
                        @case(1)
                            <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
                        @break
                        @case(2)
                            <h6 class="form-label text-center fst-italic mt-2">Từ Chối</h6>
                            <div class="col-12 text-center">
                                <label>Lý do : {{ $finance_room_lead_confirm -> note }}</label>
                            </div>
                        @break
                        @endswitch

                     
                    @else
                    <h6 class="form-label text-center fst-italic mt-2">Không Cần xác nhận</h6>
                    @endif
                </div>
                <div class="col-xl-2">
                    <h4 class="form-label text-center">Ban Giám Đốc</h4>
                    <div class="col-x1-6 text-center">
                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ ($ceo_basic_info) ? $ceo_basic_info -> full_name : "Chưa Có" }}</label>
                    </div>
                    @if($ceo_confirm)
                        @switch($ceo_confirm -> status)
                        @case(0)
                            <h6 class="form-label text-center fst-italic mt-2">Chưa xác nhận</h6>
                        @break
                        @case(1)
                            <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
                        @break
                        @case(2)
                            <h6 class="form-label text-center fst-italic mt-2">Từ Chối</h6>
                            <div class="col-12 text-center">
                                <label>Lý do : {{ $ceo_confirm -> note }}</label>
                            </div>
                        @break
                        @endswitch

                      
                    @else
                        <h6 class="form-label text-center fst-italic mt-2">Không Cần xác nhận</h6>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    <form id ="formConfirm" action ="">
        <input hidden name="next_confirm_basic_info_id" value="{{ $next_confirm_basic_info_id }}">
        <input hidden name="salary_list_id" value="{{ $salaryList -> id }}">
        @if(auth()->user() -> basic_info_id == $hr_room_lead_basic_info -> id && $salaryList -> status == 2 ||
            auth()->user() -> basic_info_id == $finance_room_lead_basic_info -> id && $salaryList -> status == 3 ||
            auth()->user() -> basic_info_id == $ceo_basic_info -> id && $salaryList -> status == 4)
            <input hidden name="confirm_id_to_update_status" value="{{ $confirm_id_to_update_status }}">
            <input hidden name="reason" id="reasonConfirm">
            <input hidden name="confirm_flag" id="confirm_flag" value="1">
            <div class="row justify-content-center mb-3 mt-3">
                <button id ="btnAccept" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content">
                <i class="ri-check-double-line label-icon align-middle fs-16 me-2"></i> Duyệt</button>
                <button id="btnDeny" class="btn btn-danger waves-effect waves-light" style="width: fit-content; margin-left:2%">
                <i class="ri-error-warning-line label-icon align-middle rounded-pill fs-16 me-2 "></i> Không Duyệt</button>
            </div>

        @endif
    </form>
@endif



@push('script-salary-detail')

<script>

</script>

@endpush