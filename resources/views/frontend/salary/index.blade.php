@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<style>
    table th {
        white-space: nowrap;
        text-align: center;
        vertical-align: middle;
    }

    td {
        text-align: center;
    }

    td input {
        width: 135px !important;
    }

    .my-confirm-class {
        padding: 3px 6px;
        font-size: 12px;
        color: white;
        text-align: center;
        vertical-align: middle;
        border-radius: 4px;
        background-color: #337ab7;
        text-decoration: none;
    }

    .my-cancel-class {
        padding: 3px 6px;
        font-size: 12px;
        color: white;
        text-align: center;
        vertical-align: middle;
        border-radius: 4px;
        background-color: #a94442;
        text-decoration: none;
    }
</style>

<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h3 class="d-inline m-0">Bảng lương</h3>

        @can('salary-create')
        <a href="{{ route('salary.create') }}">
            <button type="button" class="btn btn-success btn-label waves-effect waves-light">
                    <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Tạo</button>
        </a>
        @endcan
    </div>
</div>

<div class="row">
    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="row gy-3">
                    <div class="col-xl-3">
                        <label for="borderInput" class="form-label">Phòng</label>
                        <select name="room_id" id="selectRoom" class="form-select" aria-label="Default select example">
                            <option value="-1">Tất Cả Các Phòng</option>
                            @foreach($dataInit['rooms'] as $value)
                            <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xl-3">
                        <label for="datePicker" class="form-label">Tháng / năm</label>
                        <input type="text" class="form-control" name="monthYear" id="datePicker" value="{{ old('monthYear', date('m/Y', strtotime(now()) ) ) }}">
                    </div>
                    <div class="col-xl-2">
                        <label for="borderInput" class="form-label">Từ ngày:</label>
                        <input type="text" class="form-control border-dashed" id="startDate" value="{{ $dataInit['startDate']->format('d/m/Y') }}" readonly>
                    </div>
                    <div class="col-xl-2">
                        <label for="borderInput" class="form-label">Đến ngày:</label>
                        <input type="text" class="form-control border-dashed" id="endDate" value="{{ $dataInit['endDate']->format('d/m/Y') }}" readonly>
                    </div>
                    <div class="col-xl-2">
                        <label for="borderInput" class="form-label">Công chuẩn:</label>
                        <input type="text" class="form-control border-dashed" id="workNumber" value="{{ $dataInit['workNumber'] }}" readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class = "row" id = "salarySettingRow">
    <div class="row">
        <div class = "card" >
            <div class="card-header">
                <div class ="row justify-content-between">
                    <div style = "width:15%">
                        <h5>Cài đặt bảng lương</h5>
                    </div>
                    <div style = "width:10%">
                        <button id = "saveSalarySettingButton" type="button" class="btn btn-info btn-label waves-effect waves-light">
                                <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Lưu Cài Đặt</button>
                    </div>
                </div>
            </div>
            <form id = "formSaveSalarySetting">
                @csrf
                <input hidden name = "salary_setting_id" id = "salary_setting_id_input"> 
                <input hidden name = "salary_list_id" id = "salary_list_id_input"> 
                <div class="card-body">
                    <div class = "row">
                        <div style = "width:5%"></div>
                        <div style = "width:13%">
                            <label>27 - Giảm trừ bản thân (VND)</label>
                            <input hidden id = "input_column_27-id" name = "input_column_27"/>

                            <input class = "form-control salary_setting input_column_27 clearable_input money_input" type = "text"  data-input_id = "input_column_27-id"/>
                        </div>
                        <div style = "width:2%"></div>
                        <div style = "width:13%">
                            <label>29 - NPT (VND/SL)</label>
                            <input hidden id = "input_column_29-id" name = "input_column_29"/>
                            <input class = "form-control salary_setting input_column_29 clearable_input money_input" type = "text"  data-input_id = "input_column_29-id" />
                        </div>
                        <div style = "width:2%"></div>
                        <div style = "width:13%">
                            <label>30 - BHXH (%)</label>
                            <input hidden id = "input_column_30-id" name = "input_column_30"/>
                            <input class = "form-control salary_setting percentage-input input_column_30 clearable_input" type = "number"  data-input_id = "input_column_30-id"/>
                        </div>
                        <div style = "width:2%"></div>
                        <div style = "width:13%">
                            <label>31 - BHYT (%)</label>
                            <input hidden id = "input_column_31-id" name = "input_column_31"/>
                            <input class = "form-control salary_setting percentage-input input_column_31 clearable_input" type = "number"  data-input_id = "input_column_31-id"/>
                        </div>
                        <div style = "width:2%"></div>
                        <div style = "width:13%">
                            <label>32 - BHTN (%)</label>
                            <input hidden id = "input_column_32-id" name = "input_column_32"/>
                            <input class = "form-control salary_setting percentage-input input_column_32 clearable_input" type = "number" data-input_id = "input_column_32-id"/>
                        </div>
                        
                        <div style = "width:5%"></div>
                    </div>
                    <div class = "row mt-3">
                        <div style = "width:5%"></div>
                        <div style = "width:13%">
                            <label>34 - BHXH (%)</label>
                            <input hidden id = "input_column_34-id" name = "input_column_34"/>
                            <input class = "form-control salary_setting percentage-input input_column_34 clearable_input" type = "number" data-input_id = "input_column_34-id"/>
                        </div>
                        <div style = "width:2%"></div>
                        <div style = "width:13%">
                            <label>35 - BHYT (%)</label>
                            <input hidden id = "input_column_35-id" name = "input_column_35"/>
                            <input class = "form-control salary_setting percentage-input input_column_35 clearable_input" type = "number"  data-input_id = "input_column_35-id"/>
                        </div>
                        <div style = "width:2%"></div>
                        <div style = "width:13%">
                            <label>36 - BHTN (%)</label>
                            <input hidden id = "input_column_36-id" name = "input_column_36"/>
                            <input class = "form-control salary_setting percentage-input input_column_36 clearable_input" type = "number" data-input_id = "input_column_36-id"/>
                        </div>
                        <div style = "width:2%"></div>
                        <div style = "width:13%">
                            <label>37 - BHTNLĐ-BNN (%)</label>
                            <input hidden id = "input_column_37-id" name = "input_column_37"/>
                            <input class = "form-control salary_setting percentage-input input_column_37 clearable_input" type = "number" data-input_id = "input_column_37-id"/>
                        </div>
                        <div style = "width:2%"></div>
                        <div style = "width:13%">
                            <label>Lương cở sở (VND)</label>
                            <input hidden id = "salary_base-id" name = "salary_base"/>
                            <input class = "form-control salary_setting salary_positive salary_base clearable_input money_input" type = "text" style = "color:red" data-input_id = "salary_base-id"/>
                        </div>
                        <div style = "width:2%"></div>
                        <div style = "width:13%">
                            <label>Lương tối thiểu (VND)</label>
                            <input hidden id = "salary_min-id" name = "salary_min"/>
                            <input class = "form-control salary_setting salary_positive salary_min clearable_input money_input" type = "text" style = "color:red" data-input_id = "salary_min-id" />
                        </div>
                        <div style = "width:5%"></div>
                    </div>
                </div>
            </form>
        </div>
    </div> 
</div>

<div class="row contentTable">
    @include('frontend.salary.detail', ['dataInit' => $dataInit])
</div>

@endsection
@section('script')
<script src="{{ asset('assets/js/default-assets/cleave.min.js') }}" charset="UTF-8"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>

<script>
</script>

<script>
    var lstValue = {!!json_encode(config('constants.type_contact')) !!};
    var lstColumn = [7, 8, 10, 13, 14, 15, 16, 18, 19, 20, 21, 30, 31, 32, 36, 49, 50, 52, 53, 57, 58, 59, 60, 63];

    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        loadSalaryList();

        $('#datePicker').datepicker({
            format: 'mm/yyyy',
            startView: "months",
            minViewMode: "months",
        });
        $('.input-num').toArray().forEach(function(field) {
            new Cleave(field, {
                numeral: true,
                numeralPositiveOnly: true,
                numeralThousandsGroupStyle: 'thousand'
            })
        });

        $('#selectRoom, #datePicker').on('change', function() {
            loadSalaryList();
        });


        const documentMoneyInputFocus = $(document).on('focusin', '.salary_setting', function() {
            $(this).data('old_value', parseFloat($(this).val().replaceAll(',', '')));
        });
        
        documentMoneyInputFocus.on('change', '.money_input', function() {
            const input_id = '#'+$(this).data('input_id');
            const format_str_with_commas = s => {
                return parseFloat(s).toLocaleString('EN-US'); 
            };
            const remove_str_commas = s => {
                return s.replaceAll(',', '');
            }

            const temp_input_str = remove_str_commas($(this).val());

            const parse_float_temp_str = parseFloat(temp_input_str);
            var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;

            if (parse_float_temp_str != temp_input_str || ( $(this).hasClass('salary_positive') && parse_float_temp_str < 0 )) {
                $(this).val(format_str_with_commas($(this).data('old_value').toString())); 
                $(input_id).val($(this).data('old_value'));
            } else {
                $(input_id).val( parse_float_temp_str );
                $(this).val(format_str_with_commas(temp_input_str));
            }
        });

        
        documentMoneyInputFocus.on('change', '.percentage-input', function(){
            const temp_val =  $(this).val() ;
            const input_id = '#'+$(this).data('input_id');
            if (temp_val < -100 || temp_val > 100) {
                $(this).val($(this).data('old_value'));
                $(input_id).val(parseFloat($(this).data('old_value')));
            } else {
                $(input_id).val(temp_val);
            }
        });

      

  
        function settingEditTable(lstValue, canUpdate) {
            var lstOption = [];
            var lstInput = [];

            Object.keys(lstValue).forEach((index) => {
                lstOption.push({
                    "value": index,
                    "display": lstValue[index]
                })
            })

            lstColumn.forEach((i) => {
                if (i == 7) {
                    lstInput.push({
                        "column": 7,
                        "type": "datepicker",
                        "options": {
                            "format"    : "yyyy-mm-dd",
                        }
                    });
                } else if (i == 8) {
                    lstInput.push({
                        "column": 8,
                        "id": "selectContact",
                        "type": "list-salary",
                        "options": lstOption
                    });
                } else if (i == 63) {
                    lstInput.push({
                        "column": i,
                        "type": "textarea",
                    })
                } else {
                    lstInput.push({
                        "column": i,
                        "type": "number",
                    });
                }
            })

            totalRowInfo = [];
            var table = $('#salary').DataTable({
                responsive: false,
                columnDefs: [{
                    orderable: false,
                    targets: lstColumn
                }],
            });


            function myCallbackFunction(updatedCell, updatedRow, newValue, oldValue) {
                var id = $(updatedCell.node()).data('id');
                var income = $(updatedCell.node()).data('income');
                var support = $(updatedCell.node()).data('support');
                var allowance = $(updatedCell.node()).data('allowance');
                var insurance = $(updatedCell.node()).data('insurance');
                var tax = $(updatedCell.node()).data('tax');
                var pay = $(updatedCell.node()).data('pay');
                var type = $(updatedCell.node()).data('type');
                var column = $(updatedCell.node()).data('column');
                var workNumber = $('#work_number').val();
                var workSalary = $('#work_salary_'+id).val();

                var staff_entry_date = $(updatedCell.node()).data('entry_date');
                var staff_official_contract_date = $(updatedCell.node()).data('official_contract_date');

                if (typeof staff_entry_date !== 'undefined') {
                    if (new Date(staff_entry_date) > new Date(newValue))
                        swal.fire({
                            icon: 'error',
                            title: `Ngày ký HĐLĐ phải sau ngày vào làm!`,
                            showConfirmButton: false,
                        });
                    else {
                        var type_contact = $(updatedCell.node()).data('type_contact');
                        if (newValue === '' && typeof type_contact !== 'undefined' && (type_contact == 1 || type_contact == 3))
                            swal.fire({
                                icon: 'error',
                                title: `Cần Ngày ký HĐLĐ nếu là hợp đồng CT hoặc HT!`,
                                showConfirmButton: false,
                            });
                        else
                            savData(id, income, support, allowance, insurance, tax, pay, type, column, newValue, workNumber, workSalary)
                    }
                } else {
                    if ( typeof staff_official_contract_date !== 'undefined' && staff_official_contract_date === '' && (newValue == 1 || newValue == 3))
                        swal.fire({
                            icon: 'error',
                            title: `Cần Ngày ký HĐLĐ nếu là hợp đồng CT hoặc HT!`,
                            showConfirmButton: false,
                        });
                    else
                        savData(id, income, support, allowance, insurance, tax, pay, type, column, newValue, workNumber, workSalary)
                }

                loadSalaryList();
            }

            
            if(canUpdate) {
                table.MakeCellsEditable({
                    "onUpdate": myCallbackFunction,
                    "type": 'salary',
                    "columns": lstColumn,
                    "inputCss": "form-control mb-1",
                    "inputTypes": lstInput,
                    "confirmationButton": {
                        "confirmCss": 'my-confirm-class',
                        "cancelCss": 'my-cancel-class'
                    },
                });
            }

        }

        function savData(salary_id, income_id, support_id, allowance_id, insurance_id, tax_id, pay_id,
            type, column, newValue, workNumber, workSalary) {
        
            $.ajax({
                type: "POST",
                url: "{{ route('salary.updateData') }}",
                data: {
                    salary_id,
                    income_id,
                    support_id,
                    allowance_id,
                    insurance_id,
                    tax_id,
                    pay_id,
                    type,
                    column,
                    newValue,
                    workNumber,
                    workSalary
                },
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                    } else {
                        toastr.error(response.message);
                    }

                },
            });
        }

        function loadSalaryList() {
            $.ajax({
                type: 'POST',
                url: "{{ route('salary.loadData') }}",
                data: {
                    room_id: $('#selectRoom').find(":selected").val(),
                    monthYear: $('#datePicker').val(),
                    basic_info_id: "{{ auth() -> user() -> basic_info_id }}"
                },
                success: function(data) {
                    const old_scroll_position = $('#salaryTableCardBody').scrollLeft();
                    const old_page_number = $('#salary').DataTable().page.info().page;

                    $('.contentTable').empty();
                    $('.contentTable').append(data.html);

                    $('#startDate').val(data.startDate);
                    $('#endDate').val(data.endDate);
                    $('#workNumber').val(data.workNumber);
                    $('#salary_list_id_input').val(data.salaryListId);

        
                    if (data.salarySetting) {
                        populateSalarySetting(data.salarySetting)
                    } else {
                        populateSalarySetting(null);
                    }

                    if (data.salaryStatus) {
                        settingEditTable(lstValue, data.canUpdate);
                        setUpButtonWithSalaryListStatus(data.salaryStatus);

                        $('#salarySettingRow').show();
                        Object.keys($('.salary_setting')).slice(0, -2).forEach(key => {
                            $('.salary_setting')[key].disabled = !data.canUpdate;
                        });


                        $('#saveSalarySettingButton')[0].hidden = !data.canUpdate;

                        $('#salary').DataTable().page(old_page_number).draw(false);

                        if(data.canUpdate) {
                            $('#saveSalarySettingButton').one('click', function(e) {
                                e.preventDefault();
                                $.ajax({
                                    'method' : 'POST',
                                    'data'  : $('#formSaveSalarySetting').serialize(),
                                    'url'   : "{{ route('salary.saveSalarySetting') }}",
                                    success: response => {
                                    if (response.success) {
                                            swal.fire({
                                                icon: 'success',
                                                title: `${response.message}`,
                                                confirmButtonColor: '#3085d6',
                                                showConfirmButton: true,
                                                confirmButtonText: 'Xác Nhận',
                                            }).then(r => {
                                                loadSalaryList();
                                            });
                                    } else 
                                            swal.fire({
                                                icon: 'error',
                                                title: `${response.message}`,
                                                confirmButtonColor: '#3085d6',
                                                showConfirmButton: false,
                                            });
                                    }
                                });
                            });
                        } else 
                            $('#saveSalarySettingButton').off();

                        $('#salaryTableCardBody').scrollLeft(old_scroll_position);

                    } else {
                        $('#salarySettingRow').hide();
                    }

                },
            });
        }

        function populateSalarySetting(salarySetting) {
            if (salarySetting) {
                Object.keys(salarySetting).forEach(key => {
                    $('#'+key+'-id').val(salarySetting[key]);
                    $('.'+key).val(salarySetting[key] ? salarySetting[key].toLocaleString('EN-US') : null);
                    textKey = salarySetting[key];
                    if (key == 'input_column_30'|| key == 'input_column_31'|| key == 'input_column_32'|| 
                        key == 'input_column_34'|| key == 'input_column_35'|| key == 'input_column_36'|| key == 'input_column_37' )
                    textKey += '%';
                    $('.'+key).text(textKey);

                });
                $('#salary_setting_id_input').val(salarySetting.id);
            }

            else {
                $('.clearable_input').val(null);
                $('#salary_setting_id_input').val(null);

            }
        }

        function setUpButtonWithSalaryListStatus(status) {
            if (status) {
                if(![2,3,4,5].includes(status)) {
                    $('#creator_submit_button').on('click', function(event) {
                        event.preventDefault();
                        swal.fire({
                            title: 'Bạn có muốn xác nhận?',
                            text: 'Sau khi xác nhận bạn sẽ không chỉnh sửa được, nếu muốn chỉnh sửa vui lòng liên hệ admin',
                            icon: 'warning',
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Đồng ý',
                            showDenyButton: true,
                            denyButtonText: 'Hủy',
                        
                        }).then(r => {
                            if (r.isConfirmed) {
                                Swal.fire({
                                    title: 'Bạn chắc chắn muốn xác nhận?',
                                    html: 'Sau khi xác nhận sẽ <b> không chỉnh sửa được</b>, nếu muốn chỉnh sửa vui lòng liên hệ admin',
                                    icon: 'warning',
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Đồng ý',
                                    showDenyButton: true,
                                    denyButtonText: 'Hủy',
                                }).then(r2 => {
                                    if (r2.isConfirmed) {
                                        sendAjaxConfirm();
                                    }
                                });
                            }
                        });
                    });
                } else {
                    $('#btnAccept').on('click', function(event) {
                        event.preventDefault();
                        swal.fire({
                            title: 'Bạn có muốn xác nhận?',
                            text: 'Sau khi xác nhận bạn sẽ không chỉnh sửa được, nếu muốn chỉnh sửa vui lòng liên hệ admin',
                            icon: 'warning',
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Đồng ý',
                            showDenyButton: true,
                            denyButtonText: 'Hủy',
                        
                        }).then(r => {
                            if (r.isConfirmed) { 
                                sendAjaxConfirm();
                            }
                        });
                        
                    });
                    $('#btnDeny').on('click', function(event) {
                        event.preventDefault();
                        swal.fire({
                            title: 'Lý do không duyệt đơn',
                            html: `<input class="form-control" type="text" id="reason"  placeholder="Lý do" required>`,
                            icon: 'warning',
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Đồng ý',
                            showDenyButton: true,
                            denyButtonText: 'Hủy',
                            preConfirm: () => {
                                const reason = Swal.getPopup().querySelector('#reason').value
                                if (!reason) {
                                Swal.showValidationMessage(`Vui lòng nhập lý do`)
                                }
                                return {
                                    reason: reason,
                                }
                            }
                            })
                            .then((result) => {
                                if (result.isConfirmed) {
                                    $('#reasonConfirm').val(result.value.reason);
                                    $('#confirm_flag').val(2);
                                    sendAjaxConfirm();
                                } else if (result.isDenied) {
                                    Swal.fire('Bảng lương không được duyệt', '', 'info')
                                }
                        });
                    });
                }
            }
        }

        function sendAjaxConfirm() {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }});
            $.ajax({
                method:'POST',
                url: "{{ route('salary.confirmSalaryList') }}",
                data: $('#formConfirm').serialize(),
                success: response => {
                    if (response.success)
                        successConfirmAndReload();
                    else 
                        failureConfirm();
                }

            });
        }

        function successConfirmAndReload() {
            swal.fire({
                icon: 'success',
                title: 'Xác Nhận Thành Công',
                confirmButtonColor: '#3085d6',
                showConfirmButton: true,
                confirmButtonText: 'Xác Nhận',
            }).then(r => {
                window.location.reload();
            });
        }

        function failureConfirm() {
            swal.fire({
                icon: 'error',
                title: 'Xác Nhận Thất Bại',
                confirmButtonColor: '#3085d6',
                showConfirmButton: false,
            });
        }

    
    });
</script>

@endsection