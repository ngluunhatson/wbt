@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection

@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')

@php
$rooms = $dataInit['rooms'];
@endphp
<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h2 class="d-inline m-0">Tạo Lộ Trình Thử Việc</h2>
        <a href="#" noref>
            <button type="button" id="saveInternship" class="btn btn-success btn-label waves-effect waves-light">
                <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Lưu</button>
        </a>
    </div>
</div>

<form id="formInsert" method="POST" action="{{ route('internship.storeInsert') }}">
    @csrf
    <div class="card mt-3">
        <div class="card-header">
            <h5>Thông Tin Người Nhận Việc</h5>
        </div>
        <div class="card-body">
            <div class="row mt-3">
                <div class="col-xl-3">
                    <label for="borderInput" class="form-label">Phòng</label>
                    <select id="selectRoom" name="room_id" class="form-select mb-3" aria-label="Default select example">
                    </select>

                </div>
                <div class="col-xl-3">
                    <label class="form-label">Bộ phận</label>
                    <select id="selectTeam" name="team_id" class="form-select mb-3" aria-label="Default select example">
                    </select>
                </div>
                <div class="col-xl-3">
                    <label class="form-label">Chức danh</label>
                    <select id="selectPosition" name="position_id" class="form-select mb-3" aria-label="Default select example">
                    </select>
                </div>
                <div class="col-xl-3">
                    <label class="form-label">Họ và tên</label>
                    <select id="selectStaff" name="basic_info_id" class="form-select mb-3 @error('basic_info_id') is-invalid @enderror" aria-label="Default select example">
                    </select>
                    @error('basic_info_id')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-header">
            <h5>Thời Gian Thử Việc</h5>
        </div>
        <div class="row card-body justify-content-center">
            <div class="col-xl-3">
                <label for="start_date" class="form-label">Ngày bắt đầu</label>
                <input name="start_date" type="text" class="form-control datepicker" id="start_date" value="{{ old('start_date', now()->format('Y-m-d')) }}">
            </div>

            <div class="col-xl-3">
                <label for="end_date" class="form-label">Ngày kết thúc</label>
                <input name="end_date" type="text" class="form-control datepicker" id="end_date" value="{{ old('end_date', date('Y-m-d', strtotime('+1 day' . now())) ) }}">
            </div>

        </div>
    </div>


    <div class="card">
        <div class="row">
            <div class="row card-body justify-content-around">
                <div class="col-xl-2">
                    <div class="row ">
                        <div class="col-12 text-center">
                            <h4 class="form-label text-center">Người Tạo Đơn</h4>
                        </div>
                        <div class="col-12 text-center">
                            <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ auth()->user()->name }}</label>
                        </div>
                        <div class="col-12 d-flex justify-content-center mt-4">
                            <div class=" form-check" style="width: fit-content;">
                                <input class="form-check-input fs-15" type="checkbox" id="assign" value="1" name="assign">
                            </div>
                            <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác nhận</label>
                        </div>

                    </div>
                </div>
                <div class="col-xl-2">
                    <h4 class="form-label text-center">Người Nhận Việc</h4>
                    <select id="selectIntern" class="form-select" aria-label="Default select example">
                    </select>
                </div>
                <div class="col-xl-2">
                    <input hidden name="type_confirm_1" value="99">
                    <h4 class="form-label text-center">Người Giao Việc</h4>
                    <select name="confirm_1_basic_info_id" id="selectDirectBoss" class="form-select mb-3 @error('confirm_1_basic_info_id') is-invalid @enderror" aria-label="Default select example">
                    </select>
                    @error('confirm_1_basic_info_id')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-xl-2">
                    <input hidden name="type_confirm_2" value="4">
                    <h4 class="form-label text-center">Quản Lý Phòng</h4>
                    <select name="confirm_2_basic_info_id" id="selectRoomManager" class="form-select mb-3 @error('confirm_2_basic_info_id') is-invalid @enderror" aria-label="Default select example">
                    </select>
                    @error('confirm_2_basic_info_id')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-xl-2">
                    <input hidden name="type_confirm_3" value="2">
                    <h4 class="form-label text-center">P. QT HC-NS</h4>
                    <select name="confirm_3_basic_info_id" class="form-select mb-3 @error('confirm_3_basic_info_id') is-invalid @enderror" aria-label="Default select example">
                        @if(!empty($dataInit['hr']))
                        @foreach($dataInit['hr'] as $value)
                        @if(!empty($value['basic_info_id']))
                        <option value="{{ $value->staff->id }}">{{ $value->staff->full_name }}
                        </option>
                        @endif
                        @endforeach
                        @endif
                    </select>
                    @error('confirm_3_basic_info_id')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="col-xl-2">
                    <input hidden name="type_confirm_4" value="3">
                    <h4 class="form-label text-center">Ban Giám Đốc</h4>
                    <select name="confirm_4_basic_info_id" class="form-select mb-3 @error('confirm_4_basic_info_id') is-invalid @enderror" aria-label="Default select example">
                        @if(!empty($dataInit['ceo']))
                        @foreach($dataInit['ceo'] as $value)
                        @if(!empty($value['basic_info_id']))
                        <option value="{{ $value->staff->id }}">{{ $value->staff->full_name }}
                        </option>
                        @endif
                        @endforeach
                        @endif
                    </select>
                    @error('confirm_4_basic_info_id')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>

<script>
    $(function() {
        resetRooms();


    });

    $(function() {
        $('#start_date').datepicker({
            locale: 'vi',
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            daysOfWeekDisabled: [0]
        });

        $('#end_date').datepicker({
            locale: 'vi',
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            startDate: $('#start_date').val(),
            daysOfWeekDisabled: [0]
        });
    });
</script>

<script>
    $('#selectRoom').on('change', function() {
        var id = $('#selectRoom').find(":selected").val();
        $('#selectTeam')
            .find('option')
            .remove();
        $('#selectPosition')
            .find('option')
            .remove();
        $('#selectStaff')
            .find('option')
            .remove();
        $('#selectIntern')
            .find('option')
            .remove();

        if (id != '') {
            var url = "{!! route('dayoff.getTeamByRommID', ':id') !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                        $('#selectTeam').append($('<option>', {
                            value: 0,
                            text: 'Không Chọn'
                        }));
                        data.forEach(function(value) {
                            $('#selectTeam').append($('<option>', {
                                value: value.id,
                                text: value.name
                            }));
                        })
                    } else {
                        $('#selectTeam')
                            .find('option')
                            .remove()

                        if ($('#selectRoom').val() != 0) {
                            $('#selectTeam').append($('<option>', {
                                value: 0,
                                text: 'Không Chọn'
                            }));
                        }
                    }
                }
            });
        }
    });

    $('#selectTeam').on('change', function() {
        var id = $('#selectTeam').find(":selected").val();
        var url = "{!! route('dayoff.getPositionByRommID', ':id') !!}";
        $('#selectPosition')
            .find('option')
            .remove();
        $('#selectStaff')
            .find('option')
            .remove();
        $('#selectIntern')
            .find('option')
            .remove();
        $.ajax({
            type: 'GET',
            url: url.replace(':id', id),
            success: function(data) {
                if (data.length > 0) {
                    $('#selectPosition')
                        .find('option')
                        .remove();
                    $('#selectPosition').append($('<option>', {
                        value: 0,
                        text: 'Không Chọn'
                    }));

                    data.forEach(function(value) {
                        $('#selectPosition').append($('<option>', {
                            value: value.id,
                            text: value.name
                        }));
                    })
                } else {
                    $('#selectPosition')
                        .find('option')
                        .remove();
                    $('#selectPosition').append($('<option>', {
                        value: 0,
                        text: 'Không Chọn'
                    }));

                }
            }
        });
    });

    $('#selectPosition').on('change', function() {
        var room_id = $('#selectRoom').find(":selected").val();
        var team_id = $('#selectTeam').find(":selected").val();
        var position_id = $('#selectPosition').find(":selected").val();

        var url = "{!! route('dayoff.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";


        $('#selectStaff')
            .find('option')
            .remove();

        $('#selectIntern')
            .find('option')
            .remove();

        $.ajax({
            type: 'GET',
            url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
            success: function(data) {
                if (data.length > 0) {
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    $('#selectStaff').append($('<option>', {
                        value: 0,
                        text: `Không Có`,

                    }));

                    data.forEach(function(employee) {
                        $('#selectStaff').append($('<option>', {
                            value: employee['id'],
                            text: `${employee['full_name']}`,

                        }));
                    })
                } else {
                    $('#selectStaff')
                        .find('option')
                        .remove();

                    $('#selectStaff').append($('<option>', {
                        value: 0,
                        text: `Không Có`,

                    }));


                }
            }
        });
    });

    $('#selectStaff').on('change', function() {
        changeInternNameBox();
        getDirectBossAndRoomManager();

    });

    $('#start_date').on('change', function() {
        if ($(this)[0].value > $('#end_date')[0].value) {
            $('#end_date').datepicker('clearDates');
        }

        $('#end_date').datepicker('destroy');
        $('#end_date').datepicker({
            locale: 'vi',
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            daysOfWeekDisabled: [0],
            startDate: $(this)[0].value
        });

    });

    $('#saveInternship').on('click', function(e) {
        e.preventDefault();
        var url = "{!! route('internship.checkCreateIntership') !!}";

        $.ajax({
            method: 'POST',
            url: url,
            data: $('#formInsert').serialize(),
            success: function(response) {
                if (response.success)
                    $('#formInsert').submit();
                else {
                    swal.fire({
                        icon: 'error',
                        title: `${response.message}`,
                        confirmButtonColor: '#3085d6',
                        showConfirmButton: false,
                    });
                }

            }
        });
    });
</script>

<script>
    function resetRooms() {
        const rooms = <?php echo json_encode($rooms); ?>;
        $('#selectRoom')
            .find('option')
            .remove();
        $('#selectRoom').append($('<option>', {
            text: 'Vui lòng chọn phòng ban'
        }));

        rooms.forEach(function(room) {

            $('#selectRoom').append($('<option>', {
                value: room.id,
                text: room.name
            }));

        });

    }

    function changeInternNameBox() {
        $('#selectIntern')
            .find('option')
            .remove();

        const staff_name = $('#selectStaff').find(":selected").text();
        $('#selectIntern').append($('<option>', {
            text: `${staff_name}`,

        }));
    }

    function getDirectBossAndRoomManager() {
        var basic_info_id = $('#selectStaff').find(":selected").val();
        var url_get_direct_boss = "{!! route('FormDayOffHelper2', [':basic_info_id']) !!}";
        var url_get_room_manager = "{!! route('FormDayOffHelper4', [':basic_info_id']) !!}";

        $('#selectDirectBoss')
            .find('option')
            .remove();

        $('#selectRoomManager')
            .find('option')
            .remove();

        $.ajax({
            type: 'GET',
            url: url_get_direct_boss.replace(':basic_info_id', basic_info_id),
            success: function(data) {
                if (data.length > 0) {
                    data.forEach(function(direct_boss_basic_info) {
                        $('#selectDirectBoss').append($('<option>', {
                            value: direct_boss_basic_info['id'],
                            text: `${direct_boss_basic_info['full_name']}`,

                        }));
                    })
                }
            }
        });

        $.ajax({
            type: 'GET',
            url: url_get_room_manager.replace(':basic_info_id', basic_info_id),
            success: function(data) {
                if (data.length > 0) {
                    data.forEach(function(room_manager_basic_info) {
                        $('#selectRoomManager').append($('<option>', {
                            value: room_manager_basic_info['id'],
                            text: `${room_manager_basic_info['full_name']}`,

                        }));
                    })
                }
            }
        });
    }
</script>
@endsection