@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection

@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')

@php

$internshipX           = $dataInit['currentInternship'];

$test_admin_intern       = false;
$test_admin_direct_boss  = false;
$test_admin_room_manager = false;
$test_admin_hr           = false;
$test_admin_ceo          = false;

$isUserIntern          = auth() -> user() -> basic_info_id == $internshipX -> basic_info_id              || (auth() -> user() -> hasAnyRole(['super-admin']) && $test_admin_intern);
$isUserDirectBoss      = auth() -> user() -> basic_info_id == $internshipX -> direct_boss_basic_info_id  || (auth() -> user() -> hasAnyRole(['super-admin']) && $test_admin_direct_boss);
$isUserRoomManager     = auth() -> user() -> basic_info_id == $internshipX -> room_manager_basic_info_id || (auth() -> user() -> hasAnyRole(['super-admin']) && $test_admin_room_manager);
$isUserHr              = auth() -> user() -> basic_info_id == $internshipX -> hr_basic_info_id           || (auth() -> user() -> hasAnyRole(['super-admin']) && $test_admin_hr);
$isUserCeo             = auth() -> user() -> basic_info_id == $internshipX -> ceo_basic_info_id          || (auth() -> user() -> hasAnyRole(['super-admin']) && $test_admin_ceo);

$direct_boss_confirm  = $internshipX -> confirms[0] -> confirm;
$room_manager_confirm = null;
$hr_confirm           = null;
$ceo_confirm          = null;

if ($internshipX -> direct_boss_basic_info_id == $internshipX -> room_manager_basic_info_id) {
    $hr_confirm  = $internshipX -> confirms[1] -> confirm;
    $ceo_confirm = $internshipX -> confirms[2] -> confirm;
} else {
    $room_manager_confirm = $internshipX -> confirms[1] -> confirm;
    $hr_confirm           = $internshipX -> confirms[2] -> confirm;
    $ceo_confirm          = $internshipX -> confirms[3] -> confirm;
}


$isInternInputDisabled      = (!$isUserIntern      || ($internshipX -> status > 3  && !in_array($internshipX -> status, [8, 9, 10, 11, 14]))  );
$isDirectBossInputDisabled  = (!$isUserDirectBoss  || $direct_boss_confirm -> status != 0); 
$isRoomManagerInputDisabled = (!$isUserRoomManager || ($room_manager_confirm && $room_manager_confirm -> status != 0) ); 
$isHrInputDisabled          = (!$isUserHr          || $hr_confirm -> status != 0); 
$isCeoInputDisabled         = (!$isUserCeo         || $ceo_confirm -> status != 0); 

$typeUser = 1;
if ($isUserDirectBoss) 
    $typeUser = 2;
if ($isUserRoomManager && !$isUserDirectBoss)
    $typeUser = 3;
if ($isUserHr)
    $typeUser = 4;
if ($isUserCeo)
    $typeUser = 5;


@endphp

<style>
    .italic_text { 
        font-style: italic; 
    }

    .disabled_look_normal {
        border: 1px solid #000000;
        border-radius: 8px;
        height: 16px;
        width: 16px;
    }
</style>


<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h2 class="d-inline m-0">Lộ Trình Thử Việc</h2>
        <a href="#" noref>
            <button type="button" id="updateInternship" class="btn btn-success btn-label waves-effect waves-light">
                <i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Lưu</button>
        </a>
    </div>
</div>
<form id="formUpdate" method="POST" action="{{ route('internship.storeUpdate') }}">
    @csrf    
    <input hidden name ="internship_id" value = "{{ $internshipX -> id }}"/>
    <input hidden name ="type_user" value = "{{ $typeUser }}"/>


    <div class="card">
        <div class="card-header">
            <h5>Thông Tin Người Nhận Việc</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-3">
                    <label for="borderInput" class="form-label">Phòng</label>
                    <input disabled type="text" class="form-control" value="{{ $internshipX -> intern -> organizational -> room -> name }}">

                </div>
                <div class="col-xl-3">
                    <label class="form-label">Bộ phận</label>
                    <input disabled type="text" class="form-control" value="{{ $internshipX -> intern -> organizational -> team -> name }}">
                </div>
                <div class="col-xl-3">
                    <label class="form-label">Chức danh</label>
                    <input disabled type="text" class="form-control" value="{{ $internshipX -> intern -> organizational -> position -> name }}">

                </div>
                <div class="col-xl-3">
                    <label class="form-label">Họ và tên</label>
                    <input disabled type="text" class="form-control" value="{{ $internshipX -> intern -> full_name }}">
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h5>Thời Gian Thử Việc</h5>
        </div>
        <div class="row card-body justify-content-center">
            <div class="col-xl-3">
                <label for="start_date" class="form-label">Ngày bắt đầu</label>
                <input disabled type="text" class="form-control datepicker" id="start_date" value="{{ date('Y-m-d', strtotime($internshipX -> start_date)) }}">
            </div>

            <div class="col-xl-3">
                <label for="end_date" class="form-label">Ngày kết thúc</label>
                <input disabled type="text" class="form-control datepicker" id="end_date" value="{{ date('Y-m-d', strtotime($internshipX -> end_date)) }}">
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h5>Thông Tin Lộ Trình Thử Việc</h5>
        </div>

        <div class="card-body">
            <div class="accordion nesting2-accordion custom-accordionwithicon accordion-border-box mt-3">
                @foreach ($internshipX -> internshipDetails as $key => $internshipX_Detail)
                @php $internshipX_DetailArrayForm = $internshipX_Detail -> toArray(); @endphp
                <div class="accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#internship-part-{{ $internshipX_Detail -> part_number }}" aria-expanded="true" aria-controls="internship-part-{{ $internshipX_Detail -> part_number }}">
                            {{ $internshipX_Detail -> part_number == 1 ? "I" : "II" }}. {{ $internshipX_Detail -> part_title }}
                        </button>
                    </h2>
                    <div id="internship-part-{{ $internshipX_Detail -> part_number }}" class="accordion-collapse">
                        <div class="accordion-body">
                            <div class="accordion nesting2-accordion custom-accordionwithicon accordion-border-box mt-3">
                                @if($internshipX_Detail -> part_number == 1)
                                    @php    $part1_header_num = 1; 
                                            $part1_header_section_num_array = [1, 2, 3, 4, 7, 11 , 24, 26, 28]; 
                                            $part1_header_section_num_array_count = count($part1_header_section_num_array)
                                    @endphp
                                    
                                    @foreach ($part1_header_section_num_array as $key => $part1_header_section_num) 
                                        <div class="accordion-item">
                                            <h2 class="accordion-header">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#part1-header-{{ $part1_header_num }}" aria-expanded="false" aria-controls="part1-header-{{$part1_header_num}}">
                                                    {{ $part1_header_num }}. {{ config('header.internship_section_tile')[$internshipX_Detail -> part_number][$part1_header_section_num - 1] }}
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="part1-header-{{ $part1_header_num }}" class="accordion-collapse">
                                            <div class="accordion-body">
                                                @for ( $i = $part1_header_section_num; 
                                                       $i < ($key < $part1_header_section_num_array_count - 1 
                                                                ? $part1_header_section_num_array[$key + 1] : 30 );
                                                       $i ++ )
                                                    @if (!in_array($i, [4, 7, 11, 24, 26, 28]))
                                                <div class ="card">
                                                    <div class="card-header">
                                                        @if ($i == 27)
                                                        <h5>Phòng {{ mb_convert_case($internshipX -> intern -> organizational -> room -> name, MB_CASE_TITLE, "UTF-8"); }}
                                                        @else
                                                        <h5>{{ config('header.internship_section_tile')[$internshipX_Detail -> part_number][$i-1] }}</h5>
                                                            @if ($i == 26)
                                                                <p class="italic_text"> Theo quy trình các phòng ban được quy định tại phần nhiệm vụ của bảng 12 tiêu chí quản lý 1 vị trí </p>
                                                            @endif
                                                        @endif
                                                    </div>
                                                    <div class="card-body mt-0">
                                                        <div class="row justify-content-center mb-3">
                                                            @if ($i == 27)
                                                            <div class="col-xl-2">
                                                                <label class="form-label text-center">Nội Dung Đào Tạo</label>
                                                                <textarea  name = "section_{{ $i }}_5" class="form-control" rows="2" id = "section_{{ $i }}_5"
                                                                    @if($isInternInputDisabled) disabled @endif>{{ 
                                                                    old( 'section_'.$i.'_5' , ( array_key_exists(5, $internshipX_DetailArrayForm['section_'.$i]) 
                                                                                ? $internshipX_DetailArrayForm['section_'.$i][5] : null )) }}</textarea>
                                                            </div>
                                                            @endif
                                                            <div class="col-xl-2">
                                                                <label class="form-label text-center">Thời gian thực hiện</label>
                                                                <textarea name = "section_{{ $i }}_0" class="form-control" rows="2" id = "section_{{ $i }}_0"
                                                                        @if($isInternInputDisabled) disabled @endif>{{ 
                                                                    old( 'section_'.$i.'_0' , ( array_key_exists(0, $internshipX_DetailArrayForm['section_'.$i]) 
                                                                                ? $internshipX_DetailArrayForm['section_'.$i][0] : null )) }}</textarea>
                                                            </div>
                                                            <div class="col-xl-2">
                                                                <label class="form-label text-center">Bạn đã học được những gì</label>
                                                                <textarea  @if($isInternInputDisabled) disabled @endif
                                                                        name = "section_{{ $i }}_1" class="form-control" rows="2" id = "section_{{ $i }}_1">{{ 
                                                                    old( 'section_'.$i.'_1' , ( array_key_exists(1, $internshipX_DetailArrayForm['section_'.$i]) 
                                                                                ? $internshipX_DetailArrayForm['section_'.$i][1] : null )) }}</textarea>
                                                            </div>
                                                            <div class="col-xl-2">
                                                                <label class="form-label text-center">Điều bạn áp dụng ngay là gì</label>
                                                                <textarea  @if($isInternInputDisabled) disabled @endif
                                                                        name = "section_{{ $i }}_2" class="form-control" rows="2" id = "section_{{ $i }}_2">{{ 
                                                                    old( 'section_'.$i.'_2' , ( array_key_exists(2, $internshipX_DetailArrayForm['section_'.$i]) 
                                                                                    ? $internshipX_DetailArrayForm['section_'.$i][2] : null )) }}</textarea>
                                                            </div>
                                                            <div class="col-xl-2">
                                                                <div class = "row">
                                                                    <label class="form-label text-center">Nhân viên tự đánh giá (5/5)</label>
                                                                </div>
                                                            
                                                                @for ($option_val = 0; $option_val <= 5; $option_val ++) 
                                                                <input class="form-check-input" type="radio" style = "margin-left: 0.6em;"
                                                                    name = "section_{{ $i }}_3"  id="section_{{ $i }}_3_score_{{ $option_val }}_check" value = "{{ $option_val }}"
                                                                    @if($isInternInputDisabled) disabled @endif
                                                                    @if( ( array_key_exists(3, $internshipX_DetailArrayForm['section_'.$i])  && $internshipX_DetailArrayForm['section_'.$i][3] == $option_val )
                                                                        || old('section_'.$i.'_3') == $option_val ) checked @endif/>
                                                                <label class="form-check-label" for="section_{{ $i }}_3">
                                                                    {{ $option_val }}
                                                                </label>
                                                                @endfor
                                                                
                                                            </div>
                                                           
                                                            <div class="col-xl-2">
                                                                <div class ="row">
                                                                    <label class="form-label text-center">Người giao việc đánh giá (5/5)</label>
                                                                </div>
                                                                @if ( !in_array($internshipX -> status, [1,2,3]) || ($internshipX -> status == 3 && $isUserDirectBoss))
                                                                    @for ($option_val = 0; $option_val <= 5; $option_val ++) 
                                                                    <input class="form-check-input" type="radio" style = "margin-left: 0.6em;"
                                                                        name = "section_{{ $i }}_4"  id="section_{{ $i }}_4_score_{{ $option_val }}_check" value = "{{ $option_val }}"
                                                                        @if($isDirectBossInputDisabled) disabled @endif
                                                                        @if( ( array_key_exists(4, $internshipX_DetailArrayForm['section_'.$i])  && $internshipX_DetailArrayForm['section_'.$i][4] == $option_val )
                                                                            || old('section_'.$i.'_4') == $option_val ) checked @endif/>
                                                                    <label class="form-check-label" for="section_{{ $i }}_4">
                                                                        {{ $option_val }}
                                                                    </label>
                                                                    @endfor
                                                                @else 
                                                                <input disabled type ="text" class ="form-control" value = "Đợi Nhân Viên Đánh Giá Trước">
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                  @endif                      
                                                @endfor
                                            </div>
                                        </div>

                                        @php $part1_header_num ++; @endphp
                                    @endforeach
                                @else
                                    @php    $part2_header_num = 1; 
                                            $part2_header_section_num_array = [30, 31, 40, 41]; 
                                            $part2_header_section_num_array_count = count($part2_header_section_num_array);
                                    @endphp
                                    @foreach ($part2_header_section_num_array as $key => $part2_header_section_num) 
                                        <div class="accordion-item">
                                            <h2 class="accordion-header">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#part2-header-{{ $part2_header_num }}" aria-expanded="false" aria-controls="part2-header-{{$part2_header_num}}">
                                                    {{ $part2_header_num }}. {{ config('header.internship_section_tile')[$internshipX_Detail -> part_number][$part2_header_section_num - 1 - 29] }}
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="part2-header-{{ $part2_header_num }}" class="accordion-collapse">
                                            <div class="accordion-body">
                                                @for ( $i = $part2_header_section_num; 
                                                       $i < ($key < $part2_header_section_num_array_count - 1 
                                                                ? $part2_header_section_num_array[$key + 1] : 42 );
                                                       $i ++ )
                                                    @if ($i != 31)
                                                    <div class ="card">
                                                        <div class="card-header">
                                                            <h5>{{ config('header.internship_section_tile')[$internshipX_Detail -> part_number][$i-1-29] }}</h5>     
                                                        </div>
                                                        <div class="card-body mt-0">
                                                            <div class="row justify-content-center mb-3">
                                                                <div class="col-xl-2">
                                                                    <label class="form-label text-center">Thời gian thực hiện</label>
                                                                    <textarea name = "section_{{ $i }}_0"  @if($isInternInputDisabled) disabled @endif
                                                                        class="form-control" rows="2" id = "section_{{ $i }}_0">{{ 
                                                                    old( 'section_'.$i.'_0' , ( array_key_exists(1, $internshipX_DetailArrayForm['section_'.$i-29]) 
                                                                                    ? $internshipX_DetailArrayForm['section_'.$i-29][0] : null )) }}</textarea>
                                                                </div>
                                                                <div class="col-xl-2">
                                                                    <label class="form-label text-center">Bạn đã học được những gì</label>
                                                                    <textarea name = "section_{{ $i }}_1"  @if($isInternInputDisabled) disabled @endif
                                                                        class="form-control" rows="2" id = "section_{{ $i }}_1"> {{ 
                                                                    old( 'section_'.$i.'_1' , ( array_key_exists(1, $internshipX_DetailArrayForm['section_'.$i-29]) 
                                                                                    ? $internshipX_DetailArrayForm['section_'.$i-29][1] : null )) }}</textarea>
                                                                </div>
                                                                <div class="col-xl-2">
                                                                    <label class="form-label text-center">Điều bạn áp dụng ngay là gì</label>
                                                                    <textarea name = "section_{{ $i }}_2"  @if($isInternInputDisabled) disabled @endif
                                                                        class="form-control" rows="2" id = "section_{{ $i }}_2">{{ 
                                                                    old( 'section_'.$i.'_2' , ( array_key_exists(2, $internshipX_DetailArrayForm['section_'.$i-29]) 
                                                                                    ? $internshipX_DetailArrayForm['section_'.$i-29][2] : null )) }}</textarea>
                                                                </div>
                                                                <div class="col-xl-2">
                                                                    <div class = "row">
                                                                        <label class="form-label text-center">Nhân viên tự đánh giá (5/5)</label>
                                                                    </div>
                                                                    @for($option_val = 0; $option_val <= 5; $option_val ++) 
                                                                        <input class="form-check-input" type="radio" style = "margin-left: 0.6em;"
                                                                            name = "section_{{ $i }}_3"  id="section_{{ $i }}_3_score_{{ $option_val }}_check" value = "{{ $option_val }}"
                                                                            @if($isInternInputDisabled) disabled @endif
                                                                            @if( ( array_key_exists(3, $internshipX_DetailArrayForm['section_'.$i-29])  && $internshipX_DetailArrayForm['section_'.$i-29][3] == $option_val )
                                                                                || old('section_'.$i.'_3') == $option_val ) checked @endif/>
                                                                        <label class="form-check-label" for="section_{{ $i }}_3">
                                                                            {{ $option_val }}
                                                                        </label>
                                                                    @endfor
                                                                </div>
                                                                <div class="col-xl-2">
                                                                    <div class ="row">
                                                                        <label class="form-label text-center">Người giao việc đánh giá (5/5)</label>
                                                                    </div>
                                                                    @if ( !in_array($internshipX -> status, [1,2,3]) || ($internshipX -> status == 3 && $isUserDirectBoss))
                                                                        @for ($option_val = 0; $option_val <= 5; $option_val ++) 
                                                                        <input class="form-check-input" type="radio" style = "margin-left: 0.6em;"
                                                                            name = "section_{{ $i }}_4"  id="section_{{ $i }}_4_score_{{ $option_val }}_check" value = "{{ $option_val }}"
                                                                            @if($isDirectBossInputDisabled) disabled @endif
                                                                            @if( (array_key_exists(4, $internshipX_DetailArrayForm['section_'.$i-29]) 
                                                                                        && $internshipX_DetailArrayForm['section_'.$i-29][4] == $option_val)
                                                                                    || old('section_'.$i.'_4') == $option_val ) checked @endif/>
                                                                        <label class="form-check-label" for="section_{{ $i }}_4">
                                                                            {{ $option_val }}
                                                                        </label>
                                                                        @endfor
                                                                    @else 
                                                                    <input disabled type ="text" class ="form-control" value = "Đợi Nhân Viên Đánh Giá Trước">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                @endfor
                                            </div>
                                        </div>
                                        @php $part2_header_num++ @endphp
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row mt-3">
                <div class="col-xl-4 mt-5">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="m-0">Tổng Điểm</h3>
                        </div>
                        <div class="card-body">
                            <div class="row gy-3 justify-content-center">
                                <div class="col-xl-6">
                                    <h5 for="borderInput" class="form-label">Nhân viên tự đánh giá</h5>
                                    <div class="input-group">
                                        <input readonly type="number" maxlength="3" class="form-control text-center" name = "intern_score" id = "intern_score">
                                        <p class="input-group-text">/ <span style="margin-left: 10%;" id = "intern_max_score_text"></span></p>
                                        <input hidden name = "intern_max_score" id = "intern_max_score" value = "0">
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <h5 for="borderInput" class="form-label">Người giao việc đánh giá</h4>
                                    @if ( !in_array($internshipX -> status, [1,2,3]) || ($internshipX -> status == 3 && $isUserDirectBoss))
                                    <div class="input-group">
                                        <input readonly type="number" maxlength="3" class="form-control text-center" id = "direct_boss_score" name = "direct_boss_score">
                                        <p class="input-group-text">/ <span style="margin-left: 10%;" id = "direct_boss_max_score_text"></span></p>
                                    </div>
                                    <input hidden name = "direct_boss_max_score" id = "direct_boss_max_score" value = "0">
                                    @else 
                                    <input disabled type ="text" class ="form-control" value = "Đợi Nhân Viên Đánh Giá Trước">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="m-0">Kết quả đánh giá từ người giao việc</h3>
                        </div>
                        <div class="card-body">
                            <div class="row gy-4 mt-2 ps-3">
                                <div class="form-check">
                                    <input disabled class='form-check-input disabled_look_normal'  type="radio" name="point_check_very_low" id="point_check_0">
                                    <h5 for="formCheck2">
                                        Nếu đạt từ 0% - 50%: Xét duyệt chấm dứt thử việc
                                    </h5>
                                </div>
                                <div class="form-check">
                                    <input disabled class='form-check-input disabled_look_normal' type="radio" name="point_check_low" id="point_check_1">
                                    <h5 for="formCheck1">
                                    Nếu đạt từ 50% - 69%: Xét duyệt chấm dứt thử việc hoặc gia hạn thêm thời gian thử việc.
                                    </h5>
                                </div>
                                <div class="form-check">
                                    <input disabled class='form-check-input disabled_look_normal'  type="radio" name="point_check_mid" id="point_check_2">
                                    <h5 for="formCheck2">
                                        Nếu đạt từ 70% - 89%: Ký HĐLĐ chính thức và đào tạo thêm các nội dung chưa đạt.
                                    </h5>
                                </div>
                                <div class="form-check">
                                    <input disabled class='form-check-input disabled_look_normal' type="radio" name="point_check_high" id="point_check_3">
                                    <h5 for="formCheck3">
                                        Nếu đạt từ 90% trở lên: Ký HĐLĐ chính thức
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion nesting2-accordion custom-accordionwithicon accordion-border-box mt-3">
                <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#internship-part-3" aria-expanded="true" aria-controls="internship-part-3">
                                III. ĐỀ XUẤT TỪ NGƯỜI NHẬN VIỆC
                            </button>
                        </h2>
                        <div id="internship-part-3" class="accordion-collapse">
                            <div class="accordion-body">
                                <div class="row mt-3">
                                    <div class="col-xl-9">
                                        <label class="form-label">Đề Xuất Từ Nhân Viên Nhận Việc</label>
                                        <textarea name = "intern_comment" class="form-control" rows="7" @if($isInternInputDisabled) disabled @endif>{{ 
                                            old('intern_comment',  $internshipX -> intern_comment) }}</textarea>
                                    </div>
                                    <div class="col-xl-3">
                                        <div class="row gy-2 mb-2 mt-3">
                                            <div class = "input-group mt-4">
                                                <span class="input-group-text">Ngày Xác Nhận</span>
                                                <input readonly name="intern_confirm_date" type="text" class="form-control text-center " 
                                                    value="{{ $internshipX -> intern_confirm_date 
                                                                ? date('Y-m-d', strtotime($internshipX -> intern_confirm_date)) 
                                                                : ($isUserIntern ? now() -> format('Y-m-d') : 'Chưa Xác Nhận') }}">
                                            </div>
                                            <h4 class="form-label text-center">Nhân Viên Nhận Việc</h4>
                                            <input disabled type="text" class="form-control text-center mt-2" value="{{ $internshipX -> intern -> full_name }}">
                                        </div>
                                        <div class = "row justify-content-center">
                                            <div class="d-flex justify-content-center">
                                                @if ($internshipX -> status < 3 || (in_array($internshipX -> status, [3,8,9,10,11,14]) && $isUserIntern))
                                                <input class="form-check-input me-2" type="checkbox" @if($isInternInputDisabled) disabled @endif
                                                    name="intern_confirm" id="intern_confirm" value ="1">
                                                <label class="form-check-label " for="intern_confirm">Xác nhận</label>
                                                @else 
                                                <label class="form-check-label " for="intern_confirm">Đã Xác Nhận</label>
                                                @endif

                                            </div>
                                           
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>

                <div class = "accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#internship-part-4" aria-expanded="true" aria-controls="internship-part-4">
                            IV. ĐỀ XUẤT TỪ NGƯỜI GIAO VIỆC
                        </button>
                    </h2>        
                    <div id="internship-part-4" class="accordion-collapse">
                        <div class="accordion-body">
                            <div class="row mt-3">
                                <div class="col-xl-6">
                                    <label class="form-label"> Đề Xuất Từ Người Giao Việc
                                    </label>
                                    <textarea name ="direct_boss_comment" class="form-control" rows="10"  @if($isDirectBossInputDisabled) disabled @endif>{{ 
                                        old('direct_boss_comment',  $internshipX -> direct_boss_comment) }}</textarea>
                                </div>
                                <div class="col-xl-3 my-auto">
                                    
                                        <div class="form-check" >
                                            <input class="form-check-input" type="radio" @if($isDirectBossInputDisabled) disabled @endif
                                                name="direct_boss_decision_check" id="direct_boss_decision_1_check" value = "1"
                                                @if($internshipX -> direct_boss_decision_1 == 1 || old('direct_boss_decision_check') == "1" ) checked @endif/>
                                            <label class="form-check-label" for="direct_boss_decision_1_check">
                                                Chấm dức thử việc
                                            </label>
                                            <input hidden name="direct_boss_decision_1" id="direct_boss_decision_1" value = "{{ $internshipX -> direct_boss_decision_1 ? 1 : 0 }}" >
                                        
                                        </div>
                                   
                                        <div class = "input-group">
                                            <div class="input-group-prepend">
                                                <div class="form-check mt-2">
                                                    <input class="form-check-input" @if($isDirectBossInputDisabled) disabled @endif
                                                    type="radio" name="direct_boss_decision_check" id="direct_boss_decision_2_check" value = "2"
                                                    @if( $internshipX -> direct_boss_decision_2  || old('direct_boss_decision_check') == "2" ) checked @endif >
                                                    <label class="form-check-label" for="direct_boss_decision_2_check">
                                                        Ký HĐLĐ xác định thời hạn
                                                    </label>
                                                </div>
                                            </div>
                                            
                                            <div style ="width:20px"></div>
                                            
                                            <div  class ="col-md-2">
                                                <input style = "height:25px; width:60px;"  type="number"
                                                    name = "direct_boss_decision_2" id ="direct_boss_decision_2" 
                                                    class="form-control mt-1  @error('direct_boss_decision_2') is-invalid @enderror"
                                                    value = "{{ $internshipX -> direct_boss_decision_2 ? $internshipX -> direct_boss_decision_2 : null }}"
                                                    @if($isDirectBossInputDisabled || !$internshipX ->direct_boss_decision_2  && !old('direct_boss_decision_2')) disabled @endif/>
                                             
                                            </div>

                                            <div style ="width:10px"></div>

                                            <span class= "mt-2">Tháng</span>
                                        </div>
                                        @error('direct_boss_decision_2')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Vui lòng điền số tháng</strong>
                                            </span>
                                        @enderror

                                        <div class = "input-group">
                                            <div class="input-group-prepend">
                                                <div class="form-check mt-2">
                                                    <input class="form-check-input" type="radio" value = "3"
                                                        name="direct_boss_decision_check" id="direct_boss_decision_3_check"
                                                        @if($isDirectBossInputDisabled) disabled @endif 
                                                        @if( $internshipX -> direct_boss_decision_3 || old('direct_boss_decision_check') == "3") checked @endif/>
                                                    <label class="form-check-label" for="direct_boss_decision_3_check">
                                                        Ký HĐLĐ không xác định thời hạn, từ
                                                    </label>
                                                </div>
                                            </div>
                                            
                                            <div style ="width:20px"></div>


                                            <div class ="col-md-2">
                                                <input style = "height:25px; width:100px;"  type="text"
                                                    name = "direct_boss_decision_3" id ="direct_boss_decision_3" 
                                                    class="form-control datepicker mt-1 @error('direct_boss_decision_e') is-invalid @enderror""
                                                    value = "{{ old('direct_boss_decision_3',  ($internshipX -> direct_boss_decision_3)) }}" 
                                                    @if($isDirectBossInputDisabled || !$internshipX ->direct_boss_decision_3 && !old('direct_boss_decision_3')) disabled @endif/>

                                            </div>

                                        </div>
                                        @error('direct_boss_decision_3')
                                            <span class="text-danger" role="alert">
                                                <strong>Vui lòng nhập thời gian</strong>
                                            </span>
                                        @enderror
                                       
                                  
                                      
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="direct_boss_decision_4_check" id="direct_boss_decision_4_check"
                                                @if($isDirectBossInputDisabled) disabled @endif
                                                @if( $internshipX -> direct_boss_decision_4 || old('direct_boss_decision_4_check') ) checked @endif/>
                                            <label class="form-check-label" for="direct_boss_decision_4_check">
                                                Khác
                                            </label>
                                        </div>
                                    
                                        <textarea id = "direct_boss_decision_4" name = "direct_boss_decision_4" class="form-control mt-2" rows="3"
                                            @if($isDirectBossInputDisabled || !$internshipX ->direct_boss_decision_4 && !old('direct_boss_decision_4_check') ) disabled @endif/>{{ 
                                            old('direct_boss_decision_4',  $internshipX -> direct_boss_decision_4) }}</textarea>
                                </div>
                                <div class="col-xl-3">
                                    <div class="row gy-2 mb-3">
                                            <div class = "input-group mt-2">
                                                <span class="input-group-text">Ngày Duyệt</span>
                                                <input readonly name="direct_boss_review_date" type="text" class="form-control text-center " 
                                                    value="{{ $internshipX -> direct_boss_review_date 
                                                            ? date('Y-m-d', strtotime($internshipX -> direct_boss_review_date))
                                                            : ( $internshipX -> status == 8 ? 'Không Được Duyệt' 
                                                                : ( $isUserDirectBoss ? now() -> format('Y-m-d') : 'Chưa Duyệt')) }}">
                                            </div>
                                        <h4 class="form-label text-center">Người Giao Việc</h4>
                                        <input disabled type="text" class="form-control text-center mt-2" value="{{ $internshipX -> directBoss -> full_name }}">
                                    </div>
                                    
                                    <input hidden name = 'direct_boss_confirm_id' value = '{{ $direct_boss_confirm -> id }}'>
                                    @switch($direct_boss_confirm -> status)
                                    @case(0)
                                    <div class="row justify-content-center mt-3">
                                        <div class="col-xl-4 text-center">
                                                <input class="form-check-input me-2 @error('direct_boss_confirm') is-invalid @enderror" type="radio" name="direct_boss_confirm"
                                                        @if(!$isUserDirectBoss) disabled @endif @if (old('direct_boss_confirm') == "1") checked @endif
                                                        id="direct_boss_confirm_accept_check" value = "1">
                                                <label class="form-check-label text-center " for="direct_boss_confirm_accept_check">Duyệt</label>
                                        </div>
                                        <div class="col-xl-4 text-center">
                                                <input class="form-check-input  @error('direct_boss_confirm') is-invalid @enderror" type="radio" name="direct_boss_confirm" 
                                                        @if(!$isUserDirectBoss) disabled @endif @if (old('direct_boss_confirm') == "2") checked @endif
                                                        id="direct_boss_confirm_decline_check" value = "2">
                                                <label class="form-check-label text-center"  for="direct_boss_confirm_decline_check">Không Duyệt</label>
                                        </div>
                                      
                                    </div>
                                    @error('direct_boss_confirm')
                                            <span class="text-danger" role="alert">
                                                <strong>Vui lòng Duyệt/Không Duyệt</strong>
                                            </span>
                                    @enderror
                                    <div class ="mt-3">
                                        <label class="form-label" for = "direct_boss_decline_reason"> Lý Do Không Duyệt
                                        </label>
                                        <textarea class="form-control @error('direct_boss_decline_reason') is-invalid @enderror" name ="direct_boss_decline_reason" id = "direct_boss_decline_reason" rows="2" 
                                            @if (!old('direct_boss_confirm')) disabled @endif>{{ 
                                                old('direct_boss_decline_reason', $direct_boss_confirm -> note) }}</textarea>
                                    </div>

                                            
                                    @error('direct_boss_decline_reason')
                                            <span class="text-danger" role="alert">
                                                <strong>Vui lòng nhập Lý Do</strong>
                                            </span>
                                    @enderror
                                    
                                    @break

                                    @case(1)
                                    <div class = "row justify-content-center">
                                        <div class="d-flex justify-content-center">
                                            <label class="form-check-label">Đã Xác Nhận</label>
                                        </div>
                                    </div>
                                    @break

                                    @case(2)
                                    <div class = "row justify-content-center">
                                        <p class="text-center">Đã Từ Chối</p>
                                    </div>
                                    <div class ="mb-3">
                                        <label class="form-label"> Lý Do Không Duyệt
                                        </label>
                                        <textarea disabled class="form-control" rows="2">{{ $direct_boss_confirm -> note }}</textarea>
                                    </div>
                                    @break
                                    @endswitch
                                </div>
                            </div>
                        </div>
                    </div>           
                </div>

                <div class = "accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#internship-part-5" aria-expanded="true" aria-controls="internship-part-5">
                            V. Ý KIẾN ĐÁNH GIÁ VÀ ĐỀ XUẤT TỪ CẤP QUẢN LÝ CAO NHẤT CỦA PHÒNG
                        </button>
                    </h2> 
                    <div id ="internship-part-5" class = "accordion-collapse">
                        <div class="accordion-body">
                            <div class="row mt-3">
                                <div class="col-xl-9">
                                    <label class="form-label"> Ý kiến đánh giá và Đề xuất từ Cấp Quản Lý cao nhất của Phòng
                                    </label>
                                    <textarea @if ($isRoomManagerInputDisabled || ($isUserDirectBoss && $isUserRoomManager)) disabled @endif 
                                        class="form-control" name ="room_manager_comment" rows="13">{{ 
                                            old('room_manager_comment', $internshipX -> room_manager_comment) }}</textarea>
                                </div>
                                <div class="col-xl-3">
                                    <div class="row gy-2 mb-2 mt-1">
                                     @if ($internshipX -> direct_boss_basic_info_id != $internshipX -> room_manager_basic_info_id)
                                        <div class = "input-group mt-4">
                                            <span class="input-group-text">Ngày Duyệt</span>
                                            <input readonly name="room_manager_review_date" type="text" class="form-control text-center" 
                                                value="{{ $internshipX -> room_manager_review_date 
                                                        ? date('Y-m-d', strtotime($internshipX -> room_manager_review_date)) 
                                                        : ( $internshipX -> status == 9 ? 'Không Được Duyệt' 
                                                            :  ( $isUserRoomManager ? now() -> format('Y-m-d') : 'Chưa Duyệt')) }}"/>
                                        </div>
                                        @endif
                                        <h4 class="form-label text-center">Quản Lý Cao Nhất Của Phòng</h4>
                                        <input disabled type="text" class="form-control text-center mt-2" value="{{ $internshipX -> roomManager -> full_name }}">
                                    </div>
                                  
                                    @if ($internshipX -> direct_boss_basic_info_id == $internshipX -> room_manager_basic_info_id)
                                    <div class="row justify-content-center mt-3">
                                                <p class="text-center">Không Cần Duyệt vì trùng người với Người Giao Việc </p>
                                    </div>
                                    @else
                                    <input hidden name = 'room_manager_confirm_id' value = '{{ $room_manager_confirm -> id }}'>
                                    @switch($room_manager_confirm -> status)
                                    @case(0)
                                    <div class="row justify-content-center mt-3">
                                        <div class="col-xl-4 text-center">
                                                <input class="form-check-input me-2 @error('room_manager_confirm') is-invalid @enderror" type="radio" name="room_manager_confirm" 
                                                        @if($isRoomManagerInputDisabled) disabled @endif @if (old('room_manager_confirm') == "1") checked @endif
                                                        id="room_manager_confirm_accept_check" value = "1">
                                                <label class="form-check-label text-center" for="room_manager_confirm_accept_check">Duyệt</label>
                                        </div>
                                        <div class="col-xl-4 text-center">
                                                <input class="form-check-input @error('room_manager_confirm') is-invalid @enderror" type="radio" name="room_manager_confirm" 
                                                        @if($isRoomManagerInputDisabled) disabled @endif @if (old('room_manager_confirm') == "2") checked @endif
                                                        id="room_manager_confirm_decline_check" value = "2">
                                                <label class="form-check-label text-center" for="room_manager_confirm_decline_check">Không Duyệt</label>
                                        </div>
                                    </div>
                                    @error('room_manager_confirm')
                                            <span class="text-danger" role="alert">
                                                <strong>Vui lòng Duyệt/Không Duyệt</strong>
                                            </span>
                                    @enderror
                                    <div class ="mt-3">
                                        <label class="form-label" for = "room_manager_decline_reason"> Lý Do Không Duyệt
                                        </label>
                                        <textarea class="form-control @error('room_manager_confirm') is-invalid @enderror" name ="room_manager_decline_reason" id ="room_manager_decline_reason" rows="2" 
                                            @if (!old('room_manager_confirm')) disabled @endif>{{ 
                                                old('room_manager_decline_reason', $room_manager_confirm -> note) }}</textarea>
                                    </div>

                                    @error('room_manager_decline_reason')
                                            <span class="text-danger" role="alert">
                                                <strong>Vui lòng nhập Lý Do</strong>
                                            </span>
                                    @enderror
                                    @break
                                    @case(1)
                                    <div class = "row justify-content-center">
                                        <div class="d-flex justify-content-center">
                                            <label class="form-check-label">Đã Xác Nhận</label>
                                        </div>
                                    </div>
                                    @break

                                    @case(2)
                                    <div class = "row justify-content-center">
                                        <p class="text-center">Đã Từ Chối</p>
                                    </div>
                                    <div class ="mb-3">
                                        <label class="form-label"> Lý Do Không Duyệt
                                        </label>
                                        <textarea disabled class="form-control" rows="2">{{ $room_manager_confirm -> note }}</textarea>
                                    </div>
                                    @break
                                    @endswitch
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>                                
                </div>

                <div class = "accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#internship-part-6" aria-expanded="true" aria-controls="internship-part-6">
                            VI. Ý KIẾN ĐÁNH GIÁ VÀ ĐỀ XUẤT TỪ P. QT HÀNH CHÍNH - NHÂN SỰ
                        </button>
                    </h2>        
                    <div id="internship-part-6" class="accordion-collapse">
                        <div class="accordion-body">
                            <div class="row mt-3">
                                <div class="col-xl-6">
                                    <label class="form-label"> Ý kiến đánh giá và Đề xuất từ P.QT HC-NS
                                    </label>
                                    <textarea name ="hr_comment" class="form-control" rows="10" @if($isHrInputDisabled ) disabled @endif>{{ 
                                        old('hr_comment',  $internshipX -> hr_comment) }}</textarea>
                                </div>
                                <div class="col-xl-3 my-auto">
                                    @if ($internshipX -> status != 12)
                                    <div class = "input-group">
                                        <div class="input-group-prepend">
                                            <div class="form-check mt-2">
                                                <input class="form-check-input"@if( $isHrInputDisabled ) disabled @endif
                                                    type="checkbox" name="hr_decision_1_check" id="hr_decision_1_check"
                                                    @if( $internshipX -> hr_decision_1 || old('hr_decision_1_check') ) checked @endif >
                                                <label class="form-check-label" for="hr_decision_1_check">
                                                    Mức lương chính thức 
                                                </label>
                                            </div>
                                        </div>
                                        
                                        <div style ="width:20px"></div>
                                        
                                        <div class ="col-md-2">
                                            <input style = "height:25px; width:100px;"  type="number"   
                                                name = "hr_decision_1" id ="hr_decision_1" class="form-control mt-1 @error('hr_decision_1') is-invalid @enderror"
                                                value = "{{ old('hr_decision_1',  ($internshipX -> hr_decision_1)) }}"
                                                @if($isHrInputDisabled || !$internshipX ->  hr_decision_1 && !old('hr_decision_1_check') ) disabled @endif/>      
                                        </div>

                                        <div style ="width:50px"></div>

                                        <span class= "mt-2">/ Tháng</span>
                                    </div>
                                    @error('hr_decision_1')
                                        <span class="text-danger" role="alert">
                                            <strong>Vui lòng nhập Mức Lương</strong>
                                        </span>
                                    @enderror

                                    <div class = "input-group">
                                        <div class="input-group-prepend">
                                            <div class="form-check mt-2">
                                                <input class="form-check-input" @if($isHrInputDisabled) disabled @endif
                                                    type="checkbox" name="hr_decision_2_check" id="hr_decision_2_check"
                                                    @if( $internshipX -> hr_decision_2 || old('hr_decision_2_check')) checked @endif/>
                                                <label class="form-check-label" for="hr_decision_2_check">
                                                    Các phụ cấp/trợ cấp/phúc lợi khác (nếu có)
                                                </label>
                                            </div>
                                        </div>
                                        
                                        <div style ="width:20px"></div>
                                        
                                        <div class ="col-md-2">
                                            <input style = "height:25px; width:80px;"  type="number"
                                                name = "hr_decision_2" id ="hr_decision_2" class="form-control mt-1 @error('hr_decision_2') is-invalid @enderror"
                                                value = "{{ old('hr_decision_2',  ($internshipX -> hr_decision_2)) }}"
                                                @if($isHrInputDisabled || !$internshipX ->  hr_decision_2 && !old('hr_decision_2_check')) disabled @endif/>          
                                        </div>
                                    </div>

                                    @error('hr_decision_2')
                                        <span class="text-danger" role="alert">
                                            <strong>Vui lòng nhập số tiền Phụ Cấp</strong>
                                        </span>
                                    @enderror

                                    <div class = "input-group">
                                        <div class="input-group-prepend">
                                            <div class="form-check mt-2">
                                                <input class="form-check-input" @if($isHrInputDisabled ) disabled @endif
                                                    type="radio" name="hr_decision_hdld_check" id="hr_decision_3_check" value = "3"
                                                    @if( $internshipX -> hr_decision_3 || old('hr_decision_hdld_check') == "3") checked @endif/>
                                                <label class="form-check-label" for="hr_decision_3_check">
                                                    Ký HĐLĐ xác định thời hạn
                                                </label>
                                            </div>
                                        </div>
                                        
                                        <div style ="width:20px"></div>
                                        
                                        <div  class ="col-md-2">
                                            <input style = "height:25px; width:60px;"  type="number"
                                                name = "hr_decision_3" id ="hr_decision_3" class="form-control mt-1 @error('hr_decision_3') is-invalid @enderror"
                                                value = "{{ $internshipX -> hr_decision_3 ? $internshipX -> hr_decision_3 : null }}"
                                                @if($isHrInputDisabled || !$internshipX ->  hr_decision_3 && !( old('hr_decision_hdld_check') == "3") ) disabled @endif/>
                                        </div>

                                        <div style ="width:10px"></div>


                                        <span class= "mt-2">Tháng</span>
                                    </div>

                                    @error('hr_decision_3')
                                        <span class="text-danger" role="alert">
                                            <strong>Vui lòng nhập số Tháng</strong>
                                        </span>
                                    @enderror

                                    <div class = "input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="form-check mt-2">
                                                <input class="form-check-input" type="radio" name="hr_decision_hdld_check" 
                                                    id="hr_decision_4_check" value = "4"
                                                    @if($isHrInputDisabled) disabled @endif
                                                    @if( $internshipX -> hr_decision_4 || old('hr_decision_hdld_check') == "4") checked @endif/>
                                                <label class="form-check-label" for="hr_decision_4_check">
                                                    Ký HĐLĐ không xác định thời hạn, từ
                                                </label>
                                            </div>
                                        </div>
                                        
                                        <div style ="width:20px"></div>

                                        <div class ="col-md-2">
                                            <input  style = "height:25px; width:100px;"  type="text" name = "hr_decision_4" id ="hr_decision_4"
                                                 class="form-control datepicker mt-1 @error('hr_decision_4') is-invalid @enderror"
                                                value = "{{ old('hr_decision_4',  ($internshipX -> hr_decision_4)) }}"
                                                @if($isHrInputDisabled || !$internshipX ->  hr_decision_4 && !(old('hr_decision_hdld_check') == "4")) disabled @endif/>
                                                            
                                        </div>
                                    </div>
                                    @error('hr_decision_4')
                                        <span class="text-danger" role="alert">
                                            <strong>Vui lòng nhập Thời Gian</strong>
                                        </span>
                                    @enderror
                                    
                                    @endif
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="hr_decision_5_check" id="hr_decision_5_check"
                                            @if($isHrInputDisabled ) disabled @endif
                                            @if( $internshipX ->  hr_decision_5 || old('hr_decision_5_check') ) checked @endif/>
                                        <label class="form-check-label" for="hr_decision_5_check">
                                            Khác
                                        </label>
                                    </div>
                                   
                                    <textarea id = "hr_decision_5"name = "hr_decision_5" class="form-control mt-2" rows="3"
                                        @if($isHrInputDisabled || !$internshipX ->  hr_decision_5 && !old('hr_decision_5_check')) disabled @endif>{{                                         
                                        old('hr_decision_5',  $internshipX -> hr_decision_5) }}</textarea>
                                </div>
                                <div class="col-xl-3">
                                    <div class="row gy-2 mb-3">
                                            <div class = "input-group mt-2">
                                                <span class="input-group-text">Ngày Duyệt</span>
                                                <input readonly name="hr_review_date" type="text" class="form-control text-center " 
                                                    value="{{ $internshipX -> hr_review_date 
                                                            ? date('Y-m-d', strtotime($internshipX -> hr_review_date)) 
                                                            : ( $internshipX -> status == 10 ? 'Không Được Duyệt' 
                                                                : ($isUserHr ? now() -> format('Y-m-d') : 'Chưa Duyệt')) }}">
                                            </div>
                                        <h4 class="form-label text-center">P. QT HC-NS</h4>
                                        <input disabled type="text" class="form-control text-center mt-2" value="{{ $internshipX -> hr -> full_name }}">
                                    </div>

                                    <input hidden name = 'hr_confirm_id' value = '{{ $hr_confirm -> id }}'>
                                    @switch($hr_confirm -> status)
                                    @case(0)
                                    <div class="row justify-content-center mt-3">
                                        <div class="col-xl-4 text-center">
                                                <input class="form-check-input me-2 @error('hr_confirm') is-invalid @enderror" type="radio" name="hr_confirm" 
                                                       @if($isHrInputDisabled ) disabled @endif @if (old('hr_confirm') == "1") checked @endif
                                                       id="hr_confirm_accept_check" value = "1">
                                                <label class="form-check-label text-center" for="hr_confirm_accept_check">Duyệt</label>
                                        </div>
                                        <div class="col-xl-4 text-center">
                                                <input class="form-check-input @error('hr_confirm') is-invalid @enderror" type="radio" name="hr_confirm" 
                                                       @if($isHrInputDisabled ) disabled @endif @if (old('hr_confirm') == "2") checked @endif
                                                       id="hr_confirm_decline_check" value = "2">
                                                <label class="form-check-label text-center" for="hr_confirm_decline_check">Không Duyệt</label>
                                        </div>
                                    </div>
                                    @error('hr_confirm')
                                            <span class="text-danger" role="alert">
                                                <strong>Vui lòng Duyệt/Không Duyệt</strong>
                                            </span>
                                    @enderror
                                    <div class ="mt-3">
                                        <label class="form-label @error('hr_decline_reason') is-invalid @enderror" for = "hr_decline_reason"> Lý Do Không Duyệt
                                        </label>
                                        <textarea class="form-control" name ="hr_decline_reason" id = "hr_decline_reason" rows="2" @if (!old('hr_confirm')) disabled @endif>{{ 
                                                old('hr_decline_reason', $hr_confirm -> note) }}</textarea>
                                    </div>
                                    @error('hr_decline_reason')
                                            <span class="text-danger" role="alert">
                                                <strong>Vui lòng nhập Lý Do</strong>
                                            </span>
                                    @enderror
                                    @break

                                    @case(1)
                                    <div class = "row justify-content-center">
                                        <div class="d-flex justify-content-center">
                                            <label class="form-check-label">Đã Xác Nhận</label>
                                        </div>
                                    </div>
                                    @break

                                    @case(2)
                                    <div class = "row justify-content-center">
                                        <p class="text-center">Đã Từ Chối</p>
                                    </div>
                                    <div class ="mb-3">
                                        <label class="form-label"> Lý Do Không Duyệt
                                        </label>
                                        <textarea disabled class="form-control" rows="2">{{ $hr_confirm -> note }}</textarea>
                                    </div>
                                    @break
                                    @endswitch
                                </div>
                            </div>
                        </div>
                    </div>           
                </div>

                <div class="accordion-item">
                    <h2 class="accordion-header">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#internship-part-7" aria-expanded="true" aria-controls="internship-part-7">
                            PHÊ DUYỆT TỪ BAN TỔNG GIÁM ĐỐC
                        </button>
                    </h2>
                    <div id="internship-part-7" class="accordion-collapse">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col-xl-3 m-auto">
                                    <div class="row gy-2">
                                        <div class = "input-group mt-2">
                                            <span class="input-group-text">Ngày Duyệt</span>
                                            <input readonly name="ceo_review_date" type="text" class="form-control text-center " 
                                                value="{{ $internshipX -> ceo_review_date 
                                                        ? date('Y-m-d', strtotime($internshipX -> ceo_review_date)) 
                                                        : ( $internshipX -> status == 11 ? 'Không Được Duyệt' 
                                                            : ($isUserCeo ? now() -> format('Y-m-d') : 'Chưa Duyệt')) }}">
                                        </div>
                                        <h4 class="form-label text-center">PHÊ DUYỆT CỦA BAN TỔNG GIÁM ĐỐC</h4>
                                        <input disabled type="text" class="form-control text-center mt-2" value="{{ $internshipX -> ceo -> full_name }}">
                                        
                                        <input hidden name = 'ceo_confirm_id' value = '{{ $ceo_confirm -> id }}'>
                                        @switch($ceo_confirm -> status)
                                        @case(0)
                                        <div class="row justify-content-center mt-3">
                                            <div class="col-xl-4 text-center">
                                                    <input class="form-check-input me-2 @error('ceo_confirm') is-invalid @enderror" type="radio" name="ceo_confirm" 
                                                            @if($isCeoInputDisabled) disabled @endif @if (old('ceo_confirm') == "1") checked @endif
                                                            id="ceo_confirm_accept_check" value = "1">
                                                    <label class="form-check-label text-center" for="ceo_confirm_accept_check">Duyệt</label>
                                            </div>
                                            <div class="col-xl-4 text-center">
                                                    <input class="form-check-input @error('ceo_confirm') is-invalid @enderror" type="radio" name="ceo_confirm" 
                                                            @if($isCeoInputDisabled) disabled @endif @if (old('ceo_confirm') == "2") checked @endif
                                                            id="ceo_confirm_decline_check" value = "2">
                                                    <label class="form-check-label text-center" for="ceo_confirm_decline_check">Không Duyệt</label>
                                            </div>
                                        </div>
                                        @error('ceo_confirm')
                                            <span class="text-danger" role="alert">
                                                <strong>Vui lòng Duyệt/Không Duyệt</strong>
                                            </span>
                                        @enderror
                                        
                                        <div class ="mt-4">
                                            <label class="form-label" for = "ceo_decline_reason"> Lý Do Không Duyệt
                                            </label>
                                            <textarea class="form-control @error('ceo_decline_reason') is-invalid @enderror" name ="ceo_decline_reason" id ="ceo_decline_reason" rows="2" 
                                            @if (!old('ceo_confirm')) disabled @endif>{{ 
                                                    old('ceo_decline_reason', $ceo_confirm -> note) }}</textarea>
                                        </div>
                                        @error('ceo_decline_reason')
                                            <span class="text-danger" role="alert">
                                                <strong>Vui lòng nhập Lý Do</strong>
                                            </span>
                                        
                                            @enderror
                                        @break

                                        @case(1)
                                        <div class = "row justify-content-center">
                                            <div class="d-flex justify-content-center">
                                                <label class="form-check-label">Đã Xác Nhận</label>
                                            </div>
                                        </div>
                                        @break

                                        @case(2)
                                        <div class = "row justify-content-center">
                                        <p class="text-center">Đã Từ Chối</p>
                                        </div>
                                        <div class ="mb-3">
                                            <label class="form-label"> Lý Do Không Duyệt
                                            </label>
                                            <textarea disabled class="form-control" rows="2">{{ $ceo_confirm -> note }}</textarea>
                                        </div>
                                        @break
                                        @endswitch
                                    </div>
                                </div>
                            </div>                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection
@section('script')
<script src="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/form-pickers.init.js') }}"></script>
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>

<script>
    const internshipX = <?php echo json_encode($internshipX); ?>;

    let intern_max_score = 0;
    let direct_boss_max_score = 0;

    const valid_intern_sec_num_set = new Set();

    const isUserDirectBoss = <?php echo json_encode($isUserDirectBoss); ?>;
    const isUserIntern     = <?php echo json_encode($isUserIntern); ?>;



    const currentDateString = (new Date()).toISOString().slice(0,10);
    const direct_boss_decision_2 =  internshipX['direct_boss_decision_2'] ? internshipX['direct_boss_decision_2'] : 0;
    const direct_boss_decision_3 =  internshipX['direct_boss_decision_3'] ? internshipX['direct_boss_decision_3'].slice(0,10) : currentDateString;

    const hr_decision_1          =  internshipX['hr_decision_1'] ? internshipX['hr_decision_1']  : 0;
    const hr_decision_2          =  internshipX['hr_decision_2'] ? internshipX['hr_decision_2']  : 0;
    const hr_decision_3          =  internshipX['hr_decision_3'] ? internshipX['hr_decision_3']  : 0;
    const hr_decision_4          =  internshipX['hr_decision_4'] ? internshipX['hr_decision_4'].slice(0,10)  : currentDateString;



    $(document).ready(function() {
        $(function() {
         
            // $(`#direct_boss_decision_2`)[0].disabled = !(internshipX['direct_boss_decision_2']); 
            // $(`#direct_boss_decision_3`)[0].disabled = !(internshipX['direct_boss_decision_3']);
            // $(`#direct_boss_decision_4`)[0].disabled = !(internshipX['direct_boss_decision_4']);

            // $(`#hr_decision_1`)[0].disabled = !(internshipX['hr_decision_1']);
            // $(`#hr_decision_2`)[0].disabled = !(internshipX['hr_decision_2']); 
            // $(`#hr_decision_3`)[0].disabled = !(internshipX['hr_decision_3']);
            // $(`#hr_decision_4`)[0].disabled = !(internshipX['hr_decision_4']);
            // $(`#hr_decision_5`)[0].disabled = !(internshipX['hr_decision_5']);


            $(`#direct_boss_decision_3`).datepicker({
                locale: 'vi',
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                daysOfWeekDisabled: [0],
                startDate: direct_boss_decision_3,
            });

            $(`#hr_decision_4`).datepicker({
                locale: 'vi',
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                daysOfWeekDisabled: [0],
                startDate: hr_decision_4,
            });
          
            for (let i = 1; i <= 41; i ++) 
               checkIfInternValidForEval(i);

            calculateAllScores();
        });
    });
</script>
<script>
    $(function() {
        for (let i = 1; i <= 41; i++) {
            s_array = [0,1,2];
            if (i == 27) 
                s_array.push(5);
            s_array.forEach( s => {
                $(`#section_${i}_${s}`).on('change', function() {
                    checkIfInternValidForEval(i);
                    calculateAllScores();
                })
            });

            for (let x = 1; x <= 5; x++) {
                if ($(`#section_${i}_3_score_${x}_check`)[0] != undefined)
                    $(`#section_${i}_3_score_${x}_check`).on('change', function() {
                        calculateInternScore();
                    })

                if ($(`#section_${i}_4_score_${x}_check`)[0] != undefined)
                    $(`#section_${i}_4_score_${x}_check`).on('change', function() {
                        calculateDirectBossScore();
                    })
            }

        }
    });

    $(function() {

        $(`#updateInternship`).on('click', function(e) {
            e.preventDefault();
            $(`#formUpdate`).submit();
        });

        $(`#direct_boss_decision_1_check`).on('change', function() {
            if ($(this)[0].checked) {
                $(`#direct_boss_decision_1`).val(1);
                $(`#direct_boss_decision_2`)[0].disabled = true;
                $(`#direct_boss_decision_2`)[0].value = null;
                $(`#direct_boss_decision_3`)[0].disabled = true;
                $(`#direct_boss_decision_3`)[0].value = null;
            }
        });


        $(`#direct_boss_decision_2_check`).on('change', function() {
            if ($(this)[0].checked) {
                $(`#direct_boss_decision_1`).val(0);
                $(`#direct_boss_decision_2`)[0].disabled = false
                $(`#direct_boss_decision_2`)[0].value = direct_boss_decision_2;
                $(`#direct_boss_decision_3`)[0].disabled = true;
                $(`#direct_boss_decision_3`)[0].value = null;
            }

        });

        $(`#direct_boss_decision_2, #hr_decision_1, #hr_decision_2, #hr_decision_3`).on('change', function() {
            if ($(this)[0].value < 0) 
                $(this)[0].value = null;
        });

        $(`#direct_boss_decision_3_check`).on('change', function() {
            if ($(this)[0].checked) {
                $(`#direct_boss_decision_1`).val(0);
                $(`#direct_boss_decision_2`)[0].disabled =  true;
                $(`#direct_boss_decision_2`)[0].value = null;
                $(`#direct_boss_decision_3`)[0].disabled = false
                $(`#direct_boss_decision_3`)[0].value = direct_boss_decision_3;
            }
        });
        
        $(`#direct_boss_decision_4_check`).on('change', function() {
            $(`#direct_boss_decision_4`)[0].disabled =  !($(this)[0].checked);
        });

        
    });

    $(function() {
        $(`#hr_decision_1_check`).on('change', function() {
            if ($(this)[0].checked)
                $(`#hr_decision_1`)[0].value = hr_decision_1;
            else
                $(`#hr_decision_1`)[0].value = null;
            $(`#hr_decision_1`)[0].disabled =  !($(this)[0].checked);
        });

        $(`#hr_decision_2_check`).on('change', function() {
            if ($(this)[0].checked)
                $(`#hr_decision_2`)[0].value = hr_decision_2;
            else
                $(`#hr_decision_2`)[0].value = null;
            $(`#hr_decision_2`)[0].disabled =  !($(this)[0].checked);
        });

        $(`#hr_decision_3_check`).on('change', function() {
            if ($(this)[0].checked) {
                $(`#hr_decision_4`)[0].disabled  = true;
                $(`#hr_decision_4`)[0].value = null;
                $(`#hr_decision_3`)[0].disabled  = false;
                $(`#hr_decision_3`)[0].value = hr_decision_3;
           }
        });

        $(`#hr_decision_4_check`).on('change', function() {
            if ($(this)[0].checked) {
                $(`#hr_decision_3`)[0].disabled  = true;
                $(`#hr_decision_3`)[0].value = null;
                $(`#hr_decision_4`)[0].disabled  = false;
                $(`#hr_decision_4`)[0].value = hr_decision_4;
           }
        });

        $(`#hr_decision_5_check`).on('change', function() {
            $(`#hr_decision_5`)[0].disabled =  !($(this)[0].checked);
        });
    });

    $(function() {

        $(`#direct_boss_confirm_decline_check`).on('change', function() {
            $(`#direct_boss_decline_reason`)[0].disabled = false
        });

        $(`#direct_boss_confirm_accept_check`).on('change', function() {
            $(`#direct_boss_decline_reason`)[0].disabled = true;
        });


        $(`#room_manager_confirm_decline_check`).on('change', function() {
            $(`#room_manager_decline_reason`)[0].disabled = false
        });

        $(`#room_manager_confirm_accept_check`).on('change', function() {
            $(`#room_manager_decline_reason`)[0].disabled = true;
        });

        $(`#hr_confirm_decline_check`).on('change', function() {
            $(`#hr_decline_reason`)[0].disabled = false;
        });

        $(`#hr_confirm_accept_check`).on('change', function() {
            $(`#hr_decline_reason`)[0].disabled = true;
        });

        $(`#ceo_confirm_decline_check`).on('change', function() {
            $(`#ceo_decline_reason`)[0].disabled = false;
        });

        $(`#ceo_confirm_accept_check`).on('change', function() {
            $(`#ceo_decline_reason`)[0].disabled = true;
        });
    });

</script>


<script>
    function calculateInternScore() {
        let total = 0;
        valid_intern_sec_num_set.forEach( sec_num =>  {
            for (let x = 0; x <= 5; x++) 
                if ($(`#section_${sec_num}_3_score_${x}_check`)[0].checked )
                    total += x;
        });
        
        $(`#intern_score`).val(total);
    }

    function calculateDirectBossScore() {
        $(`#point_check_0`)[0].checked = false;
        $(`#point_check_1`)[0].checked = false;
        $(`#point_check_2`)[0].checked = false;
        $(`#point_check_3`)[0].checked = false;

        if(![1,2,3].includes(internshipX['status']) || (internshipX['status'] == 3 && isUserDirectBoss)) {
            let total = 0;
            valid_intern_sec_num_set.forEach( sec_num =>  {
                for (let x = 0; x <= 5; x++) 
                    if ($(`#section_${sec_num}_4_score_${x}_check`)[0].checked )
                        total += x;
            });
            
            $(`#direct_boss_score`).val(total);

            const percent = total/direct_boss_max_score * 100;

            if (direct_boss_max_score > 0) {
                if (percent >= 50 && percent <= 69) 
                    $(`#point_check_1`)[0].checked = true;
                else if (percent >= 70 && percent <= 89)
                    $(`#point_check_2`)[0].checked = true;
                else if (percent >= 90 && percent <= 100)
                    $(`#point_check_3`)[0].checked = true;
                else {
                    $(`#point_check_0`)[0].checked = true;
                }
            }

            
            if (isUserDirectBoss) {
                const direct_boss_decision_2_checked =  (internshipX['direct_boss_decision_2'] != null);
                const fail = percent < 50;
                $(`#direct_boss_decision_1_check`)[0].checked = fail;
                $(`#direct_boss_decision_2_check`)[0].checked = !fail && (direct_boss_decision_2_checked);
                $(`#direct_boss_decision_3_check`)[0].checked = !fail && (!direct_boss_decision_2_checked);

                $(`#direct_boss_decision_2_check`)[0].disabled = fail;
                $(`#direct_boss_decision_3_check`)[0].disabled = fail;

                $(`#direct_boss_decision_2`)[0].disabled = !$(`#direct_boss_decision_2_check`)[0].checked ;
                $(`#direct_boss_decision_3`)[0].disabled = !$(`#direct_boss_decision_3_check`)[0].checked;

                if (fail) {
                    $(`#direct_boss_decision_1`).val(1);
                    $(`#direct_boss_decision_2`)[0].value = null;
                    $(`#direct_boss_decision_3`)[0].value = null;
                } else {
                    $(`#direct_boss_decision_1`).val(0)
                    if (direct_boss_decision_2_checked) 
                        $(`#direct_boss_decision_2`).val(direct_boss_decision_2);
                    else 
                        $(`#direct_boss_decision_3`).val(direct_boss_decision_3);
                }
            }
        }

    }

    function checkIfInternValidForEval(sec_num) {
        if (![4, 7, 11, 24, 26, 28, 31].includes(sec_num)) {
            total_valid = 0;
            s_array = [0,1,2];

            if (sec_num==27) 
                s_array.push(5);

            s_array.forEach( s => {
                if ($(`#section_${sec_num}_${s}`).val().trim() != '')
                    total_valid ++; 
            });

            if (total_valid == s_array.length)
                valid_intern_sec_num_set.add(sec_num);
            else 
                valid_intern_sec_num_set.delete(sec_num);

            for (let x = 0; x <= 5; x++) {
                if (isUserIntern)
                    $(`#section_${sec_num}_3_score_${x}_check`)[0].disabled = (total_valid != s_array.length);
                if (isUserDirectBoss)
                    $(`#section_${sec_num}_4_score_${x}_check`)[0].disabled = (total_valid != s_array.length);
            }
            
        }                  
    }

    function calculateInternMaxScore() {
        intern_max_score = 0;
        valid_intern_sec_num_set.forEach( x =>  intern_max_score += 5 );
        $(`#intern_max_score_text`).text(intern_max_score);
        $(`#intern_max_score`).val(intern_max_score);

    }

    function calculateDirectBossMaxScore() {
        direct_boss_max_score = 0;
        valid_intern_sec_num_set.forEach( x =>  direct_boss_max_score += 5 );
        $(`#direct_boss_max_score_text`).text(direct_boss_max_score);
        $(`#direct_boss_max_score`).val(direct_boss_max_score);

    }

    function calculateAllScores() {
        calculateInternMaxScore();
        calculateInternScore();
        calculateDirectBossMaxScore();
        calculateDirectBossScore();

    }

</script>
@endsection