@extends('layouts.fe.master2')
@section('title') @lang('translation.starter') @endsection

@section('content')
<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h3 class="d-inline m-0">LỘ TRÌNH THỬ VIỆC</h3>
        @can('internship-create')
        <a href="/internship/create">
            <button type="button" class="btn btn-success btn-label waves-effect waves-light"><i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm</button>
        </a>
        @endcan
    </div>
</div>
<div class="row">
    <div class="card">
        <div class="card-body overflow-auto">
            <table id="internship_table" class="table table-bordered dt-responsive nowrap table-striped align-middle overflow-auto" style="width:101%">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Mã nhân viên</th>
                        <th>Họ tên</th>
                        <th>Phòng</th>
                        <th>Bộ Phận</th>
                        <th>Chức danh</th>
                        <th>Ngày Bắt Đầu</th>
                        <th>Ngày Kết Thúc</th>
                        <th>Người Tạo</th>
                        <th>Trạng thái</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dataInit['allInternships'] as $key => $internshipX)
                    <tr>
                        <td class="dtr-control">{{ $key + 1 }}</td>
                        <td>{{ $internshipX -> intern -> employee_code }}</td>
                        <td>{{ $internshipX -> intern -> full_name }}</td>
                        <td>{{ $internshipX -> intern -> organizational -> room     -> name   }}</td>
                        <td>{{ $internshipX -> intern -> organizational -> team     -> name   }}</td>
                        <td>{{ $internshipX -> intern -> organizational -> position -> name   }}</td>
                        <td>{{ date('Y-m-d', strtotime($internshipX -> start_date))           }}</td>
                        <td>{{ date('Y-m-d', strtotime($internshipX -> end_date))             }}</td>
                        <td>{{ $internshipX -> user -> name                                   }}</td>
                        <td>{{ config('constants.status_confirm.internship')[$internshipX -> status] }} </td>
                        <td>
                            <div class="dropdown d-inline-block">
                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-fill align-middle"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    @can('internship-view')
                                    <li><a href="/internship/view/{{$internshipX -> id}}" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
                                    @endcan
                                    @can('internship-edit')
     
                                    <li><a href="/internship/edit/{{$internshipX -> id}}" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                                    @endcan
                                    @can('internship-delete')
                                    @if(!in_array($internshipX -> status, [4,5,6,7,13]))
                                    <li><a href="#" id="{{ $internshipX -> id }}" class="dropdown-item remove-item-btn"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Delete</a>
                                    </li>
                                    <form method="post" id="internshipDelete{{ $internshipX -> id }}" action="{{  route('internship.delete', $internshipX->id) }}" style="display:inline">
                                        @method('delete')
                                        @csrf
                                    </form>
                                    @endif
                                    @endcan
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
<!--end row-->

@endsection
@section('script')
<script>
    $(document).ready(function() {

        const allInternships = <?php echo json_encode($dataInit['allInternships']); ?>;
        $(function() {
            $('#internship_table').dataTable({
                responsive: false,
            });

            $('#internship_table tbody').on('click', '.remove-item-btn', function(event) {
                const id = $(this)[0].id;
                event.preventDefault();
                swal.fire({
                        title: 'Bạn muốn xóa dữ liệu này ?',
                        text: "Lưu ý : dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                        icon: 'warning',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Đồng ý',
                        showDenyButton: true,
                        denyButtonText: 'Hủy bỏ',
                    })
                    .then((result) => {
                        if (result.isConfirmed) {
                            $(`#internshipDelete${id}`).submit();
                        } else if (result.isDenied) {
                            Swal.fire('Dữ liệu được giữ lại', '', 'info')
                        }
                    });
            })
        });


    });
</script>
@endsection