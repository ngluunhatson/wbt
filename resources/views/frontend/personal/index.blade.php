@extends('layouts.fe.master2')
@section('title')
@lang('translation.personal')
@endsection
@section('css')
@endsection
@section('content')
{{-- @component('components.breadcrumb')
        @slot('li_1')
            Pages
        @endslot
        @slot('title')
            Pricing
        @endslot
    @endcomponent --}}

<div class="row justify-content-between mb-3">
    <h3 style="width: fit-content">DANH SÁCH NHÂN VIÊN</h3>
    @can('personal-create')
    <a type="button" class="btn btn-success btn-label waves-effect waves-light" style="width: fit-content" href={{ route('personalAdd') }}><i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i> Thêm nhân viên</a>
    @endcan
</div>
<!--end row-->

<div class="row">
    <div class="card">
        {{-- <div class="card-header">
                <h5 class="card-title mb-0">Basic Datatables</h5>
            </div> --}}
        <div class="card-body">
            <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                <thead>
                    <tr>
                        <!-- <th scope="col" style="width: 10px;">
                            <div class="form-check">
                                <input class="form-check-input fs-15" type="checkbox" id="checkAll" value="option">
                            </div>
                        </th> -->
                        <th>STT</th>
                        <th>Công Ty</th>
                        <th>Phòng</th>
                        <th>Bộ Phận</th>
                        <th>Chức Danh</th>
                        <th>Họ Và Tên</th>
                        <th>Tên Tiếng Anh</th>
                        <th>Mã Nhân Viên</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $a = 0 ?>
                    @foreach ($persons as $person )
                    <tr>
                        <!-- <th scope="row">
                            <div class="form-check">
                                <input class="form-check-input fs-15" type="checkbox" name="checkbox_1" value="option1">
                            </div>
                        </th> -->
                        <td>{{ ++$a }}</td>
                        <td>{{ $person->company }}</td>
                        @if($person->organizational_special_count <= 1)
                        <td>{{ !empty($person->organizationalSpecial->first()) ? 
                            $person->organizationalSpecial->first()->room->name : 'Chưa có' }}</td>
                        <td>{{ !empty($person->organizationalSpecial->first()) ? 
                            $person->organizationalSpecial->first()->team->name : 'Chưa có' }}</td>
                        <td>{{ !empty($person->organizationalSpecial->first()) ? 
                            $person->organizationalSpecial->first()->position->name : 'Chưa có' }}</td>
                        @else
                        @foreach($person->organizationalSpecial as $key => $personSpecial)
                            @if(!empty($personSpecial->positionSpecial))
                            <td>{{ !empty($person->organizationalSpecial[$key]) ? 
                            $person->organizationalSpecial[$key]->room->name : 'Chưa có' }}</td>
                            <td>{{ !empty($person->organizationalSpecial[$key]) ? 
                                $person->organizationalSpecial[$key]->team->name : 'Chưa có' }}</td>
                            <td>{{ $personSpecial->positionSpecial->name }}</td>      
                            @endif
                        @endforeach
                        @endif
                        <td>{{ $person->full_name }}</td>
                        <td>{{ !empty($person->english_name) ? $person->english_name : 'Chưa có' }}</td>
                        <td>{{ $person->employee_code }}</td>
                        <td>
                            <div class="dropdown d-inline-block">
                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-fill align-middle"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    @can('personal-read-1')
                                        <li><a href="{{ route('personal.view', $person->id) }}" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> Xem</a></li>
                                    @endcan
                                    @if (auth()->user()->hasAnyPermission(['personal-edit']) || $person->id == auth()->user()->basic_info_id)
                                    <li><a href={{ route('editPersonal', $person->id) }} class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Sửa</a></li>
                                    @endif
                                    @can('personal-delete')
                                    <li>
                                        <a href={{ route('deletePersonal', $person->id) }} class="dropdown-item remove-item-btn">
                                            <i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Xóa
                                        </a>
                                    </li>
                                    @endcan
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
@section('script')

<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>

<script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

<script>
    $(function() {
        var myTable = $('#example').dataTable();
    })
</script>
@endsection