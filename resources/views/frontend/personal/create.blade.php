@extends('layouts.fe.master2')
@section('title')
@lang('translation.createPerson')
@endsection
@section('content')
<style>
    .form-label {
        color: #212529 !important;
    }
</style>

<form id="formCreate" action="{{ route('createPersonal') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="card-header d-flex justify-content-between">
            <h3 style="width: fit-content" class="m-0">THÔNG TIN NHÂN VIÊN</h3>
            <button type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content"><i class="ri-save-3-line label-icon  fs-16 "></i> Lưu</button>
        </div>
        <div class="card">
            <div class="card-body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-bs-toggle="tab" href="#basic-info" role="tab" aria-selected="false">
                            </i> Thông tin cơ bản
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#personal-info" role="tab" aria-selected="false">
                            </i> Thông tin cá nhân
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#relative-info" role="tab" aria-selected="false">
                            </i>Thông tin người thân
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#contract-info" role="tab" aria-selected="false">
                            </i>Thông tin HĐLĐ
                        </a>
                    </li>
                </ul>
                <div class="tab-content text-muted">
                    <div class="tab-pane active" id="basic-info" role="tabpanel">
                        <form action="javascript:void(0);">
                            <div class="row ">
                                <div class="card-body p-4 ">
                                    <div class="text-center m-auto" style="width: fit-content">
                                        <div class="profile-user position-relative d-inline-block mx-auto  mb-4">

                                            <img id="output" style="width: 200px; height: 200px" src="{{ asset('uploads/common/user.jpg') }}" class="  rounded-circle avatar-xl img-thumbnail user-profile-image" alt="user-profile-image">

                                            <div class="avatar-xs p-0 rounded-circle profile-photo-edit">
                                                <input id="profile-img-file-input" type="file" class="profile-img-file-input" accept="image/png, image/gif, image/jpeg" name="image_path" onchange="loadFile(event)">
                                                <label for="profile-img-file-input" class="profile-photo-edit avatar-xs">
                                                    <span class="avatar-title rounded-circle bg-light text-body">
                                                        <i class="ri-camera-fill"></i>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        {{-- <h5 class="fs-16 mb-1">Anna Adame</h5>
                                                <p class="text-muted mb-0">Lead Designer / Developer</p> --}}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-3">
                                        <label for="company" class="form-label">Công ty</label>
                                        <input rule type="text" class="form-control @error('company') is-invalid @enderror" placeholder="(Điền thông tin)" id="company" name="company" value="{{ old('company') }}">
                                        @error('company')
                                        <small class="help-block text-danger">{{ $message }} *</small>
                                        @enderror
                                    </div>
                                </div>
                                <!--end col Công ty-->
                                <!-- <div class="col-6">
                                        <div class="mb-3">
                                            <label for="room" class="form-label">Phòng</label>

                                            <div class="col-xl-12">
                                                <select class="form-select" id="selectRoom" aria-label="Default select example" name="room">
                                                    <option value="0">Vui lòng chọn phòng ban</option>
                                                    @foreach($dataInit['rooms'] as $room)
                                                    <option value="{{ $room['id'] }}">{{ $room['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div> -->
                                <!--end col Phòng-->
                                <!-- <div class="col-6">
                                        <div class="mb-3">
                                            <label for="part" class="form-label">Bộ phận</label>

                                            <div class="col-xl-12">
                                                <select class="form-select" aria-label="Default select example" id="selectTeam" name="team">
                                                </select>
                                            </div>
                                        </div>
                                    </div> -->
                                <!--end col Bộ phận-->
                                <!-- <div class="col-6">
                                        <div class="mb-3">
                                            <label for="position" class="form-label">Chức danh</label>

                                            <div class="col-xl-12">
                                                <select class="form-select" aria-label="Default select example" id="selectPosition" name="position">
                                                </select>
                                            </div>
                                        </div>
                                    </div> -->
                                <!--end col Chức danh-->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="rank" class="form-label">Cấp bậc</label>

                                        <div class="col-xl-12">
                                            <select class="form-select @error('rank') is-invalid @enderror" aria-label="Default select example" name="rank">
                                                <option value="">Vui lòng chọn cấp bậc</option>
                                                @foreach(config('constants.rank') as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                            @error('rank')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <!--end col Cấp bậc-->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="full_name" class="form-label">Họ và tên</label>
                                        <input rile type="text" class="form-control @error('full_name') is-invalid @enderror" placeholder="(Điền thông tin)" id="full_name" name="full_name" value="{{ old('full_name') }}">
                                        @error('full_name')
                                        <small class="help-block text-danger">{{ $message }} *</small>
                                        @enderror
                                    </div>
                                </div>
                                <!--end col Họ và tên-->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="name_eng" class="form-label">Tên Tiếng Anh</label>
                                        <input rule type="text" class="form-control @error('name_eng') is-invalid @enderror" placeholder="(Điền thông tin)" id="name_eng" name="name_eng" value="{{ old('name_eng') }}">
                                        @error('name_eng')
                                        <small class="help-block text-danger">{{ $message }} *</small>
                                        @enderror
                                    </div>
                                </div>
                                <!--end col Tên Tiếng Anh-->
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="code_staff" class="form-label">Mã nhân viên</label>
                                        <input rule type="text" class="form-control @error('code_staff') is-invalid @enderror" placeholder="(Điền thông tin)" id="code_staff" name="code_staff" value="{{ old('code_staff') }}">
                                        @error('code_staff')
                                        <small class="help-block text-danger">{{ $message }} *</small>
                                        @enderror
                                    </div>
                                </div>
                                <!--end col Mã nhân viên-->
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="date_work" class="form-label">Ngày vào làm</label>
                                        <input rule type="date" class="form-control @error('date_work') is-invalid @enderror" id="date_work" name="date_work" value="{{ old('date_work') }}">
                                        @error('date_work')
                                        <small class="help-block text-danger">{{ $message }} *</small>
                                        @enderror
                                    </div>
                                </div>
                                <!--end col Ngày vào làm-->
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="birth_date" class="form-label">Ngày sinh</label>
                                        <input rule type="date" class="form-control @error('birth_date') is-invalid @enderror" id="birth_date" name="birth_date" value="{{ old('birth_date') }}">
                                        @error('birth_date')
                                        <small class="help-block text-danger">{{ $message }} *</small>
                                        @enderror
                                    </div>
                                </div>
                                <!--end col Ngày sinh-->
                                <div class="col-3">
                                    <div class="mb-3">
                                        <label for="genders" class="form-label">Giới tính</label>
                                        <!-- Base Radios -->
                                        <div class="d-flex mt-2">
                                            <div class="col-xl-6 form-check mb-2">
                                                <input class="form-check-input" type="radio" checked name="genders" id="male" value="1">
                                                <label class="form-check-label" for="male">
                                                    Nam
                                                </label>
                                            </div>
                                            <div class="col-xl-6 form-check">
                                                <input class="form-check-input" type="radio" name="genders" id="female" value="0">
                                                <label class="form-check-label" for="female">
                                                    Nữ
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--end col Giới tính-->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="nation" class="form-label">Quốc tịch</label>
                                        <div class="col-xl-12">
                                            <select class="form-select" aria-label="Default select example" name="nation">
                                                @foreach(config('constants.national') as $key => $nation)
                                                <option @selected($key == '192') value="{{ $key }}">{{ $nation }}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <!--end col Quốc tịch-->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="phone" class="form-label">Số điện thoại</label>
                                        <input rule type="number" class="form-control @error('phone') is-invalid @enderror" placeholder="(Điền thông tin)" id="phone" name="phone" value="{{ old('phone') }}">
                                        @error('phone')
                                        <small class="help-block text-danger">{{ $message }} *</small>
                                        @enderror
                                    </div>
                                </div>
                                <!--end col Số điện thoại-->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="email" class="form-label">Email cá nhân</label>
                                        <input rule type="email" class="form-control @error('email') is-invalid @enderror" placeholder="(Điền thông tin)" id="email" name="email" value="{{ old('email') }}">
                                        @error('email')
                                        <small class="help-block text-danger">{{ $message }} *</small>
                                        @enderror
                                    </div>
                                </div>
                                <!--end col Email cá nhân-->
                                <div class="col-6">
                                    <div class="mb-3">
                                        <label for="email_company" class="form-label">Email Công ty cấp</label>
                                        <input rule type="email" class="form-control @error('email_company') is-invalid @enderror" placeholder="(Điền thông tin)" id="email_company" name="email_company" value="{{ old('email_company') }}">
                                        @error('email_company')
                                        <small class="help-block text-danger">{{ $message }} *</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-6"> </div>
                                <!--end col Email Công ty cấp-->
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="file_cv" class="form-label">CV</label>
                                        <div class="card-body">
                                            <input rule type="file" class="filepond filepond-input-multiple @error('file_cv') is-invalid @enderror" multiple name="file_cv" data-allow-reorder="true" accept=".xlsx,.xls, .doc, .docx,.ppt, .pptx,.txt,.pdf" data-max-file-size="3MB" data-max-files="3" value="{{ old('file_cv') }}">
                                            @error('file_cv')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                            
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="file_disc" class="form-label">DISC</label>
                                        <div class="card-body">
                                            <input rule type="file" class="filepond filepond-input-multiple @error('file_disc') is-invalid @enderror" multiple name="file_disc" data-allow-reorder="true" accept=".xlsx,.xls, .doc, .docx,.ppt, .pptx,.txt,.pdf" data-max-file-size="3MB" data-max-files="3" value="{{ old('file_disc') }}">
                                            @error('file_disc')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <!--end col DISC-->
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label for="file_motivator" class="form-label">Motivator</label>
                                        <div class="card-body">
                                            <input rule type="file" class="filepond filepond-input-multiple @error('file_motivator') is-invalid @enderror" multiple name="file_motivator" data-allow-reorder="true" accept=".xlsx,.xls, .doc, .docx,.ppt, .pptx,.txt,.pdf" data-max-file-size="3MB" data-max-files="3" value="{{ old('file_motivator') }}">
                                            @error('file_motivator')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <!--end col Motivator-->

                            </div>
                            <!--end row-->
                        </form>
                    </div>
                    <div class="tab-pane" id="personal-info" role="tabpanel">
                        <div class="row">
                            <div class="col-12">
                                <div class="col-3 mb-3">
                                    <label for="ethnic" class="form-label">Dân tộc</label>
                                    <div class="col-xl-12 pe-3">
                                        <select class="form-select" aria-label="Default select example" name="ethnic">
                                            @foreach(config('constants.ethnic') as $key => $ethnic)
                                            <option value="{{ $key }}">{{ $ethnic }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--end col DÂN TỘC-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="passport" class="form-label">CCCD/Passport</label>
                                    <input rule type="text" class="form-control @error('passport') is-invalid @enderror" placeholder="(Điền thông tin) " id="passport" name="passport" value="{{ old('passport') }}">
                                    @error('passport')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col CCCD/Passport-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="issue_date" class="form-label">Ngày cấp</label>
                                    <input type="date" class="form-control @error('issue_date') is-invalid @enderror" id="issue_date" name="issue_date" value="{{ old('issue_date') }}">
                                    @error('issue_date')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Ngày cấp-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="providers" class="form-label ">Nơi cấp</label>
                                    <input rule type="text" class="form-control @error('providers') is-invalid @enderror" placeholder="(Điền thông tin)" id="providers" name="providers" value="{{ old('providers') }}">
                                    @error('providers')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Nơi cấp-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="permanent_address" class="form-label">Địa chỉ thường trú</label>
                                    <input rule type="text" class="form-control @error('permanent_address') is-invalid @enderror" placeholder="(Điền thông tin)" id="permanent_address" name="permanent_address" value="{{ old('permanent_address') }}">
                                    @error('permanent_address')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Địa chỉ thường trú-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="temporary_address" class="form-label">Địa chỉ tạm trú</label>
                                    <input rule type="text" class="form-control @error('temporary_address') is-invalid @enderror" placeholder="(Điền thông tin)" id="temporary_address" name="temporary_address" value="{{ old('temporary_address') }}">
                                    @error('temporary_address')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Địa chỉ tạm trú-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="insurance_code" class="form-label">Mã BHXH</label>
                                    <input rule type="number" class="form-control @error('insurance_code') is-invalid @enderror" placeholder="(Điền chuỗi số)" id="insurance_code" name="insurance_code" value="{{ old('insurance_code') }}">
                                    @error('insurance_code')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Mã BHXH-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="medical_code" class="form-label">Mã BHYT</label>
                                    <input type="number" class="form-control @error('medical_code') is-invalid @enderror" placeholder="(Điền chuỗi số)" id="medical_code" name="medical_code" value="{{ old('medical_code') }}">
                                    @error('medical_code')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Mã BHYT-->
                            <div class="col-12">
                                <div class="mb-3">
                                    <label for="address_kcb" class="form-label">Nơi KCB</label>
                                    <input type="text" class="form-control @error('address_kcb') is-invalid @enderror" placeholder="(Điền thông tin bệnh viện)" id="address_kcb" name="address_kcb" value="{{ old('address_kcb') }}">
                                    @error('address_kcb')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Nơi KCB-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="tax" class="form-label">Thuế TNCN</label>
                                    <input type="number" class="form-control @error('tax') is-invalid @enderror" placeholder="(Điền chuỗi số)" id="tax" name="tax" value="{{ old('tax') }}">
                                    @error('tax')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Thuế TNCN-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="dependent_person" class="form-label">Người phụ thuộc</label>
                                    <input type="number" class="form-control @error('dependent_person') is-invalid @enderror" placeholder="(Điền số lượng)" id="dependent_person" name="dependent_person" value="{{ old('dependent_person') }}">
                                    @error('dependent_person')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Người phụ thuộc-->
                            <div class="col-12">
                                <div class="mb-3">
                                    <label for="npt" class="form-label">Chi tiết NPT</label>
                                    <input type="text" class="form-control @error('npt') is-invalid @enderror" placeholder="(Điền họ tên - mối quan hệ) mỗi người cách nhau bằng dấu ;" id="npt" name="npt" value="{{ old('npt') }}">
                                    @error('npt')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Chi tiết NPT-->
                            <div class="col-12">
                                <div class="col-3 mb-3">
                                    <label for="level" class="form-label">Trình độ học vấn</label>

                                    <div class="col-xl-11">
                                        <select class="form-select" aria-label="Default select example" name="level">
                                            @foreach(config('constants.literacy') as $key => $literacy)
                                            <option value="{{ $key }}">{{ $literacy }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--end col Trình độ học vấn-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="school" class="form-label">Trường</label>
                                    <input type="text" class="form-control @error('school') is-invalid @enderror" placeholder="(Điền thông tin)" id="school" name="school" value="{{ old('school') }}">
                                    @error('school')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Trường-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="specialized" class="form-label">Chuyên ngành</label>
                                    <input type="text" class="form-control @error('specialized') is-invalid @enderror" placeholder="(Điền thông tin)" id="specialized" name="specialized" value="{{ old('specialized') }}">
                                    @error('specialized')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Chuyên ngành-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="tknh" class="form-label">Số TKNH</label>
                                    <input type="number" class="form-control @error('tknh') is-invalid @enderror" placeholder="(Điền chuỗi số)" id="tknh" name="tknh" value="{{ old('tknh') }}">
                                    @error('tknh')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Số TKNH-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="bank" class="form-label">Tên ngân hàng</label>
                                    <input type="text" class="form-control @error('bank') is-invalid @enderror" placeholder="(Điền thông tin)" id="bank" name="bank" value="{{ old('bank') }}">
                                    @error('bank')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Tên ngân hàng-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="license_plates" class="form-label">Biển số xe</label>
                                    <input type="text" class="form-control @error('license_plates') is-invalid @enderror" placeholder="(Điền chuỗi mã)" id="license_plates" name="license_plates" value="{{ old('license_plates') }}">
                                    @error('license_plates')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Số Biển số xe-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="carriage_category" class="form-label">Loại xe</label>
                                    <input type="text" class="form-control @error('carriage_category') is-invalid @enderror" placeholder="(Điền thông tin)" id="carriage_category" name="carriage_category" value="{{ old('carriage_category') }}">
                                    @error('carriage_category')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Tên Loại xe-->

                        </div>
                        <!--end row-->
                    </div>
                    <div class="tab-pane" id="relative-info" role="tabpanel">
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="prioritized_1" class="form-label">Họ và tên ưu tiên 1</label>
                                    <input rule type="text" class="form-control @error('prioritized_1') is-invalid @enderror" placeholder="(Điền thông tin)" id="prioritized_1" name="prioritized_1" value="{{ old('prioritized_1') }}">
                                    @error('prioritized_1')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Họ và tên ưu tiên 1-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="relationship_prioritized_1" class="form-label">Mối quan hệ</label>
                                    <div class="col-xl-12">
                                        <select class="form-select" aria-label="Default select example" name="relationship_prioritized_1">
                                            @foreach (config('constants.relations') as $index => $relation)
                                            <option value="{{ $index }}"> {{ $relation }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--end col Mối quan hệ 1-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="phone_prioritized_1" class="form-label">Số điện thoại</label>
                                    <input rule type="tel" class="form-control @error('phone_prioritized_1') is-invalid @enderror" placeholder="(Điền chuỗi số)" id="phone_prioritized_1" name="phone_prioritized_1" value="{{ old('phone_prioritized_1') }}">
                                    @error('phone_prioritized_1')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Số điện thoại-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="prioritized_2" class="form-label">Họ và tên ưu tiên 2</label>
                                    <input rule type="text" class="form-control @error('prioritized_2') is-invalid @enderror" placeholder="(Điền thông tin)" id="prioritized_2" name="prioritized_2" value="{{ old('prioritized_2') }}">
                                    @error('prioritized_2')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Họ và tên ưu tiên 2-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="relationship_prioritized_2" class="form-label">Mối quan hệ</label>
                                    <div class="col-xl-12">
                                        <select class="form-select" aria-label="Default select example" name="relationship_prioritized_2">
                                            @foreach (config('constants.relations') as $index => $relation)
                                            <option value="{{ $index }}"> {{ $relation }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--end col Mối quan hệ 2-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="phone_prioritized_2" class="form-label">Số điện thoại</label>
                                    <input rule type="tel" class="form-control @error('phone_prioritized_2') is-invalid @enderror" placeholder="(Điền chuỗi số)" id="phone_prioritized_2" name="phone_prioritized_2" value="{{ old('phone_prioritized_2') }}">
                                    @error('phone_prioritized_2')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Số điện thoại-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="child_1" class="form-label">Họ và tên con 1</label>
                                    <input rule type="text" class="form-control @error('child_1') is-invalid @enderror" placeholder="(Điền thông tin)" id="child_1" name="child_1" value="{{ old('child_1') }}">
                                    @error('child_1')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Họ và tên con 1-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="gender_child_1" class="form-label">Giới tính</label>
                                    <!-- Base Radios -->
                                    <div class="d-flex mt-2">
                                        <div class="col-xl-6 form-check mb-2">
                                            <input class="form-check-input" type="radio" name="gender_child_1" id="male_child_1" value="1">
                                            <label class="form-check-label" for="male_child_1">
                                                Nam
                                            </label>
                                        </div>
                                        <div class="col-xl-6 form-check">
                                            <input class="form-check-input" type="radio" name="gender_child_1" id="female_child_1" value="0">
                                            <label class="form-check-label" for="female_child_1">
                                                Nữ
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end col Giới tính-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="birth_date_child_1" class="form-label">Ngày sinh</label>
                                    <input rule type="date" class="form-control @error('birth_date_child_1') is-invalid @enderror" id="birth_date_child_1" name="birth_date_child_1" value="{{ old('birth_date_child_1') }}">
                                    @error('birth_date_child_1')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Ngày sinh-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="child_2" class="form-label">Họ và tên con 2</label>
                                    <input rule type="text" class="form-control @error('child_2') is-invalid @enderror" placeholder="(Điền thông tin)" id="child_2" name="child_2" value="{{ old('child_2') }}">
                                    @error('child_2')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Họ và tên con 2-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="gender_child_2" class="form-label">Giới tính</label>
                                    <!-- Base Radios -->
                                    <div class="d-flex mt-2">
                                        <div class="col-xl-6 form-check mb-2">
                                            <input class="form-check-input" type="radio" name="gender_child_2" id="male_child_2" value="1">
                                            <label class="form-check-label" for="male_child_2">
                                                Nam
                                            </label>
                                        </div>
                                        <div class="col-xl-6 form-check">
                                            <input class="form-check-input" type="radio" name="gender_child_2" id="female_child_2" value="0">
                                            <label class="form-check-label" for="female_child_2">
                                                Nữ
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!--end col Giới tính-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="birth_date_child_2" class="form-label">Ngày sinh</label>
                                    <input rule type="date" class="form-control @error('birth_date_child_2') is-invalid @enderror" id="birth_date_child_2" name="birth_date_child_2" value="{{ old('birth_date_child_2') }}">
                                    @error('birth_date_child_2')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Ngày sinh-->
                            <div class="col-6">
                                <div class="mb-3">
                                    <label for="child_3" class="form-label">Họ và tên con 3</label>
                                    <input rule type="text" class="form-control @error('child_3') is-invalid @enderror" placeholder="(Điền thông tin)" id="child_3" name="child_3" value="{{ old('child_3') }}">
                                    @error('child_3')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Họ và tên con 3-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="gender_child_3" class="form-label">Giới tính</label>
                                    <!-- Base Radios -->
                                    <div class="d-flex mt-2">
                                        <div class="col-xl-6 form-check mb-2">
                                            <input class="form-check-input" type="radio" name="gender_child_3" id="male_child_3" value="1">
                                            <label class="form-check-label" for="male_child_3">
                                                Nam
                                            </label>
                                        </div>
                                        <div class="col-xl-6 form-check">
                                            <input class="form-check-input" type="radio" name="gender_child_3" id="female_child_3" value="0">
                                            <label class="form-check-label" for="female_child_3">
                                                Nữ
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!--end col Giới tính-->
                            <div class="col-3">
                                <div class="mb-3">
                                    <label for="birth_date_child_3" class="form-label">Ngày sinh</label>
                                    <input rule type="date" class="form-control @error('birth_date_child_3') is-invalid @enderror" id="birth_date_child_3" name="birth_date_child_3" value="{{ old('birth_date_child_3') }}">
                                    @error('birth_date_child_3')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                            </div>
                            <!--end col Ngày sinh-->
                        </div>
                    </div>
                    <div class="tab-pane" id="contract-info" role="tabpanel">
                        <div class="row">
                            <?php $i = 1 ?>
                            @for ($i; $i<4; $i++) <div class="col-3">
                                <div class="mb-3">
                                    <label for="number_hdld_{{ $i }}" class="form-label">Số HĐLĐ lần {{ $i }}</label>
                                    <input type="text" class="form-control  @error('number_hdld_{{ $i }}') is-invalid @enderror" placeholder="(Điền thông tin)" id="number_hdld_{{ $i }}" name="number_hdld_{{ $i }}" value="{{ old('number_hdld_'.$i) }}">
                                    @error('number_hdld_{{ $i }}')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                        </div>
                        <!--end col Số  HĐLĐ lần 1-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="contract_{{ $i }}" class="form-label">Loại hợp đồng</label>
                                <div class="col-xl-12">
                                    <select class="form-select" aria-label="Default select example" name="contract_{{ $i }}">
                                        @foreach(config('constants.contract') as $key => $contract)
                                        <option value="{{ $contract }}">{{ $contract }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--end col Loại hợp đồng-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="date_form_{{ $i }}" class="form-label">Từ ngày</label>
                                <input type="date" class="form-control @error('date_form_{{ $i }}') is-invalid @enderror" id="date_form_{{ $i }}" value="{{ old('date_form_'.$i) }}" name="date_form_{{ $i }}">
                                @error('date_form_{{ $i }}')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Từ ngày-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="to_form_{{ $i }}" class="form-label">Đến ngày</label>
                                <input type="date" class="form-control @error('to_form_{{ $i }}') is-invalid @enderror" id="to_form_{{ $i }}" name="to_form_{{ $i }}" value="{{ old('to_form_'.$i) }}">
                                @error('to_form_{{ $i }}')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Đến ngày-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="wage_hdld_{{ $i }}" class="form-label">Lương HĐLĐ</label>
                                <input type="number" class="form-control @error('wage_hdld_{{ $i }}') is-invalid @enderror" placeholder="(Điền số tiền VND)" id="wage_hdld_{{ $i }}" name="wage_hdld_{{ $i }}" value="{{ old('wage_hdld_'.$i) }}">
                                @error('wage_hdld_{{ $i }}')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Lương HĐLĐ-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="number_plhd_{{ $i }}" class="form-label">Số PLHĐ</label>
                                <input type="text" class="form-control @error('number_plhd_{{ $i }}') is-invalid @enderror" placeholder="(Điền chuỗi mã)" value="{{ old('number_plhd_'.$i) }}" id="number_plhd_{{ $i }}" name="number_plhd_{{ $i }}">
                                @error('number_plhd_{{ $i }}')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Số  PLHĐ-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="date_form_plhd_{{ $i }}" class="form-label">Từ ngày</label>
                                <input type="date" class="form-control @error('date_form_plhd_{{ $i }}') is-invalid @enderror" id="date_form_plhd_{{ $i }}" value="{{ old('date_form_plhd_'.$i) }}" name="date_form_plhd_{{ $i }}">
                                @error('date_form_plhd_{{ $i }}')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Từ ngày-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="wage_plhd_{{ $i }}" class="form-label">Lương PLHĐ</label>
                                <input type="number" class="form-control @error('wage_plhd_{{ $i }}') is-invalid @enderror" placeholder="(Điền số tiền VND)" value="{{ old('wage_plhd_'.$i) }}" id="wage_plhd_{{ $i }}" name="wage_plhd_{{ $i }}">
                                @error('wage_plhd_{{ $i }}')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Lương PLHĐ-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="wage_kpi_{{ $i }}" class="form-label">Lương KPIs</label>
                                <input type="number" class="form-control @error('wage_kpi_{{ $i }}') is-invalid @enderror" placeholder="(Điền số tiền VND)" value="{{ old('wage_kpi_'.$i) }}" id="wage_kpi_{{ $i }}" name="wage_kpi_{{ $i }}">
                                @error('wage_kpi_{{ $i }}')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Lương KPIs-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="diff_{{ $i }}" class="form-label">Chính sách khác</label>
                                <input type="text" class="form-control @error('diff_{{ $i }}') is-invalid @enderror" placeholder="(Điền thông tin)" value="{{ old('diff_'.$i) }}" id="diff_{{ $i }}" name="diff_{{ $i }}">
                                @error('diff_{{ $i }}')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Chính sách khác-->
                        @endfor
                        {{-- <div class="col-3">
                                            <div class="mb-3">
                                                <label for="number_hdld_2" class="form-label">Số HĐLĐ lần 2</label>
                                                <input type="text" class="form-control" placeholder="(Điền thông tin)"
                                                    id="number_hdld_2" name="number_hdld_2">
                                            </div>
                                        </div>
                                        <!--end col Số  HĐLĐ lần 2-->
                                        < class="col-3">
                                            <div class="mb-3">
                                                <label for="contract_2" class="form-label">Loại hợp đồng</label>
                                                <div class="col-xl-12">
                                                    <select class="form-select" aria-label="Default select example">
                                                    @foreach(config('constants.contract') as $key => $contract)
                                                    <option value="{{ $key }}">{{ $contract }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>

                <!--end col Loại hợp đồng-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="date_form_2" class="form-label">Từ ngày</label>
                        <input type="date" class="form-control" id="date_form_2" name="date_form_2">
                    </div>
                </div>
                <!--end col Từ ngày-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="to_form_2" class="form-label">Đến ngày</label>
                        <input type="date" class="form-control" id="to_form_2" name="to_form_2">
                    </div>
                </div>
                <!--end col Đến ngày-->
                <div class="col-6">
                    <div class="mb-3">
                        <label for="wage_hdld_2" class="form-label">Lương HĐLĐ</label>
                        <input type="number" class="form-control" placeholder="(Điền số tiền VND)" id="wage_hdld_2" name="wage_hdld_2">
                    </div>
                </div>
                <!--end col Lương HĐLĐ-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="number_plhd_2" class="form-label">Số PLHĐ</label>
                        <input type="text" class="form-control" placeholder="(Điền chuỗi mã)" id="number_plhd_2" name="number_plhd_2">
                    </div>
                </div>
                <!--end col Số  PLHĐ-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="date_form_plhd_2" class="form-label">Từ ngày</label>
                        <input type="date" class="form-control" id="date_form_plhd_2" name="date_form_plhd_2">
                    </div>
                </div>
                <!--end col Từ ngày-->
                <div class="col-6">
                    <div class="mb-3">
                        <label for="wage_plhd_2" class="form-label">Lương PLHĐ</label>
                        <input type="number" class="form-control" placeholder="(Điền số tiền VND)" id="wage_plhd_2" name="wage_plhd_2">
                    </div>
                </div>
                <!--end col Lương PLHĐ-->
                <div class="col-6">
                    <div class="mb-3">
                        <label for="wage_kpi_2" class="form-label">Lương KPIs</label>
                        <input type="number" class="form-control" placeholder="(Điền số tiền VND)" id="wage_kpi_2" name="wage_kpi_2">
                    </div>
                </div>
                <!--end col Lương KPIs-->
                <div class="col-12">
                    <div class="mb-3">
                        <label for="diff_2" class="form-label">Chính sách khác</label>
                        <input type="text" class="form-control" placeholder="(Điền thông tin)" id="diff_2" name="diff_2">
                    </div>
                </div>
                <!--end col Chính sách khác-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="number_hdld_3" class="form-label">Số HĐLĐ lần 3</label>
                        <input type="text" class="form-control" placeholder="(Điền thông tin)" id="number_hdld_3" name="number_hdld_3">
                    </div>
                </div>
                <!--end col Số  HĐLĐ lần 3-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="number_hdld_3" class="form-label">Loại hợp đồng</label>
                        <div class="col-xl-12">
                            <select class="form-select" aria-label="Default select example">
                                @foreach(config('constants.contract') as $key => $contract)
                                <option value="{{ $key }}">{{ $contract }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <!--end col Loại hợp đồng-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="date_form_3" class="form-label">Từ ngày</label>
                        <input type="date" class="form-control" id="date_form_3" name="date_form_3">
                    </div>
                </div>
                <!--end col Từ ngày-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="to_form_3" class="form-label">Đến ngày</label>
                        <input type="date" class="form-control" id="to_form_3" name="to_form_3">
                    </div>
                </div>
                <!--end col Đến ngày-->
                <div class="col-6">
                    <div class="mb-3">
                        <label for="wage_hdld_3" class="form-label">Lương HĐLĐ</label>
                        <input type="number" class="form-control" placeholder="(Điền số tiền VND)" id="wage_hdld_3" name="wage_hdld_3">
                    </div>
                </div>
                <!--end col Lương HĐLĐ-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="number_plhd_3" class="form-label">Số PLHĐ</label>
                        <input type="text" class="form-control" placeholder="(Điền chuỗi mã)" id="number_plhd_3" name="number_plhd_3">
                    </div>
                </div>
                <!--end col Số  PLHĐ-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="date_form_plhd_3" class="form-label">Từ ngày</label>
                        <input type="date" class="form-control" id="date_form_plhd_3" name="date_form_plhd_3">
                    </div>
                </div>
                <!--end col Từ ngày-->
                <div class="col-6">
                    <div class="mb-3">
                        <label for="wage_plhd_3" class="form-label">Lương PLHĐ</label>
                        <input type="number" class="form-control" placeholder="(Điền số tiền VND)" id="wage_plhd_3" name="wage_plhd_3">
                    </div>
                </div>
                <!--end col Lương PLHĐ-->
                <div class="col-6">
                    <div class="mb-3">
                        <label for="wage_kpi_3" class="form-label">Lương KPIs</label>
                        <input type="number" class="form-control" placeholder="(Điền số tiền VND)" id="wage_kpi_3" name="wage_kpi_3">
                    </div>
                </div>
                <!--end col Lương KPIs-->
                <div class="col-12">
                    <div class="mb-3">
                        <label for="diff_3" class="form-label">Chính sách khác</label>
                        <input type="text" class="form-control" placeholder="(Điền thông tin)" id="diff_3" name="diff_3">
                    </div>
                </div>
                <!--end col Chính sách khác--> --}}
                <div class="col-3">
                    <div class="mb-3">
                        <label for="number_bbtlhd" class="form-label">Số BBTLHĐ</label>
                        <input type="text" class="form-control @error('number_bbtlhd') is-invalid @enderror" placeholder="(Điền chuỗi mã)" id="number_bbtlhd" name="number_bbtlhd" value="{{ old('number_bbtlhd') }}">
                        @error('number_bbtlhd')
                        <small class="help-block text-danger">{{ $message }} *</small>
                        @enderror
                    </div>
                </div>
                <!--end col Số  BBTLHĐ-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="date_form_bbtlhd" class="form-label">Từ ngày</label>
                        <input type="date" class="form-control @error('date_form_bbtlhd') is-invalid @enderror" id="date_form_bbtlhd" name="date_form_bbtlhd" value="{{ old('date_form_bbtlhd') }}">
                        @error('date_form_bbtlhd')
                        <small class="help-block text-danger">{{ $message }} *</small>
                        @enderror
                    </div>
                </div>
                <!--end col Từ ngày-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="number_qdtv" class="form-label">Số QĐTV</label>
                        <input type="text" class="form-control @error('number_qdtv') is-invalid @enderror" placeholder="(Điền chuỗi mã)" id="number_qdtv" name="number_qdtv" value="{{ old('number_qdtv') }}">
                        @error('number_qdtv')
                        <small class="help-block text-danger">{{ $message }} *</small>
                        @enderror
                    </div>
                </div>
                <!--end col Số QĐTV-->
                <div class="col-3">
                    <div class="mb-3">
                        <label for="date_form_qdtv" class="form-label">Từ ngày</label>
                        <input type="date" class="form-control @error('date_form_qdtv') is-invalid @enderror" id="date_form_qdtv" name="date_form_qdtv" value="{{ old('date_form_qdtv') }}">
                        @error('date_form_qdtv')
                        <small class="help-block text-danger">{{ $message }} *</small>
                        @enderror
                    </div>
                </div>
                <!--end col Từ ngày-->
            </div>
        </div>
    </div><!-- end card-body -->
    </div>
</form>

@endsection
@section('script')
<script src="{{ URL::asset('assets/js/pages/pricing.init.js') }}"></script>

<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>


<!-- listjs init -->
<script src="{{ URL::asset('assets/js/pages/listjs.init.js') }}"></script>

<script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<script>
    var loadFile = function(event) {
        var image_path = document.getElementById('output');
        image_path.src = URL.createObjectURL(event.target.files[0]);
    }
    $(document).ready(function() {
        if ($('.is-invalid').length > 0) {
            $('#accor_borderedExamplecollapse1').collapse()
        }
        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            var url = "{!! route('helper.getTeamByRommID', ':id') !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                        $('#selectTeam').append($('<option>', {
                            value: 0,
                            text: 'Tất cả phòng ban'
                        }));
                        data.forEach(function(value) {
                            $('#selectTeam').append($('<option>', {
                                value: value.id,
                                text: value.name
                            }));
                        })
                    } else {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                    }
                }
            });
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            var url = "{!! route('helper.getPositionByTeamID', ':id') !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectPosition')
                            .find('option')
                            .remove()
                        $('#selectPosition').append($('<option>', {
                            value: 0,
                            text: 'Tất cả chức danh'
                        }));
                        data.forEach(function(value) {
                            $('#selectPosition').append($('<option>', {
                                value: value.id,
                                text: value.name
                            }));
                        })
                    } else {
                        $('#selectPosition')
                            .find('option')
                            .remove()
                    }
                }
            });
        });

        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();

            var url = "{!! route('helper.getStaffByConditons', [':room_id', ':team_id', ':positon_id']) !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(':positon_id', position_id),
                success: function(data) {
                    if (data.length > 0) {
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectStaff').append($('<option>', {
                            value: 0,
                            text: 'Tất cả nhân viên'
                        }));
                        data.forEach(function(value) {
                            $('#selectStaff').append($('<option>', {
                                value: value.id,
                                text: value.full_name
                            }));
                        })
                    } else {
                        $('#selectStaff')
                            .find('option')
                            .remove()
                    }
                }
            });
        });
    })
</script>
@endsection