@extends('layouts.fe.master2')
@section('title')
@lang('translation.pricing')
@endsection
@section('content')
<style>
    .form-label {
        color: #212529 !important;
    }
</style>
<!-- TAB -->

<div class="row">
    <div class="card-header d-flex justify-content-between">
        <h3 style="width: fit-content" class="m-0">THÔNG TIN NHÂN VIÊN</h3>
        <a href={{ route('editPersonal', $person->id) }}><button type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content; background-color: #0ab39c;"><i class="ri-save-3-line label-icon  fs-16 "></i> Sửa</button>
        </a>
    </div>
    <div class="card">
        <div class="card-body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-justified nav-border-top nav-border-top-info mb-3" role="tablist">
                @can('personal-read-1')
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#basic-info" role="tab" aria-selected="false">
                        </i> Thông tin cơ bản
                    </a>
                </li>
                @endcan
                @if (auth()->user()->hasAnyPermission(['personal-read-2-3-4']) || $basic->id == auth()->user()->basic_info_id)
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#personal-info" role="tab" aria-selected="false">
                        </i> Thông tin cá nhân
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#relative-info" role="tab" aria-selected="false">
                        </i>Thông tin người thân
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#contract-info" role="tab" aria-selected="false">
                        </i>Thông tin HĐLĐ
                    </a>
                </li>
                @endif
            </ul>
            <div class="tab-content text-muted">
                <div class="tab-pane active" id="basic-info" role="tabpanel">
                    <div class="row ">
                        <div class="card-body p-4 ">
                            <div class="text-center m-auto" style="width: fit-content">
                                <div class="profile-user position-relative d-inline-block mx-auto  mb-4">
                                    @if ($basic->avatar)
                                    <img id="output" style="width: 200px; height: 200px" src="{{ URL::asset($basic->avatar) }}" class="  rounded-circle avatar-xl img-thumbnail user-profile-image" alt="user-profile-image">
                                    @else
                                    <img id="output" style="width: 200px; height: 200px" src="{{ asset('uploads/common/user.jpg') }}" class="  rounded-circle avatar-xl img-thumbnail user-profile-image" alt="user-profile-image">
                                    @endif

                                    <div class="avatar-xs p-0 rounded-circle profile-photo-edit">
                                        <input disabled id="profile-img-file-input" type="file" class="profile-img-file-input" name="image_path" onchange="loadFile(event)" accept="image/png, image/gif, image/jpeg">
                                        <label for="profile-img-file-input" class="profile-photo-edit avatar-xs">
                                            <span class="avatar-title rounded-circle bg-light text-body">
                                                <i class="ri-camera-fill"></i>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                {{-- <h5 class="fs-16 mb-1">Anna Adame</h5>
                                            <p class="text-muted mb-0">Lead Designer / Developer</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="company" class="form-label">Công ty</label>
                                <input disabled rule type="text" class="form-control @error('company') is-invalid @enderror" placeholder="(Điền thông tin)" id="company" name="company" value=" {{ $basic->company }} ">
                                @error('company')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Công ty-->
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="room" class="form-label">Phòng : <span style="font-size: 15px;"> {{ !empty($basic->organizational) ? $basic->organizational->room->name : 'Chưa có' }} </span></label>
                            </div>
                        </div>
                        <!--end col Phòng-->
                        <div class="col-sm-12 col-md-4">
                            <div class="mb-3">
                                <label for="part" class="form-label">Bộ phận: <span style="font-size: 15px;"> {{ !empty($basic->organizational) ? $basic->organizational->team->name : 'Chưa có' }} </span></label>
                            </div>
                        </div>
                        <!--end col Bộ phận-->
                        <div class="col-sm-12 col-md-4">
                            <div class="mb-3">
                                <label for="position" class="form-label">Chức danh: <span style="font-size: 15px;">
                                @if(!empty($basic->organizational))
                                    @if(in_array($basic->rank, [2,3]) && $basic->organizational->room->id != 8)
                                    {{ !empty($basic->organizational) ? (
                                        count(explode('CHUYÊN VIÊN / NHÂN VIÊN',$basic->organizational->position->name)) > 1 ?
                                        mb_strtoupper(config('constants.rank')[$basic->rank]) . 
                                        explode('CHUYÊN VIÊN / NHÂN VIÊN',$basic->organizational->position->name)[1] : 
                                        $basic->organizational->position->name) : 'Chưa có' }}
                                    @else
                                    {{ !empty($basic->organizational) ? $basic->organizational->position->name : 'Chưa có' }}
                                    @endif
                                @else
                                    {{ 'Chưa có' }}
                                @endif
                                </span></label>
                            </div>
                        </div>
                        <!--end col Chức danh-->
                        <div class="col-sm-12 col-md-4">
                            <div class="mb-3">
                                <label for="rank" class="form-label">Cấp bậc</label>
                                <input disabled rile type="text" class="form-control @error('full_name') is-invalid @enderror" placeholder="(Điền thông tin)" id="full_name" name="full_name" value="{{ config('constants.rank')[$basic->rank] }}">
                            </div>
                        </div>
                        <!--end col Cấp bậc-->
                        <div class="col-sm-12 col-md-3">
                            <div class="mb-3">
                                <label for="full_name" class="form-label">Họ và tên</label>
                                <input disabled rile type="text" class="form-control @error('full_name') is-invalid @enderror" placeholder="(Điền thông tin)" id="full_name" name="full_name" value="{{ $basic->full_name }}">
                                @error('full_name')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Họ và tên-->
                        <div class="col-sm-12 col-md-3">
                            <div class="mb-3">
                                <label for="name_eng" class="form-label">Tên Tiếng Anh</label>
                                <input disabled rule type="text" class="form-control @error('name_eng') is-invalid @enderror" placeholder="(Điền thông tin)" id="name_eng" name="name_eng" value="{{ $basic->english_name }}">
                                @error('name_eng')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Tên Tiếng Anh-->
                        <div class="col-sm-12 col-md-2">
                            <div class="mb-3">
                                <label for="code_staff" class="form-label">Mã nhân viên</label>
                                <input disabled rule type="text" class="form-control @error('code_staff') is-invalid @enderror" placeholder="(Điền thông tin)" id="code_staff" name="code_staff" value="{{ $basic->employee_code }}">
                                @error('code_staff')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Mã nhân viên-->
                        <div class="col-sm-12 col-md-3">
                            <div class="mb-3">
                                <label for="date_work" class="form-label">Ngày vào làm</label>
                                <input disabled rule type="date" class="form-control @error('date_work') is-invalid @enderror" id="date_work" name="date_work" value="{{ $basic->date_of_entry_to_work }}">
                                @error('date_work')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Ngày vào làm-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="birth_date" class="form-label">Ngày sinh</label>
                                <input disabled rule type="date" class="form-control @error('birth_date') is-invalid @enderror" id="birth_date" name="birth_date" value="{{ $basic->dob }}">
                                @error('birth_date')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Ngày sinh-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="genders" class="form-label">Giới tính</label>
                                <!-- Base Radios -->
                                <div class="d-flex mt-2">
                                    <div class="col-xl-6 form-check mb-2">
                                        <input disabled class="form-check-input"
                                        @checked($basic->gender) type="radio" name="genders" id="male" value="1">
                                        <label class="form-check-label" for="male">
                                            Nam
                                        </label>
                                    </div>
                                    <div class="col-xl-6 form-check">
                                        <input disabled 
                                        @checked(!$basic->gender) class="form-check-input" type="radio" name="genders" id="female" value="0">
                                        <label class="form-check-label" for="female">
                                            Nữ
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end col Giới tính-->
                        <div class="col-6">
                            <div class="mb-3">
                            <label for="nation" class="form-label">Quốc tịch</label>
                                    <div class="col-xl-12">
                                        <select disabled class="form-select" aria-label="Default select example" name="nation">
                                            @foreach(config('constants.national') as $key => $nation)
                                                <option @selected($key == $basic->nationality) value="{{ $key }}">{{ $nation }}</option>
                                            @endforeach
                                        </select>

                                    </div>
                            </div>
                        </div>
                        <!--end col Quốc tịch-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="phone" class="form-label">Số điện thoại</label>
                                <input disabled rule type="number" class="form-control @error('phone') is-invalid @enderror" placeholder="(Điền thông tin)" id="phone" name="phone" value="{{ $basic->phone }}">
                                @error('phone')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Số điện thoại-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="email" class="form-label">Email cá nhân</label>
                                <input disabled rule type="email" class="form-control @error('email') is-invalid @enderror" placeholder="(Điền thông tin)" id="email" name="email" value="{{ $basic->personal_email }}">
                                @error('email')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Email cá nhân-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="email_company" class="form-label">Email Công ty cấp</label>
                                <input disabled rule type="email" class="form-control @error('email_company') is-invalid @enderror" placeholder="(Điền thông tin)" id="email_company" name="email_company" value="{{ $basic->company_email }}">
                                @error('email_company')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Email Công ty cấp-->

                        <div class="col-4">
                            <div class="mb-3">
                                <label for="file_motivator" class="form-label">CV : </label>
                                @if(!empty($basic->cv))
                                <a href="{{ url($basic->cv) }}" download>{{ explode('/', $basic->motivators)[5] }}</a>
                                <div class="card-body">
                                    <input disabled rule type="file" class="filepond filepond-input-multiple @error('file_motivator') is-invalid @enderror" multiple name="file_cv" data-allow-reorder="true" accept=".xlsx,.xls, .doc, .docx,.ppt, .pptx,.txt,.pdf" data-max-file-size="3MB" data-max-files="3" value="{{ $basic->cv }}">
                            
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="file_disc" class="form-label">DISC : </label>
                                @if(!empty($basic->disc))
                                <a href="{{ url($basic->disc) }}" download>{{ explode('/', $basic->disc)[5] }}</a>
                                <div class="card-body">
                                    <input disabled rule type="file" class="filepond filepond-input-multiple @error('file_disc') is-invalid @enderror" multiple name="file_disc" data-allow-reorder="true" accept=".xlsx, .xls, .doc, .docx,.ppt, .pptx,.txt,.pdf" data-max-file-size="3MB" data-max-files="3" value="{{ $basic->disc }}">
                                    @error('file_disc')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                                @endif
                            </div>
                        </div>
                        <!--end col DISC-->
                        <div class="col-4">
                            <div class="mb-3">
                                <label for="file_motivator" class="form-label">Motivator : </label>
                                @if(!empty($basic->disc))
                                <a href="{{ url($basic->motivators) }}" download>{{ explode('/', $basic->motivators)[5] }}</a>
                                <div class="card-body">
                                    <input disabled rule type="file" class="filepond filepond-input-multiple @error('file_motivator') is-invalid @enderror" multiple name="file_motivator" data-allow-reorder="true" accept=".xlsx,.xls, .doc, .docx,.ppt, .pptx,.txt,.pdf" data-max-file-size="3MB" data-max-files="3" value="{{ $basic->motivators }}">
                                    @error('file_motivator')
                                    <small class="help-block text-danger">{{ $message }} *</small>
                                    @enderror
                                </div>
                                @endif
                            </div>
                        </div>
                        <!--end col Motivator-->
                    </div>
                    <!--end row-->
                </div>
                <div class="tab-pane" id="personal-info" role="tabpanel">
                    <div class="row">
                        <div class="col-12">
                            <div class="col-3 mb-3">
                                <label for="ethnic" class="form-label">Dân tộc</label>

                                <div class="col-xl-12 pe-3">
                                    <select disabled class="form-select" aria-label="Default select example" name="ethnic">
                                        @foreach(config('constants.ethnic') as $key => $ethnic)
                                        <option @if($person->ethnic == $key) selected @endif value="{{ $key }}">{{ $ethnic }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                        </div>
                        <!--end col DÂN TỘC-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="passport" class="form-label">CCCD/Passport</label>
                                <input disabled rule type="text" class="form-control @error('passport') is-invalid @enderror" placeholder="(Điền thông tin)" id="passport" name="passport" value="{{ $person->cccd }}">
                                @error('passport')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col CCCD/Passport-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="issue_date" class="form-label">Ngày cấp</label>
                                <input disabled rule type="date" class="form-control @error('issue_date') is-invalid @enderror" id="issue_date" name="issue_date" value="{{ $person->date_range }}">
                                @error('issue_date')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Ngày cấp-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="providers" class="form-label">Nơi cấp</label>
                                <input disabled rule type="text" class="form-control @error('providers') is-invalid @enderror" placeholder="(Điền thông tin)" id="providers" name="providers" value="{{ $person->issued_by }}">
                                @error('providers')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Nơi cấp-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="permanent_address" class="form-label">Địa chỉ thường trú</label>
                                <input disabled rule type="text" class="form-control @error('permanent_address') is-invalid @enderror" placeholder="(Điền thông tin)" id="permanent_address" name="permanent address" value="{{ $person->permanent_address }}">
                                @error('permanent_address')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Địa chỉ thường trú-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="temporary_address" class="form-label">Địa chỉ tạm trú</label>
                                <input disabled rule type="text" class="form-control @error('temporary_address') is-invalid @enderror" placeholder="(Điền thông tin)" id="temporary_address" name="temporary_address" value="{{ $person->temporary_residence_address }}">
                                @error('temporary_address')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Địa chỉ tạm trú-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="insurance_code" class="form-label">Mã BHXH</label>
                                <input disabled rule type="number" class="form-control @error('insurance_code') is-invalid @enderror" placeholder="(Điền chuỗi số)" id="insurance_code" name="insurance_code" value="{{ $person->social_insurance_code }}">
                                @error('insurance_code')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Mã BHXH-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="medical_code" class="form-label">Mã BHYT</label>
                                <input disabled rule type="number" class="form-control @error('medical_code') is-invalid @enderror" placeholder="(Điền chuỗi số)" id="medical_code" name="medical_code" value="{{ $person->health_insurance_code }}">
                                @error('medical_code')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Mã BHYT-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="address_kcb" class="form-label">Nơi KCB</label>
                                <input disabled rule type="text" class="form-control @error('address_kcb') is-invalid @enderror" placeholder="(Điền thông tin bệnh viện)" id="address_kcb" name="address_kcb" value="{{ $person->healthcare_place }}">
                                @error('address_kcb')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Nơi KCB-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="tax" class="form-label">Thuế TNCN</label>
                                <input disabled rule type="number" class="form-control @error('tax') is-invalid @enderror" placeholder="(Điền chuỗi số)" id="tax" name="tax" value="{{ $person->personal_income_tax }}">
                                @error('tax')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Thuế TNCN-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="dependent_person" class="form-label">Người phụ thuộc</label>
                                <input disabled rule type="number" class="form-control @error('dependent_person') is-invalid @enderror" placeholder="(Điền số lượng)" id="dependent_person" name="dependent_person" value="{{ $person->dependent_person }}">
                                @error('dependent_person')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Người phụ thuộc-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="npt" class="form-label">Chi tiết NPT</label>
                                <input disabled rule type="text" class="form-control @error('npt') is-invalid @enderror" placeholder="(Điền họ tên - mối quan hệ) mỗi người cách nhau bằng dấu ;" id="npt" name="npt" value="{{ $person->details_dependent_person }}">
                                @error('npt')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Chi tiết NPT-->
                        <div class="col-12">
                            <div class="col-3 mb-3">
                                <label for="level" class="form-label">Trình độ học vấn</label>

                                <div class="col-xl-11">
                                    <select disabled class="form-select" aria-label="Default select example" name="level">
                                        @foreach(config('constants.literacy') as $key => $literacy)
                                        <option @if($person->academic_level == $key) selected @endif value="{{ $key }}" >{{ $literacy }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--end col Trình độ học vấn-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="school" class="form-label">Trường</label>
                                <input disabled rule type="text" class="form-control @error('school') is-invalid @enderror" placeholder="(Điền thông tin)" id="school" name="school" value="{{ $person->school }}">
                                @error('school')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Trường-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="specialized" class="form-label">Chuyên ngành</label>
                                <input disabled rule type="text" class="form-control @error('school') is-invalid @enderror" placeholder="(Điền thông tin)" id="specialized" name="specialized" value="{{ $person->specialized }}">
                                @error('school')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Chuyên ngành-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="tknh" class="form-label">Số TKNH</label>
                                <input disabled rule type="number" class="form-control @error('school') is-invalid @enderror" placeholder="(Điền chuỗi số)" id="tknh" name="tknh" value="{{ $person->number_bank_account }}">
                                @error('school')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Số TKNH-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="bank" class="form-label">Tên ngân hàng</label>
                                <input disabled rule type="text" class="form-control @error('school') is-invalid @enderror" placeholder="(Điền thông tin)" id="bank" name="bank" value="{{ $person->bank_name }}">
                                @error('school')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Tên ngân hàng-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="license_plates" class="form-label">Biển số xe</label>
                                <input disabled rule type="text" class="form-control @error('school') is-invalid @enderror" placeholder="(Điền chuỗi mã)" id="license_plates" name="license_plates" value="{{ $person->license_plates }}">
                                @error('school')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Số TKNH-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="carriage_category" class="form-label">Loại xe</label>
                                <input disabled rule type="text" class="form-control @error('school') is-invalid @enderror" placeholder="(Điền thông tin)" id="carriage_category" name="carriage_category" value="{{ $person->range_of_vehicle }}">
                                @error('school')
                                <small class="help-block text-danger">{{ $message }} *</small>
                                @enderror
                            </div>
                        </div>
                        <!--end col Tên ngân hàng-->

                    </div>
                    <!--end row-->
                </div>
                <div class="tab-pane" id="relative-info" role="tabpanel">
                    <div class="row">
                        @foreach ($relative['priority'] as $index => $priority)
                        <input disabled hidden name="priority_id_{{ $index + 1 }}" value="{{ $priority->id }}">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="prioritized_1" class="form-label">Họ và tên ưu tiên {{ $index + 1 }}</label>
                                <input disabled type="text" class="form-control" placeholder="(Điền thông tin)" id="prioritized_1" name="prioritized_{{ $index + 1 }}" value="{{ $priority->full_name_priority }}">
                            </div>
                        </div>
                        <!--end col Họ và tên ưu tiên 1-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="relationship" class="form-label">Mối quan hệ</label>
                                <div class="col-xl-12">
                                    <select disabled class="form-select" aria-label="Default select example" name="relationship_{{ $index + 1 }}">
                                        @foreach (config('constants.relations') as $index => $relation)
                                        <option @if($priority->relationship == $index) selected @endif value="{{ $index }}"> {{ $relation }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--end col Mối quan hệ-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="phone_prioritized_{{ $index + 1 }}" class="form-label">Số điện thoại</label>
                                <input disabled type="tel" class="form-control" placeholder="(Điền chuỗi số)" id="phone_prioritized_{{ $index + 1 }}" name="phone_prioritized_{{ $index + 1 }}" value="{{ $priority->phone_number }}">
                            </div>
                        </div>
                        {{-- <!--end col Số điện thoại-->
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label for="prioritized_2" class="form-label">Họ và tên ưu tiên 2</label>
                                            <input disabled type="text" class="form-control" placeholder="(Điền thông tin)"
                                                id="prioritized_2" name="prioritized_2">
                                        </div>
                                    </div>
                                    <!--end col Họ và tên ưu tiên 2-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="relationship" class="form-label">Mối quan hệ</label>
                                            <div class="col-xl-12">
                                                <select disabled class="form-select" aria-label="Default select example">
                                                    <option selected>Mẹ</option>
                                                    <option value="1">Mẹ</option>
                                                    <option value="2">2</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col Mối quan hệ-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="phone_prioritized_2" class="form-label">Số điện thoại</label>
                                            <input disabled type="tel" class="form-control" placeholder="(Điền chuỗi số)"
                                                id="phone_prioritized_2" name="phone_prioritized_2">
                                        </div>
                                    </div>                                 --}}
                        @endforeach
                        <!--end col Số điện thoại-->
                        @foreach ($relative['child'] as $index => $childPriority)
                        <input disabled hidden name="childPriority_id_{{ $index + 1 }}" value="{{ $childPriority->id }}">
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="child_{{ $index + 1 }}" class="form-label">Họ và tên con {{ $index + 1 }}</label>
                                <input disabled type="text" class="form-control" placeholder="(Điền thông tin)" id="child_{{ $index + 1 }}" name="child_{{ $index + 1 }}" value="{{ $childPriority->full_name_child }}">
                            </div>
                        </div>
                        <!--end col Họ và tên con 1-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="gender_child_{{ $index + 1 }}" class="form-label">Giới tính</label>
                                <!-- Base Radios -->
                                <div class="d-flex mt-2">
                                    <div class="col-xl-6 form-check mb-2">
                                        <input disabled class="form-check-input" type="radio" @if($childPriority->gender == 1) checked @endif
                                        name="gender_child_{{ $index + 1 }}" id="male_child_{{ $index + 1 }}" value="1">
                                        <label class="form-check-label" for="male_child_{{ $index + 1 }}">
                                            Nam
                                        </label>
                                    </div>
                                    <div class="col-xl-6 form-check">
                                        <input disabled class="form-check-input" type="radio" @if($childPriority->gender == 0) checked @endif
                                        name="gender_child_{{ $index + 1 }}" id="female_child_{{ $index + 1 }}" value="0">
                                        <label class="form-check-label" for="female_child_{{ $index + 1 }}">
                                            Nữ
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end col Giới tính-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="birth_date_child_{{ $index + 1 }}" class="form-label">Ngày sinh</label>
                                <input disabled type="date" class="form-control" id="birth_date_child_{{ $index + 1 }}" name="birth_date_child_{{ $index + 1 }}" value="{{ $childPriority->dob }}">
                            </div>
                        </div>
                        <!--end col Ngày sinh-->
                        {{-- <div class="col-6">
                                        <div class="mb-3">
                                            <label for="child_2" class="form-label">Họ và tên con 2</label>
                                            <input disabled type="text" class="form-control" placeholder="(Điền thông tin)"
                                                id="child_2" name="child_2">
                                        </div>
                                    </div>
                                    <!--end col Họ và tên con 2-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="gender_child_2" class="form-label">Giới tính</label>
                                            <!-- Base Radios -->
                                            <div class="d-flex mt-2">
                                                <div class="col-xl-6 form-check mb-2">
                                                    <input disabled class="form-check-input" type="radio" name="gender_child_2"
                                                        id="male_child_2" value="1">
                                                    <label class="form-check-label" for="male_child_2">
                                                        Nam
                                                    </label>
                                                </div>
                                                <div class="col-xl-6 form-check">
                                                    <input disabled class="form-check-input" type="radio" name="gender_child_2"
                                                        id="female_child_2" value="0" checked>
                                                    <label class="form-check-label" for="female_child_2">
                                                        Nữ
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--end col Giới tính-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="birth_date_child_2" class="form-label">Ngày sinh</label>
                                            <input disabled type="date" class="form-control" id="birth_date_child_2"
                                                name="birth_date_child_2">
                                        </div>
                                    </div>
                                    <!--end col Ngày sinh-->
                                    <div class="col-6">
                                        <div class="mb-3">
                                            <label for="child_3" class="form-label">Họ và tên con 3</label>
                                            <input disabled type="text" class="form-control" placeholder="(Điền thông tin)"
                                                id="child_3" name="child_3">
                                        </div>
                                    </div>
                                    <!--end col Họ và tên con 3-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="gender_child_3" class="form-label">Giới tính</label>
                                            <!-- Base Radios -->
                                            <div class="d-flex mt-2">
                                                <div class="col-xl-6 form-check mb-2">
                                                    <input disabled class="form-check-input" type="radio" name="gender_child_3"
                                                        id="male_child_3" value="1">
                                                    <label class="form-check-label" for="male_child_3">
                                                        Nam
                                                    </label>
                                                </div>
                                                <div class="col-xl-6 form-check">
                                                    <input disabled class="form-check-input" type="radio" name="gender_child_3"
                                                        id="female_child_3" value="0" checked>
                                                    <label class="form-check-label" for="female_child_3">
                                                        Nữ
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--end col Giới tính-->
                                    <div class="col-3">
                                        <div class="mb-3">
                                            <label for="birth_date_child_3" class="form-label">Ngày sinh</label>
                                            <input disabled type="date" class="form-control" id="birth_date_child_3"
                                                name="birth_date_child_3">
                                        </div>
                                    </div>
                                    <!--end col Ngày sinh--> --}}
                        @endforeach
                    </div>
                </div>
                <div class="tab-pane" id="contract-info" role="tabpanel">
                    <div class="row">
                        @foreach ($labors['hdld'] as $index => $labor)
                        <input disabled hidden name="labor_id_{{ $index + 1 }}" value="{{ $labor->id }}">
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="number_hdld_{{ $index + 1 }}" class="form-label">Số HĐLĐ lần {{ $index + 1 }}</label>
                                <input disabled type="text" class="form-control" placeholder="(Điền thông tin)" value="{{ $labor->number_of_labor_contract }}" id="number_hdld_{{ $index + 1 }}" name="number_hdld_{{ $index + 1 }}">
                            </div>
                        </div>
                        <!--end col Số  HĐLĐ lần 1-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="contract_{{ $index + 1 }}" class="form-label">Loại hợp đồng</label>
                                <div class="col-xl-12">
                                    <select disabled class="form-select" aria-label="Default select example" name="contract_{{ $index + 1 }}">
                                        @foreach(config('constants.contract') as $key => $contract)
                                        <option @if($labor->type_of_contract == $key) selected @endif value="{{ $key }}">{{ $contract }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--end col Loại hợp đồng-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="date_form_{{ $index + 1 }}" class="form-label">Từ ngày</label>
                                <input disabled type="date" class="form-control" id="date_form_{{ $index + 1 }}" name="date_form_{{ $index + 1 }}" value="{{ $labor->form_date_labor }}">
                            </div>
                        </div>
                        <!--end col Từ ngày-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="to_form_{{ $index + 1 }}" class="form-label">Đến ngày</label>
                                <input disabled type="date" class="form-control" id="to_form_{{ $index + 1 }}" name="to_form_{{ $index + 1 }}" value="{{ $labor->to_date }}">
                            </div>
                        </div>
                        <!--end col Đến ngày-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="wage_hdld_{{ $index + 1 }}" class="form-label">Lương HĐLĐ</label>
                                <input disabled type="number" class="form-control" placeholder="(Điền số tiền VND)" id="wage_hdld_{{ $index + 1 }}" name="wage_hdld_{{ $index + 1 }}" value="{{ $labor->labor_contract_salary }}">
                            </div>
                        </div>
                        <!--end col Lương HĐLĐ-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="number_plhd_{{ $index + 1 }}" class="form-label">Số PLHĐ</label>
                                <input disabled type="text" class="form-control" placeholder="(Điền chuỗi mã)" id="number_plhd_{{ $index + 1 }}" name="number_plhd_{{ $index + 1 }}" value="{{ $labor->contract_addendum_number }}">
                            </div>
                        </div>
                        <!--end col Số  PLHĐ-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="date_form_plhd_{{ $index + 1 }}" class="form-label">Từ ngày</label>
                                <input disabled type="date" class="form-control" id="date_form_plhd_{{ $index + 1 }}" name="date_form_plhd_{{ $index + 1 }}" value="{{ $labor->form_date_addendum }}">
                            </div>
                        </div>
                        <!--end col Từ ngày-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="wage_plhd_{{ $index + 1 }}" class="form-label">Lương PLHĐ</label>
                                <input disabled type="number" class="form-control" placeholder="(Điền số tiền VND)" id="wage_plhd_{{ $index + 1 }}" name="wage_plhd_{{ $index + 1 }}" value="{{ $labor->contract_addendum_salary }}">
                            </div>
                        </div>
                        <!--end col Lương PLHĐ-->
                        <div class="col-6">
                            <div class="mb-3">
                                <label for="wage_kpi_{{ $index + 1 }}" class="form-label">Lương KPIs</label>
                                <input disabled type="number" class="form-control" placeholder="(Điền số tiền VND)" id="wage_kpi_{{ $index + 1 }}" name="wage_kpi_{{ $index + 1 }}" value="{{ $labor->salary_kpis }}">
                            </div>
                        </div>
                        <!--end col Lương KPIs-->
                        <div class="col-12">
                            <div class="mb-3">
                                <label for="diff_{{ $index + 1 }}" class="form-label">Chính sách khác</label>
                                <input disabled type="text" class="form-control" placeholder="(Điền thông tin)" id="diff_{{ $index + 1 }}" name="diff_{{ $index + 1 }}" value="{{ $labor->other_policy }}">
                            </div>
                        </div>
                        <!--end col Chính sách khác-->
                        @endforeach
                    </div>
                    <div class="row">

                        <!--end col Chính sách khác-->
                        <input disabled hidden name="bbt_id_id" value="{{ $labors['bbt']->id }}">
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="number_bbtlhd" class="form-label">Số BBTLHĐ</label>
                                <input disabled type="text" class="form-control" placeholder="(Điền chuỗi mã)" id="number_bbtlhd" name="number_bbtlhd" value="{{ $labors['bbt']->no_bbtlhd }}">
                            </div>
                        </div>
                        <!--end col Số  BBTLHĐ-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="date_form_bbtlhd" class="form-label">Từ ngày</label>
                                <input disabled type="date" class="form-control" id="date_form_bbtlhd" name="date_form_bbtlhd" value="{{ $labors['bbt']->from_date_bbtlhd }}">
                            </div>
                        </div>
                        <!--end col Từ ngày-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="number_qdtv" class="form-label">Số QĐTV</label>
                                <input disabled type="text" class="form-control" placeholder="(Điền chuỗi mã)" id="number_qdtv" name="number_qdtv" value="{{ $labors['bbt']->no_qdtv }}">
                            </div>
                        </div>
                        <!--end col Số QĐTV-->
                        <div class="col-3">
                            <div class="mb-3">
                                <label for="date_form_qdtv" class="form-label">Từ ngày</label>
                                <input disabled type="date" class="form-control" id="date_form_qdtv" name="date_form_qdtv" value="{{ $labors['bbt']->from_date_qdtv }}">
                            </div>
                        </div>
                        <!--end col Từ ngày-->
                    </div>
                </div>
            </div>
        </div><!-- end card-body -->
    </div>
</div>
<!--end col-->
</div>

@endsection
@section('script')
<script src="{{ URL::asset('assets/js/pages/pricing.init.js') }}"></script>
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/listjs.init.js') }}"></script>
<script src="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
<script>
    var loadFile = function(event) {
        var image_path = document.getElementById('output');
        image_path.src = URL.createObjectURL(event.target.files[0]);
    }
</script>
@endsection