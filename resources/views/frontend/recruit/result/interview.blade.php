@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<form id="formInterView" action="" method="POST">
@foreach($dataInit['interview']['interview_scores'] as $value)
@if ($value['confirm']['basic_info_id'] == auth()->user()->basic_info_id)
    @csrf
    <input hidden name="interview_id" value="{{ $dataInit['interview']['id'] }}">
    <input hidden name="type_confirm" value="{{ $value['confirm']['type_confirm'] }}">
    <input hidden name="detail_interview_id" value="{{ $value['id'] }}">
    <div class="row justify-content-between pe-5 mb-3">
        <h3 style="width: fit-content" class="m-0"></h3>
        <button type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content"><i class="ri-save-3-line label-icon  fs-16 "></i> Lưu</button>
    </div>
    <div class="card">
        <div class="card-header">
            <h3>KẾT QUẢ PHỎNG VẤN</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-2">
                    <label for="borderInput" class="form-label">Họ tên ứng viên</label>
                    <input value="{{ $dataInit['interview']['candidates_name'] }}" type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
                </div>
                <div class="col-xl-3 form-group">
                    <label for="exampleInputdate" class="form-label">Ngày phỏng vấn</label>
                    <input type="text" class="form-control datepicker" name="interview_date" id="exampleInputdate">
                </div>
                <div class="col-xl-3">
                    <label for="borderInput" class="form-label">Phòng</label>
                    <input value="{{ $dataInit['interview']['room']['name'] }}" type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
                </div>
                <div class="col-xl-2">
                    <label for="borderInput" class="form-label">Bộ phận</label>
                    <input value="{{ $dataInit['interview']['team']['name'] }}" type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
                </div>
                <div class="col-xl-2">
                    <label for="borderInput" class="form-label">Chức danh</label>
                    <input value="{{ $dataInit['interview']['position']['name'] }}" type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="d-flex justify-content-between">
                <h3 class="m-0" style="width: fit-content;">Phần I: Đánh giá kết quả phỏng vấn</h3>
                <button type="button" class="btn btn-success position-relative d-inline" id="borderedToast2Btn"><i class="ri-error-warning-line fs-16 align-middle"></i></button>
                <div style="z-index: 11; position: absolute; top: 0; right: 0">
                    <div id="borderedToast2" class="toast toast-border-success overflow-hidden mt-3" role="alert" aria-live="assertive" aria-atomic="true">
                        <div class="toast-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-column">
                                    <h6 class="mb-2">1- Biết</h6>
                                    <h6 class="mb-2">2- Hiểu phải làm gì</h6>
                                    <h6 class="mb-2">3- Làm - làm được, làm rõ được tình huống (clarify)</h6>
                                    <h6 class="mb-2">4- Thành thạo - có khả năng tổng hợp ý kiến từ những người có liên quan để hiểu gốc rễ (confirm)</h6>
                                    <h6 class="mb-0">5- Xuất sắc - lựa chọn giải pháp thay thế, đánh giá được thách thức, hướng tới kết quả win-win(create, challenge, commit)</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row gy-3 align-items-end">
                <div class="col-12">
                    <table class="table table-bordered table-nowrap text-center">
                        <thead>
                            <tr>
                                <th class="text-start">Phần I: Đánh giá năng lực ứng viên</th>
                                <th style="width:150px;">1</th>
                                <th style="width:150px;">2</th>
                                <th style="width:150px;">3</th>
                                <th style="width:150px;">4</th>
                                <th style="width:150px;">5</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 1; @endphp
                            @foreach(config('header.criteria') as $key => $title)

                            <tr>
                                <td class="text-start">{{$i}}. {{ $title }}</td>
                                <td>
                                    <input class="form-check-input" value="1" style="width:30px ; height:30px ;" type="radio" name="{{$key}}">
                                </td>
                                <td>
                                    <input class="form-check-input" value="2" style="width:30px ; height:30px ;" type="radio" name="{{$key}}">
                                </td>
                                <td>
                                    <input class="form-check-input" value="3" style="width:30px ; height:30px ;" type="radio" name="{{$key}}">
                                </td>
                                <td>
                                    <input class="form-check-input" value="4" style="width:30px ; height:30px ;" type="radio" name="{{$key}}">
                                </td>
                                <td>
                                    <input class="form-check-input" value="5" style="width:30px ; height:30px ;" type="radio" name="{{$key}}">
                                </td>
                            </tr>

                            @php $i++; @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <h3 class="m-0">Phần II: Kết quả các bài kiểm tra chuyên môn (nếu có)</h3>
                    </div>
                    <div class="col-6">
                        <div class="row ">
                            <div class="d-flex align-items-center" style="width: fit-content;">
                                <input @if($value['confirm']['type_confirm'] !=6) disabled @endif value="1" 
                                class="form-check-input" style="width:30px ; height:30px ;" type="radio" 
                                name="specialize" id="achieved-1">
                                <h3 class="form-check-label ms-1" for="achieved-1">Đạt</label>
                            </div>
                            <div class="d-flex align-items-center" style="width: fit-content;">
                                <input @if($value['confirm']['type_confirm'] !=6) disabled @endif value="2" 
                                class="form-check-input" style="width:30px ; height:30px ;" type="radio" 
                                name="specialize" id="achieved-2">
                                <h3 class="form-check-label ms-1" for="achieved-2">Không đạt</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <h3 class="m-0">Phần III: Kết quả kiểm tra tính cách (nếu có)</h3>
                    </div>
                    <div class="col-6">
                        <div class="row ">
                            <div class="d-flex align-items-center" style="width: fit-content;">
                                <input @if($value['confirm']['type_confirm'] !=2) disabled @endif class="form-check-input" style="width:30px ; height:30px ;" type="checkbox" name="disc[]" value="1" id="disc-1">
                                <h3 class="form-check-label ms-1" for="disc-1">D</label>
                            </div>
                            <div class="d-flex align-items-center" style="width: fit-content;">
                                <input @if($value['confirm']['type_confirm'] !=2) disabled @endif class="form-check-input" style="width:30px ; height:30px ;" type="checkbox" name="disc[]" value="2" id="disc-2">
                                <h3 class="form-check-label ms-1" for="disc-2">I</h3>
                            </div>
                            <div class="d-flex align-items-center" style="width: fit-content;">
                                <input @if($value['confirm']['type_confirm'] !=2) disabled @endif class="form-check-input" style="width:30px ; height:30px ;" type="checkbox" name="disc[]" value="3" id="disc-3">
                                <h3 class="form-check-label ms-1" for="disc-3">S</h3>
                            </div>
                            <div class="d-flex align-items-center" style="width: fit-content;">
                                <input @if($value['confirm']['type_confirm'] !=2) disabled @endif class="form-check-input" style="width:30px ; height:30px ;" type="checkbox" name="disc[]" value="4" id="disc-4">
                                <h3 class="form-check-label ms-1" for="disc-4">C</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <h3 class="m-0">Mức lương đề xuất của ứng viên</h3>
                    </div>
                    <div class="col-6">
                        @if($value['confirm']['type_confirm'] == 2)
                        <input type="text" class="form-control" id="salary" name="salary" maxlength="12">
                        @else
                        <h3>{{ number_format($dataInit['interview']['salary']) }}<span> đồng</span></h3>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="card">
            <div class="row card-body justify-content-center">

                <input hidden name="confirm_id" value="{{ $value['confirm']['id'] }}">
                <div class="col-xl-4">
                    @switch($value['confirm']['type_confirm'])
                    @case(2)
                    <h4 class="fw-bold text-center">P.QT HC-NS</h4>
                    <h5 class="text-center">{{ $value['confirm']['staff']['full_name'] }}</h5>
                    @break
                    @case(6)
                    <h4 class="fw-bold text-center">Trưởng Phòng</h4>
                    <h5 class="text-center">{{ $value['confirm']['staff']['full_name'] }}</h5>
                    @break
                    @case(3)
                    <h4 class="fw-bold text-center">Ban Giám Đốc</h4>
                    <h5 class="text-center">{{ $value['confirm']['staff']['full_name'] }}</h5>
                    @break
                    @endswitch
                </div>
                <div class="row justify-content-around form-group text-center">
                    <div style="width: fit-content;    font-size: 20px;">
                        <input class="form-check-input" type="radio" name="status" value="1" id="room-accept-1">
                        <label class="form-check-label ms-1" for="room-accept-1">Đạt</label>
                    </div>
                    <div style="width: fit-content;    font-size: 20px;">
                        <input class="form-check-input" type="radio" name="status" value="2" id="room-accept-2">
                        <label class="form-check-label ms-1" for="room-accept-2">Không đạt</label>
                    </div>
                    <div style="width: fit-content;    font-size: 20px;">
                        <input class="form-check-input" type="radio" name="status" value="3" id="room-accept-3">
                        <label class="form-check-label ms-1" for="room-accept-3">Ý kiến khác </label>
                    </div>
                </div>
                <div class="col-12 mt-3 form-group">
                    <label for="department_resource_notes" class="form-label">Lý do:</label>
                    <textarea disabled name="department_resource_notes" class="form-control" id="department_resource_notes" rows="3"></textarea>
                </div>
            </div>
        </div>
    </div>
@endif

@if ($value['confirm']['type_confirm'] == 6 && $dataInit['interview']['status'] == 1) 
    <input hidden name="confimerIDNext" value="{{ $value['confirm']['basic_info_id'] }}">
@elseif ($value['confirm']['type_confirm'] == 3 && $dataInit['interview']['status'] == 2)
    <input hidden name="confimerIDNext" value="{{ $value['confirm']['basic_info_id'] }}">
@endif
@endforeach
</form>
@endsection
@section('script')
<script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/cleave.js/cleave.js.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('input[name=status]').on('click', (event) => {
            if ($(event.target).val() == 3) {
                $('#department_resource_notes').removeAttr('disabled')
            } else {
                $('#department_resource_notes').attr('disabled', true)
            }
        })

        $('#borderedToast2Btn').on('click', function() {
            var toast = new bootstrap.Toast($('#borderedToast2'));
            toast.show();
        })

        $('#exampleInputdate').datepicker({
            format: 'dd/mm/yyyy',
            startDate: 'toDay',
            orientation: 'bottom',
            clearBtn: true
        });
        if ($('#salary').length > 0) {
            var salary = new Cleave('#salary', {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                unformatOnSubmit: true
            });
        }

        $('#formInterView').on('submit', function(event) {
            event.preventDefault()
            if ($('#salary').length > 0) {
                $('#salary').val(salary.getRawValue())
            }
            $.ajax({
                method: "POST",
                url: "{{ route('result.interviewCandidate') }}",
                data: $('#formInterView').serialize(),
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                        window.location.href = response.link
                    } else {
                        toastr.error(response.message);
                    }

                },
                error: function(error) {
                    handleFails(error);
                    if ($('#salary').length > 0) {
                        salary.setRawValue($('#salary').val())
                    }
                }
            })
        })

        function handleFails(response) {
            $('#selectRoom, #selectTeam, #selectPosition').removeClass('is-invalid')
            $('.is-invalid').removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formInterView').find(".has-error").find(".help-block").remove();
                $('#formInterView').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formInterView').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response.responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }
    });
</script>
@endsection