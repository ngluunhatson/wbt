@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('content')
<div class="card">
    <div class="card-header">
        <h3>KẾT QUẢ PHỎNG VẤN</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-xl-3">
                <label for="borderInput" class="form-label">Họ tên ứng viên</label>
                <input value="{{ $dataInit['interview']['candidates_name'] }}" type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
            </div>
            <div class="col-xl-3">
                <label for="borderInput" class="form-label">Phòng</label>
                <input value="{{ $dataInit['interview']['room']['name'] }}" type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
            </div>
            <div class="col-xl-3">
                <label for="borderInput" class="form-label">Bộ phận</label>
                <input value="{{ $dataInit['interview']['team']['name'] }}" type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
            </div>
            <div class="col-xl-3">
                <label for="borderInput" class="form-label">Chức danh</label>
                <input value="{{ $dataInit['interview']['position']['name'] }}" type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <h3 class="m-0" style="width: fit-content;">Phần I: Đánh giá kết quả phỏng vấn</h3>
            <button type="button" class="btn btn-success position-relative d-inline" id="borderedToast2Btn"><i class="ri-error-warning-line fs-16 align-middle"></i></button>
            <div style="z-index: 11; position: absolute; top: 0; right: 0">
                <div id="borderedToast2" class="toast toast-border-success overflow-hidden mt-3" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-body">
                        <div class="d-flex align-items-center">
                            <div class="flex-column">
                                <h6 class="mb-2">1- Biết</h6>
                                <h6 class="mb-2">2- Hiểu phải làm gì</h6>
                                <h6 class="mb-2">3- Làm - làm được, làm rõ được tình huống (clarify)</h6>
                                <h6 class="mb-2">4- Thành thạo - có khả năng tổng hợp ý kiến từ những người có liên quan để hiểu gốc rễ (confirm)</h6>
                                <h6 class="mb-0">5- Xuất sắc - lựa chọn giải pháp thay thế, đánh giá được thách thức, hướng tới kết quả win-win(create, challenge, commit)</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row gy-3 align-items-end">
            <div class="col-xl-4">
                <label for="exampleInputdate" class="form-label">Từ ngày</label>
                <input value="{{ $dataInit['interview']['interview_scores'][0]['interview_date'] }}" 
                type="text" class="form-control border-dashed" id="exampleInputdate" readonly>
            </div>
            <div class="col-xl-4">
                <input value="{{ $dataInit['interview']['interview_scores'][1]['interview_date'] }}" 
                type="text" class="form-control border-dashed" id="exampleInputdate" readonly>
            </div>
            <div class="col-xl-4">
                <input value="{{ $dataInit['interview']['interview_scores'][2]['interview_date'] }}" 
                type="text" class="form-control border-dashed" id="exampleInputdate" readonly>
            </div>


            <div class="col-12">
                <table class="table table-bordered table-nowrap text-center">
                    <thead>
                        <tr>
                            <th style="width:60px;">STT</th>
                            <th>TIÊU CHUẨN</th>
                            <th style="width:200px;">P. QT GC-NS</th>
                            <th style="width:200px;">TRƯỞNG PHÒNG</th>
                            <th style="width:200px;">GIÁM ĐỐC</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1; @endphp
                        @foreach(config('header.criteria') as $key => $title)
                        <tr>
                            <td>{{$i}}</td>
                            <td class="text-start">{{ $title }}</td>
                            <td>{{ $dataInit['interview']['interview_scores'][0][$key] }}</td>
                            <td>{{ $dataInit['interview']['interview_scores'][1][$key] }}</td>
                            <td>{{ $dataInit['interview']['interview_scores'][2][$key] }}</td>
                        </tr>
                        @php $i++; @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <h3 class="m-0">Phần II: Kết quả các bài kiểm tra chuyên môn (nếu có)</h3>
                </div>
                <div class="col-6">
                    <div class="row ">
                        <div class="d-flex align-items-center" style="width: fit-content;">
                            <input disabled @checked($dataInit['interview']['specialize'])==1) class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="achieved" id="achieved-1">
                            <h3 class="form-check-label ms-1" for="achieved-1">Đạt</label>
                        </div>
                        <div class="d-flex align-items-center" style="width: fit-content;">
                            <input disabled @checked($dataInit['interview']['specialize'])==2) class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="achieved" id="achieved-2">
                            <h3 class="form-check-label ms-1" for="achieved-2">Không đạt</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <h3 class="m-0">Phần III: Kết quả kiểm tra tính cách (nếu có)</h3>
                </div>
                <div class="col-6">
                    <div class="row ">
                        <div class="d-flex align-items-center" style="width: fit-content;">
                            <input disabled 
                            @if(!empty($dataInit['interview']['disc'])) 
                                @checked(in_array(1, $dataInit['interview']['disc']))
                            @endif
                            class="form-check-input" style="width:30px ; height:30px ;" type="checkbox" name="disc" id="disc-1">
                            <h3 class="form-check-label ms-1" for="disc-1">D</label>
                        </div>
                        <div class="d-flex align-items-center" style="width: fit-content;">
                            <input disabled 
                            @if(!empty($dataInit['interview']['disc'])) 
                            @checked(in_array(2, $dataInit['interview']['disc'])) 
                            @endif
                            class="form-check-input" style="width:30px ; height:30px ;" type="checkbox" name="disc" id="disc-2">
                            <h3 class="form-check-label ms-1" for="disc-2">I</h3>
                        </div>
                        <div class="d-flex align-items-center" style="width: fit-content;">
                            <input disabled
                            @if(!empty($dataInit['interview']['disc']))  
                                @checked(in_array(3, $dataInit['interview']['disc']))
                            @endif 
                            class="form-check-input" style="width:30px ; height:30px ;" type="checkbox" name="disc" id="disc-3">
                            <h3 class="form-check-label ms-1" for="disc-3">S</h3>
                        </div>
                        <div class="d-flex align-items-center" style="width: fit-content;">
                            <input disabled
                            @if(!empty($dataInit['interview']['disc']))  
                            @checked(in_array(4, $dataInit['interview']['disc'])) 
                            @endif
                            class="form-check-input" style="width:30px ; height:30px ;" type="checkbox" name="disc" id="disc-4">
                            <h3 class="form-check-label ms-1" for="disc-4">C</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <h3 class="m-0">Mức lương đề xuất của ứng viên</h3>
                </div>
                <div class="col-6">
                    <h3>{{ number_format($dataInit['interview']['salary']) }}<span> đồng</span></h3>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="card">
        <div class="row card-body justify-content-around">
            @foreach($dataInit['interview']['interview_scores'] as $value)
            <div class="col-xl-4">
                @switch($value['confirm']['type_confirm'])
                @case(2)
                <h4 class="fw-bold text-center">P.QT HC-NS</h4>
                <h5 class="text-center">{{ $value['confirm']['staff']['full_name'] }}</h5>
                @break
                @case(6)
                <h4 class="fw-bold text-center">Trưởng Phòng</h4>
                <h5 class="text-center">{{ $value['confirm']['staff']['full_name'] }}</h5>
                @break
                @case(3)
                <h4 class="fw-bold text-center">Ban Giám Đốc</h4>
                <h5 class="text-center">{{ $value['confirm']['staff']['full_name'] }}</h5>
                @break
                @endswitch
                @switch($value['confirm']['status'])
                @case(0)
                <h6 class="form-label text-center fst-italic mt-2">{{ 'Chưa phỏng vấn' }}</h6>
                @break
                @case(1)
                <h6 for="firstNameinput" class="form-label text-center fst-italic mt-2">{{ 'Đạt' }}</h6>
                @break
                @case(2)
                <h6 for="firstNameinput" class="form-label text-center fst-italic mt-2">{{ 'Không đạt' }}</h6>
                @break
                @case(3)
                <h6 for="firstNameinput" class="form-label text-center fst-italic mt-2 mb-0">{{ 'Ý kiến khác' }}</h6>
                <div class="text-center fst-italic mt-2">
                    <label>Lý do : {{ $value['confirm']['note']}}</label>
                </div>
                @break
                @endswitch
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
@section('script')
@endsection