@extends('layouts.fe.master2')
@section('title') @lang('translation.starter') @endsection
@section('content')
<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h3 class="d-inline m-0">DANH SÁCH KẾT QUẢ PHỎNG VẤN</h3>
        @can('recruit_result-create')
        <a href="/recruit/result/create">
            <button type="button" class="btn btn-success btn-label waves-effect waves-light"><i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm</button>
        </a>
        @endcan
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Ứng viên</th>
                            <th>Phòng</th>
                            <th>Bộ phận</th>
                            <th>Vị trí tuyển dụng</th>
                            <th>Người tạo đánh giá</th>
                            <th>Trạng thái</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1;$canInterview=false; @endphp
                        @foreach($dataInit['listData'] as $value)
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $value['candidates_name'] }}</td>
                            <td>{{ $value['room']['name'] }}</td>
                            <td>{{ $value['team']['name'] }}</td>
                            <td>{{ $value['position']['name'] }}</td>
                            <td>{{ !empty($value['user']['staff']) ? $value['user']['staff']['full_name'] : $value['user']['name'] }}</td>
                            <td>{{ config('constants.status_confirm.recruit_result')[$value['status']] }}</td>
                            @foreach($value['interview_scores'] as $data)
                            @if ($data['confirm']['basic_info_id'] == auth()->user()->basic_info_id && $data['confirm']['status'] == 0 )
                            @if ($value['status'] == 1 && $data['confirm']['type_confirm'] == 2)
                            @php
                            $canInterview = true;
                            @endphp
                            @elseif ($value['status'] == 2 && $data['confirm']['type_confirm'] == 6)
                            @php
                            $canInterview = true;
                            @endphp
                            @elseif ($value['status'] == 3 && $data['confirm']['type_confirm'] == 3)
                            @php
                            $canInterview = true;
                            @endphp
                            @endif
                            @endif
                            @endforeach
                            <td>
                                <div class="dropdown d-inline-block">
                                    <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="ri-more-fill align-middle"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-end">
                                        @can('recruit_result-read')
                                        <li><a href="{{ route('result.view', $value['id']) }}" class="dropdown-item">
                                                <i class="ri-eye-fill align-bottom me-2 text-muted"></i> Xem</a>
                                        </li>
                                        @endcan
                                        @can('recruit_result-edit')
                                        @if (auth()->id() == $value['created_by'] && $value['status'] == 1)
                                        <li><a href="{{ route('result.edit', $value['id']) }}" class="dropdown-item edit-item-btn">
                                                <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Sửa</a>
                                        </li>
                                        @endif
                                        @endcan
                                        @if ($canInterview)
                                        <li><a href="{{ route('result.interview', $value['id']) }}" class="dropdown-item">
                                                <i class="ri-eye-fill align-bottom me-2 text-muted"></i> Phỏng vấn</a>
                                        </li>
                                        @endif
                                        @can('recruit_result-delete')
                                        @if (auth()->id() == $value['created_by'] && (in_array($value['status'], [1, 5, 6, 7])))
                                        <li>
                                            <a href="#" class="dropdown-item remove-item-btn">
                                                <i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Xoá
                                            </a>
                                            <form method="post" id="formDelete" action="{{ route('result.delete', $value['id']) }}" style="display:inline">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        </li>
                                        @endif
                                        @endcan
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @php $i++;@endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--end col-->
</div>
<!--end row-->

@endsection
@section('script')

<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
<script>
    $(function() {
        var myTable = $('#example').dataTable();

        $("#checkAll").click(function() {
            $(".form-check-input").prop('checked', $(this).prop("checked"));
        });

        $('#example tbody').on('click', '.remove-item-btn', function(event) {
            var form = $(this).closest("form");
            event.preventDefault();
            swal.fire({
                    title: 'Bạn muốn xóa dữ liệu này ?',
                    text: "Lưu ý : dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                    icon: 'warning',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Đồng ý',
                    showDenyButton: true,
                    denyButtonText: 'Hủy bỏ',
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        $('#formDelete').submit();
                    } else if (result.isDenied) {
                        Swal.fire('Dữ liệu được giữ lại', '', 'info')
                    }
                });
        });
    })
</script>

@endsection