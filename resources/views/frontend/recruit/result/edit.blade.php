@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('content')
<form method="PUT" id="formUpdate" enctype="multipart/form-data">
    <div class="row justify-content-between pe-3 mb-3">
        <h3 style="width: fit-content" class="m-0"></h3>
        <button type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content"><i class="ri-save-3-line label-icon  fs-16 "></i> Lưu</button>
    </div>
    @csrf
    <input name="interview_id" value="{{ $dataInit['interview']['id'] }}" hidden>
    <div class="card">
        <div class="card-header">
            <h3>TẠO ĐÁNH GIÁ PHỎNG VẤN</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-xl-3 form-group">
                    <label for="borderInput" class="form-label">Họ tên ứng viên</label>
                    <input type="text" value="{{ old('candidates_name', $dataInit['interview']['candidates_name']) }}" name="candidates_name" class="form-control" placeholder="Vui lòng nhập họ tên ứng viên">
                </div>
                <div class="col-xl-3">
                    <div class="mb-3">
                        <label for="firstNameinput" class="form-label">Phòng</label>
                        <div class="col-xl-12 form-group p-2">
                            <label>{{ $dataInit['interview']['room']['name'] }}</label>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="mb-3">
                        <label for="firstNameinput" class="form-label">Bộ phận</label>
                        <div class="col-xl-12 form-group p-2">
                            <label>{{ $dataInit['interview']['team']['name'] }}</label>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="mb-3">
                        <label for="firstNameinput" class="form-label">Chức danh</label>
                        <div class="col-xl-12 form-group p-2">
                            <label>{{ $dataInit['interview']['position']['name'] }}</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="card">
            <div class="row card-body justify-content-around">
                <div class="row mb-5">
                    @foreach($dataInit['interview']['interview_scores'] as $value)
                    <div class="col-4 d-flex justify-content-center">
                        <div class="row  ">
                            <div class="col-12 text-center">
                                @switch($value['confirm']['type_confirm'])
                                    @case(2)
                                        <h5>P.QT HC-NS</h5>
                                        @break
                                    @case(6)
                                        <h5>Trưởng phòng</h5>
                                        @break
                                    @case(3)
                                        <h5>Ban Giám Đốc</h5>
                                        @break
                                @endswitch
                            </div>
                            <input hidden name="type_confirm_1" value="2">
                            <div class="col-12 form-group text-center">
                                <h5> {{ $value['confirm']['staff']['full_name'] }} </h5>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            loadTeam(id);
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            loadPosition(id);
        });

        $('#formUpdate').on('submit', function(event) {
            event.preventDefault()
            $.ajax({
                method: "PUT",
                url: "{{ route('result.storeUpdate') }}",
                data: $('#formUpdate').serialize(),
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                        window.location.href = response.link
                    } else {
                        toastr.error(response.message);
                    }

                },
                error: function(error) {
                    handleFails(error);
                }
            })
        })

        function handleFails(response) {
            $('#selectRoom, #selectTeam, #selectPosition, #selectHr, #selectChief, #selectManager').removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formUpdate').find(".has-error").find(".help-block").remove();
                $('#formUpdate').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formUpdate').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response.responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }
    });

    function loadTeam(id, teamID = null) {
        if (id != '') {
            var url = "{!! route('helper.getTeamByRommID', ':id') !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (Object.keys(data).length > 0) {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectPosition')
                            .find('option')
                            .remove()
                        $('#selectTeam').append($('<option>', {
                            value: '',
                            text: "Vui lòng chọn bộ phận",
                            disabled: true,
                            selected: true,
                        }));
                        Object.keys(data).forEach(function(index) {
                            if (data[index]) {
                                if (Number.parseInt(teamID) == data[index].id) {
                                    $('#selectTeam').append($('<option>', {
                                        value: data[index].id,
                                        text: data[index].name,
                                        selected: true
                                    }));
                                } else {
                                    $('#selectTeam').append($('<option>', {
                                        value: data[index].id,
                                        text: data[index].name,
                                    }));
                                }
                            }
                        })
                    } else {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                    }
                }
            });
        } else {
            $('#selectTeam')
                .find('option')
                .remove()
        }
    }

    function loadPosition(id, poisitionID = null) {
        var url = "{!! route('helper.getPositionByTeamID', ':id') !!}";

        $.ajax({
            type: 'GET',
            url: url.replace(':id', id),
            success: function(data) {
                if (Object.keys(data).length > 0) {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    $('#selectPosition').append($('<option>', {
                        value: '',
                        text: "Vui lòng chọn chức danh",
                        disabled: true,
                        selected: true,
                    }));
                    Object.keys(data).forEach(function(index) {
                        if (data[index] != null) {
                            if (Number.parseInt(poisitionID) == data[index].id) {
                                $('#selectPosition').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name,
                                    selected: true,
                                }));
                            } else {
                                $('#selectPosition').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name
                                }));
                            }
                        }
                    })
                } else {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                }
            }
        });
    }
</script>
@endsection