@extends('layouts.fe.master2')
@section('title') @lang('translation.starter') @endsection
@section('content')
<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h3 class="d-inline m-0">DANH SÁCH PHIẾU ĐÁNH GIÁ PHỎNG VẤN</h3>
        <a href="/recruit/form/create">
            <button type="button" class="btn btn-success btn-label waves-effect waves-light"><i class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm</button>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle" style="width:100%">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Ứng viên</th>
                            <th>Họ tên người phỏng vấn</th>
                            <th>Phòng</th>
                            <th>Bộ phận</th>
                            <th>Vị trí tuyển dụng</th>
                            <th>Trạng thái</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1; @endphp
                        @foreach($dataInit['listData'] as $value)
                            @foreach($value['interview_scores'] as $interView)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $value['candidates_name'] }}</td>
                                <td>{{ $interView['confirm']['staff']['full_name'] }}</td>
                                <td>{{ $value['room']['name'] }}</td>
                                <td>{{ $value['team']['name'] }}</td>
                                <td>{{ $value['position']['name'] }}</td>
                                <td>
                                    @switch($interView['confirm']['status'])
                                        @case(0)
                                            Chưa xác nhận
                                            @break
                                        @case(1)
                                            Đạt 
                                            @break
                                        @case(2)
                                            Không đạt
                                            @break
                                        @case(3)
                                            Ý kiến khác
                                            @break
                                    @endswitch
                                </td>
                                <td>
                                    <div class="dropdown d-inline-block">
                                        <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="ri-more-fill align-middle"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-end">
                                            <li><a href="#" class="dropdown-item">
                                                    <i class="ri-eye-fill align-bottom me-2 text-muted"></i> Xem</a>
                                            </li>
                                            <li><a href="#" class="dropdown-item edit-item-btn">
                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Sửa</a>
                                            </li>
                                            <li>
                                                <a href="#" class="dropdown-item remove-item-btn">
                                                    <i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Xoá
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @php $i++;@endphp
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--end col-->
</div>
<!--end row-->

@endsection
@section('script')
<script>
    $(function() {
        var myTable = $('#example').dataTable();
    })
</script>

@endsection