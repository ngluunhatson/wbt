@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('content')
<div class="card">
 <div class="card-header">
  <h3>KẾT QUẢ PHỎNG VẤN</h3>
 </div>
 <div class="card-body">
  <div class="row">
   <div class="col-xl-4">
    <label for="borderInput" class="form-label">Phòng</label>
    <input type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
   </div>
   <div class="col-xl-4">
    <label for="borderInput" class="form-label">Bộ phận</label>
    <input type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
   </div>
   <div class="col-xl-4">
    <label for="borderInput" class="form-label">Chức danh</label>
    <input type="text" class="form-control border-dashed" id="borderInput" value="Enter your name" readonly>
   </div>
  </div>
 </div>
</div>

<div class="card">
 <div class="card-header">
  <div class="d-flex justify-content-between">
   <h3 class="m-0" style="width: fit-content;">Phần I: Đánh giá kết quả phỏng vấn</h3>
   <button type="button" class="btn btn-success position-relative d-inline" id="borderedToast2Btn"><i class="ri-error-warning-line fs-16 align-middle"></i></button>
   <div style="z-index: 11; position: absolute; top: 0; right: 0">
    <div id="borderedToast2" class="toast toast-border-success overflow-hidden mt-3" role="alert" aria-live="assertive" aria-atomic="true">
     <div class="toast-body">
      <div class="d-flex align-items-center">
       <div class="flex-column">
        <h6 class="mb-2">1- Biết</h6>
        <h6 class="mb-2">2- Hiểu phải làm gì</h6>
        <h6 class="mb-2">3- Làm - làm được, làm rõ được tình huống (clarify)</h6>
        <h6 class="mb-2">4- Thành thạo - có khả năng tổng hợp ý kiến từ những người có liên quan để hiểu gốc rễ (confirm)</h6>
        <h6 class="mb-0">5- Xuất sắc - lựa chọn giải pháp thay thế, đánh giá được thách thức, hướng tới kết quả win-win(create, challenge, commit)</h6>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
 <div class="card-body">
  <div class="row gy-3 align-items-end">
   <div class="col-xl-4">
    <label for="exampleInputdate" class="form-label">Từ ngày</label>
    <input type="date" class="form-control border-dashed" id="exampleInputdate" readonly>
   </div>
   <div class="col-xl-4">
    <input type="date" class="form-control border-dashed" id="exampleInputdate" readonly>
   </div>
   <div class="col-xl-4">
    <input type="date" class="form-control border-dashed" id="exampleInputdate" readonly>
   </div>


   <div class="col-12">
    <table class="table table-bordered table-nowrap text-center">
     <thead>
      <tr>
       <th style="width:60px;">STT</th>
       <th>TIÊU CHUẨN</th>
       <th style="width:200px;">P.QT HC-NS</th>
       <th style="width:200px;">TRƯỞNG PHÒNG</th>
       <th style="width:200px;">GIÁM ĐỐC</th>
      </tr>
     </thead>
     <tbody>
      @php $i = 1; @endphp
      @foreach(config('header.criteria') as $key => $value)

      <tr>
       <td>{{$i}}</td>

       @php $j = 0; @endphp
       @foreach ($value as $a)
       <td class="text-start">{{ $a['title'] }}</td>
       @php $j++; @endphp
       @endforeach
       <td>1</td>
       <td>3</td>
       <td>5</td>
      </tr>

      @php $i++; @endphp
      @endforeach
     </tbody>
    </table>
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="card">
  <div class="card-body">
   <div class="row">
    <div class="col-6">
     <h3 class="m-0">Phần II: Kết quả các bài kiểm tra chuyên môn (nếu có)</h3>
    </div>
    <div class="col-6">
     <div class="row ">
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="achieved" id="achieved-1">
       <h3 class="form-check-label ms-1" for="achieved-1">Đạt</label>
      </div>
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="achieved" id="achieved-2">
       <h3 class="form-check-label ms-1" for="achieved-2">Không đạt</h3>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="card">
  <div class="card-body">
   <div class="row">
    <div class="col-6">
     <h3 class="m-0">Phần III: Kết quả kiểm tra tính cách (nếu có)</h3>
    </div>
    <div class="col-6">
     <div class="row ">
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="disc" id="disc-1">
       <h3 class="form-check-label ms-1" for="disc-1">D</label>
      </div>
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="disc" id="disc-2">
       <h3 class="form-check-label ms-1" for="disc-2">I</h3>
      </div>
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="disc" id="disc-3">
       <h3 class="form-check-label ms-1" for="disc-3">S</h3>
      </div>
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="disc" id="disc-4">
       <h3 class="form-check-label ms-1" for="disc-4">C</h3>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="card">
  <div class="card-body">
   <div class="row">
    <div class="col-6">
     <h3 class="m-0">Mức lương đề xuất của ứng viên</h3>
    </div>
    <div class="col-6">
     <h3>10.000.000<span> đồng</span></h3>
    </div>
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="card">
  <div class="row card-body justify-content-around">
   <div class="col-xl-4">
    <h4 class="fw-bold text-center">P.QT HC-NS</h4>
    <h5 class="text-center">Huỳnh Khánh Linh</h5>
    <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
    <div class="row justify-content-around">
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="room-accept" id="room-accept-1">
      <label class="form-check-label ms-1" for="room-accept-1">Đạt</label>
     </div>
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="room-accept" id="room-accept-2">
      <label class="form-check-label ms-1" for="room-accept-2">Không đạt</label>
     </div>
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="room-accept" id="room-accept-3">
      <label class="form-check-label ms-1" for="room-accept-3">Ý kiến khác </label>
     </div>
    </div>
    <div class="col-12 mt-3">
     <label for="department_resource_notes" class="form-label">Lý do:</label>
     <textarea name="department_resource_notes" class="form-control" id="department_resource_notes" rows="3"></textarea>
    </div>
   </div>
   <div class="col-xl-4">
    <h4 class="fw-bold text-center">Trưởng phòng</h4>
    <h5 class="text-center">Huỳnh Khánh Linh</h5>
    <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
    <div class="row justify-content-around">
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="leader-accept" id="leader-accept-1">
      <label class="form-check-label ms-1" for="leader-accept-1">Đạt</label>
     </div>
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="leader-accept" id="leader-accept-2">
      <label class="form-check-label ms-1" for="leader-accept-2">Không đạt</label>
     </div>
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="leader-accept" id="leader-accept-3">
      <label class="form-check-label ms-1" for="leader-accept-3">Ý kiến khác </label>
     </div>
    </div>
    <div class="col-12 mt-3">
     <label for="department_resource_notes" class="form-label">Lý do:</label>
     <textarea name="department_resource_notes" class="form-control" id="department_resource_notes" rows="3"></textarea>
    </div>
   </div>
   <div class="col-xl-4">
    <h4 class="fw-bold text-center">Ban giám đốc</h4>
    <h5 class="text-center">Huỳnh Khánh Linh</h5>
    <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
    <div class="row justify-content-around">
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="ceo-accept" id="ceo-accept-1">
      <label class="form-check-label ms-1" for="ceo-accept-1">Đạt</label>
     </div>
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="ceo-accept" id="ceo-accept-2">
      <label class="form-check-label ms-1" for="ceo-accept-2">Không đạt</label>
     </div>
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="ceo-accept" id="ceo-accept-3">
      <label class="form-check-label ms-1" for="ceo-accept-3">Ý kiến khác </label>
     </div>
    </div>
    <div class="col-12 mt-3">
     <label for="department_resource_notes" class="form-label">Lý do:</label>
     <textarea name="department_resource_notes" class="form-control" id="department_resource_notes" rows="3"></textarea>
    </div>
   </div>

  </div>
 </div>
 @endsection
 @section('script')
 <script src="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.js') }}"></script>
 <script src="{{ URL::asset('assets/js/pages/form-pickers.init.js') }}"></script>
 <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>

 <script src="{{ URL::asset('assets/libs/prismjs/prismjs.min.js') }}"></script>
 <script src="{{ URL::asset('assets/js/pages/notifications.init.js') }}"></script>

 @endsection