@extends('layouts.fe.master2')
@section('title')
@lang('translation.starter') @endsection
@section('content')
<div class="card">
 <div class="card-header">
  <h3>KẾT QUẢ PHỎNG VẤN</h3>
 </div>
 <div class="card-body">
  <div class="row">
   <div class="col-xl-3">
    <label for="borderInput" class="form-label">Họ tên ứng viên</label>
    <input type="text" class="form-control" id="borderInput" value="Enter your name">
   </div>
   <div class="col-xl-3">
    <label for="exampleInputdate" class="form-label">Ngày phỏng vấn</label>
    <input type="date" class="form-control" id="exampleInputdate">
   </div>
   <div class="col-xl-3">
    <label for="borderInput" class="form-label">Phòng</label>
    <select class="form-select mb-3" aria-label=".form-select-lg example">
     <option selected>Open this select menu</option>
     <option value="1">One</option>
     <option value="2">Two</option>
     <option value="3">Three</option>
    </select>
   </div>
   <div class="col-xl-3">
    <label for="borderInput" class="form-label">Vị trí tuyển dụng</label>
    <select class="form-select mb-3" aria-label=".form-select-lg example">
     <option selected>Open this select menu</option>
     <option value="1">One</option>
     <option value="2">Two</option>
     <option value="3">Three</option>
    </select>
   </div>
  </div>
 </div>
</div>

<div class="card">
 <div class="card-header">
  <div class="d-flex justify-content-between">
   <h3 class="m-0" style="width: fit-content;">Phần I: Đánh giá kết quả phỏng vấn</h3>
   <i class="ri-error-warning-line fs-24 align-middle position-relative btn text-warning" id="borderedToast2Btn"></i>
   <div style="z-index: 11; position: absolute; top: 0; right: 0">
    <div id="borderedToast2" class="toast toast-border-success overflow-hidden mt-3" role="alert" aria-live="assertive" aria-atomic="true">
     <div class="toast-header">
      <span class="fw-bold me-auto">Hướng dẫn tính điểm theo thang điểm 5</span>
      <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
     </div>
     <div class="toast-body">
      <div class="d-flex align-items-center">
       <div class="flex-column">
        <h6 class="mb-2">1- Biết</h6>
        <h6 class="mb-2">2- Hiểu phải làm gì</h6>
        <h6 class="mb-2">3- Làm - làm được, làm rõ được tình huống (clarify)</h6>
        <h6 class="mb-2">4- Thành thạo - có khả năng tổng hợp ý kiến từ những người có liên quan để hiểu gốc rễ (confirm)</h6>
        <h6 class="mb-0">5- Xuất sắc - lựa chọn giải pháp thay thế, đánh giá được thách thức, hướng tới kết quả win-win(create, challenge, commit)</h6>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
 <div class="card-body">
  <div class="row gy-3 align-items-end">
   <div class="col-xl-4">
    <label for="exampleInputdate" class="form-label">Từ ngày</label>
    <input type="date" class="form-control border-dashed" id="exampleInputdate" readonly>
   </div>
   <div class="col-xl-4">
    <input type="date" class="form-control border-dashed" id="exampleInputdate" readonly>
   </div>
   <div class="col-xl-4">
    <input type="date" class="form-control border-dashed" id="exampleInputdate" readonly>
   </div>


   <div class="col-12 overflow-auto">
    <table class="table table-bordered table-nowrap text-center">
     <thead>
      <tr>
       <th class="text-start">Phần I: Đánh giá năng lực ứng viên</th>
       <th style="width:150px;">1</th>
       <th style="width:150px;">2</th>
       <th style="width:150px;">3</th>
       <th style="width:150px;">4</th>
       <th style="width:150px;">5</th>
      </tr>
     </thead>
     <tbody>
      @php $i = 1; @endphp
      @foreach(config('header.criteria') as $key => $value)

      <tr>
       @php $j = 0; @endphp
       @foreach ($value as $a)
       <td class="text-start">{{$i}}. {{ $a['title'] }}</td>
       @php $j++; @endphp
       @endforeach
       <td>
        <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="{{$a['name']}}">
       </td>
       <td>
        <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="{{$a['name']}}">
       </td>
       <td>
        <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="{{$a['name']}}">
       </td>
       <td>
        <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="{{$a['name']}}">
       </td>
       <td>
        <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="{{$a['name']}}">
       </td>
      </tr>

      @php $i++; @endphp
      @endforeach
     </tbody>
    </table>
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="card">
  <div class="card-body">
   <div class="row">
    <div class="col-6">
     <h3 class="m-0">Phần II: Kết quả các bài kiểm tra chuyên môn (nếu có)</h3>
    </div>
    <div class="col-6">
     <div class="row ">
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="achieved" id="achieved-1">
       <h3 class="form-check-label ms-1" for="achieved-1">Đạt</label>
      </div>
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="achieved" id="achieved-2">
       <h3 class="form-check-label ms-1" for="achieved-2">Không đạt</h3>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="card">
  <div class="card-body">
   <div class="row">
    <div class="col-6">
     <h3 class="m-0">Phần III: Kết quả kiểm tra tính cách (nếu có)</h3>
    </div>
    <div class="col-6">
     <div class="row ">
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="disc" id="disc-1">
       <h3 class="form-check-label ms-1" for="disc-1">D</label>
      </div>
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="disc" id="disc-2">
       <h3 class="form-check-label ms-1" for="disc-2">I</h3>
      </div>
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="disc" id="disc-3">
       <h3 class="form-check-label ms-1" for="disc-3">S</h3>
      </div>
      <div class="d-flex align-items-center" style="width: fit-content;">
       <input class="form-check-input" style="width:30px ; height:30px ;" type="radio" name="disc" id="disc-4">
       <h3 class="form-check-label ms-1" for="disc-4">C</h3>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="card">
  <div class="card-body">
   <div class="row">
    <div class="col-6">
     <h3 class="m-0">Mức lương đề xuất của ứng viên</h3>
    </div>
    <div class="col-6">
     <h3>10.000.000<span> đồng</span></h3>
    </div>
   </div>
  </div>
 </div>
</div>

<div class="row">
 <div class="card">
  <div class="row card-body justify-content-around">
   <div class="col-xl-4">
    <h4 class="fw-bold text-center">P.QT HC-NS</h4>
    <h5 class="text-center">Huỳnh Khánh Linh</h5>
    <h6 class="form-label text-center fst-italic mt-2">Đã xác nhận</h6>
    <div class="row justify-content-around">
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="room-accept" id="room-accept-1">
      <label class="form-check-label ms-1" for="room-accept-1">Đạt</label>
     </div>
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="room-accept" id="room-accept-2">
      <label class="form-check-label ms-1" for="room-accept-2">Không đạt</label>
     </div>
     <div style="width: fit-content;">
      <input class="form-check-input" type="radio" name="room-accept" id="room-accept-3">
      <label class="form-check-label ms-1" for="room-accept-3">Ý kiến khác </label>
     </div>
    </div>
    <div class="col-12 mt-3">
     <label for="department_resource_notes" class="form-label">Lý do:</label>
     <textarea name="department_resource_notes" class="form-control" id="department_resource_notes" rows="3"></textarea>
    </div>
   </div>
  </div>
 </div>
 @endsection
 @section('script')
 <script src="{{ URL::asset('assets/libs/@simonwep/@simonwep.min.js') }}"></script>
 <script src="{{ URL::asset('assets/js/pages/form-pickers.init.js') }}"></script>
 <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>

 <script src="{{ URL::asset('assets/libs/prismjs/prismjs.min.js') }}"></script>
 <script src="{{ URL::asset('assets/js/pages/notifications.init.js') }}"></script>

 @endsection