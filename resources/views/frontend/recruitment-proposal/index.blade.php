@extends('layouts.fe.master2')
@section('title')
@lang('translation.recruitment-proposal')
@endsection
@section('css')
<!--datatable css-->
@endsection
@section('content')

<div class="row">
    <h3>DANH SÁCH ĐỀ XUẤT TUYỂN DỤNG</h3>
</div>

<div class="row">
    <div class="col-12 d-flex justify-content-between mb-3">
        <h3 class="d-inline m-0"></h3>
        <a href="/recruitment-proposal/create">
            <button type="button" class="btn btn-success btn-label waves-effect waves-light"><i
                    class="ri-add-circle-fill label-icon align-middle fs-16 me-2"></i>Thêm</button>
        </a>
    </div>
</div>
<div class="row">
    <div class="card">
        <div class="card-body">
            <table id="example" class="table table-bordered dt-responsive nowrap table-striped align-middle"
                style="width:100%">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Phòng</th>
                        <th>Bộ Phận</th>
                        <th>Chức Danh</th>
                        <th>Người tạo</th>
                        <th>Trạng thái</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($dataInit['listData'] as $key => $value)
                    <tr>
                        <td class="dtr-control">{{ $key + 1 }}</td>
                        <td>{{ $value->room != NULL ? $value->room->name : "NULL" }}</td>
                        <td>{{ $value->team != NULL ? $value->team->name : "NULL" }}</td>
                        <td>{{ $value->position != NULL ? $value->position->name : "NULL" }}</td>
                        <td>{{ $value->user->name }}</td>
                        <td>{{ config('constants.status_confirm.recruitment-proposal')[$value->status] }}</td>
                        <td>
                            <div class="dropdown d-inline-block">
                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-fill align-middle"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    @can('recruit_proposal-read')
                                    <li><a href="/recruitment-proposal/view/{{$value->id}}" class="dropdown-item"><i
                                                class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
                                    @endcan
                                    @can('recruit_proposal-edit')
                                    @if (auth()->id() == $value->created_by && (!in_array($value->status, [3,4,5,9])))
                                    <li><a href="/recruitment-proposal/edit/{{$value->id}}"
                                            class="dropdown-item edit-item-btn"><i
                                                class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                                    @endif
                                    @endcan
                                    @can('recruit_proposal-delete')
                                    @if (auth()->id() == $value->created_by && (!in_array($value->status, [3,4,5,9])))
                                    <li><a href="#" class="dropdown-item remove-item-btn"><i
                                                class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>Delete</a>
                                    </li>
                                    <form method="post" id="formDelete"
                                        action="{{ route('recruitment-proposal.delete', $value->id) }}"
                                        style="display:inline">
                                        @method('delete')
                                        @csrf
                                    </form>
                                    @endif
                                    <!-- <li>
                                        <a href="/recruitment-proposal/delete/{{$value->id}}" class="dropdown-item remove-item-btn">
                                            <i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Delete
                                        </a>
                                    </li> -->
                                    @endcan
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
@section('script')
<script>
$(function() {
    $('#example').dataTable()
    $('#example tbody').on('click', '.remove-item-btn', function(event) {
        var form = $(this).closest("form");
        event.preventDefault();
        swal.fire({
                title: 'Bạn muốn xóa dữ liệu này ?',
                text: "Lưu ý : dữ liệu được xóa sẽ đi và nếu muốn khôi phục hãy liên lạc admin",
                icon: 'warning',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Đồng ý',
                showDenyButton: true,
                denyButtonText: 'Hủy bỏ',
            })
            .then((result) => {
                if (result.isConfirmed) {
                    $('#formDelete').submit();
                } else if (result.isDenied) {
                    Swal.fire('Dữ liệu được giữ lại', '', 'info')
                }
            });
    });
});
</script>
@endsection