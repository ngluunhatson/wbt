@extends('layouts.fe.master2')
@section('title')
@lang('translation.personal')
@endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')

<style>
    .table td p {
        margin: auto;
    }

    .table>:not(caption)>*>* {
        padding: 6px;
    }

    .table th {
        font-weight: 700;
        text-align: center;
    }

    .brl-radio {
        border-left: 1px solid;
        padding: 6px;
    }

    .form-label {
        margin: 0;
    }

    @print {
        @page :footer {
            display: none
        }

        @page :header {
            display: none
        }
    }

    @media print {
        /* .address {
            margin-top: 77% !important;
        } */

        #btnPrint {
            display: none !important;
        }
    }
</style>
<div id="btnPrint" class="justify-content-between pe-3 mb-3 d-flex">
    <h3></h3>
    <a href="#">
        <button type="button" id="btnPrint" class="btn btn-primary btn-label waves-effect waves-light">
            <i class="ri-printer-line label-icon align-middle fs-16 me-2"></i>IN</button>
    </a>
</div>
<div class="d-flex justify-content-between">
    <div>
        <h3 class="mt-4 fw-bold">PHIẾU ĐỀ XUẤT TUYỂN DỤNG</h3>
        <div style = "width:26em;">
            <label for="room" class="form-label col-4">Phòng</label>
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['recruit']['room']) ? $dataInit['recruit']['room']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div style = "width:26em;">
            <label for="room" class="form-label col-4">Bộ phận</label>
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['recruit']['team']) ? $dataInit['recruit']['team']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div style = "width:26em;">
            <label for="room" class="form-label col-4">Vị trí tuyển dụng</label>
            <span class="form-label" style="font-size: 15px;">
                {{ !empty($dataInit['recruit']['position']) ? $dataInit['recruit']['position']['name'] : 'Chưa có' }}
            </span>
        </div>
        <div class="col-sm-5">
        </div>
    </div>
    <div style="width:35% ; height: fit-content;">
        <img style="object-fit: contain; width: 100%;height: 100%;" src="{{ url('/uploads/common/logo.png') }}">
    </div>
</div>
<div class="row card-body">
    <table class="table table-bordered border-dark dt-responsive nowrap align-middle" style="width:100%; background: white;">
        <thead>
            <tr>
                <th>STT</th>
                <th>HẠNG MỤC</th>
                <th>YÊU CẦU</th>
            </tr>
        </thead>
        <tbody>
            @php $i = 1; @endphp
            @foreach(config('constants.header_recruit') as $key => $value)
            <tr>
                <td class="text-center" style="width: 7%;">{{ $i }}</td>
                <td style="width: 20%;">{{ $value }}</td>
                @if ($key == 'recruitment_purpose')
                <td class="p-0">
                    <div class="row px-2">
                        <div class="col-6 d-flex align-items-center">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" @checked($dataInit['recruit']['recruitment_purpose']==1) id="new" value="1" name="recruitment_purpose">
                            <label for="new" class=" form-label mb-0 ">Tuyển mới</label>
                        </div>
                        <div class="col-6 d-flex align-items-center brl-radio">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" @checked($dataInit['recruit']['recruitment_purpose']==2) id="replace" value="2" name="recruitment_purpose">
                            <label for="replace" class=" form-label mb-0 ">Tuyển thay
                                thế</label>
                        </div>
                        @error('recruitment_purpose')
                        <small class="help-block text-danger">{{ $message }} *</small>
                        @enderror
                    </div>
                    <div class="col-12 p-2" style="border-top: 1px solid;">
                        <label for="reason" class="form-label ">Lý
                            do: {{ old('reason', $dataInit['recruit']['reason']) }}</label>
                    </div>
                </td>
                @elseif( $key == 'type_work')
                <td class="p-0">
                    <div class="row px-2">
                        <div class="col-6 d-flex align-items-center">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" id="full" @checked($dataInit['recruit']['type_work']==1) value="1" name="type_work">
                            <label for="new" class=" form-label mb-0 ">Toàn thời gian</label>
                        </div>
                        <div class="col-6 d-flex align-items-center brl-radio">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" id="co-operate" @checked($dataInit['recruit']['type_work']==2) value="2" name="type_work">
                            <label for="replace" class=" form-label mb-0 ">Hợp tác</label>
                        </div>
                    </div>
                </td>
                @elseif ($key == 'manpower_demarcation')

                <td class="p-0">
                    <div class="row px-2">
                        <div class="col-6 d-flex align-items-center">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" id="plan" @checked($dataInit['recruit']['manpower_demarcation']==1 ) value="1" name="manpower_demarcation">
                            <label for="new" class=" form-label mb-0 ">Theo kế hoạch năm</label>
                        </div>
                        <div class="col-6 d-flex align-items-center brl-radio">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" id="budget" @checked($dataInit['recruit']['manpower_demarcation']==2) value="2" name="manpower_demarcation">
                            <label for="replace" class=" form-label mb-0 ">Theo ngân sách năm</label>
                        </div>
                    </div>
                </td>

                @elseif ($key == 'gender')

                <td class="p-0">
                    <div class="row px-2">
                        <div class="col-3 d-flex align-items-center">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" id="male" @checked($dataInit['recruit']['gender']==1) value="1" name="gender">
                            <label for="new" class=" form-label mb-0 ">Nam</label>
                        </div>
                        <div class="col-3 d-flex align-items-center brl-radio">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" id="female" @checked($dataInit['recruit']['gender']==0) value="0" name="gender">
                            <label for="replace" class=" form-label mb-0 ">Nữ</label>
                        </div>
                        <div class="col-6 d-flex align-items-center brl-radio">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" @checked($dataInit['recruit']['gender']==2) id="noRequire" value="2" name="gender">
                            <label for="replace" class=" form-label mb-0 ">Không yêu cầu</label>
                        </div>
                    </div>
                </td>

                @elseif ($key == 'academic_level')

                <td class="p-0">
                    <div class="row px-2">
                        <div class="col-3 d-flex align-items-center">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" id="university" @checked($dataInit['recruit']['academic_level'] !=null && $dataInit['recruit']['academic_level']==0) value="0" name="academic_level">
                            <label for="university" class=" form-label mb-0 ">Đại học</label>
                        </div>
                        <div class="col-3 d-flex align-items-center brl-radio">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" id="college" @checked($dataInit['recruit']['academic_level'] !=null && $dataInit['recruit']['academic_level']==1) value="1" name="academic_level">
                            <label for="college" class=" form-label mb-0 ">Cao đẳng</label>
                        </div>
                        <div class="col-3 d-flex align-items-center brl-radio">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" id="intermediate" @checked($dataInit['recruit']['academic_level'] !=null && $dataInit['recruit']['academic_level']==2) value="2" name="academic_level">
                            <label for="intermediate" class=" form-label mb-0 ">Trung cấp</label>
                        </div>
                        <div class="col-3 brl-radio">
                            <label for="intermediate" class=" form-label mb-0 ">Khác: {{ $dataInit['recruit']['academic_level_other'] }}</label>
                        </div>
                    </div>
                </td>

                @elseif ($key == 'computer_skill')

                <td class="p-0">
                    <div class="row px-2">
                        <div class="col-6 d-flex align-items-center">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" @checked($dataInit['recruit']['computer_skill']==1) id="mso" value="1" name="computer_skill">
                            <label for="mso" class=" form-label mb-0 ">MS Office</label>
                        </div>
                        <div class="col-6 d-flex align-items-center brl-radio">
                            <input disabled class="form-check-input fs-15 me-2" type="radio" id="diffInfomatics" @checked($dataInit['recruit']['computer_skill']==2) value="2" name="computer_skill">
                            <label for="diffInfomatics" class=" form-label mb-0 ">Khác : {!! old('computer_skill_other', $dataInit['recruit']['computer_skill_other']) !!}</label>
                        </div>
                    </div>
                </td>

                @else
                <td class="">{{ $dataInit['recruit'][$key] }}</td>
                @endif
            </tr>
            @php $i ++; @endphp
            @endforeach
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="col-12 d-flex mt-2 justify-content-end" style="padding-right: 2%;">
                    @if (!empty($dataInit['recruit']['review_date']))
                    <label for="confirm" class=" form-label mb-0 me-5 " style="margin-top: 2px">
                        @php
                        $date = \Carbon\Carbon::parse($dataInit['recruit']['review_date']);
                        @endphp
                        Ngày duyệt : Ngày {{ $date->day }} Tháng {{ $date->month }} Năm {{ $date->year }}</label>
                    @endif
                </div>
                <div class="row mb-5">
                    <div class="col-3 d-flex justify-content-center">
                        <div class="row  ">
                            <div class="col-sm-12 text-center">
                                <h5 class="fw-bold">Người giao việc</h5>
                            </div>
                            <div class="col-12 text-center">
                                <label for="firstNameinput" class="form-label mb-0">{{ $dataInit['recruit']['user']['name'] }}</label>
                            </div>
                            <div class="col-12 text-center">
                                @if($dataInit['recruit']['status'] != 1)
                                <label for="firstNameinput" class="form-label mb-0 mt-4 ">Đã xác nhận</label>
                                @else
                                <div class="col-12 d-flex justify-content-center mt-4">
                                    <div class=" form-check" style="width: fit-content;">
                                        <input disabled class="form-check-input fs-15" type="checkbox" id="assign" value="1" name="assign">
                                    </div>
                                    <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác
                                        nhận</label>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--end col Người giao việc-->
                    @php
                    $idConfirmer = [];
                    $id;
                    $idConfirmerNext;
                    $type;
                   
                    @endphp
                    @foreach( $dataInit['recruit']['confirms'] as $key => $value)
                    <input name="confirm_id_{{$key+1}}" hidden>
                    <div class="col-3 d-flex justify-content-center">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                @switch($value['confirm']['type_confirm'])
                                @case(4)
                                @php
                                if ($dataInit['recruit']['status'] == 2) {
                                $idConfirmer[] = $value['confirm']['basic_info_id'];
                                }
                                $id = $value['confirm']['id'];
                                $type = 4;
                                @endphp
                                <h5 class="fw-bold">Quản Lý Phòng</h5>
                                @break
                                @case(2)
                                @php
                                $idConfirmerNext = $dataInit['recruit']['status'] == 2   ? $value['confirm']['basic_info_id'] : '';
                                @endphp
                                @if ((in_array($dataInit['recruit']['status'], [3, 9])) && empty($idConfirmer) && $value['confirm']['status'] == 0)
                                @php
                                $idConfirmer[] = $value['confirm']['basic_info_id'];
                                $id = $value['confirm']['id'];
                                $type = 2;
                                @endphp
                                @endif
                                <h5 class="fw-bold">P.QT HC-NS</h5>
                                @break
                                @case(3)
                                @php
                                if ($dataInit['recruit']['status'] == 3) {
                                $idConfirmerNext = $value['confirm']['basic_info_id'];
                                }
                                @endphp
                                @if ($dataInit['recruit']['status'] == 4 && empty($idConfirmer) && $value['confirm']['status'] == 0)
                                @php
                                $idConfirmer[] = $value['confirm']['basic_info_id'];
                                $id = $value['confirm']['id'];
                                $type = 3;
                                @endphp
                                @endif
                                <h5 class="fw-bold">Ban Giám Đốc</h5>
                                @break
                                @endswitch
                            </div>
                            <div class="col-sm-12 text-center">
                                @switch($value['confirm']['type_confirm'])
                                @case(1)
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff'] == null ? 'Chưa có' :  $value['confirm']['staff']['full_name'] }}</label>
                                @break
                                @case(2)
                                @if(!empty($dataInit['hr']))
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff'] == null ? 'Chưa có' :  $value['confirm']['staff']['full_name'] }}</label>
                                @endif
                                @break
                                @case(3)
                                @if(!empty($dataInit['manager']))
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff'] == null ? 'Chưa có' :  $value['confirm']['staff']['full_name'] }}</label>
                                @endif
                                @break
                                @case(4)
                                <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff'] == null ? 'Chưa có' :  $value['confirm']['staff']['full_name']}}</label>
                                @break
                                @endswitch
                            </div>
                            <div class="col-sm-12 text-center">
                                <div class="col-12 text-center mt-2">
                                    @if($value['confirm']['type_confirm'] != 1)
                                    @switch($value['confirm']['status'])
                                    @case(0)
                                    <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff'] == null ? 'Không cần xác nhận' : 'Chưa xác nhận' }}</label>
                                    @break
                                    @case(1)
                                    <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff'] == null ? 'Không cần xác nhận' : 'Đã xác nhận' }}</label>
                                    @break
                                    @case(2)
                                    <label for="firstNameinput" class="form-label mb-0">{{ $value['confirm']['staff'] == null ? 'Không cần từ chối' : 'Từ chối' }}</label>
                                    <div>
                                        <label>Lý do : {{ $value['confirm']['note']}}</label>
                                    </div>
                                    @break
                                    @endswitch
                                    @else
                                    <label for="firstNameinput" class="form-label mb-0"></label>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @if (in_array(auth()->user()->basic_info_id, $idConfirmer))
                    <form method="POST" action="{{ route('recruitment-proposal.updateStatus', $dataInit['recruit']['id']) }}" id="formConfirm">
                        @csrf
                        <input hidden name="confirmID" value="{{ $id }}">
                        <input hidden name="confimerIDNext" value="{{ $idConfirmerNext }}">
                        <input hidden name="reason" id="reasonConfirm">
                        <input hidden name="statusConfirm" id="statusConfirm" value="1">
                        <input hidden name="type" value="{{ $type }}">
                        <div class="row justify-content-center pe-3 mb-3 mt-3">
                            <div class="col-3 justify-content-between d-flex">
                                <button data-id="1" type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content">
                                    <i class="ri-check-double-line label-icon align-middle fs-16 me-2"></i> Duyệt</button>
                                <button data-id="2" type="submit" id="btnDenie" class="btn btn-danger waves-effect waves-light" style="width: fit-content">
                                    <i class="ri-error-warning-line label-icon align-middle rounded-pill fs-16 me-2 "></i> Không Duyệt</button>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
            <div class="card-body mt-5">
                <p class="fw-bold m-0">ActionCOACH CBD Firm</p>
                <p class="m-0">Địa chỉ: CBD Building, 90 - 92 Lê Thị Riêng, Phường Bến Thành, Quận 1, TP.HCM</p>
                <p class="m-0">Hotline: 1800 8087</p>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#btnPrint').on('click', function() {
            window.print()
        })
        $('#btnDenie').on('click', function(event) {
            var status = $(this).data('id');
            var form = $(this).closest("form");
            event.preventDefault();
            swal.fire({
                    title: 'Lý do không duyệt đơn',
                    html: `<input class="form-control" type="text" id="reason"  placeholder="Lý do" required>`,
                    icon: 'warning',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Đồng ý',
                    showDenyButton: true,
                    denyButtonText: 'Hủy',
                    preConfirm: () => {
                        const reason = Swal.getPopup().querySelector('#reason').value
                        if (!reason) {
                            Swal.showValidationMessage(`Vui lòng nhập lý do`)
                        }
                        return {
                            reason: reason,
                        }
                    }
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        $('#reasonConfirm').val(result.value.reason);
                        $('#statusConfirm').val(2);
                        form.submit();
                    } else if (result.isDenied) {
                        Swal.fire('Your record is safe', '', 'info')
                    }

                });
        });
    });
</script>
@endsection