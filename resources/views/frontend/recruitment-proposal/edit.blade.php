@extends('layouts.fe.master2')
@section('title')
@lang('translation.personal')
@endsection
@section('css')
<link href="{{ URL::asset('/assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<form method="PUT" id="formUpdate" enctype="multipart/form-data">
    @csrf
    <input hidden name="recruit_id" value="{{ $dataInit['recruit']['id'] }}">
    <div class="row ">
        <h3 class="text-center">ĐỀ XUẤT TUYỂN DỤNG</h3>
    </div>
    <div class="row justify-content-between pe-3 mb-3">
        <h3 style="width: fit-content" class="m-0"></h3>
        <button type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content"><i class="ri-save-3-line label-icon  fs-16 "></i> Lưu</button>
    </div>
    <!--end col Quốc tịch-->

    <div class="row">
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <label for="firstNameinput" class="form-label">Phòng</label>
                    <div class="col-xl-12">
                        <select name="room_id" id="selectRoom" class="form-select" aria-label="Default select example">
                            <option value="">Vui lòng chọn phòng ban</option>
                            @foreach($dataInit['room'] as $value)
                            <option @if($dataInit['recruit']['room_id']==$value['id']) selected @endif value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                            @endforeach

                        </select>
                    </div>
                </div>
            </div>
            <!--end col Phòng-->

            <div class="col-4">
                <div class="mb-3">
                    <label for="firstNameinput" class="form-label">Bộ phận</label>
                    <div class="col-xl-12">
                        <select name="team_id" id="selectTeam" class="form-select" aria-label="Default select example">
                        </select>
                    </div>
                </div>
            </div>
            <!--end col Bộ phận-->

            <div class="col-4">
                <div class="mb-3">
                    <label for="firstNameinput" class="form-label">Chức danh</label>
                    <div class="col-xl-12">
                        <select name="position_id" id="selectPosition" class="form-select" aria-label="Default select example">
                        </select>
                    </div>
                </div>
            </div>
            <!--end col Chức danh-->
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row ">
                            <div class="row">

                                <div class="col-6 ">
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="number" class="form-label " style="margin-top: 10px">1. Số lượng
                                                tuyển dụng</label>
                                        </div>

                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control @error('number_vacanciess') is-invalid @enderror" placeholder="(Điền thông tin)" id="number_vacancies" name="number_vacancies" value="{{ old('number_vacancies', $dataInit['recruit']['number_vacancies']) }}">
                                            @error('number_vacancies')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 1 Số lượng tuyển dụng-->
                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="purpose" class="form-label" style="margin-top: 10px">2. Mục đích
                                                    tuyển dụng</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-6 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" @checked($dataInit['recruit']['recruitment_purpose']==1) id="new" value="1" name="recruitment_purpose">
                                                    </div>
                                                    <label for="new" class=" form-label mb-0 " style="margin-top: 2px">Tuyển mới</label>
                                                </div>
                                                <div class="col-6 d-flex justify-content-end align-items-center ">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" @checked($dataInit['recruit']['recruitment_purpose']==2) id="replace" value="2" name="recruitment_purpose">
                                                    </div>
                                                    <label for="replace" class=" form-label mb-0 " style="margin-top: 2px">Tuyển thay
                                                        thế</label>
                                                </div>
                                                @error('recruitment_purpose')
                                                <small class="help-block text-danger">{{ $message }} *</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex">
                                            <div class="col-4 form-group">
                                                <label for="reason" class="form-label " style="margin-top: 10px">Lý
                                                    do:</label>
                                            </div>
                                            <div class="col-8">
                                                <input type="text" class="form-control" placeholder="(Điền thông tin)" id="reason" name="reason" value="{{ old('reason', $dataInit['recruit']['reason']) }}">
                                                @error('reason')
                                                <small class="help-block text-danger">{{ $message }} *</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col 2 Mục đích tuyển dụng-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="time" class="form-label " style="margin-top: 10px">3. Thời gian
                                                tuyển dụng</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control datepicker" placeholder="(Điền thông tin)" id="recruitment_time" name="recruitment_time" value="{{ old('recruitment_time', $dataInit['recruit']['recruitment_time']) }}">
                                            @error('recruitment_time')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 3 Thời gian tuyển dụng-->
                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="type" class="form-label" style="margin-top: 10px">4. Loại
                                                    hình làm việc</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-6 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="full" @checked($dataInit['recruit']['type_work']==1) value="1" name="type_work">
                                                    </div>
                                                    <label for="full" class=" form-label mb-0 " style="margin-top: 2px">Toàn thời gian</label>
                                                </div>
                                                <div class="col-6 d-flex justify-content-end align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="co-operate" @checked($dataInit['recruit']['type_work']==2) value="2" name="type_work">
                                                    </div>
                                                    <label for="co-operate" class=" form-label mb-0 " style="margin-top: 2px">Hợp tác</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col 4 Loại hình làm việc-->
                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="demarcation" class="form-label" style="margin-top: 10px">5. Định
                                                    biên nhân sự</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-6 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="plan" @checked($dataInit['recruit']['manpower_demarcation']==1 ) value="1" name="manpower_demarcation">
                                                    </div>
                                                    <label for="plan" class=" form-label mb-0 " style="margin-top: 2px">Theo kế hoạch năm</label>
                                                </div>
                                                <div class="col-6 d-flex justify-content-end align-items-center ">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="budget" @checked($dataInit['recruit']['manpower_demarcation']==2) value="2" name="manpower_demarcation">
                                                    </div>
                                                    <label for="budget" class=" form-label mb-0 " style="margin-top: 2px">Theo ngân sách năm</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col 5 Định biên nhân sự-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="age" class="form-label " style="margin-top: 10px">6. Độ
                                                tuổi</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="number" class="form-control" placeholder="(Điền thông tin)" value="{{ old('age', $dataInit['recruit']['age']) }}" id="age" name="age">
                                            @error('age')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 6 Độ tuổi-->
                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="gender" class="form-label" style="margin-top: 10px">7. Giới
                                                    tính</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-4 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="male" @checked($dataInit['recruit']['gender']==1) value="1" name="gender">
                                                    </div>
                                                    <label for="male" class=" form-label mb-0 " style="margin-top: 2px">Nam</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-center align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="female" @checked($dataInit['recruit']['gender']==0) value="0" name="gender">
                                                    </div>
                                                    <label for="female" class=" form-label mb-0 " style="margin-top: 2px">Nữ</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-end align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" @checked($dataInit['recruit']['gender']==2) id="noRequire" value="2" name="gender">
                                                    </div>
                                                    <label for="noRequire" class=" form-label mb-0 " style="margin-top: 2px">Không yêu cầu</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col 7 Giới tính-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="experience_year" class="form-label " style="margin-top: 10px">8. Số năm
                                                kinh nghiệm</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="number" class="form-control" placeholder="(Điền thông tin)" value="{{ old('experience_year', $dataInit['recruit']['experience_year']) }}" id="experience_year" name="experience_year">
                                            @error('experience_year')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 8. Số năm kinh nghiệm-->
                                </div>
                                <!--end col 1->8-->


                                <div class="col-6 ">

                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="education" class="form-label" style="margin-top: 10px">9.
                                                    Trình độ học vấn</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-4 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="university" @checked($dataInit['recruit']['academic_level'] !=null && $dataInit['recruit']['academic_level']==0) value="0" name="academic_level">
                                                    </div>
                                                    <label for="university" class=" form-label mb-0 " style="margin-top: 2px">Đại học</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-center align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="college" @checked($dataInit['recruit']['academic_level'] !=null && $dataInit['recruit']['academic_level']==1) value="1" name="academic_level">
                                                    </div>
                                                    <label for="college" class=" form-label mb-0 " style="margin-top: 2px">Cao đẳng</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-end align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="intermediate" @checked($dataInit['recruit']['academic_level'] !=null && $dataInit['recruit']['academic_level']==2) value="2" name="academic_level">
                                                    </div>
                                                    <label for="intermediate" class=" form-label mb-0 " style="margin-top: 2px">Trung cấp</label>
                                                </div>
                                            </div>

                                            @error('academic_level')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                        <div class="col-12 d-flex">
                                            <div class="col-4">
                                                <!-- <input class="form-check-input fs-15" type="radio" id="intermediate" value="2" name="academic_level">
                                                <label for="intermediate" class=" form-label mb-0 " style="margin-top: 2px">Khác</label> -->
                                            </div>
                                            <div class="col-8 d-flex px-3 form-group">
                                                <div class="col-4 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" @checked($dataInit['recruit']['academic_level'] !=null && $dataInit['recruit']['academic_level']==3) id="university" value="3" name="academic_level">
                                                    </div>
                                                    <label for="university" class=" form-label mb-0 " style="margin-top: 2px">Khác</label>
                                                </div>
                                                <div class="col-8 d-flex justify-content-start align-items-center form-group">
                                                    <input type="text" class="form-control" placeholder="(Điền thông tin)" value="{{ old('academic_level_other', $dataInit['recruit']['academic_level_other']) }}" id="diff" name="academic_level_other">
                                                    @error('academic_level_other')
                                                    <small class="help-block text-danger">{{ $message }} *</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col 9 Trình độ học vấn-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="specialize" class="form-label " style="margin-top: 10px">10. Trình
                                                độ chuyên môn</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Ghi rõ chi tiết các yêu cầu)" value="{{ old('qualification', $dataInit['recruit']['qualification']) }}" id="specialize" name="qualification">
                                            @error('qualification')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 10. Trình độ chuyên môn-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="mission" class="form-label " style="margin-top: 10px">11. Nhiệm
                                                vụ công việc</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Link thông tin từ 12 TC) Có thể chỉnh sửa" id="mission" value="{{ old('job_duties', $dataInit['recruit']['job_duties']) }}" name="job_duties">
                                            @error('job_duties')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 11. Nhiệm vụ công việc-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="disc" class="form-label " style="margin-top: 10px">12.
                                                DISC</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Link thông tin từ 12 TC) Có thể chỉnh sửa" value="{{ old('disc', $dataInit['recruit']['disc']) }}" id="disc" name="disc">
                                            @error('disc')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 12. DISC-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="language" class="form-label " style="margin-top: 10px">13. Trình
                                                độ ngoại ngữ</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Ghi rõ chi tiết các yêu cầu)" value="{{ old('english_level', $dataInit['recruit']['english_level']) }}" id="language" name="english_level">
                                            @error('english_level')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 13. Trình độ ngoại ngữ-->
                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="Informatics" class="form-label" style="margin-top: 10px">14.
                                                    Trình độ tin học</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-4 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" @checked($dataInit['recruit']['computer_skill']==1) id="mso" value="1" name="computer_skill">
                                                    </div>
                                                    <label for="mso" class=" form-label mb-0 " style="margin-top: 2px">MS Office</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-center align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="diffInfomatics" @checked($dataInit['recruit']['computer_skill']==2) value="2" name="computer_skill">
                                                    </div>
                                                    <label for="diffInfomatics" class=" form-label mb-0 " style="margin-top: 2px">Khác</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-end align-items-center">
                                                    <textarea type="text" class="form-control" placeholder="" id="Informatics" name="computer_skill_other">
                                                    @if ($dataInit['recruit']['computer_skill'] == 2)
                                                    {!! old('computer_skill_other', $dataInit['recruit']['computer_skill_other']) !!}
                                                    @endif
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col 14. Trình độ tin học-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="company" class="form-label " style="margin-top: 10px">15. Yêu cầu thiết bị</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Ghi rõ chi tiết các yêu cầu)" value="{{ old('device_requirements', $dataInit['recruit']['device_requirements']) }}" id="company" name="device_requirements">
                                        </div>
                                    </div>
                                    <!--end col 15. Yêu cầu thiết bị-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="company" class="form-label " style="margin-top: 10px">16. Yêu cầu khác</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Ghi rõ chi tiết các yêu cầu)" value="{{ old('other_requirements', $dataInit['recruit']['other_requirements']) }}" id="company" name="other_requirements">
                                        </div>
                                    </div>
                                    <!--end col 16. Yêu cầu khác -->

                                </div>
                                <!--end col 9->16-->
                            </div>
                            <!--end row-->
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">


                        <div class="row mb-5">
                            <div class="col-3 d-flex justify-content-center">
                                <div class="row  ">
                                    <div class="col-12 text-center">
                                        <h5>Người Giao Việc</h5>
                                    </div>
                                    <div class="col-12 text-center">
                                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ $dataInit['recruit']['user']['name'] }}</label>
                                    </div>
                                    <div class="col-12 text-center">
                                        @if($dataInit['recruit']['status'] != 1)
                                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">Đã xác nhận</label>
                                        @else
                                        <div class="col-12 d-flex justify-content-center mt-4">
                                            <div class=" form-check" style="width: fit-content;">
                                                <input class="form-check-input fs-15" type="checkbox" id="assign" value="1" name="assign">
                                            </div>
                                            <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác
                                                nhận</label>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--end col Người giao việc-->

                            @foreach( $dataInit['recruit']['confirms'] as $key => $value)
                            <input name="confirm_id_{{$key+1}}" value="{{$value['confirm']['id']}}" hidden>
                            <div class="col-3 d-flex justify-content-center">
                                <div class="row">
                                    <div class="col-12 text-center">
                                        @switch($value['confirm']['type_confirm'])
                                        @case(2)
                                        <h5>P.QT HC-NS</h5>
                                        @break
                                        @case(3)
                                        <h5>Ban Giám Đốc</h5>
                                        @break
                                        @case(4)
                                        <h5>Quản Lý Phòng</h5>
                                        @break
                                        @endswitch
                                    </div>
                                    <div class="col-12">
                                        @switch($value['confirm']['type_confirm'])
                                        @case(2)
                                        @if(!empty($dataInit['hr']))
                                        <select name="selectConfirm[]" class="form-select text-center mt-2" aria-label="Default select example">
                                            @foreach($dataInit['hr'] as $hr)
                                            @if(!empty($hr['basic_info_id']))
                                            <option @if($hr->basic_info_id == $value['confirm']['basic_info_id']) selected @endif
                                                value="{{ $hr->basic_info_id}}">{{ $hr->staff->full_name }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @endif
                                        @break
                                        @case(3)
                                        @if(!empty($dataInit['manager']))
                                        <select name="selectConfirm[]" class="form-select text-center mt-2" aria-label="Default select example">
                                            @foreach($dataInit['manager'] as $manager)
                                            @if(!empty($manager['basic_info_id']))
                                            <option @if($manager->basic_info_id == $value['confirm']['basic_info_id']) selected @endif
                                                value="{{ $manager->basic_info_id }}">{{ $manager->staff->full_name }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @endif
                                        @break
                                        @case(4)
                                        <select id="selectManagerRoom" data-old="{{ $value['confirm']['basic_info_id'] }}" name="selectConfirm[]" class="form-select text-center mt-2" aria-label="Default select example">
                                        </select>
                                        @break
                                        @endswitch
                                    </div>
                                    <div class="col-12 text-center mt-2 row">
                                        @if($value['confirm']['type_confirm'] != 1)
                                        @switch($value['confirm']['status'])
                                        @case(0)
                                        <label for="firstNameinput" class="form-label mb-0">{{ 'Chưa xác nhận' }}</label>
                                        @break
                                        @case(1)
                                        <label for="firstNameinput" class="form-label mb-0">{{ 'Đã xác nhận' }}</label>
                                        @break
                                        @case(2)
                                        <label for="firstNameinput" class="form-label mb-0">{{ 'Từ chối' }}</label>
                                        <div>
                                            <label>Lý do : {{ $value['confirm']['note']}}</label>
                                        </div>
                                        @break
                                        @endswitch
                                        @else
                                        <label for="firstNameinput" class="form-label mb-0"></label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!--end col Ban giám đốc-->
                        </div>
                        <!-- <div class="row justify-content-end">

                            <div class="col-4 d-flex justify-content-center">
                                <div class="col-12 text-center">
                                    <textarea class="form-control" aria-label="With textarea" rows="5" id="texta1" name="texta1"></textarea>
                                </div>
                            </div>
                            <div class="col-4 d-flex justify-content-center">
                                <div class="col-12 text-center">
                                    <textarea class="form-control" aria-label="With textarea" rows="5" id="texta2" name="texta2"></textarea>
                                </div>
                            </div>

                        </div> -->
                    </div>
                </div>
            </div>


        </div>
    </div>
</form>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('input[name=academic_level]').on('click', (event) => {
            if ($(event.target).val() == 3) {
                $('#diff').removeAttr('disabled')
            } else {
                $('#diff').attr('disabled', true)
            }
        })
        $('input[name=computer_skill]').on('click', (event) => {
            if ($(event.target).val() == 2) {
                $('#Informatics').removeAttr('disabled')
            } else {
                $('#Informatics').attr('disabled', true)
            }
        })
        $('#formUpdate').on('submit', function(event) {
            event.preventDefault()
            $.ajax({
                method: "PUT",
                url: "{{ route('recruitment-proposal.storeUpdate') }}",
                data: $('#formUpdate').serialize(),
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                        window.location.href = response.link
                    } else {
                        toastr.error(response.message);
                    }

                },
                error: function(error) {
                    handleFails(error);
                }
            })
        })

        function handleFails(response) {
            $('#selectRoom, #selectTeam, #selectPosition, #locationEditor, #targetEditor, #recruitEditor, #capabilityEditor, #functionEditor, #missionEditor, #wageEditor, #kpisEditor, #reportEditor, #powerEditor, #bonusEditor, #discEditor').removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formUpdate').find(".has-error").find(".help-block").remove();
                $('#formUpdate').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formUpdate').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response.responseJSON.errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }

        var roomID = $('#selectRoom').find(":selected").val();
        var teamID = "{!! $dataInit['recruit']['team_id'] !!}";
        var positionID = "{!! $dataInit['recruit']['position_id'] !!}";
        var managerRoomID = $('#selectManagerRoom').data('old');

        if (roomID != '') {
            loadTeam(roomID, teamID)
            loadManagerInRoom(roomID, managerRoomID)
        }

        if (teamID != '') {
            loadPosition(teamID, positionID)
        }

        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            loadTeam(id);
            loadManagerInRoom(id)
        });

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            loadPosition(id);
        });
    });

    function loadTeam(id, teamID = null) {
        if (id != '') {
            var url = "{!! route('helper.getTeamByRommID', ':id') !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (Object.keys(data).length > 0) {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectPosition')
                            .find('option')
                            .remove()
                        $('#selectTeam').append($('<option>', {
                            value: '',
                            text: "Vui lòng chọn bộ phận",
                            disabled: true,
                            selected: true,
                        }));
                        Object.keys(data).forEach(function(index) {
                            if (data[index]) {
                                if (Number.parseInt(teamID) == data[index].id) {
                                    $('#selectTeam').append($('<option>', {
                                        value: data[index].id,
                                        text: data[index].name,
                                        selected: true
                                    }));
                                } else {
                                    $('#selectTeam').append($('<option>', {
                                        value: data[index].id,
                                        text: data[index].name,
                                    }));
                                }
                            }
                        })
                    } else {
                        $('#selectTeam')
                            .find('option')
                            .remove()
                    }
                }
            });
        } else {
            $('#selectTeam')
                .find('option')
                .remove()
        }
    }

    function loadPosition(id, poisitionID = null) {
        var url = "{!! route('helper.getPositionByTeamID', ':id') !!}";

        $.ajax({
            type: 'GET',
            url: url.replace(':id', id),
            success: function(data) {
                if (Object.keys(data).length > 0) {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                    $('#selectStaff')
                        .find('option')
                        .remove()
                    $('#selectPosition').append($('<option>', {
                        value: '',
                        text: "Vui lòng chọn chức danh",
                        disabled: true,
                        selected: true,
                    }));
                    Object.keys(data).forEach(function(index) {
                        if (data[index] != null) {
                            if (Number.parseInt(poisitionID) == data[index].id) {
                                $('#selectPosition').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name,
                                    selected: true,
                                }));
                            } else {
                                $('#selectPosition').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name
                                }));
                            }
                        }
                    })
                } else {
                    $('#selectPosition')
                        .find('option')
                        .remove()
                }
            }
        });
    }

    function loadManagerInRoom(id, mangerRoomID = null) {
        var url = "{!! route('helper.getMangerInRoom', ':id') !!}";
        var flgCheck = false;
        $.ajax({
            type: 'GET',
            url: url.replace(':id', id),
            success: function(data) {
                if (Object.keys(data).length > 0) {
                    $('#selectManagerRoom')
                        .find('option')
                        .remove()
                    // $('#selectManagerRoom').append($('<option>', {
                    //     value: '',
                    //     text: "Vui lòng chọn bộ phận",
                    //     disabled: true,
                    //     selected: true,
                    // }));
                    Object.keys(data).forEach(function(index) {
                        if (data[index].basic_info_id != null && !flgCheck) {
                            $('#selectManagerRoom').append($('<option>', {
                                value: data[index].staff.id,
                                text: data[index].staff.full_name,
                                selected: index == 0 ? true : false,
                            }));
                        } else {
                            flgCheck = true;
                            $('#selectManagerRoom').append($('<option>', {
                                value: '',
                                text: 'Chưa Có',
                                selected: true,
                            }));
                        }
                    })
                } else {
                    $('#selectManagerRoom')
                        .find('option')
                        .remove()
                }
            }
        });
    }
</script>
@endsection