@extends('layouts.fe.master2')
@section('title')
@lang('translation.personal')
@endsection
@section('css')
@endsection
@section('content')
<div class="row ">
    <h3 class="text-center">ĐỀ XUẤT TUYỂN DỤNG</h3>
</div>
<!--end col Quốc tịch-->
<form id="formInsert" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row justify-content-between pe-3 mb-3">
        <h3 style="width: fit-content" class="m-0"></h3>
        <button type="submit" class="btn btn-secondary btn-label waves-effect waves-light" style="width: fit-content"><i class="ri-save-3-line label-icon  fs-16 "></i> Lưu</button>
    </div>
    <div class="row ">
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <label for="firstNameinput" class="form-label">Phòng</label>
                    <div class="col-xl-12 form-group">
                        <select name="room_id" id="selectRoom" class="form-select" aria-label="Default select example">
                            <option value="">Vui lòng chọn phòng ban</option>
                            @foreach($dataInit['room'] as $value)
                            <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-4">
                <div class="mb-3">
                    <label for="firstNameinput" class="form-label">Bộ phận</label>
                    <div class="col-xl-12 form-group">
                        <select name="team_id" id="selectTeam" class="form-select" aria-label="Default select example">
                        </select>

                    </div>
                </div>
            </div>

            <div class="col-4">
                <div class="mb-3">
                    <label for="firstNameinput" class="form-label">Chức danh</label>
                    <div class="col-xl-12 form-group">
                        <select name="position_id" id="selectPosition" class="form-select" aria-label="Default select example">
                        </select>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row ">
                            <div class="row">

                                <div class="col-6 ">
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="number" class="form-label " style="margin-top: 10px">1. Số lượng
                                                tuyển dụng</label>
                                        </div>

                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control @error('number_vacanciess') is-invalid @enderror" placeholder="(Điền thông tin)" id="number_vacancies" name="number_vacancies" value="{{ old('number_vacancies') }}">
                                            @error('number_vacancies')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 1 Số lượng tuyển dụng-->
                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="purpose" class="form-label" style="margin-top: 10px">2. Mục
                                                    đích
                                                    tuyển dụng</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-6 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" checked id="new" value="1" name="recruitment_purpose">
                                                    </div>
                                                    <label for="new" class=" form-label mb-0 " style="margin-top: 2px">Tuyển mới</label>
                                                </div>
                                                <div class="col-6 d-flex justify-content-end align-items-center ">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="replace" value="2" name="recruitment_purpose">
                                                    </div>
                                                    <label for="replace" class=" form-label mb-0 " style="margin-top: 2px">Tuyển thay
                                                        thế</label>
                                                </div>
                                                @error('recruitment_purpose')
                                                <small class="help-block text-danger">{{ $message }} *</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex">
                                            <div class="col-4 form-group">
                                                <label for="reason" class="form-label " style="margin-top: 10px">Lý
                                                    do:</label>
                                            </div>
                                            <div class="col-8">
                                                <input type="text" class="form-control" placeholder="(Điền thông tin)" id="reason" name="reason">
                                                @error('reason')
                                                <small class="help-block text-danger">{{ $message }} *</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="time" class="form-label " style="margin-top: 10px">3. Thời gian
                                                tuyển dụng</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control datepicker" placeholder="(Điền thông tin)" id="recruitment_time" name="recruitment_time">
                                            @error('recruitment_time')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="type" class="form-label" style="margin-top: 10px">4. Loại
                                                    hình làm việc</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-6 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="full" checked value="1" name="type_work">
                                                    </div>
                                                    <label for="full" class=" form-label mb-0 " style="margin-top: 2px">Toàn thời gian</label>
                                                </div>
                                                <div class="col-6 d-flex justify-content-end align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="co-operate" value="2" name="type_work">
                                                    </div>
                                                    <label for="co-operate" class=" form-label mb-0 " style="margin-top: 2px">Hợp tác</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col 4 Loại hình làm việc-->
                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="demarcation" class="form-label" style="margin-top: 10px">5.
                                                    Định
                                                    biên nhân sự</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-6 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="plan" checked value="1" name="manpower_demarcation">
                                                    </div>
                                                    <label for="plan" class=" form-label mb-0 " style="margin-top: 2px">Theo kế hoạch năm</label>
                                                </div>
                                                <div class="col-6 d-flex justify-content-end align-items-center ">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="budget" value="2" name="manpower_demarcation">
                                                    </div>
                                                    <label for="budget" class=" form-label mb-0 " style="margin-top: 2px">Theo ngân sách năm</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col 5 Định biên nhân sự-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="age" class="form-label " style="margin-top: 10px">6. Độ
                                                tuổi</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="number" class="form-control" placeholder="(Điền thông tin)" id="age" name="age">
                                            @error('age')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 6 Độ tuổi-->
                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="gender" class="form-label" style="margin-top: 10px">7. Giới
                                                    tính</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-4 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="male" value="1" name="gender">
                                                    </div>
                                                    <label for="male" class=" form-label mb-0 " style="margin-top: 2px">Nam</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-center align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="female" value="0" name="gender">
                                                    </div>
                                                    <label for="female" class=" form-label mb-0 " style="margin-top: 2px">Nữ</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-end align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" checked id="noRequire" value="2" name="gender">
                                                    </div>
                                                    <label for="noRequire" class=" form-label mb-0 " style="margin-top: 2px">Không yêu cầu</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col 7 Giới tính-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="experience_year" class="form-label " style="margin-top: 10px">8.
                                                Số năm
                                                kinh nghiệm</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Điền thông tin)" id="experience_year" name="experience_year">
                                            @error('experience_year')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 8. Số năm kinh nghiệm-->
                                </div>
                                <!--end col 1->8-->


                                <div class="col-6 ">

                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="education" class="form-label" style="margin-top: 10px">9.
                                                    Trình độ học vấn</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-4 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="university" value="0" name="academic_level">
                                                    </div>
                                                    <label for="university" class=" form-label mb-0 " style="margin-top: 2px">Đại học</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-center align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="college" value="1" name="academic_level">
                                                    </div>
                                                    <label for="college" class=" form-label mb-0 " style="margin-top: 2px">Cao đẳng</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-end align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="intermediate" value="2" name="academic_level">
                                                    </div>
                                                    <label for="intermediate" class=" form-label mb-0 " style="margin-top: 2px">Trung cấp</label>
                                                </div>
                                            </div>

                                            @error('academic_level')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                        <div class="col-12 d-flex">
                                            <div class="col-4">
                                                <!-- <input class="form-check-input fs-15" type="radio" id="intermediate" value="2" name="academic_level">
                                                <label for="intermediate" class=" form-label mb-0 " style="margin-top: 2px">Khác</label> -->
                                            </div>
                                            <div class="col-8 d-flex px-3 form-group">
                                                <div class="col-4 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="university" value="3" name="academic_level">
                                                    </div>
                                                    <label for="university" class=" form-label mb-0 " style="margin-top: 2px">Khác</label>
                                                </div>
                                                <div class="col-8 d-flex justify-content-start align-items-center form-group">
                                                    <input disabled type="text" class="form-control" placeholder="(Điền thông tin)" id="diff" name="academic_level_other">
                                                </div>
                                                @error('academic_level_other')
                                                <small class="help-block text-danger">{{ $message }} *</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col 9 Trình độ học vấn-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="specialize" class="form-label " style="margin-top: 10px">10.
                                                Trình
                                                độ chuyên môn</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Ghi rõ chi tiết các yêu cầu)" id="specialize" name="qualification">
                                            @error('qualification')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 10. Trình độ chuyên môn-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="mission" class="form-label " style="margin-top: 10px">11. Nhiệm
                                                vụ công việc</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Link thông tin từ 12 TC) Có thể chỉnh sửa" id="mission" name="job_duties">
                                            @error('job_duties')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 11. Nhiệm vụ công việc-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="disc" class="form-label " style="margin-top: 10px">12.
                                                DISC</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Link thông tin từ 12 TC) Có thể chỉnh sửa" id="disc" name="disc">
                                            @error('disc')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 12. DISC-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="language" class="form-label " style="margin-top: 10px">13. Trình
                                                độ ngoại ngữ</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Ghi rõ chi tiết các yêu cầu)" id="language" name="english_level">
                                            @error('english_level')
                                            <small class="help-block text-danger">{{ $message }} *</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <!--end col 13. Trình độ ngoại ngữ-->
                                    <div class="mb-3 ">
                                        <div class="d-flex">
                                            <div class="col-4">
                                                <label for="Informatics" class="form-label" style="margin-top: 10px">14.
                                                    Trình độ tin học</label>
                                            </div>
                                            <div class="col-8 d-flex px-3">
                                                <div class="col-4 d-flex justify-content-start align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" checked id="mso" value="1" name="computer_skill">
                                                    </div>
                                                    <label for="mso" class=" form-label mb-0 " style="margin-top: 2px">MS Office</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-center align-items-center">
                                                    <div class=" form-check" style="width: fit-content;">
                                                        <input class="form-check-input fs-15" type="radio" id="diffInfomatics" value="2" name="computer_skill">
                                                    </div>
                                                    <label for="diffInfomatics" class=" form-label mb-0 " style="margin-top: 2px">Khác</label>
                                                </div>
                                                <div class="col-4 d-flex justify-content-end align-items-center">
                                                    <textarea disabled type="text" class="form-control" placeholder="" id="Informatics" name="computer_skill_other"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col 14. Trình độ tin học-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="company" class="form-label " style="margin-top: 10px">15. Yêu
                                                cầu thiết bị</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Ghi rõ chi tiết các yêu cầu)" id="company" name="device_requirements">
                                        </div>
                                    </div>
                                    <!--end col 15. Yêu cầu thiết bị-->
                                    <div class="mb-3 d-flex">
                                        <div class="col-4">
                                            <label for="company" class="form-label " style="margin-top: 10px">16. Yêu
                                                cầu khác</label>
                                        </div>
                                        <div class="col-8 form-group">
                                            <input type="text" class="form-control" placeholder="(Ghi rõ chi tiết các yêu cầu)" id="company" name="other_requirements">
                                        </div>
                                    </div>
                                    <!--end col 16. Yêu cầu khác -->

                                </div>
                                <!--end col 9->16-->
                            </div>
                            <!--end row-->
                        </div>
                    </div>
                </div>
            </div>


        </div>


        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">


                        <div class="row mb-5">
                            <div class="col-3 d-flex justify-content-center">
                                <div class="row  ">
                                    <div class="col-12 text-center">
                                        <h5>Người Đề Xuất</h5>
                                    </div>
                                    <div class="col-12 text-center">
                                        <label for="firstNameinput" class="form-label mb-0 mt-4 ">{{ auth()->user()->name }}</label>
                                    </div>
                                    <!-- <div class="col-12 text-center">
                                    <label for="firstNameinput" class="form-label mb-0 mt-4 ">Đã xác nhận</label>
                                </div> -->

                                    <div class="col-12 d-flex justify-content-center mt-4">
                                        <div class=" form-check" style="width: fit-content;">
                                            <input class="form-check-input fs-15" type="checkbox" id="assign" value="1" name="assign">
                                        </div>
                                        <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác
                                            nhận</label>
                                    </div>

                                </div>
                            </div>

                            <div class="col-3 d-flex justify-content-center">
                                <div class="row  ">
                                    <div class="col-12 text-center">
                                        <h5>Quản Lý Phòng</h5>
                                    </div>
                                    <input hidden name="type_confirm_1" value="4">
                                    <div class="col-12">
                                        <select id="selectManagerRoom" name="selectConfirm[]" class="form-select text-center mt-3" aria-label="Default select example">
                                            <option value=""> Chưa Có </option>
                                        </select>

                                    </div>
                                    <!-- <div class="col-12 text-center">
                                    <label for="firstNameinput" class="form-label mb-0 mt-3 ">Đã xác nhận</label>
                                </div>
                                <div class="col-12 d-flex justify-content-center mt-3">
                                    <div class=" form-check" style="width: fit-content;">
                                        <input class="form-check-input fs-15" type="checkbox" id="pqt" value="1" name="pqt">
                                    </div>
                                    <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác
                                        nhận</label>
                                </div> -->
                                </div>
                            </div>

                            <div class="col-3 d-flex justify-content-center">
                                <div class="row  ">
                                    <div class="col-12 text-center">
                                        <h5>P.QT HC-NS</h5>
                                    </div>
                                    <input hidden name="type_confirm_2" value="2">
                                    <div class="col-12">
                                        <select id="selectHr" name="selectConfirm[]" class="form-select text-center mt-3" aria-label="Default select example">
                                            @if(!empty($dataInit['hr']))
                                            @foreach($dataInit['hr'] as $value)
                                            @if(!empty($value['basic_info_id']))
                                            <option value="{{ $value->staff->id }}">{{ $value->staff->full_name }}
                                            </option>
                                            @endif
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <!-- <div class="col-12 text-center">
                                    <label for="firstNameinput" class="form-label mb-0 mt-3 ">Đã xác nhận</label>
                                </div>
                                <div class="col-12 d-flex justify-content-center mt-3">
                                    <div class=" form-check" style="width: fit-content;">
                                        <input class="form-check-input fs-15" type="checkbox" id="pqt" value="1" name="pqt">
                                    </div>
                                    <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác
                                        nhận</label>
                                </div> -->
                                </div>
                            </div>

                            <div class="col-3 d-flex justify-content-center">
                                <div class="row  ">
                                    <div class="col-12 text-center">
                                        <h5>Ban Giám Đốc</h5>
                                    </div>
                                    <input hidden name="type_confirm_3" value="3">
                                    <div class="col-12">
                                        <select name="selectConfirm[]" class="form-select text-center mt-3" aria-label="Default select example">
                                            @if(!empty($dataInit['manager']))
                                            @foreach($dataInit['manager'] as $value)
                                            @if(!empty($value['basic_info_id']))
                                            <option value="{{ $value->staff->id }}">{{ $value->staff->full_name}}
                                            </option>
                                            @endif
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <!-- <div class="col-12 text-center">
                                    <label for="firstNameinput" class="form-label mb-0 mt-3 ">Chờ xác nhận</label>
                                </div>
                                <div class="col-12 d-flex justify-content-center mt-3">
                                    <div class=" form-check" style="width: fit-content;">
                                        <input class="form-check-input fs-15" type="checkbox" id="manager" value="1" name="manager">
                                    </div>
                                    <label for="confirm" class=" form-label mb-0 " style="margin-top: 2px">Xác
                                        nhận</label>
                                </div> -->
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row justify-content-end">

                        <div class="col-4 d-flex justify-content-center">
                            <div class="col-12 text-center">
                                <textarea class="form-control" aria-label="With textarea" rows="5" id="texta1" name="texta1"></textarea>
                            </div>
                        </div>
                        <div class="col-4 d-flex justify-content-center">
                            <div class="col-12 text-center">
                                <textarea class="form-control" aria-label="With textarea" rows="5" id="texta2" name="texta2"></textarea>
                            </div>
                        </div>
                    </div> -->
                    </div>
                </div>
            </div>


        </div>
    </div>
</form>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('input[name=academic_level]').on('click', (event) => {
            if ($(event.target).val() == 3) {
                $('#diff').removeAttr('disabled')
            } else {
                $('#diff').attr('disabled', true)
            }
        })
        $('input[name=computer_skill]').on('click', (event) => {
            if ($(event.target).val() == 2) {
                $('#Informatics').removeAttr('disabled')
            } else {
                $('#Informatics').attr('disabled', true)
            }
        })
        $('#selectRoom').on('change', function() {
            var id = $('#selectRoom').find(":selected").val();
            if (id != '') {
                var url = "{!! route('helper.getTeamByRommID', ':id') !!}";
                var urlMangerRoom = "{!! route('helper.getMangerInRoom', ':id') !!}";
                $.ajax({
                    type: 'GET',
                    url: url.replace(':id', id),
                    success: function(data) {
                        if (Object.keys(data).length > 0) {
                            $('#selectTeam')
                                .find('option')
                                .remove()
                            $('#selectStaff')
                                .find('option')
                                .remove()
                            $('#selectPosition')
                                .find('option')
                                .remove()
                            $('#selectTeam').append($('<option>', {
                                value: '',
                                text: "Vui lòng chọn bộ phận",
                                disabled: true,
                                selected: true,
                            }));
                            Object.keys(data).forEach(function(index) {
                                $('#selectTeam').append($('<option>', {
                                    value: data[index].id,
                                    text: data[index].name
                                }));
                            })
                        } else {
                            $('#selectTeam')
                                .find('option')
                                .remove()
                        }
                    }
                });

                $.ajax({
                    type: 'GET',
                    url: urlMangerRoom.replace(':id', id),
                    success: function(data) {
                        console.log(data);
                        if (Object.keys(data).length > 0) {
                            $('#selectManagerRoom')
                                .find('option')
                                .remove()
                            // $('#selectManagerRoom').append($('<option>', {
                            //     value: '',
                            //     text: "Vui lòng chọn bộ phận",
                            //     disabled: true,
                            //     selected: true,
                            // }));
                            let x = false;
                            Object.keys(data).forEach(function(index) {
                                if (data[index].staff) {
                                    if (!x) {
                                        $('#selectManagerRoom').append($('<option>', {
                                            value: "",
                                            text: "Không Chọn",
                                            selected: index == 0 ? true : false,
                                        }));
                                    }
                                    $('#selectManagerRoom').append($('<option>', {
                                        value: data[index].staff.id !=
                                            null ? data[index].staff.id : "",
                                        text: data[index].staff.full_name !=
                                            null ? data[index].staff
                                            .full_name : "Chưa Có",
                                        selected: index == 0 ? true : false,
                                        // value: data[index].staff.id,
                                        // text: data[index].staff.full_name,
                                        // selected : index == 0 ? true : false,
                                    }));


                                    x = true;
                                } 
                            });
                        } else {
                            $('#selectManagerRoom')
                                .find('option')
                                .remove()
                             
                            $('#selectManagerRoom').append($('<option>', {
                                value: "",
                                text: "Chưa Có",
                                selected: true,
                            }));
                                

                        }
                    }
                });
            } else {
                $('#selectTeam')
                    .find('option')
                    .remove()
            }
        });

        $('#formInsert').on('submit', function(event) {
            event.preventDefault()
            $.ajax({
                method: "POST",
                url: "{{ route('recruitment-proposal.storeInsert') }}",
                data: $('#formInsert').serialize(),
                success: function(response) {
                    if (response.status) {
                        toastr.success(response.message);
                        window.location.href = response.link
                    } else {
                        toastr.error(response.message);
                    }

                },
                error: function(error) {
                    handleFails(error);
                }
            })
        })

        function handleFails(response) {
            $('#selectRoom, #selectTeam, #selectPosition, #locationEditor, #targetEditor, #recruitEditor, #capabilityEditor, #functionEditor, #missionEditor, #wageEditor, #kpisEditor, #reportEditor, #powerEditor, #bonusEditor, #discEditor')
                .removeClass('is-invalid')
            if (typeof response.responseJSON.errors != "undefined") {
                var keys = Object.keys(response.responseJSON.errors);
                $('#formInsert').find(".has-error").find(".help-block").remove();
                $('#formInsert').find(".has-error").removeClass("has-error");

                for (var i = 0; i < keys.length; i++) {
                    // Escape dot that comes with error in array fields
                    var key = keys[i].replace(".", '\\.');
                    var formarray = keys[i];

                    // If the response has form array
                    if (formarray.indexOf('.') > 0) {
                        var array = formarray.split('.');
                        response.responseJSON.errors[keys[i]] = response.responseJSON.errors[keys[i]];
                        key = array[0] + '[' + array[1] + ']';
                    }

                    var ele = $('#formInsert').find("[name='" + key + "']");

                    ele.addClass('is-invalid')

                    var grp = ele.closest(".form-group");
                    $(grp).find(".help-block").remove();

                    //check if wysihtml5 editor exist
                    var wys = $(grp).find(".note-editor note-frame card").length;

                    if (wys > 0) {
                        var helpBlockContainer = $(grp);
                    } else {
                        var helpBlockContainer = $(grp).find("div:first");
                    }
                    if ($(ele).is(':radio')) {
                        helpBlockContainer = $(grp);
                    }

                    if (helpBlockContainer.length == 0) {
                        helpBlockContainer = $(grp);
                    }

                    helpBlockContainer.append('<div class="ml-2 help-block text-danger">' + response.responseJSON
                        .errors[keys[i]] + '</div>');
                    $(grp).addClass("has-error");
                }

                if (keys.length > 0) {
                    var element = $("[name='" + keys[0] + "']");
                    if (element.length > 0) {
                        $("html, body").animate({
                            scrollTop: element.offset().top - 150
                        }, 200);
                    }
                }
            }
        }

        $('#selectTeam').on('change', function() {
            var id = $('#selectTeam').find(":selected").val();
            var url = "{!! route('helper.getPositionByTeamID', ':id') !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':id', id),
                success: function(data) {
                    if (Object.keys(data).length > 0) {
                        $('#selectPosition')
                            .find('option')
                            .remove()
                        $('#selectStaff')
                            .find('option')
                            .remove()
                        $('#selectPosition').append($('<option>', {
                            value: '',
                            text: "Vui lòng chọn chức danh",
                            disabled: true,
                            selected: true,
                        }));
                        Object.keys(data).forEach(function(index) {
                            $('#selectPosition').append($('<option>', {
                                value: data[index].id,
                                text: data[index].name
                            }));
                        })
                    } else {
                        $('#selectPosition')
                            .find('option')
                            .remove()
                    }
                }
            });
        });

        $('#selectPosition').on('change', function() {
            var room_id = $('#selectRoom').find(":selected").val();
            var team_id = $('#selectTeam').find(":selected").val();
            var position_id = $('#selectPosition').find(":selected").val();

            var url =
                "{!! route('helper.getCriteriaByConditions', [':room_id', ':team_id', ':positon_id']) !!}";

            $.ajax({
                type: 'GET',
                url: url.replace(':room_id', room_id).replace(':team_id', team_id).replace(
                    ':positon_id', position_id),
                success: function(data) {
                    if (Object.keys(data).length > 0) {
                        $('#mission').val(data.mission.replace(/(<([^>]+)>)/gi, ""));
                        $('#disc').val(data.disc.replace(/(<([^>]+)>)/gi, ""));
                    }
                }
            });
        })
    });
</script>
@endsection