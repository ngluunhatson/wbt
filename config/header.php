<?php
return [
    'intern' => [
        'Tầm nhìn' => [
            [
                'title' => 'Tầm nhìn',
                'name' => 'vision',
            ]
        ],
        'Sứ mệnh' => [
            [
                'title' => 'Sứ mệnh',
                'name' => 'mission',
            ]
        ],
        'Giá trị văn hoá' => [
            [
                'title' => 'Giá trị văn hoá',
                'name' => 'culture',
            ]
        ],
        'Mục tiêu' => [
            'Mục tiêu' => [
                'title' => 'Mục tiêu',
                'name' => 'target',
            ],
            'Mục tiêu công ty' => [
                'title' => 'Mục tiêu công ty',
                'name' => 'target_company',
            ],
            'Mục tiêu phong ban va muc ti vi tri' => [
                'title' => 'Mục tiêu',
                'name' => 'target_room',
            ],
        ],
        'Sơ đồ tổ chức' => [
            'Sơ đồ tổ chức' => [
                'title' => 'Sơ đồ tổ chức',
                'name' => 'organizational',
            ],
            'Sơ đồ tổ chức công ty' => [
                'title' => 'Sơ đồ tổ chức công ty',
                'name' => 'organizational_company',
            ],
            'Sơ đồ tổ chức phòng ban' => [
                'title' => 'Sơ đồ tổ chức phòng ban',
                'name' => 'organizational_room',
            ],
            'Nấc thang phát triển nhân viên' => [
                'title' => 'Nấc thang phát triển nhân viên',
                'name' => 'organizational_staff',
            ],
        ],
        '12 tiêu chí quản lý một trí' => [
            'criteria_1' => [
                'title' => 'Vị trí',
                'name' => 'criteria_1',
            ],
            'criteria_2' => [
                'title' => 'Mục tiêu',
                'name' => 'criteria_2',
            ],
            'criteria_3' => [
                'title' => 'Yêu cầu tuyển dụng',
                'name' => 'criteria_3',
            ],
            'criteria_4' => [
                'title' => 'Yêu cầu năng lực',
                'name' => 'criteria_4',
            ],
            'criteria_5' => [
                'title' => 'Chức năng',
                'name' => 'criteria_5',
            ],
            'criteria_6' => [
                'title' => 'Nhiệm vụ',
                'name' => 'criteria_6',
            ],
            'criteria_7' => [
                'title' => 'Quyền hạn',
                'name' => 'criteria_7',
            ],
            'criteria_8' => [
                'title' => 'Lương',
                'name' => 'criteria_8',
            ],
            'criteria_9' => [
                'title' => 'KPIs',
                'name' => 'criteria_9',
            ],
            'criteria_10' => [
                'title' => 'Thưởng',
                'name' => 'criteria_10',
            ],
            'criteria_11' => [
                'title' => 'DISC & Motivators',
                'name' => 'criteria_11',
            ],
            'criteria_12' => [
                'title' => 'Báo cáo ',
                'name' => 'criteria_12',
            ],
        ],
        'KPIs' => [
            'KPIs' => [
                'title' => 'KPIs',
                'name' => 'kpis',
            ],
            'KPIs_2' => [
                'title' => 'Hướng dẫn triển khai và thực hiện KPIs của vị trí trong thời gian thử việc',
                'name' => 'organizational_company',
            ],
        ],
        'Quy trình' => [
            'Quy trình' => [
                'title' => 'Quy trinh',
                'sub_title' => 'Quy trinh',
                'name' => 'procedure',
            ],
            'Phòng Quản trị Kinh Doanh' => [
                'title' => 'Quy trinh',
                'sub_title' => 'Phòng Quản trị Kinh Doanh',
                'name' => 'qtkd',
            ],
            'Phòng Quản trị Marketing' => [
                'title' => 'Quy trinh',
                'sub_title' => 'Phòng Quản trị Marketing',
                'name' => 'qtmr',
            ],
            'Phòng quản trị Hành Chính - Nhân Sự' => [
                'title' => 'Quy trinh',
                'sub_title' => 'Phòng quản trị Hành Chính - Nhân Sự',
                'name' => 'qthcns',
            ],
            'Phòng Quản trị Tài Chính - Kế Toán' => [
                'title' => 'Quy trinh',
                'sub_title' => 'Phòng Quản trị Tài Chính - Kế Toán',
                'name' => 'qttckt',
            ],
            'Phòng Quản trị Mua Hàng' => [
                'title' => 'Quy trinh',
                'sub_title' => 'Phòng Quản trị Mua Hàng',
                'name' => 'qtmr',
            ],
        ],
        'Quản trị (Hệ thống và công nghệ)' => [
            'administration_1' => [
                'title' => 'Quản trị (Hệ thống và công nghệ)',
                'name' => 'administration_1',
            ],
            'administration_2' => [
                'title' => 'Hướng dẫn triển sử dụng hệ thống làm việc tại công ty',
                'name' => 'administration_2',
            ],
        ],
    ],

    'product' => [
        'Thang sản phẩm ActionCOACH CBD' => [
            [
                'title' => 'Thang sản phẩm ActionCOACH CBD',
                'name' => 'CBD',
            ]
        ],
        'Tham gia khoá học ActionCLUB' => [
            'ac_1' => [
                'title' => 'Tham gia khoá học ActionCLUB',
                'name' => 'ac_1',
            ],
            'ac_2' => [
                'title' => '6 Steps',
                'name' => 'ac_2',
            ],
            'ac_3' => [
                'title' => 'Purpose',
                'name' => 'ac_3',
            ],
            'ac_4' => [
                'title' => 'Genarating Cashflow',
                'name' => 'ac_4',
            ],
            'ac_5' => [
                'title' => 'Streetwise Marketing',
                'name' => 'ac_5',
            ],
            'ac_6' => [
                'title' => 'SalesRICH & PhoneRICH',
                'name' => 'ac_6',
            ],
            'ac_7' => [
                'title' => 'TEAMRICH',
                'name' => 'ac_7',
            ],
            'ac_8' => [
                'title' => 'SYSTEMRICH',
                'name' => 'ac_8',
            ],
            'ac_9' => [
                'title' => 'LEVERAGE Game',
                'name' => 'ac_9',
            ],
        ],
        'GrowthClub' => [
            [
                'title' => 'GrowthClub',
                'name' => 'growth_club',
            ]
        ],
        'Các sản phẩm phụ bao gồm : sổ tay, sách ...' => [
            [
                'title' => 'Các sản phẩm phụ bao gồm : sổ tay, sách ...',
                'name' => 'byproduct',
            ]
        ],
    ],

    'criteria' => [
        'expertise' =>  'Kiến thức chuyên môn phù hợp với công việc',
        'work_experience' =>  'Kinh nghiệm làm việc phù hợp với nhu cầu công việc',
        'problem_solving' =>  'Khả năng giải quyết vấn đề và xử lý tình hống',
        'plan' =>  'Khả năng hoạch định sắp xếp công việc',
        'orientation' =>  'Khả năng định hướng và phát triển bản thân',
        'commitment_value' =>  'ý thức thời gian và giá trị cam kết',
        'communicate' =>  'Kỹ năng giao tiếp và truyền đạt thông tin',
        'team_work' =>  'Kỹ năng làm việc nhóm và khả năng lãnh đạo / quản lý (đối với vị trí quản lý)',
        'english_level' =>  'Trình độ ngoại ngữ',
        'it_skills' =>  'Kỹ năng tin học',
        'psychological_control' =>  'Mức độ kiểm soát tâm lý trong công việc',
        'attitude' =>  'Thái độ đối với tầm nhìn sứ mệnh và giá trị văn hoá',
    ],
    'cbdfirm' => [
        'Khoá học CIA (ActionCLUB)' => [
            'cia_1' => [
                'title' => '6 Steps',
                'name' => 'cia_1',
            ],
            'cia_2' => [
                'title' => 'Purpose',
                'name' => 'cia_2',
            ],
            'cia_3' => [
                'title' => 'Genarating Cashflow',
                'name' => 'cia_3',
            ],
            'cia_4' => [
                'title' => 'Streetwise Marketing',
                'name' => 'cia_4',
            ],
            'cia_5' => [
                'title' => 'SalesRICH & PhoneRICH',
                'name' => 'cia_5',
            ],
            'cia_6' => [
                'title' => 'TEAMRICH',
                'name' => 'cia_6',
            ],
            'cia_7' => [
                'title' => 'SYSTEMRICH',
                'name' => 'cia_7',
            ],
            'cia_8' => [
                'title' => 'LEVERAGE Game',
                'name' => 'cia_8',
            ],
        ],
        'Khoá học CEO key to success (Assessment 247)' => [
            'ceo_1' => [
                'title' => 'DISC',
                'name' => 'ceo_1',
            ],
            'ceo_2' => [
                'title' => 'Motivators',
                'name' => 'ceo_2',
            ],
            'ceo_3' => [
                'title' => 'Sale IQ Plus',
                'name' => 'ceo_3',
            ],
            'ceo_4' => [
                'title' => 'EIQ',
                'name' => 'ceo_4',
            ],
            'ceo_5' => [
                'title' => 'Learning Style',
                'name' => 'ceo_5',
            ],
        ],
    ],


    'payslip_stt' => [
        'A',
        'A.1',
        'A.2',
        'B',
        'C',
        'C.1',
        'C.2',
        'C.3',
        'C.4',
        'D',
        'E',
        'E.1',
        'E.2',
        'E.3',
        'E.4',
        'G',
        'H',
    ],
    'payslip_content' => [
        'Chi tiết các khoản',		
        'Mức lương chính',		
        'Các khoản hỗ trợ khác',		
        'Thu nhập theo ngày công',		
        'Các khoản khác',		
        'Lương KPIs',		
        'Lương ngoài giờ',		
        'Bổ sung lương',		
        'Giảm trừ lương',		
        'Tổng thu nhập (D = B + C)',		
        'Trích nộp BHXH và thuế TNCN',		
        'BHXH (8%)',		
        'BHYT (1.5%)',		
        'BHTN (1%)',		
        'Thuế TNCN',		
        'Thực lãnh (G = D - E)',		
        'Ghi chú',
    ],

    'internship_section_tile' => [
        1 => [
            "Tầm nhìn",
            "Sứ mệnh",
            "Giá trị văn hóa",
            "Mục tiêu",
            "Mục tiêu công ty",
            "Mục tiêu phòng ban và mục tiêu vị trí",
            "Sơ đồ tổ chức",
            "Sơ đồ tổ chức công ty",
            "Sơ đồ tổ chức các phòng ban",
            "Nấc thang phát triển nhân viên",
            "12 tiêu chí quản lý một vị trí",
            "Vị trí",
            "Mục tiêu",
            "Yêu cầu tuyển dụng",
            "Yêu cầu năng lực",
            "Chức năng",
            "Nhiệm vụ",
            "Quyền hạn",
            "Lương",
            "KPIs",
            "Thưởng",
            "DISC & Motivators",
            "Báo cáo",
            "KPIs",
            "Hướng dẫn triển khai và thực hiện KPIs của vị trí trong thời gian thử việc",
            "Quy trình",
            "",
            "Quản trị (Hệ thống và công nghệ)",
            "Hướng dẫn sử dụng hệ thống làm việc tại công ty"
        ],
        2 => [
            "Thang sản phẩm ActionCOACH CBD",
            "Tham gia khóa học ActionCLUB",
            "6 Steps",
            "Purpose",
            "Generating Cashflow",
            "Streetwise Marketing",
            "SalesRICH & PhoneRICH",
            "TEAMRICH",
            "SYSTEMRICH",
            "LEVERAGE Game",
            "GrowthClub",
            "Các sản phẩm phụ bao gồm: sổ tay, sách,….",
        ],
    ],

];
